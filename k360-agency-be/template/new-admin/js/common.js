/**
 * Elektron - An Admin Toolkit
 * @version 0.3.1
 * @license MIT
 * @link https://github.com/onokumus/elektron#readme
 */
'use strict';

/* eslint-disable no-undef */
$(function () {
	if ($(window).width() < 992) {
		$('#app-side').onoffcanvas('hide');
	} else {
		$('#app-side').onoffcanvas('show');
	}

	$('.side-nav .unifyMenu').unifyMenu({ toggle: true });

	$('#app-side-hoverable-toggler').on('click', function () {
		$('.app-side').toggleClass('is-hoverable');
		$(undefined).children('i.fa').toggleClass('fa-angle-right fa-angle-left');
	});

	$('#app-side-mini-toggler').on('click', function () {
		$('.app-side').toggleClass('is-mini');
		$("#app-side-mini-toggler i").toggleClass('icon-chevron-thin-left icon-chevron-thin-right');
	});

	$('#onoffcanvas-nav').on('click', function () {
		$('.app-side').toggleClass('left-toggle');
		$('.app-main').toggleClass('left-toggle');
		$("#onoffcanvas-nav i").toggleClass('icon-sort icon-menu5');
	});	

	$('.onoffcanvas-toggler').on('click', function () {
		$(".onoffcanvas-toggler i").toggleClass('icon-chevron-thin-left icon-chevron-thin-right');
	});
});


// $(function() {
// 	$('#unifyMenu')
// 	.unifyMenu()
// 	.on('shown.unifyMenu', function(event) {
// 			$('body, html').animate({
// 					scrollTop: $(event.target).parent('li').position().top
// 			}, 600);
// 	});
// });


// Bootstrap Tooltip
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})


// Bootstrap Popover
$(function () {
  $('[data-toggle="popover"]').popover()
})
$('.popover-dismiss').popover({
  trigger: 'focus'
})


// Todays Date
$(function() {
	var interval = setInterval(function() {
		var momentNow = moment();
		$('#today-date').html(momentNow.format('MMMM . DD') + ' '
		+ momentNow.format('. dddd').substring(0,5).toUpperCase());
	}, 100);
});


// Task list
$('.task-list').on('click', 'li.list', function() {
	$(this).toggleClass('completed');
});


// Loading
$(function() {
	$(".loading-wrapper").fadeOut();
});

// const objImg = { arrImg: [] };
// const proxyUpload = new Proxy(objImg, {
// 	set(obj, prop, value){
// 		console.log({ obj, prop, value, objImg });

// 		// obj[prop] = [...obj[prop], value];
// 		previewUpload(value);
// 		Reflect.set(objImg, 'arrImg', [...obj[prop], value]);

// 		return true;
// 	}
// });

function validatePhone(phone) {
	let phone_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
	return phone_regex.test(phone);
}

function validateEmail(email){
	const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}


// ======================= UPLOAD S3 =========================
let arrLinksUploaded = [];
let isDoneUpload = true;
let countDone = 0;

window.onbeforeunload = function() {
	if(!isDoneUpload){
		return isDoneUpload;
	}else{
		return;
	}
};

async function addFileToUpload(multiInput, cbDone = null, cbProgress = null, elementContainer = '') {
	isDoneUpload = false;
	let arrUrlPromise = [];
	let totalFile = 0;

	// Thêm url vào mảng arrUrlPromise
	if(multiInput.length === 1){
		const url = generateLinkS3({ file: multiInput[0] });

		arrUrlPromise = [url];
		totalFile = 1;
	} else{
		for (const input of multiInput) {
			if(input.files && input.files.length){
                for (const file of input.files) {
                    const url = generateLinkS3({ file, type: input.type });
                    arrUrlPromise = [...arrUrlPromise, url];
                    totalFile++;
                }
            }else{
                let url = generateLinkS3({ file: input, type: input.type });
                arrUrlPromise = [...arrUrlPromise, url];
                totalFile++;
            }
		}
	}

	// Generate link S3 -> get list link upload -> upload S3 async
	const listUrl = await Promise.all(arrUrlPromise);
	listUrl.length && listUrl.map(link => {
		const { file, uri, signedUrl, type } = link;
		uploadFileS3({ file, uri, signedUrl, elementContainer, totalFile, type, cbDone, cbProgress });
	})

	return () => listUrl;
}

function generateLinkS3({ file, type = '' }) {
	return new Promise(resolve => {
		const { type: contentType, name: fileName } = file;
		// console.log({ contentType, fileName });
		$.get(
			`${location.origin}/generate-link-s3?fileName=${fileName}&type=${contentType}`, 
			signedUrl => resolve({ 
				signedUrl: signedUrl.linkUpload.data,
				uri: `https://s3-ap-southeast-1.amazonaws.com/ldk-software.k360/root/${signedUrl.fileName}`,
				file,
				type
			})
		);
	})
}

function uploadFileS3({ file, uri, signedUrl, elementContainer, totalFile, type, cbDone, cbProgress }) {
	// console.log({ signedUrl });
	$.ajax({
		url: signedUrl.url,
		type: 'PUT',
		dataType: 'html',
		processData: false,
		headers: { 'Content-Type': file.type },
		crossDomain: true,
		data: file,
		xhr: function() {
			let myXhr = $.ajaxSettings.xhr();

			myXhr.upload.onprogress = function(e) {
				console.log(Math.floor(e.loaded / e.total * 100) + '%');
			};

			if(myXhr.upload){
				if({}.toString.call(cbProgress) === '[object Function]'){
					myXhr.upload.addEventListener('progress', e => {
						if(e.lengthComputable){
							let max = e.total;
							let current = e.loaded;
							let percentage = (current * 100)/max;
							cbProgress(percentage, type);
						}
					}, false);
				} else{
					myXhr.upload.addEventListener('progress', progress, false);
				}
			}

			return myXhr;
		},
	}).done(function(){
		previewUpload({ elementContainer, uri });
		countDone++;
		arrLinksUploaded = [...arrLinksUploaded, {
			uri,
			type
		}]

		if(countDone === totalFile){
			// Check is function
			({}.toString.call(cbDone) === '[object Function]') && cbDone(arrLinksUploaded);

			arrLinksUploaded = [];
			isDoneUpload = true;
			countDone = 0;
		}
	}).fail(function (error) {
		console.error(error);
	});
}

function progress(e){
	if(e.lengthComputable){
		let max = e.total;
		let current = e.loaded;
		let percentage = (current * 100)/max;
		$('.progress').css({ width: parseInt(percentage) + '%' });
	}
}

function previewUpload({ uri, elementContainer }) {
	const container = $(elementContainer);
	if(container.length){
		const img = `<img src="${uri}" alt="Img Preview" />`;
		container.append(img);
	}
}


async function generateQRCode({ text }) {
	console.log({ text });
	return new Promise(resolve => {
		try {
			console.log('jonnnnnnnnnnnnnnnnnnnn');
			$.ajax({
				url: `${location.origin}/api/add-qr-code?text=${text}`,
				method: "GET",
				success: resp => {
					console.log({ resp });
					return resolve(resp);
				}
			})
		} catch (error) {
			return resolve({ error: true });
		}
	})
}
