"use strict";

const agencySession = new (require('./intalize/session'))('agency-session');

module.exports = {
    /**
     * 
     * SAVE CONTEXT AGENCY USER CHOOSED
     */
    saveContextAgency(session, { agencyID }) {
        agencySession.saveSession(session, {
            agencyID
        });
    },

    getContextAgency(session) {
        return agencySession.getSession(session);
    },

	destroySession (session) {
        return agencySession.detroySession(session);
    },
};
