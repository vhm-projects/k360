"use strict";

exports._base_logo1_url = "";
exports._base_logo2_url = "";
exports.webName = "AST - Voucher";

exports.ADMIN_ACCESS      = [0];
exports.OWNER_ACCESS      = [1];
exports.CUSTOMER_ACCESS   = [1, 2, 3];
exports.EMPLOYEE_ACCESS   = [1, 2];

// ----------------KHÁNH: bổ sung response type -> web browser/mobile app ---------------//
exports.BROWSER_ACCESS = 1;
exports.MOBILE_ACCESS  = 2;

exports.TYPE_RESPONSE = {
    NOT_PROVIDE_TOKEN: 1,
    TOKEN_INVALID: 2,
    PERMISSION_DENIED: 3,
}
