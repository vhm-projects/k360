"use strict";

const ObjectID  = require('mongoose').Types.ObjectId;
const XLSX      = require('xlsx');
const lodash    = require('lodash');
const crypto    = require('crypto');
const moment = require('moment');

const algorithm = 'aes-256-ctr';
const secretKey = process.env.ENCRYPT_SECRET_KEY;
const iv        = crypto.randomBytes(16);

const {addNumberToSetDate, subStractDateGetDay} = require("../config/cf_time");

exports.isEmpty = function (value) {
    return typeof value == 'string'
        && !value.trim()
        || typeof value == 'undefined'
        || value === null
        || value == undefined;
};

exports.isEmptyObject = function (obj) {
    for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            return false;
        }
    }
    return true;
};

/**
 * kiểm tra mảng array có tồn tại giá trị hay không?
 * @param params
 * @returns {boolean}
 */
exports.checkParamsValidate = function (params) {
    var isParamIsValidate = true;
    params.forEach(function (item, index) {
        if (item == null || item.trim().length == 0) {
            isParamIsValidate = false;
        }
    });

    return isParamIsValidate;
};

exports.currencyFormat = function (number, fixLength) {
    if (fixLength == null) {
        let stringNum = number + '';
        let arrInc = stringNum.split('.');
        let fixNum = 0;
        if (arrInc.length == 2) {
            fixNum = arrInc[1].length;
        }

        fixNum = fixNum > 18 ? 18 : fixNum;

        return (Number(number)).toLocaleString('en-US', { minimumFractionDigits: fixNum });
    } else {
        return (Number(number)).toLocaleString('en-US', { minimumFractionDigits: fixLength });
    }
};

// Hàm làm tròn tiền tệ
exports.roundingNumber = function (num, dp, isCurrency) {
    if (arguments.length < 2) throw new Error("2 arguments required");
    num = `${num}`;
    if(!num.includes('.')){
        num = Number(num)/1000;
    }
    num = `${num}`;
    if (num.indexOf('e+') != -1) {
        // Can't round numbers this large because their string representation
        // contains an exponent, like 9.99e+37
        throw new Error("num too large");
    }
    if (num.indexOf('.') == -1) {
        // Nothing to do
        if(isCurrency){
            return  Number(num)*1000
        }
        return Number(num);
    }
 
    var parts = num.split('.'),
        beforePoint = parts[0],
        afterPoint = parts[1],
        shouldRoundUp = afterPoint[dp] >= 5,
        finalNumber;

    if(dp >= `${afterPoint}`.length){
        dp = afterPoint.length - 1;
        console.log({__ : afterPoint[dp]})
        shouldRoundUp = afterPoint[dp] >= 5
    }
    if(dp == 0){
        afterPoint = afterPoint[0];
        finalNumber =  Number(beforePoint);
        if(Number(afterPoint)>=5){
            finalNumber =  finalNumber + 1;
            if(isCurrency){
                return  Number(finalNumber)*1000
            }
            return finalNumber;
        }else{
            if(isCurrency){
                return  Number(finalNumber)*1000
            }
            return finalNumber;
        }
    }else{
        afterPoint = afterPoint.slice(0, dp);
        if (!shouldRoundUp) {
            finalNumber = beforePoint + '.' + afterPoint;
        } else if (/^9+$/.test(afterPoint)) {
            // If we need to round up a number like 1.9999, increment the integer
            // before the decimal point and discard the fractional part.
            finalNumber = Number(beforePoint)+1;
        } else {
            // Starting from the last digit, increment digits until we find one
            // that is not 9, then stop
            var i = dp-1;
            while (true) {
                if (afterPoint[i] == '9') {
                    afterPoint = afterPoint.substr(0, i) +
                                 '0' +
                                 afterPoint.substr(i+1);
                    i--;
                } else {
                    afterPoint = afterPoint.substr(0, i) +
                                 (Number(afterPoint[i]) + 1) +
                                 afterPoint.substr(i+1);
                    break;
                }
            }
     
            finalNumber = beforePoint + '.' + afterPoint;
        }

        if(isCurrency){
            return Number(finalNumber.replace(/0+$/, ''))*1000
        }
      
        // Remove trailing zeroes from fractional part before returning
        return Number(finalNumber.replace(/0+$/, ''));
    }
    
}

/**
 * tính khoảng cách giữ 2
 * @param lat1
 * @param long1
 * @param lat2
 * @param long2
 * @returns {number}
 */
exports.getDistanceFromLatLonInKm = function (lat1, long1, lat2, long2) {
    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(long2 - long1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
};

exports.randomIntBetween = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};

exports.randomIntFromInterval = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};

function change_alias(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str;
}
/**
 * Hàm chuyển đổi từ name sang slug
 * @param {*} plainText
 * @returns {string} 
 */
exports.convertToSlug = function (plainText) {
    const text_converted_alias = change_alias(plainText);
    const text_split_with_space = text_converted_alias.split(' ');
    const text_joined = text_split_with_space.join('-');
    return text_joined;
}

exports.filterObject = (obj, filter, filterValue) =>
    Object.keys(obj).reduce((acc, val) =>
        (obj[val][filter] === filterValue ? {
            ...acc,
            [val]: obj[val]
        } : acc
        ), {});


exports.checkPhoneNumber = (phone) => {
    var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    if (phone !== '') {
        if (vnf_regex.test(phone) == false)
            return false;
        return true;
    } else {
        return false;
    }
}

/**
 * param: email
 * return bolean
 */
exports.checkEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

/**
 * param: price
 * return number
 */
exports.checkNumber = price => {
    var re = /^[0-9]*$/;
    return re.test(price);
}

/* 
 * Hàm validation các kí tự thực thi script 
 * param: data
 * return boolean
*/
exports.isValidationData = data => {
    const denineString = ['<', '>', '\'', '\"', '&', '\\', '\\\\'];
    const dataAfterSplited = data.split('');
    let temp = 0;
    while (temp < dataAfterSplited.length) {
        if (dataAfterSplited.includes(denineString[temp])) {
            return false;
        }
        temp++;
    }
    return true;
}


let formatCurrentcy = x => {
    x = x.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
    return x;
};

exports.formatCurrentcy = formatCurrentcy;
 
exports.checkRangeValid = (arr, val) => {
    if (!Array.isArray(arr) || !arr.includes(val))
        return false;
    return true;
}

// Read data from excel file (first sheet in file)
exports.readFileExcel = (pathFileInternal) => {
    let wb      = XLSX.readFile(pathFileInternal);
    let data    = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]], { header: 1 });
    return data;
}

// Đọc dữ liệu từ sheet 2 để import data
exports.readFileExcel1 = (pathFileInternal) => {
    let wb      = XLSX.readFile(pathFileInternal);
    let data    = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[1]], { header: 1 });
    return data;
}

let _isValid = input => {
    if (!input)
        return false;
    let inputForCheck = input.toString();
    if (ObjectID.isValid(input.toString())) {
        if (String(new ObjectID(input)) === inputForCheck) 
            return true;
        return false;
    }
    return false;
}

// Khánh sẽ xem xét lại: bị lỗi khi check với req.query
exports.checkObjectIDs = (...params) => {
    let flag = true;
    let arrParams = lodash.flattenDeep(params);
    for(let i = 0; i < arrParams.length; i++) {
        if (!_isValid(arrParams[i]))
            return flag = false
    }
    return flag;
}

/**
 * Tính số ngày giữa 2 ngày
 * Author: DEPV
*/
exports.numberOfNightsBetweenDates = (startDate, endDate) => {
    const start = new Date(startDate) //clone
    const end = new Date(endDate) //clone
    let dayCount = 0
  
    while (end > start) {
      dayCount++
      start.setDate(start.getDate() + 1)
    }
  
    return dayCount
}

exports.replaceExist = (arr, userID, socketID, username) => {
    let newArr;
    let isExist = arr.find(item => Object.is(item.username, username)); // kiểm tra tồn tại

    if (isExist) {
        let arrWithoutExist = arr.filter(item => !Object.is(item.username, username));
        newArr = [...arrWithoutExist, 
            { 
                userID, 
                socketID: [...isExist.socketID, socketID], 
                username }
        ];
    } else {
        newArr = [...arr, 
            {
                userID,
                socketID: [socketID],
                username
            }
        ]
    }
    return newArr;
};

exports.encrypt = text => {
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

    return {
        iv: iv.toString('hex'),
        content: encrypted.toString('hex')
    };
}

exports.decrypt = hash => {
    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'));
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);

    return decrpyted.toString();
}

//func tính chu kỳ lãi
/**  
 * VD: input 75 (với chu kỳ là 30 ngày)
 *  -> 2 chu kỳ         (#1)
 *  -> 15 ngày lẽ       (#2)
 * @param periodPerDays     1 chu kỳ bao nhiều ngày
 * @param daysForCalculate  số ngày tính lãi
 * @returns { period, days }
 *  period: số chu kỳ (#1)
 *  days: số ngãy lẽ (#2)
 */
 exports.calculateInteres = ({ periodPerDays = 30, daysForCalculate }) => {
    let period = Math.floor(daysForCalculate / periodPerDays);
    let days   = daysForCalculate % periodPerDays;

    return {
        period, days
    }
};

// exports.calculateInteres = calculateInteres;
//func lấy số tiền quỹ
/**  
 * @param statusFinancial     LOẠI QUỸ 
 *      => 1: QUỸ TIỀN MẶT
 *      => 2: QUỸ NGÂN HÀNG
 * @param infoDataAgency  Thông tin đại lý
 * @returns { funds }
 */
exports.getFundsAgency = ({ statusFinancial, infoDataAgency }) => {
    const TYPE_MONEY = 1;
    if (statusFinancial == TYPE_MONEY) {
        return infoDataAgency.funds;
    } else {
        return infoDataAgency.fundsBank;
    }
}

//func Số ngày của chu kỳ
/**  
 * @param period
 * @returns { funds }
 */
exports.calculateDateOfPeriod = ({ period }) => {
    return Number(period) * 30;
}

//func Tính số ngày tính lãi vs ngày đến lãi dựa vào CHU KÌ
/**  
 * Cách tính:
 *      1: Lấy ngày hiện tại + Số ngày của chu kỳ (dateOfPeriod);
 *      2: Cộng thêm 30 ngày nữa để ra ngày đến hạn (dateExpriseDate)
 * @param dateOfPeriod
 * @returns { dateInterestDate, dateExpriseDate }
 */
 exports.calculateInterestAndExpireDate = ({ dateOfPeriod, date1, date2 }) => {
    // let dateInterestDate = new Date();
    // let dateExpriseDate  = new Date();

    addNumberToSetDate({ date1: date1, numberToSet: dateOfPeriod });
    
    let DATE_OF_MONTH = 30;
    addNumberToSetDate({ date1: date2, numberToSet: dateOfPeriod + DATE_OF_MONTH });
    return { dateInterestDate: date1, dateExpriseDate: date2 };
}

//func TÍNH TOÁN 2 SỐ TIỀN
/**  
 * Cách tính:
 *      1: Lấy ngày hiện tại + Số ngày của chu kỳ (dateOfPeriod);
 *      2: Cộng thêm 30 ngày nữa để ra ngày đến hạn (dateExpriseDate)
 * @param price1, 
 * @param price2,
 * @returns { price1, price2 }
 */
 exports.calculateBetweentTwoPrice = ({ price1, price2 }) => {
    
    if(price1 < price2) {
        price2         =  price2 - price1;
        price1 = 0;
    }else

    // 2// Nếu số tiền phải trả lãi > số tiền khuyến mãi
    if(price1 > price2) {
        price1 = price1 - price2
        price2         = 0;
    }else

    // 3// Nếu số tiền phải trả  = số tiền khuyến mãi
    if(price1 == price2) {
        price1 = 0;
        price2 = 0;
    }
    return {price1, price2};
}

//func TÍNH TOÁN 2 SỐ TIỀN
/**  
 * Cách tính:
 *      1: Lấy ngày hiện tại + Số ngày của chu kỳ (dateOfPeriod);
 *      2: Cộng thêm 30 ngày nữa để ra ngày đến hạn (dateExpriseDate)
 * @param price1, 
 * @param price2,
 * @returns { price1, price2 }
 */
 exports.checkLimitDate = ({ interestStartDate, type }) => {
    let dateNow   =  new Date();
    interestStartDate          = moment(interestStartDate).startOf('day').format();

    let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
    // limitDate     = Math.floor(limitDate);
    /**
     * 1: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại > 0
     *    => limitDate + 1, ngày tính lãi bắt đầu 1
     * 2: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại < 0
     *    => Đã trả lãi trước
     *    => limitDate - 1, ngày tính lãi - 1
     */
    if(limitDate >= 0) {
        limitDate     = Math.floor(limitDate) + 1;
    }
    /**
     * 1: Với type = 2 và  limitDate < 0 => GIAO ĐỊCH ĐÃ TRẢ TRƯỚC VÀ KHI TẠO THÊM GIAO DỊCH BÌNH THƯỜNG => KHÔNG TÍNH LÃI
     * 2: Các type còn lại trừ vay thêm, báo mất => Tính lãi
     */
    if(limitDate < 0 && type == 2) {
        limitDate     = 0;
    }else if(limitDate < 0) {
        limitDate = 1;
    }
    /**
     * Kiểm tra xem đã tính lãi hay chưa
     * 1// Tính rồi thì phải chọn GD Bình Thường trước
     * 2// Chưa tính thì được thêm GD khác
     */
    const TYPE_NORMAL = 2;
    if(limitDate > 1 && type != TYPE_NORMAL){
        return { error: true, message: "cannot_create_inject_transaction_before_pay_interest" }
    }else{
        return { error: false, message: "can_create_inject" }
    }
}


/**  
 * @param meta, meta -> GIAO DỊCH BÌNH THƯỜNG
 * @param expireTimeAfterPeriod, Ngày chốt lãi ở chu kỳ tiếp theo
 * @returns { NUMBER_DATE_HAVE_PERIOD } -> SỐ NGÀY TÍNH LÃI
 */
 exports.getNumberDateHavePeriodLate = (meta, expireTimeAfterPeriod) => {
    const DAY_OF_MONTH       = 30;
    let NUMBER_DATE_HAVE_PERIOD;
    /**
     * ĐÓNG TRÒN KÌ
     * EXAMPLE:
     *      Khách hàng trễ 32 ngày -> Chỉ đóng 30 ngày -> Trả lãi trước
     */
     let period = meta.cycleInterest;
     if (meta && meta.typePayLate == 'full_period') {
        NUMBER_DATE_HAVE_PERIOD = DAY_OF_MONTH * Number(period);
        titleTransaction = 'GIAO DỊCH BÌNH THƯỜNG TRẢ TRƯỚC ĐẦU TIÊN';
    }

    /**
     * ĐÓNG KHÔNG TRÒN KÌ VÀ NGÀY CHỐT LÃI KHÔNG PHẢI NGÀY HIỆN TẠI
     */
    if (meta && meta.typePayLate == 'late_period_date_not_now') {
        /**
         * LẤY RA SỐ NGÀY ĐÓNG TRỄ
        */
        let numberDateBetweenDate = subStractDateGetDay({date1: expireTimeAfterPeriod, date2: new Date() });

        /**                                                                |---------------------| 
         *  EXAMPLE:                                                       |(2/9)                |   
         *      |------------------------------------|-------------|-------|------|              |
         *     1/8                                 31/8           1/9            3/9             |
         *       ----------------30 ngày--------------(trễ 1 ngày)---------(Nhận được tiền) -> Dời 1 ngày (2/9)
         * 
         *      => NUMBER_DATE_HAVE_PERIOD = 1 ngày trễ (31/8 -> 1/9) + 1 Ngày (Dời) + 30 NGÀY (1 Chu kỳ) 
         *                                   (numberDateBetweenDate)  + (DATE_MOVING) + (DAY_OF_MONTH)
         */
        const DATE_MOVING = 1;
        numberDateBetweenDate     = Math.floor(Math.abs(numberDateBetweenDate));
        console.log({ numberDateBetweenDate });
        // NUMBER_DATE_HAVE_PERIOD   = numberDateBetweenDate + DAY_OF_MONTH + DATE_MOVING;
        NUMBER_DATE_HAVE_PERIOD = numberDateBetweenDate + DATE_MOVING;
        if (meta.cycleInterest) {
            NUMBER_DATE_HAVE_PERIOD += DAY_OF_MONTH * Number(period);
        }
        console.log({ NUMBER_DATE_HAVE_PERIOD });
    }
    return NUMBER_DATE_HAVE_PERIOD
}



