"use strict";

var DATABASE = require('./db_connect');
var Schema = require('mongoose').Schema;
var random = require('mongoose-simple-random');

module.exports = function (dbName, dbOb, ops) {
    try {
        dbOb.createAt = Date;
        dbOb.modifyAt = Date;
        let s = new Schema(dbOb);

        if (ops && ops.isRecursive) {
            s.pre('findOne', function(next) {
                if(ops.pathsChild){
                    this.populate({
                        path: ops.paths.join(' '),
                        populate: ops.pathsChild.join(' ')
                    })
                } else{
                    this.populate(ops.paths.join(' '));
                }
                return next();
            })
        }

        s.plugin(random);

        return DATABASE.model(dbName, s);
    } catch (error) {
        return DATABASE.model(dbName);
    }
};