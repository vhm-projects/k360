exports.CSS_ADD_MAIN_TRANSACTION_REQUEST            = 'CSS_ADD_MAIN_TRANSACTION_REQUEST';
exports.SSC_ADD_MAIN_TRANSACTION_REQUEST            = 'SSC_ADD_MAIN_TRANSACTION_REQUEST';

exports.CSS_CONFIRM_TRANSACTION_REQUEST             = 'CSS_CONFIRM_TRANSACTION_REQUEST';
exports.SSC_CONFIRM_TRANSACTION_REQUEST             = 'SSC_CONFIRM_TRANSACTION_REQUEST';

exports.CSS_PRICING_REQUEST                         = 'CSS_PRICING_REQUEST';
exports.SSC_PRICING_REQUEST                         = 'SSC_PRICING_REQUEST';

exports.CSS_SEND_NOTIFICATION                       = 'CSS_SEND_NOTIFICATION';
exports.SSC_SEND_NOTIFICATION                       = 'SSC_SEND_NOTIFICATION';

exports.CSS_ADD_INJECT_TRANSACTION_RANSOM_REQUEST   = 'CSS_ADD_INJECT_TRANSACTION_RANSOM_REQUEST';
exports.SSC_ADD_INJECT_TRANSACTION_RANSOM_REQUEST   = 'SSC_ADD_INJECT_TRANSACTION_RANSOM_REQUEST';


exports.CSS_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST   = 'CSS_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST';
exports.SSC_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST   = 'SSC_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST';
