"use strict";

let jwt                             = require('jsonwebtoken');
let redis                           = require('redis');
let redisAdapter                    = require('socket.io-redis');
let { REDIS_SEPERATOR }             = require('../config/cf_redis');
const { ADMIN_DOMAIN } 	= require('../config/cf_domain');      

// tạo GlobalStore -> lưu trữ danh sách user đang online (nhận được socketIO)
let usersConnectedInstance          = require('../config/cf_globalscope').GlobalStore;
let usersConnected                  = usersConnectedInstance.usersOnline;
let ObjectID                        = require('mongoose').Types.ObjectId;
// const ioClient                      = require("socket.io-client");


let {
    CSS_PRICING_REQUEST,
    SSC_PRICING_REQUEST,

    CSS_ADD_MAIN_TRANSACTION_REQUEST,
    SSC_ADD_MAIN_TRANSACTION_REQUEST,

    CSS_SEND_NOTIFICATION,
    SSC_SEND_NOTIFICATION,

    CSS_CONFIRM_TRANSACTION_REQUEST,
    SSC_CONFIRM_TRANSACTION_REQUEST,

    CSS_ADD_INJECT_TRANSACTION_RANSOM_REQUEST,
    SSC_ADD_INJECT_TRANSACTION_RANSOM_REQUEST,

    CSS_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST,
    SSC_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST
} = require('./constants');

/**
 * UTILS
 */
let { replaceExist, decrypt }   			= require('../utils/utils');
let { mapSocketIDAndData, sendToClient, getUserNotConnected, socketClient }    = require('../utils/socket_utils');
// let { mapSocketIDAndData, sendToClient }    = require('../utils/socket_utils');
let { sendMessageMobile }    = require('../fcm/utils');
let { SCREEN_KEY }           = require("../config/cf_constants")

/**
 * MODELS, COLLECTIONS
 */
let { AGENCY_COLL }           = require('../packages/agency');
let NOTIFICATION_COLL         = require('../packages/notification/databases/notification-coll')({});
let { NOTIFICATION_MODEL }    = require('../packages/notification');
let { USER_MODEL }            = require('../packages/permissions/users');
let CUSTOMER_MODEL            = require('../packages/customer/models/customer').MODEL;


module.exports = function (io) {
    let pub = redis.createClient({
        // detect_buffers: true,
        // return_buffers: true,

        host: REDIS_SEPERATOR.HOST,
        port: REDIS_SEPERATOR.PORT,
        auth_pass: REDIS_SEPERATOR.PWD
    });

    let sub = redis.createClient({
        // detect_buffers: true,
        // return_buffers: true,

        host: REDIS_SEPERATOR.HOST,
        port: REDIS_SEPERATOR.PORT,
        auth_pass: REDIS_SEPERATOR.PWD
    });
    // let sub = CLIENT_REDIS;
    // let pub = CLIENT_REDIS;

    io.adapter(redisAdapter({
        pubClient: pub,
        subClient: sub
    }));

    io
    .set('transports', ['websocket']) //config
    .use(async (socket, next) => {
        // console.log({ __handshake: socket.handshake });
        socket.isAuth    = false;
        
        if (socket.handshake.query && socket.handshake.query.token) {
            let signalVerifyToken = await USER_MODEL.checkAuth({ token: socket.handshake.query.token });
            if (signalVerifyToken.error) {
                console.log({ [`signalVerifyToken.error`]: signalVerifyToken.error })
                return next(new Error('Authentication error'));
            }
            let { data: infoUser } = signalVerifyToken;

            socket.decoded   = infoUser;
            socket.isAuth    = true;
            socket.tokenFake = false;
            /**
             * ADD USER INTO usersConnected
             * usersConnected
             */
            let { id: socketID } = socket;
            let { _id: userID, username }  = infoUser;
            socket.userID = userID;
            
            let usersConnectedGlobal = usersConnectedInstance.usersOnline;
            // console.log({
            //     [`danh sách TRƯỚC KHI user connected vào socket`]: usersConnectedGlobal
            // })

            // console.log({
            //     [`thông tin trước khi connect`]: usersConnectedGlobal,
            //     [`số lượng`]: usersConnectedGlobal.length
            // })

            usersConnected = replaceExist(usersConnectedGlobal, userID, socketID, username);

            // console.log({
            //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
            // })
            // console.log({
            //     [`thông tin trước khi connect`]: usersConnected,
            //     [`số lượng`]: usersConnected.length,
            //     [`Truy cập mới`]: {
            //         userID, socketID, username
            //     }
            // })
            
            // console.log({
            //     [`danh sách SAU KHI user not connected vào socket`]: listUserNotConnected
            // });

            usersConnectedInstance.setUsersOnline(usersConnected);
            next();
        }
        if(socket.handshake.query && socket.handshake.query._token){ // customer_fake, customer auth
            try {
                let decoded = await jwt.verify(socket.handshake.query._token, process.env.JWT_SECRET_KEY);
                if (decoded && decoded.hashed) { // đang dùng token FAKE (user)
                    // console.log({ decoded })
                    let content = decrypt(decoded.hashed);
                    if(content !== process.env.ENCRYPT_SECRET_KEY) { 
                        console.log({ 
                            content,
                            [`process.env.ENCRYPT_SECRET_KEY`]: process.env.ENCRYPT_SECRET_KEY
                        })
                        next(new Error('Authentication error, catch workspace'));
                    } else {
                        console.log(`---------------2-----------------`)

                        socket.decoded      = decoded;
                        socket.tokenFake    = true;
                        socket.isAuth       = true;
                    }
                } else { // dùng token user (mobile có login)
                    let signalVerifyToken = await CUSTOMER_MODEL.checkAuth({ token: socket.handshake.query._token });
                    if (signalVerifyToken.error) {
                        console.log({ [`signalVerifyToken.error`]: signalVerifyToken.error })
                        return next(new Error('Authentication error'));
                    }
                    let { data: infoUser  } = signalVerifyToken;
                    socket.decoded   = infoUser;
                    socket.isAuth    = true;
                    socket.tokenFake = false;
                    /**
                     * ADD USER INTO usersConnected
                     * usersConnected
                     */
                    let { id: socketID } = socket;
                    let { _id: userID, phone }  = infoUser;
                    
                    let usersConnectedGlobal = usersConnectedInstance.usersOnline
                    // console.log({
                    //     [`danh sách TRƯỚC KHI user connected vào socket`]: usersConnectedGlobal
                    // })

                    // console.log({
                    //     [`thông tin trước khi connect`]: usersConnectedGlobal,
                    //     [`số lượng`]:  usersConnectedGlobal && usersConnectedGlobal.length
                    // })
        
                    // usersConnected = replaceExist(usersConnectedGlobal, userID, socketID, phone); //TODO cần bổ sung thêm trường hợp gửi cho ENDUSER
        
                    // console.log({
                    //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
                    // })
                    // console.log({
                    //     [`thông tin trước khi connect`]: usersConnectedGlobal,
                    //     [`số lượng`]: usersConnectedGlobal && usersConnectedGlobal.length,
                    //     [`Truy cập mới`]: {
                    //         userID, socketID, phone
                    //     }
                    // })
        
                    // console.log({
                    //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
                    // })
        
                    usersConnectedInstance.setUsersOnline(usersConnected);
                }

                return next();
            } catch (error) {
                console.log({ error })
                next(new Error('Authentication error, catch workspace'));
            }
        }
    })
    .on('connection', function (socket) {
        // console.log(`------------------connection------------`)
        // const { id: socketID, decoded, isAuth } = socket;
        // const { id: socketID, userID: currentUserID } = socket;
        const { id: socketID, decoded, isAuth, tokenFake } = socket;
        // console.log({ decoded, tokenFake });
        
        if (socket.isAuth) {
            socket.on(CSS_PRICING_REQUEST, async data => {
                let { agencyID, fullname, requestID } = data;
                if(agencyID){
                    let infoAgency = await AGENCY_COLL
                        .findById(agencyID)
                        .select('owners employees')
                        .lean();

                    let title     = 'YÊU CẦU ĐỊNH GIÁ ONLINE';
                    let content   = `${fullname} đã gửi 1 yêu cầu định giá`;
                    let link	  = `/pricing-request/list-pricing-request?request=${requestID}`;

                    let dataInsert = {
                        title,
                        description: content,
                        url: link,
                        type: 1,
                        agency: agencyID
                    }
                    /**
                     * FIELD NẾU ĐĂNG NHẬP CUSTOMER BẰNG APP
                     */
                    if (!tokenFake && !decoded.level) {
                        dataInsert = {
                            ...dataInsert,
                            customer: decoded._id
                        }
                    }

                    let arrReceiver = [];
                    if(infoAgency.owners && infoAgency.owners.length){
                        arrReceiver = infoAgency.owners;
                    }
                    if(infoAgency.employees && infoAgency.employees.length){
                        arrReceiver = [...arrReceiver, ...infoAgency.employees] ;
                    }
                    console.log({ arrReceiver });
                    arrReceiver.map(async receiver => {
                        const notify = await NOTIFICATION_MODEL.insert({
                            ...dataInsert, receiver
                        });

                        // Send notify to client
                        sendToClient(receiver, SSC_PRICING_REQUEST, {
                            content, title, link, notify: notify.data
                        }, usersConnected, io);
                    })
                }
            })

            socket.on(CSS_SEND_NOTIFICATION, async data => {
                let titleDefault = `Hệ thống đã gửi cho bạn thông báo mới`;
                let { notificationCoreID } = data;
                let userReceiveNoti = [];
                let notificationAfterInsert = [];
                let TYPE_NOTIFICATION_SYSTEM_SEND_AGENCY = 13;

                //Check các owner trùng nhau do nằm nhiều đại lý
                function unique(userReceiveNoti) {
                    var newArr = []
                    newArr = userReceiveNoti.filter(function (item) {
                        return newArr.includes(item.toString()) ? '' : newArr.push(item.toString())
                    });
                    return newArr
                }

                let infoNotificationCore = await NOTIFICATION_COLL.findById(notificationCoreID);
                let { agencys_receive: arrayAgencyReceive, sender, title, description } = infoNotificationCore;

                for (const agency of arrayAgencyReceive) {
                    let infoAgency = await AGENCY_COLL.findById(agency);
                    let { owners } = infoAgency;
                    //userReceiveNoti = userReceiveNoti.concat(owners);
                    userReceiveNoti = [...userReceiveNoti, {agencyID: agency, owners}]
                }
                //let resultUserReceiveNoti = unique(userReceiveNoti);
                
                for (const item of userReceiveNoti) {
                    for (const userID of item.owners) {
                        let notificationInsert = await NOTIFICATION_MODEL.insert({
                            notificationCore: notificationCoreID,
                            receiver: userID,
                            agency: item.agencyID,
                            type: TYPE_NOTIFICATION_SYSTEM_SEND_AGENCY,
                            sender,
                            title,
                            description
                            //url: `/notification-core/${systemNotification}`
                        });
                        let { _id: notificationLoopID } = notificationInsert.data;
                        let url = `/notification/notification-core/${notificationLoopID}`;
    
                        notificationInsert = await NOTIFICATION_COLL.findByIdAndUpdate(notificationLoopID, { url }, {new: true})
                        notificationAfterInsert = [...notificationAfterInsert, notificationInsert];
                    }
                }
                let resultUserReceiveNoti = [];
                for (const item of userReceiveNoti) {
                    resultUserReceiveNoti = [...resultUserReceiveNoti, ...item.owners]
                }
                resultUserReceiveNoti = unique(resultUserReceiveNoti)

                // console.log({resultUserReceiveNoti, userReceiveNoti, titleDefault, title, notificationAfterInsert });

                mapSocketIDAndData(resultUserReceiveNoti, SSC_SEND_NOTIFICATION, {
                    title: titleDefault, content: title, notificationAfterInsert
                }, usersConnected, io);
                
                /**
                    let messageNoti = `HELLO WORLD - DESCRIPTION`;
                 *  let bodyCloudMSG = {
                        screen_key: `${....}`,
                        metadata...
                    }
                 */
                // sendMessageMobile(arrReceiver, messageNoti, bodyCloudMSG, userID) 
            })

            socket.on(CSS_ADD_MAIN_TRANSACTION_REQUEST, async data => {
                let { agencyID, userID, userName, transactionID } = data;

                if(agencyID){
                    let infoAgency = await AGENCY_COLL
                        .findById(agencyID)
                        .select('owners name')
                        .lean();

                    if(infoAgency.owners){
                        let title     = 'YÊU CẦU DUYỆT GIAO DỊCH';
                        let content   = `${userName} đã gửi 1 yêu cầu duyệt giao dịch`;
						let link	  = `/transaction/info-transaction/${transactionID}`;
                                                
                        let listOwnerIgnoreSender = infoAgency.owners.filter(member => `${member}` != `${userID}`);
                        let arrNotifiIDOfMember = [];
                        // console.log("======================================= YÊU CẦU TẠO GIAO DỊCH");
                        let listNotiMapInfo = listOwnerIgnoreSender.map(member => {
                            return NOTIFICATION_MODEL.insertV2({
                                title,
                                description: content,
                                sender: userID,
                                receiver: member,
                                type: 2,
                                arrayAgencyReceive: [agencyID],
                                url: link,
                                agency: agencyID
                            });
                        })
                        
                        let listNotiAfterInsert = await Promise.all(listNotiMapInfo);
                        // 3/ GỬI socket tới tất cả recivers Online
                        listNotiAfterInsert.map(infoNoti => {
                            arrNotifiIDOfMember[arrNotifiIDOfMember.length] = {
                                notifiID: infoNoti.data._id,
                                member:   infoNoti.data.receiver,
                                agency:   infoNoti.data.agency
                            }
                        });
                        
                       
                        // console.log("============================ BEFORE SEND SOCKET");
                        // console.log({ arrNotifiIDOfMember });
                        mapSocketIDAndData(listOwnerIgnoreSender, SSC_ADD_MAIN_TRANSACTION_REQUEST, {
                            title, content, arrNotifiIDOfMember, transactionID
                        }, usersConnected, io);
                        // // console.log({ __owwner: infoAgency.owners, infoNotification });
                        // console.log("============================ AFTER SEND SOCKET");
                        // /**
                        //  * Lấy danh sách user not connected
                        //  */
                        // console.log({
                        //     usersConnected, listOwnerIgnoreSender
                        // });
                        let listUserNotConnected = getUserNotConnected(usersConnected, listOwnerIgnoreSender);
                        // console.log({
                        //     listUserNotConnected
                        // });
                        let body = {
                            screen_key: SCREEN_KEY.DetailDealScreen.text,
                            sender: userID,
                            transactionID: transactionID,
                            agencyID: agencyID,
                        };
                        // console.log({
                        //     body
                        // });
                        
                        sendMessageMobile({ 
                            title: title , 
                            description: " (Đại lý " + infoAgency.name + ") " + content,
                            arrReceiverID: listUserNotConnected, 
                            senderID: userID, 
                            body 
                        });
                    }
                }
            })

            socket.on(CSS_CONFIRM_TRANSACTION_REQUEST, async data => {
                let { agencyID, userID, userName, type, transactionID } = data;
                
                if(agencyID){
                    let infoAgency = await AGENCY_COLL
                        .findById(agencyID)
                        .select('owners employees name')
                        .lean();
                    let title       = type == 2 ? "DUYỆT GIAO DỊCH" : "KHÔNG DUYỆT GIAO DỊCH";
                    let string      = type == 2 ? "duyệt giao dịch" : "không duyệt giao dịch";
                    let content   = `${userName} đã ` + string;
                    let link	  = `/transaction/info-transaction/${transactionID}`;

                    // let listNotiOfEmployee = infoAgency.employees.map(async employeeID => {
                    //     return NOTIFICATION_MODEL.insert({
                    //         title,
                    //         description: content,
                    //         receiver: employeeID,
                    //         type: type
                    //     })
                    // });
                    // 1/ lấy tất cả owner trừ thằng gửi(nếu sender là owner)
                    let listUserSendNotifi = [];
                    // let listOwnerIgnoreSender = infoAgency.owners.filter(member => `${member}` != `${userID}`);
                    listUserSendNotifi = infoAgency.owners.concat(infoAgency.employees);
                    // 2/ insert noti cho tất cả recievers
                    let arrNotifiIDOfMember = [];
                    
                    let listNotiMapInfo = listUserSendNotifi.map(member => {
                        if ( member.toString() != userID.toString() ){
                            return NOTIFICATION_MODEL.insertV2({
                                title,
                                description: content,
                                sender: userID,
                                receiver: member,
                                type: type,
                                arrayAgencyReceive: [agencyID],
                                url: link,
                                agency: agencyID
                            });
                        }   
                    });

                    let listNotiAfterInsert = await Promise.all(listNotiMapInfo);
                    // 3/ GỬI socket tới tất cả recivers Online
                    listNotiAfterInsert = listNotiAfterInsert.filter(infoNoti => (
                        infoNoti && infoNoti.data
                    ));
                   
                    // const socketNew = socketClient(ioClient, ADMIN_DOMAIN, socket.handshake.query.token);
                    // console.log({
                    //     socketNew
                    // });
                    // socketNew.on('connect', function (socketClients) { 
                    //     console.log({
                    //         socketClients
                    //     });
                    //     socketNew.emit('CSS_CONFIRM_TRANSACTION_REQUEST_ENDUSER', { agencyID, customerID, content, title })
                    // })
                    // socketNew.disconnect();
                    
                    mapSocketIDAndData(listUserSendNotifi, SSC_CONFIRM_TRANSACTION_REQUEST, {
                        title, content, listNotiAfterInsert, userSend: userID, transactionID
                    }, usersConnected, io);

                    /**
                     * Lấy danh sách user not connected
                     */
                    // console.log({
                    //     usersConnected, listUserSendNotifi
                    // });
                    let listUserNotConnected = getUserNotConnected(usersConnected, listUserSendNotifi);
                    // console.log({
                    //     listUserNotConnected
                    // });
                    let body = {
                        screen_key: SCREEN_KEY.DetailDealScreen.text,
                        sender: userID,
                        transactionID: transactionID,
                        agencyID: agencyID,
                    };
                    // console.log({
                    //     body
                    // });
                    
                    sendMessageMobile({ 
                        title: title, 
                        description: "(Đại lý " + infoAgency.name + ") " + content,
                        arrReceiverID: listUserNotConnected, 
                        senderID: userID, 
                        body 
                    });
                }
            })

            socket.on(CSS_ADD_INJECT_TRANSACTION_RANSOM_REQUEST, async data => {
                let { agencyID, userID, userName, transactionID } = data;

                let infoAgency = await AGENCY_COLL
                    .findById(agencyID)
                    .select('owners name')
                    .lean();

                let title       = 'YÊU CẦU DUYỆT CHUỘC ĐỒ';
                let content     = `${userName} đã gửi 1 yêu cầu duyệt giao dịch chuộc đồ`;
                let link	    = `/transaction/info-transaction/${transactionID}`;

                // 1/ lấy tất cả owner trừ thằng gửi(nếu sender là owner)
                let listOwnerIgnoreSender = infoAgency.owners.filter(member => `${member}` != `${userID}`);

                // 2/ insert noti cho tất cả recievers
                let arrNotifiIDOfMember = [];
                let listNotiMapInfo = listOwnerIgnoreSender.map(member => {
                    return NOTIFICATION_MODEL.insertV2({
                        title,
                        description: content,
                        sender: userID,
                        receiver: member,
                        type: 5,
                        arrayAgencyReceive: [agencyID],
                        url: link,
                        agency: agencyID
                    });
                })
                let listNotiAfterInsert = await Promise.all(listNotiMapInfo);
                // 3/ GỬI socket tới tất cả recivers Online
                listNotiAfterInsert = listNotiAfterInsert.filter(infoNoti => (
                    infoNoti && infoNoti.data
                ));
                // console.log({ listNotiAfterInsert });
                mapSocketIDAndData(listOwnerIgnoreSender, SSC_ADD_INJECT_TRANSACTION_RANSOM_REQUEST, {
                    title, content, listNotiAfterInsert, userSend: userID, transactionID
                }, usersConnected, io);
                    
                /**
                 * Lấy danh sách user not connected
                 */
                // console.log({
                //     usersConnected, listOwnerIgnoreSender
                // });
                let listUserNotConnected = getUserNotConnected(usersConnected, listOwnerIgnoreSender);
                // console.log({
                //     listUserNotConnected
                // });
                let body = {
                    screen_key: SCREEN_KEY.DetailDealScreen.text,
                    sender: userID,
                    transactionID: transactionID,
                    agencyID: agencyID,
                };
                // console.log({
                //     body
                // });
                
                sendMessageMobile({ 
                    title: title, 
                    description: "(Đại lý " + infoAgency.name + ") " + content,
                    arrReceiverID: listUserNotConnected, 
                    senderID: userID, 
                    body 
                });
            })

            socket.on(CSS_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST, async data => {
                let { agencyID, userID, userName, type, transactionID } = data;

                let infoAgency = await AGENCY_COLL
                    .findById(agencyID)
                    .select('owners employees name')
                    .lean();

                let title       = type == 6 ? "DUYỆT CHUỘC ĐỒ" : "KHÔNG DUYỆT CHUỘC ĐỒ";
                let string      = type == 6 ? "duyệt chuộc đồ" : "không duyệt chuộc đồ";
                let content   = `${userName} đã ` + string;
                let link	    = `/transaction/info-transaction/${transactionID}`;

                // 1/ lấy tất cả owner trừ thằng gửi(nếu sender là owner)
                let listUserSendNotifi = [];
                // let listOwnerIgnoreSender = infoAgency.owners.filter(member => `${member}` != `${userID}`);
                listUserSendNotifi = infoAgency.owners.concat(infoAgency.employees);
                // 2/ insert noti cho tất cả recievers
                // let arrNotifiIDOfMember = [];
                let listNotiMapInfo = listUserSendNotifi.map(member => {
                    if ( member.toString() != userID.toString() ){
                        return NOTIFICATION_MODEL.insertV2({
                            title,
                            description: content,
                            sender: userID,
                            receiver: member,
                            type: type,
                            arrayAgencyReceive: [agencyID],
                            url: link,
                            agency: agencyID
                        });
                    }   
                })
                let listNotiAfterInsert = await Promise.all(listNotiMapInfo);
                // 3/ GỬI socket tới tất cả recivers Online
                listNotiAfterInsert = listNotiAfterInsert.filter(infoNoti => (
                    infoNoti && infoNoti.data
                ));
                // console.log({ listNotiAfterInsert });
                mapSocketIDAndData(listUserSendNotifi, SSC_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST, {
                    title, content, listNotiAfterInsert, userSend: userID, transactionID
                }, usersConnected, io);

               /**
                 * Lấy danh sách user not connected
                 */
                // console.log({
                //     usersConnected, listUserSendNotifi
                // });
                let listUserNotConnected = getUserNotConnected(usersConnected, listUserSendNotifi);
                // console.log({
                //     listUserNotConnected
                // });
                let body = {
                    screen_key: SCREEN_KEY.DetailDealScreen.text,
                    sender: userID,
                    transactionID: transactionID,
                    agencyID: agencyID,
                };
                // console.log({
                //     body
                // });
                
                sendMessageMobile({ 
                    title: title, 
                    description: "(Đại lý " + infoAgency.name + ") " + content,
                    arrReceiverID: listUserNotConnected, 
                    senderID: userID, 
                    body 
                });
            })
        } // END AUTH

        socket.on('disconnect', function () {
            // console.log(`Disconnected..., Socket ID: ${socket.id}`);
            console.log(`Disconnected..., Socket ID: ${socket.id}`);
            /**
            * ADD USER INTO usersConnected
            * usersConnected
            */
           let { id: socketIDDisconnect, decoded } = socket;
           if (!decoded || !decoded._id) return;

           let { _id: userIDDisconnect } = decoded;
           
           let usersConnectedGlobal = usersConnectedInstance.usersOnline;

           let itemDisconnect          = usersConnectedGlobal.find(itemSocket => itemSocket.userID == userIDDisconnect);

        //    console.log({
        //        [`thông tin trước khi disconnect`]: itemDisconnect,
        //        [`số lượng socketID trước khi disconnect`]: itemDisconnect && itemDisconnect.socketID && itemDisconnect.socketID.length
        //    })

           let listItemStillConnect    = usersConnectedGlobal.filter(itemSocket => itemSocket.userID != userIDDisconnect);
           let listSocketIDOfItemDisconnectAfterRemoveSocketDisconnect = itemDisconnect && itemDisconnect.socketID.filter(socketID => socketID != socketIDDisconnect);

           let itemDisconnectAfterRemoveSocketID = {
               ...itemDisconnect, socketID: listSocketIDOfItemDisconnectAfterRemoveSocketDisconnect
           }
           let listUserConnectAfterRemoveSocketDisconnect = [
               ...listItemStillConnect, itemDisconnectAfterRemoveSocketID
           ]

        //    console.log({
        //        [`thông tin sau khi disconnect`]: itemDisconnectAfterRemoveSocketID,
        //        [`số lượng socketID sau khi disconnect`]: itemDisconnectAfterRemoveSocketID && itemDisconnectAfterRemoveSocketID.socketID && itemDisconnectAfterRemoveSocketID.socketID.length
        //    })

           usersConnectedInstance.setUsersOnline(listUserConnectAfterRemoveSocketDisconnect);
        })
    });
};
