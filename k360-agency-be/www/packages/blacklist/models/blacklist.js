"use strict";

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 	= require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel 	 		= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const BLACKLIST_COLL 		= require('../database/blacklist-coll');


class Model extends BaseModel {
    constructor() {
        super(BLACKLIST_COLL);
    }

    insert({ agencyID, userID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([agencyID, userID]))
                    return resolve({ error: true, message: 'param_invalid' });

                let infoAfterInsert = await this.insertData({
					agency: agencyID,
					user: userID
				});

                if (!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_add_blacklist' });

                return resolve({ error: false, data: infoAfterInsert })
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

	delete({ agencyID, userID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs([agencyID, userID]))
                    return resolve({ error: true, message: 'param_invalid' });

                let infoAfterDelete = await BLACKLIST_COLL.deleteOne({
					agency: agencyID,
					user: userID
				})

                if (!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_delete_blacklist' });

                return resolve({ error: false, data: infoAfterDelete })
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

	getList() {
        return new Promise(async resolve => {
            try {
                let listBlacklist = await BLACKLIST_COLL.find().lean();

                if (!listBlacklist)
                    return resolve({ error: true, message: 'cannot_get_list_blacklist' });

                return resolve({ error: false, data: listBlacklist })
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

}
exports.MODEL = new Model;