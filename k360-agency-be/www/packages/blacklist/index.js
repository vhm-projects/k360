const BLACKLIST_MODEL 	= require('./models/blacklist').MODEL;
const BLACKLIST_COLL  	= require('./database/blacklist-coll');

module.exports = {
    BLACKLIST_COLL,
    BLACKLIST_MODEL,
}
