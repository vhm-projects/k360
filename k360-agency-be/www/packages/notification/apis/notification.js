"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const { TYPE_NOTIFICATION }             = require('../../../config/cf_constants');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_NOTIFICATION }      = require('../constants/notification.uri');
const { provinces }                     = require('../../common/constant/provinces');

/**
 * MODELS
 */
const NOTIFICATION_MODEL                 = require("../models/notification").MODEL;
const AGENCY_MODEL                       = require("../../agency/models/agency").MODEL;


/**
 * COLLECTIONS
 */
const NOTIFICATION_COLL                 = require('../databases/notification-coll')({});



module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            
            /**
             * Function: Danh sách thông báo (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION,
					inc: path.resolve(__dirname, '../views/list_notification.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID = req.agencyID;
                        //console.log({ userID, agencyID });
                        let { start, end } = req.query;
                        let listNotification = await NOTIFICATION_MODEL.getList({ agencyID, start, end, userID });
                     
                        ChildRouter.renderToView(req, res, {
							listNotification: listNotification.data,
                            start, end,
                            TYPE_NOTIFICATION
						});
                    }]
                },
            },

            /**
             * Function: Danh sách thông báo (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.VIEW_LIST_CORE_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION,
					inc: path.resolve(__dirname, '../views/list_notification_core.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID = req.agencyID;
                        let { start, end } = req.query;
                        let listNotification = await NOTIFICATION_MODEL.getListNotificationCore({ agencyID, start, end, userID });

                        ChildRouter.renderToView(req, res, {
							listNotification: listNotification.data,
                            start, end
						});
                    }]
                },
            },

            /**
             * Function: Chi tiết thông báo (View)
             * Date: 09/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.DETAIL_CORE_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'view',
					title: `Detail Agency - K360`,
					code: CF_ROUTINGS_NOTIFICATION.DETAIL_CORE_NOTIFICATION,
					inc: path.resolve(__dirname, '../views/detail_notification.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { notificationCoreID } = req.params;
                        let { type }      = req.query;
                        let infoNotification = await NOTIFICATION_MODEL.getInfo({ notificationID: notificationCoreID });
                       
                        if (type && type === "api") {
                            return res.json(infoNotification);
                        }
                        ChildRouter.renderToView(req, res, {
                            infoNotification: infoNotification.data
						});
                    }]
                },
            },


            //========================= JSON ============================
          
            /**
             * Function: Info Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.INFO_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { notificationID } = req.params;
                        
                        const infoNotification = await NOTIFICATION_MODEL.getInfo({ notificationID });

                        res.json(infoNotification);
                    }]
                },
            },

			/**
             * Function: List Notification of agency (API)
             * Date: 24/06/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_NOTIFICATION.LIST_NOTIFICATIONS]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { _id: receiverID } = req.user;
                        const agencyID = req.agencyID;
                        const { skip } = req.query;

                        const listNotifications = await NOTIFICATION_MODEL.getListLoadMore({ 
                            skip, receiverID, agencyID
                        });
                        res.json(listNotifications);
                    }]
                },
            },

            /**
             * Function: Remove Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.REMOVE_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { notificationID } = req.params;
                        const infoNotificationRemove = await NOTIFICATION_MODEL.remove({ notificationID });
                        res.json(infoNotificationRemove);
                    }]
                },
            },

            /**
             * Function: Update Seen all Notification of Agency
             * Date: 03/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.SEEN_ALL_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID } = req.query;
                        const listNotificationSeenAll = await NOTIFICATION_MODEL.updateSeenAllNotification({ agencyID });
                        res.json(listNotificationSeenAll);
                    }]
                },
            },

            /**
             * Function: Update Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.UPDATE_STATUS_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { notifyID, status } = req.query;

                        const infoAfterUpdate = await NOTIFICATION_COLL.findByIdAndUpdate(notifyID, {
                            $set: { status }
                        }, { new: true });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            //============================== API MOBILE =========================

            /**
             * Function: Danh sách thông báo của USERID
             * Date: 04/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_NOTIFICATION.LIST_NOTIFICATION_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { start, end, page, phone, keyword } = req.query;
                        const agencyID = req.agencyID;
                        console.log(userID);
                        let listNotification = await NOTIFICATION_MODEL.getListNotificationOfAgencyID({ agencyID, page, keyword, userID });
                        // let listProvince = Object.entries(provinces);
                        // let listAgency = await AGENCY_MODEL.getList({ })
                        let description = {
                            status: "Trạng thái xem thông báo(0 chưa xem/ 1 đã xem)",
                            title: "Tên thông báo",
                            description: "Mô tả",
                            content: "Nội dung",
                            createAt: "Ngày gửi"
                        }
                        res.json({
                            description,
							...listNotification,
						});
                    }]
                },
            },
        }
    }
};
