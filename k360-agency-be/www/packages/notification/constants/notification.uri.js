const BASE_ROUTE = '/notification';
const BASE_ROUTE_API = '/api/notification';

const CF_ROUTINGS_NOTIFICATION = {
    VIEW_LIST_NOTIFICATION: `${BASE_ROUTE}`,
    VIEW_LIST_CORE_NOTIFICATION: `${BASE_ROUTE}/list-notification-core`,
    DETAIL_CORE_NOTIFICATION: `${BASE_ROUTE}/notification-core/:notificationCoreID`,

    ADD_NOTIFICATION: `${BASE_ROUTE}/add-notification`,
    INFO_NOTIFICATION: `${BASE_ROUTE}/info-notification/:notificationID`,
    REMOVE_NOTIFICATION: `${BASE_ROUTE}/remove-notification/:notificationID`,
    UPDATE_NOTIFICATION: `${BASE_ROUTE}/update-notification/:notificationID`,
    UPDATE_STATUS_NOTIFICATION: `${BASE_ROUTE}/update-status-notification`,
    FILTER_NOTIFICATION: `${BASE_ROUTE}/filter-notification`,
    SEEN_ALL_NOTIFICATION: `${BASE_ROUTE}/seen-all-notification`,

	LIST_NOTIFICATIONS: `${BASE_ROUTE}/list-notifications`,
    /**
     * API MOBILE
     */
    LIST_NOTIFICATION_API:   `${BASE_ROUTE_API}/list-notification`,

	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_NOTIFICATION = CF_ROUTINGS_NOTIFICATION;