"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const moment            = require("moment");

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const NOTIFICATION_COLL = require('../databases/notification-coll')({});
const CUSTOMER_COLL     = require('../../customer/database/customer-coll')({});
const { AGENCY_COLL }   = require('../../agency');

class Model extends BaseModel {
    constructor() {
        super(NOTIFICATION_COLL);
    }

    /*
    Thêm thông báo
    Dattv
     */
	insert({ title, description, type, url, sender, receiver, arrayAgencyReceive, content, notificationCore, agency }) {
        return new Promise(async resolve => {
            try {
                if(!title || !description)
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataInsert = {
                    title,
                    description,
                    type,
                    content
                }
                sender      && (dataInsert.sender   = sender);
                receiver    && (dataInsert.receiver = receiver);
                url    		&& (dataInsert.url 		= url);
                agency      && (dataInsert.agency = agency);
                notificationCore && (dataInsert.notificationCore = notificationCore);

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'send_notification_failed' });

                if(arrayAgencyReceive){
                    arrayAgencyReceive = arrayAgencyReceive.split(",");
                    let notifyAfterUpdate = await NOTIFICATION_COLL.findByIdAndUpdate(infoAfterInsert._id, {
                        $addToSet: {
                            agencys_receive: {
                                $each: arrayAgencyReceive
                            }
                        }
                    }, { new: true })
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: THÊM THÔNG BÁO
     * AUTHOR: SONLP
     * @param {*} param0 
     * @returns 
     */
    insertV2({ title, description, type, url, sender, receiver, customerReceiver, arrayAgencyReceive, agency }) {
        return new Promise(async resolve => {
            try {
                if(!title || !description)
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataInsert = {
                    title,
                    description,
                    type
                }
                sender      && (dataInsert.sender   = sender);
                receiver    && (dataInsert.receiver = receiver);
                url    		&& (dataInsert.url 		= url);
                arrayAgencyReceive    		&& (dataInsert.agencys_receive 		= arrayAgencyReceive);
                customerReceiver    		&& (dataInsert.customerReceiver 	= customerReceiver);
                agency    		            && (dataInsert.agency 	            = agency);

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'send_notification_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /*
    Lấy thông tin thông báo và đồng thời cập nhật đã xem
    Dattv
     */
    getInfo({ notificationID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(notificationID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoNotification = await NOTIFICATION_COLL.findById(notificationID)
                .populate({ path: 'agencys_receive notificationCore'})

                if(!infoNotification)
                    return resolve({ error: true, message: 'notification_is_not_exists' });

                // console.log({ agencyID })
                // let infoAgency = await AGENCY_COLL.findById(agencyID);
                // let { owners } = infoAgency;

                // //Cập nhật đã xem nếu đó là owner của đại lý
                // if(owners && owners.length && owners.includes(userID)){
                //     await NOTIFICATION_COLL.findByIdAndUpdate(notificationID, {
                //         $addToSet: { agencys_seen: agencyID }
                //     })
                // }

                return resolve({ error: false, data: infoNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Update đã xem tất cả thông báo của đại lý
    Dattv
     */
    updateSeenAllNotification({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'param_not_valid' });

                    let listNotificationOfAgency = await NOTIFICATION_COLL.find({
                        agencys_receive: { $in: [agencyID] }
                        //agencys_receive: { $elemMatch: { agencyID } }
                    });

                    for (let notification of listNotificationOfAgency) {
                        await NOTIFICATION_COLL.findByIdAndUpdate(notification._id, {
                            $addToSet: { agencys_seen: agencyID }
                        });
                    }

                return resolve({ error: false, data: listNotificationOfAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Lấy tất cả thông báo của user
    Dattv
     */
    getList({ agencyID, start, end, userID }){
        return new Promise(async resolve => {
            try {
                let conditionObj = { };
                
                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate)
                    }
                }
                const listNotification = await NOTIFICATION_COLL.find({
                    ...conditionObj, agency: agencyID, receiver: userID 
                }).sort({ createAt: -1 }).lean()

                if(!listNotification)
                    return resolve({ error: true, message: 'not_found_notification_list' });

                return resolve({ error: false, data: listNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Lấy tất cả thông báo của đại lý từ hệ thống
    Dattv
     */
    getListNotificationCore({ agencyID, start, end, userID }){
        return new Promise(async resolve => {
            try {
                let conditionObj = { };
                let TYPE_NOTIFICATION_FROM_SYSTEM = 13;

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate)
                    }
                }

                const listNotification = await NOTIFICATION_COLL.find({
                    ...conditionObj, type: TYPE_NOTIFICATION_FROM_SYSTEM, receiver: userID, agency: agencyID
                }).sort({ createAt: -1 }).lean()

                if(!listNotification)
                    return resolve({ error: true, message: 'not_found_notification_list' });

                return resolve({ error: false, data: listNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    /*
    Update thông báo
    Dattv
     */
    update({ notificationID, title, description, content, arrayAgencyReceive }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(notificationID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const dataUpdate = {
                    title, 
                    description, 
                    content,
                }
                
                let infoNotificationAfterUpdate = await NOTIFICATION_COLL.findByIdAndUpdate({ _id: notificationID }, dataUpdate);
                if(!infoNotificationAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_notification' });

                let { _id } = infoNotificationAfterUpdate;

                await NOTIFICATION_COLL.findByIdAndUpdate(_id, { agencys_receive: [] });
                
                //Thêm các agency mới vào mảng
                if(arrayAgencyReceive){
                    arrayAgencyReceive = arrayAgencyReceive.split(",");
                    await NOTIFICATION_COLL.findByIdAndUpdate(_id, {
                        $addToSet: {
                            agencys_receive: {
                                $each: arrayAgencyReceive
                            }
                        }
                    }, { new: true });
                }

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Xóa thông báo
    Dattv
     */
    remove({ notificationID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(notificationID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await NOTIFICATION_COLL.findByIdAndDelete(notificationID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'notification_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Lọc danh sách thông báo từ ngày đến ngày
    Dattv
     */
    getListNotificationBetweenDay({ start, end }){
        return new Promise(async resolve => {
            try {

                let _fromDate   = moment(start).startOf('day').format();
                let _toDate     = moment(end).endOf('day').format();

                const listNotification = await NOTIFICATION_COLL.find({ 
                    createAt : { 
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate) 
                    }
                }).lean();

                if(!listNotification)
                    return resolve({ error: true, message: 'not_found_contacts_list' });

                return resolve({ error: false, data: listNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListLoadMore({ skip, receiverID, agencyID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(receiverID) || isNaN(skip))
                    return resolve({ error: true, message: 'param_invalid' });

				const listNotification = await NOTIFICATION_COLL
					.find({ receiver: receiverID, status: 0, agency: agencyID })
					.sort({ createAt: -1 })
					.limit(10)
					.skip(+skip)
					.lean();

				if(!listNotification)
					return resolve({ error: true, message: 'not_found_notify' });

				return resolve({ error: false, data: listNotification });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    /**
     * FUCNTION: LẤY DANH SÁCH THÔNG BÁO CỦA USER
     * AUHTOR: SONLP
     * @param {*} userID 
     * @param {*} page 
     * @returns {listNotifications}  
     */
    getListNotificationOfAgencyID({ agencyID, page, keyword, userID }) {
        return new Promise(async resolve => {
            try {
                const limit = 10;

                const TYPE_SYSTEM = 0;
                const TYPE_PRICINGPRODUCT = 1;
                const ARR_TYPE = [TYPE_SYSTEM, TYPE_PRICINGPRODUCT];
                const STATUS_INACTIVE = 0;

                let listArrCustomerId = [];
                if(keyword){
                    let condition = {};
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { name: new RegExp(key, 'i') },
                        { phone: new RegExp(key, 'i') }
                    ]
                    let listCustomer = await CUSTOMER_COLL.find({...condition});
                    listArrCustomerId = listCustomer.map( customer => customer._id );
                }
                let dataFind = {
                    receiver: userID,
                    agency: agencyID,
                    // type: { $in: ARR_TYPE },
                };

                if (listArrCustomerId.length) {
                    dataFind.customer = {
                        $in: listArrCustomerId
                    }
                }

                let countListNotifyInactive = await NOTIFICATION_COLL.count({ status: STATUS_INACTIVE, receiver: userID, agency: agencyID  })

                const listNotifications = await NOTIFICATION_COLL
                    .find({...dataFind})
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ createAt: -1 })
                    // .limit(30)
                    .populate({
                        path: "customer",
                        select: "_id name createAt phone status"
                    })
                    .lean();
                if(!listNotifications)
					return resolve({ error: true, message: 'not_found_notify' });
               
				return resolve({ error: false, data: listNotifications, countListNotifyInactive });
            } catch (error) {
				return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
