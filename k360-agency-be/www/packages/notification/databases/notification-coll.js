"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION NOTIFICATIONS CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'notification' }){
	return BASE_COLL(name, {
        title: {
            type: String,
            require: true
        },

        //Nội dung thông báo
        description: {
            type: String,
            require: true
        },

        //Dành cho thông báo liên hệ
        content: String,

        /**
         * Người tạo thông báo
         */
        sender: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
        /**
         * FIELD NẾU ĐĂNG NHẬP CUSTOMER BẰNG APP
         * CUSTOMER SENDER
         */
        customer : {
            type:  Schema.Types.ObjectId,
            ref : 'customer'
        },
        /**
         * FIELD NẾU ĐĂNG NHẬP CUSTOMER BẰNG APP
         * CUSTOMER Receive
         */
         customerReceiver : {
            type:  Schema.Types.ObjectId,
            ref : 'customer'
        },
        /**
         * Người nhận thông báo
         */
        receiver: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },

        /**
         * Đại lý nhận thông báo
         */
        agency: {
            type: Schema.Types.ObjectId,
            ref: 'agency'
        },

        /**
         * 0: Thông báo hệ thống
         * 1: Yêu cầu định giá
         * 2: Yêu cầu tạo giao dịch
         * 3: Yêu cầu duyệt giao dịch cầm đồ
         * 4: Yêu cầu kh duyệt giao dịch cầm đồ
         * 5: Yêu cầu tạo giao dịch bổ sung chuộc đồ
         * 6: Yêu cầu duyệt chuộc đồ
         * 7: Yêu cầu không duyệt chuộc đồ
         * 8: Yêu cầu liên hệ
         * 9: Yêu cầu giao dịch đến hạn
         * 10: Yêu cầu giao dịch hết hạn
         * 11: Yêu cầu giao dịch quá hạn
         * 12: Yêu cầu thanh lý sản phẩm
         * 13: Thông báo của hệ thống cho đại lý
         */
        type: {
            type: Number,
            default: 0
        },

         /**
         * Trạng thái xem thông báo(0 chưa xem/ 1 đã xem)
         */
        status: {
            type: Number,
            default: 0
        },

         /**
         * Đường link truy cập
         */
        url: {
			type: String,
			default: ''
		},

        //Danh sách đại lý nhận thông báo
        agencys_receive: [{
            type: Schema.Types.ObjectId,
            ref: 'agency'
        }],

        //Danh sách đại lý đã xem thông báo
        agencys_seen: [{
            type: Schema.Types.ObjectId,
            ref: 'agency'
        }],

        //Thông báo của hệ thống cho đại lý
        notificationCore: {
            type: Schema.Types.ObjectId,
            ref: 'notification'
        },
     
		...fields
    });
}
