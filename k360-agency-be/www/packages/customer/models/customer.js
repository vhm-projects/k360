/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');
const cfJWS                         = require('../../../config/cf_jws');
/**
 * MODELS
 */
/**
 * COLLECTIONS
 */
const TRANSACTION_MODEL            = require('../../transaction/models/transaction').MODEL;

const CUSTOMER_COLL                = require('../database/customer-coll')({});
const CUSTOMER_AGENCY_COLL                = require('../database/customer_agency')({});

class Model extends BaseModel {
    constructor() {
        super(CUSTOMER_COLL);
    }

	insert({ name, gender, birthday, phone, job, address, city, district, ward, auth_paper, number_auth_paper, issued_on, issued_by, note, image_customer, image_auth_paper, code, agencyID }) {
        return new Promise(async resolve => {
            try {
                // if(!ObjectID.isValid(agencyID))
                //     return resolve({ error: true, message: "param_not_valid" });

                const dataInsert = {};
                if ( !name ){
                    return resolve({ error: true, message: 'name_not_valid' });
                }else{
                    dataInsert.name             = name;
                }
                /**
                 * Kiểm tra Loại xác thực
                 */
                if ( !auth_paper && !Number(auth_paper) ){
                    return resolve({ error: true, message: 'auth_paper_not_valid' });
                }else{
                    dataInsert.auth_paper             = Number(auth_paper);
                }
                /**
                 * Kiểm tra Số giấy xác thực
                 */
                if ( !number_auth_paper ){
                    return resolve({ error: true, message: 'number_auth_paper_not_valid' });
                }
                
                if ( Number(auth_paper) == 0 && number_auth_paper.length != 9 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_9_character' });
                }
                if ( Number(auth_paper) == 1 && number_auth_paper.length != 12 ) {
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });
                }
                if ( Number(auth_paper) == 2 && number_auth_paper.length != 12 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });
                }
                let checkNumberAuth = await CUSTOMER_COLL.findOne({ number_auth_paper: number_auth_paper });
                if(checkNumberAuth) {
                    return resolve({ error: true, message: "number_auth_paper_existed" });
                }
                dataInsert.number_auth_paper             = number_auth_paper;
                /**
                 * Kiểm tra ngày cấp giấy xác thực
                 */
                let checkDateIssuedOn = new Date(issued_on);
                if ( !issued_on || !checkDateIssuedOn ){
                    return resolve({ error: true, message: 'issued_on_not_valid' });
                }else{
                    dataInsert.issued_on             = checkDateIssuedOn;
                }
                /**
                 * Kiểm tra nơi cấp giấy xác thực
                 */

                if ( !issued_by ){
                    return resolve({ error: true, message: 'issued_by_not_valid' });
                }else{
                    dataInsert.issued_by             = issued_by;
                }
                /**
                 * Kiểm tra ảnh khách hàng
                 */
                if ( !image_customer && image_customer.length == 0 ){
                    return resolve({ error: true, message: 'image_customer_not_valid' });
                }else{
                    dataInsert.image_customer             = image_customer;
                }
                /**
                 * Kiểm tra ảnh giấy xác thực
                 */
                if ( !image_auth_paper && image_auth_paper.length == 0  ){
                    return resolve({ error: true, message: 'image_auth_paper_not_valid' });
                }else{
                    dataInsert.image_auth_paper           = image_auth_paper;
                }
                /**
                 * Kiểm tra phone có 10 ký tự 
                 */
                
                if ( phone && Number(phone) && phone.length == 10 && Number(phone) > 0){
                    let checkPhoneExisted = await CUSTOMER_COLL.findOne({ phone: phone });
                    if(checkPhoneExisted) {
                        return resolve({ error: true, message: "phone_existed" });
                    }
                    dataInsert.phone = phone;
                }else{
                    return resolve({ error: true, message: "phone_invalid" });
                }
                /**
                 * Kiểm tra ngày sinh
                 */
                let checkDateBirthDay = new Date(birthday);
                if ( birthday && checkDateBirthDay ){
                    dataInsert.birthday             = checkDateBirthDay;
                }
                /**
                 * Kiểm tra thanh pho
                 */
                if ( !city ){
                    return resolve({ error: true, message: "city_invalid" });
                }else{
                    dataInsert.city             = city;
                }
                /**
                 * Kiểm tra thanh pho
                */
                if ( !code ){
                    return resolve({ error: true, message: "code_invalid" });
                }else{
                    let checkExistCode = await CUSTOMER_COLL.findOne({ code: Number(code) });
                    if ( checkExistCode ){
                        return resolve({ error: true, message: "info_customer_have_code_exist" });
                    }else{
                        dataInsert.code             = Number(code);
                    }
                }
                address         && (dataInsert.address          = address);
                // agencyID        && (dataInsert.agency           = agencyID);
                gender          && (dataInsert.gender           = gender);
                job             && (dataInsert.job              = job);
                district        && (dataInsert.district         = district);
                ward            && (dataInsert.ward             = ward);
                note            && (dataInsert.note             = note);
                // console.log({dataInsert  });
                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'register_customer_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ customerID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoCustomer = await CUSTOMER_COLL.findById(customerID)
                    .populate({
                        path: 'image_customer',
                        select:'_id name path'
                    })
                    .populate({
                        path: 'image_auth_paper',
                        select:'_id name path'
                });
                if(!infoCustomer)
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByPhone({ phone }) {
        return new Promise(async resolve => {
            try {
                if(!phone)
                    return resolve({ error: true, message: "param_not_valid" });
                console.log({ phone });
                const infoCustomer = await CUSTOMER_COLL.find({ phone });
                console.log({ infoCustomer });

                if(!infoCustomer)
                    return resolve({ error: true, message: "customer_is_not_exists" });
                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     * Kiểm tra thông tin khách hàng bằng code
     */
    getInfoCode({ code }) {
        return new Promise(async resolve => {
            try {
                if( !code ){
                    return resolve({ error: true, message: "code_invalid" });
                }
                const infoCustomer = await CUSTOMER_COLL.findOne({ code: Number(code) });
                    
                if(!infoCustomer)
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Kiểm tra thông tin khách hàng bằng code
     */
     checkCode({ code }) {
        return new Promise(async resolve => {
            try {
                if( !code ){
                    return resolve({ error: true, message: "code_invalid" });
                }
                const infoCustomer = await CUSTOMER_COLL.findOne({ code: Number(code) });
                    
                if(infoCustomer)
                    
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // Lấy thông tin để check số điện thoại có tồn tại
    getInfoPhone({ phone }) {
        return new Promise(async resolve => {
            try {
                if( !phone || phone.length != 10 || !Number(phone)){
                    return resolve({ error: true, message: "phone_invalid" });
                }
                
                const infoCustomer = await CUSTOMER_COLL.findOne({ phone: phone });
                    
                if(infoCustomer)
                    return resolve({ error: true, message: "phone_existed" });

                return resolve({ error: false, message: "phone_inexisted", data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // Lấy thông tin Khách hàng với sdt số điện thoại 
    getInfoByPhones({ phone }) {
        return new Promise(async resolve => {
            try {
                if( !phone || phone.length != 10 || !Number(phone)){
                    return resolve({ error: true, message: "phone_invalid" });
                }
                
                const infoCustomer = await CUSTOMER_COLL.findOne({ phone: phone });
                    
                if(!infoCustomer)
                    return resolve({ error: true, message: "phone_inexisted" });

                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoPhoneUpdate({ phone, customerID }) {
        return new Promise(async resolve => {
            try {
                if( !phone || phone.length != 10 || !Number(phone)){
                    return resolve({ error: true, message: "phone_invalid" });
                }
                
                const infoCustomer = await CUSTOMER_COLL.findOne({ phone: phone });
                
                // console.log({ infoCustomer });
                if(infoCustomer && infoCustomer._id != customerID)
                    return resolve({ error: true, message: "phone_existed" });

                return resolve({ error: false, message: "phone_inexisted" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({  }){
        return new Promise(async resolve => {
            try {
                const listCustomer = await CUSTOMER_COLL.find({});
                if(!listCustomer)
                    return resolve({ error: true, message: "not_found_accounts_list" });
                
                return resolve({ error: false, data: listCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ agencyID, customerID, name, gender, birthday, phone, job, address, city, district, ward, auth_paper, number_auth_paper, issued_on, issued_by, note, image_customer, image_auth_paper, status }) {
        return new Promise(async resolve => {
            try {
                const dataUpdateUser = {};
                if ( name ){
                    dataUpdateUser.name             = name;
                }
                if( !ObjectID.isValid(customerID) )
                    return resolve({ error: true, message: "param_not_valid" });
                /**
                 * Kiểm tra Loại xác thực
                 */
                if ( auth_paper && Number(auth_paper) ){
                    dataUpdateUser.auth_paper             = auth_paper;
                }
                /**
                 * Kiểm tra Số giấy xác thực
                 */
                // if ( number_auth_paper ){
                //     dataUpdateUser.number_auth_paper             = number_auth_paper;
                // }
                if ( !number_auth_paper && !Number(number_auth_paper) ){
                    // return resolve({ error: true, message: 'number_auth_paper_not_valid' });
                
                }else if ( Number(auth_paper) == 0 && number_auth_paper.length != 9 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_9_character' });
                
                }else if ( Number(auth_paper) == 1 && number_auth_paper.length != 12 ) {
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });
                
                }else if ( Number(auth_paper) == 2 && number_auth_paper.length != 12 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });

                }else{
                    // if (!Number.isNaN(number_auth_paper)) {
                    //     return resolve({ error: true, message: "number_auth_paper_existed" });
                    // }
                    let checkNumberAuth = await CUSTOMER_COLL.findOne({ number_auth_paper: number_auth_paper });
                    if(checkNumberAuth && checkNumberAuth._id.toString() != customerID.toString()) {
                        return resolve({ error: true, message: "number_auth_paper_existed" });
                    }
                    dataUpdateUser.number_auth_paper             = number_auth_paper;
                }
                /**
                 * Kiểm tra ngày cấp giấy xác thực
                 */
                 let checkDateIssuedOn = new Date(issued_on);
                if ( issued_on || checkDateIssuedOn ){
                    dataUpdateUser.issued_on             = checkDateIssuedOn;
                }
                /**
                 * Kiểm tra nơi cấp giấy xác thực
                 */
                if ( issued_by ){
                    dataUpdateUser.issued_by             = issued_by;
                }
                /**
                 * Kiểm tra ảnh khách hàng
                 */
                if ( image_customer && image_customer.length ){
                    dataUpdateUser.image_customer             = image_customer;
                    
                }
                /**
                 * Kiểm tra ảnh giấy xác thực
                 */
                if ( image_auth_paper && image_auth_paper.length ){
                    dataUpdateUser.image_auth_paper           = image_auth_paper;
                }
                // console.log({ status });
                if ( status && status != "undefined" ){
                    let infoCustomerAfterInactive = await CUSTOMER_AGENCY_COLL.findOneAndUpdate({ agency: agencyID, customer: customerID }, { status: Number(status) })
                    // dataUpdateUser.status           = Number(status);
                    // console.log({ infoCustomerAfterInactive });
                }
                /**
                 * Kiểm tra phone có 10 ký tự -> 11 ký tự
                 */
                if ( phone && Number(phone) && phone.length == 10){
                    const infoCustomer = await CUSTOMER_COLL.findOne({ phone: phone });
                
                    if(infoCustomer && infoCustomer._id != customerID)
                        return resolve({ error: true, message: "phone_existed" });
                    
                    dataUpdateUser.phone = phone;
                }
                /**
                 * Kiểm tra thanh pho
                 */
                if ( !city ){
                    return resolve({ error: true, message: "city_invalid" });
                }else{
                    dataUpdateUser.city             = city;
                }

                address         && (dataUpdateUser.address          = address);
                gender          && (dataUpdateUser.gender           = gender);
                birthday        && (dataUpdateUser.birthday         = new Date(birthday));
                job             && (dataUpdateUser.job              = job);
                // city            && (dataUpdateUser.city             = city);
                (dataUpdateUser.district         = district);
                (dataUpdateUser.ward             = ward);
                note            && (dataUpdateUser.note             = note);
                // console.log({ dataUpdateUser });
                await this.updateWhereClause({ _id: customerID }, {
                    ...dataUpdateUser
                });

                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ customerID }) {
        return new Promise(async resolve => {
            try {
            if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_not_valid" });

                let checkTransaction = await TRANSACTION_MODEL.getInfoTransactionCustomerID({ customerID })
                // console.log({ checkTransaction });
                if(checkTransaction) {
                    return resolve({ error: true, message: "account_have_transaction" });
                }
                const infoAfterDelete = await CUSTOMER_COLL.findByIdAndRemove(customerID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "account_is_not_exists" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    filter({ agencyID, gender, city, district, ward, auth_paper, limit = 100, status }){
        return new Promise(async resolve => {
            try {
                const condition = {};

                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });

                // condition.agency = agencyID;

                if ( gender ){
                    condition.gender = gender;
                }

                if ( city ){
                    condition.city = city;
                }

                if ( district ){
                    condition.district = district;
                }

                if ( ward ){
                    condition.ward = ward;
                }

                if ( auth_paper ){
                    condition.auth_paper = auth_paper;
                }

                if(status) {
                    condition.status = status;
                }
                // console.log({ condition });
                const listCustomer = await CUSTOMER_COLL.find(condition).sort({ createAt: -1 })
                    // .limit(limit);
                if( !listCustomer )
                    return resolve({ error: true, message: "not_found_customers_list" });

                return resolve({ error: false, data: listCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByAgency({ agencyID, status }){
        return new Promise(async resolve => {
            try {
                // console.log({ agencyID, status });
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });
                let dataFilter = { agency: agencyID };

                if ( status ){
                    dataFilter.status = status
                }

                const listData = await CUSTOMER_COLL.find(dataFilter);
                if(!listData) 
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    checkAuth({ token }) {
        return new Promise(async resolve => {
            try {
                let decoded = await jwt.verify(token, cfJWS.secret);
                if (!decoded) return resolve({ error: true, message: 'token_not_valid' });

                const { _id: userID } = decoded;
                
                if (!userID) return resolve({ error: true, message: 'token_not_valid' });

                const STATUS_ACTIVE_OF_USER = 1;
                let infoUserDB = await CUSTOMER_COLL.findOne({ status: STATUS_ACTIVE_OF_USER, _id: userID });
                if (!infoUserDB) 
                    return resolve({ error: true, message: 'token_not_valid' });

                return resolve({ error: false, data: infoUserDB });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;