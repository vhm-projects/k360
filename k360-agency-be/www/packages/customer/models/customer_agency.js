/**
 * EXTERNAL PAKCAGE
 */
 const ObjectID = require('mongoose').Types.ObjectId;

 /**
  * INTERNAL PAKCAGE
  */
 /**
  * BASE
  */
 const BaseModel = require('../../../models/intalize/base_model');
 
 /**
  * MODELS
  */
 /**
  * COLLECTIONS
  */
 const CUSTOMER_AGENCY_COLL                = require('../database/customer_agency')({});
 const CUSTOMER_COLL                       = require('../database/customer-coll')({});
const { checkObjectIDs }            = require('../../../utils/utils');
 
 class Model extends BaseModel {
     constructor() {
         super(CUSTOMER_AGENCY_COLL);
     }
 
     insert({ agencyID, customerID }) {
         return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(agencyID) || !checkObjectIDs(customerID))
                    return resolve({ error: true, message: "param_not_valid" });

                let dataInsert = {
                    agency:   agencyID,
                    customer: customerID
                }
                let checkExist = await CUSTOMER_AGENCY_COLL.findOne({...dataInsert});
                if(checkExist){
                    return resolve({ error: true, message: "customer_agency_existed" });
                }
                // console.log({dataInsert  });
                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'register_customer_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
     }

     filter({ agencyID, gender, city, district, ward, auth_paper, limit = 100, status }){
        return new Promise(async resolve => {
            try {
                const condition = {};

                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });

                if ( gender ){
                    condition.gender = gender;
                }

                if ( city ){
                    condition.city = city;
                }

                if ( district ){
                    condition.district = district;
                }

                if ( ward ){
                    condition.ward = ward;
                }

                if ( auth_paper ){
                    condition.auth_paper = auth_paper;
                }

                if(status) {
                    condition.status = status;
                }

                // Lấy danh sách khách hàng của đại lý
                const listCustomerOfAgency = await CUSTOMER_AGENCY_COLL.find({ agency: agencyID }).sort({ createAt: -1 });
                let listCustomerIDOfAgency = listCustomerOfAgency.map( customer => (customer.customer));

                const STATUS_INACTIVE = 0;
                let listCustomerIDInactive = []
                listCustomerOfAgency.forEach( customer => {
                    if (customer.status == STATUS_INACTIVE) {
                        listCustomerIDInactive.push(customer.customer.toString());
                    }
                });

                if(listCustomerOfAgency && !listCustomerOfAgency.length) {
                    return resolve({ error: false, data: [] });
                }

                if(listCustomerIDOfAgency && listCustomerIDOfAgency.length) {
                    condition._id = {
                        $in: listCustomerIDOfAgency
                    }
                }

                const listCustomer = await CUSTOMER_COLL.find(condition).sort({ createAt: -1 });
                    // .limit(limit);
                if( !listCustomer )
                    return resolve({ error: true, message: "not_found_customers_list" });

                return resolve({ error: false, data: listCustomer, listCustomerIDInactive });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByAgency({ agencyID, status }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });

                const STATUS_ACTIVE = 1;
                
                // Lấy danh sách khách hàng của đại lý
                const listCustomerOfAgency       = await CUSTOMER_AGENCY_COLL.find({ agency: agencyID, status: STATUS_ACTIVE }).sort({ createAt: -1 });
                let listCustomerIDOfAgency       = listCustomerOfAgency.map( customer => (customer.customer));
                
                let condition = {  };

                if(status) {
                    condition.status = status
                }

                /**
                 * Kieemr tra xem khách hàng trong đại lý có tồn tại
                 */
                if(listCustomerIDOfAgency && listCustomerIDOfAgency.length) {
                    condition._id = {
                        $in: listCustomerIDOfAgency
                    }
                }else{
                    return resolve({ error: false, data: [] });
                }

                const listCustomer = await CUSTOMER_COLL.find(condition).sort({ createAt: -1 });

                return resolve({ error: false, data: listCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ agencyID, customerID }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(agencyID) || !checkObjectIDs(customerID))
                    return resolve({ error: true, message: "param_not_valid" });

                let dataFind = {
                    agency:   agencyID,
                    customer: customerID,
                }
                let checkExist = await CUSTOMER_AGENCY_COLL.findOne({ ...dataFind });
                // console.log({ });
                if(!checkExist){
                    return resolve({ error: true, message: "customer_agency_existed" });
                }

                return resolve({ error: false, data: checkExist });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
 }
 
 exports.MODEL = new Model;