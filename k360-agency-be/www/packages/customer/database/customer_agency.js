"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION Khách hàng của Đại lý
 */
module.exports = function({ fields = {}, name = 'customer_agency' }){
   return BASE_COLl(name, {
        //ID đại lý
        agency: {
            type: Schema.Types.ObjectId,
            ref: 'agency'
        },

        // ID Customer
        customer: {
            type: Schema.Types.ObjectId,
            ref: 'customer'
        },
        /**
         * Trạng thái hoạt động.
         * 1. Hoạt động
         * 0. Khóa
         */
        status: {
            type: Number,
            default: 1
        },
        
        ...fields
    });
}
