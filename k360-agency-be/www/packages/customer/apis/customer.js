"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs   = require('fs');
 const moment = require('moment');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadCusAndAuthPaper }                     = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_CUSTOMER }                      = require('../constant/customer.uri');
const { KIND_OF_AUTH_PAPER, TYPE_GENDER, TYPE_TRANSACTION }           = require('../../../config/cf_constants');
const { subStractDateGetDay }                       = require('../../../config/cf_time');

/**
 * MODELS, COLLECTIONS
 */  
const CUSTOMER_MODEL                                = require('../models/customer').MODEL;
const CUSTOMER_AGENCY_MODEL                         = require('../models/customer_agency').MODEL;
const { IMAGE_MODEL }                               = require('../../image');
const { provinces }                                 = require('../../common/constant/provinces');
const { districts }                                 = require('../../common/constant/districts');
const { TRANSACTION_MODEL }                         = require('../../transaction');
const { INTEREST_RATE_MODEL }                       = require('../../interest_rate');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ GROUP PERMISSION ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Thêm khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_CUSTOMER.ADD_ACCOUNT_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadCusAndAuthPaper, async function (req, res) {
                        const { _id: userCreate } = req.user;
                        const { 
                            name, gender, birthday, phone, job, address, city, district, 
                            ward, auth_paper, number_auth_paper, issued_on, issued_by, note, 
                            code, image_customer, image_auth_paper
                        } = req.body;
                        console.log({
                            name, gender, birthday, phone, job, address, city, district, 
                            ward, auth_paper, number_auth_paper, issued_on, issued_by, note, 
                            code, image_customer, image_auth_paper
                        });
                        // const file = req.files;
                        let agencyID = req.agencyID;
                        // console.log({ name, gender, birthday, phone, job, address, city, district, ward, auth_paper, number_auth_paper, issued_on, issued_by, note });
                        let imageCustomer = [];
                        let imageAuth = [];

                        if ( !image_customer || !image_auth_paper ){
                            return res.json({ error: true, message: "image_not_completely" })
                        }
                        // console.log({ image_customer, image_auth_paper });
                        for (let imgCustomer of image_customer){
                            let infoImageCustomerAfterInsert = await IMAGE_MODEL.insert({
                                name: imgCustomer,
                                path: imgCustomer,
                                userCreate: userCreate
                            });
                            imageCustomer[imageCustomer.length] = infoImageCustomerAfterInsert.data._id;
                        }
                        for (let imgAuthPaper of image_auth_paper){
                            let infoImageAuthPaperAfterInsert = await IMAGE_MODEL.insert({ 
                                name: imgAuthPaper,
                                path: imgAuthPaper,
                                userCreate: userCreate
                            });
                            imageAuth[imageAuth.length] = infoImageAuthPaperAfterInsert.data._id;
                        }
                        // console.log({ imageAuth, imageCustomer });

                        let infoAccountCustomer = await CUSTOMER_MODEL.insert({ 
                            name, gender, birthday, phone, job, address, city, district, 
                            ward, auth_paper, number_auth_paper, issued_on, issued_by, 
                            note, image_customer: imageCustomer, image_auth_paper: imageAuth, code
                        });
                        // console.log({ infoAccountCustomer });
                        if(infoAccountCustomer.error == true) {
                            return res.json({infoAccountCustomer});
                        }
                        // Insert Customer vào Agency với quan hệ nhiều nhiều
                        let infoCustomerAgency = await CUSTOMER_AGENCY_MODEL.insert({ customerID: infoAccountCustomer.data._id, agencyID });
                        if(infoCustomerAgency.error) {
                            return res.json({infoCustomerAgency});
                        }

                        res.json({ infoAccountCustomer });
                    }]
                },
            },

             /**
             * Function: update khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_CUSTOMER.UPDATE_ACCOUNT_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadCusAndAuthPaper, async function (req, res) {
                        const { _id: userCreate } = req.user;
                        let agencyID = req.agencyID;

                        const { 
                            name, gender, birthday, phone, job, address, city, district, ward, auth_paper, 
                            number_auth_paper, issued_on, issued_by, note, status, 
                            image_customer_old, 
                            image_auth_paper_old, 
                            image_customer, image_auth_paper,
                            imageCustomerIsDeleted, imageAuthPaperIsDeleted, 
                        } = req.body;
                        // console.log({ data: req.body });
                        console.log({
                            name, gender, birthday, phone, job, address, city, district, ward, auth_paper, 
                            number_auth_paper, issued_on, issued_by, note, status, 
                            image_customer_old, 
                            image_auth_paper_old, 
                            image_customer, image_auth_paper,
                            imageCustomerIsDeleted, imageAuthPaperIsDeleted, 
                        });
                        const { customerID } = req.params;
                        // const file           = req.files;
                        // console.log({ gender });
                        let imageCustomer = [];
                        let imageAuth = [];
                        // console.log({ 
                        //     image_customer_old, 
                        //     image_auth_paper_old, 
                        //     imageCustomerIsDeleted, 
                        //     imageAuthPaperIsDeleted, 
                        //     image_customer,
                        //     image_auth_paper
                        // });
                        //Image Customer
                        if( image_customer_old ){
                            // return res.json({ error: true, message: "image_customer_old_old_invalid" })
                            image_customer_old.forEach( imgCus => {
                                imageCustomer.push(imgCus);
                            })
                        }

                        /**
                         * Xóa image khách hàng trong coll IMAGE
                         */
                        if( imageCustomerIsDeleted ){
                            for( let imgCus of imageCustomerIsDeleted ){
                                let infoImageCustomerAfterDelete = await IMAGE_MODEL.delete({ imageID: imgCus });
                            }
                        }
                        //fs.unlinkSync
                        //Image Auth Paper
                        if( image_auth_paper_old ){
                            // return res.json({ error: true, message: "image_auth_paper_old_invalid" })
                            image_auth_paper_old.forEach( imgAuth => {
                                imageAuth.push(imgAuth);
                            })
                        }
                        // /**
                        //  * Xóa image giấy xác thực trong coll IMAGE
                        //  */
                        if( imageAuthPaperIsDeleted ){
                            for( let imgAuth of imageAuthPaperIsDeleted ){
                                let infoImageAuthAfterDelete = await IMAGE_MODEL.delete({ imageID: imgAuth });
                            }
                        }

                        // if ( !image_customer || !image_auth_paper ){
                        //     return res.json({ error: true, message: "image_not_completely" })
                        // }
                        if ( image_customer ) {
                            for (let imgCustomer of image_customer){
                                let infoImageCustomerAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgCustomer,
                                    path: imgCustomer,
                                    userCreate: userCreate
                                });
                                imageCustomer[imageCustomer.length] = infoImageCustomerAfterInsert.data._id;
                            }
                        }
                        
                        if ( image_auth_paper ){
                            for (let imgAuthPaper of image_auth_paper){
                                let infoImageAuthPaperAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgAuthPaper,
                                    path: imgAuthPaper,
                                    userCreate: userCreate
                                });
                                imageAuth[imageAuth.length] = infoImageAuthPaperAfterInsert.data._id;
                            }
                        }
                       
                        // console.log({ imageAuth, imageCustomer });
                        let infoAccountCustomer = await CUSTOMER_MODEL.update({ 
                            customerID, name, gender, birthday, phone, job, address, city, district, 
                            ward, auth_paper, number_auth_paper, issued_on, issued_by, 
                            note, image_customer: imageCustomer, image_auth_paper: imageAuth, status, agencyID
                        });
                        if(infoAccountCustomer.error == true) {
                            return res.json({infoAccountCustomer});
                        }
                        res.json({ infoAccountCustomer });
                    }]
                },
            },

            /**
             * Function: Danh sách khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
            */
            [CF_ROUTINGS_CUSTOMER.LIST_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_CUSTOMER.LIST_CUSTOMER,
					inc: path.resolve(__dirname, '../views/list_customer.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { gender, ward, city, district, authPaper } = req.query;
                        let agencyID = req.agencyID;
                        // console.log({ CUSTOMER_MODEL });
                        let listAccountCustomer = await CUSTOMER_AGENCY_MODEL.filter({ agencyID, gender, city, district, ward, auth_paper: authPaper });
                        // console.log({ agencyID });
                        let listProvince        = Object.entries(provinces);
                        // let listDistricts        = Object.entries(districts);
                        // console.log("asdasd");
                        let listDistrictsFilter = [];

                        let filterObject = (obj, filter, filterValue) => 
                            Object.keys(obj).reduce((acc, val) =>
                            (obj[val][filter] === filterValue ? {
                                ...acc,
                                [val]: obj[val]  
                            } : acc
                        ), {});
                        
                        if (city && !Number.isNaN(Number(city))) {
                            listDistrictsFilter = filterObject(districts, 'parent_code', city.toString())
                            listDistrictsFilter = Object.entries(listDistrictsFilter);
                        }
                        let listWards = [];
                        if( district && !Number.isNaN(Number(district)) ){
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${district}.json`);
                            fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);

                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            listAccountCustomer: listAccountCustomer.data, 
                                            _gender: gender, 
                                            city, 
                                            district, 
                                            ward, 
                                            auth_paper: authPaper, 
                                            KIND_OF_AUTH_PAPER,
                                            TYPE_GENDER,
                                            listProvince,
                                            listDistrictsFilter,
                                            listWards,
                                            listCustomerIDInactive: listAccountCustomer.listCustomerIDInactive,
                                        }
                                    );
                                    // res.json({ infoAccountCustomer, districtArr, listDistrictsFilter, wardArr, listWards  });
                                } else {
                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            listAccountCustomer: listAccountCustomer.data, 
                                            _gender: gender, 
                                            city, 
                                            district, 
                                            ward, 
                                            auth_paper: authPaper, 
                                            KIND_OF_AUTH_PAPER,
                                            TYPE_GENDER,
                                            listProvince,
                                            listDistrictsFilter,
                                            listWards,
                                            listCustomerIDInactive: listAccountCustomer.listCustomerIDInactive,
                                        }
                                    );
                                }
                            });
                        }else{
                            ChildRouter.renderToView(req, res, 
                                { 
                                    listAccountCustomer: listAccountCustomer.data, 
                                    _gender: gender, 
                                    city, 
                                    district, 
                                    ward, 
                                    auth_paper: authPaper, 
                                    KIND_OF_AUTH_PAPER,
                                    TYPE_GENDER,
                                    listProvince,
                                    listDistrictsFilter,
                                    listWards,
                                    listCustomerIDInactive: listAccountCustomer.listCustomerIDInactive,
                                }
                            );
                        }
                        // ChildRouter.renderToView(req, res, 
                        //     { 
                        //         listAccountCustomer: listAccountCustomer.data, 
                        //         _gender: gender, 
                        //         city, 
                        //         district, 
                        //         ward, 
                        //         auth_paper: authPaper, 
                        //         KIND_OF_AUTH_PAPER,
                        //         TYPE_GENDER,
                        //         listProvince,
                        //         // listDistricts
                        //         listDistrictsFilter
                        //     }
                        // );
                    }],
                },
            },

            /**
             * API lấy danh sách Customer Mobile
            */
             [CF_ROUTINGS_CUSTOMER.LIST_CUSTOMER_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { gender, ward, city, district, authPaper, status } = req.query;
                        let agencyID = req.agencyID;
                        // console.log({ req });
                        let listAccountCustomer = await CUSTOMER_AGENCY_MODEL.filter({ agencyID, gender, city, district, ward, auth_paper: authPaper, status });
                        // if(listAccountCustomer.error == true){
                        //     return res.json({ error: true, message: "cannot_get_list_customer" })
                        // } 
                        let listProvince        = Object.entries(provinces);
                        let listCustomer        = [];

                        let listDistricts       = Object.entries(districts);
                        if(listAccountCustomer.error == false){
                            listAccountCustomer.data.forEach( customer => {
                                let nameProvince;
                                let objInfoCustomer = {};
                                for(let province of listProvince) {
                                    if(province[1].code == customer.city) {
                                        nameProvince = province[1].name_with_type;
                                        break;
                                    }
                                }
                                objInfoCustomer = {
                                    _id:      customer._id,
                                    code:     customer.code,
                                    phone:    customer.phone,
                                    name:     customer.name,
                                    gender:   customer.gender,
                                    birthday: customer.birthday,
                                    status:   customer.status,
                                    city:     nameProvince,
                                }
                                if(customer.district) {
                                    for(let district of listDistricts) {
                                        if(district[1].code == customer.district ){
                                            objInfoCustomer.district = district[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                                listCustomer.push({
                                    ...objInfoCustomer
                                });
                                // break;
                            });
                        }else{
                            listCustomer = []
                        }
                        let description = {
                            status: "Trạng thái hoạt động (1. Hoạt động/ 0. Khóa)"
                        };
                        return res.json({
                            description,
                            listCustomer
                        });
                    }],
                },
            },
            /**
             * Function: Kiểm tra code đã tồn tại (API)
             * Date: 26/04/2021
             * Dev: SonLP aaaaaaaaa
            */
            [CF_ROUTINGS_CUSTOMER.CHECK_CODE_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { code } = req.params;
                        let infoCustomer = await CUSTOMER_MODEL.getInfoCode({ code });
                        res.json({ infoCustomer })
                    }],
                },
            },

            [CF_ROUTINGS_CUSTOMER.CHECK_PHONE_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { phone } = req.params;
                        let infoCustomer = await CUSTOMER_MODEL.getInfoPhone({ phone });
                        res.json(infoCustomer)
                    }],
                },
            },

            [CF_ROUTINGS_CUSTOMER.GET_INFO_PHONE_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { phone } = req.params;
                        let { type }  = req.query;
                        let infoCustomer = await CUSTOMER_MODEL.getInfoByPhones({ phone });

                        if(infoCustomer.error) {
                            return res.json({ infoCustomer })
                        }
                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        if (type && type === "api") {
                            for (let province of listProvince){
                                if ( province[1].code == infoCustomer.data.city ){
                                    infoCustomer.data.city = province[1].name_with_type;
                                    break;
                                }
                            }
    
                            let listWards = [];
                            let wardArr = [];
                            if ( infoCustomer.data.district ){
                                for (let district of listDistricts){
                                    if ( district[1].code == infoCustomer.data.district ){
                                        districtArr = district;
                                        infoCustomer.data.district = district[1].name_with_type;
                                        break;
                                    }
                                }
                                let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                                await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                    if (!err) {
                                        listWards = JSON.parse(data);
                                        listWards = Object.entries(listWards);
                                        for (let ward of listWards){
                                            if ( ward[1].code == infoCustomer.data.ward ){
                                                infoCustomer.data.ward = ward[1].name_with_type;
                                                break;
                                            }
                                        }
                                        res.json({ infoCustomer });
                                    } else {
                                        res.json({ infoCustomer });
                                    }
                                });
                            }else{
                                res.json({ infoCustomer });
                            }
                        }
                        for (let province of listProvince){
                            if ( province[1].code == infoCustomer.data.city ){
                                provinceArr = province;
                                break;
                            }
                        }

                        let listWards = [];
                        let wardArr = [];
                        if ( infoCustomer.data.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoCustomer.data.district ){
                                    districtArr = district;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoCustomer.data.ward ){
                                            wardArr = ward;
                                            break;
                                        }
                                    }
                                    res.json({ infoCustomer, TYPE_GENDER, districtArr, provinceArr, wardArr  });
                                } else {
                                    res.json({ infoCustomer, TYPE_GENDER, districtArr, provinceArr, wardArr  });
                                }
                            });
                        }else{
                            res.json({ infoCustomer, TYPE_GENDER, districtArr, provinceArr, wardArr  });
                        }
                    }],
                },
            },

            [CF_ROUTINGS_CUSTOMER.CHECK_PHONE_UPDATE_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { phone, customerID } = req.params;
                        let infoCustomer = await CUSTOMER_MODEL.getInfoPhoneUpdate({ phone, customerID });
                        res.json(infoCustomer)
                    }],
                },
            },

            [CF_ROUTINGS_CUSTOMER.LIST_CUSTOMER_JSON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const listCustomer = await CUSTOMER_MODEL.getList({});
                        // ChildRouter.renderToView(req, res, { listCustomer: listCustomer.data });
                        res.json({ listCustomer });
                    }],
                },
            },
            /**
             * Function: Thông tin khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_CUSTOMER.GET_INFO_ACCOUNT_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_CUSTOMER.GET_INFO_ACCOUNT_CUSTOMER,
					inc: path.resolve(__dirname, '../views/info_customer.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customerID } = req.params;
                        let agencyID         = req.agencyID;
                        const infoAccountCustomer = await CUSTOMER_MODEL.getInfo({ customerID });

                        if(infoAccountCustomer.error) {
                            return res.json({ infoAccountCustomer })
                        }
                        const listTransactionOfCustomer = await TRANSACTION_MODEL.getListByCustomer({ agencyID, customerID });
                        let interestStartDate;
                        let listTransaction = [];
                        //Lấy thông tin giao dịch
                        if(listTransactionOfCustomer.error == false && listTransactionOfCustomer.data) {
                            for (let infoTransOfCustomer of listTransactionOfCustomer.data) {
                                interestStartDate = infoTransOfCustomer.interestStartDate;
                                let dateNow      = new Date();
                                interestStartDate          = moment(interestStartDate).startOf('day').format();
                                
                                // TÍnh số ngày của giao dịch đó
                                let limitDate    = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                                limitDate        = Math.floor(limitDate);
                                // let infoInterest = await INTEREST_RATE_MODEL.getInfo({ agencyID, type: 1, limitDate, typeProduct: infoTransOfCustomer.products[0].type._id });

                                if(limitDate < 0) {
                                    limitDate = 0;
                                }

                                let loanAmoutAfterInterest = 0;
                                // Tính số tiền khi + lãi
                                let TYPE_PRODUCT = infoTransOfCustomer.products[0].type._id;
                                let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate, price: infoTransOfCustomer.loanAmount, typeProduct: TYPE_PRODUCT});
                                checkDateToInterest && checkDateToInterest.data && checkDateToInterest.data.forEach( price => {
                                    loanAmoutAfterInterest += price;
                                })
                                let percent         = checkDateToInterest.percent;
                                let arrObjPerPeriod = checkDateToInterest.arrObjPerPeriod 
                                let payInterest     = infoTransOfCustomer.payInterest;
                                // if(infoInterest.data && infoInterest.data.length && limitDate > 0) {
                                //     loanAmoutAfterInterest = infoTransOfCustomer.loanAmount * infoInterest.data[0].percent /100;
                                // }
                                listTransaction.push({
                                    infoTransaction: infoTransOfCustomer,
                                    limitDate,
                                    loanAmoutAfterInterest,
                                    percent,
                                    arrObjPerPeriod,
                                    payInterest
                                })
                            };
                        }

                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        for (let province of listProvince){
                            if ( province[1].code == infoAccountCustomer.data.city ){
                                provinceArr = province;
                                break;
                            }
                        }

                        let listWards = [];
                        let wardArr = [];
                        if ( infoAccountCustomer.data.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoAccountCustomer.data.district ){
                                    districtArr = district;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoAccountCustomer.data.ward ){
                                            wardArr = ward;
                                            break;
                                        }
                                    }
                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            infoAccountCustomer: infoAccountCustomer.data,
                                            TYPE_GENDER,
                                            KIND_OF_AUTH_PAPER,
                                            districtArr,
                                            provinceArr,
                                            wardArr,
                                            listTransaction,
                                            TYPE_TRANSACTION
                                        }
                                    );
                                } else {
                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            infoAccountCustomer: infoAccountCustomer.data,
                                            TYPE_GENDER,
                                            KIND_OF_AUTH_PAPER,
                                            districtArr,
                                            provinceArr,
                                            wardArr,
                                            listTransaction,
                                            TYPE_TRANSACTION
                                        }
                                    );
                                }
                            });
                        }else{
                            ChildRouter.renderToView(req, res, 
                                { 
                                    infoAccountCustomer: infoAccountCustomer.data,
                                    TYPE_GENDER,
                                    KIND_OF_AUTH_PAPER,
                                    districtArr,
                                    provinceArr,
                                    wardArr,
                                    listTransaction,
                                    TYPE_TRANSACTION
                                }
                            );
                        }
                    }],
                },
            },

            [CF_ROUTINGS_CUSTOMER.GET_INFO_ACCOUNT_CUSTOMER_JSON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customerID } = req.params;
                        let agencyID = req.agencyID;

                        const infoAccountCustomer = await CUSTOMER_MODEL.getInfo({ customerID });
                        let infoCustomerAgency = await CUSTOMER_AGENCY_MODEL.getInfo({ customerID, agencyID });
                        // let listProvince            = Object.entries(provinces);

                        if(infoAccountCustomer.error) {
                            return res.json(infoAccountCustomer);
                        }
                        
                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];
                        let listDistrictsFilter = [];

                        let filterObject = (obj, filter, filterValue) => 
                            Object.keys(obj).reduce((acc, val) =>
                            (obj[val][filter] === filterValue ? {
                                ...acc,
                                [val]: obj[val]  
                            } : acc
                        ), {});
                        if (infoAccountCustomer.data.city && !Number.isNaN(Number(infoAccountCustomer.data.city))) {
                            listDistrictsFilter = filterObject(districts, 'parent_code', infoAccountCustomer.data.city.toString())
                        }
                        
                        let listWards = [];
                        let wardArr = [];
                        if ( infoAccountCustomer.data.district && !Number.isNaN(Number(infoAccountCustomer.data.district))){
                            for (let district of listDistricts){
                                if ( district[1].code == infoAccountCustomer.data.district ){
                                    districtArr = district;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoAccountCustomer.data.ward ){
                                            wardArr = ward;
                                            break;
                                        }
                                    }
                                    res.json({ infoAccountCustomer, districtArr, listDistrictsFilter, wardArr, listWards, infoCustomerAgency  });
                                } else {
                                    res.json({ infoAccountCustomer, districtArr, listDistrictsFilter, wardArr, listWards, infoCustomerAgency  });
                                }
                            });
                        }else{
                            res.json({ infoAccountCustomer, districtArr, listDistrictsFilter, wardArr, listWards, infoCustomerAgency  });
                        }
                    }],
                },
            },

            [CF_ROUTINGS_CUSTOMER.INFO_ACCOUNT_CUSTOMER_JSON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customerID } = req.params;

                        const infoAccountCustomer = await CUSTOMER_MODEL.getInfo({ customerID });
                        // let listProvince            = Object.entries(provinces);
                        
                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        for (let province of listProvince){
                            if ( province[1].code == infoAccountCustomer.data.city ){
                                provinceArr = province;
                                break;
                            }
                        }

                        let listWards = [];
                        let wardArr = [];
                        if ( infoAccountCustomer.data.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoAccountCustomer.data.district ){
                                    districtArr = district;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoAccountCustomer.data.ward ){
                                            wardArr = ward;
                                            break;
                                        }
                                    }
                                    res.json({ infoAccountCustomer, TYPE_GENDER, districtArr, provinceArr, wardArr  });
                                } else {
                                    res.json({ infoAccountCustomer, TYPE_GENDER, districtArr, provinceArr, wardArr  });
                                }
                            });
                        }else{
                            res.json({ infoAccountCustomer, TYPE_GENDER, districtArr, provinceArr, wardArr  });
                        }
                    }],
                },
            },
            /**
             * API lấy thông tin Customer Mobile
            */
            [CF_ROUTINGS_CUSTOMER.INFO_ACCOUNT_CUSTOMER_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customerID } = req.params;

                        const infoAccountCustomer = await CUSTOMER_MODEL.getInfo({ customerID });
                        // let listProvince            = Object.entries(provinces);
                        if(infoAccountCustomer.error == true) {
                            return res.json(infoAccountCustomer);
                        }
                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        let nameProvince;
                        let nameDistrict;
                        let objInfoCustomer = {};
                        if(infoAccountCustomer.error == false && infoAccountCustomer.data) {
                            objInfoCustomer = { ...infoAccountCustomer };
                        }
                        
                        for (let province of listProvince){
                            if ( province[1].code == infoAccountCustomer.data.city ){
                                // provinceArr = province;
                                nameProvince = province[1].name_with_type;
                                objInfoCustomer.nameProvince = province[1].name_with_type;
                                break;
                            }
                        }
                        // objInfoCustomer = {
                        //     ...objInfoCustomer,
                        //     nameProvince
                        // }
                        let listWards = [];
                        let wardArr = [];
                        // objInfoCustomer =
                        if ( infoAccountCustomer.data.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoAccountCustomer.data.district ){
                                    districtArr  = district;
                                    // nameDistrict = district[1].name_with_type;
                                    objInfoCustomer.nameDistrict = district[1].name_with_type;
                                    break;
                                }
                            }
                            
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    // let nameWard;
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoAccountCustomer.data.ward ){
                                            // wardArr  = ward;
                                            // nameWard = ward[1].name_with_type;
                                            objInfoCustomer.nameWard = ward[1].name_with_type;
                                            break;
                                        }
                                    }
                                    
                                    res.json({ 
                                        objInfoCustomer
                                    });
                                } else {
                                    res.json({ objInfoCustomer  });
                                }
                            });
                        }else{
                            res.json({ objInfoCustomer });
                        }
                    }],
                },
            },
            
            /**
             * Function: Thông tin khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_CUSTOMER.DELETE_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { customerID } = req.params;

                        // const infoAccountCustomerToDelete = await CUSTOMER_MODEL.getInfo({ customerID });

                        // if ( infoAccountCustomerToDelete.data ){
                        //     for ( let imageCustomer of infoAccountCustomerToDelete.data.image_customer){
                        //         let infoImageAuthAfterDelete = await IMAGE_MODEL.delete({ imageID: imageCustomer._id });
                        //         fs.unlinkSync(path.resolve(__dirname, `../../../../files/${imageCustomer.name}`))
                        //     }

                        //     for ( let imageAuthPaper of infoAccountCustomerToDelete.data.image_auth_paper){
                        //         let infoImageAuthAfterDelete = await IMAGE_MODEL.delete({ imageID: imageAuthPaper._id });
                        //         fs.unlinkSync(path.resolve(__dirname, `../../../../files/${imageAuthPaper.name}`))
                        //     }
                        // }
                        
                        const infoAccountCustomer = await CUSTOMER_MODEL.delete({ customerID });
                        if(infoAccountCustomer.error == true) {
                            return res.json(infoAccountCustomer);
                        }
                        res.json({ infoAccountCustomer })
                    }],
                },
            },

            /**
             * Function: Lọc khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_CUSTOMER.FILTER_CUSTOMER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { key, limit } = req.query;
                        const { gender, city, district, ward, auth_paper } = req.body;
                        
                        let nLimit = Number(limit);
                        const listCustomer = await CUSTOMER_MODEL.filter({ keyword: key, gender, city, district, ward, auth_paper, limit: nLimit });

                        res.json({ listCustomer })
                    }],
                },
            },

            // '/send-otp-register/:phone': {
            //     config: {
            //         auth: [ roles.role.all.bin ],
            //         type: 'json',
            //     },
            //     methods: {
            //         get: [ async (req, res) => {
            //             let { phone }      = req.params;
            //             let signalAfterCreateRecord = await CUSTOMER_MODEL.sendOTP({ phone });
            //             res.json(signalAfterCreateRecord)
            //         }]
            //     },
            // },
            // // [check ok]✅
            // '/verify-otp-register/:phone': {
            //     config: {
            //         auth: [ roles.role.all.bin ],
            //         type: 'json',
            //     },
            //     methods: {
            //         post: [ async (req, res) => {
            //             let { phone }      = req.params;
            //             let { code }       = req.body;
            //             //type: SMS_PRE_REGISTER 
            //             let signalAfterUpdate = await CUSTOMER_MODEL.verificationOTP({ phone, code});
            //             res.json(signalAfterUpdate)
            //         }]
            //     },
            // },
        }
    }
};
