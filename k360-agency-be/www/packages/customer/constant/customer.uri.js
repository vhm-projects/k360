const BASE_ROUTE_ADMIN = '/customer';
const BASE_ROUTE_API   = '/api/customer';

const CF_ROUTINGS_CUSTOMER = {
    // ACCOUNT KHÁCH HÀNG
    ADD_ACCOUNT_CUSTOMER: `${BASE_ROUTE_ADMIN}/add`,
    GET_INFO_ACCOUNT_CUSTOMER: `${BASE_ROUTE_ADMIN}/info/:customerID`,
    GET_INFO_ACCOUNT_CUSTOMER_JSON: `${BASE_ROUTE_ADMIN}/info-customer/:customerID`,
    INFO_ACCOUNT_CUSTOMER_JSON: `${BASE_ROUTE_ADMIN}/info-customers/:customerID`,
    UPDATE_ACCOUNT_CUSTOMER: `${BASE_ROUTE_ADMIN}/update/:customerID`,
    LIST_CUSTOMER: `${BASE_ROUTE_ADMIN}/list`,
    //API Render View Mobile
    LIST_CUSTOMER_API:             `${BASE_ROUTE_API}/list`,
    GET_INFO_ACCOUNT_CUSTOMER_API: `${BASE_ROUTE_API}/info/:customerID`,
    INFO_ACCOUNT_CUSTOMER_API:     `${BASE_ROUTE_API}/info-customer/:customerID`,



    LIST_CUSTOMER_JSON: `${BASE_ROUTE_ADMIN}/list-customer`,
    DELETE_CUSTOMER: `${BASE_ROUTE_ADMIN}/delete/:customerID` ,
    FILTER_CUSTOMER: `${BASE_ROUTE_ADMIN}/filter`,
    CHECK_CODE_CUSTOMER: `${BASE_ROUTE_ADMIN}/check-code/:code`,
    CHECK_PHONE_CUSTOMER: `${BASE_ROUTE_ADMIN}/check-phone/:phone`,
    GET_INFO_PHONE_CUSTOMER: `${BASE_ROUTE_ADMIN}/get-info-phone/:phone`,
    CHECK_PHONE_UPDATE_CUSTOMER: `${BASE_ROUTE_ADMIN}/check-phone-update/:customerID/:phone`,
    
    ORIGIN_APP: BASE_ROUTE_ADMIN
}

exports.CF_ROUTINGS_CUSTOMER = CF_ROUTINGS_CUSTOMER;
