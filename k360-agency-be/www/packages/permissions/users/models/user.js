"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      				 = require('mongoose').Types.ObjectId;
const jwt                           				 = require('jsonwebtoken');
const { hash, hashSync, compare }   				 = require('bcryptjs');
const { sendMailNewAccount, sendMailForgetPassword } = require('../../../../mailer/module/mail_user');

/**
 * INTERNAL PACKAGE
 */
const cfJWS 						= require('../../../../config/cf_jws');
const { randomStringFixLengthCode } = require('../../../../utils/string_utils');

/**
 * BASE
 */
const BaseModel = require('../../../../models/intalize/base_model');

/**
 * MODELS, COLLECTIONS
 */
const USER_COLL         					= require('../databases/user-coll')({});
const AGENCY_COLL   			     		= require('../../../agency/databases/agency-coll')({});
const CODE_RESET_PASSWORD_COLL 	    		= require('../databases/code_reset_password-coll')({});
const CODE_RESET_PASSWORD_MODEL 			= require('./code_reset_password').MODEL;
const { BLACKLIST_MODEL, BLACKLIST_COLL } 	= require('../../../blacklist');
const { checkObjectIDs }                = require('../../../../utils/utils');


class Model extends BaseModel {
    constructor() {
        super(USER_COLL);
    }

	insert({ username, email, password, status, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!username || !email)
                    return resolve({ error: true, message: 'params_not_valid' });

                if(agency && !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'agency_params_not_valid' });

                let emailValid = email.toLowerCase().trim();
                let usernameValid = username.toLowerCase().trim();

                // let checkExists = await USER_COLL.findOne({
                //     $or: [
                //         { username: usernameValid },
                //         { email: emailValid },
                //     ]
                // });
                // if(checkExists)
                //     return resolve({ error: true, message: "name_or_email_existed" });

                let infoUserByEmail = await USER_COLL.findOne({ email });
                
                let infoUserByUsername = await USER_COLL.findOne({ username });

                if(infoUserByEmail){
                    return resolve({ error: true, message: 'email_existed' });
                }

                if(infoUserByUsername){
                    return resolve({ error: true, message: 'username_existed' });
                }

                let hashPassword;
                let dataInsert = {
                    username: usernameValid, 
                    email: emailValid,
                    status,
                    level
                }

                if(ObjectID.isValid(agency)){
					dataInsert.$addToSet = { agency };
                    password = randomStringFixLengthCode(6);
                    hashPassword = await hash(password, 8);
                    dataInsert.password = hashPassword;
                } else{
                    hashPassword = await hash(password, 8);
                    if (!hashPassword)
                        return resolve({ error: true, message: 'cannot_hash_password' });
                    dataInsert.password = hashPassword;
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_user_failed' });

                // Thêm vào danh sách owners của agency
                if(agency && ObjectID.isValid(agency) && +level === 1){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $addToSet: { owners: infoAfterInsert._id }
                    }, { new: true });

                    //Thêm vào agency của User
                    let { _id: userID } = infoAfterInsert;
                    await USER_COLL.findByIdAndUpdate(userID, {
                        $addToSet: { agency: agency }
                    });
                }

                // Thêm vào danh sách employees của agency
                if(agency && ObjectID.isValid(agency) && +level === 2){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $addToSet: { employees: infoAfterInsert._id }
                    }, { new: true });

                    //Thêm vào agency của User
                    let { _id: userID } = infoAfterInsert;
                    await USER_COLL.findByIdAndUpdate(userID, {
                        $addToSet: { agency: agency }
                    });
                }

                if(agency && ObjectID.isValid(agency)){
                    sendMailNewAccount(email, username, password);
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoUser = await USER_COLL.findById(userID);
                if(!infoUser)
                    return resolve({ error: true, message: "user_is_not_exists" });

                return resolve({ error: false, data: infoUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkEmailExisted({ email, username }) {
        return new Promise(async resolve => {
            try {

                let infoUserByEmail = await USER_COLL.findOne({ email })
                .populate("agency");
                let infoUserByUsername = await USER_COLL.findOne({ username });

                if(infoUserByEmail){
                    return resolve({ error: true, message: 'email_existed', data: infoUserByEmail });
                }else if(infoUserByUsername){
                    return resolve({ error: true, message: 'username_existed' });
                }else{
                    return resolve({ error: false, message: "can_use_account" });
                }
                
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ userID, email, username, password, oldPassword, status, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                if(agency && !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'param_not_valid' });

                const checkExists = await USER_COLL.findById(userID);
                if(!checkExists)
                    return resolve({ error: true, message: "user_is_not_exists" });

				if(oldPassword){
					const isMatchPass = await compare(oldPassword, checkExists.password);
					if (!isMatchPass) 
						return resolve({ error: true, message: 'old_password_wrong' });
				}

                const dataUpdateUser = {};
                email    && (dataUpdateUser.email       = email.toLowerCase().trim());
                username && (dataUpdateUser.username    = username.toLowerCase().trim());
                password && (dataUpdateUser.password    = hashSync(password, 8));
                status   && (dataUpdateUser.status      = status);
                agency   && (dataUpdateUser.$addToSet   = { agency });

                if([1,2].includes(+level)){
                    dataUpdateUser.level = level;
                }

                let infoUserUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, { new: true });

                if(infoUserUpdate){
                    password && delete infoUserUpdate.password;
                    if(Number(level) == 1){
                        await AGENCY_COLL.findByIdAndUpdate(agency, {
                            $addToSet: { owners: userID },
                            $pull: { employees: userID }
                        }, { new: true })
                    }
    
                    if(Number(level) == 2){
                        await AGENCY_COLL.findByIdAndUpdate(agency, {
                            $pull: { owners: userID },
                            $addToSet: { employees: userID }
                        }, { new: true })
                    }
                    return resolve({ error: false, data: infoUserUpdate });
                }else{
                    return resolve({ error: true, data: "cannot_update" });
                }
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	updateUserAgency({ userID, email, username, status, level, agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_invalid" });

                const checkExists = await USER_COLL.findById(userID);
                if(!checkExists)
                    return resolve({ error: true, message: "user_is_not_exists" });

                const dataUpdateUser = {};
                email    && (dataUpdateUser.email       = email.toLowerCase().trim());
                username && (dataUpdateUser.username    = username.toLowerCase().trim());

				if([1,2].includes(+level)){
                    dataUpdateUser.level = level;
                }

				if(status){
					status == 0 && await BLACKLIST_MODEL.insert({ agencyID, userID });
					status == 1 && await BLACKLIST_MODEL.delete({ agencyID, userID });
				}

                let infoUserUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, { new: true }).lean();

				if(!infoUserUpdate)
					return resolve({ error: true, message: 'cannot_update_user_agency' })

				switch (+level) {
					case 1:
						await AGENCY_COLL.findByIdAndUpdate(agencyID, {
							$addToSet: { owners: userID },
							$pull: { employees: userID }
						})
						break;
					case 2:
						await AGENCY_COLL.findByIdAndUpdate(agencyID, {
							$pull: { owners: userID },
							$addToSet: { employees: userID }
						})
						break;
					default:
						break;
				}
				delete infoUserUpdate.password;

				return resolve({ error: false, data: infoUserUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

				let infoAfterDelete = await USER_COLL.deleteOne({ 
                    $and: [
                        { _id: userID },
                        { level: 2 }
                    ]
                });

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "cannot_delete_user" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    deleteUser({ userID, level, agency, levelUserRemove }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

				let infoAfterDelete = null;

                //console.log({ levelUserRemove, agency });

				// switch (+level) {
				// 	case 0:
				// 		infoAfterDelete = await USER_COLL.findByIdAndRemove(userID);
                //         console.log("case 0");
				// 		break;
				// 	case 1:
				// 		infoAfterDelete = await USER_COLL.deleteOne({ 
				// 			_id: userID,
                //             level: 2
				// 		});
                //         console.log("case 1");
				// 		break;
				// 	default:
				// 		break;
				// }

                if(Number(levelUserRemove) == 1){
                    infoAfterDelete = await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $pull: { owners: userID }
                    })
                }

                if(Number(levelUserRemove) == 2){
                    infoAfterDelete = await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $pull: { employees: userID }
                    })
                }

                await USER_COLL.findByIdAndUpdate(userID, {
                    $pull: { agency: agency }
                }, {new: true});

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "cannot_delete_user" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListUser({ keyword, level, status }){
        return new Promise(async resolve => {
            try {
                const condition = { };
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { username: new RegExp(key, 'i') },
                        { email: new RegExp(key, 'i') }
                    ]
                }

				if(level){
					if(![1,2].includes(+level)){
						return resolve({ error: false, message: "cannot_get_list_admin" });
					}

					condition.level = level;
				}

                status && (condition.status = status);

                const listUsers = await USER_COLL.find(condition);
                if(!listUsers)
                    return resolve({ error: true, message: "not_found_accounts_list" });

                return resolve({ error: false, data: listUsers });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    signIn({ email, password }) {
        return new Promise(async resolve => {
            try {
                console.log({
                    email, password
                });
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim() });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                const isMatchPassword = await compare(password, checkExists.password);
                if (!isMatchPassword) 
                    return resolve({ error: true, message: 'password_is_wrong' });

                if (checkExists.status == 0) 
                    return resolve({ error: true, message: 'user_blocked' });

                if (!checkExists.agency || !checkExists.agency.length)
                    return resolve({ error: true, message: 'user_not_inside_agency' });

				// let listAgency = checkExists.agency;
				// let listBlacklistOfUser = await BLACKLIST_COLL
				// 	.find({ user: checkExists._id })
				// 	.select('agency')
				// 	.lean();

				// listBlacklistOfUser = listBlacklistOfUser.map(blacklist => blacklist.agency.toString());
				// let listAgencyAfterRemoveBlackList = [];
                // listAgency.map((agencyID, index) => {
				// 	if(!listBlacklistOfUser.includes(agencyID.toString())){
				// 		listAgencyAfterRemoveBlackList.push(agencyID);
				// 	}
				// })

                // listAgency = await AGENCY_COLL
				// 	.find({ _id: { $in: listAgencyAfterRemoveBlackList }, status: 1 })
				// 	.lean();

                // if(!listAgency.length)
                //     return resolve({ error: true, message: 'user_not_inside_agency' });
                // let listAgencyID = listAgency.map( agency => agency._id);
                // console.log({ listAgencyID });
                const infoUser = {
                    _id: checkExists._id,
                    username: checkExists.username,
                    email: checkExists.email,
                    status: checkExists.status,
                    level: checkExists.level,
                    // agency: listAgency 
                }
                const token = jwt.sign(infoUser, cfJWS.secret);
                // console.log({ token });
                return resolve({
                    error: false,
                    data: { user: infoUser, token }
                });
            } catch (error) {
				console.error(error);
                return resolve({ error: true, message: error.message });
            }
        })
    }

    signinWithMobile({ email, password, deviceID, deviceName, registrationID }) {
        return new Promise(async resolve => {
            try {
                const isExist = await USER_COLL.findOne({ email: email.toLowerCase().trim() }).lean();
                if (!isExist) 
                    return resolve({ error: true, message: 'email_not_exist' });

                const isMatchPassword = await compare(password, isExist.password);
                if (!isMatchPassword) 
                    return resolve({ error: true, message: 'password_is_wrong' });

                if (isExist.status == 0) 
                    return resolve({ error: true, message: 'user_blocked' });

                if (!isExist.agency.length)
                    return resolve({ error: true, message: 'user_not_inside_agency' });

                let listAgency = isExist.agency;
                let listBlacklistOfUser = await BLACKLIST_COLL
                    .find({ user: isExist._id })
                    .select('agency')
                    .lean();

                listBlacklistOfUser = listBlacklistOfUser.map(blacklist => blacklist.agency.toString());
                let listAgencyAfterRemoveBlackList = [];
                listAgency.map((agencyID, index) => {
                    if(!listBlacklistOfUser.includes(agencyID.toString())){
                        listAgencyAfterRemoveBlackList.push(agencyID);
                    }
                })

              
                listAgency = await AGENCY_COLL
                    .find({ _id: { $in: listAgencyAfterRemoveBlackList }, status: 1 })
                    .lean()
                    .populate({
                        path : "avatar gallery",
                        select: "_id path",
                    });
                
                if(!listAgency.length)
                    return resolve({ error: true, message: 'user_not_inside_agency' });

                // if(isExist.agency && isExist.agency.length){
                //     listAgency = await AGENCY_COLL
                //         .find({ _id: { $in: isExist.agency }, status: 1 })
                //         .lean()
                //         .populate({
                //             path : "avatar gallery",
                //             select: "_id path",
                //         })
                // }
                // console.log({ listAgency });
                let  infoUser = {
                    _id: isExist._id,
                    username: isExist.username,
                    email: isExist.email,
                    status: isExist.status,
                    level: isExist.level,
                    // agency: listAgency
                }
                const token = jwt.sign(infoUser, cfJWS.secret);

                infoUser = {
                    ...infoUser,
                    agency: listAgency
                }
                /**
                 * UPDATE LAST DEVICE (cập nhật device cuối cùng login)
                 * Nếu DeviceID tồn tại => cập nhật deviceName + fcm_token
                 */
                 let devices     = isExist.devices || [];
                 let newDevices  = [];
                 
                 let itemNewDevice = { deviceName, deviceID, registrationID }
                 if (devices.length) {
                     devices.forEach(device => {
                         let _device = device;
                         if (device.deviceID == itemNewDevice.deviceID) {
                             _device = {
                                 deviceID :  device.deviceID, 
                                 deviceName: device.deviceName, 
                                 registrationID : registrationID
                             };
                         } 
                         newDevices.push(_device);
                     });
                     if (!newDevices.find(item => item.deviceID === itemNewDevice.deviceID)) 
                         newDevices.push(itemNewDevice)
                 } else {
                     newDevices = [
                         itemNewDevice
                     ]
                 }
                //--- END UPDATE DEVICES ---
                let infoAfterUpdateWithRegistration = await USER_COLL.findByIdAndUpdate(isExist._id, {
                    devices :  newDevices, 
                    modifyAt: Date.now(),
                    lastLog: Date.now()
                }, { new: true });

                return resolve({
                    error: false,
                    data: { user: infoAfterUpdateWithRegistration, token, infoUser }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateToOwner({ userID, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(agency))
                    return resolve({ error: true, message: "param_not_valid" });

                let dataUpdateUser = {
                    level
                };

                let infoUserUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, { new: true });
                
                // Đẩy ra khỏi mảng Employees
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $pull: { employees: userID }
                }, { new: true })

                // Thêm vào mảng Owners
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $addToSet: { owners: userID }
                }, { new: true })
               
                return resolve({ error: false, data: infoUserUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateToEmployee({ userID, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(agency))
                    return resolve({ error: true, message: "param_not_valid" });

                let dataUpdateUser = {
                    level
                };

                let infoUserUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, { new: true });
                
                // Đẩy ra khỏi mảng Owners
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $pull: { owners: userID }
                }, { new: true })

                // Thêm vào mảng Employees
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $addToSet: { employees: userID }
                }, { new: true })
               
                return resolve({ error: false, data: infoUserUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkAuth({ token }) {
        return new Promise(async resolve => {
            try {
                let decoded = await jwt.verify(token, cfJWS.secret);
                if (!decoded) return resolve({ error: true, message: 'token_not_valid' });

                const { _id: userID } = decoded;
                
                if (!userID) return resolve({ error: true, message: 'token_not_valid' });

                const STATUS_ACTIVE_OF_USER = 1;
                let infoUserDB = await USER_COLL.findOne({ status: STATUS_ACTIVE_OF_USER, _id: userID });

                if (!infoUserDB) 
                    return resolve({ error: true, message: 'token_not_valid' });

                return resolve({ error: false, data: infoUserDB });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    forgetPassword({ email }) {
        return new Promise(async resolve => {
            try {
                const LEVEL_OWNER    = 1;
                const LEVEL_EMPLOYEE = 2;
                const ARR_LEVER_ACCESS = [LEVEL_OWNER, LEVEL_EMPLOYEE];
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim(), level: { $in: ARR_LEVER_ACCESS } });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                let codeReset = randomStringFixLengthCode(6);

                let infoCodeAfterInsert =  await CODE_RESET_PASSWORD_MODEL.insert({ email, code: codeReset });

                if (infoCodeAfterInsert.error) {
                    return resolve(infoCodeAfterInsert);
                }

                sendMailForgetPassword(email, codeReset);

                return resolve(infoCodeAfterInsert);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkCodeForgetPassword({ email, code }) {
        return new Promise(async resolve => {
            try {
                const LEVEL_OWNER    = 1;
                const LEVEL_EMPLOYEE = 2;
                const ARR_LEVER_ACCESS = [LEVEL_OWNER, LEVEL_EMPLOYEE];
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim(), level: { $in: ARR_LEVER_ACCESS } });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });


                let infoCode =  await CODE_RESET_PASSWORD_MODEL.checkCode({ email, code });

                if (infoCode.error) {
                    return resolve(infoCode);
                }

                return resolve(infoCode);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateForgetPassword({ email, code, password }) {
        return new Promise(async resolve => {
            try {
                const LEVEL_OWNER    = 1;
                const LEVEL_EMPLOYEE = 2;
                const ARR_LEVER_ACCESS = [LEVEL_OWNER, LEVEL_EMPLOYEE];
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim(), level: { $in: ARR_LEVER_ACCESS } });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });


                if (!password) {
                    return resolve({ error: true, message: 'password_invalid' });
                }
                let infoCode = await CODE_RESET_PASSWORD_COLL.findOne({ email, code })
                if(!infoCode) {
                    return resolve({ error: true, message: 'cannot_update_password_user' });
                }
                let passwordNew = hashSync(password, 8);

                let infoUserAfterUpdate = await USER_COLL.findOneAndUpdate({ email }, {
                    password: passwordNew
                });

                if (!infoUserAfterUpdate) {
                    return resolve({ error: true, message: 'cannot_update_password_user' });
                }

                return resolve({ error: false, data: email });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAgencyOfUser({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                let listAgencyOfUser = await USER_COLL.findById(userID);
                let listAgency = listAgencyOfUser.agency;

				let listBlacklistOfUser = await BLACKLIST_COLL
					.find({ user: listAgencyOfUser._id })
					.select('agency')
					.lean();

				listBlacklistOfUser = listBlacklistOfUser.map(blacklist => blacklist.agency.toString());
				
                let listAgencyAfterRemoveBlackList = [];
                listAgency.map((agencyID, index) => {
					if(!listBlacklistOfUser.includes(agencyID.toString())){
						listAgencyAfterRemoveBlackList.push(agencyID);
					}
				})
                listAgencyAfterRemoveBlackList = listAgencyAfterRemoveBlackList.map( agency => ObjectID(agency));
                let obj = {
                    _id: { 
                        $in: [...listAgencyAfterRemoveBlackList] 
                    }, 
                    status: 1 
                }

                let listAgencyAfterFind = await AGENCY_COLL.find({ 
                        ...obj
                    })
					.lean();

                if (!listAgencyAfterFind) {
                    return resolve({ error: true, message: 'cannot_find_agency_user' });
                }

                return resolve({ error: false, data: listAgencyAfterFind });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
