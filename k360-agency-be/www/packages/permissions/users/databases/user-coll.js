"use strict";

const Schema    	= require('mongoose').Schema;
const BASE_COLL  	= require('../../../../databases/intalize/base-coll');

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'user' }){
    return BASE_COLL(name, {
        username: {
			type: String,
			trim: true,
			unique : true
		}, 
		email: {
			type: String,
			trim: true,
			unique : true
		},
		password: {
			type: String
		},
		/**
		 * Nhóm đại lý mà user là member (owner, employee)
		 */
		agency: [{
			type: Schema.Types.ObjectId,
			ref: 'agency'
		}],
		/**
		 * Trạng thái hoạt động.
		 * 1. Hoạt động
		 * 0. Khóa
		 */
		status: {
			type: Number,
			default: 1
		},
		/**
		 * Phân quyền truy cập.
		 * 0. ADMIN
		 * 1. OWNER
		 * 2. EMPLOYEE
		 */
		level: {
			type: Number,
			default: 1
		},
		devices: [{
			deviceName      :{ type : String }, 
			deviceID        :{ type : String },
			registrationID  :{ type : String }
		}],
		...fields
    });
}
