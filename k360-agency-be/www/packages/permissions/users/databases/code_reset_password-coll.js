"use strict";
const Schema    	= require('mongoose').Schema;
const BASE_COLL  	= require('../../../../databases/intalize/base-coll');

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'code_reset_password' }){
    return BASE_COLL(name, {
		email: {
			type: String,
			trim: true,
		},
		code: {
			type: String,
			unique : true
		},
        /**
         * Vô hiệu hóa sau 15p
         */
		timeLife: {
			type: Date,
		},
		...fields
    });
}
