const BASE_ROUTE_AGENCY = '/agency';

const CF_ROUTINGS_AGENCY = {
    // ADD_ACCOUNT: TẠO TÀI KHOẢN - mặc định member (vì chỉ owner được tạo bởi admin cấp)
    /**
     * b1: Check user tạo có phải là owner của agency đó?
     * b2: Tạo user với Password Random
     */
    ADD_ACCOUNT: `${BASE_ROUTE_AGENCY}/add-account`, 
    ADD_EMPLOYEE: `${BASE_ROUTE_AGENCY}/add-employee`, 
    ADD_EMPLOYEE_AGENCY_BRANCH: `${BASE_ROUTE_AGENCY}/add-employee-agency-branch`,
    ADD_OWNER: `${BASE_ROUTE_AGENCY}/add-owner`,
    INFO_ACCOUNT: `${BASE_ROUTE_AGENCY}/info-account`,
    INFO_USER: `${BASE_ROUTE_AGENCY}/info-user/:employeeID`, 
    INFO_USER_UPDATE: `${BASE_ROUTE_AGENCY}/info-user-update`,
    UPDATE_USER: `${BASE_ROUTE_AGENCY}/update-user`, 
    UPDATE_USER_AGENCY_BRANCH: `${BASE_ROUTE_AGENCY}/update-user-agency-branch`,
    UPDATE_TO_EMPLOYEE: `${BASE_ROUTE_AGENCY}/update-to-employee`,
    UPDATE_TO_OWNER: `${BASE_ROUTE_AGENCY}/update-to-owner`,
    INFO_USER_BY_EMAIL: `${BASE_ROUTE_AGENCY}/info-user-by-email`, 
    DELETE_USER: `${BASE_ROUTE_AGENCY}/delete-user`,
    ORIGIN_APP: BASE_ROUTE_AGENCY
}

exports.CF_ROUTINGS_AGENCY = CF_ROUTINGS_AGENCY;