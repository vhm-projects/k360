"use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../../routing/child_routing');
const roles                                         = require('../../../../config/cf_role');
const { CF_ROUTINGS_AGENCY }                        = require('../constant/user.uri');
let path                                            = require('path');
/**
 * MODELS
 */
 const USER_MODEL = require('../models/user').MODEL;
const { AGENCY_COLL, AGENCY_MODEL } = require('../../../agency');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ TÀI KHOẢN AGENCY ================================
             * ========================== ************************ ================================
             */

            [CF_ROUTINGS_AGENCY.ADD_ACCOUNT]: {
                config: {
                    // TODO update theo chỉ owner nhân viên mới cho phép tạo
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { username, email, password, status, level, agency } = req.body;

                        const infoAfterInsertAccount = await USER_MODEL.insert({ 
                            username, email, password, status, level, agency
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

            /**
             * Function: tạo user employee (permission: admin, owner) (API)
             * Date: 01/04/2021
             * Update:
             *  + 17/04/2021 Dattv: Thêm field agency để đăng ký tài khoản admin cho agency
             * Dev: MinhVH
             */
             [CF_ROUTINGS_AGENCY.ADD_EMPLOYEE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const LEVEL_ADMIN = 0;
                        const LEVEL_OWNER = 1;
                        const { level } = req.user;
                        const agencyID = req.agencyID;
                        const { username, email } = req.body;
                        
                        if(level == LEVEL_ADMIN || level == LEVEL_OWNER){
                            const infoAfterInsertAccount = await USER_MODEL.insert({ 
                                username, email, level: 2, agency: agencyID
                            });
    
                            // console.log({ infoAfterInsertAccount });
                            res.json(infoAfterInsertAccount);
                        }else{
                            res.json({ error: true, message: "can_not_access"});
                        }
                    }]
                },
            },

             [CF_ROUTINGS_AGENCY.ADD_EMPLOYEE_AGENCY_BRANCH]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const LEVEL_ADMIN = 0;
                        const LEVEL_OWNER = 1;
                        const { level } = req.user;
                        const { username, email, agency } = req.body;
                        if(level == LEVEL_ADMIN || level == LEVEL_OWNER){
                            const infoAfterInsertAccount = await USER_MODEL.insert({ 
                                username, email, level: 2, agency
                            });
                            
                            res.json(infoAfterInsertAccount);
                        }else{
                            res.json({ error: true, message: "can_not_access"});
                        }
                    }]
                },
            },

			[CF_ROUTINGS_AGENCY.INFO_ACCOUNT]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: userID } = req.user;

                        const infoAccount = await USER_MODEL.getInfo({ userID });
                        res.json(infoAccount);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.INFO_USER]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { employeeID } = req.params;
                        const infoAccount = await USER_MODEL.getInfo({ userID: employeeID });
                        res.json(infoAccount);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.INFO_USER_UPDATE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { level } = req.user;
                        const { userID } = req.query;

                        const infoUser = await USER_MODEL.getInfo({ userID, level });
                        res.json(infoUser);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.INFO_USER_BY_EMAIL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email, username } = req.body;
                        const infoUser = await USER_MODEL.checkEmailExisted({ email, username });
                        res.json(infoUser);
                    }]
                },
            },

			[CF_ROUTINGS_AGENCY.UPDATE_USER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let agencyID  = req.agencyID;
                        const { userID, email, username, password, oldPassword, status, level } = req.body;
                        const infoAfterAdd = await USER_MODEL.update({ 
                            userID, email, username, password, oldPassword, status, level, agency: agencyID
                        });
                        res.json(infoAfterAdd);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.UPDATE_USER_AGENCY_BRANCH]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, email, username, status, level, agencyID } = req.body;

                        const infoAfterUpdate = await USER_MODEL.updateUserAgency({ 
                            userID, email, username, status, level, agencyID
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.DELETE_USER]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { level } = req.user;
                        const { userID, agency, levelUserRemove } = req.query;

                        const infoAfterDelete = await USER_MODEL.deleteUser({ userID, level, agency, levelUserRemove });
                        res.json(infoAfterDelete);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.ADD_OWNER]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const LEVEL_ADMIN = 0;
                        const LEVEL_OWNER = 1;
                        let { level } = req.user;
                        const { username, email, password, status, agency } = req.body;

                        if(Number(level) == LEVEL_ADMIN || Number(level) == LEVEL_OWNER){
                            const infoAfterInsertAccount = await USER_MODEL.insert({ 
                                username, email, password, status, level: 1, agency
                            });
                            res.json(infoAfterInsertAccount);
                        }else{
                            res.json({ error: true, message: "employee_cannot_access"});
                        }
                        
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.UPDATE_TO_EMPLOYEE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, agency } = req.body;
                        const infoUserUpdate = await USER_MODEL.updateToEmployee({ userID, level: 2, agency });
                        res.json(infoUserUpdate);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.UPDATE_TO_OWNER]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, agency } = req.body;
                        const infoUserUpdate = await USER_MODEL.updateToOwner({ userID, level: 1, agency });
                        res.json(infoUserUpdate);
                    }]
                },
            },

        }
    }
};
