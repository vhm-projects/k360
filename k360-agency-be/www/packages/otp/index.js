const OTP_MODEL 		= require('./models/otp');
const OTP_COLL  		= require('./database/otp');
const OTP_ROUTES        = require('./apis/otp');

module.exports = {
    OTP_ROUTES,
    OTP_COLL,
    OTP_MODEL: OTP_MODEL.MODEL,
	// CUSTOMER_MODEL_BASE: CUSTOMER_MODEL.AccountModel
}
