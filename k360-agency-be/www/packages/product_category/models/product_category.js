"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS, COLLECTIONS
 */
const PRODUCT_CATEGORY_COLL         = require('../databases/product-category-coll')({});
const AGENCY_PRODUCT_CATEGORY_MODEL = require('../../agency_product_category/models/agency_product_category').MODEL;


class Model extends BaseModel {
    constructor() {
        super(PRODUCT_CATEGORY_COLL);
    }

    getInfo({ categoryID }){
        return new Promise(async resolve => {
            try {
                if(!categoryID || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const infoProductCategory = await PRODUCT_CATEGORY_COLL.findById(categoryID).populate('image');

                if(!infoProductCategory)
                    return resolve({ error: true, message: 'get_info_product_category_failed' });

                return resolve({ error: false, data: infoProductCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ status = 1, parent = null }){
        return new Promise(async resolve => {
            try {
                const listProductCategory = await PRODUCT_CATEGORY_COLL
                    .find({ status, parent })
                    .populate('image')
                    .lean();

                if(!listProductCategory)
                    return resolve({ error: true, message: 'get_list_product_category_failed' });

                return resolve({ error: false, data: listProductCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListV2({ agencyID, status = 1, parent = null }){
        return new Promise(async resolve => {
            try {
                const listProductCategory = await AGENCY_PRODUCT_CATEGORY_MODEL.getListV3({ agencyID });

                if(!listProductCategory)
                    return resolve({ error: true, message: 'get_list_product_category_failed' });
                // console.log({ listProductCategory });
                return resolve(listProductCategory);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByParent({ agencyID, status, parent = null }){
        return new Promise(async resolve => {
            try {
                const conditionGetList = { parent };
                status && (conditionGetList.status = status);

                const listChildsCategory = await PRODUCT_CATEGORY_COLL
                    .find(conditionGetList)
                    .populate('image')
                    .lean();

                if(!listChildsCategory)
                    return resolve({ error: true, message: 'cannot_get_list_product_category' });

                let listAgencyCategory  = await AGENCY_PRODUCT_CATEGORY_MODEL.getList({ agencyID });
                let listProductCategory = [];

                listChildsCategory.map(category => {
                    let checkExistAndLength = listAgencyCategory.data && listAgencyCategory.data.length;
                    let agencyCategory      = checkExistAndLength && listAgencyCategory.data.find(agencyCategory => 
                        category._id.toString() === agencyCategory.product_category.toString()
                    );

                    listProductCategory = [...listProductCategory, {
                        ...category,
						priceAgency: agencyCategory ? agencyCategory.price : category.price,
                        statusAgency: agencyCategory ? agencyCategory.status : 0,
                        interestAgency: agencyCategory ? agencyCategory.interestAgency : 5,
                        percentValueAgency: agencyCategory ? agencyCategory.percentValue : category.percentValue,
                    }];
                })

				if(parent){
					let breadcrumb = '';
					(async function recursiveParent(parent){
						if(parent){
							const infoParent = await PRODUCT_CATEGORY_COLL
								.findById(parent)
								.select('name parent')
								.lean();
							breadcrumb += ` / ${infoParent.name}`;
							recursiveParent(infoParent.parent);
						} else{
							breadcrumb = breadcrumb.split('/').reverse().join(' / ');
							return resolve({ error: false, listProductCategory, parentCategory: breadcrumb });
						}
					})(parent);
				} else{
					return resolve({ error: false, listProductCategory, parentCategory: null });
				}
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListActiveByParent({ agencyID, parentCategoryID = null }){
        return new Promise(async resolve => {
            try {
                if(parentCategoryID && !ObjectID.isValid(parentCategoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let listChildsCategory = await PRODUCT_CATEGORY_COLL
                    .find({ parent: parentCategoryID, status: 1 })
                    .populate('image')
                    .lean();

                let infoParent = await PRODUCT_CATEGORY_COLL
                    .findOne({ _id: parentCategoryID, status: 1 })
                    .populate('image')
                    .lean();

                if(!listChildsCategory)
                    return resolve({ error: true, message: 'cannot_get_list_product_category' });

                let listAgencyCategory = await AGENCY_PRODUCT_CATEGORY_MODEL.getList({ agencyID });
                let listProductCategoryWithAgencyCategory = [];

                listChildsCategory.map(category => {
                    let checkExistAndLength = listAgencyCategory.data && listAgencyCategory.data.length;
                    let agencyCategory      = checkExistAndLength && listAgencyCategory.data.find(agencyCategory => 
                        category._id.toString() === agencyCategory.product_category.toString()
                    );

                    listProductCategoryWithAgencyCategory = [...listProductCategoryWithAgencyCategory, {
                        ...category,
                        priceAgency: agencyCategory ? agencyCategory.price : 0,
                        statusAgency: agencyCategory ? agencyCategory.status : 1
                    }];
                })

                return resolve({ 
                    error: false, 
                    infoParent,
                    listChildsCategory: listProductCategoryWithAgencyCategory
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListActiveByParentV2({ agencyID, parentCategoryID = null }){
        return new Promise(async resolve => {
            try {
                if(parentCategoryID && !ObjectID.isValid(parentCategoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let listChildsCategory = await PRODUCT_CATEGORY_COLL
                    .find({ parent: parentCategoryID, status: 1 })
                    .populate('image')
                    .lean();

                let infoParent = await PRODUCT_CATEGORY_COLL
                    .findOne({ _id: parentCategoryID, status: 1 })
                    .populate('image')
                    .lean();
                
                if(!listChildsCategory)
                    return resolve({ error: true, message: 'cannot_get_list_product_category' });

                let listAgencyCategory = await AGENCY_PRODUCT_CATEGORY_MODEL.getListV2({ agencyID });
                let listProductCategoryWithAgencyCategory = [];
                
                listChildsCategory.map(category => {
                    let checkExistAndLength = listAgencyCategory.data && listAgencyCategory.data.length;
                    let agencyCategory      = checkExistAndLength && listAgencyCategory.data.find(agencyCategory => 
                        category._id.toString() === agencyCategory.product_category.toString()
                    );
                    // let agencyCategory ;
                    // let dropDown = [];
                    // checkExistAndLength && listAgencyCategory.data.forEach(agencyCategoryChild => {
                    //     if (category._id.toString() === agencyCategoryChild.product_category.toString()) {
                    //         agencyCategory = agencyCategoryChild;
                    //         dropDown       = agencyCategoryChild.dropDown
                    //     }
                        
                    // });

                    /**
                     * THÊM percentValue 
                     * AUTHOR: SONLP
                     */
                    if (agencyCategory && agencyCategory.dropDown && agencyCategory.dropDown.length) {
                        category.dropDown = agencyCategory.dropDown;
                    }

                    listProductCategoryWithAgencyCategory = [...listProductCategoryWithAgencyCategory, {
                        ...category,
                        priceAgency: agencyCategory ? (agencyCategory.price ? agencyCategory.price : 0) : 0,
                        statusAgency: agencyCategory ? agencyCategory.status : 1,
                        percentValue: agencyCategory ? agencyCategory.percentValue : 0,
                    }];
                })

                return resolve({ 
                    error: false, 
                    infoParent,
                    listChildsCategory: listProductCategoryWithAgencyCategory
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
