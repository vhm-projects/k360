const BASE_ROUTE = '/product-category';

const CF_ROUTINGS_PRODUCT_CATEGORY = {
    // PRODUCT CATEGORY
    ADD_PRODUCT_CATEGORY: `${BASE_ROUTE}/add-product-category`,
    UPDATE_PRODUCT_CATEGORY: `${BASE_ROUTE}/update-product-category`,
    DELETE_PRODUCT_CATEGORY: `${BASE_ROUTE}/delete-product-category`,
    INFO_PRODUCT_CATEGORY: `${BASE_ROUTE}/info-product-category`,

    LIST_PRODUCT_CATEGORY: `${BASE_ROUTE}/list-product-category`,
    LIST_PRODUCT_CATEGORY_BY_PARENT: `${BASE_ROUTE}/list-product-category-by-parent`,
    LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT: `${BASE_ROUTE}/list-child-by-parent`,
    LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT_V2: `${BASE_ROUTE}/list-child-by-parent-v2`,

    LIST_PRODUCT_CATEGORY_V2: `${BASE_ROUTE}/list-product-category-v2`,

    //API MOBILE
    LIST_PRODUCT_CATEGORY_API: `/api${BASE_ROUTE}/list-product-category`,
    LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT_API: `/api${BASE_ROUTE}/list-child-by-parent`,


    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PRODUCT_CATEGORY = CF_ROUTINGS_PRODUCT_CATEGORY;
