"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION PRODUCT CATEGORY CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'product_category' }){
	return BASE_COLL(name, {
        name: {
            type: String,
            require: true
        },
        description: {
            type: String,
            default: ''
        },
        parent: {
            type: Schema.Types.ObjectId,
            ref: 'product_category',
			default: null
        },
        childs: [{
            type: Schema.Types.ObjectId,
            ref: 'product_category'
        }],
        textParent: {
            type: String,
            default: ''
        },
        image: {
            type: Schema.Types.ObjectId,
            ref: 'image'
        },
		//  Giá sản phẩm
        price: {
            type: Number,
            default: 0
        },
		/**
		 * Kiểu danh mục đặc biệt:
		 * 0: Danh mục bình thường
		 * 2: Vàng nguyên liệu
		 * 3: Trang sức
		 * 4: Non brand
		 * 5: Không có hóa đơn
		 * 6: Danh mục cuối
		 */
		specialCategory: {
			type: Number,
			default: 0
		},
		// Giá trị phần trăm của sản phẩm
		percentValue: {
			type: Number,
			default: 0
		},
		textField: {
			text: {
				type: String,
				default: ''
			},
			/**
			 * Kiểu input nhập:
			 * 0: Có hóa đơn
			 * 1: Trọng lượng vàng
			 * 2: Số ly
			 */
			type: {
				type: Number,
				default: 0
			}
		},
		dropDown: [{
			text: {
				type: String,
				default: ''
			},
			value: {
				type: Number,
				default: 0
			}
		}],
        /**
         * 0: Checkbox
         * 1: Button
         * 2: Textbox
         * 3: Dropdown
         */
        type: {
            type: Number,
            default: 1
        },
        /**
         * 0: Không hiển thị
         * 1: hiển thị
         * 2: Tạm xóa
         */
        status: {
            type: Number,
            default: 1
        },

		// FOR ENDUSER
		content: {
            type: String,
            default: ''
        },
		// Nhãn hiệu
        brands: [{
            type: String,
        }],
        // Người tạo
        userCreate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
        // Người cập nhật
        userUpdate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
		...fields
    });
}
