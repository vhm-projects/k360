"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_PRODUCT_CATEGORY } 				= require('../constants/product_category.uri');

/**
 * MODELS
 */
const PRODUCT_CATEGORY_MODEL            = require('../models/product_category').MODEL;
const { IMAGE_MODEL }                   = require('../../image');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ PRODUCT CATEGORY ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Chi tiết product category (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.INFO_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryID } = req.query;

                        const infoProductCategory = await PRODUCT_CATEGORY_MODEL.getInfo({ categoryID });
                        res.json(infoProductCategory);
                    }]
                },
            },

            /**
             * Function: LIST PRODUCT CATEGORY (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { parent, status } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ parent, status });
                        res.json(listProductCategory.data);
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY_V2]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;

                        let { parent, status, type } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getListV2({ agencyID, parent, status });
                        
                        if(type && type === 'API'){
                            return res.json({
                                error: false,
                                listChildsCategory: listProductCategory.data,
                                infoParent: null,
                            });
                        }
                        res.json(listProductCategory.data);
                    }]
                },
            },

            /**
             * Function: LIST PRODUCT CATEGORY (API) MOBILE
             * SONLP
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { parent, status } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ parent, status });
                        res.json({ listProductCategory: listProductCategory.data});
                    }]
                },
            },

            /**
             * Function: LIST PRODUCT CATEGORY BY PARENT (VIEW, API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY_BY_PARENT]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'view',
					title: 'List Product Category - K360',
					code: CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY_BY_PARENT,
					inc: path.resolve(__dirname, '../views/list_product_category.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { parent, type } = req.query;

                        let listProductCategoryWithAgencyCategory = await PRODUCT_CATEGORY_MODEL.getListByParent({ 
                            parent, status: 1, agencyID
                        });

                        if(type && type === 'API'){
                            return res.json(listProductCategoryWithAgencyCategory);
                        }

                        ChildRouter.renderToView(req, res, {
                            ...listProductCategoryWithAgencyCategory,
                            CF_ROUTINGS_PRODUCT_CATEGORY
						});
                    }]
                },
            },

            /**
             * Function: LIST PRODUCT CATEGORY ACTIVE BY PARENT (VIEW, API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { parentCategoryID, type } = req.query;

                        let listProductCategoryWithAgencyCategory = await PRODUCT_CATEGORY_MODEL.getListActiveByParent({ 
                            parentCategoryID, agencyID
                        });

                        if(type && type === 'API'){
                            return res.json(listProductCategoryWithAgencyCategory);
                        }

                        // ChildRouter.renderToView(req, res, {
                        //     ...listProductCategoryWithAgencyCategory,
                        //     // infoParent,
						// 	// listChildsCategory: listProductCategoryWithAgencyCategory,
                        //     CF_ROUTINGS_PRODUCT_CATEGORY
						// });
                    }]
                },
            },

            [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT_V2]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { parentCategoryID, type } = req.query;

                        let listProductCategoryWithAgencyCategory = await PRODUCT_CATEGORY_MODEL.getListActiveByParentV2({ 
                            parentCategoryID, agencyID
                        });
                        // console.log({ listProductCategoryWithAgencyCategory });
                        if(type && type === 'API'){
                            return res.json(listProductCategoryWithAgencyCategory);
                        }

                        // ChildRouter.renderToView(req, res, {
                        //     ...listProductCategoryWithAgencyCategory,
                        //     // infoParent,
						// 	// listChildsCategory: listProductCategoryWithAgencyCategory,
                        //     CF_ROUTINGS_PRODUCT_CATEGORY
						// });
                    }]
                },
            },

			/**
             * Function: LIST PRODUCT CATEGORY ACTIVE BY PARENT (API) MOBILE
             * SONLP
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { parentCategoryID, type } = req.query;

						let listProductCategoryWithAgencyCategory = await PRODUCT_CATEGORY_MODEL.getListActiveByParent({ 
							parentCategoryID, agencyID
						});

                        if(type && type === 'API'){
                            return res.json({listProductCategoryWithAgencyCategory});
                        }

                        // ChildRouter.renderToView(req, res, {
                        //     ...listProductCategoryWithAgencyCategory,
                        //     // infoParent,
                        // 	// listChildsCategory: listProductCategoryWithAgencyCategory,
                        //     CF_ROUTINGS_PRODUCT_CATEGORY
                        // });
                    }]
                },
            },

        }
    }
};
