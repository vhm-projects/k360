
"use strict";

/**
 * EXTERNAL PACKAGE
 */
const uuidv4 = require('uuid').v4;

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                          = require('../../../routing/child_routing');
const roles                                = require('../../../config/cf_role');
const { CF_ROUTINGS_UPLOAD_S3 }            = require('../constants');
const { GENERATE_LINK_S3 }                 = require('../utils');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ********** ================================
             * ========================== QUẢN LÝ S3 ================================
             * ========================== ********** ================================
             */

            [CF_ROUTINGS_UPLOAD_S3.GENERATE_LINK_S3]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let fileName = uuidv4();
                        let type 	 = req.query.type;
                        let fileNameExtension = req.query.fileName.split(".")[1].toLowerCase();;
                        fileName = `${fileName}.${fileNameExtension}`;
                        
                        let linkUpload  = await GENERATE_LINK_S3(fileName, type);
                        // console.log({ pathFile, linkUpload , path });
                        if(linkUpload.error)
                            return res.json({error: true, message: "cannot_create_link"});

                        return res.json({ error: false, linkUpload, fileName });
                    }]
                }
            },
        }
    }
};
