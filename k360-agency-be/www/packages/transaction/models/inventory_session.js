"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const moment = require("moment");

/**
 * INTERNAL PAKCAGE
 */

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const INVENTORY_SESSION_COLL                = require('../database/inventory_session-coll')({});
const TRANSACTION_COLL                      = require('../../transaction/database/transaction-coll')({});
const HISTORY_CHECK_COLL                      = require('../../transaction/database/history_check-coll')({});

class Model extends BaseModel {
    constructor() {
        super(INVENTORY_SESSION_COLL);
    }

    /**
     * Tạo phiên kiểm kê
     * Update: 10/09/2021
     * Dev: Dattv
     */
	insert({ name, description, agencyID, userID, fromDate, endDate }) {
        return new Promise(async resolve => {
            try {
                if (!name || !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let conditionObj = { };

                if(fromDate && endDate){
                    let _fromDate   = moment(fromDate).startOf('day').format();
                    let _toDate     = moment(endDate).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate)
                    }
                }

                let listTransaction = await TRANSACTION_COLL.find({...conditionObj, status: 1, agency: agencyID})

                if(listTransaction.length == 0){
                    return resolve({ error: true, message: 'cannot_transaction_between_time' });
                }
                let arrTransactionID = listTransaction.map(item => item._id);

                let dataInsert = { 
                    name,
                    description,
                    agency: agencyID,
                    fromDate, endDate,
                    userID: ObjectID(userID)
                };

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                let { _id } = infoAfterInsert;
                infoAfterInsert = await INVENTORY_SESSION_COLL.findByIdAndUpdate(_id, {
                    $addToSet: {
                        transactions: {
                            $each: arrTransactionID
                        }
                    }
                }, { new: true });

                return resolve({ error: false, data: infoAfterInsert, message: 'success' });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chi tiết phiên giao dịch thuộc Agency
     * Dev: Dattv
     */
     getInfo({ inventorySessionID }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(inventorySessionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoInventorySession = await INVENTORY_SESSION_COLL.findById(inventorySessionID).select("name")
                
                if(!infoInventorySession)
                    return resolve({ error: true, message: 'cannot_get_info' });

                return resolve({ error: false, data: infoInventorySession });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách phiên giao dịch thuộc Agency
     * Dev: Dattv
     */
    getList({ agencyID, start, end }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let conditionObj = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let listInventoryOfAgency = await INVENTORY_SESSION_COLL.find({ agency: agencyID, ...conditionObj })
                // .populate({ 
                //     path: "transactions",
                //     select: "_id typeInventory historyCheck",
                // })
                .sort({ createAt: -1 });
                
                if(!listInventoryOfAgency)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listInventoryOfAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách giao dịch thuộc phiên kiểm kê
     * Dev: Dattv
     */
     getListTransactionOfInventorySession({ inventorySessionID }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(inventorySessionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoInventorySession = await INVENTORY_SESSION_COLL.findById(inventorySessionID)
                .populate({
                    path: "transactions",
                    match: { status: 1 },
                    options: { sort: { createAt: -1 }},
                    select: "customer products totalMoneyNumber note createAt status historyCheck code",
                    populate: {
                        path: "customer products historyCheck",
                        select: "name price createAt userID inventorySession",
                        populate: {
                            path: "userID",
                            select: "username"
                        }
                    },
                });

                let { transactions } = infoInventorySession;

                let result = [];
                let countTransactionIsCheck = 0;

                for (const transaction of transactions) {
                    let historyCheckOfTransactionAndInventory = await HISTORY_CHECK_COLL.findOne({transaction: transaction._id, inventorySession: inventorySessionID })
                    .populate({
                        path: "userID",
                        select: "username"
                    });

                    if(historyCheckOfTransactionAndInventory){
                        countTransactionIsCheck+=1
                        result.unshift({ transaction, historyCheckOfTransactionAndInventory})
                    }else{
                        result.push({ transaction, historyCheckOfTransactionAndInventory});
                    }
                }

                if(!result)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: {result, countTransactionIsCheck}});
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Đếm số lượng giao dịch chưa kiểm kê
     * Dev: Dattv
     */
     countTransactionNotInventory({ agencyID, start, end }){
        return new Promise(async resolve => {
            try {

                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let conditionObj = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let amountTransactionNotInventory = await TRANSACTION_COLL.find({ 
                    agency: agencyID, historyCheck: { $exists: false }, status: 1
                })
                .populate({ path: "customer", select: "name" })
                .populate({ path: "products", select: "name price" });;
                
                if(!amountTransactionNotInventory)
                    return resolve({ error: true, message: 'cannot_count' });

                return resolve({ error: false, data: amountTransactionNotInventory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Đếm số lượng giao dịch đã kiểm kê
     * Dev: Dattv
     */
     countTransactionHaveInventory({ agencyID, start, end }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let conditionObj = {};

                let d = new Date();
                let month = ``;
                let day = ``;

                if(Number(d.getMonth()+1) < 10){
                    month = `0`
                }

                if(Number(d.getDate()) < 10){
                    day = `0`
                }

                let datestring = d.getFullYear() + "-" + `${month}${(d.getMonth()+1)}` + "-" + `${day}${d.getDate()}`;
                let curentDate = moment(datestring).startOf('day').format();

                // if(start && end){
                //     let _fromDate   = moment(start).startOf('day').format();
                //     let _toDate     = moment(end).endOf('day').format();
                //     conditionObj.createAt = {
                //         $gte: new Date(_fromDate),
                //         $lt: new Date(_toDate)
                //     }
                // }else{
                //     conditionObj.createAt = {
                //         $gt: new Date(curentDate)
                //     }
                // }

                conditionObj.createAt = {
                    $gt: new Date(curentDate)
                }

                let amountTransactionHaveInventory = await HISTORY_CHECK_COLL.aggregate([

                    {
                        $match: {...conditionObj, agency: ObjectID(agencyID)}
                    },

                    {
                        $group : { 
                            _id : "$transaction",
                        },
                    },
                    {
                        $lookup: {
                            from: "transactions",
                            localField: "_id",
                            foreignField: "_id",
                            as: "transaction"
                        }
                    },
                    {
                        $unwind: "$transaction"
                    },
                    {
                        $project: {
                            "transaction.historyCheck": 1,
                            "transaction.status": 1,
                            "transaction.customer": 1,
                            "transaction.products": 1,
                            "transaction.totalMoneyNumber": 1,
                            "transaction.note": 1,
                            "transaction.createAt": 1,
                            "transaction.loanAmount": 1,
                        }
                    },
                    {
                        $match: {
                            "transaction.status": 1,
                            "transaction.historyCheck": { $exists: true }
                        }
                    },
                    
                    {
                        $lookup: {
                            from: "customers",
                            localField: "transaction.customer",
                            foreignField: "_id",
                            as: "customer"
                        }
                    },
                    {
                        $unwind: "$customer"
                    },
                    {
                        $lookup: {
                            from: "products",
                            localField: "transaction.products",
                            foreignField: "_id",
                            as: "products"
                        }
                    },
                    {
                        $lookup: {
                            from: "history_checks",
                            localField: "transaction.historyCheck",
                            foreignField: "_id",
                            as: "historyCheck"
                        }
                    },
                    
                    {
                        $addFields: {
                           "totalMoneyNumber": "$transaction.totalMoneyNumber",
                           "note": "$transaction.note",
                           "createAt": "$transaction.createAt",
                           "lastHistoryCheck": { 
                               $arrayElemAt: ["$historyCheck", -1] 
                            },
                            "loanAmount": "$transaction.loanAmount",
                        }
                    },
                    {
                        $lookup: {
                            from: "users",
                            localField: "lastHistoryCheck.userID",
                            foreignField: "_id",
                            as: "userID"
                        }
                    },
                    {
                        $unwind: "$userID"
                    },
                ]);

                if(!amountTransactionHaveInventory)
                    return resolve({ error: true, message: 'cannot_count' });

                return resolve({ error: false, data: amountTransactionHaveInventory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Số lượng gói hàng hiện đang giữ
     * Dev: Dattv
     */
     countTransaction({ agencyID, start, end }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let conditionObj = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let amountTransaction = await TRANSACTION_COLL.find({ agency: agencyID, status: 1 })
                .populate({ path: "customer", select: "name" })
                .populate({ path: "products", select: "name price" })
                .populate({ 
                    path: "historyCheck",
                    populate: { 
                        path: "userID",
                        select: "username"
                    }
                }).sort({ createAt: -1 })
                
                if(!amountTransaction)
                    return resolve({ error: true, message: 'cannot_count' });

                return resolve({ error: false, data: amountTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật phiên kiểm tra
     * Dev: Dattv
     */
    update({ name, description, inventorySessionID }) {
        return new Promise(async resolve => {
            try {
                if (!name)
                    return resolve({ error: true, message: 'name_not_valid' });

                let dataUpdate = { name, description };
               
                let infoAfterUpdate = await INVENTORY_SESSION_COLL.findByIdAndUpdate(inventorySessionID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ inventorySessionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(inventorySessionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterRemove = await INVENTORY_SESSION_COLL.findByIdAndDelete(inventorySessionID);
                if(!infoAfterRemove)
                    return resolve({ error: true, message: 'cannot_remove' });

                return resolve({ error: false, data: infoAfterRemove });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
