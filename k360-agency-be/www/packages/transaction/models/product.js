"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const PRODUCT_COLL                = require('../database/product-coll')({});

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_COLL);
    }

	insert({ name, type, price, transaction, lastChilds, note, priceHaveBillChoose, meta }) {
        return new Promise(async resolve => {
            try {
                if (!name  || !price|| !ObjectID.isValid(type) || !ObjectID.isValid(transaction))
                    return resolve({ error: true, message: 'name_not_valid' });
                let dataInsert = { name, type, price, transaction};

                if(lastChilds) {
                    dataInsert.lastChilds = lastChilds;
                }
                if(note) {
                    dataInsert.note = note;
                }

                if(priceHaveBillChoose) {
                    dataInsert.priceHaveBill = priceHaveBillChoose;
                }

                if (meta) {
                    if(meta.typeName){
                        dataInsert["meta.typeName"]        = meta.typeName
                    }
                    if(meta.typeProductCategory){
                        dataInsert["meta.typeProductCategory"]            = Number(meta.typeProductCategory)
                    }
                    if(meta.size){
                        dataInsert["meta.size"]            = Number(meta.size)
                    }
                    if(meta.weight){
                        dataInsert["meta.weight"]          = Number(meta.weight)
                    }
                    if(meta.have_bill){
                        dataInsert["meta.have_bill"]       = Number(meta.have_bill)
                    }
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ productID, name, type, price, note }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'name_not_valid' });

                let dataUpdate = { };
                if(name){
                    dataUpdate.name = name
                }

                if(ObjectID.isValid(type)){
                    dataUpdate.type = type
                }

                if(price){
                    dataUpdate.price = price
                }

                if(note){
                    dataUpdate.note = note
                }

                let infoAfterUpdate = await PRODUCT_COLL.findByIdAndUpdate(productID, dataUpdate, { new: true });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    remove({ productID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'name_not_valid' });

                let infoAfterUpdate = await PRODUCT_COLL.findByIdAndDelete(productID);
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_remove' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductWithType({ typeProductCategoryID }) {
        return new Promise(async resolve => {
            try {

                let listProductWithType = await PRODUCT_COLL.find({ type: typeProductCategoryID }).populate("transaction");

                if(!listProductWithType)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listProductWithType });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
