"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
 const { checkObjectIDs }            = require('../../../utils/utils');
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const INJECT_TRANSACTION_COLL                = require('../database/inject_transaction-coll')({});
const TRANSACTION_COLL                       = require('../database/transaction-coll')({});

class Model extends BaseModel {
    constructor() {
        super(INJECT_TRANSACTION_COLL);
    }

	insert({ agencyID, title, type, transaction, note, meta, ransom, dataUpdate, status, userCreate, userConfirm }) {
        return new Promise(async resolve => {
            try {
                if (!title  || !type || !ObjectID.isValid(transaction) || !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataInsert = { title, type, transaction, agency: agencyID };
                if(note){
                    dataInsert.note = note;
                }
                if(userCreate){
                    dataInsert.userCreateAt = userCreate;
                }
                if(userConfirm){
                    dataInsert.userConfirm = userConfirm;
                }
                 // Code sau này lấy theo 3 ký tự đầu của ký hiệu đại lý + random 5 số
                // let code = Math.floor(100000 + Math.random() * 900000);
                // let checkCode = await INJECT_TRANSACTION_COLL.find({ code: code });
                // if(code) {
                //     code = Math.floor(100000 + Math.random() * 900000);
                // }
                let { data: code } = await this.checkCode({  });
                dataInsert.code = code;

                if(meta){
                    if(meta.loanAmount){
                        dataInsert["meta.loanAmount"]        = Number(meta.loanAmount)
                    }

                    if(meta.loanAmountAfterChange || meta.loanAmountAfterChange == 0){
                        dataInsert["meta.loanAmountAfterChange"]        = Number(meta.loanAmountAfterChange)
                    }
                    
                    if(meta.cycleInterest){
                        dataInsert["meta.cycleInterest"]        = Number(meta.cycleInterest)
                    }

                    if(meta.promotionInterest){
                        dataInsert["meta.promotionInterest"]       = Number(meta.promotionInterest)
                    }

                    if(meta.priceMuchPayInterest || meta.priceMuchPayInterest == 0){
                        dataInsert["meta.priceMuchPayInterest"] = Number(meta.priceMuchPayInterest)
                    }

                    if(meta.priceMuchPayInterestOriginal || meta.priceMuchPayInterestOriginal == 0){
                        dataInsert["meta.priceMuchPayInterestOriginal"] = Number(meta.priceMuchPayInterestOriginal)
                    }

                    if(meta.numberDatePayLate){
                        dataInsert["meta.numberDatePayLate"] = Number(meta.numberDatePayLate)
                    }

                    if(meta.percentDatePayLate){
                        dataInsert["meta.percentDatePayLate"] = Number(meta.percentDatePayLate)
                    }

                    if(meta.expireTimeLate){ // NGÀY LÙI
                        dataInsert["meta.expireTimeLate"] = meta.expireTimeLate;
                    }meta.note

                    if(meta.note){ // GHI CHÚ KHUYẾN MÃI
                        dataInsert["meta.note"] = meta.note;
                    }
                    /**
                     * ======================================================
                     * ======================GD CHUỘC ĐỒ=====================
                     * ======================================================
                    */
                    if(meta.priceRansomPay){
                        dataInsert["meta.priceRansomPay"]       = Number(meta.priceRansomPay)
                    }

                    if(meta.priceRansomPayOriginal){
                        dataInsert["meta.priceRansomPayOriginal"]       = Number(meta.priceRansomPayOriginal)
                    }
                    /**
                     * ======================================================
                     * ======================GD CHUỘC ĐỒ=====================
                     * ======================================================
                    */
                    if(meta.cycleInterest){
                        dataInsert["meta.cycleInterest"] = Number(meta.cycleInterest)
                    }
                    
                    if(meta.reissueFee){
                        dataInsert["meta.reissueFee"] = Number(meta.reissueFee)
                    }

                    if(meta.requestPrintTh){
                        dataInsert["meta.requestPrintTh"] = Number(meta.requestPrintTh)
                    }
                    /**
                     * SỐ TIỀN CÓ THỂ MƯỢN
                     */
                    if(meta.canBorrowMore){
                        dataInsert["meta.canBorrowMore"] = meta.canBorrowMore
                    }
                    /**
                     * SỐ TIỀN MUỐN MƯỢN
                     */
                    if(meta.amountWantBorrowMore){
                        dataInsert["meta.amountWantBorrowMore"] = meta.amountWantBorrowMore
                    }
                    /**
                     * SỐ NGÀY TRẢ LÃI
                     */
                    if(meta.numberDatePayInterest){
                        dataInsert["meta.numberDatePayInterest"] = Number(meta.numberDatePayInterest)
                    }
                    /**
                     * SỐ TIỀN ĐÃ TRẢ TRƯỚC ĐỂ RESET VỀ SỐ DƯ
                     */
                    if (meta.pricePayInterest) {
                        dataInsert["meta.pricePayInterest"] = Number(meta.pricePayInterest)
                    }
                    /**
                     * TỔNG TIỀN PHẢI TRẢ VAY THÊM
                     */
                    if (meta.totalAmountWantBorrowMore) {
                        dataInsert["meta.totalAmountWantBorrowMore"] = Number(meta.totalAmountWantBorrowMore)
                    }
                    /**
                     * ======================================================
                     * ======================GD TRẢ BỚT=====================
                     * ======================================================
                    */
                    if(meta.paybackMin){
                        dataInsert["meta.paybackMin"] = meta.paybackMin
                    }
                   
                    if(meta.amountWantPayback){
                        dataInsert["meta.amountWantPayback"] = meta.amountWantPayback
                    }
                    
                    if(meta.totalAmountWantPayBack){
                        dataInsert["meta.totalAmountWantPayBack"] = meta.totalAmountWantPayBack
                    }
                    /**
                     * ======================================================
                     * ======================GD TRẢ BỚT=====================
                     * ======================================================
                    */
                    
                    if(meta.statusFinancial){ // LOẠI QUỸ
                        dataInsert["meta.statusFinancial"] = meta.statusFinancial
                    }
                }

                // if(type == 2) {
                //     return resolve({ error: true, message: 'cannot_insert_type_2_have_meta_invalid' });
                // }

                if(type == 3 && !meta.canBorrowMore && !meta.amountWantBorrowMore) {
                    return resolve({ error: true, message: 'cannot_insert_type_3_have_meta_invalid' });
                }
                
                if(type == 4 && !meta.paybackMin && !meta.amountWantPayback) {
                    return resolve({ error: true, message: 'cannot_insert_type_4_have_meta_invalid' });
                }
                if(type == 5 && !meta.reissueFee && !meta.requestPrintTh) {
                    return resolve({ error: true, message: 'cannot_insert_type_5_have_meta_invalid' });
                }
                // if(type == 6) {
                //     return resolve({ error: true, message: 'cannot_insert_type_6_have_meta' });
                // }
                if(type == 6 && ransom) {
                    dataInsert.ransom = ransom;
                }
                if (dataUpdate) {
                    dataInsert = {
                        ...dataInsert,
                        ...dataUpdate
                    }
                }
                if (status) {
                    dataInsert = {
                        ...dataInsert,
                        status
                    }
                }
                // console.log({ dataInsert });
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                // Cập nhật injectTransaction cho transaction.
                if ( type == 5 ){
                    await TRANSACTION_COLL.findByIdAndUpdate(transaction, {
                        $addToSet: {
                            injectTransaction: infoAfterInsert._id
                        },
                        modifyAt: new Date()
                    }, { new: true });
                }else{
                    await TRANSACTION_COLL.findByIdAndUpdate(transaction, {
                        $addToSet: {
                            injectTransaction:       infoAfterInsert._id,
                        },
                        latestInjectTransaction: infoAfterInsert._id,
                        modifyAt: new Date()
                    }, { new: true });
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Kiểm tra code
     */
     checkCode({ }) {
        return new Promise(async resolve => {
            try {
                let codeNew = Math.floor(100000 + Math.random() * 900000);
                const infoFinded = await INJECT_TRANSACTION_COLL.findOne({ code: Number(codeNew) });
                
                if(infoFinded)  {
                    await checkCode({  });
                }

                return resolve({ error: false, data: codeNew });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateInjectTransaction({ transactionID, title, type, note, meta, customerImagesInjectIsDelete, KYCImagesInjectIsDelete, receiptImagesInjectIsDelete, formVerificationImagesInjectIsDelete }) {
        return new Promise(async resolve => {
            try {
                if (!title  || !type || !ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataUpdate = { title, type, transactionID , $pullAll: {} };
                if(note){
                    dataUpdate.note = note;
                }
                
                if(meta){
                    if(meta.reissueFee){
                        dataUpdate["meta.reissueFee"] = Number(meta.reissueFee)
                    }
                    if(meta.requestPrintTh){
                        dataUpdate["meta.requestPrintTh"] = Number(meta.requestPrintTh)
                    }
                    if(meta.canBorrowMore){
                        dataUpdate["meta.canBorrowMore"] = meta.canBorrowMore
                    }
                    if(meta.amountWantBorrowMore){
                        dataUpdate["meta.amountWantBorrowMore"] = meta.amountWantBorrowMore
                    }
                    if(meta.paybackMin){
                        dataUpdate["meta.paybackMin"] = meta.paybackMin
                    }
                    if(meta.amountWantPayback){
                        dataUpdate["meta.amountWantPayback"] = meta.amountWantPayback
                    }
                }

                if(customerImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].customerImages = customerImagesInjectIsDelete;
                }

                if(KYCImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].KYCImages = KYCImagesInjectIsDelete;
                }

                if(receiptImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].receiptImages = receiptImagesInjectIsDelete;
                }
                if(formVerificationImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].formVerificationImages = formVerificationImagesInjectIsDelete;
                }
                // console.log({ dataUpdate });
                let infoAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(transactionID, 
                    dataUpdate,
                    {
                        new: true
                    }
                );

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ transactionID, status }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID) || ![0, 1, 2].includes(Number(status)))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let infoAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(transactionID, { status }, { new: true });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ transactionID, ransom, noteNotApproved, transactionDate }) {
        return new Promise(async resolve => {
            try {
                // console.log({ transactionID, ransom });
                if (!ObjectID.isValid(transactionID) || ![1, 2, 3].includes(Number(ransom)))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataUpdate = { ransom }
                if(noteNotApproved) {
                    dataUpdate.noteNotApproved = noteNotApproved;
                }
                if(transactionDate) {
                    dataUpdate.transactionDate = transactionDate;
                }
                let infoAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(transactionID, 
                    dataUpdate, 
                    { new: true });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoData = await INJECT_TRANSACTION_COLL.findById(transactionID);
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculateTotalInterest({ transactionID }) {
        return new Promise(async resolve => {
            try {
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                const TYPE_BORROW_PAY             = [TYPE_TRANSACTION_BORROW, TYPE_TRANSACTION_PAY];
                if (!checkObjectIDs(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                // điều kiện tìm kiếm với giao dịch bình thường chỉ lấy ra các giao dich có trạng thái đang xử lý - chờ duyệt
                const STATUS_PAY_ON_TIME = 0, STATUS_PAY_LATE = 1;
                let dataFindTransactionNormal = { 
                    type: TYPE_TRANSACTION_NORMMAL, 
                    $or: [{ status: STATUS_PAY_ON_TIME }, { status: STATUS_PAY_LATE }] 
                };

                // điều kiện tìm kiếm với giao dịch vay thêm và trả bớt
                const STATUS_VALID_BORROW_PAY = 1;
                let dataFindTransactionBorrowPay = { 
                    type: { $in: TYPE_BORROW_PAY }, 
                    status: STATUS_VALID_BORROW_PAY
                };

                // điều kiện tìm kiếm với các giao dịch là giao dịch chuộc đồ thì chỉ tính đối với các giao dịch chuộc đồ đã duyệt
                const STATUS_ACTIVE_RANSOM = 1;
                let dataFindTransactionRansom = {
                    type: TYPE_TRANSACTION_RANSOM, ransom: STATUS_ACTIVE_RANSOM
                }

                let infoData = await INJECT_TRANSACTION_COLL.find({
                    transaction: transactionID,
                    $or: [
                        dataFindTransactionNormal,
                        dataFindTransactionRansom,
                        dataFindTransactionBorrowPay
                    ]
                }, { meta: 1, _id: 1, transaction: 1 }).sort({ createAt: -1 });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });

                /**
                 * TÍNH TỔNG TIỀN LÃI CỦA GIAO DỊCH
                 */
                let totalPrice = 0
                if (infoData && infoData.length) {
                    infoData.forEach(inject => {
                        if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                            totalPrice += inject.meta.priceMuchPayInterest;
                    });
                }

                return resolve({ error: false, data: totalPrice });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculatePriceTypeTransaction({ transactionID, type }) {
        return new Promise(async resolve => {
            try {
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                if (!checkObjectIDs(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                // điều kiện tìm kiếm với giao dịch bình thường chỉ lấy ra các giao dich có trạng thái đang xử lý - chờ duyệt
                const STATUS_PAY_ON_TIME = 0, STATUS_PAY_LATE = 1;
                
                // điều kiện tìm kiếm với giao dịch vay thêm và trả bớt
                const STATUS_VALID_BORROW_PAY = 1;

                // điều kiện tìm kiếm với các giao dịch là giao dịch chuộc đồ thì chỉ tính đối với các giao dịch chuộc đồ đã duyệt
                const STATUS_ACTIVE_RANSOM = 1;

                let dataFind = {};

                switch(+type) {
                    case TYPE_TRANSACTION_NORMMAL: {
                        dataFind = {
                            type: TYPE_TRANSACTION_NORMMAL, 
                            $or: [{ status: STATUS_PAY_ON_TIME }, { status: STATUS_PAY_LATE }] 
                        }
                        break;
                    }
                    case TYPE_TRANSACTION_BORROW: {
                        dataFind = {
                            type: TYPE_TRANSACTION_BORROW, 
                            status: STATUS_VALID_BORROW_PAY
                        }
                        break;
                    }
                    case TYPE_TRANSACTION_PAY: {
                        dataFind = {
                            type: TYPE_TRANSACTION_PAY, 
                            status: STATUS_VALID_BORROW_PAY
                        }
                        break;
                    }
                    case TYPE_TRANSACTION_RANSOM: {
                        dataFind = {
                            type: TYPE_TRANSACTION_RANSOM, ransom: STATUS_ACTIVE_RANSOM
                        }
                        break;
                    }
                }

                let infoData = await INJECT_TRANSACTION_COLL.find({
                    transaction: transactionID,
                    ...dataFind
                    
                }, { meta: 1, _id: 1, transaction: 1 }).sort({ createAt: -1 });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });

                /**
                 * TÍNH TỔNG TIỀN LÃI CỦA GIAO DỊCH
                 */
                let totalPrice = 0;
                /**
                 * Tổng số tiền lãi và phí của giao dịch
                 */
                let totalInterestAndFee = 0;
                if (infoData && infoData.length) {
                    switch (Number(type)) {
                        /**
                         * GIAO DỊCH BÌNH THƯỜNG (TRẢ LÃI)
                         */
                        case TYPE_TRANSACTION_NORMMAL:
                            infoData.forEach( inject => {
                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalPrice += inject.meta.priceMuchPayInterest;
                            });
                            totalInterestAndFee = totalPrice;
                            break;
                        /**
                         * GIAO DỊCH VAY THÊM
                         */
                        case TYPE_TRANSACTION_BORROW:
                            infoData.forEach( inject => {
                                if (inject.meta.amountWantBorrowMore || inject.meta.amountWantBorrowMore == 0)
                                    totalPrice += inject.meta.amountWantBorrowMore;

                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalInterestAndFee += inject.meta.priceMuchPayInterest;
                            });
                            break;
                        /**
                         * GIAO DỊCH TRẢ BỚT
                         */
                        case TYPE_TRANSACTION_PAY:
                            infoData.forEach( inject => {
                                if (inject.meta.amountWantPayback || inject.meta.amountWantPayback == 0)
                                    totalPrice += inject.meta.amountWantPayback;

                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalInterestAndFee += inject.meta.priceMuchPayInterest;
                            });
                            break;

                        /**
                         * GIAO DỊCH CHUỘC ĐỒ
                         */
                        case TYPE_TRANSACTION_RANSOM:
                            infoData.forEach( inject => {
                                if (inject.meta.priceRansomPayOriginal || inject.meta.priceRansomPayOriginal == 0)
                                    totalPrice += inject.meta.priceRansomPayOriginal;

                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalInterestAndFee += inject.meta.priceMuchPayInterest;
                            });
                            break;
                        default:
                            break;
                    }
                }

                return resolve({ error: false, data: { totalPrice, totalInterestAndFee } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
