"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const PAY_INTEREST_TRANSACTION_COLL                = require('../database/pay_interest_transaction-coll')({});
// const TRANSACTION_COLL                       = require('../database/transaction-coll')({});

class Model extends BaseModel {
    constructor() {
        super(PAY_INTEREST_TRANSACTION_COLL);
    }

	insert({ title, employee, type, transaction, period, interestRateTotal, interestStartDate, resetDate, expireTime, interstAmountDate, interestPercent, transactionMain }) {
        return new Promise(async resolve => {
            try {
                if ( !title || !type|| !period|| !ObjectID.isValid(employee) || !ObjectID.isValid(transaction) || !ObjectID.isValid(transactionMain) || !expireTime)
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataInsert = { title, type, transaction, employee, period, resetDate, interestStartDate, interestPercent, interestRateTotal, transactionMain, expireTime };

                if ( interstAmountDate ){
                    dataInsert.interstAmountDate = interstAmountDate;
                }

                 // Code sau này lấy theo 3 ký tự đầu của ký hiệu đại lý + random 5 số
                // let code = Math.floor(100000 + Math.random() * 900000);
                // dataInsert.code = code;
                // console.log({ dataInsert });
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByTransactionID({ transactionID, status = 1 }){
        return new Promise( async resolve => {
            try {
                if(!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let data = { transactionMain: transactionID, status };
                data.expireTime = { $gte: new Date() };
                // console.log({ data });
                let infoData = await PAY_INTEREST_TRANSACTION_COLL.find(data).sort({ createAt: -1 });
                if(!infoData) {
                    return resolve({ error: true, message: "cannot_get_info" });
                }
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ transactionID, status }){
        return new Promise( async resolve => {
            try {
                if(!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = {};
                if(status || status == 0) {
                    dataUpdate.status = status;
                }

                let infoData = await PAY_INTEREST_TRANSACTION_COLL.findByIdAndUpdate(transactionID, {
                    ...dataUpdate
                },{
                    new: true
                });

                if(!infoData) {
                    return resolve({ error: true, message: "cannot_get_info" });
                }
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
