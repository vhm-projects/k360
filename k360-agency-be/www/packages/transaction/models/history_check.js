"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const HISTORY_CHECK_COLL = require('../database/history_check-coll')({});
const TRANSACTION_COLL = require('../database/transaction-coll')({});
const INVENTORY_SESSION_COLL = require('../database/inventory_session-coll')({});

class Model extends BaseModel {
    constructor() {
        super(HISTORY_CHECK_COLL);
    }

	createInventoryTransaction({ transactionID, agencyID, userID, inventorySessionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID) || !ObjectID.isValid(agencyID) || !ObjectID.isValid(inventorySessionID) || !ObjectID.isValid(userID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoInventorySession = await INVENTORY_SESSION_COLL.findById(inventorySessionID);

                let { transactions } = infoInventorySession;

                //Check transaction không có trong phiên kiểm kê
                if(transactions && transactions.length && transactions.indexOf(transactionID) === -1)
                    return resolve({ error: true, message: "transaction_is_not_exist_inventory_session" });
                
                let dataInsert = {
                    transaction: transactionID,
                    agency: agencyID,
                    userID,
                    inventorySession: inventorySessionID
                };

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                await TRANSACTION_COLL.findByIdAndUpdate(transactionID, {
                    $push: { historyCheck: { $each: [infoAfterInsert._id], $position: 0} }
                }, {new: true})

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListHistoryCheckOfTransaction({ transactionID, inventorySessionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                // let infoTransaction = await TRANSACTION_COLL.findById(transactionID)
                // .populate({ 
                //     path: "historyCheck",
                //     options: { sort: { createAt: -1 }},
                //     populate: { 
                //         path: "userID",
                //         select: "username"
                //     }
                // });

                let listHistoryCheckOfTransaction;

                if(inventorySessionID){
                    listHistoryCheckOfTransaction = await HISTORY_CHECK_COLL.find({ transaction: transactionID, inventorySession: inventorySessionID})
                    .populate({ 
                        path: "userID",
                        select: "username"
                    }).sort({ createAt: -1 })
                }else{
                    listHistoryCheckOfTransaction = await HISTORY_CHECK_COLL.find({ transaction: transactionID }).populate({ 
                        path: "userID",
                        select: "username"
                    }).sort({ createAt: -1 })
                }

                if(listHistoryCheckOfTransaction){
                    return resolve({ error: false, data: listHistoryCheckOfTransaction });
                }else{
                    return resolve({ error: false, data: [] });
                }

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
