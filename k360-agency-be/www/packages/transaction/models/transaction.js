"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const moment   = require("moment");

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const timeUtils = require('../../../utils/time_utils');
const { subStractDateGetDay, addNumberToSetDate, diff_months, minusNumberToSetDate }                             = require('../../../config/cf_time');
const { getFundsAgency, calculateDateOfPeriod, calculateInterestAndExpireDate, calculateBetweentTwoPrice, checkLimitDate, isEmptyObject }                   = require("../../../utils/utils");

/**
 * MODELS
 */
 const PAY_INTEREST_TRANSACTION_MODEL                = require('../models/pay_interest_transaction').MODEL;
 const INTEREST_RATE_MODEL                           = require('../../interest_rate/models/interest_rate').MODEL;
 const { IMAGE_MODEL }                               = require('../../image');
 const INJECT_TRANSACTION_MODEL                      = require('../models/inject_transaction').MODEL;
 const HISTORY_TRANSACTION_MODEL                     = require('../models/history_transaction').MODEL;
 const { FINANCIAL_MODEL }                           = require('../../financial');
 const PRODUCT_MODEL                                 = require('../models/product').MODEL;
 const COMMON_MODEL                                   = require('../../common/models/common').MODEL;

/**
 * COLLECTIONS
 */
const TRANSACTION_COLL                = require('../database/transaction-coll')({});
const INJECT_TRANSACTION_COLL         = require('../database/inject_transaction-coll')({});
const PRODUCT_COLL                    = require('../database/product-coll')({});
const AGENCY_COLL                     = require('../../agency/databases/agency-coll')({});
const FINANCIAL_COLL                  = require('../../financial/database/financial-coll')({});

const { checkObjectIDs }            = require('../../../utils/utils');


class Model extends BaseModel {
    constructor() {
        super(TRANSACTION_COLL);
    }

	insert({ customer, transactionEmployee, agency, note, loanAmount, approvalLimit, totalMoneyChar, approvalNote, category }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(customer) || !checkObjectIDs(transactionEmployee) || !checkObjectIDs(agency) || !checkObjectIDs(category) ||!loanAmount ||!approvalLimit )
                    return resolve({ error: true, message: 'params_invalid' });

                let dataInsert = {
                    customer, transactionEmployee, agency, loanAmount, approvalLimit, category, totalMoneyNumber: loanAmount
                }
                // totalMoneyNumber && (dataInsert.loanAmount = loanAmount)
                // if ( totalMoneyChar ){
                //     dataInsert.totalMoneyChar = totalMoneyChar;
                // }
                dataInsert.interestStartDate = new Date();
                dataInsert.transactionDate   = new Date();
                const NUMBER_MINUS = 1;
                let dateConfirmChange     =  minusNumberToSetDate({date1: new Date(), numberToSet: NUMBER_MINUS });
                let expireTimeAfterPeriod = new Date(dateConfirmChange);
                addNumberToSetDate({ date1: expireTimeAfterPeriod, numberToSet: 30 });

                dataInsert.expireTime = new Date(dateConfirmChange);
                dataInsert.expireTimeAfterPeriod = expireTimeAfterPeriod;
                console.log({ dataInsert });
                if(note){
                    dataInsert.note = note;
                }
                // expireTime
                if(approvalNote){
                    dataInsert.approvalNote = approvalNote;
                }

                // Code sau này lấy theo 3 ký tự đầu của ký hiệu đại lý + random 5 số bbbbb
                let { data: code } = await this.checkCode({  });
                
                dataInsert.code = code;
                // console.log({ dataInsert });
                let infoAfterInsert = await this.insertData(dataInsert);

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Kiểm tra code
     */
     checkCode({ }) {
        return new Promise(async resolve => {
            try {
                let codeNew = Math.floor(1000000000 + Math.random() * 9000000000);
                const infoFinded = await TRANSACTION_COLL.findOne({ code: Number(codeNew) });
                
                if(infoFinded)  {
                    await checkCode({  });
                }

                return resolve({ error: false, data: codeNew });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ 
        transactionID, loanAmount, qrCode, description, interestStartDateNew, expireTimeNew, payInterest, transactionDateNew, 
        expireTimeAfterPeriod , transactionConfirm
    }) {
        return new Promise(async resolve => {
            try {
                if ( !ObjectID.isValid(transactionID) )
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = {};

                if ( loanAmount ){
                    dataUpdate.loanAmount = loanAmount;
                }

                if ( qrCode ){
                    dataUpdate.qrCode = qrCode;
                }

                if(payInterest || payInterest == 0) {
                    dataUpdate.payInterest = payInterest
                }

                if (transactionConfirm) {
                    dataUpdate.transactionConfirm = transactionConfirm;
                }
                // CHECK XEM API UPDATE
               
                // dataUpdate.interestStartDate = new Date();
                if(interestStartDateNew && expireTimeNew && transactionDateNew) {
                    dataUpdate.interestStartDate   = interestStartDateNew;
                    dataUpdate.expireTime          = expireTimeNew;
                    dataUpdate.transactionDate     = transactionDateNew

                    let expireTimeChange = new Date(expireTimeNew);
                    addNumberToSetDate({ date1: expireTimeChange, numberToSet: 30 });
                    dataUpdate.expireTimeAfterPeriod     = expireTimeChange
                }else{
                    let expireTime = new Date();
    
                    addNumberToSetDate({ date1: expireTime, numberToSet: 30 });
                    let interestStartDate = new Date(expireTime);

                    addNumberToSetDate({ date1: interestStartDate, numberToSet: 1 });
                    let expireTimeAfterPeriod = new Date(expireTime);

                    addNumberToSetDate({ date1: expireTimeAfterPeriod, numberToSet: 30 });

                    if(description && description == "update") {
                        
                    }else{
                        dataUpdate.expireTime            = expireTime;
                        dataUpdate.interestStartDate     = interestStartDate;
                        dataUpdate.expireTimeAfterPeriod = expireTimeAfterPeriod;
                        dataUpdate.transactionDate       =  new Date();
                    }
                }
                // dataUpdate.modifyAt = new Date();

                // console.log({ dataUpdate });
                await this.updateWhereClause({ _id: transactionID }, {
                    ...dataUpdate
                });

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: UPDATE LẠI SỐ TIỀN LÃI TRẢ TRƯỚC
     * SONLP
     */
    updatePayInterest({ transactionID, payInterest }) {
        return new Promise(async resolve => {
            try {
                if ( !ObjectID.isValid(transactionID) )
                    return resolve({ error: true, message: 'params_invalid' });

                let infoData = await TRANSACTION_COLL.findById(transactionID);
                let { payInterest: payInterestOld } = infoData;

                let dataUpdate = { payInterest: payInterestOld + payInterest };
                
                // console.log({ dataUpdate });
                await this.updateWhereClause({ _id: transactionID }, {
                    ...dataUpdate
                });

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateTransaction({ transactionID, transactionEmployee, arrProductIDDelete, loanAmount, approvalLimit, totalMoneyNumber, totalMoneyChar, approvalNote, customerImagesIsDelete, KYCImagesIsDelete, receiptImagesIsDelete, formVerificationImagesIsDelete, billImagesIsDelete }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID) || !ObjectID.isValid(transactionEmployee))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataUpdate = { transactionEmployee, $pullAll: {} };

                if(loanAmount) {
                    dataUpdate.loanAmount = loanAmount;
                }

                if(approvalLimit) {
                    dataUpdate.approvalLimit = approvalLimit;
                }

                if(totalMoneyNumber) {
                    dataUpdate.totalMoneyNumber = totalMoneyNumber;
                }

                if(approvalNote){
                    dataUpdate.approvalNote = approvalNote;
                }else{
                    dataUpdate.approvalNote = "";
                }

                if(arrProductIDDelete) {
                    dataUpdate['$pullAll'].products = arrProductIDDelete;
                }

                if(customerImagesIsDelete && customerImagesIsDelete.length) {
                    dataUpdate['$pullAll'].customerImages = customerImagesIsDelete;
                }

                if(KYCImagesIsDelete && KYCImagesIsDelete.length) {
                    dataUpdate['$pullAll'].KYCImages = KYCImagesIsDelete;
                }

                if(receiptImagesIsDelete && receiptImagesIsDelete.length) {
                    dataUpdate['$pullAll'].receiptImages = receiptImagesIsDelete;
                }

                if(formVerificationImagesIsDelete && formVerificationImagesIsDelete.length) {
                    dataUpdate['$pullAll'].formVerificationImages = formVerificationImagesIsDelete;
                }

                if(billImagesIsDelete && billImagesIsDelete.length) {
                    dataUpdate['$pullAll'].billImages = billImagesIsDelete;
                }

                // const STATUS_WAIT_APPROVE = 0;
                // dataUpdate.status = STATUS_WAIT_APPROVE;

                dataUpdate.modifyAt = new Date();

                let infoTransactionAfterUpdate = await TRANSACTION_COLL.findByIdAndUpdate(transactionID, 
                    dataUpdate
                ,{
                    new: true
                });

                if(!infoTransactionAfterUpdate) {
                    return resolve({ error: true, message: "cannot_update_transaction" });
                }

                return resolve({ error: false, data: infoTransactionAfterUpdate });
                
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateLoanAmountRansom({ transactionID, loanAmount = 0, status = 3 }) {
        return new Promise(async resolve => {
            try {
                if ( !ObjectID.isValid(transactionID) )
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = {};

                dataUpdate.loanAmount = loanAmount;
                // let dateNowToExpireTime = new Date();
                let dateNow = new Date();
                let expireTime = new Date();
                addNumberToSetDate({ date1: expireTime, numberToSet: 30 });
                let interestStartDate = new Date(expireTime);

                addNumberToSetDate({ date1: interestStartDate, numberToSet: 1 });
                let expireTimeAfterPeriod = new Date(expireTime);

                addNumberToSetDate({ date1: expireTimeAfterPeriod, numberToSet: 30 });
                // addNumberToSetDate({ date1: dateNowToExpireTime, numberToSet: 30 });

                // dataUpdate.expireTime        = dateNowToExpireTime;
                dataUpdate.expireTime            = expireTime;
                dataUpdate.interestStartDate     = interestStartDate;
                dataUpdate.expireTimeAfterPeriod = expireTimeAfterPeriod;
                dataUpdate.transactionDate       =  new Date();
                dataUpdate.status = status;

                dataUpdate.payInterest = 0;
                // dataUpdate.modifyAt = new Date();

                // dataUpdate.interestStartDate = new Date();
                
                await this.updateWhereClause({ _id: transactionID }, {
                    ...dataUpdate
                });

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ transactionID, status, noteNotApproved, ownerConfirm }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID) || ![0, 1, 2].includes(Number(status)))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = {
                    status
                }
                if ( noteNotApproved ){
                    dataUpdate.noteNotApproved = noteNotApproved
                }

                if ( ownerConfirm ){
                    dataUpdate.ownerConfirm = ownerConfirm
                }

                dataUpdate.interestStartDate = new Date();
                dataUpdate.transactionDate   = new Date();
                dataUpdate.modifyAt = new Date();
                const NUMBER_MINUS = 1;
                let dateConfirmChange =  minusNumberToSetDate({date1: new Date(), numberToSet: NUMBER_MINUS })
                let expireTimeAfterPeriod = new Date(dateConfirmChange);
                addNumberToSetDate({ date1: expireTimeAfterPeriod, numberToSet: 30 });

                dataUpdate.expireTime = new Date(dateConfirmChange);
                dataUpdate.expireTimeAfterPeriod = expireTimeAfterPeriod;

                let infoAfterUpdate = await TRANSACTION_COLL.findByIdAndUpdate(transactionID, dataUpdate, { new: true });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Update trạng thái kiểm kê của gói hàng
     * Dattv
     */
    updateTypeInventory({ transactionID, agencyID, typeInventory }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID) || !ObjectID.isValid(agencyID) || ![0, 1].includes(Number(typeInventory)))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoTransaction = await TRANSACTION_COLL.findById(transactionID);
                let { agency } = infoTransaction;
                if(agency.toString() != agencyID.toString()){
                    return resolve({ error: true, message: 'have_not_access' });
                }

                let infoAfterUpdate = await TRANSACTION_COLL.findByIdAndUpdate(transactionID, { 
                    typeInventory
                }, { new: true });
                
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chi tiết giao dịch
     * Depv
     */
    getInfo({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.findById(transactionID).populate({
                    path: "injectTransaction latestInjectTransaction customer customerImages KYCImages receiptImages formVerificationImages billImages"
                })
                // .populate({
                //     path : "injectTransaction",
                //     populate: {
                //         path: "customerImages KYCImages receiptImages formVerificationImages",
                //         select: "_id name path"
                //     }
                // })
                .populate({
                    path : "products",
                    // select: "_id name price note",
                    populate: {
                        path: "type",
                        select: "_id name"
                    }
                })
                .populate({
                    path : "qrCode",
                    select: "_id image",
                    populate: {
                        path: "image",
                        select: "_id path"
                    }
                });

                let listInjectTransactionOfTransactionID = await INJECT_TRANSACTION_COLL.find({ transaction: transactionID })
                    .populate({
                        path: "customerImages KYCImages receiptImages formVerificationImages "
                    })
                    .sort({ createAt: -1 });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoAfterUpdate, listInjectTransactionOfTransactionID });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoV2({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.findById(transactionID, { 
                    latestInjectTransaction: 1, transactionDate: 1, expireTime: 1, expireTimeAfterPeriod: 1, loanAmount: 1, 
                    payInterest: 1, approvalLimit: 1, interestStartDate: 1, status: 1, customer: 1, agency: 1, code: 1
                })
                .populate({
                    path: "latestInjectTransaction",
                    select: 'title code'
                })
                .populate({
                    path: "customer",
                    select: 'name phone code'
                })
                // .populate({
                //     path : "injectTransaction",
                //     populate: {
                //         path: "customerImages KYCImages receiptImages formVerificationImages",
                //         select: "_id name path"
                //     }
                // })
                // .populate({
                //     path : "products",
                //     // select: "_id name price note",
                //     populate: {
                //         path: "type",
                //         select: "_id name"
                //     }
                // })
                // .populate({
                //     path : "qrCode",
                //     select: "_id image",
                //     populate: {
                //         path: "image",
                //         select: "_id path"
                //     }
                // });
                console.log({
                    infoAfterUpdate
                });
                // let listInjectTransactionOfTransactionID = await INJECT_TRANSACTION_COLL.find({ transaction: transactionID })
                //     .populate({
                //         path: "customerImages KYCImages receiptImages formVerificationImages "
                //     })
                //     .sort({ createAt: -1 });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoPrint({ transactionID, agencyID, injectTransactionID }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoData = await TRANSACTION_COLL.findById(transactionID, { 
                    transactionDate: 1, loanAmount: 1, products: 1, expireTime: 1, expireTimeAfterPeriod: 1, totalMoneyNumber: 1,
                    approvalLimit: 1, status: 1, customer: 1, agency: 1, code: 1, createAt: 1, note: 1, injectTransaction: 1, transactionConfirm: 1
                })
                    .populate({
                        path: "agency",
                        select: 'name address city district ward avatar',
                        populate: {
                            path: 'avatar',
                            select: 'path'
                        }
                    })
                    .populate({
                        path: "products",
                        select: 'type name price meta',
                        // populate: {
                        //     path: 'type',
                        //     select: ''
                        // }
                    })
                    .populate({
                        path: "customer",
                        select: 'name phone code number_auth_paper address city district ward'
                    }).lean();

                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });
                
                if (infoData.status == 0 || infoData.status == 2) {
                    return resolve({ error: true, message: "Bạn cần duyệt giao dịch để có thể in giao dịch" });
                }

                let priceMuchPayInterest = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: 30, price: infoData.loanAmount, typeProduct: infoData.products[0].type });
                let infoFinancial        = await FINANCIAL_MODEL.getInfo({ transactionID, onModel: 'transaction' });
                let data = {
                    ...infoData,
                    priceMuchPayInterest: priceMuchPayInterest.data.loanAmoutAfterInterest,
                    statusFinancial: infoFinancial.data[0].status == 1 ? 'Tiền mặt' : 'Ngân hàng',
                }
                if (injectTransactionID) {

                    let infoFinancialInject        = await FINANCIAL_MODEL.getInfo({ transactionID: injectTransactionID, onModel: 'inject_transaction' });
                    let statusFinancialInject = infoFinancialInject.data.length && infoFinancialInject.data[0].status == 1 ? 'Tiền mặt' : 'Ngân hàng';
                    data = {
                        ...data,
                        statusFinancialInject: statusFinancialInject,
                    }
                }
                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoTransactionCustomerID({ customerID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.find({ customer: customerID });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách tất cả giao dịch trong 24h của đại lý
     * Dattv
     */
     getListTransaction24h({ agencyID, start, end }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {};

                if (ObjectID.isValid(agencyID)){
                    conditionObj.agency = agencyID;
                }

                var d = new Date();
                let month = ``;
                let day = ``;

                if(Number(d.getMonth()+1) < 10){
                    month = `0`
                }

                if(Number(d.getDate()) < 10){
                    day = `0`
                }

                let datestring = d.getFullYear() + "-" + `${month}${(d.getMonth()+1)}` + "-" + `${day}${d.getDate()}`;
                let curentDate = moment(datestring).startOf('day').format();

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }else{
                    conditionObj.createAt = {
                        $gt: new Date(curentDate)
                    }
                }

                let listTransaction = await TRANSACTION_COLL.find({ 
                    ...conditionObj, status: 1
                })
                .populate({ path: "customer", select: "name" })
                .populate({ path: "products", select: "name price" });
               

                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách tất cả sản phẩm của đại lý trong 24h
     * Dattv
     */
     getListProduct24h({ agencyID, start, end }) {
        return new Promise(async resolve => {
            try {

                let conditionObj = { };

                //B1. Danh sách tất cả transaction thuộc agency
                let listTransaction = await TRANSACTION_COLL.find({ agency: agencyID, status: 1 });

                //B2. Danh sách transactionID
                let arrTransactionID = listTransaction && listTransaction && listTransaction.length
                    && listTransaction.map(transaction => {
                        return transaction._id
                    });

                var d = new Date();
                let month = ``;
                let day = ``;

                if(Number(d.getMonth()+1) < 10){
                    month = `0`
                }

                if(Number(d.getDate()) < 10){
                    day = `0`
                }

                let datestring = d.getFullYear() + "-" + `${month}${(d.getMonth()+1)}` + "-" + `${day}${d.getDate()}`;
                let curentDate = moment(datestring).startOf('day').format();

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }else{
                    conditionObj.createAt = {
                        $gt: new Date(curentDate)
                    }
                }

                if (ObjectID.isValid(agencyID)){
                    conditionObj.transaction = {
                        $in: arrTransactionID 
                    }
                }

                //B3. Danh sách tất cả product thuộc agency đó
                let listProductOfAgency = await PRODUCT_COLL.find(
                    conditionObj
                )
                .populate({ path: "transaction", populate: { path: "customer", select: "name" } })
                .populate({ path: "type", select: "name" });

                if(!listProductOfAgency)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listProductOfAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách giao dịch của đại lý
     * Depv
     */
    getListByAgency({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.find({ agency: agencyID }).populate({
                    path: "products injectTransaction"
                });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách giao dịch của đại lý
     * Depv
     */
     getListInjectNormal({ agencyID, status, type }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let conditionObj = { agency: agencyID };

                if (status && [1, 2, 3].includes(Number(status))) {
                    conditionObj = {
                        ...conditionObj,
                        status
                    }
                }
                // console.log({ conditionObj });
                let listInjectTransaction = await INJECT_TRANSACTION_COLL.find({ ...conditionObj }).sort({ modifyAt: -1 });
                // console.log({ listInjectTransaction });
                
                let listTransactionID = listInjectTransaction.map( inject => inject.transaction );
                
                let infoAfterUpdate   = await TRANSACTION_COLL.find({ _id: { $in: listTransactionID } }).populate({
                    path: "products injectTransaction customer latestInjectTransaction"
                }).sort({ modifyAt: -1 });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByAgencyID({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataFilter  = { agency: agencyID };
                let arrTransactionID = [];
                let dataFindDateMainTrans = {};
                let dataFindDate = {};
                // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
                let start;
                let end;
                if ( fromDay && !toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    dataFindDateMainTrans.interestStartDate = { $gte: start };
                    dataFindDate.createAt = { $gte: start }
                }
                if ( !fromDay && toDay ){
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFindDateMainTrans.interestStartDate = { $lte: end };
                    dataFindDate.createAt = { $lte: end }
                }
                if ( fromDay && toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFindDateMainTrans.interestStartDate = {     
                        $gte: start, 
                        $lte:  end 
                    };
                    dataFindDate.createAt = {     
                        $gte: start, 
                        $lte:  end }
                }
                if(typeTransaction) {
                    if ( typeTransaction == 1){
                        if( dataFindDate.createAt ){
                            dataFilter.interestStartDate = dataFindDate.createAt;
                        }
                        dataFilter.latestInjectTransaction = undefined;
                    }else{
                        dataFindDate.agency = agencyID;
                        dataFindDate.type   = typeTransaction;
                        if ( ransom ){
                            dataFindDate.ransom = ransom
                        }
                        let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate).sort({ createAt: -1 });
                        // console.log({ listInjectTransaction });
                        arrTransactionID = listInjectTransaction.map( item => item._id );
                        arrTransactionID = [...new Set(arrTransactionID)];
                        // console.log({ arrTransactionID });
                        dataFilter.latestInjectTransaction = {
                            $in: arrTransactionID
                        }
                    }
                }else{
                    if( dataFindDate.createAt ){
                        dataFilter.interestStartDate = dataFindDate.createAt;
                    }
                    // dataFilter.latestInjectTransaction = undefined;
                }

                if ( typeStatusTransaction ){
                    dataFilter.status = typeStatusTransaction
                }
                
                let listTransaction = await TRANSACTION_COLL.find(dataFilter)
                    .populate({
                        path: "products injectTransaction customer latestInjectTransaction"
                    })
                    .sort({ modifyAt: -1 });
                
                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListExportExcelByAgencyID({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataFilter  = { agency: agencyID };
                let arrTransactionID = [];
                let dataFindDateMainTrans = {};
                let dataFindDate = {};
                // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
                let start;
                let end;
                if ( fromDay && !toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    dataFindDateMainTrans.interestStartDate = { $gte: start };
                    dataFindDate.createAt = { $gte: start }
                }
                if ( !fromDay && toDay ){
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFindDateMainTrans.interestStartDate = { $lte: end };
                    dataFindDate.createAt = { $lte: end }
                }
                if ( fromDay && toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFindDateMainTrans.interestStartDate = {     
                        $gte: start, 
                        $lte:  end 
                    };
                    dataFindDate.createAt = {     
                        $gte: start, 
                        $lte:  end }
                }
                if(typeTransaction) {
                    if ( typeTransaction == 1){
                        if( dataFindDate.createAt ){
                            dataFilter.interestStartDate = dataFindDate.createAt;
                        }
                        dataFilter.latestInjectTransaction = undefined;
                    }else{
                        dataFindDate.agency = agencyID;
                        dataFindDate.type   = typeTransaction;
                        if ( ransom ){
                            dataFindDate.ransom = ransom
                        }
                        let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate).sort({ createAt: -1 });
                        // console.log({ listInjectTransaction });
                        arrTransactionID = listInjectTransaction.map( item => item._id );
                        arrTransactionID = [...new Set(arrTransactionID)];
                        // console.log({ arrTransactionID });
                        dataFilter.latestInjectTransaction = {
                            $in: arrTransactionID
                        }
                    }
                }else{
                    if( dataFindDate.createAt ){
                        dataFilter.interestStartDate = dataFindDate.createAt;
                    }
                    // dataFilter.latestInjectTransaction = undefined;
                }

                if ( typeStatusTransaction ){
                    dataFilter.status = typeStatusTransaction
                }
                
                let listTransaction = await TRANSACTION_COLL.find(dataFilter)
                    .populate({
                        path: "products injectTransaction customer latestInjectTransaction"
                    })
                    .sort({ modifyAt: -1 });
                
                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // lấy danh sách hết hạn, đến hạn, thanh lý
    getListByAgencyIDCheckDate({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                const STATUS_ACTIVE = 1;
                let dataFilter  = {
                    '$match': {
                      'agency': new ObjectID(agencyID),
                      status: STATUS_ACTIVE
                    },
                    '$project': {}
                };
                
                let arrTransactionID = [];
                let dataFindDateMainTrans = {};
                let dataFindDate = {};
                // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
                let start;
                let end;

                // let nowDate = new Date();
                // subStractDate({ date1: nowDate,  })

                if ( fromDay && !toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    // dataFindDateMainTrans.interestStartDate = { $gte: start };
                    dataFindDate.createAt = { $gte: start }
                }
                if ( !fromDay && toDay ){
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = { $lte: end };
                    dataFindDate.createAt = { $lte: end }
                }
                if ( fromDay && toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = {     
                    //     $gte: start, 
                    //     $lte:  end 
                    // };
                    dataFindDate.createAt = {     
                        $gte: start, 
                        $lte:  end }
                }
                if ( typeTransaction ){
                    if ( typeTransaction == 1){
                        if( dataFindDate.createAt ){
                            // dataFilter.createAt = dataFindDate.createAt;
                            dataFilter['$match'].interestStartDate = dataFindDate.createAt;
                        }
                        dataFilter['$match'].latestInjectTransaction = undefined;
                    }else{
                        dataFindDate.agency = agencyID;
                        dataFindDate.type   = typeTransaction;
                        if ( ransom ){
                            dataFindDate.ransom = ransom
                        }
                        let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate);
                        // console.log({ listInjectTransaction });
                        arrTransactionID = listInjectTransaction.map( item => ObjectID(item._id));
                        arrTransactionID = ["$latestInjectTransaction", [...new Set(arrTransactionID)]];
                        dataFilter['$project'].checkLastInjectTransaction = {
                            $in: arrTransactionID
                        }
                    }
                }else{
                    if( dataFindDate.createAt ){
                        dataFilter['$match'].interestStartDate = dataFindDate.createAt;
                    }
                }

                if ( typeStatusTransaction ){
                    dataFilter['$match'].status = Number(typeStatusTransaction);
                }
                dataFilter['$project'] = {
                    ...dataFilter['$project'],
                    'customer': 1, 
                    'loanAmount': 1, 
                    'code': 1, 
                    'status': 1, 
                    'expireTime': 1, 
                    'latestInjectTransaction': 1,
                    'interestStartDate': 1, 
                    'modifyAt': 1, 
                    'dateDifference': {
                        '$divide': [
                        {
                            '$subtract': [
                                '$expireTimeAfterPeriod', new Date()
                            ]
                        }, 1000 * 60 * 60 * 24
                        ]
                    }
                }
                // console.log({ dataFilter });
                // console.log({latestInjectTransaction:  dataFilter['$project'].latestInjectTransaction });
                let listTransaction = await TRANSACTION_COLL.aggregate([
                    {
                        '$match':{
                            ...dataFilter['$match']
                        }
                    },
                    {
                        '$project':{
                            ...dataFilter['$project']
                        }
                    },
                    {
                      '$lookup': {
                        'from': 'customers', 
                        'localField': 'customer', 
                        'foreignField': '_id', 
                        'as': 'customer'
                      }
                    }
                    , {
                      '$lookup': {
                        'from': 'inject_transactions', 
                        'localField': 'latestInjectTransaction', 
                        'foreignField': '_id', 
                        'as': 'latestInjectTransaction'
                      }
                    },
                    {
                        '$sort': {
                            'modifyAt': -1
                        }
                    }
                ])
               
                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListCheckDateAll({ }) {
        return new Promise(async resolve => {
            try {
                const STATUS_ACTIVE = 1;
                let dataFilter  = {
                    '$match': {status: STATUS_ACTIVE},
                    '$project': {}
                };
                
                dataFilter['$project'] = {
                    ...dataFilter['$project'],
                    'customer': 1, 
                    'loanAmount': 1, 
                    'code': 1, 
                    'status': 1, 
                    'expireTime': 1, 
                    'latestInjectTransaction': 1,
                    'interestStartDate': 1, 
                    'dateDifference': {
                        '$divide': [
                        {
                            '$subtract': [
                                '$expireTime', new Date()
                            ]
                        }, 1000 * 60 * 60 * 24
                        ]
                    }
                }
                // console.log({ dataFilter });
                // console.log({latestInjectTransaction:  dataFilter['$project'].latestInjectTransaction });
                let listTransaction = await TRANSACTION_COLL.aggregate([
                    {
                        '$match':{
                            ...dataFilter['$match']
                        }
                    },
                    {
                        '$project':{
                            ...dataFilter['$project']
                        }
                    },
                    {
                      '$lookup': {
                        'from': 'customers', 
                        'localField': 'customer', 
                        'foreignField': '_id', 
                        'as': 'customer'
                      }
                    }
                    , {
                      '$lookup': {
                        'from': 'inject_transactions', 
                        'localField': 'latestInjectTransaction', 
                        'foreignField': '_id', 
                        'as': 'latestInjectTransaction'
                      }
                    }
                ])

                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // lấy danh sách 24h
    getListByAgencyID24H({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataFilter  = {
                    '$match': {
                      'agency': new ObjectID(agencyID)
                    },
                    '$project': {}
                };
                
                let arrTransactionID = [];
                let dataFindDateMainTrans = {};
                let dataFindDate = {};
                // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
                let start;
                let end;

                // let nowDate = new Date();
                // subStractDate({ date1: nowDate,  })

                if ( fromDay && !toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    // dataFindDateMainTrans.interestStartDate = { $gte: start };
                    dataFindDate.createAt = { $gte: start }
                }
                if ( !fromDay && toDay ){
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = { $lte: end };
                    dataFindDate.createAt = { $lte: end }
                }
                if ( fromDay && toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = {     
                    //     $gte: start, 
                    //     $lte:  end 
                    // };
                    dataFindDate.createAt = {     
                        $gte: start, 
                        $lte:  end }
                }
                if ( typeTransaction ){
                    if ( typeTransaction == 1){
                        if( dataFindDate.createAt ){
                            // dataFilter.createAt = dataFindDate.createAt;
                            dataFilter['$match'].interestStartDate = dataFindDate.createAt;
                        }
                        dataFilter['$match'].latestInjectTransaction = undefined;
                    }else{
                        dataFindDate.agency = agencyID;
                        dataFindDate.type   = typeTransaction;
                        if ( ransom ){
                            dataFindDate.ransom = ransom
                        }
                        let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate);
                        // console.log({ listInjectTransaction });
                        arrTransactionID = listInjectTransaction.map( item => ObjectID(item._id));
                        arrTransactionID = ["$latestInjectTransaction", [...new Set(arrTransactionID)]];
                        dataFilter['$project'].checkLastInjectTransaction = {
                            $in: arrTransactionID
                        }
                    }
                }else{
                    if( dataFindDate.createAt ){
                        dataFilter['$match'].interestStartDate = dataFindDate.createAt;
                    }
                }

                if ( typeStatusTransaction ){
                    dataFilter['$match'].status = Number(typeStatusTransaction);
                }
                dataFilter['$project'] = {
                    ...dataFilter['$project'],
                    'customer': 1, 
                    'loanAmount': 1, 
                    'code': 1, 
                    'status': 1, 
                    'expireTime': 1, 
                    'latestInjectTransaction': 1,
                    'interestStartDate': 1, 
                    'dateDifference': {
                        '$divide': [
                        {
                            '$subtract': [new Date(), '$interestStartDate']
                        }, 1000 * 60 * 60 * 24
                        ]
                    }
                }
                // console.log({latestInjectTransaction:  dataFilter['$project'].latestInjectTransaction });
                let listTransaction = await TRANSACTION_COLL.aggregate([
                    {
                        '$match':{
                            ...dataFilter['$match']
                        }
                    },
                    {
                        '$project':{
                            ...dataFilter['$project']
                        }
                    },
                    {
                      '$lookup': {
                        'from': 'customers', 
                        'localField': 'customer', 
                        'foreignField': '_id', 
                        'as': 'customer'
                      }
                    }
                    // , {
                    //   '$project': {
                    //     'customer': 1, 
                    //     'loanAmount': 1, 
                    //     'code': 1, 
                    //     'status': 1, 
                    //     'expireTime': 1, 
                    //     'interestStartDate': 1, 
                    //     'latestInjectTransaction': 1, 
                    //     'dateDifference': {
                    //       '$divide': [
                    //         {
                    //           '$subtract': [
                    //             '$expireTime', new Date()
                    //           ]
                    //         }, 1000 * 60 * 60 * 24
                    //       ]
                    //     }
                    //   }
                    // }
                    , {
                      '$lookup': {
                        'from': 'inject_transactions', 
                        'localField': 'latestInjectTransaction', 
                        'foreignField': '_id', 
                        'as': 'latestInjectTransaction'
                      }
                    }
                ])

                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     *  API lấy những trường cần thiết cho mobile
     *  SONLP
     */
    getListByAgencyIDMobile({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataFilter  = { agency: agencyID };
                let arrTransactionID = [];
                let dataFindDateMainTrans = {};
                let dataFindDate = {};
                // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
                let start;
                let end;
                if ( fromDay && !toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    dataFindDateMainTrans.interestStartDate = { $gte: start };
                    dataFindDate.createAt = { $gte: start }
                }
                if ( !fromDay && toDay ){
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFindDateMainTrans.interestStartDate = { $lte: end };
                    dataFindDate.createAt = { $lte: end }
                }
                if ( fromDay && toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFindDateMainTrans.interestStartDate = {     
                        $gte: start, 
                        $lte:  end 
                    };
                    dataFindDate.createAt = {     
                        $gte: start, 
                        $lte:  end }
                }
                if ( typeTransaction ){
                    if ( typeTransaction == 1){
                        if( dataFindDate.createAt ){
                            dataFilter.interestStartDate = dataFindDate.createAt;
                        }
                        dataFilter.latestInjectTransaction = undefined;
                    }else{
                        dataFindDate.agency = agencyID;
                        dataFindDate.type   = typeTransaction;
                        if ( ransom ){
                            dataFindDate.ransom = ransom
                        }
                        let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate);
                        // console.log({ listInjectTransaction });
                        arrTransactionID = listInjectTransaction.map( item => item._id );
                        arrTransactionID = [...new Set(arrTransactionID)];
                        // console.log({ arrTransactionID });
                        dataFilter.latestInjectTransaction = {
                            $in: arrTransactionID
                        }
                    }
                }else{
                    if( dataFindDate.createAt ){
                        dataFilter.interestStartDate = dataFindDate.createAt;
                    }
                    // dataFilter.latestInjectTransaction = undefined;
                }

                if ( typeStatusTransaction ){
                    dataFilter.status = typeStatusTransaction
                }
                // select trường cần lấy
                let listTransaction = await TRANSACTION_COLL.find({
                    ...dataFilter,
                }, { interestStartDate: 1, code: 1, status: 1, latestInjectTransaction: 1})
                    .populate({
                        path: "customer latestInjectTransaction"
                    })
                    .sort({ modifyAt: -1});
                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByAgencyWithStatus({ agencyID, status }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.find({ agency: agencyID, status })
                .populate({
                    path: "products injectTransaction customer"
                })
                .sort({ modifyAt: -1 });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    /**
     * Danh sách giao dịch của khách hàng
     */
    getListByCustomer({ agencyID, customerID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID) || !ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });
                    
                let infoAfterUpdate = await TRANSACTION_COLL.find({ agency: agencyID, customer: customerID })
                .populate({
                    path: "latestInjectTransaction customerImages KYCImages receiptImages formVerificationImages"
                })
                .populate({
                    path : "injectTransaction",
                    populate: {
                        path: "customerImages KYCImages receiptImages formVerificationImages",
                        select: "_id name path"
                    }
                })
                .populate({
                    path : "products",
                    select: "_id name price",
                    populate: {
                        path: "type",
                        select: "_id name"
                    }
                })
                .populate({
                    path : "qrCode",
                    select: "_id image",
                    populate: {
                        path: "image",
                        select: "_id path"
                    }
                })
                ;
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkInterestDateTransaction({ agencyID, transactionID, TYPE_PRODUCT, loanAmount }) {
        return new Promise(async resolve => {
            try {
                // 1// Lấy ra danh sách các giao dịch bình thường đã trả trước
                let infoDataFind           = await PAY_INTEREST_TRANSACTION_MODEL.getInfoByTransactionID({ transactionID: transactionID, type: 2, status: 1 });
                let listPayInterest        = infoDataFind.data;
                
                // Danh sách ID của GD Bình thường đã trả trước còn hoạt động
                let listPayInterestID      = listPayInterest.map( payInterest => payInterest._id);
                
                // 2// Tính khoảng cách giữa 2 ngày và tính ra lãi
                let checkDateNow = new Date();
                let totalPayInterest = 0;
                for (let payInterest of listPayInterest) {
                    console.log({ payInterest });
                    /**
                     * EXAMPLE:
                     *      => TRẢ 2 CHU KÌ
                     *                                        (new Date())
                     * (interestStartDate)                     (Vay thêm)          (expireTime)  
                     *       |---------------------------|----|-----|-------------------|
                     *     (1/8)                       (30/8) |   (2/9 )             (30/9)
                     *                                        |     | (Số tiền còn lại) |
                     *                                        |-----|-------------------|
                     *     (Reset về ngày vay thêm)<----------|     |    
                     *                                      (1/9)   |---->(Reset về ngày trả bớt)
                     *                          
                     */
                    let expireTimePayInterest   = new Date(payInterest.expireTime);
                    let interestDatePayInterest = new Date(payInterest.resetDate);
                    let interestRateTotal       = payInterest.interestRateTotal;
                    console.log({ interestDatePayInterest, interestRateTotal, expireTimePayInterest });

                     /**
                     * EXAMPLE:
                     *      => TRẢ 2 CHU KÌ
                     *                                        (new Date())
                     * (interestStartDate)                     (Vay thêm)            (expireTime)  
                     *       |---------------------------|----|-----|----------------------|
                     *     (1/8)                       (30/8)     (2/9 )                 (30/9)
                     *                                              |        (5//)         |
                     *                                              |checkDateToPayInterest|
                     *       |------------------interestRateTotal--------------------------|   
                     * 
                     *       interestRateTotal - checkDateToPayInterestOld = totalPayInterest            
                     */
                    // 3// Tính giới hạn giữa ngày hiện tại với Ngày thực hiện giao dịch bổ sung
                    let countBetweenTwoDay = subStractDateGetDay({ date1: expireTimePayInterest, date2: checkDateNow });
                    countBetweenTwoDay     = Math.floor(countBetweenTwoDay) + 1;
                    // console.log({ countBetweenTwoDay, interestRateTotal });
                    
                    // 5// Tính lãi của khoảng thời gian đó 
                    let checkDateToPayInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate: countBetweenTwoDay, price: loanAmount, typeProduct: TYPE_PRODUCT});
                    // console.log({ checkDateToPayInterest });

                     /**
                     * 6// CỘNG VÀO TỔNG TIỀN VỚI CÁC GIAO DỊCH TRẢ TRƯỚC ĐƯỢC TÌM THẤY
                     */
                    checkDateToPayInterest.data.forEach( price => {
                        totalPayInterest += price
                    });
                }
                // 6// Update lại số tiền trả trước còn lại
                if (totalPayInterest) {
                    let infoDataAfterUpdate        = await this.updatePayInterest({ transactionID, payInterest: totalPayInterest });
                    if(infoDataAfterUpdate.error) {
                        return resolve(infoDataAfterUpdate);
                    }

                    // 7// Tắt hoạt động các GD Bình Thường Trả Trước
                    let listPayInterestAfterUpdate = listPayInterestID.map( payInterest => {
                        return PAY_INTEREST_TRANSACTION_MODEL.update({ transactionID: payInterest, status: 0 });
                    });
                    let listAfterChange = await Promise.all(listPayInterestAfterUpdate);
                }
                
                // for (let payInterestID of listPayInterestID) {
                //     let infoPayInterestAfterUpdate = await PAY_INTEREST_TRANSACTION_MODEL.update({ transactionID: payInterestID, status: 0 });
                // }
                
                return resolve(totalPayInterest);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    getPricePayInterest({ agencyID, transactionID, TYPE_PRODUCT, loanAmount }) {
        return new Promise(async resolve => {
            try {
                // 1// Lấy ra danh sách các giao dịch bình thường đã trả trước
                let infoDataFind           = await PAY_INTEREST_TRANSACTION_MODEL.getInfoByTransactionID({ transactionID: transactionID, type: 2, status: 1 })
                let listPayInterest        = infoDataFind.data;
                // Danh sách ID của GD Bình thường đã trả trước còn hoạt động
                // let listPayInterestID      = listPayInterest.map( payInterest => payInterest._id);
                
                // 2// Tính khoảng cách giữa 2 ngày và tính ra lãie
                // console.log({ infoDataFind });
                let checkDateNow = new Date();
                let totalPayInterest = 0;
                for (let payInterest of listPayInterest) {
                    // console.log({ payInterest });
                    /**
                     * EXAMPLE:
                     *      => TRẢ 2 CHU KÌ
                     *                                        (new Date())
                     * (interestStartDate)                     (Vay thêm)          (expireTime)  
                     *       |---------------------------|----|-----|-------------------|
                     *     (1/8)                       (30/8) |   (2/9 )             (30/9)
                     *                                        |     | (Số tiền còn lại) |
                     *                                        |-----|-------------------|
                     *     (Reset về ngày vay thêm)<----------|     |    
                     *                                      (1/9)   |---->(Reset về ngày trả bớt)
                     *                          
                     */
                    let expireTimePayInterest   = new Date(payInterest.expireTime);
                    let interestDatePayInterest = new Date(payInterest.resetDate);
                    let interestRateTotal       = payInterest.interestRateTotal;
                    // console.log({ interestDatePayInterest, interestRateTotal, expireTimePayInterest });

                     /**
                     * EXAMPLE:
                     *      => TRẢ 2 CHU KÌ
                     *                                        (new Date())
                     * (interestStartDate)                     (Vay thêm)            (expireTime)  
                     *       |---------------------------|----|-----|----------------------|
                     *     (1/8)                       (30/8)     (2/9 )                 (30/9)
                     *                                              |        (5//)         |
                     *                                              |checkDateToPayInterest|
                     *       |------------------interestRateTotal--------------------------|   
                     * 
                     *       interestRateTotal - checkDateToPayInterestOld = totalPayInterest            
                     */
                    // 3// Tính giới hạn giữa ngày hiện tại với Ngày thực hiện giao dịch bổ sung
                    let countBetweenTwoDay = subStractDateGetDay({ date1: expireTimePayInterest, date2: checkDateNow });
                    countBetweenTwoDay     = Math.floor(countBetweenTwoDay) + 1;
                    // console.log({ countBetweenTwoDay, interestRateTotal });
                    
                    // 5// Tính lãi của khoảng thời gian đó 
                    let checkDateToPayInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate: countBetweenTwoDay, price: loanAmount, typeProduct: TYPE_PRODUCT});
                    // console.log({ checkDateToPayInterest });

                     /**
                     * 6// CỘNG VÀO TỔNG TIỀN VỚI CÁC GIAO DỊCH TRẢ TRƯỚC ĐƯỢC TÌM THẤY
                     */
                    checkDateToPayInterest.data.forEach( price => {
                        totalPayInterest += price
                    });
                }
                
                // if(!totalPayInterest)
                //     return resolve({ error: true, message: "cannot_update_transaction" });
                return resolve({ error: false, data: totalPayInterest });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    insertInjectTransactionInterest({ 
        meta, objUpdateTransactionMain, payInterest, transactionID, objPayInterest, objInject, customerName, typeFinancial,
        listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages, userID, objHistory, objFinancial
    }) {
        return new Promise(async resolve => {
            try {
                let infoPriceAfterChange = calculateBetweentTwoPrice({ price1: meta.priceMuchPayInterest, price2: payInterest});

                /**
                 *  4// SET LẠI GIÁ CỦA SỐ TIỀN TRẢ LÃI VÀ SỐ TIỀN CÒN DƯ
                 */
                objInject.meta.priceMuchPayInterest             = infoPriceAfterChange.price1;

                objUpdateTransactionMain = {
                    ...objUpdateTransactionMain,
                    payInterest: infoPriceAfterChange.price2
                }
                /**
                 * KIỂM TRA GIÁ KHUYẾN MÃI CÓ TỒN TẠI VÀ < SỐ TIỀN LÃI
                 */
                if (objInject.meta.promotionInterest && objInject.meta.promotionInterest > objInject.meta.priceMuchPayInterest) {
                    return resolve({ error: true, message: "promotionInterest_invalid" });
                }else if (objInject.meta.promotionInterest) {
                    objInject.meta.priceMuchPayInterest = objInject.meta.priceMuchPayInterest - Number(meta.promotionInterest)
                }

                let dataUpdate = {}
                if(listUrlCustomerImages && listUrlCustomerImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlCustomerImages, userCreate: userID });
                    dataUpdate.customerImages    = listIDImage;
                }
                if(listUrlKYCImages && listUrlKYCImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlKYCImages, userCreate: userID });
                    dataUpdate.KYCImages         = listIDImage;
                }
                if(listUrlReceiptImages && listUrlReceiptImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                    dataUpdate.receiptImages     = listIDImage;
                }
                if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                    dataUpdate.formVerificationImages = listIDImage;
                }
                console.log({
                    dataUpdate
                });
                /**
                 * CHECK QUỸ KH ĐỦ
                 */
                // if (funds < obj.meta.priceMuchPayInterest) {
                //     return return resolve({ error: true, message: "fund_not_enough"});
                // }
                // console.log({ objUpdateTransactionMain, meta, objInject, objPayInterest });
                let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                    ...objInject, dataUpdate
                });
                if(infoAfterInsert.error) {
                    return resolve(infoAfterInsert);
                }
                // SET NAME THÊM VÀO QUỸ
                let nameOfFinanical = `${infoAfterInsert.data.code} + ` + `${customerName} + ` + objInject.title + ` + ${objInject.meta.priceMuchPayInterest}`;

                /**
                *  // INSERT VÀO COLLECTION TRẢ TRƯỚC 
                */
                if (!isEmptyObject(objPayInterest)) {
                    objPayInterest = {
                        ...objPayInterest,
                        transaction: infoAfterInsert.data._id,
                    }
                    // console.log({ objPayInterest });
                    let infoPayInterestTransaction = await PAY_INTEREST_TRANSACTION_MODEL.insert({
                        ...objPayInterest
                    });
                    // console.log({ infoPayInterestTransaction });
                }
                // Update lại số tiền vay 
                let infoDataAfterUpdate = await this.update({ ...objUpdateTransactionMain });
                if(infoDataAfterUpdate.error) {
                    return resolve(infoDataAfterUpdate);
                }
                console.log({ infoDataAfterUpdate });
                /**
                 *  INSERT HISTORY TRANSACTION
                 */
                const TYPE_NORMAL = 2;
                let infoHistoryTransactionAfterInsert = await HISTORY_TRANSACTION_MODEL.insert({
                    ...objHistory,
                    type: TYPE_NORMAL,
                    transaction: infoAfterInsert.data._id,
                });

                objFinancial = {
                    ...objFinancial,
                    price:  objInject.meta.priceMuchPayInterest, 
                }
                // UPDATE LẠI QUỸ
                let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                    title:  nameOfFinanical, 
                    typeTransaction: TYPE_NORMAL,
                    transaction: infoAfterInsert.data._id,
                    ...objFinancial
                });

                if (infoFinancialAfterInsert.error) {
                    return resolve(infoFinancialAfterInsert)
                }

                return resolve(infoAfterInsert);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    updateInjectTransaction({ 
        objUpdateTransactionMain, payInterest, objPayInterest, objInject,
        objHistory, objFinancial, injectTransID, title, type
    }) {
        return new Promise(async resolve => {
            try {

                // console.log({
                //     objUpdateTransactionMain, payInterest, objPayInterest, objInject, customerName,
                //     objHistory, objFinancial, injectTransID
                // });
                const TYPE_NORMAL = 2;
                if (type == TYPE_NORMAL) {
                    // console.log({
                    //     objInject
                    // });
                    let infoPriceAfterChange = calculateBetweentTwoPrice({ price1: objInject.meta.priceMuchPayInterest, price2: payInterest});
                    // console.log({
                    //     objInject
                    // });
                    /**
                     *  4// SET LẠI GIÁ CỦA SỐ TIỀN TRẢ LÃI VÀ SỐ TIỀN CÒN DƯ
                     */
                    // objInject.meta.priceMuchPayInterest             = infoPriceAfterChange.price1;
                    objUpdateTransactionMain = {
                        ...objUpdateTransactionMain,
                        payInterest: infoPriceAfterChange.price2
                    }
                  
                    /**
                    *  // INSERT VÀO COLLECTION TRẢ TRƯỚC 
                    */
                    if (objPayInterest && !isEmptyObject(objPayInterest)) {
                        objPayInterest = {
                            ...objPayInterest,
                            transaction: injectTransID,
                        }
                        console.log({ objPayInterest });
                        let infoPayInterestTransaction = await PAY_INTEREST_TRANSACTION_MODEL.insert({
                            ...objPayInterest
                        });
                    }
                }

                // Update lại số tiền vay 
                let infoDataAfterUpdate = await this.update({ ...objUpdateTransactionMain });
                if(infoDataAfterUpdate.error) {
                    return resolve(infoDataAfterUpdate);
                }
                /**
                 *  INSERT HISTORY TRANSACTION
                 */
                let infoHistoryTransactionAfterInsert = await HISTORY_TRANSACTION_MODEL.insert({
                    ...objHistory,
                    type: type,
                    transaction: injectTransID,
                });

                // UPDATE LẠI QUỸ
                let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                    title:  title, 
                    typeTransaction: type,
                    transaction: injectTransID,
                    ...objFinancial
                });
                console.log({ infoFinancialAfterInsert, infoHistoryTransactionAfterInsert, infoDataAfterUpdate });
                if (infoFinancialAfterInsert.error) {
                    return resolve(infoFinancialAfterInsert)
                }

                return resolve(infoDataAfterUpdate);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    insertInjectTransaction({ 
        meta, payInterest, objInject, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages, userID, 
    }) {
        return new Promise(async resolve => {
            try {
                let infoPriceAfterChange = calculateBetweentTwoPrice({ price1: meta.priceMuchPayInterest, price2: payInterest});

                /**
                 *  4// SET LẠI GIÁ CỦA SỐ TIỀN TRẢ LÃI VÀ SỐ TIỀN CÒN DƯ
                 */
                objInject.meta.priceMuchPayInterest             = infoPriceAfterChange.price1;

                /**
                 * KIỂM TRA GIÁ KHUYẾN MÃI CÓ TỒN TẠI VÀ < SỐ TIỀN LÃI
                 */
                if (objInject.meta.promotionInterest && objInject.meta.promotionInterest > objInject.meta.priceMuchPayInterest) {
                    return resolve({ error: true, message: "promotionInterest_invalid" });
                }else if (objInject.meta.promotionInterest) {
                    objInject.meta.priceMuchPayInterest = objInject.meta.priceMuchPayInterest - Number(meta.promotionInterest)
                }

                let dataUpdate = {}
                if(listUrlCustomerImages && listUrlCustomerImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlCustomerImages, userCreate: userID });
                    dataUpdate.customerImages    = listIDImage;
                }
                if(listUrlKYCImages && listUrlKYCImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlKYCImages, userCreate: userID });
                    dataUpdate.KYCImages         = listIDImage;
                }
                if(listUrlReceiptImages && listUrlReceiptImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                    dataUpdate.receiptImages     = listIDImage;
                }
                if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                    let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                    dataUpdate.formVerificationImages = listIDImage;
                }
                /**
                 * CHECK QUỸ KH ĐỦ
                 */
                // if (funds < obj.meta.priceMuchPayInterest) {
                //     return return resolve({ error: true, message: "fund_not_enough"});
                // }
                let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                    ...objInject, dataUpdate
                });

                // let infoTransactionAfterUpdate = await TRANSACTION_COLL.findByIdAndUpdate(objInject.transaction, {
                //     modifyAt: new Date()
                // });

                return resolve(infoAfterInsert);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    updateImage({ receiptImagesUpdate, billImagesTransUpdate, transactionID, userID }) {
        return new Promise(async resolve => {
            try {
                // console.log({
                //     receiptImagesUpdate, billImagesTransUpdate, transactionID
                // });
                if (!checkObjectIDs(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let condition = {};
                if (receiptImagesUpdate && receiptImagesUpdate.length) {
                    let { data: listImageUpdate }  = await IMAGE_MODEL.insertArr({ arrImage: receiptImagesUpdate, userCreate: userID })
                    condition.$addToSet = {
                        ...condition.$addToSet,
                        receiptImages: listImageUpdate
                    }
                }
                if (billImagesTransUpdate && billImagesTransUpdate.length) {
                    let { data: listImageUpdate }  = await IMAGE_MODEL.insertArr({ arrImage: billImagesTransUpdate, userCreate: userID })
                    condition.$addToSet = {
                        ...condition.$addToSet,
                        billImages: listImageUpdate
                    }
                }
                // console.log({
                //     condition
                // });
                let infoAfterUpdate = await TRANSACTION_COLL.findByIdAndUpdate(transactionID, { 
                    ...condition
                }, {
                    new: true
                });
                // console.log({
                //     infoAfterUpdate
                // });
               
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_update" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // lấy ra danh sách đại lý gồm chính nó và con của nó
    getAgencyAndListAgencyChilds({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

				let objSearchAgency = {
                    $or: [
                        { _id: agencyID },
                        { parent: agencyID }
                    ]
                }
                let listAgency = await AGENCY_COLL.find({ 
                    ...objSearchAgency, 
                    status: 1
                });

                if(!listAgency) 
                    return resolve({ error: true, message: "cannot_get_list_agency" });

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: DOANH SỐ BÁN HÀNG
     */
     getListTransactionRevene({ agencyID, typeProductCategoryID, start, end, name, city, district, ward, status, agencyFilter }) {
        return new Promise(async resolve => {
            try {
                if(agencyID) {
                    if(!ObjectID.isValid(agencyID))
                        return resolve({ error: true, message: 'params_invalid' });
                }

                let listAgencyChilds = await AGENCY_COLL.find({ parent: agencyID });
                let arrIDAgencyChild = [];
                let arrSearchAgency = [];

                // lấy ra ID của các Agency con
                if(listAgencyChilds.length > 0) {
                    arrIDAgencyChild = listAgencyChilds.map(agency => agency._id.toString());
                    arrSearchAgency = [...arrIDAgencyChild];
                }

                let conditionAgency = {
                    $or: [
                        { _id: agencyID },
                        { parent: agencyID }
                    ]
                };

                arrSearchAgency.push(agencyID);
                let condition = {
                    agency: { $in: arrSearchAgency }
                };

                if(city && city != "null"){
                    conditionAgency.city = city;
                }
 
                if(district && district != "null"){
                    conditionAgency.district = district;
                }
 
                if(ward && ward != "null"){
                    conditionAgency.ward = ward;
                }

                if (city || district || ward) {
                    let listAgencyByAddress = await AGENCY_COLL.find({ ...conditionAgency, status: 1 });
                    condition = {
                        ...condition,
                        agency: {
                            $in: [...listAgencyByAddress.map(agency => agency._id)]
                        }
                    }
                }

                // if (agencyID) {
                //     if (!checkObjectIDs(agencyID)) {
                //         return res.json({ error: true, message: "params_invalid" });
                //     }
                //     condition = {
                //         ...condition,
                //         agency: agencyID
                //     }
                // }
                 
                 
                if (typeProductCategoryID && checkObjectIDs(typeProductCategoryID)) {
                    let listProductWithType = await PRODUCT_MODEL.getListProductWithType({ typeProductCategoryID });
                    
                    let arrProduct = listProductWithType && listProductWithType.data && listProductWithType.data.length
                        && listProductWithType.data.map(item => {
                            return item._id
                        });
                    if(arrProduct.length){
                        condition.products = { $in: arrProduct }
                    }

                    condition.category = typeProductCategoryID;
                 }
                 
                /**
                 * Trạng thái
                 * 0: Đang xử lý - chờ duyệt
                 * 1: Đang cầm - Đã duyệt
                 * 2: Không duyệt
                 * 3: Hoàn thành giao dịch
                 */ 
                const SORT_VALUE_VALID = [0, 1, 2, 3];
                if (status && !Number.isNaN(Number(status)) && SORT_VALUE_VALID.includes(Number(status))) {
                    condition.status = status;
                }
 
                if(start){
                    let _fromDate   = moment(start).startOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $gte: new Date(_fromDate),
                    }
                 }

                 if(end){
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $lt: new Date(_toDate)
                    }
                }
                
                if(agencyFilter)
                    condition.agency = agencyFilter;
                /**
                 * 1//  DANH SÁCH GIAO DỊCH
                 */
                let listTransaction = await TRANSACTION_COLL.find({ ...condition }, 
                    {
                        loanAmount: 1, 
                        status: 1, 
                        _id: 1, 
                        totalMoneyNumber: 1, 
                        createAt: 1, 
                        agency: 1,
                        note: 1

                    })
                    .sort({ modifyAt: -1 });
                /**
                 * 2// LẤY RA THÔNG TIN CỦA TỪNG GIAO DỊCH
                 *      *1: Forr mảng danh sách
                 *      *2: TÌm ID Transaction trong QUỸ
                 *      *3: 
                 *          => CỘNG SỐ TIỀN VAY THÊM
                 *          => CỘNG SỐ TIỀN TRẢ BỚT
                 *          => CỘNG SỐ TIỀN TRẢ LÃI
                 *          => CỘNG SỐ TIỀN CHUỘC ĐỒ
                 */

                /**
                 * CONSTANT INJECT TRANSACTION :
                 *      1// TYPE_TRANSACTION_NORMMAL:     2 (GIA HẠN BÌNH THƯỜNG)
                 *      2// TYPE_TRANSACTION_BORROW:      3 (GIA HẠN VAY THÊM)
                 *      3// TYPE_TRANSACTION_PAY:         4 (GIA HẠN TRẢ BỚT)
                 *      4// TYPE_TRANSACTION_RANSOM:      6 (GIA HẠN CHUỘC ĐỒ)
                 */
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                let listTransactionAfterChange = [];

                /**
                 *  1// TỔNG TIỀN VAY THÊM (calculPriceBorrow)
                 *  2// TỔNG TIỀN TRẢ BỚT  (calculPricePay)
                 *  3// TỔNG TIỀN TRÃ LÃI  (calculPriceNormal)
                 *  4// TỔNG TIỀN CHUỘC ĐỒ (calculPriceRansom)
                 *  5// TỔNG TIỀN CẦM ĐỒ   (calculPricePawn)
                 */
                let calculPriceBorrow  = 0;
                let calculPricePay     = 0;
                let calculPriceNormal  = 0;
                let calculPriceRansom  = 0;
                let calculPricePawn    = 0;
                let interestAndFeeAllInjectTransaction = 0;
                let totalInterestAllTransaction = 0;
                for (let transaction of listTransaction) {
                    calculPricePawn += transaction.totalMoneyNumber;
                    let { data: typeTransactionBorrow } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_BORROW,
                    });
                    calculPriceBorrow += typeTransactionBorrow.totalPrice;

                    let { data: typeTransactionPay } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_PAY,
                    });
                    calculPricePay += typeTransactionPay.totalPrice;

                    let { data: typeTransactionNormal } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_NORMMAL,
                    });
                    calculPriceNormal += typeTransactionNormal.totalPrice;

                    let { data: typeTransactionRansom } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_RANSOM,
                    });
                    calculPriceRansom += typeTransactionRansom.totalPrice;

                    interestAndFeeAllInjectTransaction = typeTransactionBorrow.totalInterestAndFee + typeTransactionPay.totalInterestAndFee + typeTransactionNormal.totalInterestAndFee + typeTransactionRansom.totalInterestAndFee;
                    totalInterestAllTransaction += interestAndFeeAllInjectTransaction;

                    let obj = {
                        ...transaction._doc,
                        totalPriceBorrow: typeTransactionBorrow.totalPrice,
                        totalPricePay: typeTransactionPay.totalPrice,
                        totalPriceNormal: typeTransactionNormal.totalPrice,
                        totalPriceRansom: typeTransactionRansom.totalPrice,
                        interestAndFeeAllInjectTransaction
                    }
                    /**
                     * LẤY AGENCY CỦA TRANSACTION
                     */
                    let agencyOfTransaction = await AGENCY_COLL.findById(transaction.agency, { name: 1 }).populate({
                        path: "parent",
                        select: "_id name"
                    });
                    if (agencyOfTransaction) {
                        obj = {
                            ...obj,
                            name: agencyOfTransaction.name
                        }
                        if (agencyOfTransaction.parent) {
                            obj = {
                                ...obj,
                                nameParent: agencyOfTransaction.parent.name
                            }
                        }
                    }

                    /**
                     * LẤY THÔNG TIN QUỸ
                     */
                    let infoFinancial = await FINANCIAL_COLL.find({ transaction: transaction._id, onModel: "transaction" }, { status: 1 });
                    
                    let nameFinancial = "";
                    if (infoFinancial.length) {
                        nameFinancial = infoFinancial[0].status == 1 ? "Quỹ tiền mặt" : "Quỹ Ngân Hàng";
                    }
                    listTransactionAfterChange.push({
                        ...obj, nameFinancial
                    });
                }
                // console.log({ listTransactionAfterChange });

                if(!listTransactionAfterChange) 
                    return resolve({ error: true, message: "cannot_get_list_transaction" });

                return resolve({ error: false, data: { listTransaction: listTransactionAfterChange, calculPriceBorrow, calculPricePay, calculPriceNormal, calculPriceRansom, calculPricePawn, totalInterestAllTransaction} });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListTransactionReveneExportExcel({ agencyID, typeProductCategoryID, start, end, name, city, district, ward, status, agencyFilter }) {
        return new Promise(async resolve => {
            try {
                if(agencyID) {
                    if(!ObjectID.isValid(agencyID))
                        return resolve({ error: true, message: 'params_invalid' });
                }

                let listAgencyChilds = await AGENCY_COLL.find({ parent: agencyID });
                let arrIDAgencyChild = [];
                let arrSearchAgency = [];

                // lấy ra ID của các Agency con
                if(listAgencyChilds.length > 0) {
                    arrIDAgencyChild = listAgencyChilds.map(agency => agency._id.toString());
                    arrSearchAgency = [...arrIDAgencyChild];
                }

                let conditionAgency = {
                    $or: [
                        { _id: agencyID },
                        { parent: agencyID }
                    ]
                };

                arrSearchAgency.push(agencyID);
                let condition = {
                    agency: { $in: arrSearchAgency }
                };

                if(city && city != "null"){
                    conditionAgency.city = city;
                }
 
                if(district && district != "null"){
                    conditionAgency.district = district;
                }
 
                if(ward && ward != "null"){
                    conditionAgency.ward = ward;
                }

                if (city || district || ward) {
                    let listAgencyByAddress = await AGENCY_COLL.find({ ...conditionAgency, status: 1 });
                    condition = {
                        ...condition,
                        agency: {
                            $in: [...listAgencyByAddress.map(agency => agency._id)]
                        }
                    }
                }

                // if (agencyID) {
                //     if (!checkObjectIDs(agencyID)) {
                //         return res.json({ error: true, message: "params_invalid" });
                //     }
                //     condition = {
                //         ...condition,
                //         agency: agencyID
                //     }
                // }
                 
                 
                if (typeProductCategoryID && checkObjectIDs(typeProductCategoryID)) {
                    let listProductWithType = await PRODUCT_MODEL.getListProductWithType({ typeProductCategoryID });
                    
                    let arrProduct = listProductWithType && listProductWithType.data && listProductWithType.data.length
                        && listProductWithType.data.map(item => {
                            return item._id
                        });
                    if(arrProduct.length){
                        condition.products = { $in: arrProduct }
                    }

                    condition.category = typeProductCategoryID;
                 }
                 
                /**
                 * Trạng thái
                 * 0: Đang xử lý - chờ duyệt
                 * 1: Đang cầm - Đã duyệt
                 * 2: Không duyệt
                 * 3: Hoàn thành giao dịch
                 */ 
                const SORT_VALUE_VALID = [0, 1, 2, 3];
                if (status && !Number.isNaN(Number(status)) && SORT_VALUE_VALID.includes(Number(status))) {
                    condition.status = status;
                }
 
                if(start){
                    let _fromDate   = moment(start).startOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $gte: new Date(_fromDate),
                    }
                 }

                 if(end){
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $lt: new Date(_toDate)
                    }
                }
                
                if(agencyFilter)
                    condition.agency = agencyFilter;
                /**
                 * 1//  DANH SÁCH GIAO DỊCH
                 */
                // console.log({
                //     condition
                // });
                let listTransaction = await TRANSACTION_COLL.find({ ...condition }, {
                    loanAmount: 1, 
                    status: 1, 
                    _id: 1, 
                    totalMoneyNumber: 1, 
                    createAt: 1, 
                    agency: 1,
                    approvalNote: 1,
                    latestInjectTransaction: 1,
                    customer: 1,
                    products: 1,
                    code: 1,
                    approvalLimit: 1,
                    transactionEmployee: 1,
                    injectTransaction: 1,
                    noteNotApproved: 1,
                })
                .populate({
                    path: 'customer',
                    select: 'code name phone address city district ward'
                })
                .populate({
                    path: 'products',
                    select: 'type name',
                    populate: {
                        path: 'type',
                        select: 'name '
                    }
                })
                .populate({
                    path: 'latestInjectTransaction',
                    select: 'type meta',
                })
                .populate({
                    path: 'injectTransaction',
                    select: 'type meta status ransom userConfirm userCreateAt note noteNotApproved createAt',
                    populate: {
                        path: 'userConfirm userCreateAt',
                        select: 'username'
                    }
                })
                .populate({
                    path: 'agency',
                    select: 'code name phone address email city district ward',
                    populate: {
                        path: 'owners parent',
                        select: 'username email code'
                    }
                })
                .populate({
                    path: 'transactionEmployee',
                    select: 'username',
                })
                .populate({
                    path: 'ownerConfirm',
                    select: 'username',
                })
                .sort({ modifyAt: -1 });

                // console.log({
                //     listTransaction
                // });
                /**
                 * 2// LẤY RA THÔNG TIN CỦA TỪNG GIAO DỊCH
                 *      *1: Forr mảng danh sách
                 *      *2: TÌm ID Transaction trong QUỸ
                 *      *3: 
                 *          => CỘNG SỐ TIỀN VAY THÊM
                 *          => CỘNG SỐ TIỀN TRẢ BỚT
                 *          => CỘNG SỐ TIỀN TRẢ LÃI
                 *          => CỘNG SỐ TIỀN CHUỘC ĐỒ
                 */

                /**
                 * CONSTANT INJECT TRANSACTION :
                 *      1// TYPE_TRANSACTION_NORMMAL:     2 (GIA HẠN BÌNH THƯỜNG)
                 *      2// TYPE_TRANSACTION_BORROW:      3 (GIA HẠN VAY THÊM)
                 *      3// TYPE_TRANSACTION_PAY:         4 (GIA HẠN TRẢ BỚT)
                 *      4// TYPE_TRANSACTION_RANSOM:      6 (GIA HẠN CHUỘC ĐỒ)
                 */
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                let listTransactionAfterChange = [];

                /**
                 *  1// TỔNG TIỀN VAY THÊM (calculPriceBorrow)
                 *  2// TỔNG TIỀN TRẢ BỚT  (calculPricePay)
                 *  3// TỔNG TIỀN TRÃ LÃI  (calculPriceNormal)
                 *  4// TỔNG TIỀN CHUỘC ĐỒ (calculPriceRansom)
                 *  5// TỔNG TIỀN CẦM ĐỒ   (calculPricePawn)
                 */
                let calculPriceBorrow  = 0;
                let calculPricePay     = 0;
                let calculPriceNormal  = 0;
                let calculPriceRansom  = 0;
                let calculPricePawn    = 0;
                let interestAndFeeAllInjectTransaction = 0;
                let totalInterestAllTransaction = 0;
                for (let transaction of listTransaction) {
                    calculPricePawn += transaction.totalMoneyNumber;
                    let { data: typeTransactionBorrow } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_BORROW,
                    });
                    calculPriceBorrow += typeTransactionBorrow.totalPrice;

                    let { data: typeTransactionPay } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_PAY,
                    });
                    calculPricePay += typeTransactionPay.totalPrice;

                    let { data: typeTransactionNormal } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_NORMMAL,
                    });
                    calculPriceNormal += typeTransactionNormal.totalPrice;

                    let { data: typeTransactionRansom } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_RANSOM,
                    });
                    calculPriceRansom += typeTransactionRansom.totalPrice;

                    interestAndFeeAllInjectTransaction = typeTransactionBorrow.totalInterestAndFee + typeTransactionPay.totalInterestAndFee + typeTransactionNormal.totalInterestAndFee + typeTransactionRansom.totalInterestAndFee;
                    totalInterestAllTransaction += interestAndFeeAllInjectTransaction;

                    let obj = {
                        ...transaction._doc,
                        totalPriceBorrow: typeTransactionBorrow.totalPrice,
                        totalPricePay: typeTransactionPay.totalPrice,
                        totalPriceNormal: typeTransactionNormal.totalPrice,
                        totalPriceRansom: typeTransactionRansom.totalPrice,
                        interestAndFeeAllInjectTransaction
                    }
                    /**
                     * LẤY AGENCY CỦA TRANSACTION
                     */
                    let agencyOfTransaction = await AGENCY_COLL.findById(transaction.agency, { name: 1 }).populate({
                        path: "parent",
                        select: "_id name"
                    });
                    if (agencyOfTransaction) {
                        obj = {
                            ...obj,
                            name: agencyOfTransaction.name
                        }
                        if (agencyOfTransaction.parent) {
                            obj = {
                                ...obj,
                                nameParent: agencyOfTransaction.parent.name
                            }
                        }
                    }

                    /**
                     * LẤY THÔNG TIN QUỸ
                     */
                    let infoFinancial = await FINANCIAL_COLL.find({ transaction: transaction._id, onModel: "transaction" }, { status: 1 });
                    
                    let nameFinancial = "";
                    if (infoFinancial.length) {
                        nameFinancial = infoFinancial[0].status == 1 ? "Quỹ tiền mặt" : "Quỹ Ngân Hàng";
                    }
                    listTransactionAfterChange.push({
                        ...obj, nameFinancial
                    });
                }
                // console.log({ listTransactionAfterChange });

                if(!listTransactionAfterChange) 
                    return resolve({ error: true, message: "cannot_get_list_transaction" });

                return resolve({ error: false, data: { listTransaction: listTransactionAfterChange, calculPriceBorrow, calculPricePay, calculPriceNormal, calculPriceRansom, calculPricePawn, totalInterestAllTransaction} });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAgency() {
        return new Promise(async resolve => {
            try {
				
                let listAgency = await AGENCY_COLL.find({ status: 1 });

                if(!listAgency) 
                    return resolve({ error: true, message: "cannot_get_list_agency" });

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListProductOfTransaction({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'param_invalid' });

                let listProduct = await TRANSACTION_COLL.findById({ _id: transactionID });

                if(!listProduct) 
                    return resolve({ error: true, message: "cannot_get_list_product" });

                return resolve({ error: false, data: listProduct.products });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    resendRequestTransaction({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'param_invalid' });

                const STATUS_WAIT_APPROVE = 0;
                let infoTransactionAfterUpdate = await TRANSACTION_COLL.findByIdAndUpdate({ _id: transactionID }, {
                    status: STATUS_WAIT_APPROVE,
                    modifyAt: new Date()
                }); 

                if(!infoTransactionAfterUpdate) 
                    return resolve({ error: true, message: "cannot_update_transaction" });

                return resolve({ error: false, data: infoTransactionAfterUpdate.products });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListWithFilterAgency({ agencyID, listTransactionBefore }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'param_invalid' });

                if(listTransactionBefore.listTransaction.length > 0) {
                    listTransactionBefore.listTransaction = listTransactionBefore.listTransaction.map(transaction => {
                        transaction.agency = agencyID;
                    })
                }

                return resolve({ error: false, data: listTransactionBefore });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getDataToExportExcel({
        infoTransaction
    }) {
        return new Promise(async resolve => {
            try {
                let data = {}
                let addressAgency = '';
                let infoProvinceAgency   = COMMON_MODEL.getInfoProvince({ provinceCode: infoTransaction.agency.city }); 
                let infoDistrictAgency   = COMMON_MODEL.getInfoDistrict({ districtCode: infoTransaction.agency.district })
                let infoWardAgency       = await COMMON_MODEL.getInfoWard({ district: infoTransaction.agency.district, wardCode: infoTransaction.agency.ward })
                addressAgency   = infoTransaction.agency.address + `, ${infoWardAgency.data[1].name_with_type}` + `, ${infoDistrictAgency.data[1].name_with_type}` + `, ${infoProvinceAgency.data[1].name_with_type}` 
                
                let address = infoTransaction.customer.address ? infoTransaction.customer.address + ', ' : '';
                if (infoTransaction.customer.city) {
                    let infoProvinceCustomer = COMMON_MODEL.getInfoProvince({ provinceCode: infoTransaction.customer.city });
                    if (infoTransaction.customer.district) {
                        let infoDistrictCustomer = COMMON_MODEL.getInfoDistrict({ districtCode: infoTransaction.customer.district });
                        if (infoTransaction.customer.ward) {
                            let infoWardCustomer       = await COMMON_MODEL.getInfoWard({ district: infoTransaction.customer.district, wardCode: infoTransaction.customer.ward });
                            if (infoWardCustomer.data.length) {
                                address = address + `${infoWardCustomer.data[1].name_with_type}, ` + `${infoDistrictCustomer.data[1].name_with_type}, ` + `${infoProvinceCustomer.data[1].name_with_type}`;
                            } else {
                                address = address + `${infoDistrictCustomer.data[1].name_with_type}, ` + `${infoProvinceCustomer.data[1].name_with_type}`;
                            }
                        } else {
                            address = address + `${infoDistrictCustomer.data[1].name_with_type}, ` + `${infoProvinceCustomer.data[1].name_with_type}`;
                        }
                    } else {
                        
                        address = address + `${infoProvinceCustomer.data[1].name_with_type}`
                    }
                }
                //====================ĐẠI LÝ=======================
                let parentCode = '';
                if (infoTransaction.agency.parent) {
                    parentCode = infoTransaction.agency.parent.code;
                }
                let codeAgency        = infoTransaction.agency.code;
                let nameAgency        = infoTransaction.agency.name;
                let phoneAgency       = infoTransaction.agency.phone;
                // let addressAgency     = infoTransaction.agency.address;
                let emailAgency       = infoTransaction.agency.email;
                let createAt          = infoTransaction.createAt;
                data = {
                    ...data,
                    parentCode,
                    nameAgency,
                    codeAgency,
                    phoneAgency,
                    addressAgency,
                    emailAgency,
                    createAt,
                }
                //====================KHÁCH HÀNG====================
                let codeCustomer      = infoTransaction.customer.code;
                let nameCustomer      = infoTransaction.customer.name;
                let phoneCustomer     = infoTransaction.customer.phone;
                let addressCustomer   = address;
                let listOwner = '';

                infoTransaction.agency.owners.forEach((owner, index) => {
                    if (index) {
                        listOwner += " - ";
                    }
                    listOwner += owner.username;
                });

                let typeProduct = infoTransaction.products[0].type.name;

                let productName = '';
                infoTransaction.products.forEach((product, index) => {
                    if (index) {
                        productName += " - ";
                    }
                    productName += product.name;
                })

                let approvalLimit = infoTransaction.approvalLimit;

                let codeTransaction = infoTransaction.code;
                data = {
                    ...data,
                    codeCustomer,
                    nameCustomer,
                    phoneCustomer,
                    addressCustomer,
                    listOwner,
                    codeTransaction,
                    productName,
                    typeProduct,
                    approvalLimit,
                }
                
                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
}

exports.MODEL = new Model;
