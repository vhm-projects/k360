"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs = require('fs');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadFields, uploadCusAndAuthPaper }       = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_INVENTORY }                     = require('../constant/inventory_session.uri');

/**
 * MODELS, COLLECTIONS
 */
 const INVENTORY_SESSION_MODEL                       = require('../models/inventory_session').MODEL;
 const TRANSACTION_MODEL                             = require('../../transaction/models/transaction').MODEL;

//const TRANSACTION_COLL                              = require('../database/transaction-coll')({});


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    
    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================       API INVENTORY    ================================
             * ========================== ************************ ================================
             */
            
            /**
             * Danh sách các phiên kiểm kê của đại lý
             * Dev: Dattv
             * 08/05/2021
             */
             [CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSION,
					inc: path.resolve(__dirname, '../views/list_inventory_session.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end } = req.query;
                        let listInventorySessionOld = await INVENTORY_SESSION_MODEL.getList({ agencyID, start, end });
                        let listInventorySession = [];
                        for (const item of listInventorySessionOld.data) {
                            let listTransactionOfInventorySession = await INVENTORY_SESSION_MODEL.getListTransactionOfInventorySession({inventorySessionID: item._id});
                            listInventorySession.push({data: item, count: listTransactionOfInventorySession.data.countTransactionIsCheck})
                        }

                        let amountTransactionNotInventory = await INVENTORY_SESSION_MODEL.countTransactionNotInventory({ agencyID, start, end });
                        let amountTransactionHaveInventory = await INVENTORY_SESSION_MODEL.countTransactionHaveInventory({ agencyID, start, end });
                        let amountTransaction = await INVENTORY_SESSION_MODEL.countTransaction({ agencyID, start, end });
                        //console.log({ amountTransactionNotInventory,amountTransactionHaveInventory,amountTransaction });
                        ChildRouter.renderToView(req, res, 
                            { 
                                listInventorySession,
                                start, end,
                                amountTransactionNotInventory: amountTransactionNotInventory.data && amountTransactionNotInventory.data.length || 0,
                                amountTransactionHaveInventory: amountTransactionHaveInventory.data && amountTransactionHaveInventory.data.length || 0,
                                amountTransaction: amountTransaction.data.length,
                            }
                        );
                    }]
                },
            },

            [CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSION_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end } = req.query;
                        let listInventorySessionOld = await INVENTORY_SESSION_MODEL.getList({ agencyID, start, end });
                        let listInventorySession = [];

                        for (const item of listInventorySessionOld.data) {
                            let listTransactionOfInventorySession = await INVENTORY_SESSION_MODEL.getListTransactionOfInventorySession({inventorySessionID: item._id});
                            listInventorySession.push({data: item, count: listTransactionOfInventorySession.data.countTransactionIsCheck})
                        }

                        let amountTransactionNotInventory = await INVENTORY_SESSION_MODEL.countTransactionNotInventory({ agencyID, start, end });
                        let amountTransactionHaveInventory = await INVENTORY_SESSION_MODEL.countTransactionHaveInventory({ agencyID, start, end });
                        let amountTransaction = await INVENTORY_SESSION_MODEL.countTransaction({ agencyID, start, end });
                        
                        res.json( 
                            { 
                                listInventorySession,
                                error: listInventorySessionOld.error,
                                start, end,
                                amountTransactionNotInventory: amountTransactionNotInventory.data && amountTransactionNotInventory.data.length || 0,
                                amountTransactionHaveInventory: amountTransactionHaveInventory.data && amountTransactionHaveInventory.data.length || 0,
                                amountTransaction: amountTransaction.data.length,
                            }
                        );
                    }]
                },
            },

            /**
             * Chi tiết phiên kiểm kê của đại lý
             * Dev: Dattv
             * 08/05/2021
             */
             [CF_ROUTINGS_INVENTORY.DETAIL_INVENTORY_SESSISON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_INVENTORY.DETAIL_INVENTORY_SESSISON,
					inc: path.resolve(__dirname, '../views/detail_inventory_session.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { inventorySessionID } = req.params;
                        let { type } = req.query;
                        let infoInventorySession = await INVENTORY_SESSION_MODEL.getInfo({ inventorySessionID });
                        let listTransactionOfInventory = await INVENTORY_SESSION_MODEL.getListTransactionOfInventorySession({ inventorySessionID });
                        if(type){
                            res.json({ listTransactionOfInventory: listTransactionOfInventory.data, error: listTransactionOfInventory.error, inventorySessionID, infoInventorySession: infoInventorySession.data })
                        }else{
                            ChildRouter.renderToView(req, res, 
                                {
                                    listTransactionOfInventory: listTransactionOfInventory.data.result,
                                    infoInventorySession: infoInventorySession.data,
                                }
                            );
                        }
                    }]
                },
            },
            

            /**
             * Function: Tạo phiên kiểm kê
             * Dev: Dattv
             */
             [CF_ROUTINGS_INVENTORY.ADD_INVENTORY_SESSISON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { _id: author } = req.user;
                        let { name, description, fromDate, endDate } = req.body;
                        let infoAfterInsert = await INVENTORY_SESSION_MODEL.insert({ 
                            name, description, agencyID, author, fromDate, endDate
                        });
                        res.json({
                            inventorySessionAfterInsert: infoAfterInsert.data,
                            error: infoAfterInsert.error,
                            message: infoAfterInsert.message,
                        })
                    }]
                },
            },

            /**
             * Function: Xoá phiên kiểm kê
             * Dev: Dattv
             */
             [CF_ROUTINGS_INVENTORY.REMOVE_INVENTORY_SESSISON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { inventorySessionID } = req.params;
                        let infoAfterRemove = await INVENTORY_SESSION_MODEL.remove({ inventorySessionID });
                        res.json(infoAfterRemove)
                    }]
                },
            },

            /**
             * Function: Lấy thông tin phiên kiểm kê
             * Dev: Dattv
             */
             [CF_ROUTINGS_INVENTORY.INFO_INVENTORY_SESSISON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { inventorySessionID } = req.params;
                        let infoInventorySession = await INVENTORY_SESSION_MODEL.getInfo({ inventorySessionID });
                        res.json(infoInventorySession)
                    }]
                },
            },

            /**
             * Function: Cập nhật phiên kiểm kê
             * Dev: Dattv
             */
             [CF_ROUTINGS_INVENTORY.UPDATE_INVENTORY_SESSISON]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { inventorySessionID } = req.params;
                        let { name, description } = req.body;
                        let infoAfterRemove = await INVENTORY_SESSION_MODEL.update({ name, description, inventorySessionID });
                        res.json(infoAfterRemove)
                    }]
                },
            },
        }
    }
};
