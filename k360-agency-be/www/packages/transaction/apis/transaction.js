"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs = require('fs');
 const moment = require('moment');
 const XlsxPopulate = require('xlsx-populate');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadFields, uploadCusAndAuthPaper }       = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_TRANSACTION }                   = require('../constant/transaction.uri');
const { TYPE_TRANSACTION, TYPE_GENDER, TYPE_STATUS_TRANSACTION }             = require('../../../config/cf_constants');
const { subStractDateGetDay, minusNumberToSetDate, addNumberToSetDate, compareTwoDay, subtractDate, compareTwoDayCheck, subtractDateHaveExpireTime }                             = require('../../../config/cf_time');
const { getFundsAgency, calculateDateOfPeriod, calculateInterestAndExpireDate, calculateBetweentTwoPrice, checkLimitDate, getNumberDateHavePeriodLate }                   = require("../../../utils/utils");
const AGENCY_SESSION		    = require('../../../session/agency-session');

/**
 * MODELS, COLLECTIONS
 */
 const { IMAGE_MODEL }                               = require('../../image');
 const TRANSACTION_MODEL                             = require('../models/transaction').MODEL;
 const HISTORY_TRANSACTION_MODEL                     = require('../models/history_transaction').MODEL;
 const HISTORY_CHECK_MODEL                           = require('../models/history_check').MODEL;
 const PAY_INTEREST_TRANSACTION_MODEL                = require('../models/pay_interest_transaction').MODEL;
 const INVENTORY_SESSION_MODEL                       = require('../models/inventory_session').MODEL;
 const { AGENCY_MODEL }                              = require('../../agency');
 const { FINANCIAL_MODEL }                           = require('../../financial');
 const { QRCODE_MODEL }                              = require('../../qrcode');
 const INJECT_TRANSACTION_MODEL                      = require('../models/inject_transaction').MODEL;
 const PRODUCT_MODEL                                 = require('../models/product').MODEL;
 const {PRODUCT_CATEGORY_MODEL}                      = require('../../product_category');
 const { INTEREST_RATE_MODEL }                       = require('../../interest_rate');
 const NOTIFICATION_MODEL                            = require('../../notification/models/notification').MODEL;
//  const INTEREST_RATE_MODEL                           = require('../../interest_rate').MODEL;
 const { provinces }                                 = require('../../common/constant/provinces');
 const { districts }                                 = require('../../common/constant/districts');

const { CUSTOMER_MODEL }                             = require('../../customer');
const COMMON_MODEL                                   = require('../../common/models/common').MODEL;
const CUSTOMER_AGENCY_MODEL                          = require('../../customer/models/customer_agency').MODEL;
const { trinh_tu_hoc_online, loai_khoa_hoc }         = require('../../../language/module/lang_en');
// const { constants } = require('os');
// const history_transactionColl = require('../database/history_transaction-coll');
// const { log } = require('console');
const TRANSACTION_COLL                              = require('../database/transaction-coll')({});
const INJECT_TRANSACTION_COLL                       = require('../database/inject_transaction-coll')({});
const AGENCY_COLL                                   = require('../../agency/databases/agency-coll')({});  

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    
    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================       API TRANSACTION    ================================
             * ========================== ************************ ================================
             */

            // '/update-date-in-transaction': {
			// 	config: {
			// 		auth: [ roles.role.all.bin ],
			// 	},
			// 	methods: {
            //         get: [  async function (req, res) {
            //             let listTransactions = await TRANSACTION_COLL.find();
            //             for (const element of listTransactions) {
            //                 let { expireTime, _id: transID } = element;

            //                 // console.log({ expireTime,  expireTimeAfterPeriod: new Date(addNumberToSetDate({ date1: new Date(expireTime), numberToSet: 30 })),
            //                 // interestStartDate: new Date(addNumberToSetDate({ date1: new Date(expireTime), numberToSet: 1 })),});

            //                 let listTransactionss = await TRANSACTION_COLL.findByIdAndUpdate(transID, {
            //                     expireTimeAfterPeriod: new Date(addNumberToSetDate({ date1: new Date(expireTime), numberToSet: 30 })),
            //                     interestStartDate: new Date(addNumberToSetDate({ date1: new Date(expireTime), numberToSet: 1 })),
            //                 });
            //                 // console.log({ listTransactionss });
            //             }
            //             res.json("done")
            //         }]
			// 	}
			// },

            /**
             * Function: tạo transactiop
             * Dev: Depv
             */
            [CF_ROUTINGS_TRANSACTION.ADD_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [  async function (req, res) {
                        let { _id: userID, username: nameUser } = req.user;
                        let {  note, loanAmount, approvalLimit, priceHaveBillChoose, customerIDSystem, totalMoneyChar, approvalNote, products, customer, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages, listUrlBillImages } = req.body;
                        let  agency  = req.agencyID;

                        let infoAfterInsert = await TRANSACTION_MODEL.insert({ 
                            customer, transactionEmployee: userID, agency, note, loanAmount, approvalLimit, totalMoneyChar, approvalNote, category: products[0].type
                        });
                        // Thêm mqh giữa khách hàng với Agency
                        /**
                         * THÊM KHÁCH HÀNG VS CUSTOMER BỎ customerIDSystem
                         */
                        if(customer) {
                            // Insert Customer vào Agency với quan hệ nhiều nhiều
                            let infoCustomerAgency = await CUSTOMER_AGENCY_MODEL.insert({ customerID: customer, agencyID: agency });
                            
                            // if(infoCustomerAgency.error) {
                            //     return res.json(infoCustomerAgency);
                            // }
                        }
                        // console.log({ infoAfterInsert });
                        if(infoAfterInsert.error )
                            return res.json(infoAfterInsert);

                        if(!products || !products.length) {
                            return res.json({ error: true, message: "product_invalid" });
                        }

                        //Kiểm tra category không trùng
                        for (let i = 0; i < products.length - 1; i++ ){
                            for (let j = i + 1; j < products.length; j++ ){
                                if( products[i].type != products[j].type ){
                                    return res.json({ error: true, message: "cateogry_id_exist" })
                                }
                            }
                        }

                        for (let product of products) {
                            let name  = product.name;
                            let type  = product.type;
                            let price = product.price;
                            let note  = product.note ;

                            let infoProductAfterinsert = await PRODUCT_MODEL.insert({ name, type, price, meta: product.meta, transaction: infoAfterInsert.data._id, lastChilds: product.lastChilds, note, priceHaveBillChoose })
                            // update product cho transaction
                            await TRANSACTION_COLL.findByIdAndUpdate(infoAfterInsert.data._id, {
                                $addToSet: {
                                    products: infoProductAfterinsert.data._id
                                }
                            })
                        }

                        // Thêm hình ảnh giao dịch
                        let customerImagesID = [];
                        let KYCImagesID = [];
                        let receiptImagesID = [];
                        let formVerificationImagesID = [];
                        let billImages = [];
                        // const file = req.files;
                        // console.log({ file });
                       
                        if ( !listUrlCustomerImages || !listUrlKYCImages || !listUrlformVerificationImages ){
                            return res.json({ error: true, message: "image_not_completely" })
                        }
                        // Hình ảnh khách hàng
                        for (let imgCustomer of listUrlCustomerImages){
                            let infoImageCustomerAfterInsert = await IMAGE_MODEL.insert({ 
                                name: imgCustomer,
                                path: imgCustomer,
                                userCreate: userID
                            });
                            customerImagesID.push(infoImageCustomerAfterInsert.data._id)
                        }
                        
                        // Hình ảnh KYC
                        for (let imgKYCImages of listUrlKYCImages){
                            let infoImageKYCImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                name: imgKYCImages,
                                path: imgKYCImages,
                                userCreate: userID
                            });
                            KYCImagesID.push(infoImageKYCImagesAfterInsert.data._id)
                        }

                        // Hình ảnh giấy biên nhận
                        if(listUrlReceiptImages) {
                            for (let imgReceiptImages of listUrlReceiptImages){
                                let infoImageReceiptImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgReceiptImages,
                                    path: imgReceiptImages,
                                    userCreate: userID
                                });
                                receiptImagesID.push(infoImageReceiptImagesAfterInsert.data._id)
                            }
                        }

                        if(listUrlBillImages) {
                            for (let imgBillImages of listUrlBillImages){
                                let infoImageBillImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgBillImages,
                                    path: imgBillImages,
                                    userCreate: userID
                                });
                                billImages.push(infoImageBillImagesAfterInsert.data._id)
                            }
                        }
                        
                        // Hình ảnh phiếu kiểm định
                        for (let imgformVerificationImages of listUrlformVerificationImages){
                            let infoImageformVerificationImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                name: imgformVerificationImages,
                                path: imgformVerificationImages,
                                userCreate: userID
                            });
                            formVerificationImagesID.push(infoImageformVerificationImagesAfterInsert.data._id)
                        }

                        // Cập nhật hình ảnh 
                        await TRANSACTION_COLL.findByIdAndUpdate(infoAfterInsert.data._id, {
                            customerImages: customerImagesID,
                            KYCImages: KYCImagesID,
                            receiptImages: receiptImagesID,
                            formVerificationImages: formVerificationImagesID,
                            billImages: billImages
                        });

                        /**
                         * THÊM THÔNG BÁO
                         */
                        // let infoAgency = await AGENCY_COLL
                        //     .findById(agency)
                        //     .select('owners')
                        //     .lean();

                        // // console.log({ infoAgency });
                        // let listNotiAfterInsert = [];
                        // if(infoAgency.owners.length){
                        //     let title     = '#YÊU CẦU TẠO GIAO DỊCH';
                        //     let content   = `${nameUser} đã gửi 1 yêu cầu tạo giao dịch`;
                        //     let link	  = `/transaction/info-transaction/${infoAfterInsert.data._id}`;
    
                        //     // let listOwnerIgnoreSender = infoAgency.owners.filter(member => `${member}` != `${userID}`);
                        //     let arrNotifiIDOfMember = [];
                        //     let listNotiMapInfo = infoAgency.owners.map(member => {
                        //         if (`${member}` != `${userID}`) {
                        //             return NOTIFICATION_MODEL.insertV2({
                        //                 title,
                        //                 description: content,
                        //                 sender: userID,
                        //                 receiver: member,
                        //                 type: 2,
                        //                 arrayAgencyReceive: [agency],
                        //                 url: link,
                        //                 agency: agency
                        //             });
                        //         }
                        //     });

                        //     listNotiAfterInsert = await Promise.all(listNotiMapInfo);
                        //     // 3/ GỬI socket tới tất cả recivers Online
                        //     listNotiAfterInsert = listNotiAfterInsert.filter(infoNoti => (
                        //         infoNoti && infoNoti.data
                        //     ));
                        //     // listNotiAfterInsert.map(infoNoti => {
                        //     //     arrNotifiIDOfMember[arrNotifiIDOfMember.length] = {
                        //     //         notifiID: infoNoti.data._id,
                        //     //         member:   infoNoti.data.receiver
                        //     //     }
                        //     // })
                        // }

                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Danh sách giao dịch của Đại lý trong ngày
             * Dev: Dattv
             * 07/05/2021
             */
            [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION,
					inc: path.resolve(__dirname, '../views/list_transaction_24h.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end, tab } = req.query;
                        let listTransaction;
                        let title = '';

                        if(tab == 1){
                            listTransaction = await INVENTORY_SESSION_MODEL.countTransactionNotInventory({ agencyID, start, end });
                            title = 'Danh sách Giao dịch chưa kiểm kê';
                        }else if(tab == 2){
                            listTransaction = await INVENTORY_SESSION_MODEL.countTransactionHaveInventory({ agencyID, start, end });
                            title = 'Danh sách Giao dịch đã kiểm kê';
                        }else if(tab == 3){
                            listTransaction = await INVENTORY_SESSION_MODEL.countTransaction({ agencyID, start, end });
                            title = 'Danh sách Gói hàng hiện đang giữ';
                        }else{
                            listTransaction = await TRANSACTION_MODEL.getListTransaction24h({ agencyID, start, end });
                            title = 'Danh sách Giao Dịch trong ngày';
                        }

                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransaction: listTransaction.data,
                                start, end, title, tab
                            }
                        );
                    }]
                },
            },

            /**
             * Danh sách giao dịch của đại lý và các đại lý con của nó
             * Dev: VyPQ
             * 02/09/2021
             */
            [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTIONS]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `List Transaction - K360`,
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTIONS,
					inc: path.resolve(__dirname, '../views/manage_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type, start, end, name, city, district, ward, sort, agencyID: agencyFilter } = req.query;
                        let agencyID   = AGENCY_SESSION.getContextAgency(req.session).agencyID;

                        //Danh sách danh mục sản phẩm
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ status: 1 });

                        //Danh sách agency
                        let listAgency = await TRANSACTION_MODEL.getAgencyAndListAgencyChilds({ agencyID });
                        //Danh sách transaction sau khi lọc
                        // let listTransactionWithTypeAndAgency = await TRANSACTION_MODEL.getListTransactionOfAgencyWithType({ agencyID, typeProductCategoryID: type, start, end, name, city, district, ward, sort });

                        /**
                         * DANH SÁCH GIAO DỊCH
                         */
                        let listTransaction = await TRANSACTION_MODEL.getListTransactionRevene({ agencyID, typeProductCategoryID: type, start, end, name, city, district, ward, status: sort, agencyFilter });
                        // console.log({
                        //     listAgency: listAgency.data, listTransaction
                        // });
                        //console.log({ listTransaction })
                        //Danh sách tỉnh thành  
                        let listProvince = Object.entries(provinces);

                        //let top5ProductMostSale = await TRANSACTION_MODEL.top5ProductMostSale();

                        ChildRouter.renderToView(req, res, {
							listProductCategory: listProductCategory.data,
                            listAgency: listAgency.data,
                            listTransactionWithTypeAndAgency: listTransaction.data.listTransaction,
                            agencyFilter, type, start, end,
                            listProvince,
                            name, city, district, ward, sort, TYPE_STATUS_TRANSACTION,
                            calculPriceBorrow: listTransaction.data.calculPriceBorrow,
                            calculPricePay:    listTransaction.data.calculPricePay,
                            calculPriceNormal: listTransaction.data.calculPriceNormal,
                            calculPriceRansom: listTransaction.data.calculPriceRansom,
                            calculPricePawn:   listTransaction.data.calculPricePawn,
                            totalInterestAllTransaction:   listTransaction.data.totalInterestAllTransaction,
						});
                    }]
                },
            },

            /**
             * Danh sách giao dịch 24h của đại lý và các đại lý con của nó
             * Dev: VyPQ
             * 02/09/2021
             */
            [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION_IN_DAY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `List Transaction - K360`,
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION_IN_DAY,
					inc: path.resolve(__dirname, '../views/transaction_in_day.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { city, district, ward, sort, agencyID: agencyFilter } = req.query;
                        let agencyID   = AGENCY_SESSION.getContextAgency(req.session).agencyID;
                        // let listTransaction24hOfAngency = await TRANSACTION_MODEL.getListTransactionNear24h({ agencyID });
                        let _fromDate   = moment(new Date()).startOf('day').format();
                        let _toDate     = moment(new Date()).endOf('day').format();

                        let listTransaction = await TRANSACTION_MODEL.getListTransactionRevene({ agencyID, start: _fromDate, end: _toDate, city, district, ward, status: sort, agencyFilter });
                        console.log({ listTransaction })
                        // let listAgency = await TRANSACTION_MODEL.getListAgency();
                        let listAgency = await TRANSACTION_MODEL.getAgencyAndListAgencyChilds({ agencyID });

                        ChildRouter.renderToView(req, res, {
                            listAgency: listAgency.data,
                            listTransaction24hOfAngency: listTransaction.data.listTransaction,
                            agencyFilter,
                            city, district, ward, sort,
                            calculPriceBorrow: listTransaction.data.calculPriceBorrow,
                            calculPricePay:    listTransaction.data.calculPricePay,
                            calculPriceNormal: listTransaction.data.calculPriceNormal,
                            calculPriceRansom: listTransaction.data.calculPriceRansom,
                            calculPricePawn:   listTransaction.data.calculPricePawn,
                            totalInterestAllTransaction:   listTransaction.data.totalInterestAllTransaction,
						});
                    }]
                },
            },

            /**
             * Danh sách giao dịch của Đại lý trong ngày || API Mobile
             * Dev: Dattv
             * 07/05/2021
             */
             [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION,
					inc: path.resolve(__dirname, '../views/list_transaction_24h.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end, tab, type } = req.query;
                        let listTransaction;
                        let title = '';

                        if(tab == 1){
                            listTransaction = await INVENTORY_SESSION_MODEL.countTransactionNotInventory({ agencyID, start, end });
                            title = 'Danh sách Giao dịch chưa kiểm kê';
                        }else if(tab == 2){
                            listTransaction = await INVENTORY_SESSION_MODEL.countTransactionHaveInventory({ agencyID, start, end });
                            title = 'Danh sách Giao dịch đã kiểm kê';
                        }else if(tab == 3){
                            listTransaction = await INVENTORY_SESSION_MODEL.countTransaction({ agencyID, start, end });
                            title = 'Danh sách Gói hàng hiện đang giữ';
                        }else{
                            listTransaction = await TRANSACTION_MODEL.getListTransaction24h({ agencyID, start, end });
                            title = 'Danh sách Giao Dịch trong ngày';
                        }

                        if(type){
                            res.json({ listTransaction: listTransaction.data, error: listTransaction.error, 'Từ ngày': start ? start : null, 'Đến ngày': end ? end : null, title })
                        }else{
                            ChildRouter.renderToView(req, res, 
                                { 
                                    listTransaction: listTransaction.data,
                                    start, end, title, tab
                                }
                            );
                        }
                    }]
                },
            },

            /**
             * Danh sách sản phẩm giao dịch của Đại lý trong ngày
             * Dev: Dattv
             * 07/05/2021
             */
            [CF_ROUTINGS_TRANSACTION.VIEW_LIST_PRODUCT]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_PRODUCT,
					inc: path.resolve(__dirname, '../views/list_product_24h.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end } = req.query;
                        let listProduct = await TRANSACTION_MODEL.getListProduct24h({ agencyID, start, end });
                        ChildRouter.renderToView(req, res, 
                            { 
                                listProduct: listProduct.data,
                                start, end
                            }
                        );
                    }]
                },
            },

            [CF_ROUTINGS_TRANSACTION.VIEW_LIST_PRODUCT_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end } = req.query;
                        let listProduct = await TRANSACTION_MODEL.getListProduct24h({ agencyID, start, end });
                        res.json(
                            { 
                                listProduct: listProduct.data,
                                error: listProduct.error,
                                start, end
                            }
                        );
                    }]
                },
            },
            /**
             * API trả danh sách giao dịch trong 24h Mobile
             */
             [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION_24H_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end } = req.query;
                        let listProduct = await TRANSACTION_MODEL.getListTransaction24h({ agencyID, start, end });
                        res.json(
                            { 
                                listProduct: listProduct.data,
                                start, end
                            }
                        );
                    }]
                },
            },

            /**
             * Function: Tạo lịch sử kiểm kê của gói hàng
             * Dev: Dattv
             */
             [CF_ROUTINGS_TRANSACTION.ADD_HISTORY_CHECK_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID = req.agencyID;
                        let { transactionID, inventorySessionID } = req.body;
                        let infoAfterInsert = await HISTORY_CHECK_MODEL.createInventoryTransaction({
                            transactionID, agencyID, userID, inventorySessionID
                        })

                        res.json(infoAfterInsert)
                    }]
                },
            },

            /**
             * Danh sách giao dịch của Đại lý trong ngày || API Mobile
             * Dev: Dattv
             * 07/05/2021
             */
            [CF_ROUTINGS_TRANSACTION.VIEW_LIST_HISTORY_CHECK_OF_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_HISTORY_CHECK_OF_TRANSACTION,
					inc: path.resolve(__dirname, '../views/list_history_check_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        //let agencyID = req.agencyID;
                        let { transactionID } = req.params;
                        let { type, inventorySessionID } = req.query;
                        let listHistoryCheckOfTransaction = await HISTORY_CHECK_MODEL.getListHistoryCheckOfTransaction({transactionID, inventorySessionID});
                        if(type){
                            res.json({ 
                                listHistoryCheckOfTransaction: listHistoryCheckOfTransaction.data, 
                                error: listHistoryCheckOfTransaction.error
                            })
                        }else{
                            ChildRouter.renderToView(req, res, 
                                { 
                                    listHistoryCheckOfTransaction: listHistoryCheckOfTransaction.data,
                                }
                            );
                        }
                        
                    }]
                },
            },

            /**
             * FUNCTION: XUẤT UPDATE GIAO DỊCH
             * SONLP
             */
            [CF_ROUTINGS_TRANSACTION.GET_UPDATE_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.GET_UPDATE_TRANSACTION,
					inc: path.resolve(__dirname, '../views/update_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, username: nameUser} = req.user;
                        let agencyID = req.agencyID;
                        let { transactionID } = req.params;

                        let infoAgency           = await AGENCY_MODEL.getInfo({ agencyID });

                        let infoTransaction = await TRANSACTION_MODEL.getInfo({ transactionID });
                        if(!(infoTransaction.data.status == 0 || infoTransaction.data.status == 2)) {
                            return res.redirect('/transaction/list-transaction');
                        }

                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ status: 1 });

                        // console.log({ data: listProductCategory.data });
                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        for (let province of listProvince){
                            if ( province[1].code == infoTransaction.data.customer.city ){
                                provinceArr = province;
                                break;
                            }
                        }

                        let listWards = [];
                        let wardArr = [];
                        if ( infoTransaction.data.customer.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoTransaction.data.customer.district ){
                                    districtArr = district;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoTransaction.data.customer.ward ){
                                            wardArr = ward;
                                            break;
                                        }
                                    }
                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            infoTransaction: infoTransaction.data,
                                            TYPE_GENDER,
                                            districtArr,
                                            provinceArr,
                                            wardArr,
                                            // level,
                                            TYPE_TRANSACTION,
                                            listProductCategory: listProductCategory.data,
                                            percentOfAgency: infoAgency.data.percentPawn
                                            // infoInterest: infoInterest.data,
                                            // limitDate: Math.floor(limitDate),
                                            // loanAmoutAfterInterest,
                                            // nameUser,
                                            // userID,
                                            // agency: agencyID,
                                        }
                                    );
                                } else {
                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            infoTransaction: infoTransaction.data,
                                            TYPE_GENDER,
                                            districtArr,
                                            provinceArr,
                                            wardArr,
                                            // level,
                                            TYPE_TRANSACTION,
                                            listProductCategory: listProductCategory.data,
                                            percentOfAgency: infoAgency.data.percentPawn
                                            // infoInterest: infoInterest.data,
                                            // limitDate: Math.floor(limitDate),
                                            // loanAmoutAfterInterest,
                                            // nameUser,
                                            // userID,
                                            // agency: agencyID
                                        }
                                    );
                                }
                            });
                        }else{
                            ChildRouter.renderToView(req, res, 
                                { 
                                    infoTransaction: infoTransaction.data,
                                    TYPE_GENDER,
                                    districtArr,
                                    provinceArr,
                                    wardArr,
                                    // level,
                                    TYPE_TRANSACTION,
                                    listProductCategory: listProductCategory.data,
                                    percentOfAgency: infoAgency.data.percentPawn
                                    // infoInterest: infoInterest.data,
                                    // limitDate: Math.floor(limitDate),
                                    // loanAmoutAfterInterest,
                                    // nameUser,
                                    // userID,
                                    // agency: agencyID
                                }
                            );
                        }
                    }]
                },
            },

            [CF_ROUTINGS_TRANSACTION.UPDATE_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID, username: nameUser} = req.user;
                        let agencyID = req.agencyID;
                        let { transactionID } = req.params;
                        let { loanAmount, approvalLimit, totalMoneyNumber, 
                            arrProductIDDelete, approvalNote, products, statusFinancial,
                            listUrlCustomerImages, 
                            listUrlKYCImages, 
                            listUrlReceiptImages, 
                            listUrlformVerificationImages, 
                            listUrlBillImages, 
                            customerImagesIsDelete, 
                            KYCImagesIsDelete, 
                            receiptImagesIsDelete, 
                            formVerificationImagesIsDelete,
                            billImagesIsDelete } = req.body;
                        let {data: infoFinancial}   = await FINANCIAL_MODEL.getInfo({ transactionID, onModel: "transaction" });
                        let infoData                = await TRANSACTION_MODEL.getInfo({ transactionID: transactionID });

                        if(infoData.data.status == 1) {
                            // let {data: infoFinancial}   = await FINANCIAL_MODEL.getInfo({ transactionID, onModel: "transaction" });
                            let infoDataAgency          = await AGENCY_MODEL.getInfo({ agencyID });
                            let infoTransaction         = infoData.data;
                            let FUND_MONEY = 1;
                            let funds;
                            if(statusFinancial == FUND_MONEY) {
                                funds = infoDataAgency.data.funds
                            }else{
                                funds = infoDataAgency.data.fundsBank
                            }

                            let type;
                            let COLLECT = 1;
                            let SPEND   = 2;
                            let priceOld = infoTransaction.loanAmount;
                            let priceNew = 0;
                            loanAmount = Number(loanAmount);
                            if(priceOld > loanAmount) {
                                priceNew = priceOld - loanAmount;
                                type = COLLECT;
                            }
                            if(priceOld < loanAmount){
                                priceNew = loanAmount - priceOld;
                                type = SPEND;
                            }
                            // console.log({ priceOld, loanAmount, priceNew });
                            if(priceOld != loanAmount) {
                                // // Update lại số tiền vay 
                                let title = infoFinancial[0].title;
                                let addTitleFinancial = "Chỉnh Sửa + " + title;

                                if ( type == COLLECT ){
                                    let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                                        title:  addTitleFinancial, 
                                        type:   type,
                                        typeTransaction: type,
                                        agency: agencyID, 
                                        price:  priceNew, 
                                        funds, 
                                        transaction: transactionID,
                                        onModel: "transaction",
                                        statusFinancial
                                    });
                                }else{
                                    if ( funds >= priceNew ){
                                        let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                                            title:  addTitleFinancial, 
                                            type:   type,
                                            typeTransaction: type,
                                            agency: agencyID, 
                                            price:  priceNew, 
                                            funds, 
                                            transaction: transactionID,
                                            onModel: "transaction",
                                            statusFinancial
                                        });
                                    }else{
                                        return res.json({ error: true, message: "fund_not_enough"});
                                    }
                                }
                            }
                        }

                        let infoTransactionAfterUpdate = await TRANSACTION_MODEL.updateTransaction({ 
                            transactionID, 
                            transactionEmployee: userID,
                            loanAmount, 
                            approvalLimit, 
                            totalMoneyNumber, 
                            approvalNote,
                            arrProductIDDelete,
                            customerImagesIsDelete,
                            KYCImagesIsDelete,
                            receiptImagesIsDelete,
                            formVerificationImagesIsDelete,
                            billImagesIsDelete
                        });
                        if(arrProductIDDelete) {
                            for (let product of arrProductIDDelete){
                                let infoProductAfterDelete = await PRODUCT_MODEL.remove({ productID: product });
                            }
                        }

                        if(products && products.length) {
                            //Kiểm tra category không trùng
                            for (let i = 0; i < products.length - 1; i++ ){
                                for (let j = i + 1; j < products.length; j++ ){
                                    if( products[i].type != products[j].type ){
                                        return res.json({ error: true, message: "cateogry_id_exist" })
                                    }
                                }
                            }

                            /**
                             * Tìm mảng sản phẩm của giao dịch hiện tại và xóa nó đi hết để thêm mảng sản phẩm mới vào
                             */
                            let listProductOfTransaction = await TRANSACTION_MODEL.getListProductOfTransaction({ transactionID });
                            if(listProductOfTransaction && listProductOfTransaction.data.length > 0) {
                                for(let product of listProductOfTransaction.data) {
                                    //console.log({ id: product._id })
                                    let infoTransactionAfterUpdate = await TRANSACTION_COLL.findByIdAndUpdate({ _id: transactionID }, {
                                        $pull: { products: product._id }
                                    }, { new: true });
                                    let infoProductAfterDelete = await PRODUCT_MODEL.remove({ productID: product._id });
                                }
                            }

                            // console.log({ products })
                            for (let product of products) {
                                let name = product.name;
                                let type = product.type;
                                let price = product.price;
                                let note = product.note;

                                let dataProduct = { name, type, price, note };

                                let infoProductAfterinsert = await PRODUCT_MODEL.insert({ ...dataProduct, transaction: transactionID })
                                    // update product cho transaction

                                await TRANSACTION_COLL.findByIdAndUpdate(transactionID, {
                                    $addToSet: {
                                        products: infoProductAfterinsert.data._id
                                    }
                                })
                            }
                        } 

                        // Hình ảnh khách hàng
                        if(listUrlCustomerImages && listUrlCustomerImages.length) {
                            let customerImagesID = [];
                            for (let imgCustomer of listUrlCustomerImages){
                                let infoImageCustomerAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgCustomer,
                                    path: imgCustomer,
                                    userCreate: userID
                                });
                                customerImagesID.push(infoImageCustomerAfterInsert.data._id)
                            }
                            // Cập nhật hình ảnh 
                            await TRANSACTION_COLL.findByIdAndUpdate(infoTransactionAfterUpdate.data._id, {
                                $addToSet: {
                                    customerImages: customerImagesID
                                }
                            });
                        }
                        
                        // Hình ảnh KYC
                        if(listUrlKYCImages && listUrlKYCImages.length) {
                            let KYCImagesID = [];
                            for (let imgKYCImages of listUrlKYCImages){
                                let infoImageKYCImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgKYCImages,
                                    path: imgKYCImages,
                                    userCreate: userID
                                });
                                KYCImagesID.push(infoImageKYCImagesAfterInsert.data._id)
                            }
                             // Cập nhật hình ảnh 
                            await TRANSACTION_COLL.findByIdAndUpdate(infoTransactionAfterUpdate.data._id, {
                                $addToSet: {
                                    KYCImages: KYCImagesID
                                }
                            });
                        }

                        // Hình ảnh giấy biên nhận
                        if(listUrlReceiptImages && listUrlReceiptImages.length) {
                            let receiptImagesID = [];
                            for (let imgReceiptImages of listUrlReceiptImages){
                                let infoImageReceiptImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgReceiptImages,
                                    path: imgReceiptImages,
                                    userCreate: userID
                                });
                                receiptImagesID.push(infoImageReceiptImagesAfterInsert.data._id)
                            }
                            await TRANSACTION_COLL.findByIdAndUpdate(infoTransactionAfterUpdate.data._id, {
                                $addToSet: {
                                    receiptImages: receiptImagesID
                                }
                            });
                        }

                        // Hình ảnh phiếu kiểm định
                        if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                            let formVerificationImagesID = [];
                            for (let imgformVerificationImages of listUrlformVerificationImages){
                                let infoImageformVerificationImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgformVerificationImages,
                                    path: imgformVerificationImages,
                                    userCreate: userID
                                });
                                formVerificationImagesID.push(infoImageformVerificationImagesAfterInsert.data._id)
                            }
                            await TRANSACTION_COLL.findByIdAndUpdate(infoTransactionAfterUpdate.data._id, {
                                $addToSet: {
                                    formVerificationImages: formVerificationImagesID
                                }
                            });
                        }

                        // Hình ảnh hóa đơn
                        if(listUrlBillImages && listUrlBillImages.length) {
                            let billImagesID = [];
                            for (let imgBillImages of listUrlBillImages){
                                let infoImageBillAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgBillImages,
                                    path: imgBillImages,
                                    userCreate: userID
                                });
                                billImagesID.push(infoImageBillAfterInsert.data._id)
                            }
                            let data = await TRANSACTION_COLL.findByIdAndUpdate(infoTransactionAfterUpdate.data._id, {
                                $addToSet: {
                                    billImages: billImagesID
                                }
                            });
                        }

                        res.json(infoTransactionAfterUpdate);
                    }]
                },
            },

            /**
             * Function: TẠO GIAO DỊCH BÌNH THƯỜNG 
             * Dev: SONLP
             */
            [CF_ROUTINGS_TRANSACTION.ADD_INJECT_TRANSACTION_PAY_BEFORE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title, type, note, meta, statusFinancial, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages  } = req.body;

                        let infoTransaction  = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoDataAgency   = await AGENCY_MODEL.getInfo({ agencyID });

                        if(infoTransaction.error) {
                            return res.json(infoData);
                        }
                        /**
                         * GIAO DỊCH HOÀN THÀNH
                         */
                        const STATUS_COMPLETE = 3;
                        const LOAN_AMOUNT_PAIED = 3;
                        if(infoTransaction.loanAmount == LOAN_AMOUNT_PAIED || infoTransaction.data.status == STATUS_COMPLETE) {
                            return res.json({ error: true, message: "transaction_complete"});
                        }
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CHUỘC ĐỒ HAY CHƯA
                         */
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.type == 6 && infoTransaction.data.latestInjectTransaction.ransom == 2) {
                            return res.json({ error: true, message: "transaction_ransom_existed"});
                        }
                        if(infoDataAgency.error) {
                            return res.json(infoDataAgency);
                        }
                        if (!statusFinancial) {
                            return res.json({ error: true, message: "statusFinancial_invalid"})
                        }
                        /**
                         * 1:// Kiểm tra xem giao dịch có được duyệt
                         */
                        if (infoTransaction.data.status == 1) {
                            infoTransaction = infoTransaction.data;
                            infoDataAgency  = infoDataAgency.data;
                            /**
                             * TYPE = 1 (THU TIỀN)
                             */
                            const TYPE = 1
                            let typeFinancial   = TYPE;

                            /**
                             * NẾU META KHÔNG TỒN TẠI THÌ SET META LÀ OBJ RỖNG
                             */
                            if (!meta) {
                                meta = {};
                            }
                            /**
                             * LẤY ID DANH MỤC SẢN PHẨM
                             */
                            let TYPE_PRODUCT = infoTransaction.products[0].type._id;
                            /**
                             * LẤY THÔNG TIN QUỸ CỦA ĐẠI LÝ
                             */
                             /**
                              * Check Loại quỹ
                              *     1: Quỹ Tiền Mặt
                              *     2: Quỹ Ngân Hàng
                              */
                            // let funds = getFundsAgency({ statusFinancial, infoDataAgency });
                            let funds;
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                            let objectConditionForFindFund = {
                                agency: agencyID,
                            };
                           
                            const LOAI_QUY_TIEN_MAT = 1;
                            const LOAI_QUY_TIEN_NGAN_HANG = 2;

                            objectConditionForFindFund = statusFinancial == LOAI_QUY_TIEN_MAT ?
                                { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                                { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                            let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                            
                            if (!latestRecordFinicalOfAgency) {
                                return res.json({ error: true, message: "", description: 'vui lòng tạo giao dịch nạp tiền'});
                            } else {
                                funds = latestRecordFinicalOfAgency.fundsAfterChange;
                            }
                             /**
                             *  2// LẤY NGÀY TÍNH LÃI VÀ SỐ TIỀN CÒN DƯ TRONG GIAO DỊCH
                             */
                            let interestStartDate = infoTransaction.interestStartDate;
                            let payInterest       = infoTransaction.payInterest; 
                             // 
                            /**
                             * Kiểm tra xem giao dịch đã được trả lãi trước hay kh
                             * => TH1: Trường hợp đã trả lãi trước nhưng trong thời gian đã trả lãi => KHÁCH HÀNG muốn Vay thêm, hoặc Trả bớt
                             * => TH2: So sánh giữa 2 ngày: Ngày hiện tại và Ngày bắt đầu tính lãi (checkInterestDateAndDateNow)
                             */
                            let checkDayInterestStartDate   = new Date();
                            let checkInterestDateAndDateNow = subStractDateGetDay({date1: interestStartDate, date2: checkDayInterestStartDate});

                            /**
                             *  3// TÍNH PHẦN TRĂM LÃI VÀ SỐ TIỀN LÃI CỦA GIAO DỊCH
                             */
                            let { data: infoInterest } = await INTEREST_RATE_MODEL.checkDateToInterestTransaction({ agencyID, interestStartDate, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            meta.priceMuchPayInterest = Math.floor(infoInterest.loanAmoutAfterInterest);
                            /**
                             * 3// KIỂM TRA XEM LÀ GIAO DỊCH TRẢ LÃI HAY TRÃ TRƯỚC
                             *      => Nếu tồn tại  (meta.cycleInterest) => Trã lãi trước
                             *      => Trã lãi bình thường
                             */
                            let nameOfFinanical    = "";
                            let titleOfTransaction = "";
                            let obj = { };
                            let objUpdateTransactionMain = {};
                            let objPayInterest           = {};
                            let totalPercentHistory      = infoInterest.totalPercent;
                            let totalDateHistory         = infoInterest.limitDate;
                            if (meta.cycleInterest) {
                                if (meta.cycleInterest < 0 || meta.cycleInterest > 12) {
                                    return res.json({ error: true, message: "cycleInterest_invalid" })
                                }
                                nameOfFinanical    = `GIAO DỊCH BÌNH THƯỜNG TRÃ LÃI TRƯỚC`;
                                titleOfTransaction = `GIAO DỊCH BÌNH THƯỜNG TRÃ LÃI TRƯỚC`;
                                const DATE_OF_ONE_PERIOD = 30;
                                const TRANSACTION_NORMAL = 2;
                                /**
                                 * 1// TÍNH SỐ NGÀY CỦA CHU KỲ
                                 */
                                const DATE_OF_PERIOD = calculateDateOfPeriod({ period: meta.cycleInterest });
                                totalDateHistory     += DATE_OF_PERIOD;
                                /** 
                                    CHECK XEM GIAO DỊCH CÓ ĐANG TRÃ LÃI TRƯỚC
                                */
                                let dateSetInterestDate = new Date();
                                let dateSetExprireDate  = new Date();
                                if (checkInterestDateAndDateNow > 0) {
                                    dateSetExprireDate  = new Date(interestStartDate);
                                    dateSetInterestDate = new Date(interestStartDate);
                                }
                                /**
                                 * 2// SET LẠI SỐ NGÀY TÍNH LÃU VỚI SỐ NGÀY CHU KÌ (DATE_OF_PERIOD)
                                 */
                                let dateAfterCalculate = calculateInterestAndExpireDate({ dateOfPeriod: DATE_OF_PERIOD, date1: dateSetExprireDate, date2: dateSetInterestDate });
                                /**
                                 * 3// TÍNH LÃI CỦA CHU KÌ NHẬP (meta.cycleInterest)
                                 */
                                let { data: infoInterestHavePeriod } = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: DATE_OF_PERIOD, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                                meta.priceMuchPayInterest = meta.priceMuchPayInterest + infoInterestHavePeriod.loanAmoutAfterInterest;
                                totalPercentHistory       += infoInterestHavePeriod.percetTotal;    
                                /**
                                 * OBJ UPDATE LẠI AGENCY
                                 */
                                objUpdateTransactionMain = {
                                    interestStartDateNew: dateAfterCalculate.dateInterestDate,
                                    expireTimeNew:        dateAfterCalculate.dateExpriseDate,
                                }
                                /**
                                 * OBJ THÊM GIAO DỊCH BỔ SUNG
                                 */
                                obj = {
                                    title: titleOfTransaction, type: TRANSACTION_NORMAL, transaction: transactionID, note, meta, agencyID
                                };
                                /**
                                 * OBJ THÊM GIAO DỊCH TRẢ TRƯỚC
                                 */

                                objPayInterest = {
                                    title: titleOfTransaction,
                                    type: 2,
                                    employee: userID,
                                    interestRateTotal: infoInterestHavePeriod.loanAmoutAfterInterest,
                                    period: Number(meta.cycleInterest),
                                    expireTime: dateAfterCalculate.dateInterestDate,
                                    interestStartDate: interestStartDate,
                                    resetDate: new Date(),
                                    interstAmountDate: DATE_OF_ONE_PERIOD * Number(meta.cycleInterest),
                                    interestPercent: infoInterestHavePeriod.percetTotal,
                                    transactionMain: transactionID
                                }
                            } else {
                                /**
                                 * NẾU GIAO DỊCH ĐANG TRÃ TRƯỚC THÌ KHÔNG TÍNH LÃI
                                 */
                                if (checkInterestDateAndDateNow > 0) {
                                    return res.json({ error: true, message: "not_interest_to_pay" })
                                }

                                nameOfFinanical = `GIAO DỊCH BÌNH THƯỜNG TRÃ LÃI`;
                                titleOfTransaction = `GIAO DỊCH BÌNH THƯỜNG TRÃ LÃI`;

                                obj = {
                                    title: titleOfTransaction, type: 2, transaction: transactionID, note, meta, agencyID
                                };
                            }
                            
                            /**
                             * 3// TÍNH 2 SỐ TIỀN GIỮA SỐ TIỀN CÒN DƯ VÀ SỐ TIỀN LÃI PHẢI TRẢ
                             * @param meta.priceMuchPayInterest Số tiền trả lãi
                             * @param payInterest               Số tiền còn dư
                             */
                            let infoPriceAfterChange = calculateBetweentTwoPrice({ price1: meta.priceMuchPayInterest, price2: payInterest });

                             /**
                              *  4// SET LẠI GIÁ CỦA SỐ TIỀN TRẢ LÃI VÀ SỐ TIỀN CÒN DƯ
                              */
                            meta.priceMuchPayInterest             = infoPriceAfterChange.price1;
                            meta.statusFinancial                  = statusFinancial;

                            objUpdateTransactionMain = {
                                ...objUpdateTransactionMain,
                                transactionID,
                                payInterest: infoPriceAfterChange.price2
                            }
                            /**
                             * KIỂM TRA GIÁ KHUYẾN MÃI CÓ TỒN TẠI
                             */
                            if (meta.promotionInterest && meta.promotionInterest > meta.priceMuchPayInterest) {
                                return res.json({ error: true, message: "promotionInterest_invalid" });
                            }else if(meta.promotionInterest){
                                obj.meta.priceMuchPayInterest = obj.meta.priceMuchPayInterest - Number(meta.promotionInterest)
                            }

                            let dataUpdate = {}
                            if(listUrlCustomerImages && listUrlCustomerImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlCustomerImages, userCreate: userID });
                                dataUpdate.customerImages    = listIDImage;
                            }
                            if(listUrlKYCImages && listUrlKYCImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlKYCImages, userCreate: userID });
                                dataUpdate.KYCImages         = listIDImage;
                            }
                            if(listUrlReceiptImages && listUrlReceiptImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                dataUpdate.receiptImages     = listIDImage;
                            }
                            if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                dataUpdate.formVerificationImages = listIDImage;
                            }
                            /**
                             * CHECK QUỸ KH ĐỦ
                             */
                            // if (funds < obj.meta.priceMuchPayInterest) {
                            //     return res.json({ error: true, message: "fund_not_enough"});
                            // }
                            
                           
                            let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                                ...obj, dataUpdate
                            });
                            if(infoAfterInsert.error) {
                                return res.json(infoAfterInsert);
                            }
                            // SET NAME THÊM VÀO QUỸ
                            nameOfFinanical = `${infoAfterInsert.data.code} + ` + `${infoTransaction.customer.name} + ` + nameOfFinanical + ` +    ${meta.priceMuchPayInterest}`;
    
                            /**
                            *  // INSERT VÀO COLLECTION TRẢ TRƯỚC 
                            */
                            if (meta.cycleInterest) {
                                objPayInterest = {
                                    ...objPayInterest,
                                    transaction: infoAfterInsert.data._id,
                                }
                                let infoPayInterestTransaction = await PAY_INTEREST_TRANSACTION_MODEL.insert({
                                    ...objPayInterest
                                });
                            }

                            // Update lại số tiền vay 
                            let infoDataAfterUpdate = await TRANSACTION_MODEL.update({ ...objUpdateTransactionMain });
                            if(infoDataAfterUpdate.error) {
                                return res.json(infoDataAfterUpdate);
                            }

                            /**
                             *  INSERT HISTORY TRANSACTION
                             */
                            const TYPE_NORMAL = 2;
                            let infoHistoryTransactionAfterInsert = await HISTORY_TRANSACTION_MODEL.insert({
                                title: titleOfTransaction,
                                type: TYPE_NORMAL,
                                employee: userID,
                                interestRateTotal: obj.meta.priceMuchPayInterest,
                                interestStartDate: infoTransaction.interestStartDate,
                                resetDate: new Date(),
                                transaction: infoAfterInsert.data._id,
                                interstAmountDate: totalDateHistory,
                                interestPercent: totalPercentHistory,
                                transactionMain: transactionID
                            });
                            // UPDATE LẠI QUỸ
                            let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                                title:  nameOfFinanical, 
                                type:   typeFinancial,
                                typeTransaction: TYPE_NORMAL,
                                agency: agencyID, 
                                price:  obj.meta.priceMuchPayInterest, 
                                funds, 
                                transaction: infoAfterInsert.data._id,
                                onModel: "inject_transaction",
                                statusFinancial
                            });
                            if (infoFinancialAfterInsert.error) {
                                return res.json(infoFinancialAfterInsert)
                            }
                            res.json(infoAfterInsert);
                        }else{
                            res.json({ error: true, message: "cannot_insert_inject_transaction" })
                        }
                    }]
                },
            },


            /**
             * Function: TẠO GIAO DỊCH BÌNH THƯỜNG TRẢ TRỄ
             */
            [CF_ROUTINGS_TRANSACTION.ADD_INJECT_TRANSACTION_PAY_LATE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title, type, note, meta, statusFinancial, option, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages  } = req.body;

                        let infoTransaction  = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoDataAgency   = await AGENCY_MODEL.getInfo({ agencyID });
                        if(infoDataAgency.error) {
                            return res.json(infoDataAgency);
                        }
                        if(infoTransaction.error) {
                            return res.json(infoTransaction);
                        }
                        /**
                         * GIAO DỊCH HOÀN THÀNH
                         */
                        const STATUS_COMPLETE   = 3;
                        const LOAN_AMOUNT_PAIED = 3;
                        if(infoTransaction.loanAmount == LOAN_AMOUNT_PAIED || infoTransaction.data.status == STATUS_COMPLETE) {
                            return res.json({ error: true, message: "transaction_complete"});
                        }
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CHUỘC ĐỒ HAY CHƯA
                         */
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.type == 6 && infoTransaction.data.latestInjectTransaction.ransom == 2) {
                            return res.json({ error: true, message: "transaction_ransom_existed"});
                        }
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CẦN DUYỆT HAY KHÔNG
                         */
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.status == 3) {
                            return res.json({ error: true, message: "transaction_have_check_existed"});
                        }
                        if (!statusFinancial) {
                            return res.json({ error: true, message: "statusFinancial_invalid"})
                        }
                        /**
                         * 1:// Kiểm tra xem giao dịch có được duyệt
                         */
                        if (infoTransaction.data.status == 1) {
                            infoTransaction = infoTransaction.data;
                            infoDataAgency  = infoDataAgency.data;
                            /**
                             * TYPE = 1 (THU TIỀN)
                             */
                            // const TYPE = 1
                            // let typeFinancial        = TYPE;
                            const DAY_OF_MONTH       = 30;
                            const TRANSACTION_NORMAL = 2;
                            const ONE_PERIOD         = 1;

                            /**
                             * NẾU META KHÔNG TỒN TẠI THÌ SET META LÀ OBJ RỖNG
                             */
                            if (!meta) {
                                meta = {};
                            }
                            /**
                             * LẤY ID DANH MỤC SẢN PHẨM
                             */
                            let TYPE_PRODUCT = infoTransaction.products[0].type._id;
                            
                             /**
                             *  2// LẤY NGÀY TÍNH LÃI VÀ SỐ TIỀN CÒN DƯ TRONG GIAO DỊCH VÀ NGÀY GIAO DỊCH CẦM
                             */
                            let payInterest           = infoTransaction.payInterest; 
                            let expireTimeAfterPeriod = infoTransaction.expireTimeAfterPeriod; 
                            let NUMBER_DATE_HAVE_PERIOD;
                            let NUMBER_DATE_HAVE_PERIOD_NOT_NOW;
                            let titleTransaction = '';

                            /**
                             *  EXAMPLE:
                             *      |------------------------------------|----------------|
                             *     1/8                                 31/8 
                             *      |------------ TRẢ TRƯỚC---------TRẢ ĐÚNG KÌ -------TRỂ KÌ
                             * (interestStartDate)             (expireTimeAfterPeriod)
                             */
                            let dateCheck = new Date();
                            
                            /**
                             * --------------------------TRẢ TRỄ-------------------------
                             */
                            // console.log({ meta, dateCheck, expireTimeAfterPeriod })
                            if (compareTwoDayCheck(dateCheck, expireTimeAfterPeriod) == 'bigger') {
                                console.log("--------------------------TRẢ TRỄ-------------------------");
                                /**
                                 * ĐÓNG TRÒN KÌ
                                 * EXAMPLE:
                                 *      Khách hàng trễ 32 ngày -> Chỉ đóng 30 ngày -> Trả lãi trước
                                 */
                                if (meta && meta.typePayLate == 'full_period') {
                                    let period = meta.cycleInterest ? meta.cycleInterest : ONE_PERIOD;
                                    // let period = ONE_PERIOD;
                                    NUMBER_DATE_HAVE_PERIOD = DAY_OF_MONTH * Number(period);
                                }

                                /**
                                 * ĐÓNG KHÔNG TRÒN KÌ VÀ NGÀY CHỐT LÃI KHÔNG PHẢI NGÀY HIỆN TẠI
                                 */
                                if (meta && meta.typePayLate == 'late_period_date_not_now') {
                                    /**
                                     * LẤY RA SỐ NGÀY ĐÓNG TRỄ
                                    */
                                    if (!meta.exprireTime) {
                                        return res.json({ error: true, message: 'exprireTime_invalid'});
                                    }
                                    // console.log({
                                    //     exprireTime: meta.exprireTime
                                    // });
                                    let exprireTime = new Date(meta.exprireTime); // NGÀY CHỐT LÃI TRỄ NHẬP VÀO
                                    exprireTime     = moment(exprireTime).endOf('day').format();
                                    exprireTime     = new Date(exprireTime);

                                    let dateNow2 = new Date();
                                    /**
                                     * =============================================================
                                     * ==========================VALIDATION=========================
                                     * =============================================================
                                     */
                                    let expireTimeAfterPeriodCheck = new Date(expireTimeAfterPeriod);
                                    expireTimeAfterPeriodCheck = moment(expireTimeAfterPeriodCheck).startOf('day').format();
                                    expireTimeAfterPeriodCheck = new Date(expireTimeAfterPeriodCheck);
                                   
                                    if (dateNow2 < exprireTime) {
                                        return res.json({ error: true, message: 'dateNow_not_>_exprireTime'});
                                    }
                                    if (exprireTime < expireTimeAfterPeriodCheck) {
                                        return res.json({ error: true, message: 'exprireTime_not_<_expireTimeAfterPeriod', data: expireTimeAfterPeriodCheck });
                                    }

                                    meta.expireTimeLate = exprireTime;// LƯU LẠI NGÀY LÙI
                                    // dateNow2     = moment(dateNow2).startOf('day').format();
                                    console.log({
                                        expireTimeAfterPeriod, exprireTime
                                    });
                                    // let numberDateBetweenDate = subStractDateGetDay({date1: expireTimeAfterPeriod, date2: dateNow2 });
                                    let numberDateBetweenExpireTimeVsExpireTimeAfterPeriod = subStractDateGetDay({date1: expireTimeAfterPeriodCheck, date2: exprireTime });
                                    // console.log({
                                    //     expireTimeAfterPeriod, exprireTime,
                                    //     numberDateBetweenExpireTimeVsExpireTimeAfterPeriod
                                    // });
                                    // console.log({
                                    //     numberDateBetweenDate,
                                    //     exprireTime: meta.exprireTime,
                                    //     numberDateBetweenExpireTimeVsExpireTimeAfterPeriod
                                    // });
                                    /**                                                                |---------------------| 
                                     *  EXAMPLE:                                                       |(2/9)                |   
                                     *      |------------------------------------|-------------|-------|------|              |
                                     *     1/8                                 31/8           1/9            3/9             |
                                     *       ----------------30 ngày--------------(trễ 1 ngày)---------(Nhận được tiền) -> Dời 1 ngày (2/9)
                                     * 
                                     *      => NUMBER_DATE_HAVE_PERIOD = 1 ngày trễ (31/8 -> 1/9) + 30 NGÀY (1 Chu kỳ) 
                                     *                                   (numberDateBetweenDate)  + (DAY_OF_MONTH)
                                     */
                                    const DATE_MOVING = 1;
                                    console.log({
                                        numberDateBetweenExpireTimeVsExpireTimeAfterPeriod
                                    });
                                    // numberDateBetweenDate     = Math.floor(Math.abs(numberDateBetweenDate)) ;
                                    numberDateBetweenExpireTimeVsExpireTimeAfterPeriod     = Math.floor(Math.abs(numberDateBetweenExpireTimeVsExpireTimeAfterPeriod));
                                    // console.log({ numberDateBetweenDate });
                                    // NUMBER_DATE_HAVE_PERIOD   = numberDateBetweenDate + DAY_OF_MONTH + DATE_MOVING;
                                    // NUMBER_DATE_HAVE_PERIOD         = numberDateBetweenDate + DAY_OF_MONTH; // SỐ NGÀY TÍNH LÃI
                                    // NUMBER_DATE_HAVE_PERIOD_NOT_NOW = numberDateBetweenExpireTimeVsExpireTimeAfterPeriod + DAY_OF_MONTH; // SỐ NGÀY ĐƯỢC TÍNH TỪ NGÀY CHỐT LÃI ĐƯỢC NHẬP VÀO
                                    NUMBER_DATE_HAVE_PERIOD = numberDateBetweenExpireTimeVsExpireTimeAfterPeriod + DAY_OF_MONTH; // SỐ NGÀY ĐƯỢC TÍNH TỪ NGÀY CHỐT LÃI ĐƯỢC NHẬP VÀO
                                    // console.log({ NUMBER_DATE_HAVE_PERIOD, NUMBER_DATE_HAVE_PERIOD_NOT_NOW });
                                    console.log({
                                        NUMBER_DATE_HAVE_PERIOD,
                                    });
                                    // if (meta.cycleInterest) {
                                    //     let period = meta.cycleInterest;
                                    //     NUMBER_DATE_HAVE_PERIOD += DAY_OF_MONTH * Number(period);
                                    // }
                                }
                            } else {
                                return res.json({ error: true, message: 'pay_late_invalid'})
                            }
                            
                            /**
                             * TÍNH LÃI VỚI SỐ NGÀY (NUMBER_DATE_HAVE_PERIOD)
                             */
                                
                            //  let { data: infoInterest } = await INTEREST_RATE_MODEL.checkDateToInterestTransaction({ agencyID, interestStartDate, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            //  meta.priceMuchPayInterest = infoInterest.loanAmoutAfterInterest;
                            let { data: infoInterest } = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: NUMBER_DATE_HAVE_PERIOD, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            meta.priceMuchPayInterest  = Math.floor(infoInterest.loanAmoutAfterInterest);
                            meta.priceMuchPayInterestOriginal = Math.floor(infoInterest.loanAmoutAfterInterest);
                            // console.log({
                            //     infoInterest,
                            //     NUMBER_DATE_HAVE_PERIOD
                            // });
                            let totalPercentHistory    = infoInterest.percetTotal;
                            // console.log({ infoInterest, NUMBER_DATE_HAVE_PERIOD });
                            meta = {
                                ...meta,
                                cycleInterest: ONE_PERIOD, // 1 KÌ
                                // numberDatePayLate: NUMBER_DATE_HAVE_PERIOD_NOT_NOW ? NUMBER_DATE_HAVE_PERIOD_NOT_NOW : NUMBER_DATE_HAVE_PERIOD,
                                numberDatePayLate: NUMBER_DATE_HAVE_PERIOD, // SỐ NGÀY TÍNH LÃI
                                percentDatePayLate: totalPercentHistory, // % TÍNH LÃI
                                statusFinancial, // LOẠI QUỸ
                                loanAmount: infoTransaction.loanAmount, // Số tiền vay trước khi 
                                loanAmountAfterChange: infoTransaction.loanAmount  // Số tiền vay sau khi xảy ra gd
                            }
                            // console.log({ meta });

                            /**
                             * NGÀY SAU KHI TÍNH LÃI 
                             */
                            // let dateAfterPay = subtractDate(NUMBER_DATE_HAVE_PERIOD, expireTime);
                            // console.log({ arrObjPerPeriod: infoInterest.arrObjPerPeriod });

                            const STATUS_IN_PROCESS = 3;
                            let objInject = {
                                title: "GIAO DỊCH BÌNH THƯỜNG TRẢ LÃI TRỄ", 
                                type: TRANSACTION_NORMAL, 
                                transaction: transactionID,
                                note, meta, agencyID, status: STATUS_IN_PROCESS, 
                                numberDatePayLate: NUMBER_DATE_HAVE_PERIOD,
                                userCreate: userID
                            };
                            // console.log({ objInject });
                            let infoAfterInsert = await TRANSACTION_MODEL.insertInjectTransaction({  
                                meta, userID,
                                payInterest, 
                                objInject, 
                                listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages
                            });
                            // console.log({ infoAfterInsert });
                            res.json(infoAfterInsert)
                        }else{
                            res.json({ error: true, message: "cannot_insert_inject_transaction" })
                        }
                    }]
                },
            },

            /**
             * Function: Update giao dịch trả lãi trễ
             */
            [CF_ROUTINGS_TRANSACTION.UPDATE_INJECT_TRANSACTION_NORMAL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let { transactionID } = req.params;
                        let { noteNotApproved, statusFinancial, injectTransID, status } = req.body
                        let agencyID  = req.agencyID;
                        let infoTransaction = await TRANSACTION_MODEL.getInfo({ transactionID });
                        // 1//Lấy thông tin inject transaction
                        let infoInjectTransaction = await INJECT_TRANSACTION_COLL.findById(injectTransID);
                        
                        const STATUS_ACTIVE = 1;
                        let funds;
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                        let objectConditionForFindFund = {
                            agency: agencyID,
                        };
                        
                        const LOAI_QUY_TIEN_MAT       = 1;
                        const LOAI_QUY_TIEN_NGAN_HANG = 2;
                        
                        // (infoInjectTransaction.statusFinancial) LOẠI QUỸ
                        objectConditionForFindFund = infoInjectTransaction.meta.statusFinancial == LOAI_QUY_TIEN_MAT ?
                            { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                            { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                        let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                        
                        if (!latestRecordFinicalOfAgency) {
                            return res.json({ error: true, message: "", description: 'vui lòng tạo giao dịch nạp tiền'});
                        } else {
                            funds = latestRecordFinicalOfAgency.fundsAfterChange;
                        }

                        if (infoInjectTransaction.meta.priceMuchPayInterest < 0) {
                            let parseMuchPayInterest = Math.abs(infoInjectTransaction.meta.priceMuchPayInterest);
                            if (parseMuchPayInterest > funds) {
                                return res.json({ error: true, message: "funds_not_enough"}) 
                            }
                        }
                        /**
                         * -------------------KIỂM TRA GIAO DỊCH CÒN HOẠT ĐỘNG--------------------
                         */
                        
                        if(infoTransaction.data.status == STATUS_ACTIVE) {
                            /**
                            * -------------------CONSTANTS/VARIABLE--------------------
                            */
                            const STATUS_INJECT_ACCEPT   = 1;
                            const TRANSACTION_NORMAL     = 2;
                            const STATUS_INJECT_IN_ACCEPT= 2;
                            const TYPE_FINANCIAL         = 1;
                            let title                    = `${infoInjectTransaction.code} + ${infoTransaction.data.customer.name} + GIAO DỊCH BÌNH THƯỜNG TRẢ LÃI TRỄ + ${infoInjectTransaction.meta.priceMuchPayInterest}`;
                            let objInjectUpdate          = {};   
                            let objFinancial             = {};
                            let objUpdateTransactionMain = {};
                            let objPayInterest           = {};
                            let objHistory               = {};
                            let expireTime               = infoTransaction.data.expireTime; 
                            let interestStartDate        = infoTransaction.data.interestStartDate; 
                            let payInterest              = infoTransaction.data.payInterest; 

                            /**
                             * ---------------KIỂM TRA STATUS------------------
                             */
                            if (status == STATUS_INJECT_IN_ACCEPT) {
                                objInjectUpdate = {
                                    noteNotApproved: noteNotApproved, status
                                }
                            }

                            if (status == STATUS_INJECT_ACCEPT) { 
                                /**
                                 * ----------------------OBJ THÊM QUỸ----------------------
                                 */ 
                                objFinancial = {
                                    type:   TYPE_FINANCIAL,
                                    agency: agencyID, 
                                    price:  infoInjectTransaction.meta.priceMuchPayInterest, 
                                    funds, 
                                    onModel: "inject_transaction",
                                    statusFinancial: infoInjectTransaction.meta.statusFinancial
                                }    

                                /**
                                 * NGÀY SAU KHI TÍNH LÃI 
                                 * infoInjectTransaction.meta.numberDatePayLate +1 vì tính từ ngày chốt lãi chứ kh phải ngày bắt đầu tính lãi
                                 */
                                let dateAfterPay = subtractDate(infoInjectTransaction.meta.numberDatePayLate, expireTime);
                                console.log({ 
                                    dateAfterPay, expireTime
                                });

                                /**
                                 * ----------------------OBJ UPDATE GIAO DỊCH CHÍNH----------------------
                                 */
                                objUpdateTransactionMain = {
                                    interestStartDateNew: dateAfterPay.interestStartDate,
                                    expireTimeNew:        dateAfterPay.expireTime,
                                    transactionDateNew:   new Date(),
                                    // expireTimeAfterPeriod:   dateAfterPay.expireTimeAfterPeriod,
                                    transactionID
                                }
                                /**
                                 * ----------------------OBJ THÊM GIAO DỊCH TRẢ TRƯỚC (PAY INTEREST)----------------------
                                 *  *  EXAMPLE:
                                 *                                (Ngày chốt lãi kỳ trễ)     (Ngày chốt lãi chu kỳ tiếp theo)
                                 *      |-------------(Trả trễ)--------------|----------(Trả trước)---------| 
                                 *      |                                    |         (Ngày trả)           |
                                 *      |------------------------------------|-------------|----------------|
                                 *     1/8                                 31/8           1/9             (30/9)
                                 *       ----------------30 ngày--------------(trễ 1 ngày)---------------(Trả 2 CHU KÌ)
                                 *      => TRẢ TRƯỚC (30) NGÀY
                                 */

                                let limitDatePayBefore = subStractDateGetDay({date1: new Date(), date2: interestStartDate });
                                limitDatePayBefore     = Math.floor(limitDatePayBefore) + 1;
                                if (limitDatePayBefore > 0) {
                                    let numberDatePayLate = subStractDateGetDay({date1: dateAfterPay.expireTime, date2: expireTime });
                                    numberDatePayLate     = Math.floor(numberDatePayLate) + 1;
                                    objPayInterest = {
                                        title: infoInjectTransaction.title,
                                        type: TRANSACTION_NORMAL,
                                        employee: userID,
                                        interestRateTotal: infoInjectTransaction.meta.priceMuchPayInterest,
                                        period: Number(infoInjectTransaction.meta.cycleInterest),
                                        expireTime: dateAfterPay.expireTime,
                                        interestStartDate: interestStartDate,
                                        resetDate: new Date(),
                                        interstAmountDate: numberDatePayLate,
                                        interestPercent: infoInjectTransaction.meta.percentDatePayLate,
                                        transactionMain: transactionID
                                    }
                                }
                                /**
                                 * ----------------------OBJ THÊM HISTORY GIAO DỊCH----------------------
                                 */
                                objHistory = {
                                    title: infoInjectTransaction.title,
                                    employee: userID,
                                    interestRateTotal: infoInjectTransaction.meta.priceMuchPayInterest,
                                    interestStartDate: interestStartDate,
                                    resetDate: new Date(),
                                    interstAmountDate: infoInjectTransaction.meta.numberDatePayLate,
                                    interestPercent: infoInjectTransaction.meta.percentDatePayLate,
                                    transactionMain: transactionID
                                }
                                /**
                                 * ----------------------OBJ UPDATE GIAO DỊCH BỔ SUNG----------------------
                                 */ 
                                objInjectUpdate = {
                                    status, meta: infoInjectTransaction.meta, transactionDate: new Date()
                                };
                                // console.log({ 
                                //     objUpdateTransactionMain,
                                //     objHistory,
                                //     limitDatePayBefore,

                                // });
                                let infoAfteUpdate = await TRANSACTION_MODEL.updateInjectTransaction({  
                                    payInterest, 
                                    objInject:                objInjectUpdate, // OBJ UPDATE INJECT
                                    objUpdateTransactionMain: objUpdateTransactionMain, // OBJ UPDATE GIAO DỊCH CHÍNH
                                    objHistory:               objHistory, // OBJ LƯU LẠI LỊCH SỬ
                                    objFinancial:             objFinancial, // OBJ LƯU LẠI QUỸ
                                    objPayInterest:           objPayInterest, // OBJ LƯU LẠI TRẢ TRƯỚC
                                    customerName:             infoTransaction.data.customer.name,
                                    injectTransID,
                                    title:                    title,
                                    type:                     TRANSACTION_NORMAL 
                                });
                                if (infoAfteUpdate.error) {
                                    return res.json(infoAfteUpdate);
                                }
                            }
                            objInjectUpdate.meta && delete objInjectUpdate.meta

                            console.log({ objInjectUpdate });
                            let infoInjectAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(injectTransID, {
                                ...objInjectUpdate, userConfirm: userID
                            }, {
                                new: true
                            });
                            if (!infoInjectAfterUpdate) {
                                return res.json({ error: true, message: "cannot_update" });
                            }
                            res.json({ error: false, data: infoInjectAfterUpdate });

                        } else {
                            res.json({ error: true, message: "transaction_ransomed_or_in_process" });
                        }
                    }]
                },
            },

            /**
             * Function: TẠO GIAO DỊCH BÌNH THƯỜNG TRẢ TRƯỚC
             * Dev: SONLP
             */
            [CF_ROUTINGS_TRANSACTION.ADD_INJECT_TRANSACTION_NORMAL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title, type, note, meta, statusFinancial, option, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages  } = req.body;

                        let infoTransaction  = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoDataAgency   = await AGENCY_MODEL.getInfo({ agencyID });

                        if(infoTransaction.error) {
                            return res.json(infoTransaction);
                        }
                        /**
                         * GIAO DỊCH HOÀN THÀNH
                         */
                        const STATUS_COMPLETE = 3;
                        const LOAN_AMOUNT_PAIED = 3;
                        if(infoTransaction.loanAmount == LOAN_AMOUNT_PAIED || infoTransaction.data.status == STATUS_COMPLETE) {
                            return res.json({ error: true, message: "transaction_complete"});
                        }
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CHUỘC ĐỒ HAY CHƯA
                         */
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.type == 6 && infoTransaction.data.latestInjectTransaction.ransom == 2) {
                            return res.json({ error: true, message: "transaction_ransom_existed"});
                        }
                        if(infoDataAgency.error) {
                            return res.json(infoDataAgency);
                        }
                        if (!statusFinancial) {
                            return res.json({ error: true, message: "statusFinancial_invalid"})
                        }
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CẦN DUYỆT HAY KHÔNG
                         */
                         if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.status == 3) {
                            return res.json({ error: true, message: "transaction_have_check_existed"});
                        }
                        /**
                         * 1:// Kiểm tra xem giao dịch có được duyệt
                         */
                        if (infoTransaction.data.status == 1) {
                            infoTransaction = infoTransaction.data;
                            infoDataAgency  = infoDataAgency.data;
                            /**
                             * TYPE = 1 (THU TIỀN)
                             */
                            const TYPE = 1
                            let typeFinancial        = TYPE;
                            const DAY_OF_MONTH       = 30;
                            const TRANSACTION_NORMAL = 2;

                            /**
                             * NẾU META KHÔNG TỒN TẠI THÌ SET META LÀ OBJ RỖNG
                             */
                            if (!meta) {
                                meta = {};
                            }
                            /**
                             * LẤY ID DANH MỤC SẢN PHẨM
                             */
                            let TYPE_PRODUCT = infoTransaction.products[0].type._id;
                            /**
                             * LẤY THÔNG TIN QUỸ CỦA ĐẠI LÝ
                             */
                             /**
                              * Check Loại quỹ
                              *     1: Quỹ Tiền Mặt
                              *     2: Quỹ Ngân Hàng
                              */
                            // let funds = getFundsAgency({ statusFinancial, infoDataAgency });
                            let funds;
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                            let objectConditionForFindFund = {
                                agency: agencyID,
                            };
                           
                            const LOAI_QUY_TIEN_MAT = 1;
                            const LOAI_QUY_TIEN_NGAN_HANG = 2;

                            objectConditionForFindFund = statusFinancial == LOAI_QUY_TIEN_MAT ?
                                { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                                { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                            let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                            
                            if (!latestRecordFinicalOfAgency) {
                                return res.json({ error: true, message: "", description: 'vui lòng tạo giao dịch nạp tiền'});
                            } else {
                                funds = latestRecordFinicalOfAgency.fundsAfterChange;
                            }
                             /**
                             *  2// LẤY NGÀY TÍNH LÃI VÀ SỐ TIỀN CÒN DƯ TRONG GIAO DỊCH VÀ NGÀY GIAO DỊCH CẦM
                             */
                            let interestStartDate     = infoTransaction.interestStartDate;
                            let payInterest           = infoTransaction.payInterest; 
                            let loanAmount            = infoTransaction.loanAmount; 
                            let createAt              = infoTransaction.createAt; 
                            let expireTime            = infoTransaction.expireTime; 
                            let expireTimeAfterPeriod = infoTransaction.expireTimeAfterPeriod; 
                            let dateNow               = new Date();
                            let NUMBER_DATE_HAVE_PERIOD;
                            let titleTransaction      = '';
                            let payInterestBefore     = 0;
                            const ONE_PERIOD          = 1
                            let period;

                            /**
                             * KIỂM TRA GIAO DỊCH TRẢ TRƯỚC
                             *  EXAMPLE:
                             *      |------------------------------------|----------------|
                             *     1/8                                 31/8 
                             *      |------------ TRẢ TRƯỚC---------TRẢ ĐÚNG KÌ -------TRỂ KÌ
                             * (interestStartDate)             (expireTimeAfterPeriod)
                             */
                            let dateCheck = new Date();
                            /**
                             * --------------------------TRẢ TRƯỚC-------------------------
                             */
                            /**
                             *      - TH1: TRẢ TRƯỚC NHIỀU KÌ
                             *          EXAMPLE: 
                             *              Khách hàng muốn trả trước nhiều kỳ (2, 3 kỳ trở lên);
                             *      - TH2: TRẢ TRONG KỲ
                             *          EXAMPLE:  
                             *          (compareTwoDayCheck(dateCheck, interestStartDate) == 'bigger_equal' && compareTwoDayCheck(expireTimeAfterPeriod, new Date(dateCheck)) == 'smaller_equal'
                             *          (compareTwoDayCheck(dateCheck, interestStartDate) == 'bigger'       && compareTwoDayCheck(expireTimeAfterPeriod, new Date(dateCheck)) == 'bigger')            
                             *              Khách hàng chỉ hoàn thành kỳ hiện tại (1 kỳ)
                             * `    - TH3: TRẢ TRỄ KỲ -> ĐẾN NGÀY HIỆN TẠI
                             *          EXAMPLE: 
                             *              Khách hàng đóng ngày thứ 32 -> nhân viên cho đóng tròn kỳ -> đóng 30 ngày (tùy theo số chu kỳ khách muốn đóng tròn)
                             *      - TH3: ĐÃ TRẢ TRƯỚC -> TRẢ THÊM LẦN NỮA (compareTwoDayCheck(expireTime, dateCheck) == 'bigger') (compareTwoDayCheck(expireTime, dateCheck) == 'bigger_equal')
                             *          EXAMPLE: 
                             *              Khách hàng đã trả trước 1 kỳ và sau đó trả trước thêm 1 kỳ nữa
                             */
                            // console.log({ dateCheck, interestStartDate, expireTimeAfterPeriod });
                            // console.log(compareTwoDayCheck(dateCheck, interestStartDate));
                            // console.log(compareTwoDayCheck(expireTimeAfterPeriod, dateCheck));
                            // console.log(compareTwoDay(createAt, dateNow));
                            // console.log(compareTwoDayCheck(expireTime, dateCheck));
                            
                            if (
                                /**
                                 * ==============================CHECK TRẢ TRONG 1 KỲ===============================
                                 */
                                (compareTwoDayCheck(dateCheck, interestStartDate) == 'bigger_equal' && compareTwoDayCheck(expireTimeAfterPeriod, new Date(dateCheck)) == 'bigger_equal') || 
                                (compareTwoDayCheck(dateCheck, interestStartDate) == 'bigger'       && compareTwoDayCheck(expireTimeAfterPeriod, new Date(dateCheck)) == 'bigger') || 
                                (compareTwoDayCheck(dateCheck, interestStartDate) == 'bigger_equal' && compareTwoDayCheck(expireTimeAfterPeriod, new Date(dateCheck)) == 'bigger') || 
                                (compareTwoDayCheck(dateCheck, interestStartDate) == 'bigger'       && compareTwoDayCheck(expireTimeAfterPeriod, new Date(dateCheck)) == 'bigger_equal') ||
                                /**
                                 * ==============================TRẢ TRƯỚC LẦN ĐẦU TIÊN==============================
                                 */
                                (!infoTransaction.latestInjectTransaction && compareTwoDay(createAt, dateNow)) ||
                                /**
                                 * ==============================TRẢ TRƯỚC TIẾP SAU KHI ĐÃ TRẢ TRƯỚC==============================
                                 */
                                (compareTwoDayCheck(expireTime, new Date(dateCheck)) == 'bigger') ||
                                (compareTwoDayCheck(expireTime, new Date(dateCheck)) == 'bigger_equal')
                                // || (option && option == 'full_period')
                            ) {
                                console.log("--------------------------TRẢ TRƯỚC-------------------------");
                                /**
                                 * KIỂM TRA XEM CÓ PHẢI RECORD TRẢ LÃI ĐẦU TIÊN HAY KHÔNG VÀ TRẢ TROGN NGÀY CẦM 
                                 * compareTwoDayCheck
                                 */
                                period = meta.cycleInterest ?  meta.cycleInterest: ONE_PERIOD;
                                meta.cycleInterest = meta.cycleInterest ?  meta.cycleInterest: ONE_PERIOD;
                                if (!infoTransaction.latestInjectTransaction && compareTwoDay(createAt, dateNow)) {
                                    /**
                                     * EXAPMLE: 
                                     *  CHU KÌ 1: => TÍNH ngày tính lãi + 29 NGÀY
                                     *  CHU KÌ 2: => TÍNH ngày tính lãi + 29 NGÀY + 30 NGÀY
                                     *  CHU KÌ 3: => TÍNH ngày tính lãi + 29 NGÀY + 30 NGÀY + 30 NGÀY
                                     */
                                    const NUMBER_SUBTRACT_WHEN_PAY_BEFORE = 1;
                                    NUMBER_DATE_HAVE_PERIOD = DAY_OF_MONTH * Number(period);
                                    titleTransaction = 'GIAO DỊCH BÌNH THƯỜNG TRẢ TRƯỚC ĐẦU TIÊN';
                                } else {
                                    /**
                                     * EXAPMLE: 
                                     *  CHU KÌ 1: => TÍNH 30 NGÀY
                                     *  CHU KÌ 2: => TÍNH 30 NGÀY + 30 NGÀY
                                     *  CHU KÌ 3: => TÍNH 30 NGÀY + 30 NGÀY + 30 NGÀY
                                     */
                                    NUMBER_DATE_HAVE_PERIOD = DAY_OF_MONTH * Number(period);
                                    titleTransaction = 'GIAO DỊCH BÌNH THƯỜNG TRẢ TRƯỚC';
                                }
                                payInterestBefore = 1;
                            }
                            /**s
                             * TRẢ ĐÚNG KÌ
                             */   
                            // if (compareTwoDayCheck(dateCheck, expireTimeAfterPeriod) == 'equal') {
                            //     console.log("TRẢ ĐÚNG KÌ");
                            //     console.log({ dateCheck, expireTimeAfterPeriod, interestStartDate });
                            //     let limitDate = subStractDateGetDay({ date1: expireTime, date2: dateCheck });
                            //     console.log({ limitDate });
                            //     // minusNumberToSetDate
                            // }
                            /**
                             * --------------------------TRẢ TRỄ-------------------------
                             */
                             if (compareTwoDayCheck(dateCheck, expireTimeAfterPeriod) == 'bigger') {
                                console.log("--------------------------TRẢ TRỄ-------------------------");
                                titleTransaction = 'GIAO DỊCH BÌNH THƯỜNG TRẢ LÃI TRỄ';
                                /**
                                 * LẤY RA SỐ NGÀY ĐÓNG TRỄ
                                */
                                let dateNow = new Date();
                                dateNow     = moment(dateNow).startOf('day').format();
                                
                                let numberDateBetweenDate = subStractDateGetDay({date1: expireTimeAfterPeriod, date2: dateNow });

                                /**
                                 *  EXAMPLE:
                                 *      |------------------------------------|-------------|
                                 *     1/8                                 31/8           1/9
                                 *       ----------------30 ngày--------------(trễ 1 ngày)--
                                 * 
                                 *      => NUMBER_DATE_HAVE_PERIOD = 30 NGÀY trong 1 chu kỳ + 1 ngày (31/8 -> 1/9)    
                                 */
                                // meta.cycleInterest = meta.cycleInterest ?  meta.cycleInterest: ONE_PERIOD;
                                numberDateBetweenDate     = Math.floor(Math.abs(numberDateBetweenDate));
                                console.log({ numberDateBetweenDate });
                                // NUMBER_DATE_HAVE_PERIOD   = numberDateBetweenDate + DAY_OF_MONTH;
                                NUMBER_DATE_HAVE_PERIOD   = numberDateBetweenDate + DAY_OF_MONTH;

                                // if (meta.cycleInterest) {
                                //     NUMBER_DATE_HAVE_PERIOD += DAY_OF_MONTH * Number(period);
                                // } 
                                // period = meta.cycleInterest ?  meta.cycleInterest: ONE_PERIOD;
                                period = ONE_PERIOD;
                            }
                            console.log({ NUMBER_DATE_HAVE_PERIOD });
                            /**
                             * TÍNH LÃI VỚI SỐ NGÀY (NUMBER_DATE_HAVE_PERIOD)
                             */
                            //  let { data: infoInterest } = await INTEREST_RATE_MODEL.checkDateToInterestTransaction({ agencyID, interestStartDate, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            //  meta.priceMuchPayInterest = infoInterest.loanAmoutAfterInterest;
                            let { data: infoInterest }        = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: NUMBER_DATE_HAVE_PERIOD, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            meta.priceMuchPayInterest         = Math.floor(infoInterest.loanAmoutAfterInterest);
                            meta.priceMuchPayInterestOriginal = Math.floor(infoInterest.loanAmoutAfterInterest);
                            let totalPercentHistory           = infoInterest.percetTotal;    
                            meta.loanAmount      = loanAmount;// Số tiền vay trước khi 
                            meta.statusFinancial = statusFinancial;// LOẠI QUỸ
                            meta.loanAmountAfterChange  = loanAmount;// Số tiền vay sau khi xảy ra gd
                            /**
                             * NGÀY SAU KHI TÍNH LÃI 
                             */
                            let dateAfterPay = subtractDate(NUMBER_DATE_HAVE_PERIOD, expireTime);
                            console.log({ dateAfterPay, arrObjPerPeriod: infoInterest.arrObjPerPeriod });

                            let objInject = {
                                userCreate: userID,
                                title: titleTransaction, type: TRANSACTION_NORMAL, transaction: transactionID, note, meta, agencyID, 
                            };
                            let objUpdateTransactionMain = {
                                interestStartDateNew: dateAfterPay.interestStartDate,
                                expireTimeNew:        dateAfterPay.expireTime,
                                transactionDateNew:   new Date(),
                                // expireTimeAfterPeriod:   dateAfterPay.expireTimeAfterPeriod,
                                transactionID
                            }
                            /**
                             * OBJ THÊM GIAO DỊCH TRẢ TRƯỚC
                             */
                            let objPayInterest = {};
                            if (payInterestBefore) {
                                objPayInterest = {
                                    title: titleTransaction,
                                    type: TRANSACTION_NORMAL,
                                    employee: userID,
                                    interestRateTotal: Math.floor(infoInterest.loanAmoutAfterInterest),
                                    period: period,
                                    expireTime: dateAfterPay.expireTime,
                                    interestStartDate: interestStartDate,
                                    resetDate: new Date(),
                                    interstAmountDate: NUMBER_DATE_HAVE_PERIOD,
                                    interestPercent: totalPercentHistory,
                                    transactionMain: transactionID
                                }
                            }

                            let objHistory = {
                                title: objInject.title,
                                employee: userID,
                                interestRateTotal: objInject.meta.priceMuchPayInterest,
                                interestStartDate: infoTransaction.interestStartDate,
                                resetDate: new Date(),
                                interstAmountDate: NUMBER_DATE_HAVE_PERIOD,
                                interestPercent: totalPercentHistory,
                                transactionMain: transactionID
                            }

                            let objFinancial = {
                                type:   typeFinancial,
                                agency: agencyID, 
                                // price:  objInject.meta.priceMuchPayInterest, 
                                funds, 
                                onModel: "inject_transaction",
                                statusFinancial
                            }
                            
                            let infoAfterInsert = await TRANSACTION_MODEL.insertInjectTransactionInterest({  
                                meta, userID,
                                objUpdateTransactionMain, 
                                payInterest, 
                                transactionID, 
                                objPayInterest, 
                                objInject, 
                                customerName: infoTransaction.customer.name,
                                typeFinancial, objFinancial, objHistory,
                                listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages
                            });
                            // if (infoAfterInsert.error) {
                            //     res.json(infoAfterInsert)
                            // }
                            res.json(infoAfterInsert)
                        }else{
                            res.json({ error: true, message: "cannot_insert_inject_transaction" })
                        }
                    }]
                },
            },

            /**
             * Function: TẠO GIAO DỊCH VAY THÊM
             * Dev: SONLP
             */
            [CF_ROUTINGS_TRANSACTION.ADD_INJECT_TRANSACTION_ARROW]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title, type, note, meta, statusFinancial, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages  } = req.body;
                        
                        let infoTransaction  = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoDataAgency   = await AGENCY_MODEL.getInfo({ agencyID });

                        if(infoTransaction.error) {
                            return res.json(infoData);
                        }
                        if(infoDataAgency.error) {
                            return res.json(infoDataAgency);
                        }
                        // return 
                        /**
                         * GIAO DỊCH HOÀN THÀNH
                         */
                        const STATUS_COMPLETE   = 3;
                        const LOAN_AMOUNT_PAIED = 0;
                        if(infoTransaction.loanAmount == LOAN_AMOUNT_PAIED || infoTransaction.data.status == STATUS_COMPLETE) {
                            return res.json({ error: true, message: "transaction_complete"});
                        }
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CHUỘC ĐỒ HAY CHƯA
                         */
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.type == 6 && infoTransaction.data.latestInjectTransaction.ransom == 2) {
                            return res.json({ error: true, message: "transaction_ransom_existed"});
                        }
                       
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CẦN DUYỆT HAY KHÔNG
                         */
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.status == 3) {
                            return res.json({ error: true, message: "transaction_have_check_existed"});
                        }
                        if (!statusFinancial) {
                            return res.json({ error: true, message: "statusFinancial_invalid"})
                        }

                        if (infoTransaction.data.status == 1) {
                            /**
                             * ======================================================================
                             * ==========================CONSTANTS/VARIABLE==========================
                             * ======================================================================
                             */
                            infoTransaction       = infoTransaction.data;
                            infoDataAgency        = infoDataAgency.data;
                            const TYPE_BORROW     = 3;
                            let interestStartDate = infoTransaction.interestStartDate;
                            let dateNow           =  new Date();
                            let funds;
                            const DATE_OF_PERIOD          = 30;
                            const TYPE                    = 2; // TYPE = 2 (CHI TIỀN)
                            let typeFinancial             = TYPE;
                            const LOAI_QUY_TIEN_MAT       = 1;
                            const LOAI_QUY_TIEN_NGAN_HANG = 2;
                            let price                     = Number(meta.amountWantBorrowMore);
                            const TYPE_PRODUCT            = infoTransaction.products[0].type._id;
                            let objInjectNormal           = {};
                            let objUpdateTransactionMain  = {};
                            let dataUpdate                = {}
                            const STATUS_IN_PROCESS       = 3;
                            const TYPE_TRANSACTION_BORROW = 3;

                            /**
                             * ======================================================================
                             * =========================GET LAST RECORD FUNDS========================
                             * ======================================================================
                             */
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                            // let objectConditionForFindFund = {
                            //     agency: agencyID,
                            // };
                           
                            // objectConditionForFindFund = statusFinancial == LOAI_QUY_TIEN_MAT ?
                            //     { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                            //     { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                            // let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                            
                            // if (!latestRecordFinicalOfAgency) {
                            //     return res.json({ error: true, message: "fund_not_enough", description: 'vui lòng tạo giao dịch nạp tiền'});
                            // } else {
                            //     funds = latestRecordFinicalOfAgency.fundsAfterChange;
                            // }
                            /**
                             * ======================================================================
                             * ==============================VALIDATION==============================
                             * ======================================================================
                             */

                            if (!meta.amountWantBorrowMore || Number.isNaN(Number(meta.amountWantBorrowMore))) // CHECK GIÁ MUỐN VAY TỒN TẠI
                                return res.json({ error: true, message: "amountWantBorrowMore_invalid"});
                            if (!meta.canBorrowMore || Number.isNaN(Number(meta.canBorrowMore))) // CHECK GIÁ CÓ THỂ VAY THÊM
                                return res.json({ error: true, message: "canBorrowMore_invalid"});
                            if (Number(meta.canBorrowMore) < Number(meta.amountWantBorrowMore)) // CHECK GIÁ CÓ THỂ VAY THÊM > GIÁ MUỐN VAY
                                return res.json({ error: true, message: "amountWantBorrowMore_can_>_canBorrowMore"});
                            if (funds < Number(meta.amountWantBorrowMore)) {
                                return res.json({ error: true, message: "fund_not_enough"});
                            }
                            /**
                                3// LẤY SỐ TIỀN CÓ THỂ VAY THÊM
                                    => HẠN MỨC PHÊ DUYỆT - SỐ TIỀN VAY = SỐ TIỀN CÓ THỂ VAY
                             */
                            let limitPrice = infoTransaction.approvalLimit - infoTransaction.loanAmount;
                            if ( price > limitPrice ){
                                return res.json({ error: true, message: "price_borrow_more_invalid" })
                            }else{
                                /**
                                 *  CỘNG THÊM SỐ TIỀN VAY 
                                 */
                                price += infoTransaction.loanAmount;
                            }

                            /**
                             * ======================================================================
                             * ==========================CALCULATE INTEREST==========================
                             * ======================================================================
                             */
                             /**
                             *  ====================TÍNH SỐ NGÀY TRẢ LÃI CHO ĐẾN HIỆN TAIN=========================
                             */
                            let dataNow2 = new Date()
                            interestStartDate     = moment(interestStartDate).startOf('day').format();

                             let numberDateBetweenDate = subStractDateGetDay({date1: dataNow2, date2: interestStartDate });
                            //  numberDateBetweenDate     = Math.floor(numberDateBetweenDate) + 1;
                            numberDateBetweenDate     = Math.floor(numberDateBetweenDate);
                            if (numberDateBetweenDate < 0) {
                                numberDateBetweenDate = 0;
                            }

                            meta.numberDatePayInterest = numberDateBetweenDate;
                            meta = {
                                ...meta,
                                statusFinancial,
                                loanAmount: infoTransaction.loanAmount,
                                loanAmountAfterChange: Number(infoTransaction.loanAmount) + Number(meta.amountWantBorrowMore),// Số tiền vay sau khi xảy ra gd
                            }
                            
                             /**
                             *  ====================TÍNH PHẦN TRĂM LÃI VÀ SỐ TIỀN LÃI CỦA GIAO DỊCH=========================
                             */
                            let { data: infoInterest } = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: numberDateBetweenDate, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            meta.priceMuchPayInterest  = Math.floor(infoInterest.loanAmoutAfterInterest);
                            meta.priceMuchPayInterestOriginal = Math.floor(infoInterest.loanAmoutAfterInterest);

                            meta.percentDatePayLate    = infoInterest.percetTotal;
                            
                            /**
                             * ==========================SỐ TIỀN ĐÃ TRẢ TRƯỚC (PAY INTEREST)================================
                             */
                            let { data: pricePayBefore } = await TRANSACTION_MODEL.getPricePayInterest({ agencyID, transactionID, TYPE_PRODUCT, loanAmount: infoTransaction.loanAmount });
                            meta.pricePayInterest = pricePayBefore;
                            
                            /**
                             * ==============SET LẠI TỔNG TIỀN VAY THÊM====================
                             */
                            meta.totalAmountWantBorrowMore = Number(meta.amountWantBorrowMore) - meta.priceMuchPayInterest;
                            console.log({ meta, infoInterest, objInjectNormal, pricePayBefore });
                            /**
                             * ==========================KIỂM TRA GIÁ KHUYẾN MÃI CÓ TỒN TẠI VÀ < SỐ TIỀN LÃI==========================
                             */
                             if (meta.promotionInterest && meta.promotionInterest > meta.priceMuchPayInterest) {
                                return res.json({ error: true, message: "promotionInterest_invalid" });
                            }else if (meta.promotionInterest) {
                                meta.totalAmountWantBorrowMore = meta.totalAmountWantBorrowMore + Number(meta.promotionInterest);
                            }

                          
                            /**
                             * ======================================================================
                             * ==============================LƯU HÌNH ẢNH============================
                             * ======================================================================
                             */
                            if(listUrlCustomerImages && listUrlCustomerImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlCustomerImages, userCreate: userID });
                                dataUpdate.customerImages    = listIDImage;
                            }
                            if(listUrlKYCImages && listUrlKYCImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlKYCImages, userCreate: userID });
                                dataUpdate.KYCImages         = listIDImage;
                            }
                            if(listUrlReceiptImages && listUrlReceiptImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                dataUpdate.receiptImages     = listIDImage;
                            }
                            if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                dataUpdate.formVerificationImages = listIDImage;
                            }
                            /**
                             * ======================================================================
                             * ==============================THÊM RECORD=============================
                             * ======================================================================
                             */
                            let obj = {
                                title: "GIAO DỊCH VAY THÊM", 
                                type: TYPE_TRANSACTION_BORROW, 
                                transaction: transactionID, 
                                note, meta, agencyID, status: STATUS_IN_PROCESS,
                                userCreate: userID
                            };
                            let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                                ...obj, dataUpdate
                            });
                            
                            if(infoAfterInsert.error) {
                                return res.json(infoAfterInsert);
                            }
                            return res.json(infoAfterInsert)
                            
                            // objUpdateTransactionMain = {
                            //     transactionID,
                            //     loanAmount: price
                            // }

                            // SET NAME THÊM VÀO QUỸ
                            // let nameOfFinanical = `${infoAfterInsert.data.code} + ` + `${infoTransaction.customer.name} + ` + "GIAO DỊCH VAY THÊM" + ` + ${obj.meta.amountWantBorrowMore}`;
                            
                            // Update lại số tiền vay 
                            // let infoDataAfterUpdate = await TRANSACTION_MODEL.update({ ...objUpdateTransactionMain });
                            // if(infoDataAfterUpdate.error) {
                            //     return res.json(infoDataAfterUpdate);
                            // }
                            /**
                             *  INSERT HISTORY TRANSACTION
                             */
                            //  let infoHistoryTransactionAfterInsert = await HISTORY_TRANSACTION_MODEL.insert({
                            //     title: "GIAO DỊCH VAY THÊM",
                            //     type: TYPE_BORROW,
                            //     employee: userID,
                            //     interestRateTotal: 0,
                            //     interestStartDate: infoTransaction.interestStartDate,
                            //     resetDate: dateNow,
                            //     transaction: infoAfterInsert.data._id,
                            //     interstAmountDate: 0,
                            //     interestPercent: 0,
                            //     transactionMain: transactionID
                            // });
                            // // UPDATE LẠI QUỸ
                            // let objFinancial = {
                            //     title:  nameOfFinanical, 
                            //     type:   typeFinancial,
                            //     typeTransaction: TYPE_BORROW,
                            //     agency: agencyID, 
                            //     price:  obj.meta.amountWantBorrowMore, 
                            //     funds, 
                            //     transaction: infoAfterInsert.data._id,
                            //     onModel: "inject_transaction",
                            //     statusFinancial
                            // }
                            // let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                            //     ...objFinancial
                            // });
                            // if (infoFinancialAfterInsert.error) {
                            //     return res.json(infoFinancialAfterInsert)
                            // }
                        }else{
                            res.json({ error: true, message: "cannot_insert_inject_transaction" })
                        }
                    }]
                },
            },

            /**
             * Function: DUYỆT GIAO DỊCH VAY THÊM
             * Dev: SONLP
             */
            [CF_ROUTINGS_TRANSACTION.UPDATE_INJECT_TRANSACTION_BORROW]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let { transactionID } = req.params;
                        let { noteNotApproved, statusFinancial, injectTransID, status } = req.body
                        let agencyID  = req.agencyID;
                         /**
                         * ==========================================================================
                         * =========================LẤY THÔNG TIN GIAO DỊCH==========================
                         * ==========================================================================
                        */
                        let infoTransaction = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoInjectTransaction = await INJECT_TRANSACTION_COLL.findById(injectTransID);
                        
                        /**
                         * ==========================================================================
                         * ============================CONSTANTS/VARIABLE============================
                         * ==========================================================================
                        */
                        const LOAI_QUY_TIEN_MAT       = 1;
                        const LOAI_QUY_TIEN_NGAN_HANG = 2;
                        const STATUS_ACTIVE = 1;
                        let funds;
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                        let objectConditionForFindFund = {
                            agency: agencyID,
                        };
                        /**
                         * ==========================================================================
                         * ============================LẤY LAST RECORD QUỸ===========================
                         * ==========================================================================
                        */
                        objectConditionForFindFund = infoInjectTransaction.meta.statusFinancial == LOAI_QUY_TIEN_MAT ?
                            { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                            { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                        let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                        
                        if (!latestRecordFinicalOfAgency) {
                            return res.json({ error: true, message: "", description: 'vui lòng tạo giao dịch nạp tiền'});
                        } else {
                            funds = latestRecordFinicalOfAgency.fundsAfterChange;
                        }

                        if (infoInjectTransaction.meta && infoInjectTransaction.meta.totalAmountWantBorrowMore > funds) {
                            return res.json({ error: true, message: "funds_not_enough"}) 
                        }
                        /**
                         * ============================KIỂM TRA GIAO DỊCH CÒN HOẠT ĐỘNG============================
                         */
                        if(infoTransaction.data.status == STATUS_ACTIVE) {
                             /**
                             * ==========================================================================
                             * ============================CONSTANTS/VARIABLE============================
                             * ==========================================================================
                            */
                            const BACK_ONE_DAY_EXPIRE_TIME = 1
                            const STATUS_INJECT_ACCEPT   = 1;
                            const TYPE_BORROW            = 3;
                            const STATUS_INJECT_IN_ACCEPT= 2;
                            const TYPE_FINANCIAL_EXPEND  = 2;
                            let title                    = `${infoInjectTransaction.code} + ${infoTransaction.data.customer.name} + GIAO DỊCH VAY THÊM + ${infoInjectTransaction.meta.totalAmountWantBorrowMore}`;
                            let objInjectUpdate          = {};   
                            let objFinancial             = {};
                            let objUpdateTransactionMain = {};
                            let objPayInterest           = {};
                            let objHistory               = {};
                            let payInterest              = infoTransaction.data.payInterest; 
                            let loanAmount               = infoTransaction.data.loanAmount; 
                            const TYPE_PRODUCT           = infoTransaction.data.products[0].type._id;


                            /**
                             * ==========================================================================
                             * =============================KIỂM TRA STATUS==============================
                             * ==========================================================================
                             * 
                             */
                            if (status == STATUS_INJECT_IN_ACCEPT) {
                                objInjectUpdate = {
                                    noteNotApproved: noteNotApproved, status, userConfirm: userID
                                }
                            }

                            if (status == STATUS_INJECT_ACCEPT) {
                                objInjectUpdate = {
                                    status, transactionDate: new Date(), userConfirm: userID
                                }
                                 /**
                                 * ==========================================================================
                                 * ==================================GHI CHÚ=================================
                                 * ==========================================================================
                                 * 
                                 * totalAmountWantBorrowMore: là số tiền được vay thêm => (SỐ TIỀN BỊ TRỪ VÀO QUỸ)
                                 *  => totalAmountWantBorrowMore = (amountWantBorrowMore - số tiền muốn vay thêm) - (số tiền trả lãi - priceMuchPayInterest);
                                 * 
                                 * amountWantBorrowMore: là số tiền muốn vay thêm
                                 *      SỐ TIỀN VAY      (loanAmount)                = 1,000,000 
                                 *      SỐ TIỀN VAY THÊM (totalAmountWantBorrowMore) = 1,000,000 
                                 *            => SỐ TIỀN VAY THÊM MỚI  = loanAmount + totalAmountWantBorrowMore = 2,000,000;
                                 * 
                                 * ==========================================================================
                                 * ==========================================================================
                                 * ==========================================================================
                                 * 
                                */

                                /**
                                 * ============================OBJ THÊM QUỸ============================
                                 */ 
                                
                                objFinancial = {
                                    title:  title, 
                                    type:   TYPE_FINANCIAL_EXPEND,
                                    typeTransaction: TYPE_BORROW,
                                    agency: agencyID, 
                                    price:  infoInjectTransaction.meta.totalAmountWantBorrowMore, 
                                    funds, 
                                    transaction: infoInjectTransaction._id,
                                    onModel: "inject_transaction",
                                    statusFinancial: infoInjectTransaction.meta.statusFinancial
                                }
                                console.log({
                                    objFinancial
                                });
                                 /**
                                 * ============================TÍNH LẠI SỐ NGÀY TRẢ LÃI============================
                                 * EXAMPLE:
                                 *                      (15/8)
                                *                      (Vay thêm)     (Chốt lãi kì này)     (Ngày chốt lãi chu kì tiếp theo)
                                 *      |------------------|------------------|----------------------------| 
                                 *      |                  |                  |(Ngày BĐ Tính Lãi)          |
                                 *      |-------------|----|------------------|-----|-------|--------------|
                                 *     1/8            |    |               31/8    1/9      |            (30/9)  
                                 *                    |    |                  |     |       |              |
                                 * NGÀY SAU KHI RESET |    |                  |     |       |              |
                                 *                    |---<----<----<------<--|     |       |---<---<----<-|
                                 *                 (14/8)  |-----<-----<------<-----|     (14/9)
                                 *                    | (Reset Ngày BD Tính lãi)    (Reset Ngày chốt lãi chu kì tiếp the0)
                                 *                    |     
                                 *            (Reset Ngày chốt lãi)
                                 */ 

                                let dateNow = new Date();
                                //=======================TRỪ 1 NGÀY HIỆN TẠI => NGÀY CHỐT LÃI KÌ NÀY ========================
                                let expireTime   = minusNumberToSetDate({date1: new Date(dateNow), numberToSet: BACK_ONE_DAY_EXPIRE_TIME});
                                let dateAfterPay = subtractDateHaveExpireTime(new Date(expireTime));
                                /**
                                 * ============================OBJ UPDATE GIAO DỊCH CHÍNH============================
                                 * EXAMPLE:
                                 *      SỐ TIỀN VAY      (loanAmount)                = 1,000,000 
                                 *      SỐ TIỀN VAY THÊM (totalAmountWantBorrowMore) = 1,000,000 
                                 *      => SỐ TIỀN VAY THÊM MỚI  = loanAmount + totalAmountWantBorrowMore = 2,000,000;
                                 */
                                let newLoanAmount = loanAmount + infoInjectTransaction.meta.amountWantBorrowMore

                                objUpdateTransactionMain = {
                                    interestStartDateNew:    dateAfterPay.interestStartDate,
                                    expireTimeNew:           dateAfterPay.expireTime,
                                    transactionDateNew:      new Date(),
                                    expireTimeAfterPeriod:   dateAfterPay.expireTimeAfterPeriod,
                                    transactionID,
                                    loanAmount: newLoanAmount
                                }
                                
                                /**
                                 * ----------------------OBJ THÊM HISTORY GIAO DỊCH----------------------
                                 */
                               
                                objHistory = {
                                    title: title,
                                    type: TYPE_BORROW,
                                    employee: userID,
                                    interestRateTotal: infoInjectTransaction.meta.priceMuchPayInterest,
                                    interestStartDate: infoTransaction.data.interestStartDate,
                                    resetDate: new Date(),
                                    transaction: infoInjectTransaction._id,
                                    interstAmountDate: infoInjectTransaction.meta.numberDatePayInterest,
                                    interestPercent: infoInjectTransaction.meta.percentDatePayLate,
                                    transactionMain: transactionID
                                }
                              
                                let infoAfteUpdate = await TRANSACTION_MODEL.updateInjectTransaction({  
                                    objUpdateTransactionMain: objUpdateTransactionMain, 
                                    objHistory:               objHistory, 
                                    objFinancial:             objFinancial, 
                                    injectTransID,
                                    title:                    title,
                                    type:                     TYPE_BORROW
                                });
                                if (infoAfteUpdate.error) {
                                    return res.json(infoAfteUpdate);
                                }
                                 /**
                                 * =======================KIỂM TRA XEM GIAO DỊCH CÓ ĐANG TRẢ TRƯỚC ==========================
                                 */
                                let infoTransactionAfterChange  = await TRANSACTION_MODEL.checkInterestDateTransaction({ agencyID, transactionID, TYPE_PRODUCT, loanAmount: loanAmount });
                            }
                            let infoInjectAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(injectTransID, {
                                ...objInjectUpdate
                            }, {
                                new: true
                            });
                            console.log({
                                objInjectUpdate
                            });
                            if (!infoInjectAfterUpdate) {
                                return res.json({ error: true, message: "cannot_update" });
                            }
                            res.json({ error: false, data: infoInjectAfterUpdate });

                        } else {
                            res.json({ error: true, message: "transaction_ransomed_or_in_process" });
                        }
                    }]
                },
            },

            /**
             * Function: TẠO GIAO DỊCH TRẢ BỚT
             * Dev: SONLP
             */
            [CF_ROUTINGS_TRANSACTION.ADD_INJECT_TRANSACTION_PAY_BACK]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title, type, note, meta, statusFinancial, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages  } = req.body;
                         /**
                         * ==========================================================================
                         * =========================LẤY THÔNG TIN GIAO DỊCH==========================
                         * ==========================================================================
                        */
                        let infoTransaction       = await TRANSACTION_MODEL.getInfo({ transactionID });
                        
                        /**
                         * ==========================================================================
                         * ============================CONSTANTS/VARIABLE============================
                         * ==========================================================================
                        */
                        const LOAI_QUY_TIEN_MAT       = 1;
                        const LOAI_QUY_TIEN_NGAN_HANG = 2;
                        const STATUS_ACTIVE           = 1;
                        const STATUS_COMPLETE         = 3;    // CHECK GIAO DỊCH HOÀN THÀNH
                        const LOAN_AMOUNT_PAIED       = 0;  // CHECK GIAO DỊCH HOÀN THÀNH
                        const STATUS_IN_PROCESS       = 3;

                        // let funds;
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                        // let objectConditionForFindFund = {
                        //     agency: agencyID,
                        // };
                        /**
                         * ==========================================================================
                         * ============================LẤY LAST RECORD QUỸ===========================
                         * ==========================================================================
                        */
                        // objectConditionForFindFund = statusFinancial == LOAI_QUY_TIEN_MAT ?
                        //     { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                        //     { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                        // let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                        
                        // if (!latestRecordFinicalOfAgency) {
                        //     return res.json({ error: true, message: "", description: 'vui lòng tạo giao dịch nạp tiền'});
                        // } else {
                        //     funds = latestRecordFinicalOfAgency.fundsAfterChange;
                        // }
                        /**
                         * ==========================================================================
                         * ===========================START VALIDATION===============================
                         * ==========================================================================
                         */
                        if(infoTransaction.loanAmount == LOAN_AMOUNT_PAIED || infoTransaction.data.status == STATUS_COMPLETE) {
                            return res.json({ error: true, message: "transaction_complete"});
                        }
                       
                        //===============================CHECK CÓ TẠO GIAO DỊCH CHUỘC ĐỒ HAY CHƯA===============================
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.type == 6 && infoTransaction.data.latestInjectTransaction.ransom == 2) {
                            return res.json({ error: true, message: "transaction_ransom_existed"});
                        }

                        //===============================CHECK CHỌN LOẠI QUỸ===============================
                        if (!statusFinancial) {
                            return res.json({ error: true, message: "statusFinancial_invalid"})
                        }
                        
                        //===============================CHECK CÓ TẠO GIAO DỊCH CẦN DUYỆT HAY KHÔNG===============================
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.status == STATUS_IN_PROCESS) {
                            return res.json({ error: true, message: "transaction_have_check_existed"});
                        }
                        
                        //===============================CHECK GIÁ TIỀN MUỐN TRẢ BỚT===============================
                        if (!meta.amountWantPayback || Number.isNaN(Number(meta.amountWantPayback))) {
                            return res.json({ error: true, message: "amountWantPayback_invalid"});
                        }

                        //===============================CHECK GIÁ TIỀN TRẢ BỚT TỐI THIỂU===============================
                        if (!meta.paybackMin || Number.isNaN(Number(meta.paybackMin))) {
                            return res.json({ error: true, message: "paybackMin_invalid"});
                        }
                         //===============================CHECK GIÁ TIỀN TRẢ BỚT TỐI THIỂU > GIÁ TIỀN MUỐN TRẢ BỚT ===============================
                         if (Number(meta.paybackMin) > Number(meta.amountWantPayback)) {
                            return res.json({ error: true, message: "paybackMin_cannot_>_amountWantPayback_invalid"});
                        }
                         /**
                         * ==========================================================================
                         * ===============================END VALIDATION=============================
                         * ==========================================================================
                         */

                        /**
                         * ============================KIỂM TRA GIAO DỊCH CÒN HOẠT ĐỘNG============================
                         */
                        if(infoTransaction.data.status == STATUS_ACTIVE) {
                            /**
                             * ======================================================================
                             * ==========================CONSTANTS/VARIABLE==========================
                             * ======================================================================
                             */
                            infoTransaction       = infoTransaction.data;
                            let interestStartDate = infoTransaction.interestStartDate;
                            let dateNow           =  new Date();
                            const DATE_OF_PERIOD            = 30;
                            const TYPE_PRODUCT              = infoTransaction.products[0].type._id;
                            let dataUpdate                  = {}
                            const TYPE_PAY_BACK             = 4;
 
                            /**
                             * ======================================================================
                             * ==========================CALCULATE INTEREST==========================
                             * ======================================================================
                             */

                            /**
                             *  ====================TÍNH SỐ NGÀY TRẢ LÃI CHO ĐẾN HIỆN TẠI=========================
                             */
                            let dataNow2 = new Date()
                            interestStartDate     = moment(interestStartDate).startOf('day').format();
                            let numberDateBetweenDate = subStractDateGetDay({date1: dataNow2, date2: interestStartDate });
                            if (numberDateBetweenDate < 0) {
                                numberDateBetweenDate = 0;
                            } else {
                                numberDateBetweenDate = Math.floor(numberDateBetweenDate) + 1;
                            }
                            meta.numberDatePayInterest = numberDateBetweenDate;
                            
                            /**
                             *  ====================TÍNH PHẦN TRĂM LÃI VÀ SỐ TIỀN LÃI CỦA GIAO DỊCH=========================
                             */
                            let { data: infoInterest } = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: numberDateBetweenDate, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            meta.priceMuchPayInterest         = Math.floor(infoInterest.loanAmoutAfterInterest);
                            meta.priceMuchPayInterestOriginal = Math.floor(infoInterest.loanAmoutAfterInterest);

                            meta.percentDatePayLate    = infoInterest.percetTotal;
                            
                            /**
                             * ==========================SỐ TIỀN ĐÃ TRẢ TRƯỚC (PAY INTEREST)================================
                             */
                            let { data: pricePayBefore } = await TRANSACTION_MODEL.getPricePayInterest({ agencyID, transactionID, TYPE_PRODUCT, loanAmount: infoTransaction.loanAmount });
                            meta.pricePayInterest = pricePayBefore;
                            
                            /**
                             * ==============SET LẠI TỔNG TIỀN TRẢ BỚT====================
                             * EXAMPLE: 
                             *      SÔ TIỀN TRẢ BỚT: 1,000,000 (amountWantPayback)
                             *      SÔ TRẢ LÃI:      1,000,000 (priceMuchPayInterest)
                             *      TỔNG TIỀN TRẢ:   2,000,000 (totalAmountWantPayBack)
                             */
                            meta.totalAmountWantPayBack = Number(meta.amountWantPayback) + meta.priceMuchPayInterest;
                            meta = {
                                ...meta,
                                statusFinancial,
                                loanAmount: infoTransaction.loanAmount,
                                statusFinancial,
                                loanAmountAfterChange: Number(infoTransaction.loanAmount) - Number(meta.amountWantPayback), // Số tiền vay sau khi xảy ra gd
                            }
                            console.log({
                                meta
                            });
                            /**
                             * ==========================KIỂM TRA GIÁ KHUYẾN MÃI CÓ TỒN TẠI VÀ < SỐ TIỀN LÃI==========================
                             */
                             if (meta.promotionInterest && meta.promotionInterest > meta.priceMuchPayInterest) {
                                return resolve({ error: true, message: "promotionInterest_invalid" });
                            }else if (meta.promotionInterest) {
                                meta.totalAmountWantPayBack = meta.totalAmountWantPayBack - Number(meta.promotionInterest)
                            }
                            /**
                             * ======================================================================
                             * ==============================LƯU HÌNH ẢNH============================
                             * ======================================================================
                             */
                             if(listUrlCustomerImages && listUrlCustomerImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlCustomerImages, userCreate: userID });
                                 dataUpdate.customerImages    = listIDImage;
                             }
                             if(listUrlKYCImages && listUrlKYCImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlKYCImages, userCreate: userID });
                                 dataUpdate.KYCImages         = listIDImage;
                             }
                             if(listUrlReceiptImages && listUrlReceiptImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                 dataUpdate.receiptImages     = listIDImage;
                             }
                             if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                 dataUpdate.formVerificationImages = listIDImage;
                             }
                             /**
                              * ======================================================================
                              * ==============================THÊM RECORD=============================
                              * ======================================================================
                              */
                            let obj = {
                                title: "GIAO DỊCH TRẢ BỚT", type: TYPE_PAY_BACK, transaction: transactionID, 
                                note, meta, agencyID, status: STATUS_IN_PROCESS, userCreate: userID

                            };
                             
                            let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                                ...obj, dataUpdate
                            });
                             
                            if(infoAfterInsert.error) {
                                return res.json(infoAfterInsert);
                            }
                            return res.json(infoAfterInsert)
                        } else {
                            res.json({ error: true, message: "cannot_insert_inject_transaction" });
                        }
                    }]
                },
            },

            /**
             * Function: DUYỆT GIAO DỊCH TRẢ BỚT
             * Dev: SONLP
             */
            [CF_ROUTINGS_TRANSACTION.UPDATE_INJECT_TRANSACTION_PAY_BACK]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let { transactionID } = req.params;
                        let { noteNotApproved, statusFinancial, injectTransID, status } = req.body
                        let agencyID  = req.agencyID;
                         /**
                         * ==========================================================================
                         * =========================LẤY THÔNG TIN GIAO DỊCH==========================
                         * ==========================================================================
                        */
                        let infoTransaction       = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoInjectTransaction = await INJECT_TRANSACTION_COLL.findById(injectTransID);
                        
                        /**
                         * ==========================================================================
                         * ============================CONSTANTS/VARIABLE============================
                         * ==========================================================================
                        */
                        const LOAI_QUY_TIEN_MAT       = 1;
                        const LOAI_QUY_TIEN_NGAN_HANG = 2;
                        const STATUS_ACTIVE = 1;
                        let funds;
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                        let objectConditionForFindFund = {
                            agency: agencyID,
                        };
                        /**
                         * ==========================================================================
                         * ============================LẤY LAST RECORD QUỸ===========================
                         * ==========================================================================
                        */
                        objectConditionForFindFund = infoInjectTransaction.meta.statusFinancial == LOAI_QUY_TIEN_MAT ?
                            { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                            { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                        let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                        
                        if (!latestRecordFinicalOfAgency) {
                            return res.json({ error: true, message: "", description: 'vui lòng tạo giao dịch nạp tiền'});
                        } else {
                            funds = latestRecordFinicalOfAgency.fundsAfterChange;
                        }
                       
                        /**
                         * ============================KIỂM TRA GIAO DỊCH CÒN HOẠT ĐỘNG============================
                         */
                        if(infoTransaction.data.status == STATUS_ACTIVE) {
                             /**
                             * ==========================================================================
                             * ============================CONSTANTS/VARIABLE============================
                             * ==========================================================================
                            */
                            const STATUS_INJECT_ACCEPT   = 1;
                            const TYPE_PAY_BACK          = 4;
                            const STATUS_INJECT_IN_ACCEPT= 2;
                            const TYPE_FINANCIAL_COLLECT = 1;
                            let title                    = `${infoInjectTransaction.code} + ${infoTransaction.data.customer.name} + GIAO DỊCH TRẢ BỚT + ${infoInjectTransaction.meta.totalAmountWantPayBack}`;
                            let objInjectUpdate          = {};   
                            let objFinancial             = {};
                            let objUpdateTransactionMain = {};
                            let objHistory               = {};
                            let payInterest              = infoTransaction.data.payInterest; 
                            let loanAmount               = infoTransaction.data.loanAmount; 
                            const TYPE_PRODUCT           = infoTransaction.data.products[0].type._id;


                            /**
                             * ==========================================================================
                             * =============================KIỂM TRA STATUS==============================
                             * ==========================================================================
                             * 
                             */
                            if (status == STATUS_INJECT_IN_ACCEPT) {
                                objInjectUpdate = {
                                    noteNotApproved: noteNotApproved, status, userConfirm: userID
                                }
                            }

                            if (status == STATUS_INJECT_ACCEPT) {
                                objInjectUpdate = {
                                    status, transactionDate: new Date(), userConfirm: userID
                                }
                                 /**
                                 * ==========================================================================
                                 * ==================================GHI CHÚ=================================
                                 * ==========================================================================
                                 * 
                                 * totalAmountWantPayBack: là số tiền được trả bớt => (SỐ TIỀN CỘNG VÀO QUỸ)
                                 *  => totalAmountWantPayBack = (amountWantPayback - số tiền muốn trả bớt) + (số tiền trả lãi - priceMuchPayInterest);
                                 * 
                                 * amountWantBorrowMore: là số tiền muốn vay thêm
                                 *      SỐ TIỀN VAY     (loanAmount)                = 1,000,000 
                                 *      SỐ TIỀN TRẢ BỚT (amountWantPayback)         = 500,000 
                                 *            => SỐ TIỀN VAY MỚI = loanAmount - amountWantPayback = 500,000;
                                 * 
                                 * ==========================================================================
                                 * ==========================================================================
                                 * ==========================================================================
                                 * 
                                */

                                /**
                                 * ============================OBJ THÊM QUỸ============================
                                 */ 
                                objFinancial = {
                                    title:  title, 
                                    type:   TYPE_FINANCIAL_COLLECT,
                                    typeTransaction: TYPE_PAY_BACK,
                                    agency: agencyID, 
                                    price:  infoInjectTransaction.meta.totalAmountWantPayBack, 
                                    funds, 
                                    transaction: infoInjectTransaction._id,
                                    onModel: "inject_transaction",
                                    statusFinancial: infoInjectTransaction.meta.statusFinancial
                                }
                                 /**
                                 * ============================TÍNH LẠI SỐ NGÀY TRẢ LÃI============================
                                 * EXAMPLE:
                                 *                      (15/8)
                                *                      (Trả bớt)     (Chốt lãi kì này)     (Ngày chốt lãi chu kì tiếp theo)
                                 *      |------------------|------------------|----------------------------| 
                                 *      |                  |                  |(Ngày BĐ Tính Lãi)          |
                                 *      |------------------|------------------|-----|-------|--------------|
                                 *     1/8                 |                31/8   1/9      |            (30/9)  
                                 *                         |                  |             |              |
                                 * NGÀY SAU KHI RESET      |    (16/8)        |             |              |
                                 *                         |---<--|-<------<--|             |---<---<----<-|
                                 *                         |      |-----<-----|           (14/9)
                                 *                         |  (Reset Ngày            (Reset Ngày chốt lãi chu kì tiếp the0)
                                 *                         |  BD Tính lãi)
                                 *            (Reset Ngày chốt lãi)
                                 */ 

                                //=======================NGÀY HIỆN TẠI => NGÀY CHỐT LÃI KÌ NÀY ========================
                                let dateAfterPay = subtractDateHaveExpireTime(new Date());

                               
                                /**
                                 * ============================OBJ UPDATE GIAO DỊCH CHÍNH============================
                                 * EXAMPLE:
                                 *      SỐ TIỀN VAY      (loanAmount)                = 1,000,000 
                                 *      SỐ TIỀN VAY THÊM (totalAmountWantBorrowMore) = 1,000,000 
                                 *      => SỐ TIỀN VAY THÊM MỚI  = loanAmount + totalAmountWantBorrowMore = 2,000,000;
                                 */
                                let newLoanAmount = loanAmount - infoInjectTransaction.meta.amountWantPayback

                                objUpdateTransactionMain = {
                                    interestStartDateNew:    dateAfterPay.interestStartDate,
                                    expireTimeNew:           dateAfterPay.expireTime,
                                    transactionDateNew:      new Date(),
                                    expireTimeAfterPeriod:   dateAfterPay.expireTimeAfterPeriod,
                                    transactionID,
                                    loanAmount: newLoanAmount
                                }
                               
                                /**
                                 * ----------------------OBJ THÊM HISTORY GIAO DỊCH----------------------
                                 */
                               
                                objHistory = {
                                    title: title,
                                    type: TYPE_PAY_BACK,
                                    employee: userID,
                                    interestRateTotal: infoInjectTransaction.meta.priceMuchPayInterest,
                                    interestStartDate: infoTransaction.data.interestStartDate,
                                    resetDate: new Date(),
                                    transaction: infoInjectTransaction._id,
                                    interstAmountDate: infoInjectTransaction.meta.numberDatePayInterest,
                                    interestPercent: infoInjectTransaction.meta.percentDatePayLate,
                                    transactionMain: transactionID
                                }
                               
                                let infoAfteUpdate = await TRANSACTION_MODEL.updateInjectTransaction({  
                                    objUpdateTransactionMain: objUpdateTransactionMain, 
                                    objHistory:               objHistory, 
                                    objFinancial:             objFinancial, 
                                    injectTransID,
                                    title:                    title,
                                    type:                     TYPE_PAY_BACK
                                });
                                if (infoAfteUpdate.error) {
                                    return res.json(infoAfteUpdate);
                                }
                                 /**
                                 * =======================KIỂM TRA XEM GIAO DỊCH CÓ ĐANG TRẢ TRƯỚC ==========================
                                 */
                                let infoTransactionAfterChange  = await TRANSACTION_MODEL.checkInterestDateTransaction({ agencyID, transactionID, TYPE_PRODUCT, loanAmount: loanAmount });
                                console.log({
                                    infoTransactionAfterChange
                                });
                            }
                            let infoInjectAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(injectTransID, {
                                ...objInjectUpdate
                            }, {
                                new: true
                            });
                            console.log({
                                objInjectUpdate
                            });
                            if (!infoInjectAfterUpdate) {
                                return res.json({ error: true, message: "cannot_update" });
                            }
                            res.json({ error: false, data: infoInjectAfterUpdate });

                        } else {
                            res.json({ error: true, message: "transaction_ransomed_or_in_process" });
                        }
                    }]
                },
            },

            [CF_ROUTINGS_TRANSACTION.ADD_INJECT_TRANSACTION_RANSOM]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title, type, note, meta, statusFinancial, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages  } = req.body;
                         /**
                         * ==========================================================================
                         * =========================LẤY THÔNG TIN GIAO DỊCH==========================
                         * ==========================================================================
                        */
                        let infoTransaction       = await TRANSACTION_MODEL.getInfo({ transactionID });
                        
                        /**
                         * ==========================================================================
                         * ============================CONSTANTS/VARIABLE============================
                         * ==========================================================================
                        */
                        const STATUS_ACTIVE           = 1;
                        const STATUS_COMPLETE         = 3;    // CHECK GIAO DỊCH HOÀN THÀNH
                        const LOAN_AMOUNT_PAIED       = 0;  // CHECK GIAO DỊCH HOÀN THÀNH
                        const STATUS_IN_PROCESS       = 3;
                        
                        /**
                         * ==========================================================================
                         * ===========================START VALIDATION===============================
                         * ==========================================================================
                         */
                        if(infoTransaction.loanAmount == LOAN_AMOUNT_PAIED || infoTransaction.data.status == STATUS_COMPLETE) {
                            return res.json({ error: true, message: "transaction_complete"});
                        }
                       
                        //===============================CHECK CÓ TẠO GIAO DỊCH CHUỘC ĐỒ HAY CHƯA===============================
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.type == 6 && infoTransaction.data.latestInjectTransaction.ransom == 2) {
                            return res.json({ error: true, message: "transaction_ransom_existed"});
                        }

                        //===============================CHECK CHỌN LOẠI QUỸ===============================
                        if (!statusFinancial) {
                            return res.json({ error: true, message: "statusFinancial_invalid"})
                        }
                        
                        //===============================CHECK CÓ TẠO GIAO DỊCH CẦN DUYỆT HAY KHÔNG===============================
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.status == STATUS_IN_PROCESS) {
                            return res.json({ error: true, message: "transaction_have_check_existed"});
                        }
                         /**
                         * ==========================================================================
                         * ===============================END VALIDATION=============================
                         * ==========================================================================
                         */

                        /**
                         * ============================KIỂM TRA GIAO DỊCH CÒN HOẠT ĐỘNG============================
                         */
                        if(infoTransaction.data.status == STATUS_ACTIVE) {
                            /**
                             * ======================================================================
                             * ==========================CONSTANTS/VARIABLE==========================
                             * ======================================================================
                             */
                            infoTransaction       = infoTransaction.data;
                            let interestStartDate = infoTransaction.interestStartDate;
                            const TYPE_PRODUCT    = infoTransaction.products[0].type._id;
                            let payInterest       = infoTransaction.payInterest;
                            let loanAmount        = infoTransaction.loanAmount;
                            let dataUpdate        = {}
                            const TYPE_RANSOM     = 6;
                            const TYPE_INACTIVE_RANSOM = 2;

                            if (!meta) {
                                meta                  = {}
                            }

                            meta = {
                                ...meta,
                                statusFinancial,
                                loanAmount: infoTransaction.loanAmount,
                                loanAmountAfterChange: 0
                            }

                            /**
                             * ======================================================================
                             * ==========================CALCULATE INTEREST==========================
                             * ======================================================================
                             */

                            /**
                             *  ====================TÍNH SỐ NGÀY TRẢ LÃI CHO ĐẾN HIỆN TẠI=========================
                             */
                            let dataNow2 = new Date()
                            interestStartDate     = moment(interestStartDate).startOf('day').format();

                            let numberDateBetweenDate = subStractDateGetDay({date1: dataNow2, date2: interestStartDate });
                            
                            if (numberDateBetweenDate < 0) {
                                numberDateBetweenDate = 0;
                            } else {
                                numberDateBetweenDate = Math.floor(numberDateBetweenDate) + 1;
                            }
                           
                            meta.numberDatePayInterest = numberDateBetweenDate;
                            /**
                             *  ====================TÍNH PHẦN TRĂM LÃI VÀ SỐ TIỀN LÃI CỦA GIAO DỊCH=========================
                             */
                            let { data: infoInterest } = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: numberDateBetweenDate, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT });
                            meta.priceMuchPayInterest  = Math.floor(infoInterest.loanAmoutAfterInterest);
                            meta.priceMuchPayInterestOriginal = Math.floor(infoInterest.loanAmoutAfterInterest);

                            meta.percentDatePayLate    = infoInterest.percetTotal;
                            
                            
                            /**
                             * ==========================SỐ TIỀN ĐÃ TRẢ TRƯỚC (PAY INTEREST)================================
                             */
                            let { data: pricePayBefore } = await TRANSACTION_MODEL.getPricePayInterest({ agencyID, transactionID, TYPE_PRODUCT, loanAmount: infoTransaction.loanAmount });
                            meta.pricePayInterest = pricePayBefore + payInterest;
                            
                            /**
                             * ==============SET LẠI TỔNG TIỀN CHUỘC====================
                             * EXAMPLE: 
                             *      SÔ TIỀN VAY:      1,000,000 (loanAmount)
                             *      SÔ TRẢ LÃI:     + 500,000   (priceMuchPayInterest)
                             *      SÔ TIỀN CÒN DƯ: - 200,000   (meta.pricePayInterest)
                             *      TỔNG TIỀN TRẢ:  = 1,300,000 (priceRansomPay)
                             */
                            meta.priceRansomPay = Number(loanAmount) + meta.priceMuchPayInterest - meta.pricePayInterest;
                            meta.priceRansomPayOriginal = Number(loanAmount);

                            /**
                             * ==========================KIỂM TRA GIÁ KHUYẾN MÃI CÓ TỒN TẠI VÀ < SỐ TIỀN LÃI==========================
                             */
                             if (meta.promotionInterest && meta.promotionInterest > meta.priceMuchPayInterest) {
                                return resolve({ error: true, message: "promotionInterest_invalid" });
                            }else if (meta.promotionInterest) {
                                meta.priceRansomPay = meta.priceRansomPay - Number(meta.promotionInterest)
                            }
                            console.log({
                                meta
                            });
                            /**
                             * ======================================================================
                             * ==============================LƯU HÌNH ẢNH============================
                             * ======================================================================
                             */
                             if(listUrlCustomerImages && listUrlCustomerImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlCustomerImages, userCreate: userID });
                                 dataUpdate.customerImages    = listIDImage;
                             }
                             if(listUrlKYCImages && listUrlKYCImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlKYCImages, userCreate: userID });
                                 dataUpdate.KYCImages         = listIDImage;
                             }
                             if(listUrlReceiptImages && listUrlReceiptImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                 dataUpdate.receiptImages     = listIDImage;
                             }
                             if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                                 let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                 dataUpdate.formVerificationImages = listIDImage;
                             }
                             /**
                              * ======================================================================
                              * ==============================THÊM RECORD=============================
                              * ======================================================================
                              */
                           
                            let objPayRansom = {
                                title: "GIAO DỊCH CHUỘC ĐỒ", type: TYPE_RANSOM, transaction: transactionID, meta, 
                                agencyID, ransom: TYPE_INACTIVE_RANSOM, userCreate: userID
                            };
                            let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                                ...objPayRansom, dataUpdate
                            });
                             
                            if(infoAfterInsert.error) {
                                return res.json(infoAfterInsert);
                            }
                            return res.json(infoAfterInsert)
                        } else {
                            res.json({ error: true, message: "cannot_insert_inject_transaction" });
                        }
                    }]
                },
            },

            /** 
             * API Confirm chuộc đồ
             */
            [CF_ROUTINGS_TRANSACTION.CONFIRM_RANSOM]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let { transactionID } = req.params;
                        let { noteNotApproved, statusFinancial, injectTransID, interestID, limitDate, loanAmount, status } = req.body

                        let agencyID  = req.agencyID;
                        /**
                         * ==========================================================================
                         * =========================LẤY THÔNG TIN GIAO DỊCH==========================
                         * ==========================================================================
                        */
                        let infoTransaction       = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoInjectTransaction = await INJECT_TRANSACTION_COLL.findById(injectTransID);
                        if (!infoInjectTransaction) {
                            return res.json({ error: true, message: 'id_invalid' });
                        }
                         /**
                         * ======================================================================
                         * ==========================CONSTANTS/VARIABLE==========================
                         * ======================================================================
                         */
                        let ransom = status;
                        

                        if (infoInjectTransaction.meta.priceRansomPay < 0) {
                            let parsePriceRansomPay = Math.abs(infoInjectTransaction.meta.priceRansomPay);
                            if (parsePriceRansomPay > funds) {
                                return res.json({ error: true, message: "funds_not_enough"}) 
                            }
                        }

                        // 3//Kiểm tra duyệt chuộc đồ hay kh
                        if(infoTransaction.data.status == 1) {
                            if(ransom == 1){
                                 /**
                                 * ======================================================================
                                 * ==========================CONSTANTS/VARIABLE==========================
                                 * ======================================================================
                                 */
                                let title = `${infoInjectTransaction.code} + ${infoTransaction.data.customer.name} + GIAO DỊCH CHUỘC ĐỒ + ${infoInjectTransaction.meta.priceRansomPay}`;
                                const TYPE_FINANCIAL_COLLECT = 1;
                                const TYPE_RANSOM            = 6;
                                let funds;

                                /**
                                 * ======================================================================
                                 * ===========================LAST RECORD QUỸ============================
                                 * ======================================================================
                                 */
                                // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                                let objectConditionForFindFund = {
                                    agency: agencyID,
                                };
                                
                                const LOAI_QUY_TIEN_MAT = 1;
                                const LOAI_QUY_TIEN_NGAN_HANG = 2;

                                objectConditionForFindFund = infoInjectTransaction.meta.statusFinancial == LOAI_QUY_TIEN_MAT ?
                                    { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                                    { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                                let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                                console.log({
                                    latestRecordFinicalOfAgency,
                                    objectConditionForFindFund
                                });
                                if (!latestRecordFinicalOfAgency) {
                                    return res.json({ error: true, message: "", description: 'vui lòng tạo giao dịch nạp tiền'});
                                } else {
                                    funds = latestRecordFinicalOfAgency.fundsAfterChange;
                                }
                                // 7//Thêm quỹ
                                let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({
                                    title:  title, 
                                    type:   TYPE_FINANCIAL_COLLECT,
                                    typeTransaction: TYPE_RANSOM,
                                    agency: agencyID, 
                                    price:  infoInjectTransaction.meta.priceRansomPay, 
                                    funds, 
                                    transaction: injectTransID,
                                    onModel: "inject_transaction",
                                    statusFinancial: infoInjectTransaction.meta.statusFinancial
                                });

                                if(infoFinancialAfterInsert.error){
                                    return res.json({ error: true, message: 'price_invalid'})
                                }
                                // 7//Update lại số tiền vay của Transaction
                                let transactionAfterUpdate = await TRANSACTION_MODEL.updateLoanAmountRansom({ transactionID });
                                // 8//Lưu vào history
                                let infoHistoryTransactionAfterInsert = await HISTORY_TRANSACTION_MODEL.insert({
                                    title: title,
                                    type: TYPE_RANSOM,
                                    employee: userID,
                                    interestRateTotal: infoInjectTransaction.meta.priceMuchPayInterest,
                                    interestStartDate: infoTransaction.data.interestStartDate,
                                    resetDate: new Date(),
                                    transaction: transactionID,
                                    interstAmountDate: infoInjectTransaction.meta.numberDatePayInterest,
                                    interestPercent: infoInjectTransaction.meta.percentDatePayLate
                                });
                            }
                            // Update lại Inject Transaction 
                            let injectTransactionAfterUpdate = await INJECT_TRANSACTION_MODEL.update({ 
                                transactionID: injectTransID, ransom: Number(ransom), 
                                noteNotApproved, transactionDate: new Date(), userConfirm: userID
                            });
                        
                            res.json(
                                { 
                                    injectTransactionAfterUpdate
                                }
                            );
                        } else {
                            res.json({ error: true, message: "transaction_ransomed" });
                        }
                    }]
                },
            },
            
            /**
             * Function: TẠO GIAO DỊCH BÁO MẤT
             * Dev: SONLP
             */
             [CF_ROUTINGS_TRANSACTION.ADD_INJECT_TRANSACTION_LOST]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title, type, note, meta, statusFinancial, listUrlCustomerImages, listUrlKYCImages, listUrlReceiptImages, listUrlformVerificationImages  } = req.body;
                        
                        let infoTransaction  = await TRANSACTION_MODEL.getInfo({ transactionID });
                        let infoDataAgency   = await AGENCY_MODEL.getInfo({ agencyID });

                        if(infoTransaction.error) {
                            return res.json(infoData);
                        }
                        /**
                         * GIAO DỊCH HOÀN THÀNH
                         */
                        const STATUS_COMPLETE = 3;
                        const LOAN_AMOUNT_PAIED = 0;
                        if(infoTransaction.loanAmount == LOAN_AMOUNT_PAIED || infoTransaction.data.status == STATUS_COMPLETE) {
                            return res.json({ error: true, message: "transaction_complete"});
                        }
                        /**
                         * CHECK CÓ TẠO GIAO DỊCH CHUỘC ĐỒ HAY CHƯA
                         */
                        if (infoTransaction.data.latestInjectTransaction && infoTransaction.data.latestInjectTransaction.type == 6 && infoTransaction.data.latestInjectTransaction.ransom == 2) {
                            return res.json({ error: true, message: "transaction_ransom_existed"});
                        }
                        if(infoDataAgency.error) {
                            return res.json(infoDataAgency);
                        }
                        if (!statusFinancial) {
                            return res.json({ error: true, message: "statusFinancial_invalid"})
                        }

                        if (infoTransaction.data.status == 1) {
                            /**
                             * SỐ TIỀN VAY (loanAmount)
                             * ID DANH MỤC (TYPE_PRODUCT)
                             */
                            infoTransaction     = infoTransaction.data;
                            infoDataAgency      = infoDataAgency.data;
                            const TYPE_LOST     = 5;
                            /**
                             * TYPE = 1 (THU TIỀN)
                             */
                            const TYPE           = 1;
                            let typeFinancial    = TYPE;
                            
                            if (!Number(meta.reissueFee) ) {
                                return res.json({ error: true, message: "reissueFee_invalid" });
                            }

                            if (!Number(meta.requestPrintTh) ) {
                                return res.json({ error: true, message: "requestPrintTh_invalid" });
                            }
                            meta = {
                                ...meta,
                                loanAmount: infoTransaction.loanAmount,
                                statusFinancial,
                                loanAmountAfterChange: infoTransaction.loanAmount,
                            }
                            /**
                                LẤY QUỸ CỦA AGENCY
                             */
                            // let funds = getFundsAgency({ statusFinancial, infoDataAgency });
                            let funds;
                            // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                            let objectConditionForFindFund = {
                                agency: agencyID,
                            };
                           
                            const LOAI_QUY_TIEN_MAT = 1;
                            const LOAI_QUY_TIEN_NGAN_HANG = 2;

                            objectConditionForFindFund = statusFinancial == LOAI_QUY_TIEN_MAT ?
                                { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                                { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };

                            let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                            
                            if (!latestRecordFinicalOfAgency) {
                                return res.json({ error: true, message: "funds_not_enough", description: 'vui lòng tạo giao dịch nạp tiền'});
                            } else {
                                funds = latestRecordFinicalOfAgency.fundsAfterChange;
                            }
                            /**
                                 5// LƯU HÌNH ẢNH => DANH SÁCH ID IMAGE
                             */
                            let dataUpdate = {}
                            if(listUrlCustomerImages && listUrlCustomerImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlCustomerImages, userCreate: userID });
                                dataUpdate.customerImages    = listIDImage;
                            }
                            if(listUrlKYCImages && listUrlKYCImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlKYCImages, userCreate: userID });
                                dataUpdate.KYCImages         = listIDImage;
                            }
                            if(listUrlReceiptImages && listUrlReceiptImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                dataUpdate.receiptImages     = listIDImage;
                            }
                            if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                                let { data: listIDImage }    = await IMAGE_MODEL.insertArr({ arrImage: listUrlReceiptImages, userCreate: userID });
                                dataUpdate.formVerificationImages = listIDImage;
                            }
                            
                            let objLost = {
                                title: "GIAO DỊCH BÁO MẤT", type: TYPE_LOST, transaction: transactionID, meta, agencyID, userCreate: userID
                            };
                            let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                                ...objLost, dataUpdate
                            });
                            if(infoAfterInsert.error) {
                                return res.json(infoAfterInsert);
                            }

                            // UPDATE LẠI QUỸ
                            // SET NAME THÊM VÀO QUỸ
                            let nameOfFinanical = `${infoAfterInsert.data.code} + ` + `${infoTransaction.customer.name} + ` + "GIAO DỊCH BÁO MẤT" + ` + ${meta.reissueFee}`;
                            let objFinancial = {
                                title:  nameOfFinanical, 
                                type:   typeFinancial,
                                typeTransaction: TYPE_LOST,
                                agency: agencyID, 
                                price:  Number(meta.reissueFee), 
                                funds, 
                                transaction: infoAfterInsert.data._id,
                                onModel: "inject_transaction",
                                statusFinancial
                            }
                            let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                                ...objFinancial
                            });

                            if (infoFinancialAfterInsert.error) {
                                return res.json(infoFinancialAfterInsert)
                            }
                            
                            res.json(infoAfterInsert)
                        }else{
                            res.json({ error: true, message: "cannot_insert_inject_transaction" })
                        }
                    }]
                },
            },

            /**
             * Function: tạo giao dịch bổ xung
             * Dev: Depv
             */
             [CF_ROUTINGS_TRANSACTION.UPDATE_INJECT_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID }   = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let { title,
                            type,
                            note,
                            meta,
                            statusFinancial,
                            listUrlCustomerImages,
                            listUrlKYCImages,
                            listUrlReceiptImages,
                            listUrlformVerificationImages,
                            customerImagesInjectIsDelete, 
                            KYCImagesInjectIsDelete, 
                            receiptImagesInjectIsDelete, 
                            formVerificationImagesInjectIsDelete,
                            transactionMainID } = req.body;
                        
                        // 1// UPDATE LẠI GIAO DỊCH BỔ SUNG
                        let infoData                = await TRANSACTION_MODEL.getInfo({ transactionID: transactionMainID });
                        let infoDataAgency          = await AGENCY_MODEL.getInfo({ agencyID });
                        let {data: infoInjectTran}  = await INJECT_TRANSACTION_MODEL.getInfo({ transactionID });
                        let {data: infoFinancial}   = await FINANCIAL_MODEL.getInfo({ transactionID, onModel: "inject_transaction" });
                        let infoTransaction         = infoData.data;
                        let typeFinancial;
                        // Thêm hình ảnh giao dịch
                        let customerImagesID = [];
                        let KYCImagesID = [];
                        let receiptImagesID = [];
                        let formVerificationImagesID = [];

                        // 2// Nếu hình ảnh kh có thì báo lỗi
                        // if ( !listUrlCustomerImages || !listUrlKYCImages || !listUrlformVerificationImages ){
                        //     return res.json({ error: true, message: "image_not_completely" })
                        // }
                            
                        let dataImageUpdate = { $addToSet: {} };
                        // INSERT IMAGE
                        if(listUrlCustomerImages && listUrlCustomerImages.length) {
                            for (let imgCustomer of listUrlCustomerImages){
                                let infoImageCustomerAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgCustomer,
                                    path: imgCustomer,
                                    userCreate: userID
                                });
                                customerImagesID.push(infoImageCustomerAfterInsert.data._id)
                            }
                            dataImageUpdate['$addToSet'].customerImages = customerImagesID;
                        }
                        if(listUrlKYCImages && listUrlKYCImages.length) {
                            for (let imgKYCImages of listUrlKYCImages){
                                let infoImageKYCImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgKYCImages,
                                    path: imgKYCImages,
                                    userCreate: userID
                                });
                                KYCImagesID.push(infoImageKYCImagesAfterInsert.data._id)
                            }
                            dataImageUpdate['$addToSet'].KYCImages = KYCImagesID;
                        }
                        if(listUrlReceiptImages && listUrlReceiptImages.length) {
                            for (let imgReceiptImages of listUrlReceiptImages){
                                let infoImageReceiptImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgReceiptImages,
                                    path: imgReceiptImages,
                                    userCreate: userID
                                });
                                receiptImagesID.push(infoImageReceiptImagesAfterInsert.data._id)
                            }
                            dataImageUpdate['$addToSet'].receiptImages = receiptImagesID;
                        }
                        if(listUrlformVerificationImages && listUrlformVerificationImages.length) {
                            for (let imgformVerificationImages of listUrlformVerificationImages){
                                let infoImageformVerificationImagesAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgformVerificationImages,
                                    path: imgformVerificationImages,
                                    userCreate: userID
                                });
                                formVerificationImagesID.push(infoImageformVerificationImagesAfterInsert.data._id)
                            }
                            dataImageUpdate['$addToSet'].formVerificationImages = formVerificationImagesID;
                        }
                        
                        let string = "";
                        let price = 0;
                        let loanAmt = 0;

                        // % lãi và tính tổng lãi
                        let interestStartDate = infoTransaction.interestStartDate;

                        let dateNow   =  new Date();
                        interestStartDate          = moment(interestStartDate).startOf('day').format();

                        let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                        // limitDate     = Math.floor(limitDate);
                        /**
                         * 1: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại > 0
                         *    => limitDate + 1, ngày tính lãi bắt đầu 1
                         * 2: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại < 0
                         *    => Đã trả lãi trước
                         *    => limitDate - 1, ngày tính lãi - 1
                         */
                        if(limitDate >= 0) {
                            limitDate     = Math.floor(limitDate) + 1;
                        }
                        
                        if(limitDate < 0) {
                            limitDate = 0;
                        }

                        let infoInterest;
                        if(infoTransaction.status == 3 || infoTransaction.status == 2) {
                            limitDate = 0;
                        }

                        let loanAmoutAfterInterest = 0;
                        let percent = 0;
                        //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                        // console.log({ limitDate });
                        let interestPercent = 0;
                        if (limitDate > 0 ){
                            if(!infoTransaction.products || !infoTransaction.products.length) {
                                return res.json({ error: true, message: "product_invalid" }); 
                            }
                            let TYPE_PRODUCT = infoTransaction.products[0].type._id;
                            let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate, price: infoTransaction.loanAmount, typeProduct: TYPE_PRODUCT});
                            checkDateToInterest.data.forEach( price => {
                                loanAmoutAfterInterest += price;
                            })
                            percent         = checkDateToInterest.percent;
                            interestPercent = checkDateToInterest.percent;
                            infoInterest    = checkDateToInterest.infoInterest;
                        }
                        
                        /**
                         * Kiểm tra type
                         * Nếu type != 5 thì kh tạo thêm history_transaction
                         */
                         let priceOld = 0;
                         let COLLECT = 1;
                         let SPEND   = 2;
                         if ( type == 2 ){
                             typeFinancial   = COLLECT;
                             // price = Number(meta.amountWantPayback);
 
                             if ( infoTransaction.loanAmount == 0 ){
                                 return res.json({ error: true, message: "cannot_create_payback_transaction" })
                             }
 
                             string  = string + `Gia hạn bình thường + ${ loanAmoutAfterInterest }`;
                             loanAmt = infoTransaction.loanAmount;
                             // meta   = {
                             //     interestNeedPay: loanAmoutAfterInterest
                             // }
                         }
                         
                         if ( type == 3 ){
                            string = string + `Gia hạn vay thêm + ${ meta.amountWantBorrowMore}`;
                            typeFinancial   = SPEND;
                            priceOld = infoInjectTran.meta.amountWantBorrowMore;

                            price   = Number(meta.amountWantBorrowMore);
                            let limitPrice               = infoTransaction.approvalLimit - infoTransaction.loanAmount;
                            let priceAmountWantBorrowOld = infoInjectTran.meta.amountWantBorrowMore;
                            let priceAfterChange         = Math.abs(priceAmountWantBorrowOld - price);
                        
                            if ( priceAfterChange > limitPrice ){
                                return res.json({ error: true, message: "price_borrow_more_invalid" })
                            }else{
                                loanAmt = infoTransaction.loanAmount - priceAmountWantBorrowOld + price;
                            }
                         }
 
                         if ( type == 4 ){
                             string = string + `Gia hạn trả bớt + ${ meta.amountWantPayback}`;
                             typeFinancial   = COLLECT;
                            priceOld = infoInjectTran.meta.amountWantPayback;
                             price   = Number(meta.amountWantPayback);
 
                             if ( infoTransaction.loanAmount == 0 ){
                                 return res.json({ error: true, message: "cannot_create_payback_transaction" })
                             }
                             let totalPriceNeedPay     = infoTransaction.loanAmount;
                             let priceAmountWantPayOld = infoInjectTran.meta.amountWantPayback;
                             let priceAfterChange      = Math.abs(priceAmountWantPayOld - price);
                             // console.log({ priceAfterChange });
                             if ( priceAfterChange > totalPriceNeedPay ){
                                 // loanAmt = 0;
                                 return res.json({ error: true, message: "price_payback_more_invalid" })
                             }
 
                             if ( price == totalPriceNeedPay ){
                                 loanAmt = 0;
                             }else{
                                 let priceAfter = price;
                                 loanAmt = infoTransaction.loanAmount + priceAmountWantPayOld - priceAfter;
                             }
                         }
 
                         if ( type == 5 ){
                             string = string + `Báo mất + ${ meta.reissueFee}`;
                             typeFinancial   = COLLECT;
                            priceOld = infoInjectTran.meta.reissueFee;
                             price = Number(meta.reissueFee);
                             loanAmt = null;
                         }

                         let ransom;
                         if ( type == 6 ){
                             string = string + `Chuộc đồ`;
                             typeFinancial   = 1;
                             price   = infoTransaction.loanAmount;
                             // loanAmt = 0;
                             ransom = 2;
                         }
                         // console.log({ infoDataAfterUpdate });
                        // // Update lại quỹ
                        let funds;
                        // console.log({ statusFinancial });
                        let FUND_MONEY = 1;
                        if(statusFinancial == FUND_MONEY){
                            funds = infoDataAgency.data.funds
                        }else{
                            // console.log("====da vao day 1");
                            funds = infoDataAgency.data.fundsBank
                        }

                        let typeUpdate;
                        // let priceOld = infoInjectTran.loanAmount;
                        let priceNew = 0;
                        if(priceOld > price) {
                            priceNew = priceOld - price;
                            typeUpdate = COLLECT;
                        }
                        if(priceOld < price){
                            priceNew = price - priceOld;
                            typeUpdate = SPEND;
                        }
                        // console.log({ priceOld, priceNew, typeUpdate, price });
                        // // Nếu type == 6 (Giao dịch chuộc đồ) =>  Không thêm vào quỹ
                        if(type != 2 && type != 6 && priceOld != priceNew) {
                            // // Update lại số tiền vay 
                            let infoDataAfterUpdate        = await TRANSACTION_MODEL.update({ transactionID: transactionMainID, loanAmount: loanAmt, description: "update" });
                            
                            let addTitleFinancial = "Chỉnh Sửa + " + string;
                            if ( typeUpdate == 1 ){
                                let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                                    title:  addTitleFinancial, 
                                    type:   typeFinancial,
                                    typeTransaction: type,
                                    agency: agencyID, 
                                    price:  priceNew, 
                                    funds, 
                                    transaction: transactionID,
                                    email:  infoDataAgency.data.email,
                                    onModel: "inject_transaction",
                                    statusFinancial
                                });
                            }else{
                                if ( funds >= priceNew ){
                                    let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                                        title:  addTitleFinancial, 
                                        type:   typeFinancial,
                                        typeTransaction: type,
                                        agency: agencyID, 
                                        price:  priceNew, 
                                        funds, 
                                        transaction: transactionID,
                                        email:  infoDataAgency.data.email,
                                        onModel: "inject_transaction",
                                        statusFinancial
                                    });
                                }else{
                                    return res.json({ error: true, message: "fund_not_enough"});
                                }
                            }
                        }

                         let infoInjectAfterUpdate = await INJECT_TRANSACTION_MODEL.updateInjectTransaction({
                             transactionID,
                             title,
                             type,
                             note,
                             meta,
                             customerImagesInjectIsDelete, 
                             KYCImagesInjectIsDelete, 
                             receiptImagesInjectIsDelete, 
                             formVerificationImagesInjectIsDelete
                         });
                         
                         // Kiểm tra xem giao dịch được duyệt hay chưa
                         // console.log({ price, loanAmt, typeFinancial, string });
                         // let infoAfterInsert = await INJECT_TRANSACTION_MODEL.insert({ 
                         //     title, type, transaction: transactionID , note, meta, agencyID, ransom
                         // });
                         let string2 = `${infoInjectAfterUpdate.data.code} + ${infoTransaction.customer.name} + `;
                         string      = string2 + string;
 
                         if ( type != 5 && type != 6 ){
                             let titleHistory = "Chỉnh Sửa + " + `${transactionID} + ` + string;
                             let infoHistoryTransactionAfterInsert = await HISTORY_TRANSACTION_MODEL.insert({
                                 title: titleHistory,
                                 type,
                                 employee: userID,
                                 interestRateTotal: loanAmoutAfterInterest,
                                 interestStartDate: infoTransaction.interestStartDate,
                                 resetDate: dateNow,
                                 transaction: transactionID,
                                 interstAmountDate: limitDate,
                                 interestPercent
                             });
                         }
 
                         if(dataImageUpdate) {
                             await INJECT_TRANSACTION_COLL.findByIdAndUpdate(infoInjectAfterUpdate.data._id, 
                                 dataImageUpdate
                             ,{new: true})
                         }

                        res.json(infoInjectAfterUpdate)
                        
                    }]
                },
            },

             /**
             * Function: cập nhật trạng thái giao dịch
             * Dev: Depv
             */
            [CF_ROUTINGS_TRANSACTION.UPDATE_STATUS_TRANSACTION]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID, username: nameUser } = req.user;
                        let { transactionID } = req.params;
                        let { status, noteNotApproved, statusFinancial } = req.body;
                        let agencyID    = req.agencyID;
                        let infoDataAgency = await AGENCY_MODEL.getInfo({ agencyID });
                        let infoData       = await TRANSACTION_MODEL.getInfo({ transactionID });

                        // 1// Kiểm tra xem quỹ có đủ hay kh hoặc Transaction kh được duyệt
                        // if(statusFinancial && statusFinancial == 1 && infoDataAgency.data.funds < infoData.data.loanAmount) {
                        //     return res.json({ error: true, message: "funds_not_enough"})
                        // }

                        // if(statusFinancial && statusFinancial == 2 && infoDataAgency.data.fundsBank < infoData.data.loanAmount) {
                        //     return res.json({ error: true, message: "funds_not_enough"})
                        // }

                        // if(infoDataAgency.data.funds >= infoData.data.loanAmount || infoDataAgency.data.fundsBank >= infoData.data.loanAmount || status == 2 ) {
                            // 2//Kiểm tra xem transaction có đã duyệt hay chưa, chưa được duyệt mới được phép thực hiện tiếp
                        if(infoData.data.status == 0) {
                            // 3// Nếu status == 1 ( Duyệt )
                            if (status == 1) {
                                if (!statusFinancial) {
                                    return res.json({ error: true, message: "statusFinancial_invalid"})
                                }
                                let agencyID    = req.agencyID;
                                // code, customerName, type, total
    
                                let infoTransaction = infoData.data;
    
                                let string = `${infoTransaction.code} + ${infoTransaction.customer.name} + `;
    
                                if ( !infoTransaction.injectTransaction || infoTransaction.injectTransaction.length == 0 ){
                                    string = string + `Cầm đồ + `
                                }else{
                                    TYPE_TRANSACTION.forEach( type => {
                                        if ( type.value == infoTransaction.injectTransaction.type ){
                                            string = string + `${type.text} + `
                                        }
                                    });
                                }

                                let nameFile = `` + infoData.data.code;

                                let funds;

                                // object chứa điều kiện -> tìm SỐ DƯ theo từng loại (loại tiền mặt || tiền ngân hàng)
                                let objectConditionForFindFund = {
                                    agency: agencyID,
                                };
                               
                                const LOAI_QUY_TIEN_MAT = 1;
                                const LOAI_QUY_TIEN_NGAN_HANG = 2;

                                objectConditionForFindFund = statusFinancial == LOAI_QUY_TIEN_MAT ?
                                    { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_MAT } :
                                    { ...objectConditionForFindFund, status: LOAI_QUY_TIEN_NGAN_HANG };
                                // console.log({ objectConditionForFindFund });
                                
                                let latestRecordFinicalOfAgency = await FINANCIAL_MODEL.getDocumentLatestUpdateWhere(objectConditionForFindFund);
                                // console.log({ latestRecordFinicalOfAgency });
                                
                                if (!latestRecordFinicalOfAgency) {
                                    return res.json({ error: true, message: "funds_not_enough", description: 'vui lòng tạo giao dịch nạp tiền'});
                                } else {
                                    funds = latestRecordFinicalOfAgency.fundsAfterChange;
                                }
                                // if(statusFinancial == LOAI_QUY_TIEN_MAT){
                                //     objectConditionForFindFund = {
                                //         ...objectConditionForFindFund, 
                                //         status: LOAI_QUY_TIEN_MAT
                                //     }
                                //     funds = infoDataAgency.data.funds
                                // }else{
                                //     funds = infoDataAgency.data.fundsBank
                                // }
                                /**
                                 4// CHECK QUỸ CÓ ĐỦ HAY KHÔNG
                                */
                                // console.log({ funds, infoTransaction });
                                if (funds < Number(infoTransaction.loanAmount)) {
                                    return res.json({ error: true, message: "funds_not_enough"});
                                }
                                // Tạo QR Code cho giao dịch
                                let infoQRAfterInsert = await QRCODE_MODEL.insert({ id: infoData.data._id, key: nameFile, userID })
                                // console.log({ infoQRAfterInsert });
                                let transactionConfirm = new Date();
                                let infoTransactionAfterUpdate = await TRANSACTION_MODEL.update({ 
                                    transactionID, qrCode: infoQRAfterInsert.data._id, transactionConfirm
                                });
                                string = string + `${ infoTransaction.loanAmount}`
                                
                                // Update lại quỹ
                                let infoFinancialAfterInsert = await FINANCIAL_MODEL.insert({ 
                                    title: string, 
                                    agency: agencyID, 
                                    price: infoTransaction.loanAmount, 
                                    funds, 
                                    typeTransaction: 1,
                                    transaction: transactionID,
                                    email: infoDataAgency.data.email,
                                    onModel: "transaction",
                                    statusFinancial
                                });
                                // console.log({ infoFinancialAfterInsert });
                            }
    
                            let infoAfterUpdate = await TRANSACTION_MODEL.updateStatus({ 
                                transactionID, status, noteNotApproved, ownerConfirm: userID
                            });
                            
                            return res.json(infoAfterUpdate)
                        }else{
                            return res.json({ error: true, message: "cannot_change_status_transaction" })
                        }
                        // }else{
                        //     return res.json({ error: true, message: "funds_not_enough" })
                        // }
                    }]
                },
            },

            /**
             * Function: cập nhật trạng thái giao dịch bổ sung
             * Dev: Depv
             */
            [CF_ROUTINGS_TRANSACTION.UPDATE_STATUS_INJECT_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { transactionID } = req.params;
                        let { status  } = req.body;
                        let infoAfterUpdate = await INJECT_TRANSACTION_MODEL.updateStatus({ 
                            transactionID, status
                        })
                        res.json(infoAfterUpdate)
                    }]
                },
            },

            [CF_ROUTINGS_TRANSACTION.UPDATE_IMAGE_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;

                        let { transactionID } = req.params;
                        let { receiptImagesUpdate, billImagesTransUpdate } = req.body;
                        
                        let infoAfterUpdate = await TRANSACTION_MODEL.updateImage({ 
                            receiptImagesUpdate, billImagesTransUpdate, transactionID, userID
                        })
                        res.json(infoAfterUpdate)
                    }]
                },
            },
            
            /**
             * Function: Thông tin giao dịch
             * Dev: Depv
             */
            [CF_ROUTINGS_TRANSACTION.GET_INFO_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let data    = await TRANSACTION_MODEL.getInfo({ transactionID });

                        let interestStartDate = data.data.interestStartDate;

                        let dateNow   =  new Date();
                        interestStartDate          = moment(interestStartDate).startOf('day').format();

                        let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                        // limitDate     = Math.floor(limitDate);
                        /**
                         * 1: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại > 0
                         *    => limitDate + 1, ngày tính lãi bắt đầu 1
                         * 2: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại < 0
                         *    => Đã trả lãi trước
                         *    => limitDate - 1, ngày tính lãi - 1
                         */
                         if(limitDate >= 0) {
                            limitDate     = Math.floor(limitDate) + 1;
                        }

                        if(limitDate < 0) {
                            limitDate = 0;
                        }

                        let loanAmoutAfterInterest = 0;
                        let percent = 0;
                        let infoInterest;
                        //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                        let TYPE_PRODUCT = data.data.products[0].type._id;
                        if ( limitDate > 0 ){
                            let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate, price: data.data.loanAmount, typeProduct: TYPE_PRODUCT});
                            checkDateToInterest.data.forEach( price => {
                                loanAmoutAfterInterest += price;
                            })
                            percent      = checkDateToInterest.percent;
                            infoInterest = checkDateToInterest.infoInterest;
                        }
                        /**
                         * Lấy Lãi và Phần Trăm Lãi của 1 ngày
                         */
                        let loanAmoutAfterInterestOneDay = 0;
                        let percentOneDay = 0;
                        let DTAE_OF_PERIOD =  1;

                        let checkDateToInterestOneDay = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate: DTAE_OF_PERIOD, price: data.data.loanAmount, typeProduct: TYPE_PRODUCT});
                        checkDateToInterestOneDay.data.forEach( price => {
                             loanAmoutAfterInterestOneDay += price;
                        })
                        percentOneDay      = checkDateToInterestOneDay.percent;
                        let interestOneday = {
                            percentOneDay,
                            loanAmoutAfterInterestOneDay
                        }

                        // console.log({ loanAmoutAfterInterest });
                        res.json({ data, limitDate, infoInterest, loanAmoutAfterInterest, percent, interestOneday })
                    }]
                },
            },
            
            /**
             * Function: Thông tin giao dịch (duyệt các loại giao dịch khác)
             */
            [CF_ROUTINGS_TRANSACTION.GET_INFO_TRANSACTION_NORMAL_CHECK]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let data                  = await TRANSACTION_MODEL.getInfoV2({ transactionID });
                        let infoInjectTransaction = await INJECT_TRANSACTION_COLL.findById(data.data.latestInjectTransaction._id, { meta: 1, title: 1, type: 1, code: 1, createAt: 1 });
                        if (data.error) {
                           return res.json(data)
                        }
                        if (!infoInjectTransaction) {
                            return res.json({ error: true, message: 'cannot_get_info' })
                        }
                        res.json({ 
                            error: false,
                            data: {
                                infoTransaction: data.data,
                                infoInjectTransaction
                            }
                         })
                    }]
                },
            },
                     
            
            /**
             * Function: LẤY % LÃI 1 NGÀY CHO CHUỘC ĐỒ
             * SONLP
             */
            [CF_ROUTINGS_TRANSACTION.GET_INFO_TRANSACTION_RANSOM]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { transactionID } = req.params;
                        let agencyID          = req.agencyID;
                        let data    = await TRANSACTION_MODEL.getInfo({ transactionID });
                        // let interestStartDate = data.data.interestStartDate;

                        // let dateNow   =  new Date();
                        // let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                        // limitDate     = Math.floor(limitDate);
                        
                        // if(limitDate < 0) {
                        //     limitDate = 0;
                        // }
                        let infoInjectTransaction = await INJECT_TRANSACTION_COLL.findById(data.data.latestInjectTransaction._id);
                        // console.log({ infoInjectTransaction });

                        let loanAmoutAfterInterest = 0;
                        let percent = 0;
                        let infoInterest;
                        //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                        let TYPE_PRODUCT = data.data.products[0].type._id;
                        let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate: 1, price: data.data.loanAmount, typeProduct: TYPE_PRODUCT});
                        checkDateToInterest.data.forEach( price => {
                            loanAmoutAfterInterest += price;
                        })
                        percent      = checkDateToInterest.percent;
                        infoInterest = checkDateToInterest.infoInterest;
                        // console.log({ loanAmoutAfterInterest });
                        res.json({ data, limitDate: 1, infoInterest, loanAmoutAfterInterest, percent, infoInjectTransaction })
                    }]
                },
            },

             /**
             * Function: Danh sách giao dịch
             * Dev: Depv
             */
            [CF_ROUTINGS_TRANSACTION.GET_LIST_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { agencyID } = req.params;
                        let data = await TRANSACTION_MODEL.getListByAgency({ agencyID });
                        
                        res.json(data);
                    }]
                },
            },

            /**
             * Function: Tất cả giao dịch
             * SONLP
             */
            [CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION,
					inc: path.resolve(__dirname, '../views/list_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let  agencyID  = req.agencyID;
                        let { typeTransaction, typeStatusTransaction, fromDay, toDay, type } = req.query;
                        // console.log({ typeTransaction, typeStatusTransaction, fromDay, toDay });
                        
                        let data = await TRANSACTION_MODEL.getListByAgencyID({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay });
                        // res.json(data);
                        // console.log({ data:data.data });s
                        
                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransaction: data.data,
                                TYPE_TRANSACTION,
                                TYPE_STATUS_TRANSACTION,
                                typeTrans: typeTransaction,
                                typeStatusTrans: typeStatusTransaction,
                                fromDay,
                                toDay,
                                agency: agencyID
                            }
                        );
                    }]
                },
            },

            /**
             * Function: DUYỆT GIAO DỊCH BÌNH THƯỜNG
             * SONLP
             */
             [CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_NORMAL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_NORMAL,
					inc: path.resolve(__dirname, '../views/list_transaction_normal.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let  agencyID  = req.agencyID;
                        // let { typeTransaction, typeStatusTransaction, fromDay, toDay, type } = req.query;
                        const STATUS_IN_PROCESS = 3;
                        let data = await TRANSACTION_MODEL.getListInjectNormal({ agencyID, status: STATUS_IN_PROCESS });
                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransaction: data.data,
                                agency: agencyID,
                                level,
                                userID,
                                nameUser,
                                TYPE_TRANSACTION
                            }
                        );
                    }]
                },
            },

            /**
             * danh sách đến hạn, hết hạn, có thể thanh lý
             * SONLP
             */
            [CF_ROUTINGS_TRANSACTION.LIST_MATURITY_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.LIST_MATURITY_TRANSACTION,
					inc: path.resolve(__dirname, '../views/list_maturity_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let  agencyID  = req.agencyID;
                        let { type } = req.query
                        let { typeTransaction, typeStatusTransaction, fromDay, toDay } = req.query;
                        // console.log({ typeTransaction, typeStatusTransaction, fromDay, toDay });
                        let data = await TRANSACTION_MODEL.getListByAgencyIDCheckDate({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay });
                        // console.log({ data });
                        let listTransaction = data.data;
                        if(typeTransaction && typeTransaction != 1){
                            let CHECK_TYPE_TRANSACTION = true;
                            listTransaction = listTransaction.filter( transaction => transaction.checkLastInjectTransaction == CHECK_TYPE_TRANSACTION);
                        }
                        if(type) {
                            // Đến hạn
                            if(type == "maturity") {
                                const DATE_MATURITY_MAX   = 3;
                                const DATE_MATURITY_LIMIT = 0;
                                listTransaction = listTransaction ? listTransaction.filter( transaction => transaction.dateDifference < DATE_MATURITY_MAX && transaction.dateDifference > DATE_MATURITY_LIMIT): [];
                            }
                            //Hết hạn
                            if(type == "expires") {
                                const DATE_EXPIRES_MAX   = 0;
                                const DATE_EXPIRES_LIMIT = -7;
                                listTransaction = listTransaction ? listTransaction.filter( transaction => transaction.dateDifference <= DATE_EXPIRES_MAX && transaction.dateDifference >= DATE_EXPIRES_LIMIT): [];
                            }
                            //Có thể thanh lý
                            if(type == "liquidationAssets") {
                                const DATE_LIQUIDATION_ASSETS_MAX   = -7;
                                listTransaction =  listTransaction ? listTransaction.filter( transaction => transaction.dateDifference < DATE_LIQUIDATION_ASSETS_MAX): [];
                            }
                        }

                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransaction,
                                TYPE_TRANSACTION,
                                TYPE_STATUS_TRANSACTION,
                                typeTrans: typeTransaction,
                                typeStatusTrans: typeStatusTransaction,
                                fromDay,
                                toDay,
                                agency: agencyID,
                                type
                            }
                        );
                    }]
                },
            },
            /**
             * danh sách giao dịch 24h
             * SONLP
             */
            [CF_ROUTINGS_TRANSACTION.LIST_24H_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.LIST_24H_TRANSACTION,
					inc: path.resolve(__dirname, '../views/list_24h_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let  agencyID  = req.agencyID;
                        let { type } = req.query
                        let { typeTransaction, typeStatusTransaction, fromDay, toDay } = req.query;
                        // console.log({ typeTransaction, typeStatusTransaction, fromDay, toDay });
                        let data = await TRANSACTION_MODEL.getListByAgencyID24H({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay });

                        let listTransaction = data.data;
                        if(typeTransaction && typeTransaction != 1){
                            let CHECK_TYPE_TRANSACTION = true;
                            listTransaction = listTransaction.filter(transaction => transaction.checkLastInjectTransaction == CHECK_TYPE_TRANSACTION);
                        }
                        let DATE_MATURITY_MAX   = 1;
                        let DATE_MATURITY_LIMIT = 0;
                        listTransaction = listTransaction.filter(transaction => transaction.dateDifference <= DATE_MATURITY_MAX && transaction.dateDifference >= DATE_MATURITY_LIMIT);

                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransaction,
                                TYPE_TRANSACTION,
                                TYPE_STATUS_TRANSACTION,
                                typeTrans: typeTransaction,
                                typeStatusTrans: typeStatusTransaction,
                                fromDay,
                                toDay,
                                agency: agencyID,
                                type
                            }
                        );
                    }]
                },
            },

            [CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_RANSOM]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_RANSOM,
					inc: path.resolve(__dirname, '../views/list_transaction_ransom.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser  } = req.user;
                        let  agencyID  = req.agencyID;
                        let { typeStatusTransaction, fromDay, toDay } = req.query;
                        // console.log({ typeTransaction, typeStatusTransaction, fromDay, toDay });
                        let ransom          = 2;
                        let typeTransaction = 6;
                        let data   = await TRANSACTION_MODEL.getListByAgencyID({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom });
                        // res.json(data);
                        // console.log({ data: data.data });
                        
                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransaction: data.data,
                                TYPE_TRANSACTION,
                                TYPE_STATUS_TRANSACTION,
                                typeTrans: typeTransaction,
                                typeStatusTrans: typeStatusTransaction,
                                fromDay,
                                toDay,
                                level,
                                nameUser,
                                userID,
                                agency: agencyID
                            }
                        );
                    }]
                },
            },
            /**
             * API Xuất thông tin giao dịch cho Mobile
             */
            [CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let  agencyID  = req.agencyID;
                        let { typeTransaction, typeStatusTransaction, fromDay, toDay } = req.query;
                        // console.log({ agencyID });
                        let data = await TRANSACTION_MODEL.getListByAgencyIDMobile({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay });
                        // res.json(data);

                        let listTransactionCheck;
                        if(data.error == true) {
                            data = [];
                        }else{
                            listTransactionCheck = data.data;
                            // const elementsIndex = listTransactionCheck.findIndex(transaction => !transaction.latestInjectTransaction )
                            // let newArray = [...listTransactionCheck];
                            // console.log({ newArray });
                            // newArray[elementsIndex] = {...newArray[elementsIndex], completed: !newArray[elementsIndex].completed}

                            // // listTransactionCheck: newArray,
                            listTransactionCheck.forEach((transaction, index) => {
                                if(!transaction.latestInjectTransaction){
                                    listTransactionCheck[index] = { transaction, latestInjectTransaction: {}, ransom: 0, type: 1, statusInject: 0 };  
                                }else{
                                    listTransactionCheck[index] = { 
                                        transaction, 
                                        latestInjectTransaction: transaction.latestInjectTransaction, 
                                        ransom: transaction.latestInjectTransaction.ransom, 
                                        type: transaction.latestInjectTransaction.type, 
                                        statusInject: transaction.latestInjectTransaction.status 
                                    };  
                                }
                            })
                        }   
                        
                        let description = {
                            status: "Trạng thái (GD Cầm đồ chính) (0: Đang xử lý - Chờ duyệt/ 1: Đang cầm - Đã duyệt/ 2: Không duyệt/ 3: Hoàn thành giao dịch)",
                            ransom: "Trạng thái duyệt chuộc đồ (0: Không phải giao dịch chuộc đồ /1: Đã duyệt chuộc/ 2: Chưa duyệt chuộc/ 3: Không duyệt)",
                            type:   "Loại giao dịch bổ sung (0. Phát sinh/ 1. Cầm đồ/ 2. Gia hạn bình thường/ 3. Gia hạn vay thêm/ 4. Gia hạn trả bớt/ 5. Báo mất/ 6. Chuộc đồ )",
                            statusInject: "Trạng thái giao dịch bổ sung (0. Giao dịch bình thường kh trễ hoặc báo mất/ 1. Đã duyệt/ 2. Không duyệt/ 3. Đang xử lý )",
                        };
                        res.json( 
                            { 
                                description,
                                listTransaction: listTransactionCheck,
                                TYPE_TRANSACTION,
                                TYPE_STATUS_TRANSACTION,
                                typeTrans: typeTransaction,
                                typeStatusTrans: typeStatusTransaction,
                                fromDay,
                                toDay,
                                agency: agencyID
                            }
                        );
                    }]
                },
            },

            [CF_ROUTINGS_TRANSACTION.GET_LIST_TRANSACTION_CHECK]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.GET_LIST_TRANSACTION_CHECK,
					inc: path.resolve(__dirname, '../views/list_transaction_check.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let  agencyID  = req.agencyID;
                        let status = 0;
                        let data = await TRANSACTION_MODEL.getListByAgencyWithStatus({ agencyID, status });
                        // res.json(data);
                        // console.log({ data: data.data });
                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransaction: data.data,
                                TYPE_TRANSACTION,
                                level,
                                nameUser,
                                userID,
                                agency: agencyID
                            }
                        );
                    }]
                },
            },
            /**
             * API Xuất thông tin giao dịch cho Mobile
             */
            [CF_ROUTINGS_TRANSACTION.GET_LIST_TRANSACTION_CHECK_API]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let  agencyID  = req.agencyID;
                        let status = 0;
                        let data = await TRANSACTION_MODEL.getListByAgencyWithStatus({ agencyID, status });
                        // res.json(data);
                        // console.log({ data: data.data });
                        res.json(
                            { 
                                listTransaction: data.data,
                                TYPE_TRANSACTION,
                                level,
                                nameUser,
                                userID,
                                agency: agencyID
                            }
                        );
                    }]
                },
            },
                        
            [CF_ROUTINGS_TRANSACTION.INFO_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.INFO_TRANSACTION,
					inc: path.resolve(__dirname, '../views/info_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, level, username: nameUser } = req.user;
                        let { transactionID }  = req.params;
                        let agencyID = req.agencyID;
                        let infoTransaction = await TRANSACTION_MODEL.getInfo({ transactionID });
                        // console.log({ infoTransaction });
                        if(infoTransaction.error) {
                            return res.json(infoTransaction);
                        }
                        let interestStartDate = infoTransaction.data.interestStartDate;
                        // Danh sách giao dịch bổ sung sắp xếp ngày mới nhất
                        let listInjectTransactionOfTransactionID = infoTransaction.listInjectTransactionOfTransactionID;
                       
                        let dateNow   =  new Date();
                        interestStartDate       = moment(interestStartDate).startOf('day').format();

                        let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                        
                        // let a = Math.ceil(limitDate)

                        /**
                         * 1: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại > 0
                         *    => limitDate + 1, ngày tính lãi bắt đầu 1
                         * 2: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại < 0
                         *    => Đã trả lãi trước
                         *    => limitDate - 1, ngày tính lãi - 1
                         */
                        if(limitDate >= 0) {
                            // let dateNowCheck           = moment(dateNow).startOf('day').format();
                            // let interestStartDateCheck = moment(interestStartDate).startOf('day').format();
                            // if (dateNowCheck == interestStartDateCheck) {
                            //     limitDate     = Math.floor(limitDate) + 1;
                            // }
                            // console.log({ limitDate });
                            limitDate     = Math.floor(limitDate) + 1;
                            // console.log({ limitDate });
                        }
                        
                        let infoInterest;
                        
                        if(infoTransaction.data.status == 3 || infoTransaction.data.status == 2) {
                            limitDate = 0;
                        }

                        if(limitDate < 0) {
                            limitDate = 0;
                        }

                        let loanAmoutAfterInterest = 0;
                        let percent = 0;
                        let arrObjPerPeriod = [];
                        //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                        let TYPE_PRODUCT = infoTransaction.data.products[0].type._id;
                        if (limitDate > 0 ) {
                            if(!infoTransaction.data.products || !infoTransaction.data.products.length) {
                                return res.json({ error: true, message: "product_invalid" }); 
                            }
                            let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate, price: infoTransaction.data.loanAmount, typeProduct: TYPE_PRODUCT});
                            checkDateToInterest && checkDateToInterest.data && checkDateToInterest.data.forEach( price => {
                                loanAmoutAfterInterest += price;
                            })

                            percent         = checkDateToInterest.percent;
                            infoInterest    = checkDateToInterest.infoInterest;
                            arrObjPerPeriod = checkDateToInterest.arrObjPerPeriod 
                        }
                        /**
                         * LẤY TỔNG SỐ TIỀN CỦA CÁC GIAO DỊCH ĐÃ TRÃ LÃI TRƯỚC
                         */
                        let pricePayInterest = await TRANSACTION_MODEL.getPricePayInterest({ agencyID, transactionID, TYPE_PRODUCT, loanAmount: infoTransaction.data.loanAmount });
                        if (pricePayInterest.error) {
                            pricePayInterest = 0;
                        }else{
                            pricePayInterest = pricePayInterest.data;
                        }
                        /**
                         * KIỂM TRA TRỄ HẠN
                         */
                        let expireTimeAfterPeriod = infoTransaction.data.expireTimeAfterPeriod; 

                        /**
                         * EXAMPLE:
                         *      Ngày hiện tại (1/8) -> Ngày đến hạn của chu kỳ tiếp theo 
                         */
                        let numberDateBetweenDate = subStractDateGetDay({date1: new Date(), date2: expireTimeAfterPeriod });
                        // numberDateBetweenDate     = Math.floor(Math.abs(numberDateBetweenDate));
                        numberDateBetweenDate = Math.floor(numberDateBetweenDate);
                        console.log({ numberDateBetweenDate });

                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        for (let province of listProvince){
                            if ( province[1].code == infoTransaction.data.customer.city ){
                                provinceArr = province;
                                break;
                            }
                        }

                        let listWards = [];
                        let wardArr = [];
                        if ( infoTransaction.data.customer.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoTransaction.data.customer.district ){
                                    districtArr = district;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoTransaction.data.customer.ward ){
                                            wardArr = ward;
                                            break;
                                        }
                                    }
                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            infoTransaction: infoTransaction.data,
                                            TYPE_GENDER,
                                            districtArr,
                                            provinceArr,
                                            wardArr,
                                            level,
                                            TYPE_TRANSACTION,
                                            infoInterest: infoInterest,
                                            limitDate: Math.floor(limitDate),
                                            loanAmoutAfterInterest,
                                            nameUser,
                                            userID,
                                            agency: agencyID,
                                            percent,
                                            listInjectTransactionOfTransactionID,
                                            arrObjPerPeriod,
                                            pricePayInterest,
                                            numberDateBetweenDate
                                        }
                                    );
                                } else {
                                    ChildRouter.renderToView(req, res, 
                                        { 
                                            infoTransaction: infoTransaction,
                                            TYPE_GENDER,
                                            districtArr,
                                            provinceArr,
                                            wardArr,
                                            level,
                                            TYPE_TRANSACTION,
                                            infoInterest: infoInterest.data,
                                            limitDate: Math.floor(limitDate),
                                            loanAmoutAfterInterest,
                                            nameUser,
                                            userID,
                                            agency: agencyID,
                                            percent,
                                            listInjectTransactionOfTransactionID,
                                            arrObjPerPeriod,
                                            pricePayInterest,
                                            numberDateBetweenDate
                                        }
                                    );
                                }
                            });
                        }else{
                            ChildRouter.renderToView(req, res, 
                                { 
                                    infoTransaction: infoTransaction.data,
                                    TYPE_GENDER,
                                    districtArr,
                                    provinceArr,
                                    wardArr,
                                    level,
                                    TYPE_TRANSACTION,
                                    infoInterest: infoInterest,
                                    limitDate: Math.floor(limitDate),
                                    loanAmoutAfterInterest,
                                    nameUser,
                                    userID,
                                    agency: agencyID,
                                    percent,
                                    listInjectTransactionOfTransactionID,
                                    arrObjPerPeriod,
                                    pricePayInterest,
                                    numberDateBetweenDate
                                }
                            );
                        }
                    }]
                },
            },

            /**
             * API Xuất thông tin giao dịch cho Mobile
             */
            [CF_ROUTINGS_TRANSACTION.INFO_TRANSACTION_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, level } = req.user;
                        let { transactionID }  = req.params;
                        let agencyID = req.agencyID;
                        // log
                        let infoTransaction = await TRANSACTION_MODEL.getInfo({ transactionID });
                        // console.log({ infoTransaction });


                        if(infoTransaction.error) {
                            return res.json(infoTransaction);
                        }

                        let interestStartDate = infoTransaction.data.interestStartDate;

                        let dateNow   =  new Date();
                        interestStartDate          = moment(interestStartDate).startOf('day').format();

                        let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                        // limitDate     = Math.floor(limitDate);
                        /**
                         * 1: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại > 0
                         *    => limitDate + 1, ngày tính lãi bắt đầu 1
                         * 2: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại < 0
                         *    => Đã trả lãi trước
                         *    => limitDate - 1, ngày tính lãi - 1
                         */
                        if(limitDate >= 0) {
                            limitDate     = Math.floor(limitDate) + 1;
                        }
                        if(limitDate < 0) {
                            limitDate = 0;
                        }

                        let infoInterest;
                        if(infoTransaction.data.status == 3 || infoTransaction.data.status == 2) {
                            limitDate = 0;
                        }

                        let loanAmoutAfterInterest = 0;
                        let percent = 0;
                        //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                        // console.log({ limitDate });
                        let arrObjPerPeriod = [];
                        let TYPE_PRODUCT = infoTransaction.data.products[0].type._id;

                        if (limitDate > 0 ) {
                            if(!infoTransaction.data.products || !infoTransaction.data.products.length) {
                                return res.json({ error: true, message: "product_invalid" }); 
                            }
                            let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate, price: infoTransaction.data.loanAmount, typeProduct: TYPE_PRODUCT});
                            checkDateToInterest && checkDateToInterest.data && checkDateToInterest.data.forEach( price => {
                                loanAmoutAfterInterest += price;
                            })

                            percent         = checkDateToInterest.percent;
                            infoInterest    = checkDateToInterest.infoInterest;
                            arrObjPerPeriod = checkDateToInterest.arrObjPerPeriod 
                        }
                        /**
                         * Lấy Lãi và Phần Trăm Lãi của 1 ngày
                         */
                         let loanAmoutAfterInterestOneDay = 0;
                         let percentOneDay = 0;
                         let DTAE_OF_PERIOD =  1;
 
                        //  let checkDateToInterestOneDay = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate: DTAE_OF_PERIOD, price: infoTransaction.data.loanAmount, typeProduct: TYPE_PRODUCT});
                        //  checkDateToInterestOneDay.data.forEach( price => {
                        //       loanAmoutAfterInterestOneDay += price;
                        //  })
                        //  percentOneDay      = checkDateToInterestOneDay.percent;

                        //  let interestOneday = {
                        //      percentOneDay,
                        //      loanAmoutAfterInterestOneDay
                        //  }

                        let infoTransactionAfterChange = infoTransaction.data;
                        infoTransactionAfterChange.injectTransaction = infoTransaction.listInjectTransactionOfTransactionID
                        
                        /**
                         * KIỂM TRA TRỄ HẠN
                         */
                         let expireTimeAfterPeriod = infoTransaction.data.expireTimeAfterPeriod; 

                         /**
                          * EXAMPLE:
                          *      Ngày hiện tại (1/8) -> Ngày đến hạn của chu kỳ tiếp theo 
                          */
                         let numberDateBetweenDate = subStractDateGetDay({date1: new Date(), date2: expireTimeAfterPeriod });
                         // numberDateBetweenDate     = Math.floor(Math.abs(numberDateBetweenDate));
                         numberDateBetweenDate = Math.floor(numberDateBetweenDate);
                         console.log({ numberDateBetweenDate });

                        infoTransactionAfterChange = {
                            ...infoTransactionAfterChange._doc,
                            percent, loanAmoutAfterInterest, limitDate, arrObjPerPeriod, numberDateBetweenDate
                        }
                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        for (let province of listProvince){
                            if ( province[1].code == infoTransactionAfterChange.customer.city ){
                                infoTransactionAfterChange.customer.city = province[1].name_with_type;
                                break;
                            }
                        }

                        let listWards = [];
                        let wardArr = [];
                        if ( infoTransactionAfterChange.customer.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoTransactionAfterChange.customer.district ){
                                    districtArr = district;
                                    infoTransactionAfterChange.customer.district = district[1].name_with_type;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoTransactionAfterChange.customer.ward ){
                                            infoTransactionAfterChange.customer.ward = [1].name_with_type;
                                            break;
                                        }
                                    }
                                    res.json( 
                                        { 
                                            infoTransaction: infoTransactionAfterChange,
                                        }
                                    );
                                } else {
                                    res.json( 
                                        { 
                                            infoTransaction: infoTransactionAfterChange,
                                        }
                                    );
                                }
                            });
                        }else{
                            res.json( 
                                { 
                                    infoTransaction: infoTransactionAfterChange,
                                }
                            );
                        }
                    //     ChildRouter.renderToView(req, res, 
                    //         { 
                    //             infoTransaction: infoTransaction.data,
                    //             TYPE_GENDER
                    //         }
                    //     );
                    }]
                },
            },

            /**
             * Gửi yêu cầu giao dịch lại một lần nữa
             * Dev: VyPQ
             * 07/05/2021
             */
             [CF_ROUTINGS_TRANSACTION.RESEND_REQUEST_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { transactionID } = req.query;

                        let infoTransactionAfterUpdate = await TRANSACTION_MODEL.resendRequestTransaction({ transactionID });

                        return res.json(infoTransactionAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Thông tin giao dịch (duyệt các loại giao dịch khác)
             */
             [CF_ROUTINGS_TRANSACTION.INFO_TRANSACTION_MAIN]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { transactionID, injectTransactionID } = req.query;
                        let agencyID          = req.agencyID;
                        let data              = await TRANSACTION_MODEL.getInfoPrint({ transactionID, agencyID, injectTransactionID });
                        if (injectTransactionID) {
                            let infoInjectTransaction = await INJECT_TRANSACTION_COLL.findById(injectTransactionID, { 
                                code: 1, note: 1, status: 1, meta: 1, ransom: 1, type: 1, transactionDate: 1
                            });

                            if (infoInjectTransaction.status == 2 || infoInjectTransaction.status == 3) {
                                return res.json({
                                    error: true, 
                                    message: 'Bạn cần duyệt giao dịch để in'
                                });
                            }
                            if (infoInjectTransaction.ransom == 2 || infoInjectTransaction.ransom == 3) {
                                return res.json({
                                    error: true, 
                                    message: 'Bạn cần duyệt giao dịch để in'
                                });
                            }
                           
                            data.data = {
                                ...data.data,
                                infoInjectTransaction
                            }
                            // console.log({
                            //     data, infoInjectTransaction
                            // });
                        }

                        let infoProvinceAgency   = COMMON_MODEL.getInfoProvince({ provinceCode: data.data.agency.city }); 
                        let infoDistrictAgency   = COMMON_MODEL.getInfoDistrict({ districtCode: data.data.agency.district })
                        let infoWardAgency       = await COMMON_MODEL.getInfoWard({ district: data.data.agency.district, wardCode: data.data.agency.ward })
                        let address = data.data.customer.address ? data.data.customer.address + ', ' : '';
                        data.data.agency.address   = data.data.agency.address + `, ${infoWardAgency.data[1].name_with_type}` + `, ${infoDistrictAgency.data[1].name_with_type}` + `, ${infoProvinceAgency.data[1].name_with_type}` 

                        if (data.data.customer.city) {
                            let infoProvinceCustomer = COMMON_MODEL.getInfoProvince({ provinceCode: data.data.customer.city });
                            if (data.data.customer.district) {
                                let infoDistrictCustomer = COMMON_MODEL.getInfoDistrict({ districtCode: data.data.customer.district });
                                if (data.data.customer.ward) {
                                    let infoWardCustomer       = await COMMON_MODEL.getInfoWard({ district: data.data.customer.district, wardCode: data.data.customer.ward });
                                    data.data.customer.address = address + `${infoWardCustomer.data[1].name_with_type}, ` + `${infoDistrictCustomer.data[1].name_with_type}, ` + `${infoProvinceAgency.data[1].name_with_type}`;
                                } else {
                                    data.data.customer.address = address + `${infoDistrictCustomer.data[1].name_with_type}, ` + `${infoProvinceAgency.data[1].name_with_type}`;
                                }
                            } else {
                                data.data.customer.address = address + `${infoProvinceCustomer.data[1].name_with_type}`
                            }
                        }

                        res.json({
                            TYPE_TRANSACTION,
                            ...data
                        })
                    }]
                },
            },

            /**
             * Function: Thông tin giao dịch (duyệt các loại giao dịch khác)
             */
             [CF_ROUTINGS_TRANSACTION.EXPORT_EXCEL_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { type, start, end, name, city, district, ward, sort, agencyID: agencyFilter } = req.query;
                        let agencyID   = AGENCY_SESSION.getContextAgency(req.session).agencyID;

                        /**
                         * DANH SÁCH GIAO DỊCH
                         */
                        let listTransaction = await TRANSACTION_MODEL.getListTransactionReveneExportExcel({ agencyID, typeProductCategoryID: type, start, end, name, city, district, ward, status: sort, agencyFilter });
                        
                        XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report_Transactions_Form.xlsx')))
                        .then(async workbook => {
                            let dateFilter = '';
                            if (start) {
                                dateFilter += `Từ ngày: ${moment(start).format('L')}`;
                            }
                            if (end) {
                                dateFilter += ` Đến ngày: ${moment(end).format('L')}`;
                            }
                            if (!start && !end) {
                                dateFilter = 'Tất cả';
                            }

                            workbook.sheet("Report").row(2).cell(1).value(dateFilter);
                            if (!listTransaction.error && listTransaction.data.listTransaction.length) {
                                let index = 0;
                               
                                for (let item of listTransaction.data.listTransaction) {
                                    let data = await TRANSACTION_MODEL.getDataToExportExcel({
                                        infoTransaction: item
                                    });
                                    if (item.injectTransaction && item.injectTransaction.length) {
                                        for (let i = -1; i < item.injectTransaction.length; i++) {
                                            if (i == -1) {
                                                workbook.sheet("Report").row(index + 5).cell(1).value(index + 1);
                                                workbook.sheet("Report").row(index + 5).cell(2).value(data.data.parentCode);
                                                workbook.sheet("Report").row(index + 5).cell(3).value(data.data.codeAgency);
                                                workbook.sheet("Report").row(index + 5).cell(4).value(data.data.nameAgency);
                                                workbook.sheet("Report").row(index + 5).cell(5).value(data.data.phoneAgency);
                                                workbook.sheet("Report").row(index + 5).cell(6).value(data.data.addressAgency);
                                                workbook.sheet("Report").row(index + 5).cell(7).value(data.data.listOwner);
                                                workbook.sheet("Report").row(index + 5).cell(8).value(data.data.emailAgency);
                                                workbook.sheet("Report").row(index + 5).cell(9).value(moment(data.data.createAt).format('L'));
                                                workbook.sheet("Report").row(index + 5).cell(10).value(data.data.codeCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(11).value(data.data.nameCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(12).value(data.data.phoneCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(13).value(data.data.addressCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(15).value(data.data.codeTransaction);
                                                workbook.sheet("Report").row(index + 5).cell(16).value(data.data.typeProduct);
                                                workbook.sheet("Report").row(index + 5).cell(17).value(data.data.productName);
                                                workbook.sheet("Report").row(index + 5).cell(18).value(data.data.approvalLimit);

                                                let typeCheck = 'Cầm đồ';
                                                workbook.sheet("Report").row(index + 5).cell(14).value(typeCheck);
                                                workbook.sheet("Report").row(index + 5).cell(19).value(item.totalMoneyNumber);
                                                workbook.sheet("Report").row(index + 5).cell(20).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(21).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(22).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(23).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(24).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(25).value(item.nameFinancial ? item.nameFinancial : 0);
                                                let status = '';
                                                switch (item.status) {
                                                    case 0:
                                                        status = 'Đang xử lý'   
                                                        break;
                                                    case 1:
                                                        status = 'Đang cầm'   
                                                        break;
                                                    case 2:
                                                        status = 'Không duyệt'   
                                                        break;
                                                    case 3:
                                                        status = 'Hoàn thành giao dịch'   
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                workbook.sheet("Report").row(index + 5).cell(26).value(status);
                                                workbook.sheet("Report").row(index + 5).cell(27).value(item.transactionEmployee.username);
                                                workbook.sheet("Report").row(index + 5).cell(28).value(item.approvalNote ? item.approvalNote: '');
                                                workbook.sheet("Report").row(index + 5).cell(29).value(item.ownerConfirm ? item.ownerConfirm.username : '');
                                                workbook.sheet("Report").row(index + 5).cell(30).value(item.noteNotApproved);
                                                index ++;

                                           } else {
                                                // CHECK TYPE TRANSACTION
                                                let typeCheck = '';
                                                if (item.injectTransaction[i].type != 5) {
                                                    workbook.sheet("Report").row(index + 5).cell(1).value(index + 1);
                                                    workbook.sheet("Report").row(index + 5).cell(2).value(data.data.parentCode);
                                                    workbook.sheet("Report").row(index + 5).cell(3).value(data.data.codeAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(4).value(data.data.nameAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(5).value(data.data.phoneAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(6).value(data.data.addressAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(7).value(data.data.listOwner);
                                                    workbook.sheet("Report").row(index + 5).cell(8).value(data.data.emailAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(9).value(moment(item.injectTransaction[i].createAt).format('L'));
                                                    workbook.sheet("Report").row(index + 5).cell(10).value(data.data.codeCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(11).value(data.data.nameCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(12).value(data.data.phoneCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(13).value(data.data.addressCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(15).value(data.data.codeTransaction);
                                                    workbook.sheet("Report").row(index + 5).cell(16).value(data.data.typeProduct);
                                                    workbook.sheet("Report").row(index + 5).cell(17).value(data.data.productName);
                                                    workbook.sheet("Report").row(index + 5).cell(18).value(data.data.approvalLimit);

                                                    let status = '';
                                                    switch (item.injectTransaction[i].status) {
                                                        case 0:
                                                            if (item.injectTransaction[i].ransom == 1) {
                                                                status = 'Đã duyệt';
                                                            } else if (item.injectTransaction[i].ransom == 2) {
                                                                status = 'Chưa duyệt';
                                                            } else if (item.injectTransaction[i].ransom == 3) {
                                                                status = 'Không duyệt';
                                                            } else {
                                                                status = 'Đã duyệt'   
                                                            }
                                                            break;
                                                        case 1:
                                                            status = 'Đã duyệt'   
                                                            break;
                                                        case 2:
                                                            status = 'Không duyệt'   
                                                            break;
                                                        case 3:
                                                            status = 'Đang xử lý'   
                                                            break;
                                                        default:
                                                            break;
                                                    }

                                                    let totalMoneyNumber = 0;
                                                    let totalPriceBorrow = 0;
                                                    let totalPricePay    = 0;
                                                    let totalPriceNormal = 0;
                                                    let totalPriceRansom = 0;
                                                    let loanAmountAfterChange = 0;
                                                    let nameStatusFinancial = '';
                                                    nameStatusFinancial = (item.injectTransaction[i].meta.statusFinancial && item.injectTransaction[i].meta.statusFinancial == 1) ? "Quỹ tiền mặt" : 'Quỹ ngân hàng';
                                                    switch (item.injectTransaction[i].type) {
                                                        case 2:
                                                            typeCheck = 'Gia hạn bình thường';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            loanAmountAfterChange = item.injectTransaction[i].meta.loanAmountAfterChange ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;
                                                            break;
                                                        case 3:
                                                            typeCheck = 'Gia hạn vay thêm';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            totalPriceBorrow = item.injectTransaction[i].meta.amountWantBorrowMore;
                                                            loanAmountAfterChange = item.injectTransaction[i].meta.loanAmountAfterChange ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;

                                                            break;
                                                        case 4:
                                                            typeCheck = 'Gia hạn trả bớt';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            totalPricePay    = item.injectTransaction[i].meta.amountWantPayback;
                                                            loanAmountAfterChange = item.injectTransaction[i].meta.loanAmountAfterChange ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;

                                                            break;
                                                        case 6:
                                                            typeCheck = 'Gia hạn chuộc đồ';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            totalPriceRansom = item.injectTransaction[i].meta.priceRansomPay;
                                                            loanAmountAfterChange = (item.injectTransaction[i].meta.loanAmountAfterChange) ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;
                                                            
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    
                                                    workbook.sheet("Report").row(index + 5).cell(14).value(typeCheck);
                                                    workbook.sheet("Report").row(index + 5).cell(19).value(totalMoneyNumber);
                                                    workbook.sheet("Report").row(index + 5).cell(20).value(totalPriceBorrow);
                                                    workbook.sheet("Report").row(index + 5).cell(21).value(totalPricePay);
                                                    workbook.sheet("Report").row(index + 5).cell(22).value(totalPriceRansom);
                                                    workbook.sheet("Report").row(index + 5).cell(23).value(loanAmountAfterChange);
                                                    workbook.sheet("Report").row(index + 5).cell(24).value(totalPriceNormal);
                                                    workbook.sheet("Report").row(index + 5).cell(25).value(nameStatusFinancial);
                                                    workbook.sheet("Report").row(index + 5).cell(26).value(status);
                                                    workbook.sheet("Report").row(index + 5).cell(27).value(item.injectTransaction[i].userCreateAt ? item.injectTransaction[i].userCreateAt.username: '');
                                                    workbook.sheet("Report").row(index + 5).cell(28).value(item.injectTransaction[i].note ? item.injectTransaction[i].note: '');
                                                    workbook.sheet("Report").row(index + 5).cell(29).value(item.injectTransaction[i].userConfirm ? item.injectTransaction[i].userConfirm.username : '');
                                                    workbook.sheet("Report").row(index + 5).cell(30).value(item.injectTransaction[i].noteNotApproved ? item.injectTransaction[i].noteNotApproved: '');
                                                    index ++;
                                                }
                                           }
                                        }
                                    } else {
                                        workbook.sheet("Report").row(index + 5).cell(1).value(index + 1);
                                        workbook.sheet("Report").row(index + 5).cell(2).value(data.data.parentCode);
                                        workbook.sheet("Report").row(index + 5).cell(3).value(data.data.codeAgency);
                                        workbook.sheet("Report").row(index + 5).cell(4).value(data.data.nameAgency);
                                        workbook.sheet("Report").row(index + 5).cell(5).value(data.data.phoneAgency);
                                        workbook.sheet("Report").row(index + 5).cell(6).value(data.data.addressAgency);
                                        workbook.sheet("Report").row(index + 5).cell(7).value(data.data.listOwner);
                                        workbook.sheet("Report").row(index + 5).cell(8).value(data.data.emailAgency);
                                        workbook.sheet("Report").row(index + 5).cell(9).value(moment(data.data.createAt).format('L'));
                                        workbook.sheet("Report").row(index + 5).cell(10).value(data.data.codeCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(11).value(data.data.nameCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(12).value(data.data.phoneCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(13).value(data.data.addressCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(15).value(data.data.codeTransaction);
                                        workbook.sheet("Report").row(index + 5).cell(16).value(data.data.typeProduct);
                                        workbook.sheet("Report").row(index + 5).cell(17).value(data.data.productName);
                                        workbook.sheet("Report").row(index + 5).cell(18).value(data.data.approvalLimit);

                                        let typeCheck = 'Cầm đồ';
                                        workbook.sheet("Report").row(index + 5).cell(14).value(typeCheck);
                                        workbook.sheet("Report").row(index + 5).cell(19).value(item.totalMoneyNumber);
                                        workbook.sheet("Report").row(index + 5).cell(20).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(21).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(22).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(23).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(24).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(25).value(item.nameFinancial ? item.nameFinancial : 0);
                                        let status = '';
                                        switch (item.status) {
                                            case 0:
                                                status = 'Đang xử lý'   
                                                break;
                                            case 1:
                                                status = 'Đang cầm'   
                                                break;
                                            case 2:
                                                status = 'Không duyệt'   
                                                break;
                                            case 3:
                                                status = 'Hoàn thành giao dịch'   
                                                break;
                                            default:
                                                break;
                                        }
                                        workbook.sheet("Report").row(index + 5).cell(26).value(status);
                                        workbook.sheet("Report").row(index + 5).cell(27).value(item.transactionEmployee.username);
                                        workbook.sheet("Report").row(index + 5).cell(28).value(item.approvalNote ? item.approvalNote: '');
                                        workbook.sheet("Report").row(index + 5).cell(29).value(item.ownerConfirm ? item.ownerConfirm.username : '');
                                        workbook.sheet("Report").row(index + 5).cell(30).value(item.noteNotApproved);
                                        index ++;
                                    }
                                }
                            }

                            let fileNameRandom = require('../../../utils/string_utils').randomStringFixLengthOnlyAlphabet(10);
                            let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', `Report_Transaction_${fileNameRandom}.xlsx`);
                            
                            workbook.toFileAsync(pathWriteFile)
                                .then(_ => {
                                    // Download file from server
                                    res.download(pathWriteFile, function (err) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            // Remove file on server
                                            fs.unlinkSync(pathWriteFile);
                                        }    
                                    });
                                });                           
                        });
                    }]
                },
            },

        }
    }
};
