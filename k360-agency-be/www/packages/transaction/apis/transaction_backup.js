/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs = require('fs');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadFields, uploadCusAndAuthPaper }       = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_TRANSACTION }                   = require('../constant/transaction.uri');
const { TYPE_TRANSACTION, TYPE_GENDER, TYPE_STATUS_TRANSACTION }             = require('../../../config/cf_constants');
const { subStractDateGetDay }                       = require('../../../config/cf_time');

/**
 * MODELS, COLLECTIONS
 */
//  const { IMAGE_MODEL }                               = require('../../image');
//  const TRANSACTION_MODEL                             = require('../models/transaction').MODEL;
//  const HISTORY_TRANSACTION_MODEL                             = require('../models/history_transaction').MODEL;
//  const INVENTORY_SESSION_MODEL                       = require('../models/inventory_session').MODEL;
//  const { AGENCY_MODEL }                              = require('../../agency');
//  const { FINANCIAL_MODEL }                           = require('../../financial');
//  const { QRCODE_MODEL }                              = require('../../qrcode');
//  const INJECT_TRANSACTION_MODEL                      = require('../models/inject_transaction').MODEL;
//  const PRODUCT_MODEL                                 = require('../models/product').MODEL;
//  const PRODUCT_CATEGORY_MODEL                        = require('../../product_category').MODEL;
//  const { INTEREST_RATE_MODEL }                       = require('../../interest_rate');
//  const INTEREST_RATE_MODEL                           = require('../../interest_rate').MODEL;
 const { provinces }                                 = require('../../common/constant/provinces');
 const { districts }                                 = require('../../common/constant/districts');

const CUSTOMER_MODEL                             = require('../../customer/models/customer').MODEL;
const CUSTOMER_AGENCY_MODEL                      = require('../../customer/models/customer_agency').MODEL;
const { AGENCY_MODEL } = require('../../agency');
// const history_transactionColl = require('../database/history_transaction-coll');
// const { log } = require('console');
const TRANSACTION_COLL                              = require('../database/transaction-coll')({});
const INJECT_TRANSACTION_COLL                       = require('../database/inject_transaction-coll')({});


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    
    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================       API TRANSACTION    ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: tạo transactiop
             * Dev: Depv
             */

            [CF_ROUTINGS_TRANSACTION.GET_ADD_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_TRANSACTION.ADD_TRANSACTION,
					inc: path.resolve(__dirname, '../views/add_transaction.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, username: nameUser} = req.user;
                        let agencyID = req.agencyID;
                        let { customerID } = req.query;
                        let listProvince        = Object.entries(provinces);

                        // let listCustomerOfAgency = await CUSTOMER_MODEL.getListByAgency({ agencyID, status: 1 });
                        let infoAgency           = await AGENCY_MODEL.getInfo({ agencyID });
                        // console.log({ infoAgency });
                        let listCustomerOfAgency = await CUSTOMER_AGENCY_MODEL.getListByAgency({ agencyID, status: 1 });
                        // console.log({  listCustomerOfAgency });
                        ChildRouter.renderToView(req, res, 
                            { 
                                nameUser,
                                listCustomerOfAgency: listCustomerOfAgency.data,
                                agency: agencyID,
                                userID,
                                customerID,
                                listProvince,
                                percentOfAgency: infoAgency.data.percentPawn
                            }
                        );
                    }]
                },
            },

        }
    }
};
