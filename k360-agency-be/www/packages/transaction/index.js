const TRANSACTION_MODEL 		= require('./models/transaction').MODEL;
const TRANSACTION_COLL  		= require('./database/transaction-coll')({});
const TRANSACTION_ROUTES        = require('./apis/transaction');
const TRANSACTION_BACKUP_ROUTES = require('./apis/transaction_backup');
const INVENTORY_ROUTES          = require('./apis/inventory_session');

module.exports = {
    TRANSACTION_ROUTES,
    TRANSACTION_BACKUP_ROUTES,
    TRANSACTION_COLL,
    TRANSACTION_MODEL,
    INVENTORY_ROUTES
}
