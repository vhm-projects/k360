"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION LỊCH SỬ KIỂM KÊ GÓI HÀNG
 * Dev: Dattv
 */
module.exports = function({ fields = {}, name = 'history_check' }){
   return BASE_COLl(name, {

        userID: {
            type: Schema.Types.ObjectId,
            ref: "user"
        },

        agency: {
            type: Schema.Types.ObjectId,
            ref: "agency"
        },

        transaction: {
            type: Schema.Types.ObjectId,
            ref: "transaction"
        },

        inventorySession: {
            type: Schema.Types.ObjectId,
            ref: "inventory_session"
        },
        
        ...fields
    });
}
