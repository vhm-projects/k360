"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION GIAO DỊCH CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'transaction' }){
   return BASE_COLl(name, {

        /**
         * Khách hàng
         */
        customer: {
            type: Schema.Types.ObjectId,
            ref: 'customer'
        },

        /**
         *  Nhân Viên Giao Dịch
         */
        transactionEmployee: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        }, 

        /**
         *  Nhân Viên Giao Dịch
         */
        ownerConfirm: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        }, 

        /**
         * Nhân Viên Kiểm Duyệt
         */ 
        transactionOwner: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        }, 
        /**
         * Đại lý
         */ 
        agency: {
            type: Schema.Types.ObjectId,
            ref: 'agency'
        },
        /**
         * Ngày giao dịch
         *  => Chuyển thành ngày giao dịch lãi
         */ 
        transactionDate:{
            type: Date,
            default: new Date()
        },

        /**
         * Ngày giao dịch được duyệt
         */ 
         transactionConfirm:{
            type: Date,
        },
        /**
         * số ngày cầm dự kiến mặc định 30 ngày
         */ 
        expectedDate: {
            type: Number,
            default: 30 // Nghiên cứu cộng 30
        },
        /**
         * Ngày Đến Hạn = ngày hiện tại + 30 ngày
         *  =>  Ngầy chốt lãi
         */ 
        expireTime: {
            type: Date,
            default: new Date() // Nghiên cứu cộng 30 day
        },

        /**
         * Ngày chốt lãi của chu kỳ sau
         *  => expireTime + 30
         */ 
         expireTimeAfterPeriod: {
            type: Date,
            default: new Date() // Nghiên cứu cộng 30 day
        },

         /**
         * Số tiền vay
         * Số tiền vay chỉ được set -5% so với HẠN_MỨC_PHÊ_DUYỆT (tức 95%)
         */ 
        loanAmount: {
            type: Number,
            default: 0
        },
        /**
         * Số tiền trả trước còn lại
         */ 
        payInterest: {
            type: Number,
            default: 0
        },
        //  loanAmount: {
        //     type: Number,
        //     default: 0
        // },
        /**
         * Hạn mức phê duyệt
         */ 
        approvalLimit: {
            type: Number,
            default: 0
        },

         /**
         * Ngày bắt đầu tính lãi
         * Ngày Hiện Tại hoặc dựa vào ngày reset
         */ 
        interestStartDate: {
            type: Date,
            default: new Date()
        },

        /**
         * Ghi chú giao dịch
         */ 
        note: String,
        /**
         * Ghi chú giao dịch không duyệt
         */ 
        noteNotApproved: String,
         /**
         * Số ngày tính lãi
         * NGÀY KẾT THÚC (HIỆN TẠI) - NGÀY BĂT ĐẦU TÍNH LÃI
         */ 
        interestAmount: {
            type: Number,
        },

        /**
         * Tổng lãi + phí
         */ 
        interestTotal: {
            type: Number,
        },
        
        /**
         * Phí được giảm
         */ 
        discountTotal: Number,
        /**
         * Lý do giảm
         */ 
        discountReason: String,
        /**
         * Tổng tiền trả bằng số
         */ 
        totalMoneyNumber: Number,
        /**
         * Tổng tiền trả bằng chữ
         */ 
        totalMoneyChar: String,
        /**
         * Ghi chú duyệt
         */ 
        approvalNote: String,
         /**
         * Mã biên nhận
         */ 
        code: {
            type: String,
            unique : true,
        },
        /**
         * Trạng thái
         * 0: Đang xử lý - chờ duyệt
         * 1: Đang cầm - Đã duyệt
         * 2: Không duyệt
         * 3: Hoàn thành giao dịch
         */ 
        status: {
            type: Number,
            default: 0
        },

        category: {
            type: Schema.Types.ObjectId,
            ref: "product_category"
        },
        /**
         * Danh sách giao dịch bổ sung
         */
        injectTransaction: [{
            type: Schema.Types.ObjectId,
            ref: 'inject_transaction'
        }],
        /**
         * Giao dịch cuối cùng
         */ 
        latestInjectTransaction: {
            type: Schema.Types.ObjectId,
            ref: 'inject_transaction'
        },
        latestUpdate: Date,
        /**
         * Hình ảnh khách hàng
         */ 
        customerImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],

        /**
         * Hình Ảnh Giấy Tờ (CMND/CCCD)
         */ 
        KYCImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],

        /**
         * Hình Ảnh Biên Nhận
         */ 
        receiptImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],
         /**
         * Hình Ảnh Phiếu Kiểm Định
         * => ĐỔI THÀNH HÌNH ẢNH SẢN PHẨM
         */ 
        formVerificationImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],
        /**
         * Hình Ảnh Hóa Đơn
         * => HÌNH ẢNH HÓA ĐƠN - PHIẾU KIỂM ĐỊNH
         */ 
         billImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],
        /**
         * Hình ảnh khách hàng
         */ 
        qrCode: {
            type: Schema.Types.ObjectId,
            ref: 'qr_code'
        },

         /**
         * Sản phẩm giao dịch
         */ 
        products: [{
            type: Schema.Types.ObjectId,
            ref: 'product'
        }],

        /**
         * Trạng thái kiểm kê
         * 0: Chưa kiểm kê
         * 1: Đã kiểm kê
         */
        typeInventory: {
            type: Number,
            default: 0
        },
            
        /**
         * History check
         */
        historyCheck: [{
            type: Schema.Types.ObjectId,
            ref: 'history_check'
        }],

        ...fields
    });
}
