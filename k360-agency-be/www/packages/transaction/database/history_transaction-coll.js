"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION LỊCH SỬ GIAO DỊCH CỦA HỆ THỐNG
 * CHỈ ÁP DỤNG CHO 3 LOẠI GIAO DỊCH
 * VAY THÊM
 * TRẢ BỚT
 * GIA HẠN
 */
module.exports = function({ fields = {}, name = 'history_transaction' }){
   return BASE_COLl(name, {
     
        title: String,
        /**
         *  Loại giao dịch
         *  7: Giao dịch trả lại số tiền khi GD chính đã được trả lãi trước và trong thời hạn đó, KH tạo gd khác
         */
        type: {
            type: Number,
            default: 1
        },
        /**
         * Nhân viên giao dịch
         */
        employee: {
            type: Schema.Types.ObjectId,
            ref: "user"
        },
        /**
         * ID inject_transaction
         */
        transaction: {
            type: Schema.Types.ObjectId,
            ref: "inject_transaction"
        },
        /**
         * ID inject_transaction
         */
        transactionMain: {
            type: Schema.Types.ObjectId,
            ref: "transaction"
        },
        // meta: {
        //     // % LÃI TẠI CHU KÌ ĐÓ
        //     interestPercent: Number,
        //     // SỐ TIỀN TẠI CHU KÌ ĐÓ,
        //     price: Number,
        //     // SỐ NGÀY TÍNH LÃI
        //     interstAmountDate: Number
        // },
        /**
         * Lãi + phí
         * Số tiền trước khi reset cho Chu Kỳ TRƯỚC
         */
        interestRateTotal: Number,
        /**
         * Ngày Tính Lãi Trong Giao Dịch Chính
         */
        interestStartDate: {
            type: Date
        },
        /**
         * Ngày Reset Lãi (Ngày hiện tại thực hiện giao dịch bổ sung)
         */
        resetDate: {
            type: Date,
            default: Date.now()
        },
        /**
         * Số ngày tính lãi
         */
        interstAmountDate: Number,
        /**
         * % Lãi được tính theo số ngày dựa vào Module Lãi
         */
        interestPercent: Number,
        ...fields
    });
}
