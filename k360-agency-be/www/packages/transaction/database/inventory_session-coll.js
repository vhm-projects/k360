"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION PHIÊN KIỂM KÊ CỦA GIAO DỊCH
 * Dev: Dattv
 */
module.exports = function({ fields = {}, name = 'inventory_session' }){
   return BASE_COLl(name, {

        name: String,

        description: String,
     
        /**
         * Danh sách các giao dịch đã kiểm tra
         */
        transactions: [{
            type: Schema.Types.ObjectId,
            ref: "transaction"
        }],
        
        agency: {
            type: Schema.Types.ObjectId,
            ref: "agency"
        },

        userID: {
            type: Schema.Types.ObjectId,
            ref: "user"
        },

        fromDate: Date,

        endDate: Date,
        
        ...fields
    });
}
