"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION LỊCH SỬ GIAO DỊCH CỦA HỆ THỐNG
 * CHỈ ÁP DỤNG CHO 3 LOẠI GIAO DỊCH
 * TRẢ LÃI TRƯỚC
 */
module.exports = function({ fields = {}, name = 'pay_interest_transaction' }){
   return BASE_COLl(name, {
     
        title: String,
        /**
         *  Loại giao dịch
         */
        type: {
            type: Number,
            default: 2
        },
        /**
         * Nhân viên giao dịch
         */
        employee: {
            type: Schema.Types.ObjectId,
            ref: "user"
        },
        /**
         * ID inject_transaction
         */
        transaction: {
            type: Schema.Types.ObjectId,
            ref: "inject_transaction"
        },
        /**
         * ID inject_transaction
         */
        transactionMain: {
            type: Schema.Types.ObjectId,
            ref: "transaction"
        },
        /**
         * Chu kỳ lãi
         */
        period: Number,
        /**
         * Lãi + phí
         * Số tiền lãi trả trước
         */
        interestRateTotal: Number,
        /**
         * Lãi + phí
         * Số tiền lãi trả trước
         */
        // interestRateRest: Number,
        /**
         * Ngày Tính Lãi Trong Giao Dịch Chính
         */
        interestStartDate: {
            type: Date
        },
        /**
         * Ngày Reset Lãi (Ngày hiện tại thực hiện giao dịch bổ sung)
         */
        resetDate: {
            type: Date,
            default: new Date()
        },
        /**
         * Ngày Đến Hạn = ngày Ngày Reset Lãi + Số ngày tính lãi interstAmountDate
         *  =>  Ngầy chốt lãi
         */ 
         expireTime: {
            type: Date,
            // default: new Date() 
        },
        /**
         * Số ngày tính lãi
         */
        interstAmountDate: Number,
        /**
         * % Lãi được tính theo số ngày dựa vào Module Lãi
         */
        interestPercent: Number,
        /**
         * Trạng thái hoạt động của GD Bình thường trả trước
         * 1: Hoạt động
         * 0: Dừng hoạt động
         */
        status: {
            type: Number,
            default: 1
        },
        ...fields
    });
}
