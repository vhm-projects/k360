const BASE_ROUTE_ADMIN = '/inventory';
const BASE_ROUTE_API   = '/api/inventory';

const CF_ROUTINGS_INVENTORY = {
   
    VIEW_LIST_INVENTORY_SESSION: `${BASE_ROUTE_ADMIN}/list-inventory`,
    VIEW_LIST_INVENTORY_SESSION_API: `${BASE_ROUTE_API}/list-inventory`,
    DETAIL_INVENTORY_SESSISON: `${BASE_ROUTE_ADMIN}/detail/:inventorySessionID`,
    ADD_INVENTORY_SESSISON: `${BASE_ROUTE_ADMIN}/add-inventory-session`,
    REMOVE_INVENTORY_SESSISON: `${BASE_ROUTE_ADMIN}/remove-inventory-session/:inventorySessionID`,
    UPDATE_INVENTORY_SESSISON: `${BASE_ROUTE_ADMIN}/update-inventory-session/:inventorySessionID`,
    INFO_INVENTORY_SESSISON: `${BASE_ROUTE_ADMIN}/:inventorySessionID`,

    ORIGIN_APP: BASE_ROUTE_ADMIN
}

exports.CF_ROUTINGS_INVENTORY = CF_ROUTINGS_INVENTORY;
