const BASE_ROUTE_ADMIN = '/transaction';
const BASE_ROUTE_API   = '/api/transaction';

const CF_ROUTINGS_TRANSACTION = {
    // ACCOUNT KHÁCH HÀNG
    ADD_TRANSACTION: `${BASE_ROUTE_ADMIN}/add-transaction`,
    GET_ADD_TRANSACTION: `${BASE_ROUTE_ADMIN}/add`,
    GET_UPDATE_TRANSACTION: `${BASE_ROUTE_ADMIN}/update-transaction/:transactionID`,
    UPDATE_TRANSACTION: `${BASE_ROUTE_ADMIN}/update/:transactionID`,
    UPDATE_STATUS_TRANSACTION: `${BASE_ROUTE_ADMIN}/update-status-transaction/:transactionID`,

    ADD_INJECT_TRANSACTION: `${BASE_ROUTE_ADMIN}/add-inject-transaction/:transactionID`,
    
    ADD_INJECT_TRANSACTION_NORMAL:   `${BASE_ROUTE_ADMIN}/add-inject-transaction-normal/:transactionID`,
    VIEW_LIST_TRANSACTIONS: `${BASE_ROUTE_ADMIN}s`,
    VIEW_LIST_TRANSACTION_IN_DAY: `${BASE_ROUTE_ADMIN}/list-transaction-in-day`,

    /**
     * API TRẢ LÃI TRƯỚC
     */
     ADD_INJECT_TRANSACTION_PAY_BEFORE:   `${BASE_ROUTE_ADMIN}/add-inject-transaction-normal-before/:transactionID`,
     /**
     * API TRẢ LÃI ĐÚNG KỲ
     */
     ADD_INJECT_TRANSACTION_PAY_ON_TIME:   `${BASE_ROUTE_ADMIN}/add-inject-transaction-normal-on-time/:transactionID`,

    /**
     * API TRẢ LÃI TRỄ KỲ CẦN DUYỆT
     */
    ADD_INJECT_TRANSACTION_PAY_LATE:   `${BASE_ROUTE_ADMIN}/add-inject-transaction-late/:transactionID`,

    /**
     * API TRẢ LÃI TRỄ KỲ KHÔNG CẦN DUYỆT
     */
     ADD_INJECT_TRANSACTION_PAY_LATE_NOT_CHECK:   `${BASE_ROUTE_ADMIN}/add-inject-transaction-late-not-check/:transactionID`,

    ADD_INJECT_TRANSACTION_ARROW:    `${BASE_ROUTE_ADMIN}/add-inject-transaction-arrow/:transactionID`,
    ADD_INJECT_TRANSACTION_PAY_BACK: `${BASE_ROUTE_ADMIN}/add-inject-transaction-pay-back/:transactionID`,
    ADD_INJECT_TRANSACTION_RANSOM:   `${BASE_ROUTE_ADMIN}/add-inject-transaction-ransom/:transactionID`,
    ADD_INJECT_TRANSACTION_LOST:     `${BASE_ROUTE_ADMIN}/add-inject-transaction-lost/:transactionID`,

    UPDATE_INJECT_TRANSACTION: `${BASE_ROUTE_ADMIN}/update-inject-transaction/:transactionID`,
    UPDATE_STATUS_INJECT_TRANSACTION: `${BASE_ROUTE_ADMIN}/update-status-inject-transaction/:transactionID`,
    ADD_HISTORY_CHECK_TRANSACTION: `${BASE_ROUTE_ADMIN}/add-history-check-transaction/`,

    GET_INFO_TRANSACTION: `${BASE_ROUTE_ADMIN}/get-info/:transactionID`,
    GET_INFO_TRANSACTION_RANSOM: `${BASE_ROUTE_ADMIN}/get-info-ransom/:transactionID`,
    
    GET_INFO_TRANSACTION_NORMAL_CHECK: `${BASE_ROUTE_ADMIN}/get-info-normal-check/:transactionID`,
    
    /**
     * ===================UPDATE GIAO DỊCH BỔ SUNG TRẢ LÃI TRỄ===================
     */
    UPDATE_INJECT_TRANSACTION_NORMAL:  `${BASE_ROUTE_ADMIN}/update-inject-transaction-normal/:transactionID`,
   
    /**
     * ===================UPDATE GIAO DỊCH VAY THÊM===================
     */
    UPDATE_INJECT_TRANSACTION_BORROW:  `${BASE_ROUTE_ADMIN}/update-inject-transaction-borrow/:transactionID`,

     /**
     * ===================UPDATE GIAO DỊCH TRẢ BỚT===================
     */
    UPDATE_INJECT_TRANSACTION_PAY_BACK:  `${BASE_ROUTE_ADMIN}/update-inject-transaction-pay-back/:transactionID`,

    INFO_TRANSACTION: `${BASE_ROUTE_ADMIN}/info-transaction/:transactionID`,
    
    INFO_TRANSACTION_MAIN: `${BASE_ROUTE_ADMIN}/info-transaction-main/:transactionID`,

    EXPORT_EXCEL_TRANSACTION: `${BASE_ROUTE_ADMIN}/export-excel-transaction`,

    GET_LIST_TRANSACTION: `${BASE_ROUTE_ADMIN}/get-list-transaction/:agencyID`,
    LIST_TRANSACTION: `${BASE_ROUTE_ADMIN}/list-transaction`,
    LIST_TRANSACTION_RANSOM: `${BASE_ROUTE_ADMIN}/list-ransom-inspect`,
    GET_LIST_TRANSACTION_CHECK: `${BASE_ROUTE_ADMIN}/list-create-transaction-inspect`,
    
    LIST_TRANSACTION_NORMAL: `${BASE_ROUTE_ADMIN}/list-transaction-normal`,


    LIST_MATURITY_TRANSACTION: `${BASE_ROUTE_ADMIN}/list-maturity-transaction`,
    LIST_24H_TRANSACTION: `${BASE_ROUTE_ADMIN}/list-24h-transaction`,
    // LIST_EXPIRES_TRANSACTION: `${BASE_ROUTE_ADMIN}/list-expires-transaction`,
    // LIST_LIQUIDATION_ASSETS_TRANSACTION: `${BASE_ROUTE_ADMIN}/list-liquidation-assets-transaction`,
    // DUYỆT CHUỘC
    CONFIRM_RANSOM: `${BASE_ROUTE_ADMIN}/confirm-ransom/:transactionID`,
    

    //API Render View Mobile
    GET_LIST_TRANSACTION_CHECK_API: `${BASE_ROUTE_API}/list`,
    LIST_TRANSACTION_API: `${BASE_ROUTE_API}/list-transaction`,
    INFO_TRANSACTION_API: `${BASE_ROUTE_API}/info-transaction/:transactionID`,
    VIEW_LIST_TRANSACTION_24H_API: `${BASE_ROUTE_API}/list-transaction-24h`,
    VIEW_LIST_PRODUCT_API: `${BASE_ROUTE_API}/list-product-24h`,
    /**
     * Chỉnh sửa hình ảnh không bắt buộc
     */
    UPDATE_IMAGE_TRANSACTION: `${BASE_ROUTE_API}/update-image/:transactionID`,

    VIEW_LIST_TRANSACTION: `${BASE_ROUTE_ADMIN}/list-transaction-24h`,
    VIEW_LIST_PRODUCT: `${BASE_ROUTE_ADMIN}/list-product-24h`,
    VIEW_LIST_INVENTORY_SESSION: `${BASE_ROUTE_ADMIN}/list-inventory-session`,

    VIEW_LIST_HISTORY_CHECK_OF_TRANSACTION: `${BASE_ROUTE_ADMIN}/history-check/:transactionID`,
    RESEND_REQUEST_TRANSACTION: `${BASE_ROUTE_ADMIN}/resend-request-transaction`,
    
    ORIGIN_APP: BASE_ROUTE_ADMIN
}

exports.CF_ROUTINGS_TRANSACTION = CF_ROUTINGS_TRANSACTION;
