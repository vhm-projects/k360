"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION AGENCY PRODUCT CATEGORY CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'agency_product_category' }){
	return BASE_COLL(name, {
        agency: {
            type: Schema.Types.ObjectId,
            ref: 'agency'
        },
        product_category: {
            type: Schema.Types.ObjectId,
            ref: 'product_category'
        },
        price: {
            type: Number,
            default: 0
        },
		// Phần trăm giá trị sản phẩm
		percentValue: {
			type: Number,
			default: 0
		},
		// Lãi mặc định sản phẩm
        interestDefault: {
            type: Number,
            default: 5
        },
		dropDown: [{
			text: {
				type: String,
				default: ''
			},
			value: {
				type: Number,
				default: 0
			}
		}],
        /**
         * 0: Không hiển thị
         * 1: Hiển thị
         */
        status: {
            type: Number,
            default: 1
        },
        userCreate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        userUpdate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
		...fields
    });
}
