const BASE_ROUTE = '/agency-product-category';

const CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY = {
    // AGENCY PRODUCT CATEGORY
    ADD_AGENCY_PRODUCT_CATEGORY: `${BASE_ROUTE}/add-agency-product-category`,
    UPDATE_AGENCY_PRODUCT_CATEGORY: `${BASE_ROUTE}/update-agency-product-category`,
    UPDATE_STATUS_AGENCY_CATEGORY: `${BASE_ROUTE}/update-status-agency-category`,
    INFO_CATEGORY_AGENCY: `${BASE_ROUTE}/info-agency-product-category`,
    LIST_AGENCY_PRODUCT_CATEGORY: `${BASE_ROUTE}/list-agency-product-category`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY = CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY;
