"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const BaseModel                         = require('../../../models/intalize/base_model');
const { checkObjectIDs }                = require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const AGENCY_PRODUCT_CATEGORY_COLL      = require('../databases/agency-product-category-coll')({});
const PRODUCT_CATEGORY_COLL				= require('../../product_category/databases/product-category-coll')({});



class Model extends BaseModel {
    constructor() {
        super(AGENCY_PRODUCT_CATEGORY_COLL);
    }

    updateOrInsert({ agencyID, parentID, listCategoryID, price, percentValue, interest, listItemDropdown, status, userUpdate }) {
        return new Promise(async (resolve) => {
            try {
                if(!agencyID || agencyID && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'param_invalid' });

				if(!listCategoryID || !listCategoryID.length || !checkObjectIDs(...listCategoryID))
                    return resolve({ error: true, message: 'param_invalid' });

                let dataUpdate = {
                    price,
					percentValue,
					dropDown: listItemDropdown,
                    status,
                    userUpdate
                }
                if(!parentID){
                    dataUpdate.interestDefault = +interest;
                }

                listCategoryID.map(async categoryID => {
                    await AGENCY_PRODUCT_CATEGORY_COLL.updateOne({
                        agency: agencyID,
                        product_category: categoryID
                    }, {
                        $set: dataUpdate,
                        $setOnInsert: { 
                            agency: agencyID,
                            product_category: categoryID,
                            userCreate: userUpdate 
                        }
                    }, { upsert: true });
                })

                return resolve({ error: false, message: 'update_agency_category_success' });
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

	updateStatus({ agencyID, listCategoryID, status, userUpdate }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'param_invalid' });

				if(!listCategoryID || !listCategoryID.length || !checkObjectIDs(listCategoryID))
                    return resolve({ error: true, message: 'param_invalid' });

				let dataUpdate = {
					status,
					userUpdate
				}

				listCategoryID.map(async categoryID => {
					const checkExistAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL.findOne({
						agency: agencyID,
						product_category: categoryID
					})

					if(!checkExistAgencyCategory){
						const infoSystemCategory = await PRODUCT_CATEGORY_COLL.findOne({ _id: categoryID }).lean();

						await AGENCY_PRODUCT_CATEGORY_COLL.create({
							status,
							agency: agencyID,
							product_category: categoryID,
							price: infoSystemCategory.price,
							percentValue: infoSystemCategory.percentValue,
							userCreate: userUpdate
						});
					} else{
						await AGENCY_PRODUCT_CATEGORY_COLL.updateOne({
							agency: agencyID,
							product_category: categoryID
						}, { 
							$set: dataUpdate 
						});
					}

				})

                return resolve({ error: false, message: 'update_status_category_success' });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

    getList({ categoryID, agencyID = null }){
        return new Promise(async resolve => {
            try {
                if(agencyID && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(categoryID && !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let condition = {
                    agency: agencyID, 
                };
                categoryID  && (condition.product_category  = categoryID);

                const listPriceAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL.find(condition).lean();
                if(!listPriceAgencyCategory)
                    return resolve({ error: true, message: 'cannot_found_list_price_agency_category' });

                return resolve({ error: false, data: listPriceAgencyCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListV2({ categoryID, agencyID = null }){
        return new Promise(async resolve => {
            try {
                if(agencyID && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(categoryID && !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let condition = {
                    agency: agencyID, status: 1
                };
                categoryID  && (condition.product_category  = categoryID);

                const listPriceAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL.find(condition).lean();
                if(!listPriceAgencyCategory)
                    return resolve({ error: true, message: 'cannot_found_list_price_agency_category' });

                return resolve({ error: false, data: listPriceAgencyCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListV3({ categoryID, agencyID = null }){
        return new Promise(async resolve => {
            try {
                if(agencyID && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(categoryID && !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const STATUS_ACTIVE = 1;
                let condition = {
                    agency: agencyID, status: STATUS_ACTIVE
                };
                categoryID  && (condition.product_category  = categoryID);
                const listPriceAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL.find(condition).populate('product_category').lean();
                
                if(!listPriceAgencyCategory)
                    return resolve({ error: true, message: 'cannot_found_list_price_agency_category' });
                let listData       = listPriceAgencyCategory.filter(item => item.product_category.parent == null)
                let listAfterCheck = listData.map( item => item.product_category);

                return resolve({ error: false, data: listAfterCheck });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByAgency({ agencyID, categoryID }){
        return new Promise(async resolve => {
            try {
                if(!agencyID || !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'param_invalid' });

                if(!categoryID || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_invalid' });

                const infoCategoryByAgency = await AGENCY_PRODUCT_CATEGORY_COLL.findOne({ 
                    agency: agencyID, 
                    product_category: categoryID 
                })
				.populate('product_category')
				.lean();

                if(!infoCategoryByAgency)
                    return resolve({ error: true, message: 'not_found_price_agency_category' });

                return resolve({ error: false, data: infoCategoryByAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
