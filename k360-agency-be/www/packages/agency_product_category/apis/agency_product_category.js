"use strict";

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY } 		= require('../constant/agency_product_category.uri');

/**
 * MODELS
 */
const AGENCY_PRODUCT_CATEGORY_MODEL = require('../models/agency_product_category').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ******************************* ================================
             * ========================== QUẢN LÝ AGENCY PRODUCT CATEGORY ================================
             * ========================== ******************************* ================================
             */

            /**
             * Function: Cập nhật giá danh mục cho đại lý (API)
             * Date: 20/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY.UPDATE_AGENCY_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate } = req.user;
                        const { parentID, agencyID, listCategoryID, price, percentValue, interest, listItemDropdown, status } = req.body;

                        const infoAfterUpdate = await AGENCY_PRODUCT_CATEGORY_MODEL.updateOrInsert({
							agencyID, parentID, listCategoryID, price, percentValue, interest, listItemDropdown, status, userUpdate
						})
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Cập nhật trạng thái danh mục cho đại lý (API)
             * Date: 23/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY.UPDATE_STATUS_AGENCY_CATEGORY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate } = req.user;
                        const { agencyID, listCategoryID, status } = req.body;

                        const infoAfterUpdate = await AGENCY_PRODUCT_CATEGORY_MODEL.updateStatus({
							agencyID, listCategoryID, status, userUpdate
						})
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Danh sách danh mục đại lý (API)
             * Date: 21/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY.LIST_AGENCY_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID, categoryID } = req.query;

                        const listPriceAgency = await AGENCY_PRODUCT_CATEGORY_MODEL.getList({
                            agencyID, categoryID
                        })
                        res.json(listPriceAgency);
                    }]
                },
            },

            /**
             * Function: Thông tin danh mục sản phẩm theo đại lý (API)
             * Date: 27/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY.INFO_CATEGORY_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID, categoryID } = req.query;
						agencyID = agencyID || req.agencyID;

                        let infoPriceAgency = await AGENCY_PRODUCT_CATEGORY_MODEL.getInfoByAgency({
                            agencyID, categoryID
                        })
                        res.json(infoPriceAgency);
                    }]
                },
            },

        }
    }
};
