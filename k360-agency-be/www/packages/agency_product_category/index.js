const AGENCY_PRODUCT_CATEGORY_MODEL 		    = require('./models/agency_product_category').MODEL;
const AGENCY_PRODUCT_CATEGORY_COLL  		    = require('./databases/agency-product-category-coll')({});
const AGENCY_PRODUCT_CATEGORY_ROUTES            = require('./apis/agency_product_category');

module.exports = {
    AGENCY_PRODUCT_CATEGORY_COLL,
    AGENCY_PRODUCT_CATEGORY_MODEL,
    AGENCY_PRODUCT_CATEGORY_ROUTES
}
