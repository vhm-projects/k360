"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID              = require('mongoose').Types.ObjectId;
const path                  = require('path');
const fs                    = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const AGENCY_SESSION							    = require('../../../session/agency-session');
const USER_SESSION							        = require('../../../session/user-session');
const { CF_ROUTINGS_COMMON }                        = require('../constant/common.uri');
const { districts }                                 = require('../constant/districts');
const { provinces }                                 = require('../constant/provinces');
const { TYPE_LEVEL }                                = require('../../../config/cf_constants');


/**
 * MODELS
 */
const { USER_MODEL }   = require('../../permissions/users');
const { AGENCY_MODEL } = require('../../agency');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************** ================================
             * ========================== QUẢN LÝ CHUNG  ================================
             * ========================== ************** ================================
             */

			'/': {
				config: {
					auth: [ roles.role.all.bin ],
				},
				methods: {
					get: [ (req, res) => res.redirect('/transaction/list-transaction')]
				}
			},
            
         

            // [CF_ROUTINGS_COMMON.GENERATE_LINK_S3]: {
            //     config: {
            //         auth: [ roles.role.all.bin ],
            //         type: 'json'
            //     },
            //     methods: {
            //         get: [ async function (req, res) {
            //             var fileName = req.query.fileName;
            //             var type = req.query.type;
 
            //             let linkUpload  = await GENERATE_LINK_S3(fileName, type); 
                            
            //             // console.log({ pathFile, linkUpload , path });
            //             if(linkUpload.error)
            //                 return res.json({error: true, message: "cannot_create_link"});

            //             return res.json({ error: false, linkUpload });
            //         }]
            //     }
            // },

            [CF_ROUTINGS_COMMON.HOME]: {
                config: {
					auth: [ roles.role.owner.bin ],
					type: 'view',
					title: 'Agency Manager - K360',
					code: CF_ROUTINGS_COMMON.HOME,
					inc: path.resolve(__dirname, '../../../views/inc/home.ejs'),
                    view: 'index-agency.ejs'
				},
				methods: {
					get: [ function (req, res) {
                        const userAgency = req.user.agency;
                        let agencySession = AGENCY_SESSION.getContextAgency(req.session);

                        if(!agencySession && userAgency && userAgency.length){
                            const agencyID = userAgency[0]._id;
                            AGENCY_SESSION.saveContextAgency(req.session, { agencyID });
                        }

						ChildRouter.renderToView(req, res);
					}]
				},
            },

            

            [CF_ROUTINGS_COMMON.ACCOUNT_SETTING]: {
                config: {
					auth: [ roles.role.employee.bin ],
					type: 'view',
					title: 'Agency Manager - K360',
					code: CF_ROUTINGS_COMMON.ACCOUNT_SETTING,
					inc: path.resolve(__dirname, '../../../views/inc/user_infomation.ejs'),
                    view: 'index-agency.ejs'
				},
				methods: {
					get: [ async function (req, res) {
                        const { _id: userID } = req.user;
                        const userAgency = req.user.agency;
                        let agencySession = AGENCY_SESSION.getContextAgency(req.session);

                        if(!agencySession && userAgency && userAgency.length){
                            const agencyID = userAgency[0]._id;
                            AGENCY_SESSION.saveContextAgency(req.session, { agencyID });
                        }

						ChildRouter.renderToView(req, res, {
                            TYPE_LEVEL
                        });
					}]
				},
            },

            [CF_ROUTINGS_COMMON.CHOOSE_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json'
                },
                methods: {
                    get: [async function (req, res){
                        const { agencyID, type } = req.query;
                        const envAccess          = req.envAccess;

                        if(!agencyID || !ObjectID.isValid(agencyID)){
                            return res.json({ error: true, message: 'agency_invalid' });
                        }
                        /**
                         * lưu agencyID với stateful
                         */
                        const BROWSER_ACCESS = 1;
                        if(envAccess == BROWSER_ACCESS) {
                            AGENCY_SESSION.saveContextAgency(req.session, { agencyID });

                            res.json({ error: false, message: 'save_agency_success' });
                        } else {
                            // let obj = {}
                            // if(type && type == "api"){
                            //     let token         = USER_SESSION.getUser(req.session).token;
                            //     let infoAgency    = await AGENCY_MODEL.getInfo({ agencyID });
                            //     obj = {
                            //         token, infoAgency: infoAgency.data
                            //     }
                            // }
                            res.json({ error: false, message: 'stateless_dont_need_save_agency' });
                        }
                    }]
                }
            },

            [CF_ROUTINGS_COMMON.LOGIN]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-agency.ejs',
                    view: 'pages/login-agency.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/transaction/list-transaction');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, password, type }   = req.body;
                        const infoSignIn = await USER_MODEL.signIn({ email, password });
                        console.log({ infoSignIn });
                        if(infoSignIn.error == true) {
                            return res.json(infoSignIn);
                        }
                        
                        USER_SESSION.saveUser(req.session, {
                            user: infoSignIn.data.user,
                            token: infoSignIn.data.token,
                        });
                        res.json(infoSignIn);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.LOGIN_MOBILE]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'json',
				},
				methods: {
                    post: [ async function (req, res) {
                        let { email, password, deviceID, deviceName, registrationID } = req.body;
                        let infoSignIn = await USER_MODEL.signinWithMobile({ email, password, deviceID, deviceName, registrationID });

                        if(infoSignIn.error == true) {
                            return res.json(infoSignIn);
                        }

                        let listProvince        = Object.entries(provinces);
                        let listDistricts       = Object.entries(districts);

                        let listAgency       = infoSignIn.data.infoUser.agency;
                        // console.log({ listAgency });

                        let listAgencyChange = [];
                        if(listAgency && listAgency.length) {
                            listAgency.forEach( agency => {
                                let nameProvince;
                                let objAgency = {};
                                // 2// Lấy tên thành phố
                                for(let province of listProvince) {
                                    if(province[1].code == agency.city) {
                                        nameProvince = province[1].name_with_type;
                                        break;
                                    }
                                }
                                // 3// Lấy field trong của agency đó
                                objAgency = {
                                    _id:              agency._id,
                                    count:            agency.childs,
                                    owners:           agency.owners,
                                    employees:        agency.employees,
                                    status:           agency.status,
                                    name:             agency.name,
                                    email:            agency.email,
                                    phone:            agency.phone,
                                    address:          agency.address,
                                    openTime:         agency.openTime,
                                    closeTime:        agency.closeTime,
                                    userCreate:       agency.userCreate,
                                    location:         agency.location,
                                    modifyAt:         agency.modifyAt,
                                    createAt:         agency.createAt,
                                    city:             nameProvince,
                                    code:             agency.code,
                                    tax_code:         agency.tax_code,
                                    userUpdate:       agency.userUpdate,
                                    ward:             agency.ward,
                                    funds:            agency.funds,
                                    productPawn:      agency.productPawn,
                                    avatar:           agency.avatar,
                                    gallery:          agency.gallery,
                                    percentPawn:      agency.percentPawn,
                                }
                                // 4// Lấy quận/ Huyện
                                for(let district of listDistricts) {
                                    if(district[1].code == agency.district ){
                                        objAgency.district = district[1].name_with_type;
                                        break;
                                    }
                                }

                                // 5// Push vào mảng mới
                                listAgencyChange.push(objAgency)
                            })
                        }

                        let user  =    infoSignIn.data.user;
                        let token =    infoSignIn.data.token;

                        let infoUserAfterAssignListAgency = {
                            user: {
                                ...user._doc, 
                                agency: listAgencyChange
                            },
                            token
                        }
                        // infoSignIn.data.user.agency = listAgencyChange;
                        // console.log({ agency: infoSignIn.data.user.agency });
                        // let data = {
                        //     user:     infoSignIn.data.user,
                        //     token:    infoSignIn.data.token,
                        //     // infoUser: infoSignIn.data.infoUser,
                        // }
                        return res.json({ error: false, data: infoUserAfterAssignListAgency })
                        // res.json(infoSignIn);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.LOGIN]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-agency.ejs',
                    view: 'pages/login-agency.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/transaction/list-transaction');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, password, type }   = req.body;
                        const infoSignIn = await USER_MODEL.signIn({ email, password });
                        //console.log({ infoSignIn });
                        if(infoSignIn.error == true) {
                            return res.json(infoSignIn);
                        }
                        
                        USER_SESSION.saveUser(req.session, {
                            user: infoSignIn.data.user,
                            token: infoSignIn.data.token,
                        });
                        res.json(infoSignIn);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.FORGET_PASSWORD]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/forget-password-admin.ejs',
                    view: 'pages/forget-password-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/transaction/list-transaction');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email } = req.body;

                        const infoSignIn = await USER_MODEL.forgetPassword({ email });
                        
                        res.json(infoSignIn);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.CHANGE_PASSWORD]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/change-password-admin.ejs',
                    view: 'pages/change-password-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
                        let isExistLogin = USER_SESSION.getUser(req.session);
                        if (isExistLogin && isExistLogin.user && isExistLogin.token)
                            return res.redirect('/transaction/list-transaction');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, code } = req.body;

                        const infoCheckCode = await USER_MODEL.checkCodeForgetPassword({ email, code });
                        
                        res.json(infoCheckCode);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.UPDATE_PASSWORD_RECOVER]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/update-password-admin.ejs',
                    view: 'pages/update-password-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
                        let isExistLogin = USER_SESSION.getUser(req.session);
                        if (isExistLogin && isExistLogin.user && isExistLogin.token)
                            return res.redirect('/transaction/list-transaction');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, code, password  } = req.body;

                        const infoCheckCode = await USER_MODEL.updateForgetPassword({email, code, password});
                        
                        res.json(infoCheckCode);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.LOGOUT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    view: 'pages/destroy-session.ejs',
                    type: 'view',
                },
                methods: {
                    get: [ async (req, res) => {
						AGENCY_SESSION.destroySession(req.session);
                        USER_SESSION.destroySession(req.session);
                        ChildRouter.renderToView(req, res);
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.PAGE_NOT_FOUND]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    view: 'pages/404_page.ejs',
                    type: 'view',
                },
                methods: {
                    get: [ async (req, res) => {
                        ChildRouter.renderToView(req, res);
                    }]
                },
            },
            

            [CF_ROUTINGS_COMMON.LIST_PROVINCES]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let listProvince = Object.entries(provinces);
                        res.json({ listProvince });
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_DISTRICTS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { province } = req.params;
                        let listDistricts = [];

                        let filterObject = (obj, filter, filterValue) => 
                            Object.keys(obj).reduce((acc, val) =>
                            (obj[val][filter] === filterValue ? {
                                ...acc,
                                [val]: obj[val]  
                            } : acc
                        ), {});

                        if (province && !Number.isNaN(Number(province))) {
                            listDistricts = filterObject(districts, 'parent_code', province.toString())
                        }

                        res.json({ province, listDistricts });
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_WARDS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { district } = req.params;
                        let listWards = [];
                        let  filePath = path.resolve(__dirname, `../constant/wards/${district}.json`);
                        fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                listWards = JSON.parse(data);
                                res.json({ district, listWards  });
                            } else {
                                res.json({ error: true, message: "district_not_exist" });
                            }
                        });
                    }]
                },
            },
            [CF_ROUTINGS_COMMON.TEST_SOCKET_IO]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
                    view : 'pages/socket.test.ejs',
                },
                methods: {
                    get: [(req, res) => {
                        ChildRouter.renderToView(req, res);
                    }]
                }
            },
            
        }
    }
};
