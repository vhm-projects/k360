const BASE_ROUTE = '/';

const CF_ROUTINGS_COMMON = {
    HOME: `/home`,
    LOGIN: `/login`,
    LOGIN_MOBILE: `/login-mobile`,
    FORGET_PASSWORD: `/recover-password`,
    CHANGE_PASSWORD: `/change-password`,
    UPDATE_PASSWORD_RECOVER: `/update-pass-recover`,
    LOGOUT: `/logout`,
    PAGE_NOT_FOUND: `/page-not-found`,
    LIST_DISTRICTS: `/list-districts/:province`,
    LIST_PROVINCES: `/list-provinces`,
    LIST_WARDS: `/list-wards/:district`,
    CHOOSE_AGENCY: `/choose-agency`,
    ACCOUNT_SETTING: `/account-setting`,
    GENERATE_LINK_S3: `/generate-link-s3`,

    TEST_SOCKET_IO: `/test-socket-io`,
    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_COMMON = CF_ROUTINGS_COMMON;