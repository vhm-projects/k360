"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID  = require('mongoose').Types.ObjectId;
const moment    = require("moment");

/**
 * INTERNAL PACKAGE
 */
const { validEmail } = require('../../../utils/string_utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS, MODELS
 */
const AGENCY_COLL 			= require('../databases/agency-coll')({});
const USER_COLL   			= require('../../permissions/users/databases/user-coll')({});
const USER_AGENCY_COLL      = require('../../customer/database/customer_agency')({});
const { BLACKLIST_COLL } 	= require('../../blacklist');
const { IMAGE_MODEL }       = require('../../image');
const TRANSACTION_COLL      = require('../../transaction/database/transaction-coll')({});

const INJECT_TRANSACTION_MODEL         = require('../../transaction/models/inject_transaction').MODEL;

class Model extends BaseModel {
    constructor() {
        super(AGENCY_COLL);
    }

    /**
     * Thêm đại lý
     * Dattv
     */
    insert({ name, email, phone, address, code, tax_code, city, district, ward, latitude, 
        longitude, openTime, closeTime, parent, userCreate, image_gallery_agency, image_avatar_agency, productPawn }) {
        return new Promise(async resolve => {
            try {
                if(!name || !email || !ObjectID.isValid(userCreate))
                    return resolve({ error: true, message: 'params_invalid' });

                let checkExistedEmail = await AGENCY_COLL.findOne({ email });
                if(checkExistedEmail)
                    return resolve({ error: true, message: 'email_existed' });

                let dataInsert = {
                    name,
                    email,
                    phone,
                    address,
                    code, 
                    tax_code, 
                    city, 
                    district, 
                    ward,
                    openTime,
                    closeTime,
                    userCreate,
                    productPawn,
                    location: {
                        type: "point",
                        coordinates: [latitude, longitude]
                    },
                }

                if(ObjectID.isValid(parent)){
                    dataInsert.parent = parent;
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert_agency' });

                let { _id: agencyID } = infoAfterInsert


                //Thêm vào collect image và thêm vào gallery agency
                if(image_gallery_agency && image_gallery_agency.length){
                    for (let image of image_gallery_agency) {
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: image, path: image });

                        let { _id: imageID } =  infoImageAfterInsert.data;
                        infoAfterInsert = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                            $addToSet: { gallery: imageID }
                        }, { new: true });
                    }
                }

                if(image_avatar_agency && image_avatar_agency.length){
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: image_avatar_agency[0], path: image_avatar_agency[0] });
                    let { _id: imageID } =  infoImageAfterInsert.data;
                    infoAfterInsert = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                        $addToSet: { avatar: imageID }
                    }, { new: true });
                }

                // Nếu có parent thì đẩy vào childs của agency cha
                if(ObjectID.isValid(parent)){
                    await AGENCY_COLL.findByIdAndUpdate(parent, {
                        $addToSet: { childs: infoAfterInsert._id }
                    }, { new: true })
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAgency = await AGENCY_COLL.findById(agencyID)
                .populate({
                    path: "owners employees childs",
                    options: { sort: { createAt: -1 }}
                })
                .populate({ path: 'avatar', select: 'path name' })
                .populate({ path: 'gallery', select: 'path name' });
                
                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Danh sách agency
    getList() {
        return new Promise(async resolve => {
            try {

                let listAgency = await AGENCY_COLL.find();
                
                if(!listAgency) 
                    return resolve({ error: true, message: "get_list_agency" });

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Danh sách agency gần đây
    getListAgencyNear({ lng, lat, maxDistance, minDistance }) {
        return new Promise(async resolve => {
            try {

                if (!lng || !lat)
                    return resolve({ error: true, message: 'params_invalid' });

                let listAgencyNear = await AGENCY_COLL.aggregate([
                    {
                        $geoNear: {
                            near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                            maxDistance: Number(maxDistance),
                            minDistance: Number(minDistance),
                            distanceMultiplier: 0.001, //trả về đơn vị km
                            distanceField: "dist.calculated",
                            includeLocs: "dist.location", // Returns distance
                            spherical: true
                        }
                    }
                ]);

                if(!listAgencyNear) 
                    return resolve({ error: true, message: "get_list_agency" });

                return resolve({ error: false, data: listAgencyNear });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAgencyBranch({ agencyID }) {
        return new Promise(async resolve => {
            try {
                let listAgencyBranch = await AGENCY_COLL.findById({ _id: agencyID })
                    .populate("childs")

                return resolve({ error: false, data: listAgencyBranch.childs });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByEmail({ email }) {
        return new Promise(async resolve => {
            try {

                let infoAgency = await AGENCY_COLL.findOne({ email });

                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    //Danh sách member của agency //Xem lại?
    // getListMemberOfAgency({ agencyID }) {
    //     return new Promise(async resolve => {
    //         try {
    //             if(!ObjectID.isValid(agencyID))
    //                 return resolve({ error: true, message: "param_not_valid" });

    //             let infoAgency = await AGENCY_COLL.findById(agencyID).populate("owners employees");
    //             let { members: listMemberOfAgency} = infoAgency;
                
    //             if(!listMemberOfAgency) 
    //                 return resolve({ error: true, message: "get_list_member_fail" });

    //             return resolve({ error: false, data: listMemberOfAgency });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }

    //Danh sách nhân viên của agency
    getListMemberOfAgency({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "params_invalid" });

                let infoAgency = await AGENCY_COLL
					.findById(agencyID)
					.populate("employees owners")
					.lean();

                let { employees, owners } = infoAgency;
                let listMemberOfAgency = [...owners, ...employees];

				let listBlacklistUserOfAgency = await BLACKLIST_COLL
					.find({ agency: agencyID })
					.select('user')
					.lean();

				listBlacklistUserOfAgency = listBlacklistUserOfAgency.map(blacklist => blacklist.user.toString());

				listMemberOfAgency.map(member => {
					if(listBlacklistUserOfAgency.includes(member._id.toString())){
						member.status = 0;
					}
					return member;
				})

                if(!listMemberOfAgency) 
                    return resolve({ error: true, message: "get_list_employee_fail" });

                return resolve({ error: false, data: listMemberOfAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Xóa agency
     * Dattv
     */
     remove({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterDelete = await AGENCY_COLL.findByIdAndDelete(agencyID);

                //Xóa khỏi child của menu parent nếu có
                let { parent } = infoAfterDelete;

                if(parent){
                    await AGENCY_COLL.findByIdAndUpdate(parent, {
                        $pull: { childs: infoAfterDelete._id }
                    }, {new: true })
                }

                //Xóa tất cả menu con
                await AGENCY_COLL.deleteMany({ parent: agencyID });

                if(!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_remove_agency' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật thông tin agency
     * Dattv
     */
     update({ agencyID, name, funds, openingBalance, email, phone, address, code, tax_code, city, district, ward, productPawn, 
        longitude, latitude, openTime, closeTime, status, userUpdate, avatar, gallery, galleryDelete, percentPawn }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataCheck = { agencyID }
                if (code){
                    dataCheck.code = code
                }
                
                let isExistCode = await AGENCY_COLL.findOne(dataCheck);

                if(isExistCode) {
                    return resolve({ error: true, message: 'Mã đại lý đã tồn tại' });
                }

                let isExistName = await AGENCY_COLL.findOne({ _id: { $ne: agencyID }, name });
                if(isExistName) 
                    return resolve({ error: true, message: 'Tên đại lý đã tồn tại' });

                if( longitude && !(longitude <= 180 && longitude >= -180)) {
                    return resolve({ error: true, message: 'Kinh độ phải lớn hơn hoặc bằng -180 và nhỏ hơn hoặc bằng 180' });
                }

                if( latitude && !(latitude <= 90 && latitude >= -90)) {
                    return resolve({ error: true, message: 'Vĩ độ phải lớn hơn hoặc bằng -90 và nhỏ hơn hoặc bằng 90' });
                }

                let dataUpdate = {
                    name,
                    email,
                    phone,
                    address, 
                    code, 
                    tax_code, 
                    city, 
                    district, 
                    ward,
                    productPawn,
                    location: {
                        type: "point",
                        coordinates: [longitude, latitude]
                    },
                    openTime, 
                    closeTime,
                    status,
					percentPawn,
                    userUpdate
                }

                if( funds ){
                    dataUpdate.funds = funds
                }

                if( openingBalance ){
                    dataUpdate.openingBalance = openingBalance
                }

                let infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, 
                    dataUpdate, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_agency' });

                if(avatar && avatar.length){
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: avatar[0], path: avatar[0] });
                    let { _id: imageID } =  infoImageAfterInsert.data;
                    infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                        avatar: imageID
                    }, { new: true });
                }

                //Thêm vào collect image và thêm vào gallery agency
                if(gallery && gallery.length){
                    for (let image of gallery) {
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: image, path: image });
                        let { _id: imageID } =  infoImageAfterInsert.data;
                        infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                            $addToSet: { gallery: imageID }
                        }, { new: true });
                    }
                }

                //Xóa khỏi collection agency
                if(galleryDelete && galleryDelete.length){
                    for (let image of galleryDelete) {
                        infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                            $pull: { gallery: image.id }
                        }, { new: true });
                    }
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật quỹ agency
     * SONLP
     */
    updateFunds({ agencyID, funds, fundsBank, openingBalance }){
        return new Promise(async resolve => {
            try {
                // console.log({ agencyID, funds, openingBalance });
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataUpdate = { agencyID }

                if( funds || funds == 0 ){
                    dataUpdate.funds = funds
                }

                if( fundsBank || fundsBank == 0 ){
                    dataUpdate.fundsBank = fundsBank
                }

                if( openingBalance ){
                    dataUpdate.openingBalance = openingBalance
                }
                // console.log({ dataUpdate });
                let infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, 
                    dataUpdate, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_agency' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    addOnwerExisted({ email, agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!email && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoUserWithEmail = await USER_COLL.findOne({ email });

                if(!infoUserWithEmail){
                    return resolve({ error: true, message: 'email_not_existed' });
                }

                let { _id: userID, agency, level } = infoUserWithEmail;

                if(level != 1){
                    return resolve({ error: true, message: 'account_is_not_owner' });
                }

                if(agency && agency.length){
                    for (const item of agency) {
                        if(item.toString() == agencyID.toString()){
                            return resolve({ error: true, message: 'email_is_have_agency' });
                        }
                    }
                }

                let infoAgencyUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                    $addToSet: { owners: userID }
                }, { new: true });

                await USER_COLL.findByIdAndUpdate(userID, {
                    $addToSet: { agency: agencyID }
                }, { new: true })

                if(!infoAgencyUpdate){
                    return resolve({ error: true, mesage: "cannot_update" });
                }

                return resolve({ error: false, data: infoAgencyUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    addEmployeeExisted({ email, agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!validEmail(email) && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoUserWithEmail = await USER_COLL.findOne({ email });
                if(!infoUserWithEmail){
                    return resolve({ error: true, message: 'email_not_existed' });
                }

                let { _id: userID, agency, level } = infoUserWithEmail;

                if(level != 2){
                    return resolve({ error: true, message: 'account_is_not_employee' });
                }

                if(agency && agency.length){
                    for (const ID of agency) {
                        if(ID.toString() == agencyID.toString()){
                            return resolve({ error: true, message: 'email_is_have_agency' });
                        }
                    }
                }

                let infoAgencyUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                    $addToSet: { employees: userID }
                }, { new: true });

                if(!infoAgencyUpdate){
                    return resolve({ error: true, mesage: "cannot_update" });
                }

                await USER_COLL.findByIdAndUpdate(userID, {
                    $addToSet: { agency: agencyID }
                });

                return resolve({ error: false, data: infoAgencyUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkExistedAgencyByCode({ code }) {
        return new Promise(async resolve => {
            try {

                let infoAgency = await AGENCY_COLL.findOne({ code });

                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    filterAgency({ agencyID, name, city, district, ward, start, end, sort, status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                // chỉ thao tác trên các agency con của nó và chính nó
				let condition = {
                    $or: [
                        { _id: agencyID },
                        { parent: agencyID }
                    ]
                };
				
                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    condition.name = new RegExp(keyword, 'i');
                }

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                }

                if(start && end) {
                    let _fromDate = moment(start).startOf('day').format();
                    let _toDate   = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                // if(status) {
                //     condition.status = status;
                // }

                let listAgency = await AGENCY_COLL.find({ ...condition })
                    .populate({ path: "owners", select: "username" })
                    .populate({ path: 'parent', select: '_id code name email phone code' })
                    .populate({ path: 'avatar', select: '_id path' })
                    .populate({ path: 'gallery', select: '_id path' })
                    .sort({ createAt: -1 });

                if(!listAgency)
                    return resolve({ error: true, message: "cannot_get_list_agency" });

                /**
                 * Nếu lọc list agency theo điều kiện có agency thỏa điều kiện mới tiếp tục sắp xếp
                 * tránh trường hợp sau khi lọc không agency nào thỏa thì khi group theo giao dịch sẽ trả về
                 * những agency không đúng theo điều kiện lọc
                 */
                if(listAgency.length > 0) {
                    let objCondition = {};

                    let listAgencyID;
                    if(listAgency && listAgency.length) {
                        listAgencyID = listAgency.map(agency => agency._id);
    
                        objCondition.agency = {
                            $in: listAgencyID
                        }
                    }
    
                    /**
                     * Trường hợp của sort:
                     *  + "": Mặc định
                     *  + 1: Doanh số cao nhất
                     *  + 2: Doanh số thấp nhất
                     *  + 3: Khách hàng nhiều nhất
                     *  + 4: Khách hàng ít nhất
                     */
                    let objSort = {};
                    switch(Number(sort)) {
                        case 1: {
                            objSort.total = -1;
                            break;
                        }
                        case 2: {
                            objSort.total = 1;
                            break;
                        }
                        case 3: {
                            objSort.numOfCustomer = -1;
                            break;
                        }
                        case 4: {
                            objSort.numOfCustomer = 1;
                            break;
                        }
                        default: {
                            objSort = { total: -1 };
                        }
                    }
    
                    let listAgencyWithSort;
                    if(sort){
                        listAgencyWithSort = await TRANSACTION_COLL.aggregate([
                            {
                                $match: objCondition
                            },
                            {
                                $group : { 
                                    _id : "$agency",
                                    count: { $sum: 1 },
                                    total: { $sum: "$loanAmount" },
                                    customer: { $addToSet: "$customer" },
                                    agency : { $first: '$agency' }
                                }
                            },
                            {
                                $lookup: {
                                    from: "agencies",
                                    localField: "agency",
                                    foreignField: "_id",
                                    as: "agency"
                                }
                            },
                            {
                                $unwind: "$agency"
                            },
                            {
                                $addFields: {
                                    numOfCustomer: {
                                        $size: "$customer"
                                    }
                                }
                            },
                            {
                                $sort: objSort
                            }
                        ]);
    
                        /**
                         * listAgencyWithSort: 
                         * 
                         * [{
                            _id: 60b0660da706192905f6d382,
                            count: 26, // tổng số giao dịch
                            total: 307077777, //tổng doanh thu
                            customer: [Array],
                            agency: [Object],
                            numOfCustomer: 4 // tống số lượng khách hàng
                            },]
                         */
                        let arrIDAgencyWithSort = listAgencyWithSort.map(item => ObjectID(item.agency._id));

                        /**
                         * b1: lấy danh sách AGENCY theo listAgencyWithSort -> lấy danh sách agency có chứa thông tin tài chính (1)
                         * b2: tìm những agency ko chứa trong listAgencyWithSort -> merge vào array agency ở bước (1)
                         */
                        // let listAgencyWithSortTotalAmountTransaction = await AGENCY_COLL.find({
                        //     _id: {
                        //         $in: [...arrIDAgencyWithSort]
                        //     }
                        // });

                        let listAgencyWithoutAmountTransction = await AGENCY_COLL.find({
                            _id: {
                                $nin: [...arrIDAgencyWithSort]
                            }
                        }).populate({ path: 'avatar', select: '_id path' }).sort({ createAt: -1 });

                        switch(Number(sort)) {
                            case 1 || 3: {
                                listAgency = [
                                    ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                ]
                                break;
                            }
                            case 2 || 4: {
                                listAgency = [
                                    ...listAgencyWithoutAmountTransction, ...listAgencyWithSort.map(item => item.agency),
                                ]
                                break;
                            }
                            default: {
                                listAgency = [
                                    ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                ]
                            }
                        }
                        // let arrIDAgencyWithSort = listAgencyWithSort.map(agency => agency._id.toString());
    
                        // /**
                        //  * lặp qua listAgency ban đầu để gửi vào thêm array những phần từ không có giao dịch
                        //  * Thêm điều kiện && objSort.total để biết chiều khi bỏ phần từ vào mảng cho hợp lí
                        //  */
                        // listAgency.forEach(agency => {
                        //     if(!arrIDAgencyWithSort.includes(agency._id.toString()) && (objSort.total === 1 || objSort.numOfCustomer === 1)) {
                        //         listAgencyWithSort.unshift(agency);
                        //     } else if(!arrIDAgencyWithSort.includes(agency._id.toString()) && (objSort.total === -1 || objSort.numOfCustomer === -1)) {
                        //         listAgencyWithSort.push(agency);
                        //     }
                        // })
                    }
                    
                    let STATUS_ACCEPT_TRANSACTION = [1, 3];
                    let listAgencyBestSale = await TRANSACTION_COLL.aggregate([
                        {
                            $match: { status: { $in: STATUS_ACCEPT_TRANSACTION } }
                        },
                        {
                            $group : { 
                                _id : "$agency",
                                count: { $sum: 1 },
                                total: { $sum: "$loanAmount" },
                                agency : { $first: '$agency' }
                            },
                        },
                        {
                            $lookup: {
                                from: "agencies",
                                localField: "agency",
                                foreignField: "_id",
                                as: "agency"
                            }
                        },
                        {
                            $unwind: "$agency"
                        },
                    ]);
    
                    let listTopAgencyHaveMostCustomer = await TRANSACTION_COLL.aggregate([
                        {
                            $group : { 
                                _id : "$agency",
                                customer: { $addToSet: "$customer" },
                                agency : { $first: '$agency' },
                            }
                        },
                        {
                            $lookup: {
                                from: "agencies",
                                localField: "agency",
                                foreignField: "_id",
                                as: "agency"
                            }
                        },
                        {
                            $unwind: "$agency"
                        },
                        {
                            $addFields: {
                                numOfCustomer: {
                                    $size: "$customer"
                                }
                            }
                        },
                    ]);
                    
                    // if(sort) {
                    //     listAgency = listAgencyWithSort;
                    // }
                    
                    return resolve({ error: false, data: { listAgency, listAgencyBestSale, listTopAgencyHaveMostCustomer } });
                } else {
                    return resolve({ error: false, data: { listAgency, listAgencyBestSale: [], listTopAgencyHaveMostCustomer: [] } });
                }
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    listAgencyShowAdmin({ agencyID, name, city, district, ward, fromDay, toDay }){
        return new Promise(async resolve => {
            try {
            
                let DAY_PARSE = 1000 * 60 * 60 * 24;

                let condition = {
                    $or: [
                        { _id: agencyID },
                        { parent: agencyID }
                    ]
                };
				
                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    condition.name = new RegExp(keyword, 'i');
                }

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                }

                let listAgency = await AGENCY_COLL.find({
                        ...condition, status: 1
                    })
                    .select("_id name code parent")
                    .populate({ path: 'parent', select: 'code'});

                let dataResult = [];
                let createAt = {}
                if (fromDay) {
                    let start = new Date(fromDay);
                    start   = moment(start).startOf('day').format();
                    createAt = { ...createAt, $gte: new Date(start) }
                }
                if (toDay) {
                    let end = new Date(toDay);
                    end = moment(end).endOf('day').format();
                    createAt = { ...createAt, $lte: new Date(end) }
                }
                for (const agency of listAgency) {
                    let { _id, name, code, parent } = agency;
                    const STATUS_ACTIVE_TRANSACTION        = 1;
                    const STATUS_INACTIVE_TRANSACTION      = 0;
                    const STATUS_NOT_APPROVE_TRANSACTION   = 2;

                    let agencyBase = {
                        _id,
                        name,
                        code,
                        parent: parent && parent.code
                    };
                   
                    //Lấy số lượng giao dịch cầm đồ của Đại lý đó
                    let objAmountTransactionPawn = {
                        agency: _id, status:  STATUS_ACTIVE_TRANSACTION
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objAmountTransactionPawn.createAt = createAt;
                    }
                    /**
                     * SỐ LƯỢNG GIAO DỊCH ĐNAG CẦM
                     */
                    let amountTransactionPawn = await TRANSACTION_COLL.count({ ...objAmountTransactionPawn });
                    agencyBase.amountPawn = amountTransactionPawn;  
                    //Lấy tổng tiền giao dịch cầm đồ của Đại Lý
                    let objMatchTotalMoney = {
                        $match: {agency: ObjectID(_id), status: STATUS_ACTIVE_TRANSACTION }
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objMatchTotalMoney = { 
                            $match: {agency: ObjectID(_id), status: STATUS_ACTIVE_TRANSACTION, createAt }
                        }
                    }
                    /**
                     * TỔNG TIỀN VAY GIAO DỊCH ĐANG CẦM
                     */
                    let totalMoneyOfAgency = await TRANSACTION_COLL.aggregate([
                        { 
                            ...objMatchTotalMoney
                        },
                        { 
                            $group: {
                                _id: null,
                                total: { $sum: "$loanAmount" }
                            }
                        }
                    ])
                    let originalAmt = totalMoneyOfAgency.length && totalMoneyOfAgency[0].total || 0
                    agencyBase.originalAmt = originalAmt;

                    //Lấy số lượng giao dịch chuộc đồ của Đại lý đó
                    const STATUS_COMPLETE_TRANSACTION = 3;
                    let objRedeem = {
                        agency: _id, status: STATUS_COMPLETE_TRANSACTION
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objRedeem.createAt = createAt;
                    }
                    /**
                     * SỐ LƯỢNG GIAO DỊCH ĐÃ CHUỘC
                     */
                    let redeem = await TRANSACTION_COLL.count({ ...objRedeem });
                    agencyBase.redeem = redeem;

                    /**
                     * SỐ LƯỢNG KHÁCH HÀNG
                     */
                    let objCustomer = {
                        agency: ObjectID(_id)
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objCustomer = { 
                            ...objCustomer,
                            createAt
                        }
                    }

                    let listCustomer = await USER_AGENCY_COLL.find({ 
                        ...objCustomer 
                    });
                    let listCustomerID = listCustomer.map( customer => customer.customer.toString() );
                    let totalCustomer = [...new Set(listCustomerID)].length;

                    // let totalCustomer = await TRANSACTION_COLL.aggregate([
                    //     {
                    //         ...objCustomer
                    //     },
                    //     {
                            
                    //         $group: { _id: "$customer", count: { $sum: 1 }},
                            
                    //     }
                    // ])
                    agencyBase.totalCustomer = totalCustomer;
                        
                    //     $group: { _id: "$customer", count: { $sum: 1 }},
                        
                    // }

                    //Lấy tổng tiền giao dịch chuộc đồ của Đại lý đó
                    let objMatchTotalRedeem = {
                        $match: { agency: ObjectID(_id), status: STATUS_COMPLETE_TRANSACTION }
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objMatchTotalRedeem = { 
                            $match :{ agency: ObjectID(_id), status: STATUS_COMPLETE_TRANSACTION, createAt }
                        }
                    }
                    /**
                     * TỔNG TIỀN ĐÃ CHUỘC
                     */
                    let totalMoneyRedeemOfAgency = await TRANSACTION_COLL.aggregate([
                        { 
                            ...objMatchTotalRedeem
                        },
                        {
                            $lookup : {
                                from: "inject_transactions",
                                localField: "latestInjectTransaction",
                                foreignField: "_id",
                                as: "latestInjectTransaction"
                            }
                        },
                        {
                            $unwind: "$latestInjectTransaction"
                        },
                        { 
                            $group: {
                                _id: null,
                                total: { $sum: "$latestInjectTransaction.meta.priceRansomPay" }
                            }
                        }
                    ])

                    let redeemAmt = totalMoneyRedeemOfAgency.length && totalMoneyRedeemOfAgency[0].total || 0
                    agencyBase.redeemAmt = redeemAmt;

                    //=============================== LÃI ===============================//
                    let objInterestOfAgency = {
                        $match: { agency: ObjectID(_id), status: 1 }
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objInterestOfAgency = { 
                            $match: { agency: ObjectID(_id), status: 1, createAt }
                        }
                    }
                    let totalInterestOfAgency = await TRANSACTION_COLL.aggregate([
                        {
                            ...objInterestOfAgency
                        },
                        // {
                        //     $project: { 
                        //         'customer': 1, 
                        //         'loanAmount': 1, 
                        //         'code': 1, 
                        //         'status': 1, 
                        //         'expireTime': 1, 
                        //         'latestInjectTransaction': 1,
                        //         'interestStartDate': 1, 
                        //         'products': 1,
                        //         'dateDifference': {
                        //                 $divide: [{ $subtract: [new Date(), '$interestStartDate'] }, DAY_PARSE]
                        //         }, 
                        //     }
                        // }, 
                        // {
                        //     $lookup: {
                        //         from: "products",
                        //         localField: "products",
                        //         foreignField: "_id",
                        //         as: "products"
                        //     }
                        // },
                    ]);

                    let arr = []
                    let totalInterestAmt = 0;
                    let totalInterest    = 0;
                    //const TYPE_TRANSACTION_NORMMAL    = 2;
                    for (let interest of totalInterestOfAgency) {
                        let { data: totalPriceNormal } = await INJECT_TRANSACTION_MODEL.calculateTotalInterest({ transactionID: interest._id });

                        totalInterestAmt += totalPriceNormal;
                        // 1// Lay thong tin lai, phan tram, so tien
                        // if(interest.dateDifference >= 0) {
                        //     interest.dateDifference     = Math.floor(interest.dateDifference) + 1;
                        // }
                        
                        // if (interest.dateDifference < 0) {
                        //     interest.dateDifference = 0;
                        // }
                        // let infoInterestAfterCall = await INTEREST_MODEL.checkDateToInterest({ agencyID: _id, limitDate: interest.dateDifference, price: interest.loanAmount, typeProduct: interest.products[0].type });
                        // let obj = interest
                        // let { period, datePeriod  } = infoInterestAfterCall;
                        // obj.percent = infoInterestAfterCall.percent;
                        // let totalPrice   = 0;
                        // infoInterestAfterCall.data.forEach( price => {
                        //     totalPrice   += price
                        // });

                        // obj.price = totalPrice;
                        // arr.push(obj)
                        // totalInterestAmt+=totalPrice;
                        // totalInterest += period + datePeriod/30;
                    }
                    agencyBase.totalInterest = totalInterest;
                    agencyBase.totalInterestAmt = totalInterestAmt;
                    // console.log({ agencyBase });
                    dataResult.push(agencyBase);
                }

                // console.log({ dataResult });

                return resolve({ error: false, data: dataResult });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;