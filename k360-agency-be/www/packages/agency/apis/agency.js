"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');
const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const roles                     = require('../../../config/cf_role');
const ChildRouter               = require('../../../routing/child_routing');
const { CF_ROUTINGS_AGENCY }    = require('../constants/agency.uri');
const { uploadSingle }          = require('../../../config/cf_helpers_multer');
const AGENCY_SESSION		    = require('../../../session/agency-session');
const { provinces }            = require('../../common/constant/provinces');
const { districts }            = require('../../common/constant/districts');


/**
 * MODELS, COLLECTIONS
 */
const AGENCY_MODEL             = require("../models/agency").MODEL;
const { BLACKLIST_COLL } 	   = require('../../blacklist');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            /**
             * Function: Danh sách agency (View)
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY,
					inc: path.resolve(__dirname, '../views/list_agency.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listAgency = await AGENCY_MODEL.getList();
                        ChildRouter.renderToView(req, res, {
							listAgency: listAgency.data,
						});
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.LIST_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_AGENCY.LIST_AGENCY,
					inc: path.resolve(__dirname, '../views/list_agency.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						let { name, city, district, ward, start, end, sort } = req.query;
                        let agencyID   = AGENCY_SESSION.getContextAgency(req.session).agencyID;

                        let listAgency          = await AGENCY_MODEL.filterAgency({ agencyID, name, city, district, ward, start, end, sort });
                        let listAgencyShowAdmin = await AGENCY_MODEL.listAgencyShowAdmin({ agencyID, name, city, district, ward, fromDay: start, toDay: end });
                        if (listAgency.data.listAgency && listAgency.data.listAgency.length) {
                            listAgency.data.listAgency.forEach((agency, index) => {
                                if (listAgencyShowAdmin.data && listAgencyShowAdmin.data .length) {
                                    listAgencyShowAdmin.data.forEach( agencyCalculate => {
                                        if (agency._id.toString() == agencyCalculate._id.toString()) {
                                            /**
                                             *  agencyCalculate: {
                                                    _id: 60a72d01bfd65f6f1d155263,
                                                    name: 'Test Đại lý',
                                                    code: 'TDL',
                                                    parent: undefined,
                                                    amountPawn: 0,
                                                    originalAmt: 0,
                                                    redeem: 0,
                                                    redeemAmt: 0,
                                                    totalInterest: 0,
                                                    totalInterestAmt: 0
                                                }
                                             */
                                            let objAgency = {
                                                _id:       agency._id,
                                                childs:    agency.childs,
                                                owners:    agency.owners,
                                                employees: agency.employees,
                                                gallery:   agency.gallery,
                                                openingBalance: agency.openingBalance,
                                                funds:          agency.funds,
                                                status:         agency.status,
                                                name:        agency.name,
                                                email:       agency.email,
                                                phone:       agency.phone,
                                                address:    agency.address,
                                                code:       agency.code,
                                                tax_code:   agency.tax_code,
                                                ward:       agency.ward,
                                                openTime:   agency.openTime,
                                                closeTime:   agency.closeTime,
                                                userCreate:  agency.userCreate,
                                                location:   agency.location,
                                                parent:     agency.parent,
                                                modifyAt:   agency.modifyAt,
                                                createAt:   agency.createAt,
                                                userUpdate: agency.userUpdate,
                                                avatar:     agency.avatar,
                                            }
                                            listAgency.data.listAgency[index] = {
                                                ...objAgency,
                                                amountPawn:  agencyCalculate.amountPawn,
                                                originalAmt: agencyCalculate.originalAmt,
                                                redeem:      agencyCalculate.redeem,
                                                redeemAmt:   agencyCalculate.redeemAmt,
                                                totalInterestAmt:   agencyCalculate.totalInterestAmt,
                                                totalCustomer:   agencyCalculate.totalCustomer,
                                            }
                                        }
                                    })
                                }
                            });
                        }
                        let listProvince = Object.entries(provinces);   
                        ChildRouter.renderToView(req, res, {
                            listAgency: listAgency.data.listAgency,
                            listAgencyBestSale: listAgency.data.listAgencyBestSale,
                            listTopAgencyHaveMostCustomer: listAgency.data.listTopAgencyHaveMostCustomer,
                            listProvince,
                            city, district, ward, name, sort, start, end,
                        });
                    }]
                },
            },

            /**
             * Function: Danh sách chi nhánh agency (View)
             * Date: 24/05/2021
             * Dev: VyPQ
             */
             [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY_BRANCH]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'view',
					title: `List Agency Branch - K360`,
					code: CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY_BRANCH,
					inc: path.resolve(__dirname, '../views/list_agency_branch.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;

                        let listAgencyBranch = await AGENCY_MODEL.getListAgencyBranch({ agencyID });
                        let listProvince = Object.entries(provinces);

                        ChildRouter.renderToView(req, res, {
							listAgencyBranch: listAgencyBranch.data,
                            listProvince
						});
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.VIEW_DETAIL_AGENCY_BRANCH]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `Detail Agency Branch - K360`,
					code: CF_ROUTINGS_AGENCY.VIEW_DETAIL_AGENCY_BRANCH,
					inc: path.resolve(__dirname, '../views/detail_agency_branch.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let { _id: userID } = req.user;

                        let infoAgency = await AGENCY_MODEL.getInfo({ agencyID });
						let listBlacklistUserOfAgency = await BLACKLIST_COLL
							.find({ agency: agencyID })
							.select('user')
							.lean();

						listBlacklistUserOfAgency = listBlacklistUserOfAgency.map(blacklist => blacklist.user.toString());

                        let arrOwnerOfAgency = infoAgency.data && infoAgency.data.owners.map(owner => owner._id);
                        let listProvince = Object.entries(provinces);
                        let listDistricts = Object.entries(districts);
                        let listWards;
                        let infoProvince;
                        let infoWard;
                        let infoDistrict;

                        for (let province of listProvince){
                            if ( province[1].code == infoAgency.data.city ){
                                infoProvince = province[1];
                                break;
                            }
                        }

                        for (let district of listDistricts){
                            if ( district[1].code == infoAgency.data.district ){
                                infoDistrict = district[1];
                                break;
                            }
                        }

                        if(!infoDistrict || !infoDistrict.code)
                            return ChildRouter.renderToView(req, res, {
                                infoAgency: infoAgency.data,
                                listProvince,
                                infoDistrict,
                                infoProvince,
                                infoWard: null,
                                userID,
                                arrOwnerOfAgency,
								listBlacklistUserOfAgency
                            });

                        let filePath = path.resolve(__dirname, `../../common/constant/wards/${infoDistrict.code}.json`);

                        await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                listWards = JSON.parse(data);
                                listWards = Object.entries(listWards);
                                for (let ward of listWards){
                                    if ( ward[1].code == infoAgency.data.ward ){
                                        infoWard = ward[1];
                                        break;
                                    }
                                }

                                ChildRouter.renderToView(req, res, {
                                    infoAgency: infoAgency.data,
                                    listProvince,
                                    infoDistrict,
                                    infoProvince,
                                    infoWard,
                                    userID,
                                    arrOwnerOfAgency,
									listBlacklistUserOfAgency
                                });

                            }
                        });
                    }]
                },
            },

            /**
             * Function: Danh sách agency (View)
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.VIEW_DETAIL_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `Detail Agency - K360`,
					code: CF_ROUTINGS_AGENCY.VIEW_DETAIL_AGENCY,
					inc: path.resolve(__dirname, '../views/detail_agency.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let infoAgency = await AGENCY_MODEL.getInfo({ agencyID });

                        ChildRouter.renderToView(req, res, {
							infoAgency: infoAgency.data
						});
                    }]
                },
            },

            /**
             * Danh sách nhân viên của Đại lý
             * Dev: Dattv
             * 07/05/2021
             */
             [CF_ROUTINGS_AGENCY.VIEW_LIST_EMPLOYEE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_AGENCY.VIEW_LIST_EMPLOYEE,
					inc: path.resolve(__dirname, '../views/list_employee.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listAgencyOfUser  = req.user.agency;
                        let agencyID          = req.agencyID;
                        let agencySession     = AGENCY_SESSION.getContextAgency(req.session);
                        let infoAgency        = await AGENCY_MODEL.getInfo({ agencyID });

                        if(!agencySession && listAgencyOfUser && listAgencyOfUser.length === 1){
                            agencyID = listAgencyOfUser[0]._id;
                            AGENCY_SESSION.saveContextAgency(req.session, { agencyID });
                        }

                        let listEmployee = await AGENCY_MODEL.getListMemberOfAgency({ agencyID });

                        ChildRouter.renderToView(req, res, {
                            listEmployee: listEmployee.data,
                            infoAgency: infoAgency.data
                        });
                    }]
                },
            },

            //========================= JSON ============================

            /**
             * Function: Info Agency
             * Date: 16/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.INFO_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let infoAgency = await AGENCY_MODEL.getInfo({ agencyID });
                        if(infoAgency.error) {
                            return res.json(infoAgency);
                        }
                        let listProvince        = Object.entries(provinces);
                        let listDistricts       = Object.entries(districts);

                        let city;
                        if(infoAgency.data.city) {
                            for(let province of listProvince) {
                                if(province[1].code == infoAgency.data.city) {
                                    city = province[1].name_with_type;
                                    break;
                                }
                            }
                        } 

                        let districtName;
                        let infoDistrict;
                        if(infoAgency.data.district) {
                            for(let district of listDistricts) {
                                if(district[1].code == infoAgency.data.district ){
                                    districtName = district[1].name_with_type;
                                    infoDistrict = district[1];
                                    break;
                                }
                            }
                        }

                        let objAgency = {
                            _id:       infoAgency.data._id,
                            childs:    infoAgency.data.childs,
                            owners:    infoAgency.data.owners,
                            employees: infoAgency.data.employees,
                            gallery:   infoAgency.data.gallery,
                            openingBalance: infoAgency.data.openingBalance,
                            funds: infoAgency.data.funds,
                            status: infoAgency.data.status,
                            name:        infoAgency.data.name,
                            email:       infoAgency.data.email,
                            phone:       infoAgency.data.phone,
                            address:    infoAgency.data.address,
                            code:       infoAgency.data.code,
                            tax_code:   infoAgency.data.tax_code,
                            // ward:       infoAgency.data.ward,
                            openTime:   infoAgency.data.openTime,
                            closeTime:   infoAgency.data.closeTime,
                            userCreate:  infoAgency.data.userCreate,
                            location:   infoAgency.data.location,
                            parent:     infoAgency.data.parent,
                            modifyAt:   infoAgency.data.modifyAt,
                            createAt:   infoAgency.data.createAt,
                            userUpdate: infoAgency.data.userUpdate,
                            avatar:     infoAgency.data.avatar,
                            city:       city,
                            district:       districtName,
                            // cityName:       city,
                            // districtName:   districtName,
                        }
                        let description = { status: "Trạng thái hoạt động (1: Đang hoạt động, 0: Khóa)" }
                        let filePath = path.resolve(__dirname, `../../common/constant/wards/${infoDistrict.code}.json`);

                        await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                let listWards = JSON.parse(data);
                                listWards = Object.entries(listWards);
                                for (let ward of listWards){
                                    if ( ward[1].code == infoAgency.data.ward ) {
                                        objAgency = {
                                            ...objAgency,
                                            ward:  ward[1].name_with_type,
                                        }
                                        break;
                                    }
                                }
                                return res.json({ description, error: false, data: objAgency });
                            } 
                        });
                        // res.json(infoAgency);
                    }]
                },
            },

            /**
             * Function: Danh sách member thuộc agency
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.LIST_MEMBER_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID } = req.params;
                        const listMemberOfAgency = await AGENCY_MODEL.getListMemberOfAgency({ agencyID });
                        res.json(listMemberOfAgency);
                    }]
                },
            },

            /**
             * Function: Danh sách agency gần đây
             * Date: 18/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.LIST_AGENCY_NEAR]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { lng, lat, maxDistance, minDistance } = req.query;
                        const listAgencyNear = await AGENCY_MODEL.getListAgencyNear({ lng, lat, maxDistance, minDistance });
                        res.json(listAgencyNear);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.CHECK_EXISTED_EMAIL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email } = req.body;
                        const infoAgency = await AGENCY_MODEL.getInfoByEmail({ email });
                        res.json(infoAgency);
                    }]
                },
            },

            /**
             * Function: Insert Agency
             * Date: 16/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.ADD_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userCreate, level } = req.user;
                        const agencyID = req.agencyID;

                        const { name, email, phone, code, tax_code, address, city, district, ward, latitude, longitude, openTime, closeTime, parentAgencyBranch, gallery, avatar, productPawn} = req.body;

                        let dataInsert = {
                            name, email, phone, code, tax_code, address, city, district, ward, latitude, longitude, openTime, 
                            closeTime,  userCreate, gallery, avatar, productPawn
                        }

                        // nếu đang xem chi tiết chi nhánh con và tạo chi nhánh con cho nó thì cần lấy ID chi nhánh con đó làm parent
                        if(parentAgencyBranch) {
                            dataInsert.parent = parentAgencyBranch;
                        } else {
                            dataInsert.parent = agencyID;
                        }

                        if(avatar){
                            dataInsert.image_avatar_agency = JSON.parse(avatar);
                        }

                        if(gallery){
                            dataInsert.image_gallery_agency = JSON.parse(gallery);
                        }

                        if(Number(level) == 0 || Number(level) == 1){
                            const infoAfterInsertAgency = await AGENCY_MODEL.insert(dataInsert);
                            res.json(infoAfterInsertAgency);
                        }else{
                            res.json({ error: true, message: "employee_cannot_access"});
                        }
                        
                    }]
                },
            },

            /**
             * Function: Update agency
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.UPDATE_AGENCY]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate }   = req.user;
                        const { agencyID }          = req.params;
                       
                        const { name, funds, openingBalance, email, phone, address, code, tax_code, city, district, ward, 
                            productPawn, latitude, longitude, openTime, closeTime, status, avatar, gallery, galleryDelete, percentPawn } = req.body;
                        let dataUpdate = { 
                            agencyID, name, funds, openingBalance, email, phone, address, code, tax_code, city, district, ward, productPawn, latitude, percentPawn,
                            longitude, openTime, closeTime, status, userUpdate
                        }
                        if(avatar){
                            dataUpdate.avatar = JSON.parse(avatar)
                        }
                        if(gallery){
                            dataUpdate.gallery = JSON.parse(gallery)
                        }
                        if(galleryDelete){
                            dataUpdate.galleryDelete = JSON.parse(galleryDelete)
                        }
                        const infoAgencyUpdate = await AGENCY_MODEL.update(dataUpdate);
                        res.json(infoAgencyUpdate);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.ADD_OWNER_EXISTED]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { email, agencyID } = req.body;
                        const infoAgencyUpdate = await AGENCY_MODEL.addOnwerExisted({ email, agencyID });
                        res.json(infoAgencyUpdate);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.ADD_EMPLOYEE_EXISTED]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { email, agencyID } = req.body;

                        const infoAgencyUpdate = await AGENCY_MODEL.addEmployeeExisted({ email, agencyID });
                        res.json(infoAgencyUpdate);
                    }]
                },
            },

			[CF_ROUTINGS_AGENCY.CHECK_EXISTED_CODE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { code } = req.body;
                        const infoAgency = await AGENCY_MODEL.checkExistedAgencyByCode({ code });
                        res.json(infoAgency);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY_MOBILE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
                        const agencyID = req.agencyID;
						let { name, city, fromDay, toDay, district, ward, start, end, sort } = req.query;
                        let listAgency          = await AGENCY_MODEL.filterAgency({ agencyID, name, city, district, ward, start, end, sort });
                        let listAgencyShowAdmin = await AGENCY_MODEL.listAgencyShowAdmin({ agencyID, name, city, district, ward, fromDay, toDay });

                        let listProvince        = Object.entries(provinces);
                        let listAgencyChange            = [];
                        let listDistricts       = Object.entries(districts);
                        // 1// Chạy từng phần tử trong mảng Danh sách Agency
                        if(listAgency.error == false && listAgency.data.listAgency.length) {
                            listAgency.data.listAgency.forEach(agency => {
                                let nameProvince;
                                let objAgency = {};
                                // 2// Lấy tên thành phố
                                if(agency.city) {
                                    for(let province of listProvince) {
                                        if(province[1].code == agency.city) {
                                            nameProvince = province[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                                // 3// Lấy field trong của agency đó
                                objAgency = {
                                    _id:       agency._id,
                                    childs:    agency.childs,
                                    owners:    agency.owners,
                                    employees: agency.employees,
                                    gallery:   agency.gallery,
                                    openingBalance: agency.openingBalance,
                                    funds: agency.funds,
                                    status: agency.status,
                                    name:        agency.name,
                                    email:       agency.email,
                                    phone:       agency.phone,
                                    address:    agency.address,
                                    code:       agency.code,
                                    tax_code:   agency.tax_code,
                                    ward:       agency.ward,
                                    openTime:   agency.openTime,
                                    closeTime:   agency.closeTime,
                                    userCreate:  agency.userCreate,
                                    location:   agency.location,
                                    parent:     agency.parent,
                                    modifyAt:   agency.modifyAt,
                                    createAt:   agency.createAt,
                                    userUpdate: agency.userUpdate,
                                    avatar:     agency.avatar,
                                    city:       nameProvince
                                }
                                // 4// Lấy quận/ Huyện
                                if(agency.district) {
                                    for(let district of listDistricts) {
                                        if(district[1].code == agency.district ){
                                            objAgency.district = district[1].name_with_type;
                                            break;
                                        }
                                    }
                                }

                                // 5// Push vào mảng mới
                                listAgencyChange.push(objAgency)
                            })
                        }

                        let data = {
                            listAgency: listAgencyChange
                        }

                        let description = { statusAgency: "Trạng thái hoạt động (1: Hoạt động; 0: Khóa)" };

                        // res.json({ description, ...listAgency, data, listAgencyShowAdmin });
                        res.json({ description, error:false, data, listAgencyShowAdmin });
                    }]
                },
            },

        }

    }
};
