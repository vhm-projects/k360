const BASE_ROUTE = '/agency';

const CF_ROUTINGS_AGENCY = {
    //========== VIEW ==========//
    VIEW_LIST_AGENCY: `${BASE_ROUTE}/list-agency`,
    LIST_AGENCY: `${BASE_ROUTE}`,
    VIEW_LIST_AGENCY_BRANCH: `${BASE_ROUTE}/list-agency-branch/:agencyID`, 
    VIEW_DETAIL_AGENCY_BRANCH: `${BASE_ROUTE}/detail-agency-branch/:agencyID`,
    VIEW_DETAIL_AGENCY: `${BASE_ROUTE}/detail/:agencyID`,
    VIEW_ADD_AGENCY: `${BASE_ROUTE}/view-add-agency`,
    VIEW_LIST_EMPLOYEE: `${BASE_ROUTE}/view-list-employee`,
    //========== JSON ==========//
    ADD_AGENCY: `${BASE_ROUTE}/add-agency`,
    INFO_AGENCY: `${BASE_ROUTE}/info-agency/:agencyID`,
    LIST_MEMBER_AGENCY: `${BASE_ROUTE}/list-member/:agencyID`,
    LIST_AGENCY_NEAR: `${BASE_ROUTE}/list-agency-near`,
    REMOVE_AGENCY: `${BASE_ROUTE}/remove-agency/:agencyID`,
    UPDATE_AGENCY: `${BASE_ROUTE}/update-agency/:agencyID`,
    CHECK_EXISTED_EMAIL: `${BASE_ROUTE}/check-email-existed`,
    CHECK_EXISTED_CODE: `${BASE_ROUTE}/check-code-existed`,
    ADD_OWNER_EXISTED: `${BASE_ROUTE}/add-owner-existed`,
    ADD_EMPLOYEE_EXISTED: `${BASE_ROUTE}/add-employee-existed`,

    VIEW_LIST_AGENCY_MOBILE: `/api${BASE_ROUTE}/list-agency`,


	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_AGENCY = CF_ROUTINGS_AGENCY;
