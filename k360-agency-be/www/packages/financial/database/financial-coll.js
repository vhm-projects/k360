"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION SẢN PHẨM GIAO DỊCH CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'financial' }){
   return BASE_COLl(name, {
        /**
         *  Tên chức năng thu/chi
         */

        code: {
            type: Number,
            unique : true,
        },

        title: String, 
        /**
         *  Đại lý
         */
        agency: {
            type: Schema.Types.ObjectId,
            ref: "agency"
        },
        /**
         * Loại:
         * 1. Thu
         * 2. Chi
         */
        type: {
            type: Number,
            default: 1
        },
        /**
         * Loại:
         * 1. Tự động
         * 2. Phát sinh
         */
        typeFinancial: {
            type: Number,
            default: 1
        },
        /**
         * Loại quỹ lưu:
         * 1. Tiền mặt
         * 2. Ngân hàng
         */
        status: {
            type: Number,
            default: 1
        },
        /**
         * Loại giao dịch
         * 0: Phát sinh
         * Từ 1 -> xem ở file cf_constant.js
         */
         typeTransaction: {
            type: Number,
            default: 0
        },
        /**
         *  Đơn giá
         */
        price: {
            type: Number,
            default: 0
        },
        /**
         *  Quỹ trước khi thay đổi
         */
        fundsBeforeChange: {
            type: Number,
            default: 0
        },
        /**
         *  Quỹ sau khi thay đổi
         */
        fundsAfterChange: {
            type: Number,
            default: 0
        },
        /**
         * Ngày thu/ chi
         */
        date: {
            type: Date,
            default: new Date()
        },
        note: String,
        /**
         *  Giao dịch nào
         *  Vừa transaction-coll
         *  Vừa inject-transaction
         */
        transaction: {
            type: Schema.Types.ObjectId,
            refPath: 'onModel'
        },
        onModel: {
            type: String,
            enum: ['transaction', 'inject_transaction']
        },

        ...fields
    });
}
