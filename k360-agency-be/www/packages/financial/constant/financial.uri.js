const BASE_ROUTE_ADMIN = '/financial';

const CF_ROUTINGS_FINANCIAL = {
    // financial
    ADD_FINANCIAL: `${BASE_ROUTE_ADMIN}/add-financial`,
    LIST_FINANCIAL: `${BASE_ROUTE_ADMIN}/list-financials`,
    LIST_FINANCIAL_BANK: `${BASE_ROUTE_ADMIN}/list-financial-bank`,
    LIST_FINANCIAL_BANK_API: `/api${BASE_ROUTE_ADMIN}/list-financial-bank`,
    LIST_FINANCIAL_API: `/api${BASE_ROUTE_ADMIN}/list`,
    // ADD_FINANCIAL: `${BASE_ROUTE_ADMIN}/add-financial`,
    
}

exports.CF_ROUTINGS_FINANCIAL = CF_ROUTINGS_FINANCIAL;
