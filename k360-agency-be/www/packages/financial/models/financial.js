"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
let moment = require("moment");

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */
 const AGENCY_MODEL = require('../../agency/models/agency').MODEL;

/**
 * COLLECTIONS
 */
const FINANCIAL_COLL                = require('../database/financial-coll')({});

const { checkObjectIDs }            = require('../../../utils/utils');

class Model extends BaseModel {
    constructor() {
        super(FINANCIAL_COLL);
    }

	insert({ title, agency, type = 2, email, statusFinancial = 1,  price, funds, onModel, date, note, transaction, typeTransaction, typeFinancial = 1 }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(agency) || !title )
                    return resolve({ error: true, message: 'params_invalid' });
                let dataInsert = {
                    title, agency, type, typeFinancial, funds, onModel
                }
                if(price || price == 0) { // Number.isNaN(Number)
                    dataInsert.price = price;
                } else {
                 return resolve({ error: true, message: 'price_invalid' });
                }

                // console.log(`----------3-------------`)
                // console.log({
                //     typeTransaction
                // })
                
                if ( typeTransaction ){  // Number.isNaN(Number)
                    dataInsert.typeTransaction = typeTransaction;
                }

                // console.log(`----------4------------`)
                // console.log({
                //     transaction
                // })
                if ( transaction && ObjectID.isValid(transaction) ) { //checkObjectIDs
                    dataInsert.transaction = transaction;
                }

                // console.log(`----------5------------`)
                // console.log({
                //     date
                // })
                if(date){ // stackoverflow checkDate
                    dataInsert.date = date;
                }

                if(note){
                    dataInsert.note = note;
                }
                
                // Code sau này lấy theo 3 ký tự đầu của ký hiệu đại lý + random 5 số
                let { data: code } = await this.checkCode({  });
                dataInsert.code = code;
                
                // if ( !Number(price) ){
                //     return resolve({ error: true, message: 'price_invalid' });
                // }
                let infoData = await AGENCY_MODEL.getInfo({ agencyID: agency });

                /**
                 * ADD VALUE FOR FUNDS
                 */
                let fundsBeforeChange = funds;
                let fundsAfterChange;

                dataInsert.fundsBeforeChange = fundsBeforeChange; // CHANGE SPREAD OPERATION
                // console.log(`----------6------------`)
                // console.log({
                //     ['dataInsert.fundsBeforeChange']: dataInsert.fundsBeforeChange
                // })
                let openingBalance;

                if(infoData.data.openingBalance == 0){
                    openingBalance = price
                }

                if(statusFinancial) { //Number.isNaN(Number) && enum
                    dataInsert.status = statusFinancial
                }
                // console.log(`----------6------------`)
                // console.log({
                //     ['dataInsert.status']: dataInsert.status
                // })
                
                let dataUpdateAgency = {}

                /**
                 * TYPE: 1 => THU
                 * TYPE: 2 => CHI
                 */
                if (type == 1) {
                    
                    let totalFunds = Number(funds) + Number(price);

                    // console.log(`----------CHECK THU_CHI (THU)------------`)
                    // console.log({
                    //     ['totalFunds']: totalFunds,
                    //     funds, price
                    // })
                    /**
                     * statusFinancial == 1 => UPDATE QUỸ TIỀN MẶT
                     * statusFinancial == 2 => UPDATE QUỸ NGÂN HÀNG
                     */
                    // console.log(`----------7.1------------`)
                    // console.log({
                    //     ['statusFinancial']: statusFinancial
                    // })
                    if(statusFinancial && statusFinancial == 1) { // //Number.isNaN(Number) && equal
                        dataUpdateAgency = {
                            agencyID: agency, funds: totalFunds, openingBalance
                        }
                    }else{
                        dataUpdateAgency = {
                            agencyID: agency, fundsBank: totalFunds
                        }
                    }
                    // console.log(`----------7.2------------`)
                    // console.log({
                    //     ['dataUpdateAgency']: dataUpdateAgency
                    // })
                    let infoDataUpdate = await AGENCY_MODEL.updateFunds(dataUpdateAgency);
                    fundsAfterChange = totalFunds;
                }else {
                    let totalFunds = Number(funds) - Number(price);
                    // console.log(`----------CHECK THU_CHI (CHI)------------`)
                    // console.log({
                    //     ['totalFunds']: totalFunds,
                    //     funds, price
                    // })

                    if(totalFunds < 0) {
                        return resolve({ error: true, data: "fund_invalid" });
                    }
                    /**
                     * statusFinancial == 1 => UPDATE QUỸ TIỀN MẶT
                     * statusFinancial == 2 => UPDATE QUỸ NGÂN HÀNG
                     */
                    //  console.log(`----------8.1------------`)
                    //  console.log({
                    //      ['statusFinancial']: statusFinancial
                    //  })
                    if(statusFinancial && statusFinancial == 1) { //Number.isNaN(Number) && equal
                        dataUpdateAgency = {
                            agencyID: agency, funds: totalFunds, openingBalance
                        }
                    }else{
                        dataUpdateAgency = {
                            agencyID: agency, fundsBank: totalFunds
                        }
                    }
                    // console.log(`----------8.2------------`)
                    // console.log({
                    //     ['dataUpdateAgency']: dataUpdateAgency
                    // })

                    let infoDataUpdate = await AGENCY_MODEL.updateFunds(dataUpdateAgency);
                    fundsAfterChange = totalFunds;
                }


                // console.log(`----------9.1------------`)
                // console.log({
                //     ['dataInsert']: dataInsert
                // })
                dataInsert.fundsAfterChange = Number.parseInt(fundsAfterChange);
                // console.log(`----------9.2-----------`)
                // console.log({
                //     ['dataInsert.fundsAfterChange']: dataInsert.fundsAfterChange
                // })
                let infoAfterInsert = await this.insertData(dataInsert);
                // console.log(`----------10-----------`)
                // console.log({ infoAfterInsert });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Kiểm tra code
     */
     checkCode({ }) {
        return new Promise(async resolve => {
            try {
                let codeNew = Math.floor(100000 + Math.random() * 900000);
                const infoFinded = await FINANCIAL_COLL.findOne({ code: Number(codeNew) });
                
                if(infoFinded)  {
                    await checkCode({  });
                }

                return resolve({ error: false, data: codeNew });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ agencyID, status }) {
        return new Promise(async resolve => {
            try {
                
                let listFinancial = await FINANCIAL_COLL
                    .find({ agency: agencyID })
                    .populate({
                        path: "transaction",
                        select: "_id code loanAmount"
                    })
                    .sort({  createAt: -1 });
                if(!listFinancial) 
                    return resolve({ error: true, message: "get_list_agency" });

                return resolve({ error: false, data: listFinancial });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListFilter({ agencyID, type, typeFinancial, fromDay, toDay, status }) {
        return new Promise(async resolve => {
            try {
                let dataFilter = {};
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });
                dataFilter.agency = agencyID;
                if ( type ){
                    dataFilter.type = type;
                }
                if ( typeFinancial ){
                    dataFilter.typeFinancial = typeFinancial;
                }
                if(status) {
                    dataFilter.status = status;
                }
                // console.log({ fromDay, toDay });
                // if ( fromDay ){
                //     let start = new Date(fromDay);
                //     start.setHours(0,0,0,0);
                //     dataFilter.createAt.$gte = start;
                // }
                // if ( toDay ){
                //     var end = new Date(toDay);
                //     end.setHours(23,59,59,999);
                //     dataFilter.createAt.$lt = end;
                // }
                if ( fromDay && !toDay ){
                    let start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    dataFilter.date = { $gte: start };
                }
                if ( !fromDay && toDay ){
                    var end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFilter.date = { $lte: end };
                }
                if ( fromDay && toDay ){
                    let start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    var end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    dataFilter.date = {
                        $gte: start, 
                        $lte:  end
                    };
                }
                // console.log();
                // console.log({ dataFilter });
                let listFinancial = await FINANCIAL_COLL
                    .find(dataFilter)
                    .populate({
                        path: "transaction",
                    })
                    .sort({ createAt: -1 });
                // console.log({ listFinancial });

                // console.log({ listFinancial: listFinancial[0] });
                if(!listFinancial) 
                    return resolve({ error: true, message: "get_list_agency" });

                return resolve({ error: false, data: listFinancial });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ transactionID, onModel }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });
                let data = { transaction: transactionID };
                if(onModel) {
                    data.onModel = onModel;
                }
                let infoData = await FINANCIAL_COLL.find(data).sort({ createAt: -1 });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getLastFinancial({ agency, status }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(agency))
                    return resolve({ error: true, message: 'params_invalid' });
                let data = { agency: agency };
                if(status) {
                    data.status = status;
                }
                let infoData = await FINANCIAL_COLL.findOne(data).sort({ createAt: -1 });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
