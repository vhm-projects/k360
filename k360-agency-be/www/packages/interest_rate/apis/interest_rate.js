"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const moment = require('moment');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadSingle, uploadArray, uploadCusAndAuthPaper } = require('../../../config/cf_helpers_multer');
const INTEREST_RATE_MODEL                           = require('../models/interest_rate').MODEL;
const { CF_ROUTINGS_INTEREST_RATE }                 = require("../constant/interest_rate.uri");
const { TYPE_TRANSACTION }                          = require('../../../config/cf_constants');
const { PRODUCT_CATEGORY_MODEL }                    = require('../../product_category');

const { subStractDateGetDay, subtractDate }                             = require('../../../config/cf_time');

/**
 * MODELS, COLLECTIONS
 */
module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
          
            /**
             * Function: Thêm khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_INTEREST_RATE.ADD_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        const { _id: userID } = req.user;
                        const agencyID = req.agencyID;
                        // console.log({ agencyID });
                        const { name, productID, from, to, percent } = req.body;
                        let infoAfterInsert = await INTEREST_RATE_MODEL.insert({ name, agencyID, productID, from, to, userID, percent });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            
            /**
             * Function: CẬP NHẬT QUẢN LÝ LÃI
             * Date: 17/04/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_INTEREST_RATE.UPDATE_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        const { _id: userID } = req.user;
                        // const agencyID = req.agencyID;
                        const { interestRate, name, percent } = req.body;
                        let infoAfterUpdate = await INTEREST_RATE_MODEL.update({ interestRate, name, percent });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

             /**
             * Function: Lấy thông tin quản lý lãi
             * Date: 17/04/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_INTEREST_RATE.INFO_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        const { _id: userID } = req.user;
                        const { interestRateID } = req.params;
                        let infoAfterUpdate = await INTEREST_RATE_MODEL.getInfoByID({ interestRateID });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

             /**
             * Function: Xoá Quản lý lãi
             * Date: 17/04/2021
             * Dev: DEPV
             */
              [CF_ROUTINGS_INTEREST_RATE.REMOVE_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        const { _id: userID } = req.user;
                        const { interestRateID } = req.params;
                        let infoAfterUpdate = await INTEREST_RATE_MODEL.removeByID({ interestRateID });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            // LIST INTEREST RATE
            [CF_ROUTINGS_INTEREST_RATE.LIST_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'view',
					title: `List Interest Rate - K360`,
					code: CF_ROUTINGS_INTEREST_RATE.LIST_INTEREST_RATE,
					inc: path.resolve(__dirname, '../views/list_interest_rate.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { product, type, percent } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ parent: null, status: 1 });
                        const agencyID  = req.agencyID;
                        let listData = await INTEREST_RATE_MODEL.getList({ agencyID, product, type, percent });
                        ChildRouter.renderToView(req, res, {
                            TYPE_TRANSACTION,
                            listProductCategory: listProductCategory.data,
                            listData: listData.data,
                            _product: product , _type: type, percent
						});
                    }]
                },
            },

            /**
             * FUNCTION: LẤY GIÁ VÀ PHẦN TRĂM LÃI CỦA CHU KỲ
             * SONLP
             */
             [CF_ROUTINGS_INTEREST_RATE.INTEREST_RATE_PERIOD_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID          = req.agencyID;
                        let { dateOfperiod, typeProduct, loanAmount } = req.query;
                        // 2// Tính lãi
                        /**
                         *  limitDate: Ngày giới hạn giữa ngày hiện tại vs ngày tính lãi
                         *  price: Số tiền vay
                         *  typeProduct: Danh mục sản phẩm
                         */
                        // let DTAE_OF_PERIOD = period * 30;
                        let checkDateToPayInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID, limitDate: dateOfperiod, price: Number(loanAmount), typeProduct: typeProduct});
                        // 3// Cộng mảng giá để tính ra tổng tiền lãi
                        let priceInterest = 0;
                        checkDateToPayInterest.data.forEach( price => {
                            priceInterest += price;
                        });
                        let percentOfPayInterest         = checkDateToPayInterest.percent;

                        return res.json({ error: false, dateOfperiod, loanAmount, percentOfPayInterest, priceInterest })
                    }]
                },
            },

            /**
             * FUNCTION: LẤY GIÁ VÀ PHẦN TRĂM LÃI CỦA CHU KỲ
             * SONLP
             */
             [CF_ROUTINGS_INTEREST_RATE.INTEREST_RATE_DATE_EXPIRE_TIME]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID          = req.agencyID;
                        let { expireTime, typeProduct, loanAmount, expireTimeAfterPeriod } = req.query;
                        
                        expireTime            = new Date(expireTime);
                        expireTime            = moment(expireTime).endOf('day').format();
                        expireTime            = new Date(expireTime);

                        let expireTimeAfterPeriodCheck = new Date(expireTimeAfterPeriod);
                       
                        expireTimeAfterPeriodCheck = moment(expireTimeAfterPeriodCheck).startOf('day').format();
                        expireTimeAfterPeriodCheck = new Date(expireTimeAfterPeriodCheck);

                        console.log({
                            expireTimeAfterPeriod, expireTime
                        });
                        let numberDateBetweenExpireTimeVsExpireTimeAfterPeriod = subStractDateGetDay({date1: expireTimeAfterPeriodCheck, date2: expireTime });
                        console.log({
                            numberDateBetweenExpireTimeVsExpireTimeAfterPeriod
                        });
                        /**                                                                |---------------------| 
                         *  EXAMPLE:                                                       |(2/9)                |   
                         *      |------------------------------------|-------------|-------|------|              |
                         *     1/8                                 31/8           1/9            3/9             |
                         *       ----------------30 ngày--------------(trễ 1 ngày)---------(Nhận được tiền) -> Dời 1 ngày (2/9)
                         * 
                         *      => NUMBER_DATE_HAVE_PERIOD = 1 ngày trễ (31/8 -> 1/9) + 30 NGÀY (1 Chu kỳ) 
                         *                                   (numberDateBetweenDate)  + (DAY_OF_MONTH)
                         */
                        const DAY_OF_MONTH = 30;
                        numberDateBetweenExpireTimeVsExpireTimeAfterPeriod     = Math.floor(Math.abs(numberDateBetweenExpireTimeVsExpireTimeAfterPeriod));
                        console.log({
                            numberDateBetweenExpireTimeVsExpireTimeAfterPeriod
                        });
                        const NUMBER_DATE_HAVE_PERIOD = numberDateBetweenExpireTimeVsExpireTimeAfterPeriod + DAY_OF_MONTH;
                        // 2// Tính lãi
                        /**
                         *  limitDate: Ngày giới hạn giữa ngày hiện tại vs ngày tính lãi
                         *  price: Số tiền vay
                         *  typeProduct: Danh mục sản phẩm
                         */
                        // console.log({
                        //     NUMBER_DATE_HAVE_PERIOD
                        // });
                        // let expireTimev2 = new Date('2021-07-21T11:42:27.999+00:00');
                        // let dateAfterPay = subtractDate(NUMBER_DATE_HAVE_PERIOD, expireTimev2);
                        // console.log({ 
                        //     dateAfterPay, 
                        // });
                        let { data: infoInterest } = await INTEREST_RATE_MODEL.getPriceDateToInterest({ agencyID, limitDate: NUMBER_DATE_HAVE_PERIOD, price: loanAmount, typeProduct: typeProduct });
                       
                        return res.json({ error: false, data: infoInterest })
                    }]
                },
            },

            //============API MOBILE ===================
            // LIST INTEREST RATE
            [CF_ROUTINGS_INTEREST_RATE.LIST_INTEREST_RATE_API]: {
                config: {
                    auth: [ roles.role.owner.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { product, type, percent } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ parent: null, status: 1 });
                        const agencyID  = req.agencyID;
                        
                        let listData = await INTEREST_RATE_MODEL.getList({ agencyID, product, type, percent });

                        if(listData.error == true) {    
                            listData = [];
                        }
                        res.json({
                            TYPE_TRANSACTION,
                            listProductCategory: listProductCategory.data,
                            error: false,
                            listData: listData.data,
                            _product: product , _type: type, percent
						});
                    }]
                },
            },
        }
    }
};
