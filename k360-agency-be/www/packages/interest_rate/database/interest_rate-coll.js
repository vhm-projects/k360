"use strict";
const Schema    = require('mongoose').Schema;
const BASE_COLL  = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION LÃI SUẤT DO ĐẠI LÝ QUY ĐỊNH
 */
module.exports = function({ fields = {}, name = 'interest_rate' }){
    return BASE_COLL(name, {
        // Tiêu đề lải suất
        name: String,

        // Đại lý
        agency: {
            type: Schema.Types.ObjectId,
            ref: "agency"
        }, 
        // Sản phẩm
        product: {
            type: Schema.Types.ObjectId,
            ref: "product_category"
        },
        /**
         * Có 6 laoị giao dịch (lấy trong file constanse)
         * 1: Cầm đồ
         * 2: Gia hạn bình thường
         * 3: Gia hạn vay thêm
         * 4: Gia hạn trả bớt
         * 5: Báo mất
         * 6: Chuộc đồ
         */
        // typeTransaction: {
        //     type: Number,
        //     default: 1
        // },
        // Từ ngày thứ
        from: {
            type: Number,
        },
        // Đến ngày thứ
        to: {
            type: Number,
        },
        // Phần trăm
        percent: {
            type: Number,
            default: 0
        },
        //_________Người tạo
        userCreate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
        //_________Người cập nhật
        userUpdate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
        ...fields
    });
}

