"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * EXTERNAL PAKCAGE
 */
const { checkObjectIDs } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const PRICING_REQUEST_COLL = require('../databases/pricing-request-coll')({});


class Model extends BaseModel {
    constructor() {
        super(PRICING_REQUEST_COLL);
    }

    delete({ pricingRequestID }){
        return new Promise(async resolve => {
            try {
                if(!pricingRequestID || !ObjectID.isValid(pricingRequestID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const infoAfterDelete = await PRICING_REQUEST_COLL.deleteOne({
                    _id: pricingRequestID
                });
 
                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ requestID, status }){
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(requestID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const STATUS_NOT_SEEN    = 0;
                const STATUS_NOT_CONTACT = 1;
                const STATUS_CONTACTED   = 2;
                const STATUS_VALUE = [STATUS_NOT_SEEN, STATUS_NOT_CONTACT, STATUS_CONTACTED];
                if(!Number.isNaN(Number(status)) && STATUS_VALUE.includes(Number(STATUS_VALUE)))
                    return resolve({ error: true, message: 'status_is_required' })

                const dataUpdatePricingRequest = {
                    status: Number(status),
                };
                const infoAfterUpdate = await PRICING_REQUEST_COLL.findByIdAndUpdate(requestID, {
                   ...dataUpdatePricingRequest
                }, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_status' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ pricingRequestID }){
        return new Promise(async resolve => {
            try {
                if(!pricingRequestID || !ObjectID.isValid(pricingRequestID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const infoPricingRequest = await PRICING_REQUEST_COLL
                    .findById(pricingRequestID)
                    .populate('agency categories');

                if(!infoPricingRequest)
                    return resolve({ error: true, message: 'get_info_product_category_failed' });

                return resolve({ error: false, data: infoPricingRequest });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword, agency = null, email, phone, status }){
        return new Promise(async resolve => {
            try {
                const conditionGetList = { agency };
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionGetList.email = new RegExp(key, 'i');
                }

                email   && (conditionGetList.email = email);
                phone   && (conditionGetList.phone = phone);
                status  && (conditionGetList.status = status);

                const listPricingRequest = await PRICING_REQUEST_COLL
                    .find(conditionGetList)
                    .populate('agency categories')
                    .sort({ createAt: -1})
                    .lean(); 
                if(!listPricingRequest)
                    return resolve({ error: true, message: 'cannot_get_list_pricing_request' });

                return resolve({ error: false, data: listPricingRequest });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListFilter({ keyword, agency = null, email, phone, status }){
        return new Promise(async resolve => {
            try {
                const conditionGetList = { agency };
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';
                    conditionGetList.$or = [
                        { name: new RegExp(key, 'i') },
                        { phone: new RegExp(key, 'i') }
                    ]
                }

                const listPricingRequest = await PRICING_REQUEST_COLL
                    .find(conditionGetList)
                    .populate('agency categories')
                    .sort({ createAt: -1})
                    .lean(); 
                if(!listPricingRequest)
                    return resolve({ error: true, message: 'cannot_get_list_pricing_request' });

                return resolve({ error: false, data: listPricingRequest });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
