"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_PRICING_REQUEST } 				= require('../constants/pricing_request.uri');

/**
 * MODELS
 */
const PRICING_REQUEST_MODEL = require('../models/pricing_request').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ PRICING REQUEST  ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Cập nhật yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRICING_REQUEST.UPDATE_PRICING_REQUEST]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate } = req.user;
                        const { listRequestID, fullname, email, phone, status, description } = req.body;

                        const infoAfterUpdate = await PRICING_REQUEST_MODEL.update({
                            listRequestID, fullname, email, phone, status, description, userUpdate
                        })

                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Cập nhật yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRICING_REQUEST.UPDATE_PRICING_REQUEST_STATUS]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate } = req.user;
                        const { requestID, status } = req.body;

                        const infoAfterUpdate = await PRICING_REQUEST_MODEL.update({
                            requestID, status
                        })

                        res.json(infoAfterUpdate);
                    }]
                },
            },


            /**
             * Function: Xóa yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
            // [CF_ROUTINGS_PRICING_REQUEST.DELETE_PRICING_REQUEST]: {
            //     config: {
            //         auth: [ roles.role.admin.bin ],
            //         type: 'json',
            //     },
            //     methods: {
            //         get: [ async function (req, res) {
            //             const { pricingRequestID } = req.query;

            //             const infoAfterDelete = await PRICING_REQUEST_MODEL.delete({
            //                 pricingRequestID
            //             })

            //             res.json(infoAfterDelete);
            //         }]
            //     },
            // },

             /**
             * Function: Chi tiết yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRICING_REQUEST.INFO_PRICING_REQUEST]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { pricingRequestID } = req.query;

                        const infoPricingRequest = await PRICING_REQUEST_MODEL.getInfo({
                            pricingRequestID
                        })

                        res.json(infoPricingRequest);
                    }]
                },
            },

            /**
             * Function: Danh sách yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRICING_REQUEST.LIST_PRICING_REQUEST]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: 'List Pricing Request - K360',
					code: CF_ROUTINGS_PRICING_REQUEST.LIST_PRICING_REQUEST,
					inc: path.resolve(__dirname, '../views/list_pricing_request.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
					get: [ async function(req, res){
                        const { type } = req.query;
                        const agencyID = req.agencyID;
						const listPricingRequest = await PRICING_REQUEST_MODEL.getList({ agency: agencyID });

                        if(type && type === 'API'){
                            return res.json({
                                listPricingRequest
                            })
                        }

						ChildRouter.renderToView(req, res, {
							listPricingRequest: listPricingRequest.data
						})
					}],
                    post: [ async function (req, res) {
                        const { keyword, agency, email, phone, status } = req.body;

                        const listPricingRequest = await PRICING_REQUEST_MODEL.getList({
                            keyword, agency, email, phone, status
                        })

                        res.json(listPricingRequest);
                    }]
                },
            },

            /**
             * Function: Danh sách yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRICING_REQUEST.LIST_PRICING_REQUEST_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
                        const { type, keyword } = req.query;
                        const agencyID = req.agencyID;
						const listPricingRequest = await PRICING_REQUEST_MODEL.getListFilter({ agency: agencyID, keyword });

                        return res.json({
                            listPricingRequest
                        })

					}]
                },
            },
        }
    }
};
