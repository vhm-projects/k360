const BASE_ROUTE = '/pricing-request';

const CF_ROUTINGS_PRICING_REQUEST = {
    // PRICING REQUEST
    ADD_PRICING_REQUEST: `${BASE_ROUTE}/add-pricing-request`,
    UPDATE_PRICING_REQUEST: `${BASE_ROUTE}/update-pricing-request`,
    DELETE_PRICING_REQUEST: `${BASE_ROUTE}/delete-pricing-request`,
    LIST_PRICING_REQUEST: `${BASE_ROUTE}/list-pricing-request`,
    INFO_PRICING_REQUEST: `${BASE_ROUTE}/info-pricing-request`,

    LIST_PRICING_REQUEST_API: `/api${BASE_ROUTE}/list-pricing-request`,

    UPDATE_PRICING_REQUEST_STATUS: `${BASE_ROUTE}/update-status-pricing-request`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PRICING_REQUEST = CF_ROUTINGS_PRICING_REQUEST;
