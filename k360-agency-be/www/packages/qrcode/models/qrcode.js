"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const QRCode   = require('qrcode');
const path = require('path');
const fs   = require('fs');
/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */
 const { UPLOAD_FILE_S3 }                 = require('../../upload-s3/utils');
const { IMAGE_MODEL }                     = require('../../image');

/**
 * COLLECTIONS
 */
const QRCODE_COLL                = require('../databases/qrcode-coll')({});

class Model extends BaseModel {
    constructor() {
        super(QRCODE_COLL);
    }

	insert({ id, key, userID }) {
        return new Promise(async resolve => {
            try {
                // console.log({ id, key });
                const generateQR = await QRCode.toDataURL(id.toString());
                // .toFile(
                //     path.resolve(__dirname, `../../../../files/text3.jpg`), 
                //     text
                // );
                // let uploadFileS3 = await UPLOAD_FILE_S3( path.resolve( __dirname, `../../../../files/text3.jpg`), "text2.png")
                // console.log({ generateQR });
                let uploadFileS3 = await UPLOAD_FILE_S3( generateQR, key );
                // console.log({ uploadFileS3 });
                // console.log({ path: uploadFileS3.data.singlePart.httpRequest.path });
                // console.log({ service: uploadFileS3.data.service });

                let path = `${uploadFileS3.data.service.endpoint.href}${uploadFileS3.data.singlePart.httpRequest.path.substr(1)}`
                // console.log({ path, uploadFileS3 });
                let infoImageAfterInsert;

                if ( generateQR ){
                    infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                        name: path,
                        path: path,
                        userCreate: userID
                    });
                }

                let infoAfterInsert = await this.insertData({ transaction: id, image: infoImageAfterInsert.data._id })
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
}

exports.MODEL = new Model;
