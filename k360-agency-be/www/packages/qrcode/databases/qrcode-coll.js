"use strict";
const Schema    	= require('mongoose').Schema;
const BASE_COLL  	= require('../../../databases/intalize/base-coll');

/**
 * COLLECTION AGENCY CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'qr_code' }){
    return BASE_COLL(name, {
        /**
         * Giao dịch
         */
        transaction: {
            type: Schema.Types.ObjectId,
            ref: 'transaction'
        },
    
        // Hình ảnh qrcode
        image: {
            type: Schema.Types.ObjectId,
            ref: 'image'
        },
        
        /**
         * Trạng thái hoạt động.
         * 1. Hoạt động
         * 0. Khóa
         */
        status: {
            type: Number,
            default: 1
        },
        
        userCreate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        
		...fields
    });
}
