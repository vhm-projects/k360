"use strict";

/**
 * EXTERNAL PACKAGE
 */
const express           = require('express');
const moment            = require('moment');
const url               = require('url');
const formatCurrency    = require('number-format.js');
const lodash            = require('lodash');

/**
 * INTERNAL PACKAGE
 */
require('../../app');
const { CF_ROUTINGS_COMMON }                    = require('../packages/common/constant/common.uri');
const { CF_ROUTINGS_CUSTOMER }                  = require('../packages/customer/constant/customer.uri');
const { CF_ROUTINGS_PRICING_REQUEST } 			= require('../packages/pricing_request/constants/pricing_request.uri');
const { CF_ROUTINGS_PRODUCT_CATEGORY }          = require('../packages/product_category/constants/product_category.uri');
const { CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY }   = require('../packages/agency_product_category/constant/agency_product_category.uri');
const { CF_ROUTINGS_NOTIFICATION }              = require('../packages/notification/constants/notification.uri');
const { CF_ROUTINGS_INTEREST_RATE }             = require('../packages/interest_rate/constant/interest_rate.uri');
const { CF_ROUTINGS_TRANSACTION }               = require('../packages/transaction/constant/transaction.uri');
const { CF_ROUTINGS_INVENTORY }                 = require('../packages/transaction/constant/inventory_session.uri');
const { CF_ROUTINGS_FINANCIAL }                 = require('../packages/financial/constant/financial.uri');
const { CF_ROUTINGS_AGENCY }                    = require('../packages/agency/constants/agency.uri');
const { CF_ROUTINGS_AGENCY: CF_ROUTINGS_USER } 	= require('../packages/permissions/users/constant/user.uri');
const AGENCY_SESSION                            = require('../session/agency-session');

const { ADMIN_DOMAIN, AGENCY_DOMAIN, ENDUSER_DOMAIN } 	= require('../config/cf_domain');
const { SPECIAL_CATEGORY } 								= require('../config/cf_constants');

/**
 * MODELS, COLLECTIONS
 */
const NOTIFICATION_COLL = require('../packages/notification/databases/notification-coll')({});


class ChildRouter{
    constructor(basePath) {
        this.basePath = basePath;
        this.registerRouting;
    }

    registerRouting() {
    }
    exportModule() {
        let router = express.Router();
        
        for (let basePath in this.registerRouting()) {
            const item = this.registerRouting()[basePath];
    
            if (typeof item.methods.post !== 'undefined' && item.methods.post !== null) {
                if (item.methods.post.length === 1) {
                    router.post(basePath, item.methods.post[0]);
                } else if (item.methods.post.length === 2) {
                    router.post(basePath, item.methods.post[0], item.methods.post[1]);
                }
            }

            if (typeof item.methods.get !== 'undefined' && item.methods.get !== null) {
                if (item.methods.get.length === 1) {
                    router.get(basePath, item.methods.get[0]);
                } else if (item.methods.get.length === 2) {
                    router.get(basePath, item.methods.get[0], item.methods.get[1]);
                }
            }

            if (typeof item.methods.put !== 'undefined' && item.methods.put !== null) {
                if (item.methods.put.length === 1) {
                    router.put(basePath, item.methods.put[0]);
                } else if (item.methods.put.length === 2) {
                    router.put(basePath, item.methods.put[0], item.methods.put[1]);
                }
            }

            if (typeof item.methods.delete !== 'undefined' && item.methods.delete !== null) {
                if (item.methods.delete.length === 1) {
                    router.delete(basePath, item.methods.delete[0]);
                } else if (item.methods.delete.length === 2) {
                    router.delete(basePath, item.methods.delete[0], item.methods.delete[1]);
                }
            }

        }
        return router;
    }

    /**
     * 
     * @param {*} msg 
     * @param {*} res 
     * @param {*} data 
     * @param {*} code
     * @param {*} status  tham số nhận biết lỗi
     */
    static responseError(msg, res, data, code, status) {
        if (code) {
            res.status(code);
        }
        return res.json({error: true, message: msg, data: data, status: status});
    }

    static response(response, data) {
        return response.json(data);
    }


    static responseSuccess(msg, res, data, code) {
        if (code) {
            res.status(code);
        }

        return res.json({error: false, message: msg, data: data});
    }

    static pageNotFoundJson(res) {
        res.status(404);
        return res.json({"Error": "Page not found!"});
    }

     /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} data 
     * @param {*} title 
     */
    static async renderToView(req, res, data, title) {
        data = typeof data === 'undefined' || data === null ? {} : data;

        if (title) {
            res.bindingRole.config.title = title;
        }
        
        data.render = res.bindingRole.config;
        data.url = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl
        });
		data.hrefCurrent = req.originalUrl;

		//_________Thư viện thời gian
		data._lodash        = lodash;
		data.moment         = moment;
        data.formatCurrency = formatCurrency;

        if(req.user){
            // 2: lấy list agency từ DB, bỏ lấy từ session (tránh oversize token)
            let session             = AGENCY_SESSION.getContextAgency(req.session);
            let listAgencyOfUser    = req.user.agency;
            let agencyChoised       = null;

            if(session && listAgencyOfUser && listAgencyOfUser.length){
                listAgencyOfUser.map(agency => {
                    if(agency._id.toString() === session.agencyID.toString()){
                        agencyChoised = agency;
                    }
                })
            }

			// If user only one agency, then choose that agency
            if(!agencyChoised && listAgencyOfUser && listAgencyOfUser.length === 1){
                agencyChoised = listAgencyOfUser[0];
            }

            data.agencyChoosed  = agencyChoised;
            data.infoUser       = req.user;

			// Return notifications
			const userID 		= req.user._id;
            const conditionObj 	= { receiver: userID, status: 0 };

            if(agencyChoised && agencyChoised._id){
                conditionObj.agency = agencyChoised._id;
            }

			const listNotifications = await NOTIFICATION_COLL
                .find(conditionObj)
                .sort({ createAt: -1 })
                .limit(30)
                .lean();

            const totalUnSeenNotifications = await NOTIFICATION_COLL.countDocuments(conditionObj);

            data.listNotifications          = listNotifications;
            data.totalUnSeenNotifications   = totalUnSeenNotifications;
        }

        /**
         * langSession.saveLang(req.session, langKey)
         */

        // if (userSession.isLogin(req.session)) {
        //     data.langKey = userSession.getUser(req.session).lang;
        // } else {
        //     data.langKey = langSession.getLang(req.session);
        // }

        data.CF_ROUTINGS_COMMON                     = CF_ROUTINGS_COMMON;
		data.CF_ROUTINGS_PRICING_REQUEST			= CF_ROUTINGS_PRICING_REQUEST;
        data.CF_ROUTINGS_PRODUCT_CATEGORY           = CF_ROUTINGS_PRODUCT_CATEGORY;
        data.CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY    = CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY;
        data.CF_ROUTINGS_INTEREST_RATE              = CF_ROUTINGS_INTEREST_RATE;
        data.CF_ROUTINGS_CUSTOMER                   = CF_ROUTINGS_CUSTOMER;
        data.CF_ROUTINGS_TRANSACTION                = CF_ROUTINGS_TRANSACTION;
        data.CF_ROUTINGS_INVENTORY                  = CF_ROUTINGS_INVENTORY;
        data.CF_ROUTINGS_FINANCIAL                  = CF_ROUTINGS_FINANCIAL;
        data.CF_ROUTINGS_AGENCY                     = CF_ROUTINGS_AGENCY;
		data.CF_ROUTINGS_USER						= CF_ROUTINGS_USER;
        data.CF_ROUTINGS_NOTIFICATION               = CF_ROUTINGS_NOTIFICATION;

		// CONSTANTS
		data.SPECIAL_CATEGORY = SPECIAL_CATEGORY;

        // DOMAIM SOCKET
        data.ADMIN_DOMAIN                           = ADMIN_DOMAIN;
        data.AGENCY_DOMAIN                          = AGENCY_DOMAIN;
        data.ENDUSER_DOMAIN                         = ENDUSER_DOMAIN;

        // data._csrf = req.csrfToken();
        // res.set('x-csrf-token', req.csrfToken());
        // console.log(req.headers);

        // =============BIND ALL DATA TO VIEW=============//
        return res.render(res.bindingRole.config.view, data);
    }

    static renderToPath(req, res, path, data) {
        data = data == null ? {} : data;
        data.render = res.bindingRole.config;
        return res.render(path, data);
    }

    static renderToViewWithOption(req, res, pathRender, data) {
        return res.render(pathRender, {data});
    }

    static redirect(res, path) {
        return res.redirect(path);
    }
}

module.exports = ChildRouter;