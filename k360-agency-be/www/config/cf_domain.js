module.exports = {
    ADMIN_DOMAIN: process.env.ADMIN_DOMAIN      || "http://localhost:5001",
    AGENCY_DOMAIN: process.env.AGENCY_DOMAIN    || "http://localhost:5002",
    ENDUSER_DOMAIN: process.env.ENDUSER_DOMAIN  || "http://localhost:5003"
}