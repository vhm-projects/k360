"use strict";
let moment = require('moment');

exports._default_tme_zone = 'Asia/Ho_Chi_Minh';
//sudo timedatectl set-timezone Asia/Ho_Chi_Minh

//Hàm trả về số năm giữa 2 thời gian
exports.subStractDate = function subStractDate({date1, date2}) {
    console.log({ date1, date2 });
    let timeDate1 = new Date(date1).getTime();
    let timeDate2 = new Date(date2).getTime();
    let resultSubtractTime = timeDate1 - timeDate2;
    
    const MINUTES = 60 * 1000;
    const HOURS   = MINUTES * 60;
    const DAYS    = HOURS * 24;
    let resultSubtractTimeAfterParseDay = resultSubtractTime / DAYS;
    // let bb = Math.round(Math.abs((timeDate1 - timeDate2) / DAYS));
    return resultSubtractTimeAfterParseDay/365;

    // return new Date((new Date(subAdded)).getTime() - minute * 60000);
};

//hàm trả về số ngày giữa 2 thời gian
exports.subStractDateGetDay = function subStractDate({date1, date2}) {
    let timeDate1 = new Date(date1).getTime();
    let timeDate2 = new Date(date2).getTime();
    let resultSubtractTime = timeDate1 - timeDate2;
    
    const MINUTES = 60 * 1000;
    const HOURS   = MINUTES * 60;
    const DAYS    = HOURS * 24;
    let resultSubtractTimeAfterParseDay = resultSubtractTime / (1000 * 3600 * 24);
    // let resultSubtractTimeAfterParseDay = resultSubtractTime / 24;
    // return Math.round((second-first)/(1000*60*60*24));
    // let bb = Math.round(Math.abs((timeDate1 - timeDate2) / DAYS));
    return resultSubtractTimeAfterParseDay;

    // return new Date((new Date(subAdded)).getTime() - minute * 60000);
};

exports.addNumberToSetDate = function addNumberToSetDate({date1, numberToSet}) {
    // console.log({ date1, numberToSet });
    // let dateAfterChange = Date(date1);
    return date1.setDate(date1.getDate() + numberToSet); ;
};
exports.minusNumberToSetDate = function minusNumberToSetDate({date1, numberToSet}) {
    // console.log({ date1, numberToSet });
    // let dateAfterChange = Date(date1);
    return date1.setDate(date1.getDate() - numberToSet); ;
};

exports.subtractDate = (number, date) => {
    let newDate        = this.addNumberToSetDate({ date1: new Date(date), numberToSet: number });
    
    let dateAddOneDay  = this.addNumberToSetDate({ date1: new Date(newDate), numberToSet: 1 });
    let expireTimeAfterPeriod  = this.addNumberToSetDate({ date1: new Date(newDate), numberToSet: 30 });

    return {
        transactionDate: new Date(date),
        expireTime: new Date(newDate),
        interestStartDate: new Date(dateAddOneDay),
        expireTimeAfterPeriod: new Date(expireTimeAfterPeriod),
    }
}

exports.subtractDateHaveExpireTime = (expireTime) => {
    let dateAddOneDay  = this.addNumberToSetDate({ date1: new Date(expireTime), numberToSet: 1 });
    let expireTimeAfterPeriod  = this.addNumberToSetDate({ date1: new Date(expireTime), numberToSet: 30 });

    return {
        transactionDate: new Date(),
        expireTime: new Date(expireTime),
        interestStartDate: new Date(dateAddOneDay),
        expireTimeAfterPeriod: new Date(expireTimeAfterPeriod),
    }
}

exports.compareTwoDay = (date1, date2) => {
    let dateFirst  = moment(date1).startOf('day').format();
    let dateSecond = moment(date2).startOf('day').format();
    dateFirst  = new Date(dateFirst);
    dateSecond = new Date(dateSecond); 
    return dateFirst.getTime() === dateSecond.getTime();
}

exports.compareTwoDayCheck = (date1, date2) => {
    let dateFirst  = moment(date1).startOf('day').format();
    let dateSecond = moment(date2).startOf('day').format();
    dateFirst  = new Date(dateFirst);
    dateSecond = new Date(dateSecond); 
    if (dateFirst.getTime() > dateSecond.getTime()) {
        return 'bigger'
    }
    if (dateFirst.getTime() >= dateSecond.getTime()) {
        return 'bigger_equal'
    }
    if (dateFirst.getTime() < dateSecond.getTime()) {
        return 'smaller'
    }
    if (dateFirst.getTime() <= dateSecond.getTime()) {
        return 'smaller_equal'
    }
    if (dateFirst.getTime() == dateSecond.getTime()) {
        return 'equal'
    }
}


// exports.diff_months = function diff_months(dt2, dt1) 
// {
//     console.log(dt2.getTime());
//     console.log(dt1.getTime());
//     var diff =(dt2.getTime() - dt1.getTime()) / 1000;
//     console.log({ diff });
//     diff /= (60 * 60 * 24 * 7 * 4);
//     console.log({ diff });
//     return Math.abs(Math.round(diff));
// }