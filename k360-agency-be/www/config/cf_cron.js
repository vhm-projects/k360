const cron                      = require('node-cron');
const { calculateExpireDate }   = require('../utils/time_utils');
const TRANSACTION_COLL          = require('../packages/transaction/database/transaction-coll')({});
const TRANSACTION_MODEL         = require('../packages/transaction/models/transaction').MODEL;
const NOTIFICATION_MODEL        = require('../packages/notification/models/notification').MODEL;

cron.schedule("00 15 01 * * *", async () => {
    const STATUS_ACTIVE = 1;
    let listTransactions = await TRANSACTION_COLL
        .find({ status: STATUS_ACTIVE })
        .select('_id expireTime customer agency latestInjectTransaction')
        .populate("latestInjectTransaction")
        .lean();

    if(listTransactions && listTransactions.length){
        let currentDate = Date.now();

        for (let transaction of listTransactions) {
            let { expireTime } = transaction;
            let calDate = calculateExpireDate(currentDate, expireTime);
            
            if(calDate === 3){
                await NOTIFICATION_MODEL.insertV2({
                    title: "GIAO DỊCH SẮP ĐẾN HẠN",
                    description: "Giao dịch của khách hàng sắp đến hạn",
                    customerReceiver: transaction.customer,
                    url: `${transaction._id}`,
                    type: 9,
                    // arrayAgencyReceive: [transaction.agency],
                });
            }
            if(calDate === 0){
                await NOTIFICATION_MODEL.insertV2({
                    title: "GIAO DỊCH ĐẾN HẠN",
                    description: "Giao dịch của khách hàng đã đến hạn",
                    customerReceiver: transaction.customer,
                    url: `${transaction._id}`,
                    type: 10,
                    // arrayAgencyReceive: [transaction.agency],
                });
            }
            if(calDate === -1) {
                const TYPE_BORROW = 3;
                const TYPE_RANSOM = 6;
                const WAITING_RANSOM = 2;
                if (transaction.latestInjectTransaction) {
                    if ( transaction.latestInjectTransaction.type == TYPE_BORROW || (transaction.latestInjectTransaction.type == TYPE_RANSOM && transaction.latestInjectTransaction.ransom == WAITING_RANSOM)) {
                        await NOTIFICATION_MODEL.insertV2({
                            title: "GIAO DỊCH QUÁ HẠN",
                            description: "Giao dịch của khách hàng đã quá hạn",
                            customerReceiver: transaction.customer,
                            url: `${transaction._id}`,
                            type: 11,
                            // arrayAgencyReceive: [transaction.agency],
                        });
                    }
                }
            }
            if(calDate < -7){
                await NOTIFICATION_MODEL.insertV2({
                    title: "GIAO DỊCH QUÁ HẠN, SẢN PHẨM CÓ THỂ THANH LÝ",
                    description: "Giao dịch của khách hàng đã quá hạn, sản phẩm sẽ được thanh lý",
                    customerReceiver: transaction.customer,
                    url: `${transaction._id}`,
                    type: 12,
                    // arrayAgencyReceive: [transaction.agency],
                });
            }

        }
        // console.log({ arrTransactionNofity })
    }
})


// const dateFormat = moment(Date.now()).format('MMMM Do YYYY, h:mm:ss a');
// const dateFormat2 = moment(1622221200000).format('MMMM Do YYYY, h:mm:ss a');
// // const expireDate  = moment(1624209164911).subtract(3, 'days').format('L');
// const currentDate = moment(1617210000000);
// const expireDate = moment(1624209164911);
// const diff = currentDate.diff(expireDate, 'day');
// const diffDay = expireDate.diff(currentDate, 'day');

// console.log({ currentDate, expireDate, dateFormat, dateFormat2, diff, diffDay });

// const timeStampNow = Date.now();
// const start = moment(timeStampNow).format('DD/MM/YYYY') + ' ' + moment(timeStampNow).format('HH:mm');
// const end = moment(1624209164911).format('DD/MM/YYYY') + ' ' + moment(1624209164911).format('HH:mm');

// const test = moment.utc(moment(start,"DD/MM/YYYY HH:mm:ss").diff(moment(end,"DD/MM/YYYY HH:mm:ss"))).format("DD/MM/YYYY HH:mm:ss");

// console.log({ start, end, test });
