"use strict";

const express = require('express');
// const cookieParser      = require('cookie-parser');
// const csrf              = require('csurf');
// const csrfProtection    = csrf({ cookie: true });

module.exports = function (app) {
    app.use(express.json({limit: '50mb'}));
    app.use(express.urlencoded({ limit: '50mb', extended: true }));

    // parse cookies
    // we need this because "cookie" is true in csrfProtection
    // app.use(cookieParser());
    // app.use(csrfProtection);
};