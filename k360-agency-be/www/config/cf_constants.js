// KIỂU TÀI KHOẢN
exports.ADMIN_LEVEL = [
    { value: 0, text: 'Editor' },
    { value: 1, text: 'Admin' },
]

// LOẠI TÀI KHOẢN
exports.TYPE_LEVEL = [
    { value: 0, text: 'Admin' },
    { value: 1, text: 'Quản lý' },
    { value: 2, text: 'Nhân viên' },
]

// TRẠNG THÁI
exports.ADMIN_STATUS = [
    { value: 0, text: 'Khóa' },
    { value: 1, text: 'Hoạt động' },
]

// TRẠNG THÁI
exports.KIND_OF_AUTH_PAPER = [
    { value: 0, text: 'CMND' },
    { value: 1, text: 'Bằng lái xe' },
    { value: 2, text: 'CCCD' },
    { value: 3, text: 'Khác' },
]

// LOẠI GIAO DỊCH
exports.TYPE_TRANSACTION= [
    { value: 1, text: 'Cầm đồ', type: 1 },
    { value: 2, text: 'Gia hạn bình thường', type: 2 },
    { value: 3, text: 'Gia hạn vay thêm', type: 1 },
    { value: 4, text: 'Gia hạn trả bớt', type: 2 },
    { value: 5, text: 'Giao dịch Báo mất', type: 2 },
    { value: 6, text: 'Giao dịch Chuộc đồ', type: 2 },
]

// TRẠNG THÁI GIAO DỊCH
exports.TYPE_STATUS_TRANSACTION= [
    { value: 0, text: 'Đang xử lý', class: "badge-info" },
    { value: 1, text: 'Đang cầm', class: "badge-success" },
    { value: 2, text: 'Không duyệt', class: "badge-success" },
    { value: 3, text: 'Đã hoàn thành', class: "badge-danger" },
]

//Giới tính
exports.TYPE_GENDER = [
    { value: 0, text: 'Nữ' },
    { value: 1, text: 'Nam' },
    { value: 2, text: 'Khác' },
]

//Loại phiếu quỹ
exports.TYPE_TICKET = [
    { value: 1, text: 'Thu' },
    { value: 2, text: 'Chi' },
]

//Loại giao dịch quỹ
exports.TYPE_FINANCIAL = [
    { value: 1, text: 'Tự động' },
    { value: 2, text: 'Phát sinh' },
]

/**
 * MIME Types image
 */
 exports.MIME_TYPES_IMAGE = [ 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml' ];


/**
 * Thông báo
 */
exports.NOTIFICATION_TYPES = [
    { value: 0, text: 'YÊU CẦU ĐỊNH GIÁ' },
    { value: 1, text: 'YÊU CẦU TẠO GIAO DỊCH' },
    { value: 2, text: 'YÊU CẦU DUYỆT GIAO DỊCH CẦM ĐỒ' },
    { value: 4, text: 'YÊU CẦU TẠO GIAO DỊCH BỔ SUNG' },
];

/**
 * Danh mục đặc biệt
 */
exports.SPECIAL_CATEGORY = [
	{ value: 0, text: 'Bình thường' },
	{ value: 2, text: 'Vàng nguyên liệu' },
	{ value: 3, text: 'Trang sức' },
	{ value: 4, text: 'Non brand' },
	{ value: 5, text: 'Không có hóa đơn' },
	{ value: 6, text: 'Danh mục cuối' },
]

/**
 * Loại thông báo
 */
 exports.TYPE_NOTIFICATION = [
	{ value: 0, text: 'Thông báo của hệ thống' },
	{ value: 1, text: 'Yêu cầu định giá' },
	{ value: 2, text: 'Yêu cầu tạo giao dịch' },
	{ value: 3, text: 'Yêu cầu duyệt giao dịch cầm đồ' },
	{ value: 4, text: 'Yêu cầu kh duyệt giao dịch cầm đồ' },
	{ value: 5, text: 'Yêu cầu tạo giao dịch bổ sung chuộc đồ' },
	{ value: 6, text: 'Yêu cầu duyệt chuộc đồ' },
	{ value: 7, text: 'Yêu cầu không duyệt chuộc đồ' },
	{ value: 8, text: 'Yêu cầu liên hệ' },
	{ value: 9, text: 'Yêu cầu giao dịch đến hạn' },
	{ value: 10, text: 'Yêu cầu giao dịch hết hạn' },
	{ value: 11, text: 'Yêu cầu giao dịch quá hạn' },
	{ value: 12, text: 'Yêu cầu thanh lý sản phẩm' },
	{ value: 13, text: 'Thông báo từ hệ thống' },
]

/**
 * SCREEN KEY
 */
 exports.SCREEN_KEY = {
    ImageDealScreen: {
        value: 1, text: 'ImageDealScreen'
    },
    NotificationScreen: {
        value: 2, text: 'NotificationScreen'
    },
    DetailDealScreen: {
        value: 3, text: 'DetailDealScreen'
    },
}