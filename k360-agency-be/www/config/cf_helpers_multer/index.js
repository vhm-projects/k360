let { upload }   = require('./config/cf_multer');
let uploadSingle = upload.single('file');
let uploadArray  = upload.array('files');

let uploadFields = upload.fields([
    { name: 'customerImages', maxCount: 5 },
    { name: 'KYCImages', maxCount: 5 },
    { name: 'receiptImages', maxCount: 5 },
    { name: 'formVerificationImages', maxCount: 5 }
]);

let uploadCusAndAuthPaper = upload.fields([
    { name: 'image_customer', maxCount: 5 },
    { name: 'image_auth_paper', maxCount: 5 },
]);

module.exports = {
    uploadSingle,
    uploadArray,
    uploadFields,
    uploadCusAndAuthPaper
}
