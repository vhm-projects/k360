"use strict";

let jwt                             = require('jsonwebtoken');
let redis                           = require('redis');
let redisAdapter                    = require('socket.io-redis');
let { REDIS_SEPERATOR }             = require('../config/cf_redis');

// tạo GlobalStore -> lưu trữ danh sách user đang online (nhận được socketIO)
let usersConnectedInstance          = require('../config/cf_globalscope').GlobalStore;
let usersConnected                  = usersConnectedInstance.usersOnline;

let {
    SSC_SEND_NOTIFICATION_CONTACT,

    CSS_CONFIRM_TRANSACTION_REQUEST_ENDUSER,
    SSC_CONFIRM_TRANSACTION_REQUEST_ENDUSER,

    CSS_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST_ENDUSER,
    SSC_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST_ENDUSER,
} = require('./constants');


/**
 * UTILS
 */
let { replaceExist, decrypt }       = require('../utils/utils');
let { mapSocketIDAndData, getUserNotConnected }          = require('../utils/socket_utils');
let { sendMessageMobile, sendMessageCustomerMobile }    = require('../fcm/utils');
// let { SCREEN_KEY }           = require("../config/cf_constants")
/**
 * MODELS, COLLECTIONS
 */
let USER_MODEL                      = require('../packages/permissions/users/models/user').MODEL;
let CUSTOMER_MODEL                  = require('../packages/customer/models/customer').MODEL;
let NOTIFICATION_MODEL              = require('../packages/notification/models/notification').MODEL;    
let { AGENCY_COLL }           = require('../packages/agency');


module.exports = function (io) {
    let pub = redis.createClient({
        // detect_buffers: true,
        // return_buffers: true,

        host: REDIS_SEPERATOR.HOST,
        port: REDIS_SEPERATOR.PORT,
        auth_pass: REDIS_SEPERATOR.PWD
    });

    let sub = redis.createClient({
        // detect_buffers: true,
        // return_buffers: true,

        host: REDIS_SEPERATOR.HOST,
        port: REDIS_SEPERATOR.PORT,
        auth_pass: REDIS_SEPERATOR.PWD
    });
    // let sub = CLIENT_REDIS;
    // let pub = CLIENT_REDIS;

    io.adapter(redisAdapter({
        pubClient: pub,
        subClient: sub
    }));

    /**
     * SOCKET có 2 loại token
     *      - token: token cho user: employee, owner, admin
     *      - _token:  user Không đăng nhập (web -> so sánh với env), user có đăng nhập (trên app -> flow tương tự token)
     */

    io
    .set('transports', ['websocket']) //config
    .use(async (socket, next) => {
        // console.log({ __handshake: socket.handshake });
        socket.isAuth    = false;
        // console.log({ token: socket.handshake.query.token, token: socket.handshake.query,  });
        
        if (socket.handshake.query && socket.handshake.query.token) { //user: employee, owner, admin
            let signalVerifyToken = await USER_MODEL.checkAuth({ token: socket.handshake.query.token });
           
            if (signalVerifyToken.error)
                return next(new Error('Authentication error'));
            let { data: infoUser  } = signalVerifyToken;

            socket.decoded   = infoUser;
            socket.isAuth    = true;
            /**
             * ADD USER INTO usersConnected
             * usersConnected
             */
            let { id: socketID } = socket;
            let { _id: userID, username }  = infoUser;
            
            let usersConnectedGlobal = usersConnectedInstance.usersOnline;
            // console.log({
            //     [`danh sách TRƯỚC KHI user connected vào socket`]: usersConnectedGlobal
            // })

            // console.log({
            //     [`thông tin trước khi connect`]: usersConnectedGlobal,
            //     [`số lượng`]: usersConnectedGlobal.length
            // })

            usersConnected = replaceExist(usersConnectedGlobal, userID, socketID, username);
            // console.log({
            //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
            // })
            // console.log({
            //     [`thông tin trước khi connect`]: usersConnected,
            //     [`số lượng`]: usersConnected.length,
            //     [`Truy cập mới`]: {
            //         userID, socketID, username
            //     }
            // })

            // console.log({
            //     [`danh sách SAU KHI user connected vào socket`]: usersConnected
            // })

            usersConnectedInstance.setUsersOnline(usersConnected);
            next();
        }

        if(socket.handshake.query && socket.handshake.query._token){ // customer_fake, customer auth

            try {
                let decoded = await jwt.verify(socket.handshake.query._token, process.env.JWT_SECRET_KEY);
              
                if (decoded && decoded.hashed) { // đang dùng token FAKE (user)
                    console.log({ decoded })
                    let content = decrypt(decoded.hashed);

                    if(content !== process.env.ENCRYPT_SECRET_KEY) { 
                        next(new Error('Authentication error, catch workspace'));
                    } else {
                        console.log(`---------------2-----------------`)

                        socket.decoded      = decoded;
                        socket.tokenFake    = true;
                        socket.isAuth       = true;
                    }
                } else { // dùng token user (mobile có login)
                    let signalVerifyToken = await CUSTOMER_MODEL.checkAuth({ token: socket.handshake.query._token });
                    if (signalVerifyToken.error)
                        return next(new Error('Authentication error'));
                    let { data: infoUser  } = signalVerifyToken;
                    socket.decoded   = infoUser;
                    socket.isAuth    = true;
                    socket.tokenFake    = false;
                    /**
                     * ADD USER INTO usersConnected
                     * usersConnected
                     */
                    let { id: socketID } = socket;
                    let { _id: userID, username }  = infoUser;
                    
                    let usersConnectedGlobal = usersConnectedInstance.usersOnline
                    // console.log({
                    //     [`danh sách TRƯỚC KHI user connected vào socket`]: usersConnectedGlobal
                    // })
                    // console.log({
                    //     [`thông tin trước khi connect`]: usersConnectedGlobal,
                    //     [`số lượng`]: usersConnectedGlobal.length
                    // })
        
                    usersConnected = replaceExist(usersConnectedGlobal, userID, socketID, username);
        
                    console.log({
                        [`danh sách SAU KHI user connected vào socket`]: usersConnected
                    })
                    // console.log({
                    //     [`thông tin trước khi connect`]: usersConnected,
                    //     [`số lượng`]: usersConnected.length,
                    //     [`Truy cập mới`]: {
                    //         userID, socketID, username
                    //     }
                    // })
                    
                    usersConnectedInstance.setUsersOnline(usersConnected);
                }

                return next();
            } catch (error) {
                console.log({ error })
                next(new Error('Authentication error, catch workspace'));
            }
        }
    })
    .on('connection', function (socket) {
        // console.log(`------------------connection------------`)
        const { id: socketID, decoded, isAuth, tokenFake } = socket;
        // console.log({ decoded, tokenFake });

        /**
         * CÓ 2 LOẠI TOKEN -> 2 DECODE obj KHÁC NHAU
         *      1/ TOKEN FAKE (không chứa infoUser)
         *              decoded: {
                            hashed: {
                            iv: 'c8c84639b94bf5e668ba90d016c671cb',
                            content: 'd63073a79ad560d499f862d47356270bda14896b5acbba8c8a24a13bdefbb3ca'
                            },
                            iat: 1626281543
                        },
         *      2/ TOKEN USER MOBILE 
                            decoded: {
                                gender: 0,
                                auth_paper: 1,
                                status: 1,
                                _id: 60cf72db4d87c07f91bc3141,
                                name: 'Nguyen Phuoc Nguyen',
                                number_auth_paper: '285687087122',
                                issued_by: 'Bình Phước', '
                                ...
         */
        if (isAuth) {

            // WRITE SOCKET ON HERE...

            socket.on("CSS_ADD_CONTACT", async data => {
                let { fullname, content, contactID } = data;

                let titleDefault = `${fullname} đã gửi cho bạn một Liên hệ mới`;
                let listUserReceiveContact = await USER_MODEL.getListAdmin({ });

                let listUserIDReceive = listUserReceiveContact && listUserReceiveContact.data.map(user => user._id);

                let notificationAfterInsert = [];

                let obj = {
                    title: titleDefault,
                    description: content,
                    type: 1,
                    url: `/contact/detail/${contactID}`
                }
                if (!tokenFake && !decoded.level) {
                    obj = {
                        ...obj,
                        customer: decoded._id
                    }
                }

                for (const userID of listUserIDReceive) {
                    let notificationInsert = await NOTIFICATION_MODEL.insertV2({
                        ...obj, receiver: userID,
                    });
                    notificationAfterInsert.push(notificationInsert.data)
                }
                mapSocketIDAndData(listUserIDReceive, SSC_SEND_NOTIFICATION_CONTACT, {
                    title: titleDefault, content, notificationAfterInsert
                }, usersConnected, io);
            })

            socket.on(CSS_CONFIRM_TRANSACTION_REQUEST_ENDUSER, async data => {
                let { agencyID, userID, userName, type, transactionID, customerID } = data;
                let infoAgency = await AGENCY_COLL
                    .findById(agencyID)
                    .select('name')
                    .lean();

                let title     = type == 3 ? "DUYỆT GIAO DỊCH" : "KHÔNG DUYỆT GIAO DỊCH";
                let string    = type == 3 ? "duyệt giao dịch" : "không duyệt giao dịch";
                let content   = `${infoAgency.name} đã ` + string;
                let link	  = `${transactionID}`;

                let listNotiMapInfo = await NOTIFICATION_MODEL.insertV2({
                    title,
                    description: content,
                    sender: userID,
                    customerReceiver: customerID,
                    type: type,
                    url: link,
                    // agency: agencyID
                });

                console.log({
                    listNotiMapInfo,
                    customerID
                });
             
                mapSocketIDAndData([customerID], SSC_CONFIRM_TRANSACTION_REQUEST_ENDUSER, {
                    title: title, content, 
                    // listNotiMapInfo: listNotiMapInfo.data, 
                    userSend: userID, 
                    transactionID, 
                    customerReceive: customerID
                }, usersConnected, io);

                let listUserNotConnected = getUserNotConnected(usersConnected, [customerID]);
                let body = {
                    screen_key: 'DetailDealScreen',
                    sender: userID,
                    transactionID: transactionID,
                    agencyID: agencyID,
                };
                // console.log({
                //     body
                // });
                
                sendMessageCustomerMobile({ 
                    title: title , 
                    description: " (Đại lý " + infoAgency.name + ") " + content,
                    arrReceiverID: listUserNotConnected, 
                    senderID: userID, 
                    body 
                });
            })  

            socket.on(CSS_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST_ENDUSER, async data => {
                let { agencyID, userID, userName, type, transactionID, customerID } = data;
                let infoAgency = await AGENCY_COLL
                    .findById(agencyID)
                    .select('name')
                    .lean();

                let title     = type == 6 ? "DUYỆT CHUỘC ĐỒ" : "KHÔNG DUYỆT CHUỘC ĐỒ";
                let string    = type == 6 ? "duyệt chuộc đồ" : "không duyệt chuộc đồ";
                let content   = `${infoAgency.name} đã ` + string;
                let link	  = `${transactionID}`;

                let listNotiMapInfo = await NOTIFICATION_MODEL.insertV2({
                    title,
                    description: content,
                    sender: userID,
                    customerReceiver: customerID,
                    type: type,
                    url: link,
                    // agency: agencyID
                });
             
                mapSocketIDAndData([customerID], SSC_CONFIRM_INJECT_TRANSACTION_RANSOM_REQUEST_ENDUSER, {
                    title: title, content, 
                    // listNotiMapInfo: listNotiMapInfo.data, 
                    userSend: userID, 
                    transactionID, 
                    customerReceive: customerID
                }, usersConnected, io);

                let listUserNotConnected = getUserNotConnected(usersConnected, [customerID]);
                let body = {
                    screen_key: 'DetailDealScreen',
                    sender: userID,
                    transactionID: transactionID,
                    agencyID: agencyID,
                };
                // console.log({
                //     body
                // });
                
                sendMessageCustomerMobile({ 
                    title: title , 
                    description: " (Đại lý " + infoAgency.name + ") " + content,
                    arrReceiverID: listUserNotConnected, 
                    senderID: userID, 
                    body 
                });
            })  
        }

        socket.on('disconnect', function () {
            console.log(`Disconnected..., Socket ID: ${socket.id}`);
             /**
             * ADD USER INTO usersConnected
             * usersConnected
             */
            let { id: socketIDDisconnect, decoded } = socket;
            if (!decoded || !decoded._id) return;

            let { _id: userIDDisconnect } = decoded;
            
            let usersConnectedGlobal = usersConnectedInstance.usersOnline;

            let itemDisconnect          = usersConnectedGlobal.find(itemSocket => itemSocket.userID == userIDDisconnect);

            // console.log({
            //     [`thông tin trước khi disconnect`]: itemDisconnect,
            //     [`số lượng socketID trước khi disconnect`]: itemDisconnect.socketID.length
            // })

            let listItemStillConnect    = usersConnectedGlobal.filter(itemSocket => itemSocket.userID != userIDDisconnect);
        
            let listSocketIDOfItemDisconnectAfterRemoveSocketDisconnect = itemDisconnect.socketID.filter(socketID => socketID != socketIDDisconnect);

            let itemDisconnectAfterRemoveSocketID = {
                ...itemDisconnect, socketID: listSocketIDOfItemDisconnectAfterRemoveSocketDisconnect
            }
            let listUserConnectAfterRemoveSocketDisconnect = [
                ...listItemStillConnect, itemDisconnectAfterRemoveSocketID
            ]

            // console.log({
            //     [`thông tin sau khi disconnect`]: itemDisconnectAfterRemoveSocketID,
            //     [`số lượng socketID sau khi disconnect`]: itemDisconnectAfterRemoveSocketID.socketID.length
            // })

            usersConnectedInstance.setUsersOnline(listUserConnectAfterRemoveSocketDisconnect);
        })
    });
};
