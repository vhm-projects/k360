"use strict";

const BASE_COLL = require('./intalize/base-coll');

/*
 * COLLECTION WARD CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('ward', {
    stt: Number,
    code: String,
    description: String,
    parent: String
})
