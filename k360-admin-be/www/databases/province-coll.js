"use strict";

const BASE_COLL = require('./intalize/base-coll');

/*
 * COLLECTION PROVINCE CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('province', {
    code: String,
    description: String,
    parent: String
})

