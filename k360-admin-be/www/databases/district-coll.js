"use strict";

const BASE_COLL = require('./intalize/base-coll');

/*
 * COLLECTION DISTRICT CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('district', {
    code: String,
    description: String,
    parent: String
})
