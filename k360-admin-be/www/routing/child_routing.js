"use strict";

/**
 * EXTERNAL PACKAGE
 */
const express        = require('express');
const moment         = require('moment');
const lodash         = require('lodash');
const url            = require('url');
const formatCurrency = require('number-format.js');

const { CF_ROUTINGS_COMMON } 							= require('../packages/common/constant/common.uri');
const { CF_ROUTINGS_PERMISSION: CF_ROUTINGS_ADMIN } 	= require('../packages/permissions/constants/permissions.uri');
const { CF_ROUTINGS_PRODUCT_CATEGORY } 	            	= require('../packages/product_category/constants/product_category.uri');
const { CF_ROUTINGS_AGENCY } 	                        = require('../packages/agency/constants/agency.uri');
const { CF_ROUTINGS_SLIDE } 	                        = require('../packages/slide/constants/slide.uri');
const { CF_ROUTINGS_BLOG }                            	= require('../packages/blog/constants/blog.uri');
const { CF_ROUTINGS_CONTACT }                         	= require('../packages/contact/constants/contact.uri');
const { CF_ROUTINGS_TRANSACTION }                     	= require('../packages/transaction/constant/transaction.uri');
const { CF_ROUTINGS_NOTIFICATION }                    	= require('../packages/notification/constants/notification.uri');
const { CF_ROUTINGS_STATISTICAL }                     	= require('../packages/statistical/constants/statistical.uri');
const { CF_ROUTINGS_FAQ }                             	= require('../packages/faq/constants/faq.uri');
const { CF_ROUTINGS_INVENTORY }                       	= require('../packages/inventory_session/constants/inventory_session.uri');
const { ADMIN_DOMAIN, AGENCY_DOMAIN, ENDUSER_DOMAIN } 	= require('../config/cf_domain');      
const { SPECIAL_CATEGORY } 								= require('../config/cf_constants');

const NOTIFICATION_COLL = require('../packages/notification/databases/notification-coll')({});


class ChildRouter{
    constructor(basePath) {
        this.basePath = basePath;
        this.registerRouting;
    }

    registerRouting() {
    }
    exportModule() {
        let router = express.Router();
        
        for (let basePath in this.registerRouting()) {
            const item = this.registerRouting()[basePath];
    
            if (typeof item.methods.post !== 'undefined' && item.methods.post !== null) {
                if (item.methods.post.length === 1) {
                    router.post(basePath, item.methods.post[0]);
                } else if (item.methods.post.length === 2) {
                    router.post(basePath, item.methods.post[0], item.methods.post[1]);
                }
            }

            if (typeof item.methods.get !== 'undefined' && item.methods.get !== null) {
                if (item.methods.get.length === 1) {
                    router.get(basePath, item.methods.get[0]);
                } else if (item.methods.get.length === 2) {
                    router.get(basePath, item.methods.get[0], item.methods.get[1]);
                }
            }

            if (typeof item.methods.put !== 'undefined' && item.methods.put !== null) {
                if (item.methods.put.length === 1) {
                    router.put(basePath, item.methods.put[0]);
                } else if (item.methods.put.length === 2) {
                    router.put(basePath, item.methods.put[0], item.methods.put[1]);
                }
            }

            if (typeof item.methods.delete !== 'undefined' && item.methods.delete !== null) {
                if (item.methods.delete.length === 1) {
                    router.delete(basePath, item.methods.delete[0]);
                } else if (item.methods.delete.length === 2) {
                    router.delete(basePath, item.methods.delete[0], item.methods.delete[1]);
                }
            }

        }
        return router;
    }

    /**
     * 
     * @param {*} msg 
     * @param {*} res 
     * @param {*} data 
     * @param {*} code
     * @param {*} status  tham số nhận biết lỗi
     */
    static responseError(msg, res, data, code, status) {
        if (code) {
            res.status(code);
        }
        return res.json({error: true, message: msg, data: data, status: status});
    }

    static response(response, data) {
        return response.json(data);
    }


    static responseSuccess(msg, res, data, code) {
        if (code) {
            res.status(code);
        }

        return res.json({error: false, message: msg, data: data});
    }

    static pageNotFoundJson(res) {
        res.status(404);
        return res.json({"Error": "Page not found!"});
    }

     /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} data 
     * @param {*} title 
     */
    static async renderToView(req, res, data, title) {
        data = typeof data === 'undefined' || data === null ? {} : data;

        if (title) {
            res.bindingRole.config.title = title;
        }

        data.render = res.bindingRole.config;
        data.url = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl
        });
		data.hrefCurrent = req.originalUrl;

		//_________Thư viện thời gian
		data.moment = moment;
        data._lodash = lodash;
        data.formatCurrency = formatCurrency;

        if(req.user){
		    data.infoUser = req.user;

            // Return notifications
            const userID = req.user._id;
            const listNotifications = await NOTIFICATION_COLL
                .find({ receiver: userID, status: 0 })
                .sort({ createAt: -1 })
                .limit(30)
                .lean();

            const totalUnSeenNotifications = await NOTIFICATION_COLL.countDocuments({
                receiver: userID,
                status: 0
            })

            data.listNotifications          = listNotifications;
            data.totalUnSeenNotifications   = totalUnSeenNotifications;
        }
        
        /**
         * langSession.saveLang(req.session, langKey)
         */

        // if (userSession.isLogin(req.session)) {
        //     data.langKey = userSession.getUser(req.session).lang;
        // } else {
        //     data.langKey = langSession.getLang(req.session);
        // }\

		data.CF_ROUTINGS_COMMON				= CF_ROUTINGS_COMMON;
        data.CF_ROUTINGS_ADMIN              = CF_ROUTINGS_ADMIN;
        data.CF_ROUTINGS_PRODUCT_CATEGORY   = CF_ROUTINGS_PRODUCT_CATEGORY;
        data.CF_ROUTINGS_AGENCY             = CF_ROUTINGS_AGENCY;
        data.CF_ROUTINGS_SLIDE              = CF_ROUTINGS_SLIDE;
        data.CF_ROUTINGS_BLOG               = CF_ROUTINGS_BLOG;
        data.CF_ROUTINGS_CONTACT            = CF_ROUTINGS_CONTACT;
        data.CF_ROUTINGS_TRANSACTION        = CF_ROUTINGS_TRANSACTION;
        data.CF_ROUTINGS_NOTIFICATION       = CF_ROUTINGS_NOTIFICATION;
        data.CF_ROUTINGS_STATISTICAL        = CF_ROUTINGS_STATISTICAL;
        data.CF_ROUTINGS_FAQ                = CF_ROUTINGS_FAQ;
        data.CF_ROUTINGS_INVENTORY          = CF_ROUTINGS_INVENTORY;
        
		// CONSTANTS
		data.SPECIAL_CATEGORY = SPECIAL_CATEGORY;

        // DOMAIM SOCKET
        data.ADMIN_DOMAIN                   = ADMIN_DOMAIN;
        data.AGENCY_DOMAIN                  = AGENCY_DOMAIN;
        data.ENDUSER_DOMAIN                 = ENDUSER_DOMAIN;

        let langDemo = 'en';
        data.langKey = langDemo;
        // console.log({
        //     [`res.bindingRole.config`]: res.bindingRole.config
        // });
        // =============BIND ALL DATA TO VIEW=============//
        return res.render(res.bindingRole.config.view, data);
    }

    static renderToPath(req, res, path, data) {
        data = data == null ? {} : data;
        data.render = res.bindingRole.config;
        return res.render(path, data);
    }

    static renderToViewWithOption(req, res, pathRender, data) {
        return res.render(pathRender, {data});
    }

    static redirect(res, path) {
        return res.redirect(path);
    }
}

module.exports = ChildRouter;