"use strict";

const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION CUSTOMER CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'customer' }){
   return BASE_COLl(name, {
        name: {
            type: String,
        }, 
        /**
         * Giới tính
         * 1: Nam
         * 0: Nữ
         */
        gender: {
            type: Number,
            default: 0
        },
        password: String, 
        
        birthday:{
            type: Date
        },
        phone: {
            type: String,
        },
        job: {
            type: String,
        },
        address: {
            type: String,
        },
        /**
         * Thành phốs
         */
        city: {
            type: String,
        },
        /**
         * Quận
         */
        district: {
            type: String,
        },
        /**
         * Phường
         */
        ward: {
            type: String,
        },
        /**
         * Loại giấy xác nhận
         * 0: CMND
         * 1: Bằng lái xe
         * 2: Passport
         */
        auth_paper: {
            type: Number,
            default: 0
        },
        /**
         * Số giấy xác thực
         */
        number_auth_paper: {
            type: String,
        },
        /**
         * Ngày cấp
         */
        issued_on:{
            type: Date
        },
        /**
         * Nơi cấp
         */
        issued_by:{
            type: String,
        },
        note: {
            type: String,
        },
        /**
         * Hình ảnh khách hàng
         */
        image_customer: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],
        /**
         * Hình ảnh giấy xác thực
         */
        image_auth_paper: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],
        
        // agency: {
        //     type: Schema.Types.ObjectId,
        //     ref: 'agency'
        // },
        
        /**
         * Trạng thái hoạt động.
         * 1. Hoạt động
         * 0. Khóa
         */
        status: {
            type: Number,
            default: 1
        },
        /**
         * Mã khách hàng
         */
        code: {
            type: Number,
        },
        devices: [{
			deviceName      :{ type : String }, 
			deviceID        :{ type : String },
			registrationID  :{ type : String }
		}],
        ...fields
    });
}
