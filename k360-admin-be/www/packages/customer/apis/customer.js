"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs   = require('fs');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
// const { uploadCusAndAuthPaper }                     = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_CUSTOMER }                      = require('../constant/customer.uri');
// const { KIND_OF_AUTH_PAPER, TYPE_GENDER, TYPE_TRANSACTION }           = require('../../../config/cf_constants');
// const { subStractDateGetDay }                       = require('../../../config/cf_time');

/**
 * MODELS, COLLECTIONS
 */  
const CUSTOMER_MODEL                                = require('../models/customer').MODEL;
const { IMAGE_MODEL }                               = require('../../image');
const { provinces }                                 = require('../../common/constant/provinces');
const { districts }                                 = require('../../common/constant/districts');
// const { TRANSACTION_MODEL }                         = require('../../transaction');
// const { INTEREST_RATE_MODEL }                       = require('../../interest_rate');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ GROUP PERMISSION ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Tạo khách hàng (API ENDUSER)
             * Dev: SonLP
             */
            [CF_ROUTINGS_CUSTOMER.ADD_CUSTOMER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { username, password, phone } = req.body;
                        let agencyID = req.agencyID;

                        let infoCustomer;

                        let infoData = await CUSTOMER_MODEL.getInfoByPhone({ phone });

                        if(infoData.error == false && infoData.data.length && infoData.data[0].password) {
                            return res.json({ error: true, message: "customer_password_existed" });
                        }

                        if(infoData.error == false && infoData.data.length && !infoData.data[0].password) {
                            return res.json({ error: true, message: "phone_existed" });
                        }

                        // if(infoData.error == false && infoData.data.length && !infoData.data[0].password) {
                        //     infoCustomer = await CUSTOMER_MODEL.updateCustomerByPhone({ customerID: infoData.data[0]._id, username, password, phone })
                        // }else{
                            infoCustomer = await CUSTOMER_MODEL.insertCustomerPassword({ username, password, phone })
                        // }
                        
                        res.json({ ...infoCustomer });
                    }]
                },
            },
            /**
             * Function: Tạo khách hàng (API ENDUSER)
             * Dev: SonLP
             */
            [CF_ROUTINGS_CUSTOMER.CHANGE_PASSWORD]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { password, phone } = req.body;
                        let agencyID = req.agencyID;
                        let infoData = await CUSTOMER_MODEL.getInfoByPhone({ phone });
                        
                        let infoCustomer;
                        if(infoData.error == true || !infoData.data.length) {
                            return res.json({ error: true, message: "info_customer_invalid" });
                        }else{
                            infoCustomer = await CUSTOMER_MODEL.updatePassword({ customerID: infoData.data[0]._id, password })
                        }
                        
                        res.json({ ...infoCustomer });
                    }]
                },
            },

            [CF_ROUTINGS_CUSTOMER.LOGIN]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { password, phone, deviceID, deviceName, registrationID } = req.body;

                        let infoData = await CUSTOMER_MODEL.loginCustomer({ phone, password, deviceID, deviceName, registrationID });
                        if(infoData.error) {
                            return res.json(infoData);
                        }

                        let listProvince        = Object.entries(provinces);
                        let listDistricts       = Object.entries(districts);

                        let city;
                        if(infoData.data.user.city) {
                            for(let province of listProvince) {
                                if(province[1].code == infoData.data.user.city) {
                                    city = province[1].name_with_type;
                                    break;
                                }
                            }
                            // infoData.data.user.city = city;
                        } 

                        let districtName;
                        if(infoData.data.user.district) {
                            for(let district of listDistricts) {
                                if(district[1].code == infoData.data.user.district ){
                                    districtName = district[1].name_with_type;
                                    break;
                                }
                            }
                            // infoData.data.user.district = districtName;
                        }
                        infoData.data.user = {
                            ...infoData.data.user,
                            cityName: city,
                            districtName
                        }
                        res.json({ ...infoData });
                    }]
                },
            },

            /**
             * Function: update khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
             [CF_ROUTINGS_CUSTOMER.UPDATE_ACCOUNT_CUSTOMER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userCreate } = req.user;

                        const { 
                            name, gender, birthday, phone, job, address, city, district, ward, auth_paper, 
                            number_auth_paper, issued_on, issued_by, note, status, 
                            image_customer_old, 
                            image_auth_paper_old, 
                            image_customer, image_auth_paper,
                            imageCustomerIsDeleted, imageAuthPaperIsDeleted, 
                        } = req.body;
                        // console.log({ data: req.body });
                        const { customerID } = req.params;
                        // const file           = req.files;
                        // console.log({ gender });
                        let imageCustomer = [];
                        let imageAuth = [];
                       
                        //Image Customer
                        if( image_customer_old ){
                            // return res.json({ error: true, message: "image_customer_old_old_invalid" })
                            image_customer_old.forEach( imgCus => {
                                imageCustomer.push(imgCus);
                            })
                        }

                        /**
                         * Xóa image khách hàng trong coll IMAGE
                         */
                        if( imageCustomerIsDeleted ){
                            for( let imgCus of imageCustomerIsDeleted ){
                                let infoImageCustomerAfterDelete = await IMAGE_MODEL.delete({ imageID: imgCus });
                            }
                        }
                        //fs.unlinkSync
                        //Image Auth Paper
                        if( image_auth_paper_old ){
                            // return res.json({ error: true, message: "image_auth_paper_old_invalid" })
                            image_auth_paper_old.forEach( imgAuth => {
                                imageAuth.push(imgAuth);
                            })
                        }
                        // /**
                        //  * Xóa image giấy xác thực trong coll IMAGE
                        //  */
                        if( imageAuthPaperIsDeleted ){
                            for( let imgAuth of imageAuthPaperIsDeleted ){
                                let infoImageAuthAfterDelete = await IMAGE_MODEL.delete({ imageID: imgAuth });
                            }
                        }

                        // if ( !image_customer || !image_auth_paper ){
                        //     return res.json({ error: true, message: "image_not_completely" })
                        // }
                        if ( image_customer ) {
                            for (let imgCustomer of image_customer){
                                let infoImageCustomerAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgCustomer,
                                    path: imgCustomer,
                                    userCreate: userCreate
                                });
                                imageCustomer[imageCustomer.length] = infoImageCustomerAfterInsert.data._id;
                            }
                        }
                        
                        if ( image_auth_paper ){
                            for (let imgAuthPaper of image_auth_paper){
                                let infoImageAuthPaperAfterInsert = await IMAGE_MODEL.insert({ 
                                    name: imgAuthPaper,
                                    path: imgAuthPaper,
                                    userCreate: userCreate
                                });
                                imageAuth[imageAuth.length] = infoImageAuthPaperAfterInsert.data._id;
                            }
                        }
                       
                        // console.log({ imageAuth, imageCustomer });
                        let infoAccountCustomer = await CUSTOMER_MODEL.update({ 
                            customerID, name, gender, birthday, phone, job, address, city, district, 
                            ward, auth_paper, number_auth_paper, issued_on, issued_by, 
                            note, image_customer: imageCustomer, image_auth_paper: imageAuth, status
                        });
                        if(infoAccountCustomer.error == true) {
                            return res.json({infoAccountCustomer});
                        }

                        let listProvince        = Object.entries(provinces);
                        let listDistricts       = Object.entries(districts);

                        let cityName;
                        if(infoAccountCustomer.data.city) {
                            for(let province of listProvince) {
                                if(province[1].code == infoAccountCustomer.data.city) {
                                    cityName = province[1].name_with_type;
                                    break;
                                }
                            }
                            // infoAccountCustomer.data.city = city;
                        } 

                        let districtName;
                        if(infoAccountCustomer.data.district) {
                            for(let district of listDistricts) {
                                if(district[1].code == infoAccountCustomer.data.district ){
                                    districtName = district[1].name_with_type;
                                    break;
                                }
                            }
                            // infoData.data.user.district = districtName;
                        }
                        infoAccountCustomer.data = {
                            ...infoAccountCustomer.data._doc,
                            cityName,
                            districtName
                        }

                        res.json({ infoAccountCustomer });
                    }]
                },
            },    
        }
    }
};
