/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');
const cfJWS                         = require('../../../config/cf_jws');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */
/**
 * COLLECTIONS
 */
const TRANSACTION_MODEL            = require('../../transaction/models/transaction').MODEL;

const CUSTOMER_COLL                = require('../database/customer-coll')({});

class Model extends BaseModel {
    constructor() {
        super(CUSTOMER_COLL);
    }

	insert({ name, gender, birthday, phone, job, address, city, district, ward, auth_paper, number_auth_paper, issued_on, issued_by, note, image_customer, image_auth_paper, code, agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });

                const dataInsert = {};
                if ( !name ){
                    return resolve({ error: true, message: 'name_not_valid' });
                }else{
                    dataInsert.name             = name;
                }
                /**
                 * Kiểm tra Loại xác thực
                 */
                if ( !auth_paper && !Number(auth_paper) ){
                    return resolve({ error: true, message: 'auth_paper_not_valid' });
                }else{
                    dataInsert.auth_paper             = Number(auth_paper);
                }
                /**
                 * Kiểm tra Số giấy xác thực
                 */
                // console.log({ number_auth_paper });
                if ( !number_auth_paper && !Number(number_auth_paper) ){
                    return resolve({ error: true, message: 'number_auth_paper_not_valid' });
                }
                
                if ( Number(auth_paper) == 0 && number_auth_paper.length != 10 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_10_character' });
                }
                if ( Number(auth_paper) == 1 && number_auth_paper.length != 12 ) {
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });
                }
                if ( Number(auth_paper) == 2 && number_auth_paper.length != 12 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });
                }
                
                dataInsert.number_auth_paper             = number_auth_paper;
                /**
                 * Kiểm tra ngày cấp giấy xác thực
                 */
                let checkDateIssuedOn = new Date(issued_on);
                if ( !issued_on || !checkDateIssuedOn ){
                    return resolve({ error: true, message: 'issued_on_not_valid' });
                }else{
                    dataInsert.issued_on             = checkDateIssuedOn;
                }
                /**
                 * Kiểm tra nơi cấp giấy xác thực
                 */

                if ( !issued_by ){
                    return resolve({ error: true, message: 'issued_by_not_valid' });
                }else{
                    dataInsert.issued_by             = issued_by;
                }
                /**
                 * Kiểm tra ảnh khách hàng
                 */
                if ( !image_customer && image_customer.length == 0 ){
                    return resolve({ error: true, message: 'image_customer_not_valid' });
                }else{
                    dataInsert.image_customer             = image_customer;
                }
                /**
                 * Kiểm tra ảnh giấy xác thực
                 */
                if ( !image_auth_paper && image_auth_paper.length == 0  ){
                    return resolve({ error: true, message: 'image_auth_paper_not_valid' });
                }else{
                    dataInsert.image_auth_paper           = image_auth_paper;
                }
                /**
                 * Kiểm tra phone có 10 ký tự 
                 */
                
                if ( phone && Number(phone) && phone.length == 10 && Number(phone) > 0){
                    dataInsert.phone = phone;
                }else{
                    return resolve({ error: true, message: "phone_invalid" });
                }
                /**
                 * Kiểm tra ngày sinh
                 */
                let checkDateBirthDay = new Date(birthday);
                if ( birthday && checkDateBirthDay ){
                    dataInsert.birthday             = checkDateBirthDay;
                }
                /**
                 * Kiểm tra thanh pho
                 */
                if ( !city ){
                    return resolve({ error: true, message: "city_invalid" });
                }else{
                    dataInsert.city             = city;
                }
                /**
                 * Kiểm tra thanh pho
                */
                if ( !code ){
                    return resolve({ error: true, message: "code_invalid" });
                }else{
                    let checkExistCode = await CUSTOMER_COLL.findOne({ code: Number(code) });
                    if ( checkExistCode ){
                        return resolve({ error: true, message: "info_customer_have_code_exist" });
                    }else{
                        dataInsert.code             = Number(code);
                    }
                }
                address         && (dataInsert.address          = address);
                agencyID        && (dataInsert.agency           = agencyID);
                gender          && (dataInsert.gender           = gender);
                job             && (dataInsert.job              = job);
                district        && (dataInsert.district         = district);
                ward            && (dataInsert.ward             = ward);
                note            && (dataInsert.note             = note);
                // console.log({dataInsert  });
                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'register_customer_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ customerID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoCustomer = await CUSTOMER_COLL.findById(customerID)
                    .populate({
                        path: 'image_customer',
                        select:'_id name path'
                    })
                    .populate({
                        path: 'image_auth_paper',
                        select:'_id name path'
                });
                if(!infoCustomer)
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByPhone({ phone }) {
        return new Promise(async resolve => {
            try {
                if(!phone)
                    return resolve({ error: true, message: "param_not_valid" });

                const infoCustomer = await CUSTOMER_COLL.find({ phone });
                if(!infoCustomer)
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // Lấy thông tin để check số điện thoại có tồn tại
    getInfoPhone({ phone }) {
        return new Promise(async resolve => {
            try {
                if( !phone || phone.length != 10 || !Number(phone)){
                    return resolve({ error: true, message: "phone_invalid" });
                }
                
                const infoCustomer = await CUSTOMER_COLL.findOne({ phone: phone });
                    
                if(infoCustomer)
                    return resolve({ error: true, message: "phone_existed" });

                return resolve({ error: false, message: "phone_inexisted", data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     * Kiểm tra thông tin khách hàng bằng code
     */
    getInfoCode({ code }) {
        return new Promise(async resolve => {
            try {
                if( !code ){
                    return resolve({ error: true, message: "code_invalid" });
                }
                const infoCustomer = await CUSTOMER_COLL.findOne({ code: Number(code) });
                    
                if(!infoCustomer)
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: infoCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({  }){
        return new Promise(async resolve => {
            try {
                const listCustomer = await CUSTOMER_COLL.find({});
                if(!listCustomer)
                    return resolve({ error: true, message: "not_found_accounts_list" });
                
                return resolve({ error: false, data: listCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ customerID, name, gender, birthday, phone, job, address, city, district, ward, auth_paper, number_auth_paper, issued_on, issued_by, note, image_customer, image_auth_paper, status }) {
        return new Promise(async resolve => {
            try {
                const dataUpdateUser = {};
                if ( name ){
                    dataUpdateUser.name             = name;
                }
                if( !ObjectID.isValid(customerID) )
                    return resolve({ error: true, message: "param_not_valid" });
                /**
                 * Kiểm tra Loại xác thực
                 */
                if (auth_paper && Number(auth_paper) || auth_paper == 0){
                    dataUpdateUser.auth_paper             = auth_paper;
                }
                /**
                 * Kiểm tra Số giấy xác thực
                 */
                // if ( number_auth_paper ){
                //     dataUpdateUser.number_auth_paper             = number_auth_paper;
                // }
                if ( !number_auth_paper && !Number(number_auth_paper) ){
                    // return resolve({ error: true, message: 'number_auth_paper_not_valid' });
                
                }else if ( Number(auth_paper) == 0 && number_auth_paper.length != 9 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_9_character' });
                
                }else if ( Number(auth_paper) == 1 && number_auth_paper.length != 12 ) {
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });
                
                }else if ( Number(auth_paper) == 2 && number_auth_paper.length != 12 ){
                    return resolve({ error: true, message: 'number_auth_paper_have_12_character' });

                }else{
                    let checkNumberAuth = await CUSTOMER_COLL.findOne({ number_auth_paper: number_auth_paper });
                    if(checkNumberAuth && checkNumberAuth._id.toString() != customerID.toString()) {
                        return resolve({ error: true, message: "number_auth_paper_existed" });
                    }
                    dataUpdateUser.number_auth_paper             = number_auth_paper;
                }
                /**
                 * Kiểm tra ngày cấp giấy xác thực
                 */
                if ( issued_on ){
                    let checkDateIssuedOn = new Date(issued_on);
                    dataUpdateUser.issued_on             = checkDateIssuedOn;
                }
                /**
                 * Kiểm tra nơi cấp giấy xác thực
                 */
                if ( issued_by ){
                    dataUpdateUser.issued_by             = issued_by;
                }
                /**
                 * Kiểm tra ảnh khách hàng
                 */
                if ( image_customer && image_customer.length ){
                    dataUpdateUser.image_customer             = image_customer;
                    
                }
                /**
                 * Kiểm tra ảnh giấy xác thực
                 */
                if ( image_auth_paper && image_auth_paper.length ){
                    dataUpdateUser.image_auth_paper           = image_auth_paper;
                }

                // if ( status && status != "undefined" ){
                //     dataUpdateUser.status           = Number(status);
                // }
                /**
                 * Kiểm tra phone có 10 ký tự -> 11 ký tự
                 */
                // if ( phone && Number(phone) && phone.length == 10){
                //     dataUpdateUser.phone = phone;
                // }
                /**
                 * Kiểm tra thanh pho
                 */
                if ( !city ){
                    // return resolve({ error: true, message: "city_invalid" });
                }else{
                    dataUpdateUser.city             = city;
                }

                if (!Number.isNaN(gender) || gender == 0) {
                    dataUpdateUser.gender           = gender
                }

                address         && (dataUpdateUser.address          = address);
                gender          && (dataUpdateUser.gender           = gender);
                birthday        && (dataUpdateUser.birthday         = new Date(birthday));
                job             && (dataUpdateUser.job              = job);
                // city            && (dataUpdateUser.city             = city);
                (dataUpdateUser.district         = district);
                (dataUpdateUser.ward             = ward);
                note            && (dataUpdateUser.note             = note);
                let dataAfterUpdate = await CUSTOMER_COLL.findByIdAndUpdate(customerID, {
                    ...dataUpdateUser
                }, {
                    new: true
                })
                    .populate({
                        path: "image_customer",
                        select: "_id path"
                    });
                if(!dataAfterUpdate) {
                    return resolve({ error: true, message: "cannot_update_customer" });
                }

                return resolve({ error: false, data: dataAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ customerID }) {
        return new Promise(async resolve => {
            try {
            if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_not_valid" });

                let checkTransaction = await TRANSACTION_MODEL.getInfoTransactionCustomerID({ customerID })
                // console.log({ checkTransaction });
                if(checkTransaction) {
                    return resolve({ error: true, message: "account_have_transaction" });
                }
                const infoAfterDelete = await CUSTOMER_COLL.findByIdAndRemove(customerID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "account_is_not_exists" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    filter({ agencyID, gender, city, district, ward, auth_paper, limit = 100, status }){
        return new Promise(async resolve => {
            try {
                const condition = {};

                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });

                condition.agency = agencyID;

                if ( gender ){
                    condition.gender = gender;
                }

                if ( city ){
                    condition.city = city;
                }

                if ( district ){
                    condition.district = district;
                }

                if ( ward ){
                    condition.ward = ward;
                }

                if ( auth_paper ){
                    condition.auth_paper = auth_paper;
                }

                if(status) {
                    condition.status = status;
                }
                // console.log({ condition });
                const listCustomer = await CUSTOMER_COLL.find(condition).sort({ createAt: -1 })
                    // .limit(limit);
                if( !listCustomer )
                    return resolve({ error: true, message: "not_found_customers_list" });

                return resolve({ error: false, data: listCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByAgency({ agencyID, status }){
        return new Promise(async resolve => {
            try {
                console.log({ agencyID, status });
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });
                let dataFilter = { agency: agencyID };

                if ( status ){
                    dataFilter.status = status
                }

                const listData = await CUSTOMER_COLL.find(dataFilter);
                if(!listData) 
                    return resolve({ error: true, message: "customer_is_not_exists" });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    randomStringFixLengthCode (count){
        let text = "";
        let possible = "012345678987654321";
        for (let i = 0; i < count; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    insertCustomerPassword({ agencyID, username, password, phone }) {
        return new Promise(async resolve => {
            try {
                if(!password) {
                    return resolve({ error: true, message: "password_not_valid" });
                }

                if(!phone) {
                    return resolve({ error: true, message: "phone_not_valid" });
                }

                if(!username) {
                    return resolve({ error: true, message: "username_not_valid" });
                }
                let hashPassword = await hash(password, 8);
                if (!hashPassword)
                    return resolve({ error: true, message: 'cannot_hash_password' });

                let dataInsert = { password: hashPassword, phone: `${phone}`, name: username };
                dataInsert.code = this.randomStringFixLengthCode(8);

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'register_customer_failed' });

                dataInsert && delete dataInsert.password;
                return resolve({ error: false, data: dataInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    updateCustomerByPhone({ customerID, agencyID, username, password, phone }){
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_not_valid" });

                if(!username) {
                    return resolve({ error: true, message: "username_not_valid" });
                }
                if(!password) {
                    return resolve({ error: true, message: "password_not_valid" });
                }
                if(!phone) {
                    return resolve({ error: true, message: "phone_not_valid" });
                }

                let hashPassword = await hash(password, 8);
                if (!hashPassword)
                    return resolve({ error: true, message: 'cannot_hash_password' });

                let dataUpdate = { password: hashPassword, phone, name: username };

                const infoAfterUpdate = await CUSTOMER_COLL.findByIdAndUpdate(customerID, {
                    ...dataUpdate
                },{
                    new: true
                });
                
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'register_customer_failed' });
                
                dataUpdate && delete dataUpdate.password;
                
                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    updatePassword({ customerID, password }){
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: "param_not_valid" });
                
                if(!password) {
                    return resolve({ error: true, message: "password_not_valid" });
                }
                
                let hashPassword = await hash(password, 8);
                if (!hashPassword)
                    return resolve({ error: true, message: 'cannot_hash_password' });

                let dataUpdate = { password: hashPassword };

                const infoAfterUpdate = await CUSTOMER_COLL.findByIdAndUpdate(customerID, {
                    ...dataUpdate
                },{
                    new: true
                });
                
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'change_password_failed' });

                dataUpdate && delete dataUpdate.password;
                dataUpdate = {
                    _id:   infoAfterUpdate._id,
                    name:  infoAfterUpdate.name,
                    phone: infoAfterUpdate.phone,
                }
                
                return resolve({ error: false, data: dataUpdate, message: "change_password_success" });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    

    loginCustomer({ phone, password, deviceName, deviceID, registrationID }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await CUSTOMER_COLL.findOne({ phone })
                    .populate({
                        path: "image_customer",
                        select: "_id path"
                    });
                if (!checkExists) 
                    return resolve({ error: true, message: 'phone_not_exist' });

                const isMatchPass = await compare(password, checkExists.password);
                if (!isMatchPass) 
                    return resolve({ error: true, message: 'password_is_wrong' });

                if (checkExists.status == 0) 
                    return resolve({ error: true, message: 'user_blocked' });

				// if(checkExists.level !== 0)
				// 	return resolve({ error: true, message: 'user_not_admin' });

                const infoUser = {
                    _id:        checkExists._id,
                    name:       checkExists.name,
                    gender:     checkExists.gender,
                    birthday:   checkExists.birthday,
                    phone:      checkExists.phone,
                    job:        checkExists.job,
                    address:    checkExists.address,
                    city:       checkExists.city,
                    district:   checkExists.district,
                    ward:       checkExists.ward,
                    auth_paper: checkExists.auth_paper,
                    number_auth_paper: checkExists.number_auth_paper,
                    issued_on:  checkExists.issued_on,
                    issued_by:  checkExists.issued_by,
                    note:       checkExists.note,
                    agency:     checkExists.agency,
                    status:     checkExists.status,
                    code:       checkExists.code,
                    image_customer:       checkExists.image_customer,
                    level:      3
                    // level: checkExists.level,
                }
                const token = jwt.sign(infoUser, cfJWS.secret);

                /**
                 * UPDATE LAST DEVICE (cập nhật device cuối cùng login)
                 * Nếu DeviceID tồn tại => cập nhật deviceName + fcm_token
                 */
                let devices     = checkExists.devices || [];
                let newDevices  = [];
                
                let itemNewDevice = { deviceName, deviceID, registrationID };
               
                if (devices.length) {
                    devices.forEach(device => {
                        let _device = device;
                        if (device.deviceID == itemNewDevice.deviceID) {
                            _device = {
                                deviceID :  device.deviceID, 
                                deviceName: device.deviceName, 
                                registrationID : registrationID
                            };
                        } 
                        newDevices.push(_device);
                    });
                    if (!newDevices.find(item => item.deviceID === itemNewDevice.deviceID)) 
                        newDevices.push(itemNewDevice)
                } else {
                    newDevices = [
                        itemNewDevice
                    ]
                }
            
                //--- END UPDATE DEVICES ---
                let infoAfterUpdateWithRegistration = await CUSTOMER_COLL.findByIdAndUpdate(checkExists._id, {
                    devices :  newDevices, 
                    modifyAt: Date.now(),
                    lastLog: Date.now()
                }, { new: true });
               
                return resolve({
                    error: false,
                    data: { user: infoUser, token }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message })
            }
        })
    }

    
    checkAuth({ token }) {
        return new Promise(async resolve => {
            try {
                let decoded = await jwt.verify(token, cfJWS.secret);
                if (!decoded) return resolve({ error: true, message: 'token_not_valid' });

                const { _id: userID } = decoded;
                
                if (!userID) return resolve({ error: true, message: 'token_not_valid' });

                const STATUS_ACTIVE_OF_USER = 1;
                let infoUserDB = await CUSTOMER_COLL.findOne({ status: STATUS_ACTIVE_OF_USER, _id: userID });

                if (!infoUserDB) 
                    return resolve({ error: true, message: 'token_not_valid' });

                return resolve({ error: false, data: infoUserDB });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;