const CUSTOMER_MODEL 		= require('./models/customer');
const CUSTOMER_COLL  		= require('./database/customer-coll')({});
const CUSTOMER_ROUTES       = require('./apis/customer');

module.exports = {
    CUSTOMER_ROUTES,
    CUSTOMER_COLL,
    CUSTOMER_MODEL: CUSTOMER_MODEL.MODEL,
	// CUSTOMER_MODEL_BASE: CUSTOMER_MODEL.AccountModel
}
