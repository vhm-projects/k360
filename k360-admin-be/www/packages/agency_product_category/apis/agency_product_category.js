"use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY } 		= require('../constant/agency_product_category.uri');

/**
 * MODELS
 */
const AGENCY_PRODUCT_CATEGORY_MODEL     = require('../models/agency_product_category').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ******************************* ================================
             * ========================== QUẢN LÝ AGENCY PRODUCT CATEGORY ================================
             * ========================== ******************************* ================================
             */

            /**
             * Function: Cập nhật giá danh mục cho đại lý (API)
             * Date: 20/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY.UPDATE_AGENCY_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate } = req.user;
                        const { agencyID, listCategoryID, price, status } = req.body;

                        const infoAfterUpdate = await AGENCY_PRODUCT_CATEGORY_MODEL.updateOrInsert({
							agencyID, listCategoryID, price, status, userUpdate
						})

                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Danh sách danh mục đại lý (API)
             * Date: 21/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY.LIST_AGENCY_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID, categoryID } = req.query;

                        const listPriceAgency = await AGENCY_PRODUCT_CATEGORY_MODEL.getList({
                            agencyID, categoryID
                        })

                        res.json(listPriceAgency);
                    }]
                },
            },

            /**
             * Function: Thông tin giá danh mục sản phẩm đại lý (API)
             * Date: 20/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_AGENCY_PRODUCT_CATEGORY.INFO_PRICE_AGENCY]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID, categoryID } = req.query;

                        const infoPriceAgency = await AGENCY_PRODUCT_CATEGORY_MODEL.getInfo({
                            agencyID, categoryID
                        })

                        res.json(infoPriceAgency);
                    }]
                },
            },
        }
    }
};
