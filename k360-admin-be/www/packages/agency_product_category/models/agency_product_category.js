"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const BaseModel                         = require('../../../models/intalize/base_model');
const { checkObjectIDs, isEmptyObj }                = require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const AGENCY_PRODUCT_CATEGORY_COLL      = require('../databases/agency-product-category-coll')({});

class Model extends BaseModel {
    constructor() {
        super(AGENCY_PRODUCT_CATEGORY_COLL);
    }

    getListParent({ agencyID, categoryID, status = 1 }){
        return new Promise(async resolve => {
            try {
                if(agencyID && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(categoryID && !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let condition = {};
                agencyID    && (condition.agency            = agencyID);
                categoryID  && (condition.product_category  = categoryID);
                status      && (condition.status            = status);

                const listPriceAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL
                    .find(condition)
                    .populate({
                        path: 'product_category',
                        match: {
                            parent: null
                        },
                        populate: {
                            path: 'image'
                        }
                    })
                    .lean();
                if(!listPriceAgencyCategory)
                    return resolve({ error: true, message: 'cannot_found_list_price_agency_category' });

                return resolve({ error: false, data: listPriceAgencyCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ agencyID, categoryID, status }){
        return new Promise(async resolve => {
            try {
                if(agencyID && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(categoryID && !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let condition = {};
                agencyID    && (condition.agency            = agencyID);
                categoryID  && (condition.product_category  = categoryID);
                status      && (condition.status            = status);

                const listPriceAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL
					.find(condition)
					.populate('agency product_category')
					.lean();

                if(!listPriceAgencyCategory)
                    return resolve({ error: true, message: 'cannot_found_list_price_agency_category' });

                return resolve({ error: false, data: listPriceAgencyCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAll({ categoryID, agencyID = null }){
        return new Promise(async resolve => {
            try {
                if(agencyID && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(categoryID && !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let condition = {
                    agency: agencyID
                };
                categoryID  && (condition.product_category  = categoryID);

                const listPriceAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL.find(condition)
                    .populate({
                        path: 'product_category',
                        select: "_id name"
                    })
                    .lean();
                if(!listPriceAgencyCategory)
                    return resolve({ error: true, message: 'cannot_found_list_price_agency_category' });

                return resolve({ error: false, data: listPriceAgencyCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ agencyID, categoryID }){
        return new Promise(async resolve => {
            try {
                if(!agencyID || !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(!categoryID || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const infoPriceAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL.findOne({ 
                    agency: agencyID, 
                    product_category: categoryID 
                });
                if(!infoPriceAgencyCategory)
                    return resolve({ error: true, message: 'cannot_found_price_agency_category' });

                return resolve({ error: false, data: infoPriceAgencyCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	calculatePrice({ agencyID, categories }){
		return new Promise(async resolve => {
			try {
				if(!categories || !categories.length){
					return resolve({ error: true, message: 'categories_not_exists' });
				}
				if(!agencyID || !ObjectID.isValid(agencyID)){
					return resolve({ error: true, message: 'agency_not_exists' });
				}

				let listAgencyCategory = await this.getList({ 
					agencyID, status: 1
				});
				let listCategoryID 		= categories.map(category => category.categoryID);
				let specialCategory 	= 0;
				let noBillOrNonBrand 	= false;
				let infoAgency			= {};

				let categoryOfAgency = listAgencyCategory.data && listAgencyCategory.data.filter(agencyCategory => {
					let productCategory = agencyCategory.product_category;

					// Check have product_category && return agency categories exists
					if(productCategory && listCategoryID.includes(productCategory._id.toString())){

						// check is special category
						if(productCategory.specialCategory){
							switch (productCategory.specialCategory) {
								case 3: // Trang sức
									specialCategory = productCategory.specialCategory;
									break;
								case 2: // vàng nguyên liệu
								case 4: // Non brand
								case 5: // Không có hóa đơn
									noBillOrNonBrand = true;
									break;
								default:
									break;
							}
						}

						// Check agency exists && infoAgency{} is empty. Then, Set info agency
						if(agencyCategory.agency && isEmptyObj(infoAgency)){
							infoAgency = agencyCategory.agency;
						}

						return agencyCategory;
					}

				});

				let analysePrice = 0;
				switch (specialCategory) {
					case 0: // Normal
						analysePrice = categoryOfAgency.reduce((total, category) => total + +category.price, 0);
						analysePrice = analysePrice * categoryOfAgency
							.filter(category => category.percentValue)
							.reduce((total, category) => total * (category.percentValue / 100), 1);
						break;
					case 3: // Trang sức

						if(noBillOrNonBrand){ // Non brand || Không có hóa đơn || vàng nguyên liệu
							categories = categories.reverse();
							let goldWeight 	= categories.find(category => category.type == 2);
							let goldType 	= categories.find(category => category.type == 3);
							let goldPrice	= 0;

							categoryOfAgency.map(agencyCategory => {
								const systemCategory = agencyCategory.product_category;

								if(systemCategory._id.toString() === goldType.categoryID){
									const dropDown = agencyCategory.dropDown || systemCategory.dropDown;
									goldPrice = dropDown.find(item => item._id.toString() == goldType.value);
								}
							})

							analysePrice = goldWeight.value * goldPrice.value * (infoAgency.percentPawn / 100);

						} else{ // Có hóa đơn
							let lastCategory 	 = categories[categories.length - 1];
							let categoryBillCost = categories.find(category => category.type == 2);
							let percentValueOfLastCategory = 0;

							categoryOfAgency.map(agencyCategory => {
								const agencyOfProduct = agencyCategory.product_category;

								if(agencyOfProduct._id.toString() === lastCategory.categoryID){
									percentValueOfLastCategory = agencyCategory.percentValue;
								}
							})
                            
							analysePrice = categoryBillCost.value * (percentValueOfLastCategory / 100) * (infoAgency.percentPawn / 100);
						}

						break;
					default:
						break;
				}

				return resolve({
					error: false,
					categories,
					data: analysePrice
				})
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
