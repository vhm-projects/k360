const BASE_ROUTE = '/notification';
const BASE_ROUTE_API = '/api/notification';

const CF_ROUTINGS_NOTIFICATION = {
    VIEW_LIST_NOTIFICATION: `${BASE_ROUTE}-core`,
    VIEW_LIST_NOTIFICATION_RECEIVE: `${BASE_ROUTE}/receive`,
    DETAIL_CORE_NOTIFICATION: `${BASE_ROUTE}-core/:notificationCoreID`,

    ADD_NOTIFICATION: `${BASE_ROUTE}/add-notification`,
    INFO_NOTIFICATION: `${BASE_ROUTE}/info-notification/:notificationCoreID`,
    REMOVE_NOTIFICATION: `${BASE_ROUTE}/remove-notification/:notificationID`,
    UPDATE_NOTIFICATION: `${BASE_ROUTE}/update-notification/:notificationID`,
    UPDATE_STATUS_NOTIFICATION: `${BASE_ROUTE}/update-status-notification/:notificationID`,
    FILTER_NOTIFICATION: `${BASE_ROUTE}/filter-notification`,
    SEEN_ALL_NOTIFICATION: `${BASE_ROUTE}/seen-all-notification`,

    VIEW_LIST_NOTIFICATION_API: `${BASE_ROUTE_API}/list-notification`,
	LIST_NOTIFICATIONS: `${BASE_ROUTE}/list-notifications`,

    /**
     * API MOBILE
     */
    LIST_NOTIFICATION_API:   `${BASE_ROUTE_API}/list-notification`,
    LIST_NOTIFICATION_CUSTOMER_API:   `${BASE_ROUTE_API}/list-notification-customer`,
    UPDATE_SEEN_USER_NOTIFICATION_API: `${BASE_ROUTE_API}/updata-all-notification-user`,
    UPDATE_STATUS_NOTIFICATION_CUSTOMER: `${BASE_ROUTE}/update-status-notification-customer/:notificationID`,


	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_NOTIFICATION = CF_ROUTINGS_NOTIFICATION;