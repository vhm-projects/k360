"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const moment            = require("moment");

/**
 * BASE
 */
const BaseModel          = require('../../../models/intalize/base_model');
const { checkObjectIDs } = require('../../../utils/utils');
/**
 * COLLECTIONS
 */
const NOTIFICATION_COLL = require('../databases/notification-coll')({});
const AGENCY_COLL  = require('../../agency/databases/agency-coll')({});

class Model extends BaseModel {
    constructor() {
        super(NOTIFICATION_COLL);
    }

    /*
    Thêm thông báo
    Dattv
     */
	/*
    Thêm thông báo
    Dattv
     */
	insert({ title, description, type, content, sender, receiver, arrayAgencyReceive, url, checkSendAll, customer }) {
        return new Promise(async resolve => {
            try {
                if(!title || !description)
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataInsert = {
                    title,
                    description,
                    type,
                    url,
                    content
                }
                sender      && (dataInsert.sender   = sender);
                receiver    && (dataInsert.receiver = receiver);
                customer    && (dataInsert.customer = customer);

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'send_notification_failed' });
                if(checkSendAll == 'true'){
                    let listAllAgency = await AGENCY_COLL.find({ status: 1 }).select("_id");
                    arrayAgencyReceive = listAllAgency.map(agency => agency._id);
                }

                if(arrayAgencyReceive.length > 0){
                    await NOTIFICATION_COLL.findByIdAndUpdate(infoAfterInsert._id, {
                        $addToSet: {
                            agencys_receive: {
                                $each: arrayAgencyReceive
                            }
                        }
                    }, { new: true })
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /**
     * FUNCTION: THÊM THÔNG BÁO
     * AUTHOR: SONLP
     * @param {*} param0 
     * @returns 
     */
    insertV2({ title, description, type, url, sender, receiver, arrayAgencyReceive, customer, customerReceiver }) {
        return new Promise(async resolve => {
            try {
                if(!title || !description)
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataInsert = {
                    title,
                    description,
                    type
                }
                sender      && (dataInsert.sender   = sender);
                receiver    && (dataInsert.receiver = receiver);
                url    		&& (dataInsert.url 		= url);
                arrayAgencyReceive    		&& (dataInsert.agencys_receive 		= arrayAgencyReceive);
                customer    && (dataInsert.customer = customer);
                customerReceiver    && (dataInsert.customerReceiver = customerReceiver);

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'send_notification_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    /*
    Lấy thông tin thông báo và đồng thời cập nhật đã xem
    Dattv
     */
    getInfo({ notificationCoreID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(notificationCoreID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoNotification = await NOTIFICATION_COLL.findById(notificationCoreID)
                .populate({ path: 'agencys_receive', select: 'name' })

                if(!infoNotification)
                    return resolve({ error: true, message: 'notification_is_not_exists' });

                // console.log({ agencyID })
                // let infoAgency = await AGENCY_COLL.findById(agencyID);
                // let { owners } = infoAgency;

                // //Cập nhật đã xem nếu đó là owner của đại lý
                // if(owners && owners.length && owners.includes(userID)){
                //     await NOTIFICATION_COLL.findByIdAndUpdate(notificationID, {
                //         $addToSet: { agencys_seen: agencyID }
                //     })
                // }

                return resolve({ error: false, data: infoNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Update đã xem tất cả thông báo của đại lý
    Dattv
     */
    updateSeenAllNotification({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'param_not_valid' });

                    let listNotificationOfAgency = await NOTIFICATION_COLL.find({
                        agencys_receive: { $in: [agencyID] }
                        //agencys_receive: { $elemMatch: { agencyID } }
                    });

                    for (let notification of listNotificationOfAgency) {
                        await NOTIFICATION_COLL.findByIdAndUpdate(notification._id, {
                            $addToSet: { agencys_seen: agencyID }
                        });
                    }

                return resolve({ error: false, data: listNotificationOfAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Lấy tất cả thông báo, bao gồm cả một đại lý
    Dattv
     */
    getList({ agencyID, start, end, status }){
        return new Promise(async resolve => {
            try {
                let conditionObj = { };

                if(ObjectID.isValid(agencyID))
                    conditionObj.agencys_receive = { $in: [agencyID] }

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate)
                    }
                }

                if(status) {
                    conditionObj.status = status;
                }

                const listNotification = await NOTIFICATION_COLL.find({
                    ...conditionObj, receiver: { $exists: false }, type: 0
                }).sort({ createAt: -1 }).lean()

                if(!listNotification)
                    return resolve({ error: true, message: 'not_found_notification_list' });

                return resolve({ error: false, data: listNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Lấy tất cả thông báo
    Dattv
     */
    getListNotificationTypeContact({ start, end, userID }){
        return new Promise(async resolve => {
            try {
                let conditionObj = { };

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate)
                    }
                }

                const listNotification = await NOTIFICATION_COLL.find({
                    ...conditionObj, receiver: userID
                }).sort({ createAt: -1 }).limit(30).lean()

                if(!listNotification)
                    return resolve({ error: true, message: 'not_found_notification_list' });

                return resolve({ error: false, data: listNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    /*
    Update thông báo
    Dattv
     */
    update({ notificationID, title, description, content }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(notificationID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const dataUpdate = {
                    title, 
                    description, 
                    content, 
                }
                
                let infoNotificationAfterUpdate = await NOTIFICATION_COLL.findByIdAndUpdate({ _id: notificationID }, dataUpdate);
                if(!infoNotificationAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_notification' });

                //let { _id } = infoNotificationAfterUpdate;

                // //Clear mảng agency cũ
                // await NOTIFICATION_COLL.findByIdAndUpdate(_id, { agencys_receive: [] });

                // if(checkSendAll == 'true'){
                //     let listAllAgency = await AGENCY_COLL.find({ status: 1 }).select("_id");
                //     arrayAgencyReceive = listAllAgency.map(agency => agency._id);
                // }
                
                // //Thêm các agency mới vào mảng
                // if(arrayAgencyReceive){
                //     //arrayAgencyReceive = arrayAgencyReceive.split(",");
                //     await NOTIFICATION_COLL.findByIdAndUpdate(_id, {
                //         $addToSet: {
                //             agencys_receive: {
                //                 $each: arrayAgencyReceive
                //             }
                //         }
                //     }, { new: true });
                // }

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Xóa thông báo
    Dattv
     */
    remove({ notificationID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(notificationID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await NOTIFICATION_COLL.findByIdAndDelete(notificationID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'notification_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /*
    Lọc danh sách thông báo từ ngày đến ngày
    Dattv
     */
    getListNotificationBetweenDay({ start, end }){
        return new Promise(async resolve => {
            try {

                let _fromDate   = moment(start).startOf('day').format();
                let _toDate     = moment(end).endOf('day').format();

                const listNotification = await NOTIFICATION_COLL.find({ 
                    createAt : { 
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate) 
                    }
                }).lean();

                if(!listNotification)
                    return resolve({ error: true, message: 'not_found_contacts_list' });

                return resolve({ error: false, data: listNotification });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListLoadMore({ skip, receiverID }){
		return new Promise(async resolve => {
			try {
				if(!ObjectID.isValid(receiverID) || isNaN(skip))
                    return resolve({ error: true, message: 'param_invalid' });

				const listNotification = await NOTIFICATION_COLL
					.find({ receiver: receiverID, status: 0 })
					.sort({ createAt: -1 })
					.limit(10)
					.skip(+skip)
					.lean();

				if(!listNotification)
					return resolve({ error: true, message: 'not_found_notify' });

				return resolve({ error: false, data: listNotification });
			} catch (error) {
				return resolve({ error: true, message: error.message });
			}
		})
	}

    /**
     * FUCNTION: LẤY DANH SÁCH THÔNG BÁO CỦA USER
     * AUHTOR: SONLP
     * @param {*} userID 
     * @param {*} page 
     * @param {*} start 
     * @param {*} end 
     * @returns {listNotifications}  
     */
    getListNotificationOfUserID({ userID, page, start, end }) {
        return new Promise(async resolve => {
            try {
                let limit = 10;
                const STATUS_INACTIVE = 0;

                let dataFind = {
                    receiver: userID
                };
                
                let createAt = {}
                if(start) {
                    createAt = { 
                        $gte: new Date(start), 
                    }
                }
                if (end) {
                    createAt = { 
                        ...createAt,
                        $lte: new Date(end),
                    }
                }
                if(createAt.$gte || createAt.$lte) {
                    dataFind.createAt =createAt;
                }

                let countListNotifyInactive = await NOTIFICATION_COLL.count({ status: STATUS_INACTIVE, receiver: userID })
                const listNotifications = await NOTIFICATION_COLL
                    .find({...dataFind}, { status: 1, title: 1, description: 1, createAt: 1, type: 1 })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ createAt: -1 })
                    // .limit(30)
                    .lean();
    
                // const totalUnSeenNotifications = await NOTIFICATION_COLL.countDocuments({
                //     receiver: userID,
                //     status: 0
                // })
                if(!listNotifications)
					return resolve({ error: true, message: 'not_found_notify' });

                // let data = {
                //     listNotifications,
                //     totalUnSeenNotifications
                // }
				return resolve({ error: false, data: listNotifications, countListNotifyInactive });
            } catch (error) {
				return resolve({ error: true, message: error.message });
            }
        })
    }

    getListNotificationOfCustomerID({ userID, page, start, end }) {
        return new Promise(async resolve => {
            try {
                let limit = 10;
                const STATUS_INACTIVE = 0;

                let dataFind = {
                    customerReceiver: userID
                };
                
                let createAt = {}
                if(start) {
                    createAt = { 
                        $gte: new Date(start), 
                    }
                }
                if (end) {
                    createAt = { 
                        ...createAt,
                        $lte: new Date(end),
                    }
                }
                if(createAt.$gte || createAt.$lte) {
                    dataFind.createAt =createAt;
                }

                let countListNotifyInactive = await NOTIFICATION_COLL.count({ status: STATUS_INACTIVE, customerReceiver: userID })
                const listNotifications = await NOTIFICATION_COLL
                    .find({...dataFind}, { status: 1, title: 1, description: 1, createAt: 1, type: 1, url: 1, customerReceiver: 1 })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ createAt: -1 })
                    // .limit(30)
                    .lean();
    
                // const totalUnSeenNotifications = await NOTIFICATION_COLL.countDocuments({
                //     receiver: userID,
                //     status: 0
                // })
                if(!listNotifications)
					return resolve({ error: true, message: 'not_found_notify' });

                // let data = {
                //     listNotifications,
                //     totalUnSeenNotifications
                // }
				return resolve({ error: false, data: listNotifications, countListNotifyInactive });
            } catch (error) {
				return resolve({ error: true, message: error.message });
            }
        })
    }

   /**
    * FUCNTION: UPDATE TẤT CẢ THÔNG BÁO CỦA USERID
    * @param {*} userID 
    * @returns 
    */
    updateSeenAllNotificationOfUser({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!checkObjectIDs(userID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const STATUS_INACTIVE = 0;
                const STATUS_ACTIVE   = 1;
                let listNotificationOfAgency = await NOTIFICATION_COLL.find({
                    receiver: userID, status: STATUS_INACTIVE
                });

                let listNotifyAfterUpdate = listNotificationOfAgency.map( notify => {
                    return NOTIFICATION_COLL.findByIdAndUpdate(notify._id, {
                        status: STATUS_ACTIVE
                    }, {
                        new: true
                    });
                })
                
                let resultAfterUpdate = await Promise.all(listNotifyAfterUpdate)

                return resolve({ error: false, data: resultAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
