"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_NOTIFICATION }      = require('../constants/notification.uri');

/**
 * MODELS
 */
const NOTIFICATION_MODEL                 = require("../models/notification").MODEL;
const AGENCY_MODEL                       = require("../../agency/models/agency").MODEL;
const NOTIFICATION_COLL                  = require("../databases/notification-coll")({});
const { provinces }                      = require('../../common/constant/provinces');
const { checkObjectIDs }                 = require('../../../utils/utils');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            
            /**
             * Function: Danh sách thông báo (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION,
					inc: path.resolve(__dirname, '../views/list_notification.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { agencyID, start, end } = req.query;
                        let listNotification = await NOTIFICATION_MODEL.getList({ agencyID, start, end });
                        let listProvince = Object.entries(provinces);
                        let listAgency = await AGENCY_MODEL.getList({ })
                        ChildRouter.renderToView(req, res, {
							listNotification: listNotification.data,
                            listAgency: listAgency.data,
                            listProvince,
                            agencyID,
                            start, end
						});
                    }]
                },
            },

            /**
             * Function: Chi tiết thông báo (View)
             * Date: 09/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.DETAIL_CORE_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `Detail Notification Core - K360`,
					code: CF_ROUTINGS_NOTIFICATION.DETAIL_CORE_NOTIFICATION,
					inc: path.resolve(__dirname, '../views/detail_core_notification.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { notificationCoreID } = req.params;
                        let { type }      = req.query;
                        let infoNotification = await NOTIFICATION_MODEL.getInfo({ notificationCoreID });
                        if (type && type === "api") {
                            return res.json(infoNotification);
                        }
                        ChildRouter.renderToView(req, res, {
                            infoNotification: infoNotification.data
						});
                    }]
                },
            },

            /**
             * Function: Danh sách thông báo liên hệ (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION_RECEIVE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION_RECEIVE,
					inc: path.resolve(__dirname, '../views/list_notification_receive.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { start, end, notificationID } = req.query;
                        let listNotification = await NOTIFICATION_MODEL.getListNotificationTypeContact({ start, end, userID });
                        //let infoNotiUpdateStatus = await NOTIFICATION_COLL.findByIdAndUpdate(notificationID, {status: 1}, {new: true});
                        ChildRouter.renderToView(req, res, {
							listNotification: listNotification.data,
                            start, end, notificationID
						});
                    }]
                },
            },


            //========================= JSON ============================
            /**
             * Function: Danh sách thông báo (JSON)
             * Dev: SONLP
             */
            [CF_ROUTINGS_NOTIFICATION.VIEW_LIST_NOTIFICATION_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { agencyID, start, end, status } = req.query;
                        let listNotification = await NOTIFICATION_MODEL.getList({ agencyID, customerID: userID, start, end, status });
                        // let listProvince  = Object.entries(provinces);
                        // let listAgency = await AGENCY_MODEL.getList({ })

                        res.json({
                            ...listNotification,
                            // listAgency: listAgency.data,
                            // listProvince,
                            // agencyID,
                            start, end
                        });
                    }]
                },
            },
            /**
             * Function: Insert Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.ADD_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { title, description, content, arrayAgencyReceive, checkSendAll } = req.body;
                        const infoAfterInsertNotification = await NOTIFICATION_MODEL.insert({ 
                            title, description, content, arrayAgencyReceive: JSON.parse(arrayAgencyReceive), checkSendAll
                        });
                        res.json(infoAfterInsertNotification);
                    }]
                },
            },

            /**
             * Function: Info Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.INFO_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { notificationCoreID } = req.params;
                        
                        const infoNotification = await NOTIFICATION_MODEL.getInfo({ notificationCoreID });

                        res.json(infoNotification);
                    }]
                },
            },

			/**
             * Function: List Notification of agency (API)
             * Date: 24/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_NOTIFICATION.LIST_NOTIFICATIONS]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { _id: receiverID } = req.user;
                        const { skip } = req.query;

                        const listNotifications = await NOTIFICATION_MODEL.getListLoadMore({ 
                            skip, receiverID
                        });
                        res.json(listNotifications);
                    }]
                },
            },

            /**
             * Function: Remove Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.REMOVE_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { notificationID } = req.params;
                        const infoNotificationRemove = await NOTIFICATION_MODEL.remove({ notificationID });
                        res.json(infoNotificationRemove);
                    }]
                },
            },

            /**
             * Function: Update Seen all Notification of Agency
             * Date: 03/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.SEEN_ALL_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID } = req.query;
                        const listNotificationSeenAll = await NOTIFICATION_MODEL.updateSeenAllNotification({ agencyID });
                        res.json(listNotificationSeenAll);
                    }]
                },
            },

            /**
             * Function: Update Seen all Notification of Agency (MOBILE)
             * Dev: SONLP 
             */
            //  [CF_ROUTINGS_NOTIFICATION.SEEN_ALL_NOTIFICATION]: {
            //     config: {
            //         auth: [ roles.role.admin.bin ],
            //         type: 'json',
            //     },
            //     methods: {
            //         get: [ async function (req, res) {
            //             const { agencyID } = req.query;
            //             const listNotificationSeenAll = await NOTIFICATION_MODEL.updateSeenAllNotification({ agencyID });
            //             res.json(listNotificationSeenAll);
            //         }]
            //     },
            // },

            /**
             * Function: Update Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
            [CF_ROUTINGS_NOTIFICATION.UPDATE_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { notificationID } = req.params;
                        const { title, description, content } = req.body;
                        const infoNotificationRemove = await NOTIFICATION_MODEL.update({ notificationID, title, description, content });
                        res.json(infoNotificationRemove);
                    }]
                },
            },

            /**
             * Function: Update STATUS Notification
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_NOTIFICATION.UPDATE_STATUS_NOTIFICATION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { notificationID } = req.params;

                        if (!checkObjectIDs(notificationID)){
                            return res.json({ error: true, message: "id_invalid" })
                        }

                        const infoNotificationUpdate = await NOTIFICATION_COLL.findByIdAndUpdate(notificationID, {status: 1}, {new: true});
                        res.json(infoNotificationUpdate);
                    }]
                },
            },

            //============================== API MOBILE =========================

            /**
             * Function: Danh sách thông báo của USERID
             * Date: 04/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_NOTIFICATION.LIST_NOTIFICATION_API]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { agencyID, start, end, page } = req.query;
                        let listNotification = await NOTIFICATION_MODEL.getListNotificationOfUserID({ userID, page, start, end });
                        // let listProvince = Object.entries(provinces);
                        // let listAgency = await AGENCY_MODEL.getList({ })
                        let description = {
                            status: "Trạng thái xem thông báo(0 chưa xem/ 1 đã xem)",
                            title: "Tên thông báo",
                            description: "Mô tả",
                            content: "Nội dung",
                            createAt: "Ngày gửi"
                        }
                        res.json({
                            description,
							...listNotification,
						});
                    }]
                },
            },

            /**
             * Function: Danh sách thông báo của USERID (ENDUSER)
             * Date: 04/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_NOTIFICATION.LIST_NOTIFICATION_CUSTOMER_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { agencyID, start, end, page } = req.query;
                        let listNotification = await NOTIFICATION_MODEL.getListNotificationOfCustomerID({ userID, page, start, end });
                        // let listProvince = Object.entries(provinces);
                        // let listAgency = await AGENCY_MODEL.getList({ })
                        let description = {
                            status: "Trạng thái xem thông báo(0 chưa xem/ 1 đã xem)",
                            title: "Tên thông báo",
                            description: "Mô tả",
                            content: "Nội dung",
                            createAt: "Ngày gửi"
                        }
                        res.json({
                            description,
							...listNotification,
						});
                    }]
                },
            },

            /**
             * Function: Update STATUS Notification CUSTOMER
             */
             [CF_ROUTINGS_NOTIFICATION.UPDATE_STATUS_NOTIFICATION_CUSTOMER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        const { notificationID } = req.params;

                        if (!checkObjectIDs(notificationID)){
                            return res.json({ error: true, message: "id_invalid" })
                        }

                        const infoNotificationUpdate = await NOTIFICATION_COLL.findByIdAndUpdate(notificationID, {status: 1}, {new: true});
                        res.json(infoNotificationUpdate);
                    }]
                },
            },

            /**
             * Function: UPDATE SEEM TẤT CẢ THÔNG BÁO của USERID
             * Date: 04/07/2021
             * Dev: SONLP
             */
             [CF_ROUTINGS_NOTIFICATION.UPDATE_SEEN_USER_NOTIFICATION_API]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;

                        let listNotification = await NOTIFICATION_MODEL.updateSeenAllNotificationOfUser({ userID });
                        // let listProvince = Object.entries(provinces);
                        // let listAgency = await AGENCY_MODEL.getList({ })
                        let description = {
                            status: "Trạng thái xem thông báo(0 chưa xem/ 1 đã xem)",
                            title: "Tên thông báo",
                            description: "Mô tả",
                            content: "Nội dung",
                            createAt: "Ngày gửi"
                        }
                        res.json({
                            description,
							...listNotification,
						});
                    }]
                },
            },
        }
    }
};
