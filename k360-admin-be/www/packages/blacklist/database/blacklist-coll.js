"use strict";

const Schema     = require('mongoose').Schema; 
const BASE_COLL  = require('../../../databases/intalize/base-coll');

module.exports  = BASE_COLL("blacklist", {
    agency: {
		type: Schema.Types.ObjectId,
		ref: 'agency',
		required: true
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'user',
		required: true
	},
});
