"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path                  = require('path');
const fs                    = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                               = require('../../../routing/child_routing');
const roles                                     = require('../../../config/cf_role');
const { TYPE_LEVEL }                            = require('../../../config/cf_constants');
const { CF_ROUTINGS_COMMON }                    = require('../constant/common.uri');
const USER_SESSION							    = require('../../../session/user-session');
const { districts }                             = require('../constant/districts');
const { provinces }                             = require('../constant/provinces');

/**
 * COLLECTIONS, MODELS
 */
const PROVINCE_COLL     = require('../../../databases/province-coll');
const DISTRICT_COLL     = require('../../../databases/district-coll');
const WARD_COLL         = require('../../../databases/ward-coll');
const { USER_MODEL }    = require('../../permissions/users');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************** ================================
             * ========================== QUẢN LÝ CHUNG  ================================
             * ========================== ************** ================================
             */

            /**
             * Redirect /home
             * Depv247
             */
            [CF_ROUTINGS_COMMON.ORIGIN_APP]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        res.redirect("/agency");
                    }]
                },
            },

            /**
             * Function: Trang chủ admin (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_COMMON.HOME]: {
                config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Admin Manager - K360',
					code: CF_ROUTINGS_COMMON.HOME,
					inc: path.resolve(__dirname, '../../../views/inc/home.ejs'),
                    view: 'index-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						ChildRouter.renderToView(req, res);
					}]
				},
            },

            /**
             * Function: Trang thông tin tài khoản (VIEW)
             * Date: 07/07/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_COMMON.ACCOUNT_SETTING]: {
                config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Admin Manager - K360',
					code: CF_ROUTINGS_COMMON.ACCOUNT_SETTING,
					inc: path.resolve(__dirname, '../../../views/inc/user_infomation.ejs'),
                    view: 'index-admin.ejs'
				},
				methods: {
					get: [ async function (req, res) {
						ChildRouter.renderToView(req, res, {
                            TYPE_LEVEL
                        });
					}]
				},
            },

            /**
             * Function: Đăng nhập account (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_COMMON.LOGIN]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-admin.ejs',
                    view: 'pages/login-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
                        console.log({ isExistLogin, __SESSION: req.session })
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/agency');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, password } = req.body;
                        const infoSignIn = await USER_MODEL.signIn({ email, password });
                        console.log({ email, password, infoSignIn })
						if (!infoSignIn.error) {
                            USER_SESSION.saveUser(req.session, {
                                user: infoSignIn.data.user, 
                                token: infoSignIn.data.token,
                            });
                        }
                        res.json(infoSignIn);
                    }],
				},
            },
            
            [CF_ROUTINGS_COMMON.FORGET_PASSWORD]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/forget-password-admin.ejs',
                    view: 'pages/forget-password-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/agency');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email } = req.body;

                        const infoSignIn = await USER_MODEL.forgetPassword({ email });
                        
                        res.json(infoSignIn);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.CHANGE_PASSWORD]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/change-password-admin.ejs',
                    view: 'pages/change-password-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/agency');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, code } = req.body;

                        const infoCheckCode = await USER_MODEL.checkCodeForgetPassword({ email, code });
                        
                        res.json(infoCheckCode);
                    }],
				},
            },

            [CF_ROUTINGS_COMMON.UPDATE_PASSWORD_RECOVER]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/update-password-admin.ejs',
                    view: 'pages/update-password-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/agency');
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, code, password  } = req.body;

                        const infoCheckCode = await USER_MODEL.updateForgetPassword({email, code, password});
                        
                        res.json(infoCheckCode);
                    }],
				},
            },

			/**
             * Clear session with render site and script
             */
			 [CF_ROUTINGS_COMMON.LOGOUT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    view: 'pages/destroy-session.ejs',
                    type: 'view',
                },
                methods: {
                    get: [ async (req, res) => {
                        USER_SESSION.destroySession(req.session);
                        ChildRouter.renderToView(req, res);
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_PROVINCES]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let listProvince = Object.entries(provinces);

                        res.json({ listProvince });
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_DISTRICTS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { province } = req.params;
                        let listDistricts = [];

                        let filterObject = (obj, filter, filterValue) => 
                            Object.keys(obj).reduce((acc, val) =>
                            (obj[val][filter] === filterValue ? {
                                ...acc,
                                [val]: obj[val]  
                            } : acc
                        ), {});

                        if (province && !Number.isNaN(Number(province))) {
                            listDistricts = filterObject(districts, 'parent_code', province.toString())
                        }
                        // console.log({ listDistricts });
                        res.json({ province, listDistricts });
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_WARDS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { district } = req.params;
                        let listWards = [];
                        let  filePath = path.resolve(__dirname, `../constant/wards/${district}.json`);
                        fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                listWards = JSON.parse(data);
                                // console.log({ listWards });
                                res.json({ district, listWards  });
                            } else {
                                res.json({ error: true, message: "district_not_exist" });
                            }
                        });
                    }]
                },
            },


            /**
             * DANH SÁCH TÍNH THÀNH QUẬN HUYỆN (NEW)
             */
            [CF_ROUTINGS_COMMON.API_LIST_PROVINCES]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async (_, res) => {
                        let listProvince = await PROVINCE_COLL.find({}).lean();
                        res.json({ error: false, status: 200, data: listProvince });
                    }]
                },
            },

            /**
             * DANH SÁCH TÍNH THÀNH QUẬN HUYỆN (NEW)
             */
            [CF_ROUTINGS_COMMON.API_LIST_DISTRICTS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        const { province } = req.params;

                        if(!province) {
                            return res.json({ 
                                error: true, 
                                status: 400, 
                                message: 'Params province invalid'
                            });
                        }

                        let listDistrict = await DISTRICT_COLL
                            .find({ parent: province })
                            .lean();

                        res.json({ error: false, status: 200, data: listDistrict });
                    }]
                },
            },

            /**
             * DANH SÁCH TÍNH THÀNH QUẬN HUYỆN (NEW)
             */
            [CF_ROUTINGS_COMMON.API_LIST_WARDS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        const { district } = req.params;

                        if(!district) {
                            return res.json({ 
                                error: true, 
                                status: 400, 
                                message: 'Params district invalid'
                            });
                        }

                        let listWard = await WARD_COLL
                            .find({ parent: district })
                            .lean();

                        res.json({ error: false, status: 200, data: listWard });
                    }]
                },
            },


        }
    }
};
