const BASE_ROUTE = '/';

const CF_ROUTINGS_COMMON = {
    HOME: `/home`,
    LOGIN: `/login`,
    FORGET_PASSWORD: `/recover-password`,
    CHANGE_PASSWORD: `/change-password`,
    UPDATE_PASSWORD_RECOVER: `/update-pass-recover`,
    LOGOUT: `/logout`,
    DESTROY_SESSION: `/destroy-session`,
    LIST_PROVINCES: '/list-provinces',
    LIST_DISTRICTS: `/list-districts/:province`,
    LIST_WARDS: `/list-wards/:district`,
    GENERATE_LINK_S3: `/generate-link-s3`,
    ACCOUNT_SETTING: `/account-setting`,

    API_LIST_PROVINCES: '/api/list-provinces',
    API_LIST_DISTRICTS: `/api/list-districts/:province`,
    API_LIST_WARDS: `/api/list-wards/:district`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_COMMON = CF_ROUTINGS_COMMON;