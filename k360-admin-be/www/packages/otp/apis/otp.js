"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');

/**
 * MODELS, COLLECTIONS
 */
 const OTP_MODEL                                = require('../models/otp').MODEL;
 const OTP_V2_MODEL                             = require('../models/otp_v2').MODEL;
 const CUSTOMER_MODEL                           = require('../../customer/models/customer').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ GROUP PERMISSION ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Thêm khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
            '/send-otp-register/:phone': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        let { phone }      = req.params;

                        let checkPhone     = await CUSTOMER_MODEL.getInfoPhone({ phone });
                        if(checkPhone.message == "phone_existed") {
                            return res.json(checkPhone);
                        }

                        // let signalAfterCreateRecord = await OTP_MODEL.sendOTP({ phone });
                        let signalAfterCreateRecord = await OTP_V2_MODEL.insertOTP__PHONE({ phone, type: OTP_V2_MODEL.TYPE_REGISTER });
                        res.json(signalAfterCreateRecord)
                    }]
                },
            },

            '/send-otp-forget/:phone': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async (req, res) => {
                        let { phone }      = req.params;

                        let checkPhone     = await CUSTOMER_MODEL.getInfoPhone({ phone });
                        console.log({
                            checkPhone
                        });
                        if(checkPhone.message == "phone_existed") {
                            // let signalAfterCreateRecord = await OTP_MODEL.sendOTP({ phone });
                            let signalAfterCreateRecord = await OTP_V2_MODEL.insertOTP__PHONE({ phone, type: OTP_V2_MODEL.TYPE_FORGET_PASS });

                            res.json(signalAfterCreateRecord)
                        } else {
                            return res.json({ error: true, message: "phone_invalid" });
                        }
                    }]
                },
            },

            // [check ok]✅
            '/verify-otp-register/:phone': {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async (req, res) => {
                        let { phone }      = req.params;
                        let { code, type }       = req.body;
                        //type: SMS_PRE_REGISTER 
                        // let signalAfterUpdate = await OTP_MODEL.verificationOTP({ phone, code});
                        let signalAfterUpdate = await OTP_V2_MODEL.verifyOTP__PHONE({ phone, code, type });
                        res.json(signalAfterUpdate)
                    }]
                },
            },
        }
    }
};
