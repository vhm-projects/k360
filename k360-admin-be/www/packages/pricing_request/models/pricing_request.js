"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const moment                = require("moment");

/**
 * EXTERNAL PAKCAGE
 */
// const { checkObjectIDs } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel        = require('../../../models/intalize/base_model');
const {checkObjectIDs} = require('../../../utils/utils');

/**
 * COLLECTIONS
 */
const PRICING_REQUEST_COLL = require('../databases/pricing-request-coll')({});


class Model extends BaseModel {
    constructor() {
        super(PRICING_REQUEST_COLL);
    }

	insert({ agency, name, email, phone, categories, note, priced, status = 0, userID }) {
        return new Promise(async resolve => {
            try {
                if(!agency || !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(!name || !phone)
                    return resolve({ error: true, message: 'name_email_and_phone_required' });

                let dataInsert = {
                    agency, name, phone, categories, description: note, priced, status
                }

                /**
                 * FIELD NẾU ĐĂNG NHẬP CUSTOMER BẰNG APP
                 */
                if (userID) {
                    dataInsert.sender = userID
                }
                const infoAfterAdd = await this.insertData({
                    ...dataInsert
                });
                if(!infoAfterAdd)
                    return resolve({ error: true, message: 'add_pricing_request_failed' });

                return resolve({ error: false, data: infoAfterAdd });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListOfUser({ userID, page, start, end }) {
        return new Promise(async resolve => {
            try {
                if (!checkObjectIDs(userID)){
                    return resolve({ error: true, message: "param_invalid" });
                }
                let dataFind = { sender: userID }
                if (start) {
                    let _fromDate = moment(start).startOf('day').format();
                    dataFind.createAt = {
                        $gte: _fromDate
                    }
                }

                if (end) {
                    let _toDate   = moment(end).endOf('day').format();
                    dataFind.createAt = {
                        ...dataFind.createAt,
                        $lte: _toDate
                    }
                }

                let listPricingProduct = await PRICING_REQUEST_COLL.find({ ...dataFind })
                    .populate({
                        path: "categories",
                        select: "_id name"
                    })
                    .populate({
                        path: "agency",
                        select: "_id name email phone address city  district ward"
                    })
                    .sort({ createAt: -1 });

                if(!listPricingProduct)
                    return resolve({ error: true, message: 'get_list_pricing_request_failed' });

                return resolve({ error: false, data: listPricingProduct });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
