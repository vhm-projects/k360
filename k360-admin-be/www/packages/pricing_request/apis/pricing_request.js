"use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_PRICING_REQUEST } 				= require('../constants/pricing_request.uri');
const { provinces }            = require('../../common/constant/provinces');
const { districts }            = require('../../common/constant/districts');
/**
 * MODELS
 */
const PRICING_REQUEST_MODEL = require('../models/pricing_request').MODEL;
const {CUSTOMER_MODEL}      = require('../../customer');
const NOTIFICATION_MODEL    = require('../../notification/models/notification').MODEL;
const AGENCY_COLL           = require('../../agency/databases/agency-coll')({});


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ PRICING REQUEST  ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Thêm yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRICING_REQUEST.ADD_PRICING_REQUEST]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let {_id: userID } = req.user;
                        // Lấy thông tin khách hàng
                        let infoCustomer = await CUSTOMER_MODEL.getInfo({ customerID: userID });

                        if(infoCustomer.error) {
                            return res.json(infoCustomer);
                        }
                        infoCustomer = infoCustomer.data;

                        
                        const { agency, categories, priced, status, note } = req.body;
                        let infoAgency = await AGENCY_COLL
                            .findById(agency)
                            .select('owners ')
                            .lean();
                        
                       
                        // console.log({ agency, name, phone, email, categories, priced, status, note });
                        const infoAfterInsert = await PRICING_REQUEST_MODEL.insert({
                            agency, name: infoCustomer.name, phone: infoCustomer.phone, categories, priced, status, note, userID
                        });
                        
                        if(infoAgency.owners && infoAgency.owners.length){
                            let title     = 'YÊU CẦU ĐỊNH GIÁ ONLINE';
                            let content   = `${infoCustomer.name} đã gửi 1 yêu cầu định giá`;
                            let link	  = `/pricing-request/list-pricing-request?request=${infoAfterInsert.data._id}`;

                            let dataInsert = {
                                title,
                                description: content,
                                url: link,
                                type: 1,
                                customer: userID,
                                arrayAgencyReceive: [agency]
                            }

                            let listNotify = infoAgency.owners.map(receiver => {
                                return NOTIFICATION_MODEL.insertV2({
                                    ...dataInsert, receiver
                                });
                            })
                            let resutlAfterInsert = await Promise.all(listNotify);
                        }

                        res.json(infoAfterInsert);
                    }]
                },
            },

            /**
             * Function: Danh sách yêu cầu định giá (API)
             */
             [CF_ROUTINGS_PRICING_REQUEST.LIST_PRICING_REQUEST_USER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let {_id: userID } = req.user;
                        
                        let { start, end } = req.query;

                        const listPricingProduct = await PRICING_REQUEST_MODEL.getListOfUser({
                            userID, start, end
                        });
                        let listProvince        = Object.entries(provinces);
                        let listAgencyChange            = [];
                        let listDistricts       = Object.entries(districts);
                        // 1// Chạy từng phần tử trong mảng Danh sách Agency
                        if(listPricingProduct.error == false && listPricingProduct.data.length) {
                            listPricingProduct.data.forEach( pricing => {
                                // 2// Lấy tên thành phố
                                if(pricing.agency.city) {
                                    for(let province of listProvince) {
                                        if(province[1].code == pricing.agency.city) {
                                            pricing.agency.city = province[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                                // 4// Lấy quận/ Huyện
                                if(pricing.agency.district) {
                                    for(let district of listDistricts) {
                                        if(district[1].code == pricing.agency.district ){
                                            pricing.agency.district = district[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                            })
                        }

                        res.json(listPricingProduct);
                    }]
                },
            },


        }
    }
};
