const BASE_ROUTE     = '/agency';
const BASE_ROUTE_API = '/api/agency';

const CF_ROUTINGS_AGENCY = {
    //========== VIEW ==========//
    VIEW_LIST_AGENCY: `${BASE_ROUTE}`,
    VIEW_DETAIL_AGENCY: `${BASE_ROUTE}/detail/:agencyID`,
    
    //========== JSON ==========//
    ADD_AGENCY: `${BASE_ROUTE}/add-agency`,
    INFO_AGENCY: `${BASE_ROUTE}/info-agency/:agencyID`,
    LIST_AGENCY: `${BASE_ROUTE}/list-agency`,
    LIST_AGENCY_BY_ADDRESS: `${BASE_ROUTE}/list-agency-by-address`,
    CHECK_EXISTED_EMAIL: `${BASE_ROUTE}/check-email-existed`,
    CHECK_EXISTED_CODE: `${BASE_ROUTE}/check-code-existed`,
    LIST_MEMBER_AGENCY: `${BASE_ROUTE}/list-member/:agencyID`,
    REMOVE_AGENCY: `${BASE_ROUTE}/remove-agency/:agencyID`,
    UPDATE_AGENCY: `${BASE_ROUTE}/update-agency/:agencyID`,
    LIST_AGENCY_NEAR: `${BASE_ROUTE}/list-agency-near`,
    ADD_OWNER_EXISTED: `${BASE_ROUTE}/add-owner-existed`,
    ADD_EMPLOYEE_EXISTED: `${BASE_ROUTE}/add-employee-existed`,

    DISTANCE_OF_AGENCY: `${BASE_ROUTE}/distance-of-agency`,

	ORIGIN_APP: BASE_ROUTE,

    VIEW_LIST_AGENCY_MOBILE: `${BASE_ROUTE_API}/list-agency`,
    VIEW_LIST_AGENCY_CUSTOMER_MOBILE: `${BASE_ROUTE_API}/list-agency-customer`,
    VIEW_LIST_AGENCY_CUSTOMER_MOBILE_V2: `${BASE_ROUTE_API}/list-agency-customer-v2`,
    VIEW_LIST_AGENCY_CUSTOMER_NEAR_MOBILE: `${BASE_ROUTE_API}/list-agency-customer-near`,
    INFO_AGENCY_CUSTOMER: `${BASE_ROUTE_API}/info-agency/:agencyID`,
    INFO_AGENCY_REVENUE: `${BASE_ROUTE_API}/info-agency-revenue/:agencyID`,

}

exports.CF_ROUTINGS_AGENCY = CF_ROUTINGS_AGENCY;
