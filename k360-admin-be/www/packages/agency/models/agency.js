"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID  = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { validEmail } = require('../../../utils/string_utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');
const { subStractDateGetDay, addNumberToSetDate, minusNumberToSetDate, getWeek, calculatedPercentDiff }                       = require('../../../config/cf_time');

/**
 * COLLECTIONS
 */
const AGENCY_COLL           = require('../databases/agency-coll')({});
const USER_COLL             = require('../../permissions/users/databases/user-coll')({});
const USER_AGENCY_COLL      = require('../../customer/database/customer_agency-coll')({});

const TRANSACTION_COLL      = require('../../transaction/database/transaction-coll')({});
const CUSTOMER_AGENCY_COLL  = require('../../customer/database/customer_agency-coll')({});

const { IMAGE_MODEL }       = require('../../image');

const INTEREST_MODEL        = require('../../interest_rate/models/interest_rate').MODEL;
const INJECT_TRANSACTION_MODEL         = require('../../transaction/models/inject_transaction').MODEL;

const moment                = require("moment");

class Model extends BaseModel {
    constructor() {
        super(AGENCY_COLL);
    }

    /**
     * Thêm đại lý
     * Dattv
     */
    insert({ name, email, phone, code, tax_code, address, city, district, ward, latitude, longitude, 
                openTime, closeTime, parent, userCreate, image_gallery_agency, image_avatar_agency }) {
        return new Promise(async resolve => {
            try {
                if(!name || !email || !ObjectID.isValid(userCreate))
                    return resolve({ error: true, message: 'params_invalid' });

                let checkExistedEmail = await AGENCY_COLL.findOne({ email });
                if(checkExistedEmail)
                    return resolve({ error: true, message: 'email_existed' });

                let dataInsert = {
                    name,
                    email,
                    phone,
                    address,
                    openTime,
                    closeTime,
                    userCreate,
                    code,
                    tax_code,
                    city, 
                    district,
                    ward,
                    location: {
                        type: "point",
                        coordinates: [latitude, longitude]
                    },
                }

                if(ObjectID.isValid(parent)){
                    dataInsert.parent = parent;
                }

                let infoAfterInsert;
                infoAfterInsert = await this.insertData(dataInsert);
                let { _id: agencyID } = infoAfterInsert

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert_agency' });

                //Thêm vào collect image và thêm vào gallery agency
                if(image_gallery_agency && image_gallery_agency.length){
                    for (let image of image_gallery_agency) {
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: image, path: image });

                        let { _id: imageID } =  infoImageAfterInsert.data;
                        infoAfterInsert = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                            $addToSet: { gallery: imageID }
                        }, { new: true });
                    }
                }

                if(image_avatar_agency && image_avatar_agency.length){
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: image_avatar_agency[0], path: image_avatar_agency[0] });
                    let { _id: imageID } =  infoImageAfterInsert.data;
                    infoAfterInsert = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                        $addToSet: { avatar: imageID }
                    }, { new: true });
                }

                // Nếu có parent thì đẩy vào childs của agency cha
                if(ObjectID.isValid(parent)){
                    infoAfterInsert = await AGENCY_COLL.findByIdAndUpdate(parent, {
                        $addToSet: { childs: infoAfterInsert._id }
                    }, { new: true })
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAgency = await AGENCY_COLL.findById(agencyID)
                    .populate({
                        path: "owners employees childs",
                        options: { sort: { createAt: -1 }}
                    })
                    .populate({ path: 'avatar', select: 'path name' })
                    .populate({ path: 'gallery', select: 'path name' });

                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoFieldNeed({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAgency = await AGENCY_COLL.findById(agencyID);
                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin bằng email
     * Dattv
     */
    getInfoByEmail({ email }) {
        return new Promise(async resolve => {
            try {

                let infoAgency = await AGENCY_COLL.findOne({ email });

                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }

    /**
     * Hàm kiểm tra code tồn tại chưa
     * Dattv
     */
    checkExistedAgencyByCode({ code }) {
        return new Promise(async resolve => {
            try {

                let infoAgency = await AGENCY_COLL.findOne({ code });

                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        });
    }
   
    /**
     * Danh sách agency
     * Dattv
     */
    getList({ name, city, district, ward }) {
        return new Promise(async resolve => {
            try {
				let condition = {};
				// name        && (condition.name      = name);
				// city        && (condition.city      = city);
				// district    && (condition.district  = district);
				// ward        && (condition.ward      = ward);
                //console.log(name);
                if(name){
                    condition.name = 
                        { $text: { $search: name } },
                        { score: { $meta: "textScore" } }
                }

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                }

                let listAgency = await AGENCY_COLL.find({ ...condition, status: 1 })
                    .populate({ path: 'owners', select: 'username' })
                    .populate({ path: 'parent', select: '_id code' })

                if(!listAgency) 
                    return resolve({ error: true, message: "cannot_get_list_agency" });

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách agency lân cận
     * Dattv
     */
     getListAgencyNearMobile({ city, district, ward, lng, lat, maxDistance, minDistance, name, start, end, }) {
        return new Promise(async resolve => {
            try {
                let listAgencyNear;

                let conditionObj = { };

                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    conditionObj.name = new RegExp(keyword, 'i');
                }

                if(city && city != "null"){
                    conditionObj.city = city;
                }

                if(district && district != "null"){
                    conditionObj.district = district;
                }

                if(ward && ward != "null"){
                    conditionObj.ward = ward;
                }

                if (!lng || !lat){

                    listAgencyNear = await AGENCY_COLL.find({...conditionObj, status: 1}).populate("avatar");

                }else{

                    if(!maxDistance){
                        maxDistance = 700000
                    }
    
                    if(!minDistance){
                        minDistance = 0
                    }
    
                    listAgencyNear = await AGENCY_COLL.aggregate([
                        
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                                maxDistance: Number(maxDistance),
                                minDistance: Number(minDistance),
                                distanceMultiplier: 0.001, //Returns km
                                distanceField: "dist.calculated",
                                includeLocs: "dist.location", // Returns distance
                                spherical: true
                            }
                        },
                        { 
                            $match: { ...conditionObj, status: 1 }
                        },{ 
                            $lookup: {
                                from: "images",
                                localField: "avatar",
                                foreignField: "_id",
                                as: "avatar"
                            }
                        }
                    ]);
                }

                if(!listAgencyNear) 
                    return resolve({ error: true, message: "get_list_agency" });
                return resolve({ error: false, data: listAgencyNear });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Tìm kiếm agency
     * Dattv
     */
    filterAgency({ name, city, district, ward, start, end, sort, status }) {
        return new Promise(async resolve => {
            try {
				let condition = {};
				
                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    condition.name = new RegExp(keyword, 'i');
                }

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                }

                if(start && end) {
                    let _fromDate = moment(start).startOf('day').format();
                    let _toDate   = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                // if(status) {
                //     condition.status = status;
                // }

                let listAgency = await AGENCY_COLL.find({ ...condition })
                    .populate({ path: "owners", select: "username" })
                    .populate({ path: 'parent', select: '_id code name email phone code' })
                    .populate({ path: 'avatar', select: '_id path' })
                    .populate({ path: 'gallery', select: '_id path' })
                    .sort({ createAt: -1 });

                if(!listAgency)
                    return resolve({ error: true, message: "cannot_get_list_agency" });

                /**
                 * Nếu lọc list agency theo điều kiện có agency thỏa điều kiện mới tiếp tục sắp xếp
                 * tránh trường hợp sau khi lọc không agency nào thỏa thì khi group theo giao dịch sẽ trả về
                 * những agency không đúng theo điều kiện lọc
                 */
                if(listAgency.length > 0) {
                    let objCondition = {};

                    let listAgencyID;
                    if(listAgency && listAgency.length) {
                        listAgencyID = listAgency.map(agency => agency._id);
    
                        objCondition.agency = {
                            $in: listAgencyID
                        }
                    }
    
                    /**
                     * Trường hợp của sort:
                     *  + "": Mặc định
                     *  + 1: Doanh số cao nhất
                     *  + 2: Doanh số thấp nhất
                     *  + 3: Khách hàng nhiều nhất
                     *  + 4: Khách hàng ít nhất
                     */
                    let objSort = {};
                    switch(Number(sort)) {
                        case 1: {
                            objSort.total = -1;
                            break;
                        }
                        case 2: {
                            objSort.total = 1;
                            break;
                        }
                        case 3: {
                            objSort.numOfCustomer = -1;
                            break;
                        }
                        case 4: {
                            objSort.numOfCustomer = 1;
                            break;
                        }
                        default: {
                            objSort = { total: -1 };
                        }
                    }
    
                    let listAgencyWithSort;
                    if(sort){
                        listAgencyWithSort = await TRANSACTION_COLL.aggregate([
                            {
                                $match: objCondition
                            },
                            {
                                $group : { 
                                    _id : "$agency",
                                    count: { $sum: 1 },
                                    total: { $sum: "$loanAmount" },
                                    customer: { $addToSet: "$customer" },
                                    agency : { $first: '$agency' }
                                }
                            },
                            {
                                $lookup: {
                                    from: "agencies",
                                    localField: "agency",
                                    foreignField: "_id",
                                    as: "agency"
                                }
                            },
                            {
                                $unwind: "$agency"
                            },
                            {
                                $addFields: {
                                    numOfCustomer: {
                                        $size: "$customer"
                                    }
                                }
                            },
                            {
                                $sort: objSort
                            }
                        ]);
    
                        /**
                         * listAgencyWithSort: 
                         * 
                         * [{
                            _id: 60b0660da706192905f6d382,
                            count: 26, // tổng số giao dịch
                            total: 307077777, //tổng doanh thu
                            customer: [Array],
                            agency: [Object],
                            numOfCustomer: 4 // tống số lượng khách hàng
                            },]
                         */
                        let arrIDAgencyWithSort = listAgencyWithSort.map(item => ObjectID(item.agency._id));

                        /**
                         * b1: lấy danh sách AGENCY theo listAgencyWithSort -> lấy danh sách agency có chứa thông tin tài chính (1)
                         * b2: tìm những agency ko chứa trong listAgencyWithSort -> merge vào array agency ở bước (1)
                         */
                        // let listAgencyWithSortTotalAmountTransaction = await AGENCY_COLL.find({
                        //     _id: {
                        //         $in: [...arrIDAgencyWithSort]
                        //     }
                        // });

                        let listAgencyWithoutAmountTransction = await AGENCY_COLL.find({
                            _id: {
                                $nin: [...arrIDAgencyWithSort]
                            }
                        }).populate({ path: 'avatar', select: '_id path' }).sort({ createAt: -1 });

                        switch(Number(sort)) {
                            case 1 || 3: {
                                listAgency = [
                                    ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                ]
                                break;
                            }
                            case 2 || 4: {
                                listAgency = [
                                    ...listAgencyWithoutAmountTransction, ...listAgencyWithSort.map(item => item.agency),
                                ]
                                break;
                            }
                            default: {
                                listAgency = [
                                    ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                ]
                            }
                        }
                        // let arrIDAgencyWithSort = listAgencyWithSort.map(agency => agency._id.toString());
    
                        // /**
                        //  * lặp qua listAgency ban đầu để gửi vào thêm array những phần từ không có giao dịch
                        //  * Thêm điều kiện && objSort.total để biết chiều khi bỏ phần từ vào mảng cho hợp lí
                        //  */
                        // listAgency.forEach(agency => {
                        //     if(!arrIDAgencyWithSort.includes(agency._id.toString()) && (objSort.total === 1 || objSort.numOfCustomer === 1)) {
                        //         listAgencyWithSort.unshift(agency);
                        //     } else if(!arrIDAgencyWithSort.includes(agency._id.toString()) && (objSort.total === -1 || objSort.numOfCustomer === -1)) {
                        //         listAgencyWithSort.push(agency);
                        //     }
                        // })
                    }
                    
                    let STATUS_ACCEPT_TRANSACTION = [1, 3];
                    let listAgencyBestSale = await TRANSACTION_COLL.aggregate([
                        {
                            $match: { status: { $in: STATUS_ACCEPT_TRANSACTION } }
                        },
                        {
                            $group : { 
                                _id : "$agency",
                                count: { $sum: 1 },
                                total: { $sum: "$loanAmount" },
                                agency : { $first: '$agency' }
                            },
                        },
                        {
                            $lookup: {
                                from: "agencies",
                                localField: "agency",
                                foreignField: "_id",
                                as: "agency"
                            }
                        },
                        {
                            $unwind: "$agency"
                        },
                    ]);
    
                    let listTopAgencyHaveMostCustomer = await TRANSACTION_COLL.aggregate([
                        {
                            $group : { 
                                _id : "$agency",
                                customer: { $addToSet: "$customer" },
                                agency : { $first: '$agency' },
                            }
                        },
                        {
                            $lookup: {
                                from: "agencies",
                                localField: "agency",
                                foreignField: "_id",
                                as: "agency"
                            }
                        },
                        {
                            $unwind: "$agency"
                        },
                        {
                            $addFields: {
                                numOfCustomer: {
                                    $size: "$customer"
                                }
                            }
                        },
                    ]);
                    
                    // if(sort) {
                    //     listAgency = listAgencyWithSort;
                    // }
                    
                    return resolve({ error: false, data: { listAgency, listAgencyBestSale, listTopAgencyHaveMostCustomer } });
                } else {
                    return resolve({ error: false, data: { listAgency, listAgencyBestSale: [], listTopAgencyHaveMostCustomer: [] } });
                }
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    filterAgencyV2({ name, city, district, ward, start, end, sort, status }) {
        return new Promise(async resolve => {
            try {
				let condition = {};
				
                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    condition.name = new RegExp(keyword, 'i');
                }

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                }

                if(start) {
                    let _fromDate = moment(start).startOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $gte: new Date(_fromDate),
                        // $lt: new Date(_toDate)
                    }
                }

                if(end) {
                    let _toDate   = moment(end).endOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $lte: new Date(_toDate),
                    }
                }

                if(status) {
                    condition.status = status;
                }

                let listAgency = await AGENCY_COLL.find({ ...condition })
                    // .populate({ path: "owners", select: "username" })
                    // .populate({ path: 'parent', select: '_id code name email phone code' })
                    .populate({ path: 'avatar', select: '_id path' })
                    .populate({ path: 'gallery', select: '_id path' })
                    .sort({ createAt: -1 });

                if(!listAgency)
                    return resolve({ error: true, message: "cannot_get_list_agency" });
                /**
                 * Nếu lọc list agency theo điều kiện có agency thỏa điều kiện mới tiếp tục sắp xếp
                 * tránh trường hợp sau khi lọc không agency nào thỏa thì khi group theo giao dịch sẽ trả về
                 * những agency không đúng theo điều kiện lọc
                 */
                const STATUS_ACTIVE = 1;
                if(listAgency.length > 0 && sort) {
                    let objCondition = {}; 
                    // { status: STATUS_ACTIVE};

                    let listAgencyID;
                    if(listAgency && listAgency.length) {
                        listAgencyID = listAgency.map(agency => agency._id);
    
                        objCondition.agency = {
                            $in: listAgencyID,
                        }
                    }
                    /**
                     * Trường hợp của sort: ádasd
                     *  + "": Mặc định
                     *  + 1: Doanh số cao nhất
                     *  + 2: Doanh số thấp nhất
                     *  + 3: Khách hàng nhiều nhất
                     *  + 4: Khách hàng ít nhất
                     */
                    let objSort = {};
                    switch(Number(sort)) {
                        case 1: {
                            objSort.total = -1;
                            break;
                        }
                        case 2: {
                            objSort.total = 1;
                            break;
                        }
                        case 3: {
                            objSort.count = -1;
                            break;
                        }
                        case 4: {
                            objSort.count = 1;
                            break;
                        }
                        default: {
                            objSort = { total: -1 };
                        }
                    }
    
                    const TOTAL_AMOUNT_TRANSACTION_DESC = 1;
                    const TOTAL_AMOUNT_TRANSACTION_ASC  = 2;
                    const TOTAL_NUMBER_CUSTOMER_DESC    = 3;
                    const TOTAL_NUMBER_CUSTOMER_ASC     = 4;
                    const SORT_VALUE_VALID = [TOTAL_AMOUNT_TRANSACTION_DESC, TOTAL_AMOUNT_TRANSACTION_ASC, TOTAL_NUMBER_CUSTOMER_DESC, TOTAL_NUMBER_CUSTOMER_ASC];
                    if (sort && !Number.isNaN(Number(sort)) && SORT_VALUE_VALID.includes(Number(sort))) {
                        const SORT_TRANSACTION = [TOTAL_AMOUNT_TRANSACTION_DESC, TOTAL_AMOUNT_TRANSACTION_ASC];
                        const SORT_CUSTOMER    = [TOTAL_NUMBER_CUSTOMER_DESC, TOTAL_NUMBER_CUSTOMER_ASC];
                        if (SORT_TRANSACTION.includes(Number(sort))) {
                            let listAgencyWithSort = await TRANSACTION_COLL.aggregate([
                                {
                                    $match: {
                                        ...objCondition, 
                                        status: STATUS_ACTIVE
                                    }
                                },
                                {
                                    $group : { 
                                        _id : "$agency",
                                        count: { $sum: 1 },
                                        total: { $sum: "$loanAmount" },
                                        customer: { $addToSet: "$customer" },
                                        agency : { $first: '$agency' }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "agencies",
                                        localField: "agency",
                                        foreignField: "_id",
                                        as: "agency"
                                    }
                                },
                                {
                                    $unwind: "$agency"
                                },
                                {
                                    $lookup: {
                                        from: "images",
                                        localField: "agency.avatar",
                                        foreignField: "_id",
                                        as: "agency.avatar"
                                    }
                                },
                                {
                                    $unwind: "$agency.avatar"
                                },
                                {
                                    $addFields: {
                                        numOfCustomer: {
                                            $size: "$customer"
                                        }
                                    }
                                },
                                {
                                    $sort: {
                                        ...objSort,
                                        createAt: -1
                                    }
                                }
                            ]);  
                            console.log({ listAgencyWithSort });
                            /**
                             * listAgencyWithSort: 
                             * 
                             * [{
                                _id: 60b0660da706192905f6d382,
                                count: 26, // tổng số giao dịch
                                total: 307077777, //tổng doanh thu
                                customer: [Array],
                                agency: [Object],
                                numOfCustomer: 4 // tống số lượng khách hàng
                                },]
                            */
                            let arrIDAgencyWithSort = listAgencyWithSort.map(item => ObjectID(item.agency._id));
        
                            /**
                             * b1: lấy danh sách AGENCY theo listAgencyWithSort -> lấy danh sách agency có chứa thông tin tài chính (1)
                             * b2: tìm những agency ko chứa trong listAgencyWithSort -> merge vào array agency ở bước (1)
                             */
                            // let listAgencyWithSortTotalAmountTransaction = await AGENCY_COLL.find({
                            //     _id: {
                            //         $in: [...arrIDAgencyWithSort]
                            //     }
                            // });

                            let listAgencyWithoutAmountTransction = await AGENCY_COLL.find({
                                _id: {
                                    $nin: [...arrIDAgencyWithSort]
                                }
                            }).populate({ path: 'avatar', select: '_id path' }).sort({ createAt: -1 });

                            switch(Number(sort)) {
                                case 1: {
                                    listAgency = [
                                        ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                    ]
                                    break;
                                }
                                case 2: {
                                    listAgency = [
                                        ...listAgencyWithoutAmountTransction, ...listAgencyWithSort.map(item => item.agency),
                                    ]
                                    break;
                                }
                                default: {
                                    listAgency = [
                                        ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                    ]
                                }
                            }
                        }
                        if (SORT_CUSTOMER.includes(Number(sort))) {
                            let listAgencyWithSort = await CUSTOMER_AGENCY_COLL.aggregate([
                                // {
                                //     $match: objCondition
                                // },
                                {
                                    $group : { 
                                        _id: {
                                            agency: "$agency",
                                            customer: "$customer"
                                        }
                                    },
                                },
                                {
                                    $addFields: {
                                      agency: "$_id.agency",
                                      customer: "$_id.customer",
                                   }
                                },
                                {
                                    $group : { 
                                        _id: {
                                            agency: "$agency",
                                            
                                        },
                                        count: { $sum: 1 }
                                    },
                                },
                                {
                                    $lookup: {
                                        from: "agencies",
                                        localField: "_id.agency",
                                        foreignField: "_id",
                                        as: "agency"
                                    }
                                },
                                {
                                    $unwind: "$agency"
                                },
                                {
                                    $lookup: {
                                        from: "images",
                                        localField: "agency.avatar",
                                        foreignField: "_id",
                                        as: "agency.avatar"
                                    }
                                },
                                {
                                    $unwind: "$agency.avatar"
                                },
                                {
                                    $sort: {
                                        ...objSort,
                                    }
                                }
                            ]);
                            let arrIDAgencyWithSort = listAgencyWithSort.map(item => ObjectID(item.agency._id));
                            let listAgencyWithoutAmountTransction = await AGENCY_COLL.find({
                                _id: {
                                    $nin: [...arrIDAgencyWithSort]
                                }
                            }).populate({ path: 'avatar', select: '_id path' }).sort({ createAt: -1 });
                            switch(Number(sort)) {
                                case 3: {
                                    listAgency = [
                                        ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                    ]
                                    break;
                                }
                                case 4: {
                                    listAgency = [
                                        ...listAgencyWithoutAmountTransction, ...listAgencyWithSort.map(item => item.agency),
                                    ]
                                    break;
                                }
                                default: {
                                    listAgency = [
                                        ...listAgencyWithSort.map(item => item.agency), ...listAgencyWithoutAmountTransction
                                    ]
                                }
                            }
                        }
                        // /**
                        //  * lặp qua listAgency ban đầu để gửi vào thêm array những phần từ không có giao dịch
                        //  * Thêm điều kiện && objSort.total để biết chiều khi bỏ phần từ vào mảng cho hợp lí
                        //  */
                        // listAgency.forEach(agency => {
                        //     if(!arrIDAgencyWithSort.includes(agency._id.toString()) && (objSort.total === 1 || objSort.numOfCustomer === 1)) {
                        //         listAgencyWithSort.unshift(agency);
                        //     } else if(!arrIDAgencyWithSort.includes(agency._id.toString()) && (objSort.total === -1 || objSort.numOfCustomer === -1)) {
                        //         listAgencyWithSort.push(agency);
                        //     }
                        // })
                        // // listNotiAfterInsert = listNotiAfterInsert.filter(infoNoti => (
                        // //     infoNoti && infoNoti.data
                        // // ));
                        // listAgencyWithSort = listAgencyWithSort.filter( agency => agency._id)
                        //   = listAgencyWithSort;
                    }
                }

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    listAgencyShowAdmin({ name, city, district, ward, fromDay, toDay }){
        return new Promise(async resolve => {
            try {
            
                let DAY_PARSE = 1000 * 60 * 60 * 24;

                let condition = {};
				
                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    condition.name = new RegExp(keyword, 'i');
                }

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                }

                let listAgency = await AGENCY_COLL.find({
                        ...condition, status: 1
                    })
                    .select("_id name code parent")
                    .populate({ path: 'parent', select: 'code'});

                let dataResult = [];
                let createAt = {}
                if (fromDay) {
                    let start = new Date(fromDay);
                    start   = moment(start).startOf('day').format();
                    createAt = { ...createAt, $gte: new Date(start) }
                }
                if (toDay) {
                    let end = new Date(toDay);
                    end = moment(end).endOf('day').format();
                    createAt = { ...createAt, $lte: new Date(end) }
                }
                for (const agency of listAgency) {
                    let { _id, name, code, parent } = agency;
                    const STATUS_ACTIVE_TRANSACTION        = 1;
                    const STATUS_INACTIVE_TRANSACTION      = 0;
                    const STATUS_NOT_APPROVE_TRANSACTION   = 2;

                    let agencyBase = {
                        _id,
                        name,
                        code,
                        parent: parent && parent.code
                    };
                   
                    //Lấy số lượng giao dịch cầm đồ của Đại lý đó
                    let objAmountTransactionPawn = {
                        agency: _id, status:  STATUS_ACTIVE_TRANSACTION
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objAmountTransactionPawn.createAt = createAt;
                    }
                    /**
                     * SỐ LƯỢNG GIAO DỊCH ĐNAG CẦM
                     */
                    let amountTransactionPawn = await TRANSACTION_COLL.count({ ...objAmountTransactionPawn });
                    agencyBase.amountPawn = amountTransactionPawn;  
                    //Lấy tổng tiền giao dịch cầm đồ của Đại Lý
                    let objMatchTotalMoney = {
                        $match: {agency: ObjectID(_id), status: STATUS_ACTIVE_TRANSACTION }
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objMatchTotalMoney = { 
                            $match: {agency: ObjectID(_id), status: STATUS_ACTIVE_TRANSACTION, createAt }
                        }
                    }
                    /**
                     * TỔNG TIỀN VAY GIAO DỊCH ĐANG CẦM
                     */
                    let totalMoneyOfAgency = await TRANSACTION_COLL.aggregate([
                        { 
                            ...objMatchTotalMoney
                        },
                        { 
                            $group: {
                                _id: null,
                                total: { $sum: "$loanAmount" }
                            }
                        }
                    ])
                    let originalAmt = totalMoneyOfAgency.length && totalMoneyOfAgency[0].total || 0
                    agencyBase.originalAmt = originalAmt;

                    //Lấy số lượng giao dịch chuộc đồ của Đại lý đó
                    const STATUS_COMPLETE_TRANSACTION = 3;
                    let objRedeem = {
                        agency: _id, status: STATUS_COMPLETE_TRANSACTION
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objRedeem.createAt = createAt;
                    }
                    /**
                     * SỐ LƯỢNG GIAO DỊCH ĐÃ CHUỘC
                     */
                    let redeem = await TRANSACTION_COLL.count({ ...objRedeem });
                    agencyBase.redeem = redeem;

                    /**
                     * SỐ LƯỢNG KHÁCH HÀNG
                     */
                    let objCustomer = {
                        agency: ObjectID(_id)
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objCustomer = { 
                            ...objCustomer,
                            createAt
                        }
                    }

                    let listCustomer = await USER_AGENCY_COLL.find({ 
                        ...objCustomer 
                    });
                    let listCustomerID = listCustomer.map( customer => customer.customer.toString() );
                    let totalCustomer = [...new Set(listCustomerID)].length;

                    // let totalCustomer = await TRANSACTION_COLL.aggregate([
                    //     {
                    //         ...objCustomer
                    //     },
                    //     {
                            
                    //         $group: { _id: "$customer", count: { $sum: 1 }},
                            
                    //     }
                    // ])
                    agencyBase.totalCustomer = totalCustomer;
                        
                    //     $group: { _id: "$customer", count: { $sum: 1 }},
                        
                    // }

                    //Lấy tổng tiền giao dịch chuộc đồ của Đại lý đó
                    let objMatchTotalRedeem = {
                        $match: { agency: ObjectID(_id), status: STATUS_COMPLETE_TRANSACTION }
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objMatchTotalRedeem = { 
                            $match :{ agency: ObjectID(_id), status: STATUS_COMPLETE_TRANSACTION, createAt }
                        }
                    }
                    /**
                     * TỔNG TIỀN ĐÃ CHUỘC
                     */
                    let totalMoneyRedeemOfAgency = await TRANSACTION_COLL.aggregate([
                        { 
                            ...objMatchTotalRedeem
                        },
                        {
                            $lookup : {
                                from: "inject_transactions",
                                localField: "latestInjectTransaction",
                                foreignField: "_id",
                                as: "latestInjectTransaction"
                            }
                        },
                        {
                            $unwind: "$latestInjectTransaction"
                        },
                        { 
                            $group: {
                                _id: null,
                                total: { $sum: "$latestInjectTransaction.meta.priceRansomPay" }
                            }
                        }
                    ])

                    let redeemAmt = totalMoneyRedeemOfAgency.length && totalMoneyRedeemOfAgency[0].total || 0
                    agencyBase.redeemAmt = redeemAmt;

                    //=============================== LÃI ===============================//
                    let objInterestOfAgency = {
                        $match: { agency: ObjectID(_id), status: 1 }
                    }
                    if (createAt.$lte || createAt.$gte) {
                        objInterestOfAgency = { 
                            $match: { agency: ObjectID(_id), status: 1, createAt }
                        }
                    }
                    let totalInterestOfAgency = await TRANSACTION_COLL.aggregate([
                        {
                            ...objInterestOfAgency
                        },
                        // {
                        //     $project: { 
                        //         'customer': 1, 
                        //         'loanAmount': 1, 
                        //         'code': 1, 
                        //         'status': 1, 
                        //         'expireTime': 1, 
                        //         'latestInjectTransaction': 1,
                        //         'interestStartDate': 1, 
                        //         'products': 1,
                        //         'dateDifference': {
                        //                 $divide: [{ $subtract: [new Date(), '$interestStartDate'] }, DAY_PARSE]
                        //         }, 
                        //     }
                        // }, 
                        // {
                        //     $lookup: {
                        //         from: "products",
                        //         localField: "products",
                        //         foreignField: "_id",
                        //         as: "products"
                        //     }
                        // },
                    ]);

                    let arr = []
                    let totalInterestAmt = 0;
                    let totalInterest    = 0;
                    //const TYPE_TRANSACTION_NORMMAL    = 2;
                    for (let interest of totalInterestOfAgency) {
                        let { data: totalPriceNormal } = await INJECT_TRANSACTION_MODEL.calculateTotalInterest({ transactionID: interest._id });

                        totalInterestAmt += totalPriceNormal;
                        // 1// Lay thong tin lai, phan tram, so tien
                        // if(interest.dateDifference >= 0) {
                        //     interest.dateDifference     = Math.floor(interest.dateDifference) + 1;
                        // }
                        
                        // if (interest.dateDifference < 0) {
                        //     interest.dateDifference = 0;
                        // }
                        // let infoInterestAfterCall = await INTEREST_MODEL.checkDateToInterest({ agencyID: _id, limitDate: interest.dateDifference, price: interest.loanAmount, typeProduct: interest.products[0].type });
                        // let obj = interest
                        // let { period, datePeriod  } = infoInterestAfterCall;
                        // obj.percent = infoInterestAfterCall.percent;
                        // let totalPrice   = 0;
                        // infoInterestAfterCall.data.forEach( price => {
                        //     totalPrice   += price
                        // });

                        // obj.price = totalPrice;
                        // arr.push(obj)
                        // totalInterestAmt+=totalPrice;
                        // totalInterest += period + datePeriod/30;
                    }
                    agencyBase.totalInterest = totalInterest;
                    agencyBase.totalInterestAmt = totalInterestAmt;
                    // console.log({ agencyBase });
                    dataResult.push(agencyBase);
                }

                // console.log({ dataResult });

                return resolve({ error: false, data: dataResult });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách agency lân cận
     * Dattv
     */
    getListAgencyNear({ lng, lat, maxDistance, minDistance }) {
        return new Promise(async resolve => {
            try {

                if (!lng || !lat)
                    return resolve({ error: true, message: 'params_invalid' });

                let listAgencyNear = await AGENCY_COLL.aggregate([
                    {
                        $geoNear: {
                            near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                            maxDistance: Number(maxDistance),
                            minDistance: Number(minDistance),
                            distanceMultiplier: 0.001, //Returns km
                            distanceField: "dist.calculated",
                            includeLocs: "dist.location", // Returns distance
                            spherical: true
                        }
                    },
                    {
                        $match: { status: 1 }
                    }
                ]);

                if(!listAgencyNear) 
                    return resolve({ error: true, message: "get_list_agency" });

                return resolve({ error: false, data: listAgencyNear });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách member của agency
     * Dattv
     */
    getListMemberOfAgency({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_not_valid" });

                let infoAgency = await AGENCY_COLL.findById(agencyID).populate("owners employees");
                let { owners: listOwnerOfAgency, employees: listEmployeesOfAgency } = infoAgency;

                if(!listOwnerOfAgency && !listEmployeesOfAgency) 
                    return resolve({ error: true, message: "get_list_member_fail" });

                return resolve({ 
                    error: false, 
                    data: { 
                        listOwnerOfAgency, 
                        listEmployeesOfAgency 
                    }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Xóa agency
     * Dattv
     */
     remove({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterDelete = await AGENCY_COLL.findByIdAndDelete(agencyID);

                //Xóa khỏi child của menu parent nếu có
                let { parent } = infoAfterDelete;

                if(parent){
                    await AGENCY_COLL.findByIdAndUpdate(parent, {
                        $pull: { childs: infoAfterDelete._id }
                    }, {new: true })
                }

                //Xóa tất cả menu con
                await AGENCY_COLL.deleteMany({ parent: agencyID });

                if(!infoAfterDelete)
                    return resolve({ error: true, message: 'cannot_remove_agency' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Cập nhật thông tin agency
     * Dattv
     */
     update({ agencyID, name, email, phone, address, code, tax_code, city, district, ward, latitude, longitude, openTime, closeTime, status, avatar, gallery, galleryDelete, userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let isExistAgency = await AGENCY_COLL.findById(agencyID);
                if(!isExistAgency)
                    return resolve({ error: true, message: 'agency_does_not_exist' });

                let isExistCode = await AGENCY_COLL.findOne({ code, _id: { $nin: [agencyID] }  });
                if(isExistCode)
                    return resolve({ error: true, message: 'Mã đại lý đã tồn tại' });

                if(latitude && !(latitude <= 180 && latitude >= -180)) {
                    return resolve({ error: true, message: 'Kinh độ phải lớn hơn hoặc bằng -180 và nhỏ hơn hoặc bằng 180' });
                }

                if(longitude && !(longitude <= 90 && longitude >= -90)) { 
                    return resolve({ error: true, message: 'Vĩ độ phải lớn hơn hoặc bằng -90 và nhỏ hơn hoặc bằng 90' });
                }

                let dataUpdate = {};

                name && (dataUpdate.name = name);
                email && (dataUpdate.email = email.toLowerCase().trim());
                phone && (dataUpdate.phone = phone);
                address && (dataUpdate.address = address);
                openTime && (dataUpdate.openTime = openTime);
                closeTime && (dataUpdate.closeTime = closeTime);
                code && (dataUpdate.code = code);
                tax_code && (dataUpdate.tax_code = tax_code);
                city && (dataUpdate.city = city);
                district && (dataUpdate.district = district);
                ward && (dataUpdate.ward = ward);

                if (status || status == 0) {
                    dataUpdate.status = status
                }
                ObjectID.isValid(userUpdate) && (dataUpdate.userUpdate = userUpdate);
                if(latitude && longitude) {
                    dataUpdate.location =  {
                        type: "point",
                        coordinates: [latitude, longitude]
                    }
                }

                let infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, 
                    dataUpdate, { new: true });
                
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update_agency' });

                if(avatar && avatar.length){
                    let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: avatar[0], path: avatar[0] });
                    let { _id: imageID } =  infoImageAfterInsert.data;
                    infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                        avatar: imageID
                    }, { new: true });
                }

                //Thêm vào collect image và thêm vào gallery agency
                if(gallery && gallery.length){
                    for (let image of gallery) {
                        let infoImageAfterInsert = await IMAGE_MODEL.insert({ name: image, path: image });
                        let { _id: imageID } =  infoImageAfterInsert.data;
                        infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                            $addToSet: { gallery: imageID }
                        }, { new: true });
                    }
                }

                //Xóa khỏi collection agency
                if(galleryDelete && galleryDelete.length){
                    for (let image of galleryDelete) {
                        infoAfterUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                            $pull: { gallery: image.id }
                        }, { new: true });
                    }
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Thêm quản lý có sẵn
     * Dattv
     */
     addOnwerExisted({ email, agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!email && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoUserWithEmail = await USER_COLL.findOne({ email });

                if(!infoUserWithEmail){
                    return resolve({ error: true, message: 'email_not_existed' });
                }

                let { _id: userID, agency, level } = infoUserWithEmail;

                if(level != 1){
                    return resolve({ error: true, message: 'account_is_not_owner' });
                }

                if(agency && agency.length){
                    for (const item of agency) {
                        if(item.toString() == agencyID.toString()){
                            return resolve({ error: true, message: 'email_is_have_agency' });
                        }
                    }
                }

                let infoAgencyUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                    $addToSet: { owners: userID }
                }, { new: true });

                await USER_COLL.findByIdAndUpdate(userID, {
                    $addToSet: { agency: agencyID }
                }, { new: true })

                if(!infoAgencyUpdate){
                    return resolve({ error: true, mesage: "cannot_update" });
                }

                return resolve({ error: false, data: infoAgencyUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    addEmployeeExisted({ email, agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!validEmail(email) && !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoUserWithEmail = await USER_COLL.findOne({ email });
                if(!infoUserWithEmail){
                    return resolve({ error: true, message: 'email_not_existed' });
                }

                let { _id: userID, agency, level } = infoUserWithEmail;

                if(level != 2){
                    return resolve({ error: true, message: 'account_is_not_employee' });
                }

                if(agency && agency.length){
                    for (const ID of agency) {
                        if(ID.toString() == agencyID.toString()){
                            return resolve({ error: true, message: 'email_is_have_agency' });
                        }
                    }
                }

                let infoAgencyUpdate = await AGENCY_COLL.findByIdAndUpdate(agencyID, {
                    $addToSet: { employees: userID }
                }, { new: true });

                if(!infoAgencyUpdate){
                    return resolve({ error: true, mesage: "cannot_update" });
                }

                await USER_COLL.findByIdAndUpdate(userID, {
                    $addToSet: { agency: agencyID }
                });

                return resolve({ error: false, data: infoAgencyUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    revenueAgency({ agencyID, date, type }) {
        return new Promise(async resolve => {
            try {
                // 1// KIỂM TRA TYPE
                /**
                 * 1: Set Doanh Số Hôm Nay    -> Hôm Qua 
                 * 2: Set Doanh Số Hôm Qua    -> Hôm Kia
                 * 3: Set Doanh Số Tuần Này   -> Tuần Trước
                 * 4: Set Doanh Số Tháng Này  -> Tháng Trước
                 */
                let TYPE_DATE_NOW    = 1;
                let TYPE_DATE_BEFORE = 2;
                let TYPE_WEEK_NOW    = 3;
                let TYPE_MONTH_NOW   = 4;

                const STATUS_ACTIVE  = 1;

                // 2// VỚI TYPE LÀ NGÀY HÔM NAY
                if(type == TYPE_DATE_NOW) {
                    /**
                     * 1: Lấy ngày hiện tại 
                     * 2: Trừ đi 1 ngày để ra ngày hôm qua (yesterday)
                     */
                    let day             = date.getDate();
                    let yesterday       = 1;
                    let dateYesterday   = new Date(date)
                    
                    // 1// LẤY NGÀY HÔM NAY TRỪ ĐI 1 NGÀY
                    minusNumberToSetDate({date1: dateYesterday, numberToSet: yesterday});
                    
                    // 2// LẤY NGÀY HIỆN TẠI SET 0H
                    let dateStart = new Date(date);
                    dateStart.setHours(0,0,0,0);

                    // 3// LẤY NGÀY HIỆN TẠI SET 24H
                    let dateEnd = new Date(date);
                    dateEnd.setHours(23,59,59,999);


                    // 4// LẤY NGÀY HIỆN TẠI SET 0H
                    let dateYesterdayStart = new Date(dateYesterday);
                    dateYesterdayStart.setHours(0,0,0,0);

                    // 5// LẤY NGÀY HIỆN TẠI SET 24H
                    let dateYesterdayEnd = new Date(dateYesterday);
                    dateYesterdayEnd.setHours(23,59,59,999);

                    // 6// LẤY CÁC GIAO DỊCH TRONG NGÀY HÔM NAY
                    let listTransactionNow = await TRANSACTION_COLL.aggregate([
                        {
                            $facet: {
                                // "countCustomer": [
                                //     {
                                //         $match: {
                                //             agency: ObjectID(agencyID),
                                //             createAt: {
                                //                 $gte: dateStart,
                                //                 $lte: dateEnd
                                //             },
                                //             status: STATUS_ACTIVE
                                //         }
                                //     },
                                //     {
                                        
                                //         $group: { _id: "$customer", count: { $sum: 1 }},
                                        
                                //     }
                                // ],
                                "transaction": [
                                    {
                                        $match: {
                                            agency: ObjectID(agencyID),
                                            createAt: {
                                                $gte: dateStart,
                                                $lte: dateEnd
                                            },
                                            status: STATUS_ACTIVE
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            sum: {
                                                $sum: "$loanAmount"
                                            },
                                            count: {
                                                $sum: 1
                                            }
                                        }
                                    }
                                ],
                                // "countCustomerYesterday": [
                                //     {
                                //         $match: {
                                            // agency: ObjectID(agencyID),
                                            // createAt: {
                                            //     $gte: dateYesterdayStart,
                                            //     $lte: dateYesterdayEnd
                                            // },
                                //             status: STATUS_ACTIVE
                                //         }
                                //     },
                                //     {
                                //         $group: { _id: "$customer", count: { $sum: 1 }},
                                //     }
                                // ],
                                "transactionYesterday": [
                                    {
                                        $match: {
                                            agency: ObjectID(agencyID),
                                            createAt: {
                                                $gte: dateYesterdayStart,
                                                $lte: dateYesterdayEnd
                                            },
                                            status: STATUS_ACTIVE
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            sum: {
                                                $sum: "$loanAmount"
                                            },
                                            count: {
                                                $sum: 1
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ]);

                    // let countCustomer      = await CUSTOMER_AGENCY_COLL.find({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateStart,
                    //         $lte: dateEnd
                    //     }, 
                    // });
                    let listCustomer = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateStart,
                            $lte: dateEnd
                        },  
                    });
                    let listCustomerID = listCustomer.map( customer => customer.customer.toString() );
                    let countCustomer = [...new Set(listCustomerID)].length;

                    let listCustomerYesterday = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateStart,
                            $lte: dateEnd
                        },  
                    });
                    let listCustomerYesterdayID = listCustomerYesterday.map( customer => customer.customer.toString() );
                    let countCustomerYesterday = [...new Set(listCustomerYesterdayID)].length;
                    // let countCustomerYesterday      = await CUSTOMER_AGENCY_COLL.count({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateYesterdayStart,
                    //         $lte: dateYesterdayEnd
                    //     },
                    // });

                    // 7// Tính % Doanh Thu
                    // let countCustomer          = 0;
                    // let countCustomerYesterday = 0;

                    let transaction            = {};
                    let transactionYesterday   = {};

                    /**
                     * 1: Tổng số giao dịch hôm qua (countTransactionNow)
                     * 2: Tổng số giao dịch hôm kia (countTransactionYesterday)
                     */
                    let countTransactionNow        = 0;
                    let countTransactionYesterday  = 0;
                    /**
                     * 1: Tổng số tiền cầm hiện qua (totalTransactionNow)
                     * 2: Tổng số tiền cầm hôm kia (totalTransactionYesterday)
                     */
                    let totalTransactionNow       = 0;
                    let totalTransactionYesterday = 0;

                    // if(listTransactionNow[0].countCustomer.length) {
                    //     countCustomer            = listTransactionNow[0].countCustomer.length;
                    // }  
                    // if(listTransactionNow[0].countCustomerYesterday.length) {
                    //     countCustomerYesterday   = listTransactionNow[0].countCustomerYesterday.length;
                    // }  
                    if(listTransactionNow[0].transaction[0]) {
                        transaction              = listTransactionNow[0].transaction[0];
                        totalTransactionNow      = transaction.sum;
                        countTransactionNow      = transaction.count
                    } 
                    if(listTransactionNow[0].transactionYesterday[0]) {
                        transactionYesterday      =  listTransactionNow[0].transactionYesterday[0];``
                        totalTransactionYesterday =  transactionYesterday.sum;
                        countTransactionYesterday =  transactionYesterday.count
                    }  
                    
                    /**
                     * VỚI DOANH THU NGÀY HIỆN QUA < HÔM KIA
                     */
                    let objPercent2DayTransaction      = calculatedPercentDiff(totalTransactionNow, totalTransactionYesterday);
                    /**
                     * TÍNH PHẦN TRĂM CỦA TỔNG GIAO DỊCH
                     * Constant tổng giao dịch được thực hiện
                     */
                    let objPercent2DayCountTransaction = calculatedPercentDiff(countTransactionNow, countTransactionYesterday);
                    /**
                     * TÍNH PHẦN TRĂM CỦA TỔNG KHÁCH HÀNG
                     */
                    let objPercent2DayCustomer         = calculatedPercentDiff(countCustomer, countCustomerYesterday);

                    let description = {
                        TYPE_REVENUE_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                        TYPE_REVENUE_COUNT_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                        TYPE_REVENUE_CUSTOMER: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                    }
                    // 7// Đổ data vào OBJECT
                    let dataNow = {
                        description,
                        // Data của Ngày hiện tại
                        countTransactionNow,
                        totalTransactionNow,
                        countCustomerNow : countCustomer,

                        // Data của Ngày hôm qua
                        countTransactionYesterday,
                        totalTransactionYesterday,
                        countCustomerYesterday: countCustomerYesterday,

                        //
                        TYPE_REVENUE_TRANSACTION: objPercent2DayTransaction.ratio,
                        percentBetweent2DayTransaction: objPercent2DayTransaction.valueRatio,
                        TYPE_REVENUE_COUNT_TRANSACTION: objPercent2DayCountTransaction.ratio,
                        percentBetweent2DayCountTransaction: objPercent2DayCountTransaction.valueRatio,
                        TYPE_REVENUE_CUSTOMER: objPercent2DayCustomer.ratio,
                        percentBetweent2DayCustomer: objPercent2DayCustomer.valueRatio
                    }
                    return resolve({ error: false, data: dataNow });
                }

                // 3// VỚI TYPE LÀ NGÀY HÔM QUA
                if(type == TYPE_DATE_BEFORE) {
                    /**
                     * 1: Lấy ngày hiện tại 
                     * 2: Trừ đi 1 ngày để ra ngày hôm qua (yesterday)
                     */
                     let day             = date.getDate();
                     let yesterday       = 1;
                     let theDayBefore    = 2;
                     let dateYesterday       = new Date(date);
                     let dateTheDayBefore    = new Date(date);
                     
                     // 1// LẤY NGÀY HÔM NAY TRỪ ĐI 1 NGÀY
                     minusNumberToSetDate({date1: dateYesterday, numberToSet: yesterday});
                     
                     minusNumberToSetDate({date1: dateTheDayBefore, numberToSet: theDayBefore});
                    // console.log({ dateYesterday, dateTheDayBefore });
                     // 2// LẤY NGÀY HIỆN TẠI SET 0H
                     let dateStart = new Date(dateYesterday);
                     dateStart.setHours(0,0,0,0);
 
                     // 3// LẤY NGÀY HIỆN TẠI SET 24H
                     let dateEnd = new Date(dateYesterday);
                     dateEnd.setHours(23,59,59,999);
 
 
                     // 4// LẤY NGÀY HIỆN TẠI SET 0H
                     let dateYesterdayStart = new Date(dateTheDayBefore);
                     dateYesterdayStart.setHours(0,0,0,0);
 
                     // 5// LẤY NGÀY HIỆN TẠI SET 24H
                     let dateYesterdayEnd = new Date(dateTheDayBefore);
                     dateYesterdayEnd.setHours(23,59,59,999);

                     // 6// LẤY CÁC GIAO DỊCH TRONG NGÀY HÔM NAY
                     let listTransactionNow = await TRANSACTION_COLL.aggregate([
                        {
                            $facet: {
                                // "countCustomer": [
                                //     {
                                //         $match: {
                                //             agency: ObjectID(agencyID),
                                //             createAt: {
                                //                 $gte: dateStart,
                                //                 $lte: dateEnd
                                //             },
                                //             status: STATUS_ACTIVE
                                //         }
                                //     },
                                //     {
                                        
                                //         $group: { _id: "$customer", count: { $sum: 1 }},
                                        
                                //     }
                                // ],
                                "transaction": [
                                    {
                                        $match: {
                                            agency: ObjectID(agencyID),
                                            createAt: {
                                                $gte: dateStart,
                                                $lte: dateEnd
                                            },
                                            status: STATUS_ACTIVE
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            sum: {
                                                $sum: "$loanAmount"
                                            },
                                            count: {
                                                $sum: 1
                                            }
                                        }
                                    }
                                ],
                                // "countCustomerYesterday": [
                                //     {
                                //         $match: {
                                //             agency: ObjectID(agencyID),
                                //             createAt: {
                                //                 $gte: dateYesterdayStart,
                                //                 $lte: dateYesterdayEnd
                                //             },
                                //             status: STATUS_ACTIVE
                                //         }
                                //     },
                                //     {
                                //         $group: { _id: "$customer", count: { $sum: 1 }},
                                //     }
                                // ],
                                "transactionYesterday": [
                                    {
                                        $match: {
                                            agency: ObjectID(agencyID),
                                            createAt: {
                                                $gte: dateYesterdayStart,
                                                $lte: dateYesterdayEnd
                                            },
                                            status: STATUS_ACTIVE
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            sum: {
                                                $sum: "$loanAmount"
                                            },
                                            count: {
                                                $sum: 1
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ]);
                     
 
                    // 7// Tính % Doanh Thu
                    let listCustomer = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateStart,
                            $lte: dateEnd
                        }, 
                    });
                    let listCustomerID = listCustomer.map( customer => customer.customer.toString() );
                    let countCustomer = [...new Set(listCustomerID)].length;

                    // let countCustomer      = await CUSTOMER_AGENCY_COLL.count({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateStart,
                    //         $lte: dateEnd
                    //     }, 
                    // })

                    let listCustomerYesterday = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateStart,
                            $lte: dateEnd
                        },  
                    });
                    let listCustomerYesterdayID = listCustomerYesterday.map( customer => customer.customer.toString() );
                    let countCustomerYesterday = [...new Set(listCustomerYesterdayID)].length;
                    // let countCustomerYesterday      = await CUSTOMER_AGENCY_COLL.count({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateYesterdayStart,
                    //         $lte: dateYesterdayEnd
                    //     },
                    // });

                    let transaction            = {};
                    let transactionYesterday   = {};

                    /**
                     * 1: Tổng số giao dịch hôm qua (countTransactionNow)
                     * 2: Tổng số giao dịch hôm kia (countTransactionYesterday)
                     */
                    let countTransactionNow        = 0;
                    let countTransactionYesterday  = 0;
                    /**
                     * 1: Tổng số tiền cầm hiện qua (totalTransactionNow)
                     * 2: Tổng số tiền cầm hôm kia (totalTransactionYesterday)
                     */
                    let totalTransactionNow       = 0;
                    let totalTransactionYesterday = 0;

                    // if(listTransactionNow[0].countCustomer.length) {
                    //     countCustomer            = listTransactionNow[0].countCustomer.length;
                    // }  
                    // if(listTransactionNow[0].countCustomerYesterday.length) {
                    //     countCustomerYesterday   = listTransactionNow[0].countCustomerYesterday.length;
                    // }  
                    if(listTransactionNow[0].transaction[0]) {
                        transaction              = listTransactionNow[0].transaction[0];
                        totalTransactionNow      = transaction.sum;
                        countTransactionNow      = transaction.count
                    } 
                    if(listTransactionNow[0].transactionYesterday[0]) {
                        transactionYesterday      =  listTransactionNow[0].transactionYesterday[0];
                        totalTransactionYesterday =  transactionYesterday.sum;
                        countTransactionYesterday =  transactionYesterday.count
                    }  
 
                    /**
                     * VỚI DOANH THU NGÀY HIỆN QUA < HÔM KIA
                     */
                     let objPercent2DayTransaction      = calculatedPercentDiff(totalTransactionNow, totalTransactionYesterday);

                     /**
                      * TÍNH PHẦN TRĂM CỦA TỔNG GIAO DỊCH
                      * Constant tổng giao dịch được thực hiện
                      */
                     let objPercent2DayCountTransaction = calculatedPercentDiff(countTransactionNow, countTransactionYesterday);
                     /**
                      * TÍNH PHẦN TRĂM CỦA TỔNG KHÁCH HÀNG
                      */
                     let objPercent2DayCustomer         = calculatedPercentDiff(countCustomer, countCustomerYesterday);
 
                     let description = {
                         TYPE_REVENUE_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                         TYPE_REVENUE_COUNT_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                         TYPE_REVENUE_CUSTOMER: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                     }
                     // 7// Đổ data vào OBJECT
                     let dataNow = {
                         description,
                         // Data của Ngày hiện tại
                         countTransactionNow,
                         totalTransactionNow,
                         countCustomerNow : countCustomer,
 
                         // Data của Ngày hôm qua
                         countTransactionYesterday,
                         totalTransactionYesterday,
                         countCustomerYesterday: countCustomerYesterday,
 
                         //
                         TYPE_REVENUE_TRANSACTION: objPercent2DayTransaction.ratio,
                         percentBetweent2DayTransaction: objPercent2DayTransaction.valueRatio,
                         TYPE_REVENUE_COUNT_TRANSACTION: objPercent2DayCountTransaction.ratio,
                         percentBetweent2DayCountTransaction: objPercent2DayCountTransaction.valueRatio,
                         TYPE_REVENUE_CUSTOMER: objPercent2DayCustomer.ratio,
                         percentBetweent2DayCustomer: objPercent2DayCustomer.valueRatio
                     }
                     return resolve({ error: false, data: dataNow });
                }

                // 4// VỚI TYPE LÀ TUẦN NÀY
                if(type == TYPE_WEEK_NOW) {
                    /**
                     * 1: Lấy ngày hiện tại 
                     * 2: Trừ đi 1 ngày để ra ngày hôm qua (yesterday)
                     */
                    let day             = date.getDate();
                    let yesterday       = 1;
                    let theDayBefore    = 2;

                    /**
                     * 1: Lấy ngày hiện tại 
                     * 2:  => Với (dateYesterday) => Lấy tuần hiện tại
                     *     => Với (dateTheDayBefore) => Lấy tuần trước => Trừ đi 7 ngày
                     */
                    let dateNow             = new Date(date);
                    let dateTheDayBefore    = new Date(date);

                    /**
                     * 1 // Lấy ngày hiện tại - 7 ngày = Ngày này tuần trước
                     */
                    let weekBefore = 7;
                    minusNumberToSetDate({date1: dateTheDayBefore, numberToSet: weekBefore});

                    /**
                     * 2 // Lấy số tuần trong tháng và ngày trong tuần của tuần trước
                     */
                    let dayOfWeek = getWeek(dateTheDayBefore);
                    
                    /**
                     * 3 // Lấy ngày đầu tiên của tuần
                     */
                    let dateFirstOfWeekBefore = new Date(dateTheDayBefore);
                   
                    /**
                     * 5 // Tính ra ngày đầu tuần
                     * 1: Lấy ngày thứ của tuần đó (dayOfWeek)
                     * 2: (dayOfWeek) - 1 => Khoảng cách giữa ngày đầu tuần với ngày hiện tại
                     */
                    let dayFirstOfWeek = dayOfWeek.dayOfWeek - 1;
                    minusNumberToSetDate({date1: dateFirstOfWeekBefore, numberToSet: dayFirstOfWeek});
                    // Set lại giờ cho ngày bắt đầu tuần trước
                    dateFirstOfWeekBefore.setHours(0,0,0,0);
                    
                    /**
                     * 6 // Tính ra ngày cuối tuần
                     * 1: Lấy ngày thứ của tuần đó (dayOfWeek)
                     * 2: 7 - (dayOfWeek) => Khoảng cách giữa ngày cuối tuần với ngày hiện tại
                     */
                    let dateLastOfWeekBefore = new Date(dateTheDayBefore);
                    let dayLastOfWeek = 7 - dayOfWeek.dayOfWeek;
                    addNumberToSetDate({date1: dateLastOfWeekBefore, numberToSet: dayLastOfWeek});
                    // Set lại giờ cho ngày kết thúc tuần trước
                    dateLastOfWeekBefore.setHours(23,59,59,999);
                    
                    /**
                     * 7 //  Lấy số tuần trong tháng và ngày trong tuần của tuần hiện tại
                     */
                    let dateFirstOfWeekNow = new Date(dateNow);
                    let dayOfWeekNow       = getWeek(dateFirstOfWeekNow);

                    /**
                     * 5 // Tính ra ngày đầu tuần
                     * 1: Lấy ngày thứ của tuần đó (dayOfWeek)
                     * 2: (dayOfWeek) - 1 => Khoảng cách giữa ngày đầu tuần với ngày hiện tại
                     */
                    let dayFirstOfWeekNow = dayOfWeekNow.dayOfWeek - 1;
                    minusNumberToSetDate({date1: dateFirstOfWeekNow, numberToSet: dayFirstOfWeekNow});
                    // Set lại giờ cho ngày bắt đầu của tuần hiện tại
                    dateFirstOfWeekNow.setHours(0,0,0,0);
                    // console.log({ dateFirstOfWeekNow, dateLastOfWeekBefore, dateFirstOfWeekBefore });

                     // 6// LẤY CÁC GIAO DỊCH TRONG NGÀY HÔM NAY
                     let objWeekNow = {
                        $match: {
                            agency: ObjectID(agencyID),
                            createAt: {
                                $gte: dateFirstOfWeekNow,
                                $lte: dateNow
                            },
                           status: STATUS_ACTIVE
                        }
                     }
                     let objWeekBefore = {
                        $match: {
                            agency: ObjectID(agencyID),
                            createAt: {
                                $gte: dateFirstOfWeekBefore,
                                $lte: dateLastOfWeekBefore
                            },
                            status: STATUS_ACTIVE
                        }
                     }
                     let listTransactionNow = await TRANSACTION_COLL.aggregate([
                         {
                             $facet: {
                                //  "countCustomer": [
                                //      {
                                //         ...objWeekNow
                                //      },
                                //      {
                                //          $group: { _id: "$customer", count: { $sum: 1 }},
                                //      }
                                //  ],
                                 "transaction": [
                                     {
                                        ...objWeekNow
                                     },
                                     {
                                         $group: {
                                             _id: null,
                                             sum: {
                                                 $sum: "$loanAmount"
                                             },
                                             count: {
                                                 $sum: 1
                                             }
                                         }
                                     }
                                 ],
                                //  "countCustomerYesterday": [
                                //     {
                                //         ...objWeekBefore
                                //     },
                                //      {
                                //          $group: { _id: "$customer", count: { $sum: 1 }},
                                //      }
                                //  ],
                                 "transactionYesterday": [
                                     {
                                         ...objWeekBefore
                                     },
                                     {
                                         $group: {
                                             _id: null,
                                             sum: {
                                                 $sum: "$loanAmount"
                                             },
                                             count: {
                                                 $sum: 1
                                             }
                                         }
                                     }
                                 ]
                             }
                         }
                     ]);
                     
 
                    // 7// Tính % Doanh Thu
                    // let countCustomer      = await CUSTOMER_AGENCY_COLL.count({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateFirstOfWeekNow,
                    //         $lte: dateNow
                    //     }, 
                    // })
                    let listCustomer = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateFirstOfWeekNow,
                            $lte: dateNow
                        }, 
                    });
                    let listCustomerID = listCustomer.map( customer => customer.customer.toString() );
                    let countCustomer = [...new Set(listCustomerID)].length;
                    
                    let listCustomerYesterday = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateFirstOfWeekBefore,
                            $lte: dateLastOfWeekBefore
                        },  
                    });
                    let listCustomerYesterdayID = listCustomerYesterday.map( customer => customer.customer.toString() );
                    let countCustomerYesterday = [...new Set(listCustomerYesterdayID)].length;

                    // let countCustomerYesterday      = await CUSTOMER_AGENCY_COLL.count({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateFirstOfWeekBefore,
                    //         $lte: dateLastOfWeekBefore
                    //     },
                    // });

                    let transaction            = {};
                    let transactionYesterday   = {};

                    /**
                     * 1: Tổng số giao dịch hôm qua (countTransactionNow)
                     * 2: Tổng số giao dịch hôm kia (countTransactionYesterday)
                     */
                    let countTransactionNow        = 0;
                    let countTransactionYesterday  = 0;
                    /**
                     * 1: Tổng số tiền cầm hiện qua (totalTransactionNow)
                     * 2: Tổng số tiền cầm hôm kia (totalTransactionYesterday)
                     */
                    let totalTransactionNow       = 0;
                    let totalTransactionYesterday = 0;

                    // if(listTransactionNow[0].countCustomer.length) {
                    //     countCustomer            = listTransactionNow[0].countCustomer.length;
                    // }  
                    // if(listTransactionNow[0].countCustomerYesterday.length) {
                    //     countCustomerYesterday   = listTransactionNow[0].countCustomerYesterday.length;
                    // }  
                    if(listTransactionNow[0].transaction[0]) {
                        transaction              = listTransactionNow[0].transaction[0];
                        totalTransactionNow      = transaction.sum;
                        countTransactionNow      = transaction.count
                    } 
                    if(listTransactionNow[0].transactionYesterday[0]) {
                        transactionYesterday      =  listTransactionNow[0].transactionYesterday[0];
                        totalTransactionYesterday =  transactionYesterday.sum;
                        countTransactionYesterday =  transactionYesterday.count
                    }  
 
                     /**
                     * VỚI DOANH THU NGÀY HIỆN QUA < HÔM KIA
                     */
                    let objPercent2DayTransaction      = calculatedPercentDiff(totalTransactionNow, totalTransactionYesterday);

                    /**
                     * TÍNH PHẦN TRĂM CỦA TỔNG GIAO DỊCH
                     * Constant tổng giao dịch được thực hiện
                     */
                    let objPercent2DayCountTransaction = calculatedPercentDiff(countTransactionNow, countTransactionYesterday);
                    /**
                     * TÍNH PHẦN TRĂM CỦA TỔNG KHÁCH HÀNG
                     */
                    let objPercent2DayCustomer         = calculatedPercentDiff(countCustomer, countCustomerYesterday);

                    let description = {
                        TYPE_REVENUE_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                        TYPE_REVENUE_COUNT_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                        TYPE_REVENUE_CUSTOMER: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                    }
                    // 7// Đổ data vào OBJECT
                    let dataNow = {
                        description,
                        // Data của Ngày hiện tại
                        countTransactionNow,
                        totalTransactionNow,
                        countCustomerNow : countCustomer,

                        // Data của Ngày hôm qua
                        countTransactionYesterday,
                        totalTransactionYesterday,
                        countCustomerYesterday: countCustomerYesterday,

                        //
                        TYPE_REVENUE_TRANSACTION: objPercent2DayTransaction.ratio,
                        percentBetweent2DayTransaction: objPercent2DayTransaction.valueRatio,
                        TYPE_REVENUE_COUNT_TRANSACTION: objPercent2DayCountTransaction.ratio,
                        percentBetweent2DayCountTransaction: objPercent2DayCountTransaction.valueRatio,
                        TYPE_REVENUE_CUSTOMER: objPercent2DayCustomer.ratio,
                        percentBetweent2DayCustomer: objPercent2DayCustomer.valueRatio
                    }
                    return resolve({ error: false, data: dataNow });
                }

                // 4// VỚI TYPE LÀ THÁNG NÀY
                if(type == TYPE_MONTH_NOW) {
                    /**
                     * 1: Lấy ngày hiện tại 
                     * 2: Trừ đi 1 ngày để ra ngày hôm qua (yesterday)
                     */
                    let day             = date.getDate();
                    let yesterday       = 1;
                    let theDayBefore    = 2;

                    /**
                     * 1: Lấy ngày hiện tại 
                     * 2:  => Với (dateYesterday) => Lấy tuần hiện tại
                     *     => Với (dateTheDayBefore) => Lấy tuần trước => Trừ đi 7 ngày
                     */
                    let dateNow             = new Date(date);
                    let dateTheDayBefore    = new Date(date);

                    /**
                     * 1 // Lấy ngày hiện tại - 7 ngày = Ngày này tuần trước
                     */
                    let DATE_OF_MONTH = 30;

                    /**
                     * 2 // 
                     *   => 1: Lấy ngày hiện tại - 1 + Số ngày của 1 tháng
                     *   => 2: Lấy (monthBefore) số ngày để trở về ngày đầu tiên tháng trước đó
                     */
                    let dateFirstOfMoth = new Date(dateTheDayBefore);
                    let monthFirstBefore = Math.abs(day - 1 + DATE_OF_MONTH);
                    minusNumberToSetDate({date1: dateFirstOfMoth, numberToSet: monthFirstBefore});
                    dateFirstOfMoth.setHours(0,0,0,0);

                    /**
                     * 3 // 
                     *   => 1: Lấy ngày hiện tại 
                     *   => 2: Lấy (monthLastBefore) số ngày để trở về ngày cuối cùng tháng trước đó
                     */
                    let dateLastOfMoth     = new Date(dateTheDayBefore);
                    let monthLastBefore    = day;
                    minusNumberToSetDate({date1: dateLastOfMoth, numberToSet: monthLastBefore});
                    dateLastOfMoth.setHours(23,59,59,999);

                    /**
                     * 5 // 
                     *   => 1: Lấy ngày hiện tại 
                     *   => 2: Lấy (monthFirstBeforeNow) số ngày để trở về ngày đầu tiên tháng hiện tại
                     */
                    let dateLFirstOfMothNow     = new Date(dateNow);
                    let monthFirstBeforeNow     = day - 1;
                    minusNumberToSetDate({date1: dateLFirstOfMothNow, numberToSet: monthFirstBeforeNow});
                    dateLFirstOfMothNow.setHours(0,0,0,0);

                    // 6// LẤY CÁC GIAO DỊCH TRONG NGÀY HÔM NAY
                    let objMonthNow = {
                        $match: {
                            agency: ObjectID(agencyID),
                            createAt: {
                                $gte: dateLFirstOfMothNow,
                                $lte: dateNow
                            },
                            status: STATUS_ACTIVE
                        }
                    }
                    let objMonthBefore = {
                        $match: {
                            agency: ObjectID(agencyID),
                            createAt: {
                                $gte: dateFirstOfMoth,
                                $lte: dateLastOfMoth
                            },
                            status: STATUS_ACTIVE
                        }
                    }
                    let listTransactionNow = await TRANSACTION_COLL.aggregate([
                        {
                            $facet: {
                                // "countCustomer": [
                                //     {
                                //         ...objMonthNow
                                //     },
                                //     {
                                //         $group: { _id: "$customer", count: { $sum: 1 }},
                                //     }
                                // ],
                                "transaction": [
                                    {
                                        ...objMonthNow
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            sum: {
                                                $sum: "$loanAmount"
                                            },
                                            count: {
                                                $sum: 1
                                            }
                                        }
                                    }
                                ],
                                // "countCustomerYesterday": [
                                //     {
                                //         ...objMonthBefore
                                //     },
                                //     {
                                //         $group: { _id: "$customer", count: { $sum: 1 }},
                                //     }
                                // ],
                                "transactionYesterday": [
                                    {
                                        ...objMonthBefore
                                    },
                                    {
                                        $group: {
                                            _id: null,
                                            sum: {
                                                $sum: "$loanAmount"
                                            },
                                            count: {
                                                $sum: 1
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ]);
                    

                    // 7// Tính % Doanh Thu
                    // let countCustomer      = await CUSTOMER_AGENCY_COLL.count({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateLFirstOfMothNow,
                    //         $lte: dateNow
                    //     }, 
                    // })

                    // let countCustomerYesterday      = await CUSTOMER_AGENCY_COLL.count({ 
                    //     agency: ObjectID(agencyID),
                    //     createAt: {
                    //         $gte: dateFirstOfMoth,
                    //         $lte: dateLastOfMoth
                    //     },
                    // });

                    let listCustomer = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateLFirstOfMothNow,
                            $lte: dateNow
                        }, 
                    });
                    let listCustomerID = listCustomer.map( customer => customer.customer.toString() );
                    let countCustomer = [...new Set(listCustomerID)].length;
                    
                    let listCustomerYesterday = await CUSTOMER_AGENCY_COLL.find({ 
                        agency: ObjectID(agencyID),
                        createAt: {
                            $gte: dateFirstOfMoth,
                            $lte: dateLastOfMoth
                        },  
                    });
                    let listCustomerYesterdayID = listCustomerYesterday.map( customer => customer.customer.toString() );
                    let countCustomerYesterday = [...new Set(listCustomerYesterdayID)].length;

                    let transaction            = {};
                    let transactionYesterday   = {};

                    /**
                     * 1: Tổng số giao dịch hôm qua (countTransactionNow)
                     * 2: Tổng số giao dịch hôm kia (countTransactionYesterday)
                     */
                    let countTransactionNow        = 0;
                    let countTransactionYesterday  = 0;
                    /**
                     * 1: Tổng số tiền cầm hiện qua (totalTransactionNow)
                     * 2: Tổng số tiền cầm hôm kia (totalTransactionYesterday)
                     */
                    let totalTransactionNow       = 0;
                    let totalTransactionYesterday = 0;

                    // if(listTransactionNow[0].countCustomer.length) {
                    //     countCustomer            = listTransactionNow[0].countCustomer.length;
                    // }  
                    // if(listTransactionNow[0].countCustomerYesterday.length) {
                    //     countCustomerYesterday   = listTransactionNow[0].countCustomerYesterday.length;
                    // }  
                    if(listTransactionNow[0].transaction[0]) {
                        transaction              = listTransactionNow[0].transaction[0];
                        totalTransactionNow      = transaction.sum;
                        countTransactionNow      = transaction.count
                    } 
                    if(listTransactionNow[0].transactionYesterday[0]) {
                        transactionYesterday      =  listTransactionNow[0].transactionYesterday[0];
                        totalTransactionYesterday =  transactionYesterday.sum;
                        countTransactionYesterday =  transactionYesterday.count
                    }  

                    /**
                     * VỚI DOANH THU NGÀY HIỆN QUA < HÔM KIA
                     */
                     let objPercent2DayTransaction      = calculatedPercentDiff(totalTransactionNow, totalTransactionYesterday);

                     /**
                      * TÍNH PHẦN TRĂM CỦA TỔNG GIAO DỊCH
                      * Constant tổng giao dịch được thực hiện
                      */
                     let objPercent2DayCountTransaction = calculatedPercentDiff(countTransactionNow, countTransactionYesterday);
                     /**
                      * TÍNH PHẦN TRĂM CỦA TỔNG KHÁCH HÀNG
                      */
                     let objPercent2DayCustomer         = calculatedPercentDiff(countCustomer, countCustomerYesterday);
 
                     let description = {
                         TYPE_REVENUE_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                         TYPE_REVENUE_COUNT_TRANSACTION: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                         TYPE_REVENUE_CUSTOMER: "0: (Doanh thu giữ nguyên) || 1: (Tăng) || -1: (Giảm)",
                     }
                     // 7// Đổ data vào OBJECT
                     let dataNow = {
                         description,
                         // Data của Ngày hiện tại
                         countTransactionNow,
                         totalTransactionNow,
                         countCustomerNow : countCustomer,
 
                         // Data của Ngày hôm qua
                         countTransactionYesterday,
                         totalTransactionYesterday,
                         countCustomerYesterday: countCustomerYesterday,
 
                         //
                         TYPE_REVENUE_TRANSACTION: objPercent2DayTransaction.ratio,
                         percentBetweent2DayTransaction: objPercent2DayTransaction.valueRatio,
                         TYPE_REVENUE_COUNT_TRANSACTION: objPercent2DayCountTransaction.ratio,
                         percentBetweent2DayCountTransaction: objPercent2DayCountTransaction.valueRatio,
                         TYPE_REVENUE_CUSTOMER: objPercent2DayCustomer.ratio,
                         percentBetweent2DayCustomer: objPercent2DayCustomer.valueRatio
                     }
                     return resolve({ error: false, data: dataNow });
                }

                return resolve({ error: false, data: infoAgencyUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy khoảng cách một agency
     * Dattv
     */
     getDistanceOfAgency({ agencyID, lng, lat }) {
        return new Promise(async resolve => {
            try {

                if (!lng || !lat){
                    return resolve({ error: true, message: "lng_lat_not_provide" });
                }else{

                    let infoAgencyDistance = await AGENCY_COLL.aggregate([
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                                maxDistance: 700000,
                                minDistance: 0,
                                distanceMultiplier: 0.001, //Returns km
                                distanceField: "dist.calculated",
                                includeLocs: "dist.location", // Returns distance
                                spherical: true
                            }
                        },
                        {
                            $match: { _id: ObjectID(agencyID) }
                        },
                        {
                            $project: { 
                                "name": 1,
                                "dist": 1,
                            }
                        }
                    ]);

                    return resolve({ error: false, data: infoAgencyDistance[0] });
                }

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;