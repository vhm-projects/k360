const AGENCY_MODEL 		= require('./models/agency').MODEL;
const AGENCY_COLL  		= require('./databases/agency-coll')({});
const AGENCY_ROUTES     = require('./apis/agency');

module.exports = {
    AGENCY_ROUTES,
    AGENCY_COLL,
    AGENCY_MODEL,
}
