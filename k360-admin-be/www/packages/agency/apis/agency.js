"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_AGENCY }            = require('../constants/agency.uri');

/**
 * MODELS
 */
const AGENCY_MODEL             				= require("../models/agency").MODEL;
const { IMAGE_MODEL }          				= require('../../image');
const { provinces }            				= require('../../common/constant/provinces');
const { districts }            				= require('../../common/constant/districts');
const { AGENCY_PRODUCT_CATEGORY_MODEL }    	= require('../../agency_product_category');
const { BLACKLIST_COLL } 					= require('../../blacklist');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            
            /**
             * Function: Trang Danh sách agency (View)
             * Date: 17/04/2021
             * Dev: Dattv
             * Update: Thêm field cho bộ lọc -
             * + name, city, district, ward
             */
             [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY,
					inc: path.resolve(__dirname, '../views/list_agency.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						let { name, city, district, ward, start, end, sort } = req.query;

                        let listAgency = await AGENCY_MODEL.filterAgency({ name, city, district, ward, start, end, sort });
                        
                        let listAgencyShowAdmin = await AGENCY_MODEL.listAgencyShowAdmin({ name, city, district, ward, fromDay: start, toDay: end });
                        if (listAgency.data.listAgency && listAgency.data.listAgency.length) {
                            listAgency.data.listAgency.forEach((agency, index) => {
                                if (listAgencyShowAdmin.data && listAgencyShowAdmin.data .length) {
                                    listAgencyShowAdmin.data.forEach( agencyCalculate => {
                                        if (agency._id.toString() == agencyCalculate._id.toString()) {
                                            /**
                                             *  agencyCalculate: {
                                                    _id: 60a72d01bfd65f6f1d155263,
                                                    name: 'Test Đại lý',
                                                    code: 'TDL',
                                                    parent: undefined,
                                                    amountPawn: 0,
                                                    originalAmt: 0,
                                                    redeem: 0,
                                                    redeemAmt: 0,
                                                    totalInterest: 0,
                                                    totalInterestAmt: 0
                                                }
                                             */
                                            let objAgency = {
                                                _id:       agency._id,
                                                childs:    agency.childs,
                                                owners:    agency.owners,
                                                employees: agency.employees,
                                                gallery:   agency.gallery,
                                                openingBalance: agency.openingBalance,
                                                funds:          agency.funds,
                                                status:         agency.status,
                                                name:        agency.name,
                                                email:       agency.email,
                                                phone:       agency.phone,
                                                address:    agency.address,
                                                code:       agency.code,
                                                tax_code:   agency.tax_code,
                                                ward:       agency.ward,
                                                openTime:   agency.openTime,
                                                closeTime:   agency.closeTime,
                                                userCreate:  agency.userCreate,
                                                location:   agency.location,
                                                parent:     agency.parent,
                                                modifyAt:   agency.modifyAt,
                                                createAt:   agency.createAt,
                                                userUpdate: agency.userUpdate,
                                                avatar:     agency.avatar,
                                            }
                                            listAgency.data.listAgency[index] = {
                                                ...objAgency,
                                                amountPawn:  agencyCalculate.amountPawn,
                                                originalAmt: agencyCalculate.originalAmt,
                                                redeem:      agencyCalculate.redeem,
                                                redeemAmt:   agencyCalculate.redeemAmt,
                                                totalInterestAmt:   agencyCalculate.totalInterestAmt,
                                                totalCustomer:   agencyCalculate.totalCustomer,
                                            }
                                        }
                                    })
                                }
                            });
                        }
                        let listProvince = Object.entries(provinces);   

                        ChildRouter.renderToView(req, res, {
                            listAgency: listAgency.data.listAgency,
                            listAgencyBestSale: listAgency.data.listAgencyBestSale,
                            listTopAgencyHaveMostCustomer: listAgency.data.listTopAgencyHaveMostCustomer,
                            listProvince,
                            city, district, ward, name, sort, start, end,
                        });
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY_MOBILE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						let { name, city, fromDay, toDay, district, ward, start, end, sort } = req.query;
                        let listAgency          = await AGENCY_MODEL.filterAgency({ name, city, district, ward, start, end, sort });
                        let listAgencyShowAdmin = await AGENCY_MODEL.listAgencyShowAdmin({ name, city, district, ward, fromDay, toDay });

                        let listProvince        = Object.entries(provinces);
                        let listAgencyChange            = [];
                        let listDistricts       = Object.entries(districts);
                        // 1// Chạy từng phần tử trong mảng Danh sách Agency
                        if(listAgency.error == false && listAgency.data.listAgency.length) {
                            listAgency.data.listAgency.forEach(agency => {
                                let nameProvince;
                                let objAgency = {};
                                // 2// Lấy tên thành phố
                                if(agency.city) {
                                    for(let province of listProvince) {
                                        if(province[1].code == agency.city) {
                                            nameProvince = province[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                                // 3// Lấy field trong của agency đó
                                objAgency = {
                                    _id:       agency._id,
                                    childs:    agency.childs,
                                    owners:    agency.owners,
                                    employees: agency.employees,
                                    gallery:   agency.gallery,
                                    openingBalance: agency.openingBalance,
                                    funds: agency.funds,
                                    status: agency.status,
                                    name:        agency.name,
                                    email:       agency.email,
                                    phone:       agency.phone,
                                    address:    agency.address,
                                    code:       agency.code,
                                    tax_code:   agency.tax_code,
                                    ward:       agency.ward,
                                    openTime:   agency.openTime,
                                    closeTime:   agency.closeTime,
                                    userCreate:  agency.userCreate,
                                    location:   agency.location,
                                    parent:     agency.parent,
                                    modifyAt:   agency.modifyAt,
                                    createAt:   agency.createAt,
                                    userUpdate: agency.userUpdate,
                                    avatar:     agency.avatar,
                                    city:       nameProvince
                                }
                                // 4// Lấy quận/ Huyện
                                if(agency.district) {
                                    for(let district of listDistricts) {
                                        if(district[1].code == agency.district ){
                                            objAgency.district = district[1].name_with_type;
                                            break;
                                        }
                                    }
                                }

                                // 5// Push vào mảng mới
                                listAgencyChange.push(objAgency)
                            })
                        }

                        let data = {
                            listAgency: listAgencyChange
                        }

                        let description = { statusAgency: "Trạng thái hoạt động (1: Hoạt động; 0: Khóa)" };

                        // res.json({ description, ...listAgency, data, listAgencyShowAdmin });
                        res.json({ description, error:false, data, listAgencyShowAdmin });
                    }]
                },
            },

            
            /**
             * API MOBILE ENDUSER
             * FUNCTION: LẤY DANH SÁCH ĐẠI LÝ
             * SONLP zzzz
             */
            [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY_CUSTOMER_MOBILE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						let { name, city, district, ward, start, end, sort } = req.query;

                        let listAgency = await AGENCY_MODEL.filterAgency({ name, city, district, ward, start, end, sort, status: 1 });

                        let listProvince        = Object.entries(provinces);
                        let listAgencyChange            = [];
                        let listDistricts       = Object.entries(districts);
                        // 1// Chạy từng phần tử trong mảng Danh sách Agency
                        if(listAgency.error == false && listAgency.data.listAgency.length) {
                            listAgency.data.listAgency.forEach( agency => {
                                let nameProvince;
                                let objAgency = {};
                                // 2// Lấy tên thành phố
                                if(agency.city) {
                                    for(let province of listProvince) {
                                        if(province[1].code == agency.city) {
                                            nameProvince = province[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                                // 3// Lấy field trong của agency đó
                                objAgency = {
                                    _id:       agency._id,
                                    childs:    agency.childs,
                                    owners:    agency.owners,
                                    employees: agency.employees,
                                    gallery:   agency.gallery,
                                    openingBalance: agency.openingBalance,
                                    funds: agency.funds,
                                    status: agency.status,
                                    name:        agency.name,
                                    email:       agency.email,
                                    phone:       agency.phone,
                                    address:    agency.address,
                                    code:       agency.code,
                                    tax_code:   agency.tax_code,
                                    ward:       agency.ward,
                                    openTime:   agency.openTime,
                                    closeTime:   agency.closeTime,
                                    userCreate:  agency.userCreate,
                                    location:   agency.location,
                                    parent:     agency.parent,
                                    modifyAt:   agency.modifyAt,
                                    createAt:   agency.createAt,
                                    userUpdate: agency.userUpdate,
                                    avatar:     agency.avatar,
                                    city:       nameProvince
                                }
                                // 4// Lấy quận/ Huyện
                                if(agency.district) {
                                    for(let district of listDistricts) {
                                        if(district[1].code == agency.district ){
                                            objAgency.district = district[1].name_with_type;
                                            break;
                                        }
                                    }
                                }

                                // 5// Push vào mảng mới
                                listAgencyChange.push(objAgency)
                            })
                        }
                        let data = {
                            listAgency: listAgencyChange
                        }

                        let description = { statusAgency: "Trạng thái hoạt động (1: Hoạt động; 0: Khóa)" };
                        // if(listAgency.error) {
                        //     return res.json({ description, ...listAgency })
                        // }
                        res.json({ description, error: false, data});
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY_CUSTOMER_MOBILE_V2]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						let { name, city, district, ward, start, end, sort } = req.query;

                        let listAgency = await AGENCY_MODEL.filterAgencyV2({ name, city, district, ward, start, end, sort });
                        let listProvince        = Object.entries(provinces);
                        let listAgencyChange    = [];
                        let listDistricts       = Object.entries(districts);
                        // 1// Chạy từng phần tử trong mảng Danh sách Agency
                        if(listAgency.error == false && listAgency.data.length) {
                            for (let agency of listAgency.data) {
                                let cityName;
                                let districtName;
                                let objAgency = {};
                                // 2// Lấy tên thành phố
                                if(agency.city) {
                                    for(let province of listProvince) {
                                        if(province[1].code == agency.city) {
                                            cityName = province[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                                // 4// Lấy quận/ Huyện
                                let infoDistrict;
                                if(agency.district) {
                                    for(let district of listDistricts) {
                                        if(district[1].code == agency.district ){
                                            districtName = district[1].name_with_type;
                                            infoDistrict = district[1];
                                            break;
                                        }
                                    }
                                }
                                let obj = {
                                    _id: agency._id,
                                    gallery: agency.gallery,
                                    avatar: agency.avatar,
                                    status: agency.status,
                                    name: agency.name,
                                    email: agency.email,
                                    phone: agency.phone,
                                    address: agency.address,
                                    openTime: agency.openTime,
                                    closeTime: agency.closeTime,
                                    userCreate: agency.userCreate,
                                    code: agency.code,
                                    tax_code: agency.tax_code,
                                    city: agency.city,
                                    district: agency.district,
                                    ward: agency.ward,
                                    location: agency.location,
                                    createAt: agency._id,
                                    cityName,
                                    districtName,
                                }
                                let filePath = path.resolve(__dirname, `../../common/constant/wards/${infoDistrict.code}.json`);
                                let listWards;
                                let dataOfReadFile = fs.readFileSync(filePath, {encoding: 'utf-8'});
                                listWards = JSON.parse(dataOfReadFile);
                                listWards = Object.entries(listWards);
                                for(let ward of listWards) {
                                    if(ward[1].code == agency.ward ) { 
                                        obj = {
                                            ...obj,
                                            wardName: ward[1].name_with_type
                                        }
                                    }
                                }
                                listAgencyChange.push(obj)
                                // obj = {
                                //     ...obj,
                                //     wardName: wardName
                                // }
                                /**
                                 * {
                                    _id: 60a6301cbfd65f6f1d155252,
                                    childs: [],
                                    owners: [Array],
                                    employees: [],
                                    gallery: [],
                                    openingBalance: 500000000,
                                    funds: 475000000,
                                    status: 1,
                                    name: 'Cửa hàng Bến Thành',
                                    email: 'trang.nguyenth@k-group.asia',
                                    phone: '0984999999',
                                    address: '65 đường Trương Định',
                                    openTime: '08:00',
                                    closeTime: '20:00',
                                    userCreate: 607becdd81f688f868fa1f3b,
                                    code: 'CHBT',
                                    tax_code: '001',
                                    city: '01',
                                    district: '760',
                                    ward: '26743',
                                    location: [Object],
                                    modifyAt: 2021-05-20T09:47:08.000Z,
                                    createAt: 2021-05-20T09:47:08.000Z,
                                    __v: 0,
                                    userUpdate: 607becdd81f688f868fa1f3b,
                                    cityName: 'Thành phố Hồ Chí Minh',
                                    districtName: 'Quận 1'
                                    },
                                 */
                                
                                // 5// Push vào mảng mới
                            }
                        }

                        let data = {
                            listAgency: listAgencyChange
                        }

                        let description = { statusAgency: "Trạng thái hoạt động (1: Hoạt động; 0: Khóa)" };
                        // if(listAgency.error) {
                        //     return res.json({ description, ...listAgency })
                        // }
                        res.json({ description, error: false, data});
                    }]
                },
            },
            /**
             * API MOBILE ENDUSER
             * FUNCTION: LẤY DANH SÁCH ĐẠI LÝ GẦN ĐÂY
             * SONLP zzzz
             */
             [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY_CUSTOMER_NEAR_MOBILE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json'
                },
                methods: {
                    get: [ async function (req, res) {
						let { name, city, district, ward, lng, lat, maxDistance } = req.query;

                        let listAgency = await AGENCY_MODEL.getListAgencyNearMobile({ name, city, district, ward, lng, lat, maxDistance });

                        let listProvince        = Object.entries(provinces);
                        let listAgencyChange            = [];

                        let listDistricts       = Object.entries(districts);
                        // 1// Chạy từng phần tử trong mảng Danh sách Agency
                        if(listAgency.error == false && listAgency.data.length) {
                            listAgency.data.forEach( agency => {
                                let nameProvince;
                                let objAgency = {};
                                // 2// Lấy tên thành phố
                                if(agency.city) {
                                    for(let province of listProvince) {
                                        if(province[1].code == agency.city) {
                                            nameProvince = province[1].name_with_type;
                                            break;
                                        }
                                    }
                                }
                                // 3// Lấy field trong của agency đó
                                objAgency = {
                                    _id:       agency._id,
                                    childs:    agency.childs,
                                    owners:    agency.owners,
                                    employees: agency.employees,
                                    gallery:   agency.gallery,
                                    openingBalance: agency.openingBalance,
                                    funds: agency.funds,
                                    status: agency.status,
                                    name:        agency.name,
                                    email:       agency.email,
                                    phone:       agency.phone,
                                    address:    agency.address,
                                    code:       agency.code,
                                    tax_code:   agency.tax_code,
                                    ward:       agency.ward,
                                    openTime:   agency.openTime,
                                    closeTime:   agency.closeTime,
                                    userCreate:  agency.userCreate,
                                    location:   agency.location,
                                    parent:     agency.parent,
                                    modifyAt:   agency.modifyAt,
                                    createAt:   agency.createAt,
                                    userUpdate: agency.userUpdate,
                                    dist:       agency.dist,
                                    city:       nameProvince
                                }

                                if(agency.avatar && agency.avatar.length) {
                                    objAgency.avatar =  agency.avatar[0];
                                }else{
                                    objAgency.avatar =  agency.avatar
                                }

                                if(lng && lat) {
                                    objAgency.dist =  agency.dist;
                                }else{
                                    objAgency.dist =  {};
                                }

                                // 4// Lấy quận/ Huyện
                                if(agency.district) {
                                    for(let district of listDistricts) {
                                        if(district[1].code == agency.district ){
                                            objAgency.district = district[1].name_with_type;
                                            break;
                                        }
                                    }
                                }

                                // 5// Push vào mảng mới
                                listAgencyChange.push(objAgency)
                            })
                        }
                        let data = {
                            listAgency: listAgencyChange
                        }

                        let description = { statusAgency: "Trạng thái hoạt động (1: Hoạt động; 0: Khóa)" };
                        // if(listAgency.error) {
                        //     return res.json({ description, ...listAgency })
                        // }
                        res.json({ description, error: false, data});
                    }]
                },
            },
            /**
             * Function: Trang Chi tiết agency (View)
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.VIEW_DETAIL_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `Detail Agency - K360`,
					code: CF_ROUTINGS_AGENCY.VIEW_DETAIL_AGENCY,
					inc: path.resolve(__dirname, '../views/detail_agency.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;

                        let infoAgency = await AGENCY_MODEL.getInfo({ agencyID });
						let listBlacklistUserOfAgency = await BLACKLIST_COLL
							.find({ agency: agencyID })
							.select('user')
							.lean();

						listBlacklistUserOfAgency = listBlacklistUserOfAgency.map(blacklist => blacklist.user.toString());

                        let listProvince = Object.entries(provinces);
                        let listDistricts = Object.entries(districts);
                        let listWards;
                        let infoProvince;
                        let infoWard;
                        let infoDistrict;

                        for (let province of listProvince){
                            if ( province[1].code == infoAgency.data.city ){
                                infoProvince = province[1];
                                break;
                            }
                        }

                        for (let district of listDistricts){
                            if ( district[1].code == infoAgency.data.district ){
                                infoDistrict = district[1];
                                break;
                            }
                        }

                        if(!infoDistrict || !infoDistrict.code)
                            return ChildRouter.renderToView(req, res, {
                                infoAgency: infoAgency.data,
                                listProvince,
                                infoDistrict,
                                infoProvince,
                                infoWard: null,
								listBlacklistUserOfAgency
                            });

                        let filePath = path.resolve(__dirname, `../../common/constant/wards/${infoDistrict.code}.json`);


                        await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                listWards = JSON.parse(data);
                                listWards = Object.entries(listWards);
                                for (let ward of listWards){
                                    if ( ward[1].code == infoAgency.data.ward ){
                                        infoWard = ward[1];
                                        break;
                                    }
                                }

                                ChildRouter.renderToView(req, res, {
                                    infoAgency: infoAgency.data,
                                    listProvince,
                                    infoDistrict,
                                    infoProvince,
                                    infoWard,
									listBlacklistUserOfAgency
                                });
                                
                            }
                        });
                    }]
                },
            },

            //========================= JSON ============================

            /**
             * Function: Insert Agency
             * Date: 16/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.ADD_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userCreate } = req.user;
                        const { 
                                name, email, phone, code, tax_code, address, city, district, ward, latitude, longitude, 
                                openTime, closeTime, parent, gallery, avatar
                            } = req.body;

                        let dataInsert = {
                            name, email, phone, code, tax_code, address, city, district, ward, latitude, longitude, openTime, 
                            closeTime, parent, userCreate
                        }

                        if(avatar){
                            dataInsert.image_avatar_agency = JSON.parse(avatar);
                        }

                        if(gallery){
                            dataInsert.image_gallery_agency = JSON.parse(gallery);
                        }
                        
                        const infoAfterInsertAgency = await AGENCY_MODEL.insert(dataInsert);

                        res.json(infoAfterInsertAgency);
                    }]
                },
            },

            /**
             * Function: Info Agency
             * Date: 16/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.INFO_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID } = req.params;
                        const { type } = req.query;

                        const infoAgency = await AGENCY_MODEL.getInfo({ agencyID });
                        if(infoAgency.error) {
                            return res.json(infoAgency);
                        }
                        let listProvince        = Object.entries(provinces);
                        let listDistricts       = Object.entries(districts);

                        let city;
                        if(infoAgency.data.city) {
                            for(let province of listProvince) {
                                if(province[1].code == infoAgency.data.city) {
                                    city = province[1].name_with_type;
                                    break;
                                }
                            }
                        } 

                        let districtName;
                        let infoDistrict;
                        if(infoAgency.data.district) {
                            for(let district of listDistricts) {
                                if(district[1].code == infoAgency.data.district ){
                                    districtName = district[1].name_with_type;
                                    infoDistrict = district[1];
                                    break;
                                }
                            }
                        }

                        let objAgency = {
                            _id:       infoAgency.data._id,
                            childs:    infoAgency.data.childs,
                            owners:    infoAgency.data.owners,
                            employees: infoAgency.data.employees,
                            gallery:   infoAgency.data.gallery,
                            openingBalance: infoAgency.data.openingBalance,
                            funds: infoAgency.data.funds,
                            status: infoAgency.data.status,
                            name:        infoAgency.data.name,
                            email:       infoAgency.data.email,
                            phone:       infoAgency.data.phone,
                            address:    infoAgency.data.address,
                            code:       infoAgency.data.code,
                            tax_code:   infoAgency.data.tax_code,
                            ward:       infoAgency.data.ward,
                            openTime:   infoAgency.data.openTime,
                            closeTime:   infoAgency.data.closeTime,
                            userCreate:  infoAgency.data.userCreate,
                            location:   infoAgency.data.location,
                            parent:     infoAgency.data.parent,
                            modifyAt:   infoAgency.data.modifyAt,
                            createAt:   infoAgency.data.createAt,
                            userUpdate: infoAgency.data.userUpdate,
                            avatar:     infoAgency.data.avatar,
                            city:       infoAgency.data.city,
                            district:       infoAgency.data.district,
                            cityName:       city,
                            districtName:   districtName,
                        }
                        let description = { status: "Trạng thái hoạt động (1: Đang hoạt động, 0: Khóa)" }
                        let filePath = path.resolve(__dirname, `../../common/constant/wards/${infoDistrict.code}.json`);

                        await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                let listWards = JSON.parse(data);
                                listWards = Object.entries(listWards);
                                for (let ward of listWards){
                                    if ( ward[1].code == infoAgency.data.ward ) {
                                        objAgency = {
                                            ...objAgency,
                                            wardName: ward[1].name_with_type,
                                        }
                                        break;
                                    }
                                }
                                return res.json({ description, error: false, data: objAgency });
                            } 
                        });
                        
                        // res.json(infoAgency);
                    }]
                },
            },

            /**
             * API MOBILE ENDUSER
             * FUNCTION: LẤY CHI TIẾT ĐẠI LÝ
             * SONLP
             */
             [CF_ROUTINGS_AGENCY.INFO_AGENCY_CUSTOMER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID } = req.params;
                        const { type } = req.query;

                        const infoAgency = await AGENCY_MODEL.getInfo({ agencyID });

                        if(infoAgency.error) {
                            return res.json(infoAgency);
                        }

                        let listParentCategoryOfAgency = await AGENCY_PRODUCT_CATEGORY_MODEL.getListParent({ agencyID });
                        // console.log({ listParentCategoryOfAgency });
                        // let listCategoryOfAgency = await AGENCY_PRODUCT_CATEGORY_MODEL.getListAll({ agencyID });

                        let listCategoryOfAgency = listParentCategoryOfAgency.data.filter( parentCategory => {
                            if(parentCategory.product_category && parentCategory.product_category != null) {
                                return parentCategory.product_category
                            }
                        })
                        let listProvince        = Object.entries(provinces);
                        let listDistricts       = Object.entries(districts);

                        let city;
                        if(infoAgency.data.city) {
                            for(let province of listProvince) {
                                if(province[1].code == infoAgency.data.city) {
                                    city = province[1].name_with_type;
                                    break;
                                }
                            }
                        } 

                        let districtName;
                        if(infoAgency.data.district) {
                            for(let district of listDistricts) {
                                if(district[1].code == infoAgency.data.district ){
                                    districtName = district[1].name_with_type;
                                    break;
                                }
                            }
                        }
                        let objAgency = {
                            _id:       infoAgency.data._id,
                            childs:    infoAgency.data.childs,
                            owners:    infoAgency.data.owners,
                            employees: infoAgency.data.employees,
                            gallery:   infoAgency.data.gallery,
                            openingBalance: infoAgency.data.openingBalance,
                            funds: infoAgency.data.funds,
                            status: infoAgency.data.status,
                            name:        infoAgency.data.name,
                            email:       infoAgency.data.email,
                            phone:       infoAgency.data.phone,
                            address:    infoAgency.data.address,
                            code:       infoAgency.data.code,
                            tax_code:   infoAgency.data.tax_code,
                            ward:       infoAgency.data.ward,
                            openTime:   infoAgency.data.openTime,
                            closeTime:   infoAgency.data.closeTime,
                            userCreate:  infoAgency.data.userCreate,
                            location:   infoAgency.data.location,
                            parent:     infoAgency.data.parent,
                            modifyAt:   infoAgency.data.modifyAt,
                            createAt:   infoAgency.data.createAt,
                            userUpdate: infoAgency.data.userUpdate,
                            avatar:     infoAgency.data.avatar,
                            city:       city,
                            district:   districtName,
                            listCategoryOfAgency: listCategoryOfAgency || []
                        }
                        
                        let description = { status: "Trạng thái hoạt động (1: Đang hoạt động, 0: Khóa)" }
                        res.json({ description, error: false, data: objAgency });
                        // res.json(infoAgency);
                    }]
                },
            },
            /**
             * Function: Danh sách Agency
             * Date: 05/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.LIST_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { name, city, district, ward } = req.body;
                        const listAgency = await AGENCY_MODEL.filterAgency({ name, city, district, ward });
                        res.json(listAgency);
                    }]
                },
            },
            

            /**
             * Function: Info Agency by Email
             * Date: 16/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.CHECK_EXISTED_EMAIL]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email } = req.body;
                        const infoAgency = await AGENCY_MODEL.getInfoByEmail({ email });
                        res.json(infoAgency);
                    }]
                },
            },

            /**
             * Function: Check existed agency by code
             * Date: 27/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.CHECK_EXISTED_CODE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { code } = req.body;
                        const infoAgency = await AGENCY_MODEL.checkExistedAgencyByCode({ code });
                        res.json(infoAgency);
                    }]
                },
            },

            /**
             * Function: Danh sách member thuộc agency
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.LIST_MEMBER_AGENCY]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID } = req.params;
                        const listMemberOfAgency = await AGENCY_MODEL.getListMemberOfAgency({ agencyID });
                        res.json(listMemberOfAgency);
                    }]
                },
            },

            /**
             * Function: Danh sách agency gần đây
             * Date: 18/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.LIST_AGENCY_NEAR]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { lng, lat, maxDistance, minDistance } = req.query;
                        const listAgencyNear = await AGENCY_MODEL.getListAgencyNear({ lng, lat, maxDistance, minDistance });
                        res.json(listAgencyNear);
                    }]
                },
            },

            /**
             * Function: Danh sách agency theo địa chỉ
             * Date: 02/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.LIST_AGENCY_BY_ADDRESS]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { name, city, district, ward } = req.query;
                        const listAgencyResult = await AGENCY_MODEL.getList({ name, city, district, ward });
                        res.json(listAgencyResult);
                    }]
                },
            },

            /**
             * Function: Xóa agency
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.REMOVE_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { agencyID } = req.params;
                        const infoAgencyRemove = await AGENCY_MODEL.remove({ agencyID });
                        res.json(infoAgencyRemove);
                    }]
                },
            },

            /**
             * Function: Update agency
             * Date: 17/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.UPDATE_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate }   = req.user;
                        const { agencyID }          = req.params;
                        const { name, email, phone, address, code, tax_code, city, district, ward, latitude, longitude, openTime, closeTime, status, avatar, gallery, galleryDelete } = req.body;
                        
                        let dataUpdate = {
                            name, email, phone, address, code, tax_code, city, district, ward, latitude, longitude, openTime, closeTime, status, agencyID, userUpdate
                        }
                        // console.log({ avatar, gallery });
                        if(avatar){
                            dataUpdate.avatar = JSON.parse(avatar)
                        }
                        if(gallery){
                            dataUpdate.gallery = JSON.parse(gallery)
                        }
                        if(galleryDelete){
                            dataUpdate.galleryDelete = JSON.parse(galleryDelete)
                        }

                        const infoAgencyUpdate = await AGENCY_MODEL.update(dataUpdate);
                        res.json(infoAgencyUpdate);
                    }]
                },
            },

            /**
             * Function: Add onwner có sẵn vào agency
             * Date: 22/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.ADD_OWNER_EXISTED]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { email, agencyID } = req.body;
                        const infoAgencyUpdate = await AGENCY_MODEL.addOnwerExisted({ email, agencyID });
                        res.json(infoAgencyUpdate);
                    }]
                },
            },

            [CF_ROUTINGS_AGENCY.ADD_EMPLOYEE_EXISTED]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { email, agencyID } = req.body;

                        const infoAgencyUpdate = await AGENCY_MODEL.addEmployeeExisted({ email, agencyID });
                        res.json(infoAgencyUpdate);
                    }]
                },
            },

            /**
             * Function: Add onwner có sẵn vào agency
             * Date: 22/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.INFO_AGENCY_REVENUE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let { type }     = req.query;

                        /**
                         * 1: Set Doanh Số Hôm Nay    -> Hôm Qua
                         * 2: Set Doanh Số Hôm Qua    -> Hôm Kia
                         * 3: Set Doanh Số Tuần Này   -> Tuần Trước
                         * 4: Set Doanh Số Tháng Này  -> Tháng Trước
                         */
                        let dateNow = new Date();
                        if(!type){
                            return res.json({ error: true, message: "type_cannot_founded" });
                        }
                        if(!agencyID){
                            return res.json({ error: true, message: "agencyID_invalid" });
                        } 
  
                        const infoTransactionOfAgency = await AGENCY_MODEL.revenueAgency({ agencyID, date: dateNow, type });
                        
                        return res.json(infoTransactionOfAgency);
                    }]
                },
            },

            /**
             * Function: Lấy khoảng cách của một agency
             * Date: 16/09/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.DISTANCE_OF_AGENCY]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { lat, lng, agencyID } = req.body;
                        const infoDistanceAgency = await AGENCY_MODEL.getDistanceOfAgency({ lat, lng, agencyID });
                        res.json(infoDistanceAgency);
                    }]
                },
            },
        }

    }
};
