"use strict";
const Schema    	= require('mongoose').Schema;
const BASE_COLL  	= require('../../../databases/intalize/base-coll');

const GeoSchema = new Schema({
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
});

/**
 * COLLECTION AGENCY CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'agency' }){
    return BASE_COLL(name, {
        name: {
            type: String,
            trim: true,
            require: true,
        },
        email: {
            type: String,
            trim: true,
            unique : true,
            require: true
        },
        
        phone: String,

        address: String,

        /**
         * Mã đại lý
         */
        code: {
            type: String,
        },

        /**
         * Mã số thuế
         */
        tax_code: {
            type: String,
        },

        /**
         * Thành phốs
         */
        city: {
            type: String,
        },
        /**
         * Quận
         */
        district: {
            type: String,
        },
        /**
         * Phường
         */
        ward: {
            type: String,
        },

        //Vị trí
        location: GeoSchema,
    
        //Giờ mở cửa
        openTime: String,
    
        //Giờ đóng cửa
        closeTime: String,

		// Tỷ lệ cầm
		percentPawn: {
			type: Number,
			default: 85
		},
    
        /**
         * Đại lý tổng
         */
        parent: {
            type: Schema.Types.ObjectId,
            ref: 'agency'
        },
    
        childs: [{
            type: Schema.Types.ObjectId,
            ref: 'agency'
        }],
    
        // Mảng owner của agency
        owners: [{
            type: Schema.Types.ObjectId,
            ref: 'user'
        }],
        // Mảng nhân viên của agency
        employees: [{
            type: Schema.Types.ObjectId,
            ref: 'user'
        }],
        
        banner: String,
    
        //Ảnh đại diện
        avatar: {
            type: Schema.Types.ObjectId,
            ref: 'image'
        },
    
        //Bộ sưu tập
        gallery: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],
        /**
             * Số dư đầu kỳ
             */
        openingBalance:{
            type: Number,
            default: 0
        },
        /**
         * Quỹ
         */
        funds:{
            type: Number,
            default: 0
        },
        /**
         * Trạng thái hoạt động.
         * 1. Hoạt động
         * 0. Khóa
         */
        status: {
            type: Number,
            default: 1
        },
        userCreate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        userUpdate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
		...fields
    });
}
