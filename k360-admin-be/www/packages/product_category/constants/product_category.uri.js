const BASE_ROUTE = '/product-category';

const CF_ROUTINGS_PRODUCT_CATEGORY = {
    // PRODUCT CATEGORY
    ADD_PRODUCT_CATEGORY: `${BASE_ROUTE}/add-product-category`,
    UPDATE_PRODUCT_CATEGORY: `${BASE_ROUTE}/update-product-category`,
    DELETE_PRODUCT_CATEGORY: `${BASE_ROUTE}/delete-product-category`,
    LIST_PRODUCT_CATEGORY: `${BASE_ROUTE}/list-product-category`,
    INFO_PRODUCT_CATEGORY: `${BASE_ROUTE}/info-product-category`,
    LIST_CHILD_BY_PARENT: `${BASE_ROUTE}/list-child-by-parent`,

    //API MOBILE
    LIST_AGENCY_HAS_CATEGORIES: `${BASE_ROUTE}/list-agency-has-categories`,
    LIST_PRODUCT_CATEGORY_API: `/api${BASE_ROUTE}/list-product-category`,
    LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT_API: `/api${BASE_ROUTE}/list-child-by-parent`,

	CALCULATE_PRICED: `/api${BASE_ROUTE}/calculate-priced`,
	STEP_BY_CHOOSE: `/api${BASE_ROUTE}/step-by-choose`,
	IMPORT_EXCEL_PRODUCT_CATEGORY: `/api${BASE_ROUTE}/import-excel-product-category`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PRODUCT_CATEGORY = CF_ROUTINGS_PRODUCT_CATEGORY;
