"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID      = require('mongoose').Types.ObjectId;
const fs            = require('fs');
const XlsxPopulate  = require('xlsx-populate');

/**
 * INTERNAL PACKAGE
 */
const { isEmptyObj } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const PRODUCT_CATEGORY_COLL = require('../databases/product-category-coll')({});


class Model extends BaseModel {
    constructor() {
        super(PRODUCT_CATEGORY_COLL);
    }

	insert({ 
		name, content, description, price, image, type, percentValue, textField, listValuesDropdown, 
		status, textParent, brands = [], parent = null, specialCategory = 0, userCreate 
	}) {
        return new Promise(async resolve => {
            try {
                if(!name || !ObjectID.isValid(userCreate))
                    return resolve({ error: true, message: 'params_not_valid' });

				const dataInsert = {
					name, content, description, image, type,
					status, textParent, brands, specialCategory, userCreate
				}
				parent && (dataInsert.parent = parent);

				if(percentValue){
					dataInsert.percentValue = percentValue;
				}
				if(price){
					dataInsert.price = price;
					delete dataInsert.percentValue;
				}

				if(type == 2 && textField && !isEmptyObj(textField)){
					dataInsert.textField = textField;
				}
				if(type == 3 && listValuesDropdown && listValuesDropdown.length){
					dataInsert.dropDown = listValuesDropdown;
				}

                const infoAfterAdd = await this.insertData(dataInsert);
                if(!infoAfterAdd)
                    return resolve({ error: true, message: 'add_product_category_failed' });

                if(parent && ObjectID.isValid(parent)){
                    await PRODUCT_CATEGORY_COLL.updateOne({ _id: parent }, {
                        $addToSet: {
                            childs: infoAfterAdd._id
                        }
                    });
                }

                return resolve({ error: false, data: infoAfterAdd });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ 
		categoryID, name, content, description, price, image, type, percentValue, textField, listValuesDropdown, 
		status, textParent, brands, specialCategory, userUpdate
	}){
        return new Promise(async resolve => {
            try {
				// console.log({ 
				// 	categoryID, name, content, description, price, image, type, percentValue, textField, listValuesDropdown, 
				// 	status, textParent, brands, userUpdate
				// })
                if(!categoryID || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataUpdateCategory = {};
                name    		&& (dataUpdateCategory.name 		= name);
                image    		&& (dataUpdateCategory.image 		= image);
                status    		&& (dataUpdateCategory.status 		= status);
                type    		&& (dataUpdateCategory.type 		= type);
                userUpdate    	&& (dataUpdateCategory.userUpdate 	= userUpdate);

                dataUpdateCategory.textParent   	= textParent    || '';
                dataUpdateCategory.content      	= content       || '';
                dataUpdateCategory.description  	= description   || '';
                dataUpdateCategory.specialCategory  = specialCategory || 0;
                dataUpdateCategory.brands       	= (brands && brands.split(',')) || [];

				if(percentValue){
					dataUpdateCategory.percentValue = percentValue;
				}
				if(price){
					dataUpdateCategory.price = price;
					delete dataUpdateCategory.percentValue;
				}

				if(type == 2 && textField && !isEmptyObj(textField)){
					dataUpdateCategory.textField = textField;
					dataUpdateCategory.dropDown = [];
				}
				if(type == 3 && listValuesDropdown && listValuesDropdown.length){
					dataUpdateCategory.dropDown = listValuesDropdown;
					dataUpdateCategory.textField = 0;
				}

                const infoAfterUpdate = await PRODUCT_CATEGORY_COLL.updateOne({
                    _id: categoryID
                }, { $set: dataUpdateCategory });

				if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_product_category_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ categoryID }){
        return new Promise(async resolve => {
            try {
                if(!categoryID || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const infoProductCategory = await PRODUCT_CATEGORY_COLL.findById(categoryID).populate('image');

                if(!infoProductCategory)
                    return resolve({ error: true, message: 'get_info_product_category_failed' });

                return resolve({ error: false, data: infoProductCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	updateStatus({ categoryID }){
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_invalid' });

                const infoAfterUpdateStatus = await PRODUCT_CATEGORY_COLL.findByIdAndUpdate(categoryID, {
					$set: { status: 2 }
				});
				if(!infoAfterUpdateStatus)
					return resolve({ error: true, message: 'cannot_update_status_product_category' });
 
                return resolve({ error: false, data: infoAfterUpdateStatus });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ parent, status }){
        return new Promise(async resolve => {
            try {
                const conditionGetList = {};
                if(parent){
                    if(!ObjectID.isValid(parent)){
                        return resolve({ error: true, message: 'params_not_valid' });
                    }

                    conditionGetList.parent = parent;
                } else{
                    conditionGetList.parent = null;
                }

                conditionGetList.status = status || 1;

                let listChildsCategory = await PRODUCT_CATEGORY_COLL
                    .find(conditionGetList)
                    .populate('childs image')
					.sort({ createAt: -1 })
                    .lean();

                if(!listChildsCategory)
                    return resolve({ error: true, message: 'get_list_product_category_failed' });

				listChildsCategory = listChildsCategory.map(category => {
					if(category.childs && category.childs.length){
						category.childs = category.childs.filter(childCategory => childCategory.status !== 2);
					}

					return category;
				})

				return resolve({ error: false, data: listChildsCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListByParent({ parent, status = { $in: [0,1] } }){
        return new Promise(async resolve => {
            try {
                const conditionGetList = { status };

                if(parent){
                    if(!ObjectID.isValid(parent)){
                        return resolve({ error: true, message: 'params_not_valid' });
                    }

                    conditionGetList.parent = parent;
                } else{
                    conditionGetList.parent = null;
                }

                const listProductCategory = await PRODUCT_CATEGORY_COLL
                    .find(conditionGetList)
                    .populate({
                        path: 'childs image',
                        options: {
                             sort: { createAt: -1 }
                        }
                    })
					.sort({ createAt: -1 })
                    .lean();

                if(!listProductCategory)
                    return resolve({ error: true, message: 'get_list_product_category_failed' });

				if(parent){
					let breadcrumb = '';
					(async function recursiveParent(parent){
						if(parent){
							const infoParent = await PRODUCT_CATEGORY_COLL
								.findById(parent)
								.select('name parent')
								.lean();
							breadcrumb += ` / ${infoParent.name}`;
							recursiveParent(infoParent.parent);
						} else{
							breadcrumb = breadcrumb.split('/').reverse().join(' / ');
							return resolve({ error: false, listProductCategory, parentCategory: breadcrumb });
						}
					})(parent);
				} else{
					return resolve({ error: false, listProductCategory, parentCategory: null });
				}
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListChildByParent({ parentCategoryID }){
        return new Promise(async resolve => {
            try {
                if(!parentCategoryID || !ObjectID.isValid(parentCategoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const listChildsCategory = await PRODUCT_CATEGORY_COLL
                    .findOne({ _id: parentCategoryID, status: 1 })
                    .populate('childs image');

                if(!listChildsCategory)
                    return resolve({ error: true, message: 'get_list_product_category_failed' });

                return resolve({ error: false, data: listChildsCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListActiveByParent({ agencyID, parentCategoryID = null }){
        return new Promise(async resolve => {
            try {
                if(parentCategoryID && !ObjectID.isValid(parentCategoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                let listChildsCategory = await PRODUCT_CATEGORY_COLL
                    .find({ parent: parentCategoryID, status: 1 })
                    .populate('image')
                    .lean();

                let infoParent = await PRODUCT_CATEGORY_COLL
                    .findOne({ _id: parentCategoryID, status: 1 })
                    .populate('image')
                    .lean();

                if(!listChildsCategory)
                    return resolve({ error: true, message: 'cannot_get_list_product_category' });

                let listAgencyCategory = await AGENCY_PRODUCT_CATEGORY_MODEL.getList({ agencyID });
                let listProductCategoryWithAgencyCategory = [];

                listChildsCategory.map(category => {
                    let checkExistAndLength = listAgencyCategory.data && listAgencyCategory.data.length;
                    let agencyCategory      = checkExistAndLength && listAgencyCategory.data.find(agencyCategory => 
                        category._id.toString() === agencyCategory.product_category.toString()
                    );

                    listProductCategoryWithAgencyCategory = [...listProductCategoryWithAgencyCategory, {
                        ...category,
                        priceAgency: agencyCategory ? agencyCategory.price : 0,
                        statusAgency: agencyCategory ? agencyCategory.status : 1
                    }];
                })

                return resolve({ 
                    error: false, 
                    infoParent,
                    listChildsCategory: listProductCategoryWithAgencyCategory
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    recursiveProductCategory({ indexStart, workbook }){
        return new Promise(async resolve => {
            (async function recursive(indexStart, levelPrevious = 1, parents = [{ _id: null, level: 1 }]){
                let name          = workbook.sheet(0).cell(`B${indexStart}`).value();
                let title         = workbook.sheet(0).cell(`C${indexStart}`).value();
                let price         = workbook.sheet(0).cell(`D${indexStart}`).value();
                let percentValue  = workbook.sheet(0).cell(`E${indexStart}`).value();
                let type          = workbook.sheet(0).cell(`F${indexStart}`).value();
                let status        = workbook.sheet(0).cell(`G${indexStart}`).value();
                let description   = workbook.sheet(0).cell(`H${indexStart}`).value();
                let level         = workbook.sheet(0).cell(`I${indexStart}`).value();

                if(!name) return resolve();

                status = status === 'Hoạt động' ? 1 : 0;

                switch (type) {
                    case 'Button':
                        type = 1;
                        break;
                    case 'Checkbox':
                        type = 0;
                        break;
                    default:
                        type = 1;
                        break;
                }

                let isPushParent = false;
                if(levelPrevious > level){
                    const indexParent   = parents.findIndex(parent => parent.level === level - 1);
                    parents.length      = indexParent + 1;
                    isPushParent        = true;
                }

                let parentID = parents[parents.length - 1]._id;

                if(levelPrevious === level && level !== 1){
                    parentID = parents[parents.length - 2]._id;
                }

                // console.log({ indexStart, name, type, level, levelPrevious, parentID, parents });

                let infoProductCategory = await this.insertData({
                    name,
                    textParent: title,
                    description,
                    price,
                    percentValue,
                    type,
                    status,
                    parent: parentID
                })

                if(ObjectID.isValid(parentID)){
                    await PRODUCT_CATEGORY_COLL.updateOne({ _id: parentID }, {
                        $addToSet: {
                            childs: infoProductCategory._id
                        }
                    });
                }

                /**
                 * - 1
                 * - - 2
                 * - - - 3
                 * - - - - 4
                 * - - - - - 5
                 * - - - - - 5
                 * - - - - - 5
                 * - - - - 4
                 * - - - - - 5
                 * - - - - - - 6
                 * - - - - - - 6
                 * - - - - 4
                 * - - - 3
                 * - - - - 4
                 * - - 2
                 * - - - 3
                 */

                if(level === 1){
                    parents = [{ _id: infoProductCategory._id, level: 1 }];
                }

                if(levelPrevious < level || isPushParent){
                    parents = [...parents, { _id: infoProductCategory._id, level }];
                }

                if(levelPrevious === level && level !== 1){
                    parents[parents.length - 1] = { _id: infoProductCategory._id, level };
                }

                recursive.apply(this, [++indexStart, level, parents]);
            }).apply(this, [indexStart]);
        })
    }

    importExcelProductCategory({ fileInfo }){
        return new Promise(async resolve => {
            try {

               // Read the workbook.
                XlsxPopulate.fromFileAsync(fileInfo.path)
                    .then(async workbook => {

                        await this.recursiveProductCategory({ indexStart: 4, workbook });
                        fs.unlinkSync(fileInfo.path);

                        resolve({ error: false, message: 'upload_success' });
                    }).catch(err => resolve({ error: true, message: err.message }))

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }



}

exports.MODEL = new Model;
