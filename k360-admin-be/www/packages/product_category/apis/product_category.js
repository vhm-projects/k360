"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path 		= require('path');
const ObjectID  = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadSingle }                              = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_PRODUCT_CATEGORY } 				= require('../constants/product_category.uri');
const { provinces }            						= require('../../common/constant/provinces');
const { districts }            						= require('../../common/constant/districts');

/**
 * MODELS, COLLECTIONS
 */
const PRODUCT_CATEGORY_MODEL    	= require('../models/product_category').MODEL;
const { IMAGE_MODEL }           	= require('../../image');
const AGENCY_COLL             		= require('../../agency/databases/agency-coll')({});
const AGENCY_MODEL             		= require('../../agency/models/agency').MODEL;
const PRODUCT_CATEGORY_COLL         = require('../databases/product-category-coll')({});
const AGENCY_PRODUCT_CATEGORY_COLL  = require('../../agency_product_category/databases/agency-product-category-coll')({});
const AGENCY_PRODUCT_CATEGORY_MODEL	= require('../../agency_product_category/models/agency_product_category').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ PRODUCT CATEGORY ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Thêm product category (API)
             * Date: 14/04/2021
             * Dev: MinhVH
			 * Updated: MinhVH - 25/06/2021 - Add field
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.ADD_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function (req, res) {
                        const { _id: userCreate } = req.user;
                        const { 
							name, content, description, price, pathImage, type, percentValue, textField, listValuesDropdown, 
							status, title: textParent, brands, parent, specialCategory
						} = JSON.parse(req.body.data);

                        // console.log({
                        //     __file: req.file,
                        //     __dataParse: JSON.parse(req.body.data),
                        // });

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                name: req.fileName,
                                path: pathImage,
                                size,
                                userCreate
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        const infoAfterAdd = await PRODUCT_CATEGORY_MODEL.insert({
							name, content, description, price, image, type, percentValue, textField, listValuesDropdown, 
							status, textParent, brands, parent, specialCategory, userCreate
                        })
                        res.json(infoAfterAdd);
                    }]
                },
            },

            /**
             * Function: Cập nhật product category (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.UPDATE_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ uploadSingle, async function (req, res) {
                        const { _id: userUpdate } = req.user;
                        const { 
							categoryID, name, content, description, price, type, textField, listValuesDropdown,
							percentValue, status, title: textParent, pathImage, brands, specialCategory
						} = JSON.parse(req.body.data);

                        // console.log({
                        //     __file: req.file,
                        //     __dataParse: JSON.parse(req.body.data),
                        // });

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                name: req.fileName,
                                path: pathImage,
                                size,
                                userCreate: userUpdate
                            });
                            image = infoImageAfterInsert.data._id;
                        }

                        const infoAfterUpdate = await PRODUCT_CATEGORY_MODEL.update({
							categoryID, name, content, description, price, type, textField, listValuesDropdown,
							percentValue, status, textParent, brands, image, specialCategory, userUpdate
                        })
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Xóa tạm product category (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.DELETE_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryID } = req.query;

                        const infoAfterDelete = await PRODUCT_CATEGORY_MODEL.updateStatus({ categoryID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

            /**
             * Function: Chi tiết product category (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.INFO_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { categoryID } = req.query;

                        const infoProductCategory = await PRODUCT_CATEGORY_MODEL.getInfo({ categoryID });
                        res.json(infoProductCategory);
                    }]
                },
            },

            /**
             * Function: Danh sách product category (VIEW)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List Product Category - K360',
					code: CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY,
					inc: path.resolve(__dirname, '../views/list_product_category.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { parent, status, type } = req.query;
                        const listProductCategory = await PRODUCT_CATEGORY_MODEL.getListByParent({ parent, status });

                        if(type && type === 'API'){
                            return res.json(listProductCategory);
                        }

                        ChildRouter.renderToView(req, res, {
							...listProductCategory,
                            CF_ROUTINGS_PRODUCT_CATEGORY
						});
                    }]
                },
            },

            /**
             * Function: Danh sách child product category (API)
             * Date: 14/04/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_CHILD_BY_PARENT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Detail Product Category - K360',
					code: CF_ROUTINGS_PRODUCT_CATEGORY.LIST_CHILD_BY_PARENT,
					inc: path.resolve(__dirname, '../views/info_product_category.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { parentCategoryID, type } = req.query;
                        const listChildsCategory = await PRODUCT_CATEGORY_MODEL.getListChildByParent({ 
                            parentCategoryID 
                        });

                        if(type && type === 'api'){
                            return res.json(listChildsCategory);
                        }

                        ChildRouter.renderToView(req, res, {
							listChildsCategory: listChildsCategory.data,
                            CF_ROUTINGS_PRODUCT_CATEGORY
						});
                    }]
                },
            },

            /**
             * Function: LIST PRODUCT CATEGORY ACTIVE BY PARENT (API) MOBILE
             * SONLP
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_ACTIVE_PRODUCT_CATEGORY_BY_PARENT_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { parentCategoryID, type } = req.query;

                        let listProductCategoryWithAgencyCategory = await PRODUCT_CATEGORY_MODEL.getListChildByParent({ 
                            parentCategoryID, agencyID
                        });

                        if(type && type === 'API'){
                            return res.json({...listProductCategoryWithAgencyCategory});
                        }

                        // ChildRouter.renderToView(req, res, {
                        //     ...listProductCategoryWithAgencyCategory,
                        //     // infoParent,
                        // 	// listChildsCategory: listProductCategoryWithAgencyCategory,
                        //     CF_ROUTINGS_PRODUCT_CATEGORY
                        // });
                    }]
                },
            },

            /**
             * Function: LIST PRODUCT CATEGORY (API) MOBILE
             * SONLP
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_PRODUCT_CATEGORY_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { parent, status } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ parent, status });
                        res.json({...listProductCategory});
                    }]
                },
            },

            /**
             * Function: LIST AGENCY HAS ID CATEGORIES (API)
             * Date: 24/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_AGENCY_HAS_CATEGORIES]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res){
                        let { categories, lat, lng } = req.body;
                        if(!categories || !categories.length)
                            return res.json({ error: true, message: 'param_invalid' });

                        let listAgency = await AGENCY_COLL.find({ status: 1 }).lean();
                        if(!listAgency || !listAgency.length)
                            return res.json({ error: true, message: 'cannot_get_list_agency' });

                        let listCategories = categories.map(ID => ObjectID(ID));
                        let listAgencyCategory = await AGENCY_PRODUCT_CATEGORY_COLL.aggregate([
                            {
                                $match: {
                                    product_category: { $in: listCategories },
                                    status: 1
                                }
                            },
                            {
                                $group: {
                                    _id: { agency: '$agency' },
                                    agency_category: { $addToSet: "$_id" }
                                }
                            },
                            {
                                $addFields: {
                                    "agency": "$_id.agency",
                                }
                            },
                            { $unset: '_id' },
                            {
                                $lookup: {
                                    from: "agencies",
                                    localField: "agency",
                                    foreignField: "_id",
                                    as: "agency"
                                }
                            },
                            { $unwind: '$agency' },
                            { 
                                $project: {
                                    agency: { _id: 1, name: 1, email: 1, phone: 1, openTime: 1, address: 1, city: 1, district: 1 },
                                    agency_category: 1
                                }
                            }
                        ])

                        let listProvince        = Object.entries(provinces);
                        let listDistricts       = Object.entries(districts);

                        for (let agency of listAgencyCategory) {
                            let nameProvince;
                            let objAgency = {};
                            // 2// Lấy tên thành phố
                            if(agency.agency.city) {
                                for(let province of listProvince) {
                                    if(province[1].code == agency.agency.city) {
                                        nameProvince = province[1].name_with_type;
                                        break;
                                    }
                                }
                            }
                            agency.agency.city = nameProvince;
                            // 3// Lấy field trong của agency đó
                            // 4// Lấy quận/ Huyện
                            if(agency.agency.district) {
                                for(let district of listDistricts) {
                                    if(district[1].code == agency.agency.district ){
                                        agency.agency.district = district[1].name_with_type;
                                        break;
                                    }
                                }
                            }
                            const infoDistanceAgency = await AGENCY_MODEL.getDistanceOfAgency({ lat, lng, agencyID: agency.agency._id });
                            // console.log({
                            //     dist: infoDistanceAgency.data.dist,
                            //     infoDistanceAgency
                            // });
                            if (!infoDistanceAgency.error && infoDistanceAgency.data && infoDistanceAgency.data.dist) {
                                agency.agency = {
                                    ...agency.agency,
                                    dist: infoDistanceAgency.data.dist
                                }
                            } else {
                                agency.agency = {
                                    ...agency.agency,
                                    dist: null
                                }
                            }
                        }

                        let listAgencyHasCategory = [];

                        listAgencyCategory.map(agency => {
                            if(agency.agency_category.length === categories.length){
                                listAgencyHasCategory = [...listAgencyHasCategory, agency.agency];
                            }
                        })

                        res.json({ error: false, data: listAgencyHasCategory});
                    }]
                }
            },

			 /**
             * Function: STEP BY CHOOSE (API)
             * Date: 28/06/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PRODUCT_CATEGORY.STEP_BY_CHOOSE]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function(req, res){
                        let { category } = req.query;

						let { data: listProductCategory } = await PRODUCT_CATEGORY_MODEL.getList({ 
							parent: category, status: 1 
						});
                        let dataFind = {}
                        if (category) {
                            dataFind = {
                                _id: category
                            }
                        }
						let infoParent = await PRODUCT_CATEGORY_COLL
							.find({ ...dataFind })
							.populate({
                                path: 'childs',
                                match: {
                                    status: 1
                                }
                            })
							.lean();

						res.json({
                            error: false,
							listProductCategory,
							infoParent,
						});
                    }]
                }
            },

			/**
             * Function: CALCULATE PRICE (API)
             * Date: 28/06/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PRODUCT_CATEGORY.CALCULATE_PRICED]: {
				config: {
					auth: [ roles.role.customer.bin ],
					type: 'json',
				},
				methods: {
					post: [ async function(req, res){
						let { categories, agencyID } = req.body;

                        let calculatePrice = await AGENCY_PRODUCT_CATEGORY_MODEL.calculatePrice({ 
							agencyID, categories 
						});
						res.json(calculatePrice);
					}]
				}
			},

            /**
             * Function: IMPORT EXCEL PRODUCT CATEGORY (API)
             * Date: 16/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PRODUCT_CATEGORY.IMPORT_EXCEL_PRODUCT_CATEGORY]: {
				config: {
					auth: [ roles.role.admin.bin ],
					type: 'json',
				},
				methods: {
					post: [ uploadSingle, async function(req, res){
                        if(!req.file){
                            return res.json({ error: true, message: 'Upload file thất bại' });
                        }

                        const infoAfterImport = await PRODUCT_CATEGORY_MODEL.importExcelProductCategory({ fileInfo: req.file });
						res.json(infoAfterImport);
					}]
				}
			},

        }
    }
};
