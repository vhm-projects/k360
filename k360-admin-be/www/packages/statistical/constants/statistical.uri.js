const BASE_ROUTE = '/statistical';

const CF_ROUTINGS_STATISTICAL = {
    VIEW_STATISTICAL: `${BASE_ROUTE}`,

    TOP5_AGENCY_BEST_SALE: `${BASE_ROUTE}/top-5-agency-best-sale`,
    TOP5_AGENCY_HAVE_MOST_CUSTOMER: `${BASE_ROUTE}/top-5-agency-have-most-customer`,
    TOP_CUSTOMER: `${BASE_ROUTE}/top-customer`,
    TOP_PRODUCT_CATEGORY: `${BASE_ROUTE}/top-product-category`,
    CHART_PRODUCT_CATEGORY_BY_DAY: `${BASE_ROUTE}/chart-product-category-by-day`,
    CHART_PRODUCT_CATEGORY_BY_DAY_MOBILE: `${BASE_ROUTE}/chart-product-category-by-day-mobile`,
    CHART_AGENCY_BY_DAY: `${BASE_ROUTE}/chart-agency-by-day`,

	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_STATISTICAL = CF_ROUTINGS_STATISTICAL;