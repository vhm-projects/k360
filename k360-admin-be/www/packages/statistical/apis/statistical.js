"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_STATISTICAL }       = require('../constants/statistical.uri');

/**
 * MODELS
 */
 const { TRANSACTION_MODEL }            = require("../../transaction");
 const { PRODUCT_CATEGORY_MODEL }       = require("../../product_category");
 const { AGENCY_MODEL }                 = require("../../agency");
 
 

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            
            /**
             * Function: Danh sách thông báo (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.VIEW_STATISTICAL]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `View Statistical - K360`,
					code: CF_ROUTINGS_STATISTICAL.VIEW_STATISTICAL,
					inc: path.resolve(__dirname, '../views/manage_statistical.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listTop5AgencyBestSale = await TRANSACTION_MODEL.topAgencyHaveBestSale({ limitNumber: 5 });
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ status: 1 });
                        let listAgency = await AGENCY_MODEL.getList({ });
                        ChildRouter.renderToView(req, res, {
							listTop5AgencyBestSale: listTop5AgencyBestSale.data,
                            listProductCategory: listProductCategory.data,
                            listAgency: listAgency.data
						});
                    }]
                },
            },

            //========================= JSON ============================

            /**
             * Function: TOP 5 CỬA HÀNG CÓ DOANH SỐ CAO NHẤT
             * Date: 01/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.TOP5_AGENCY_BEST_SALE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type, start, end } = req.query;

                        let listTop5AgencyBestSale = await TRANSACTION_MODEL.topAgencyHaveBestSale({ limitNumber: 5, start, end });
                        if(type && type === "api") {
                            let description = { status: "Trạng thái hoạt động (1: Đang hoạt động; 0: Khóa)" };
                            return res.json({ description, ...listTop5AgencyBestSale });
                        }
                        
                        res.json(listTop5AgencyBestSale);
                    }]
                },
            },

            /**
             * Function: TOP 5 CỬA HÀNG CÓ KHÁCH HÀNG CAO NHẤT
             * Date: 01/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.TOP5_AGENCY_HAVE_MOST_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type, start, end } = req.query;

                        let listTopAgencyHaveMostCustomer = await TRANSACTION_MODEL.topAgencyHaveMostCustomer({ limitNumber: 5, start, end });
                        if(type && type === "api") {
                            let description = { status: "Trạng thái hoạt động (1: Đang hoạt động; 0: Khóa)" };
                            return res.json({ description, ...listTopAgencyHaveMostCustomer });
                        }
                        
                        res.json(listTopAgencyHaveMostCustomer);
                    }]
                },
            },

            /**
             * Function: TOP KHÁCH HÀNG CHI TIÊU NHIỀU NHẤT
             * Date: 03/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.TOP_CUSTOMER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type, start, end } = req.query;
                        
                        let listTopCustomer = await TRANSACTION_MODEL.topCustomer({ start, end });
                        if(type && type === "api") {
                            let description = {
                                gender: "Giới tính (1: Nam; 0: Nữ)",
                                auth_paper: "Loại giấy xác nhận (0: CMND; 1: Bằng lái xe; 2: Passport)",
                                status: "Trạng thái hoạt động (1: Hoạt động, 0: Khóa)"
                            };
                            return res.json({ description, ...listTopCustomer });
                        }
                        
                        res.json(listTopCustomer);
                    }]
                },
            },

            /**
             * Function: TOP DANH MỤC SẢN PHẨM ĐƯỢC GIAO DỊCH NHIỀU NHẤT
             * Date: 03/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.TOP_PRODUCT_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type, start, end } = req.query;

                        let listProductCategory = await TRANSACTION_MODEL.top5ProductCategoryMostSale({ limit: 5, start, end });
                        if(type && type === "api") {
                            let description = {
                                status: "Trạng thái danh mục (0: Không hiển thị; 1: Hiển thị)",
                                type: "Kiểu danh mục (0: Checkbox; 1: Button)"
                            }
                            return res.json({ description, ...listProductCategory });
                        }
                        
                        res.json(listProductCategory);
                    }]
                },
            },

            /**
             * Function: Chart thể hiển loại hàng kinh doanh biến động theo ngày
             * Date: 04/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.CHART_PRODUCT_CATEGORY_BY_DAY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { typeProductCategory, type, start, end } = req.query;
                        let listProductCategory = await TRANSACTION_MODEL.chartProductCategoryByDay({ typeProductCategory, start, end });
                        if(type && type === "api") {
                            let description = {
                                status: "Trạng thái danh mục (0: Không hiển thị; 1: Hiển thị)",
                                type: "Kiểu danh mục (0: Checkbox; 1: Button)"
                            }
                            return res.json({ description, ...listProductCategory });
                        }

                        res.json(listProductCategory);
                    }]
                },
            },

            /**
             * Function: Chart thể hiển loại hàng kinh doanh biến động theo ngày - mobile
             * Date: 04/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.CHART_PRODUCT_CATEGORY_BY_DAY_MOBILE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { type, month, start, end } = req.query;
                        let d = new Date();
                        let m = d.getMonth();
                        month = month ? Number(month) : Number(m);
                        let listProductCategory = await TRANSACTION_MODEL.chartProductCategoryByDayV3({ month, start, end });
                        if(type && type === "api") {
                            let description = {
                                status: "Trạng thái danh mục (0: Không hiển thị; 1: Hiển thị)",
                                type: "Kiểu danh mục (0: Checkbox; 1: Button)"
                            }
                            return res.json({ description, ...listProductCategory });
                        }

                        res.json(listProductCategory);
                    }]
                },
            },

            /**
             * Function: Chart thể hiển Đại Lý kinh doanh biến động theo ngày
             * Date: 05/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_STATISTICAL.CHART_AGENCY_BY_DAY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { month, type, agencyID, start, end } = req.query;
                        let d = new Date();
                        let m = d.getMonth();
                        month = month ? Number(month) : Number(m);
                        console.log({ month });
                        let listTransactionEveryDateInMonth = await TRANSACTION_MODEL.chartFollowAgencySaleDaily({month, start, end});
                        let listAgencyByDay = await TRANSACTION_MODEL.chartAgencyByDay({ agencyID });
                        if(type && type === "api") {
                            let description = { status: "Trạng thái hoạt động (1: Đang hoạt động; 0: Khóa)" };
                            return res.json({ description, ...listAgencyByDay, ...listTransactionEveryDateInMonth });
                        }
                        //console.log({ listAgencyByDay });
                        res.json(listTransactionEveryDateInMonth);
                    }]
                },
            },
        }
    }
};
