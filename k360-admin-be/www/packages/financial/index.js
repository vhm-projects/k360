const FINANCIAL_MODEL 		= require('./models/financial').MODEL;
const FINANCIAL_COLL  		= require('./database/financial-coll')({});
const FINANCIAL_ROUTES        = require('./apis/financial');

module.exports = {
    FINANCIAL_ROUTES,
    FINANCIAL_COLL,
    FINANCIAL_MODEL,
}
