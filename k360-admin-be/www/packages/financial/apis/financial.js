"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs = require('fs');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_FINANCIAL }                     = require('../constant/financial.uri');
const { TYPE_TRANSACTION, TYPE_TICKET, TYPE_FINANCIAL }             = require('../../../config/cf_constants');

/**
 * MODELS, COLLECTIONS
 */
const FINANCIAL_MODEL                             = require('../models/financial').MODEL;
const { AGENCY_MODEL }                            = require('../../agency');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    
    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================       API TRANSACTION    ================================
             * ========================== ************************ ================================
             */

            
            [CF_ROUTINGS_FINANCIAL.ADD_FINANCIAL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID        = req.agencyID;       
                                       
                        let { type, title, price, typeFinancial, note, status } = req.body;
                        // console.log({  type, title, price, typeFinancial, note, status});
                        let infoDataAgency = await AGENCY_MODEL.getInfo({ agencyID });

                        
                        let funds;
                        if(status == 1){
                            funds = infoDataAgency.data.funds
                        }else{
                            funds = infoDataAgency.data.fundsBank
                        }
                        if ( type == 1 ){
                            let infoAfterInsert = await FINANCIAL_MODEL.insert({ 
                                type, title, price, 
                                agency: agencyID,
                                funds,
                                email: infoDataAgency.data.email,
                                typeFinancial, 
                                note,
                                statusFinancial: status
                            });
                            if(infoAfterInsert.error == true) {
                                return res.json(infoAfterInsert);
                            }
                            res.json(infoAfterInsert);
                        }else{
                            if ( funds >= Number(price) ){
                                let infoAfterInsert = await FINANCIAL_MODEL.insert({ 
                                    type, title, price, 
                                    agency: agencyID,
                                    funds,
                                    email: infoDataAgency.data.email,
                                    typeFinancial,
                                    note,
                                    statusFinancial: status
                                });

                                if(infoAfterInsert.error == true) {
                                    return res.json(infoAfterInsert);
                                }
                                
                                res.json(infoAfterInsert);
                            }else{
                                res.json({ error: true, message: "fund_not_enough"});
                            }
                        }
                        
                    }]
                },
            },

            [CF_ROUTINGS_FINANCIAL.LIST_FINANCIAL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
					title: `List Financial - K360`,
					code: CF_ROUTINGS_FINANCIAL.LIST_FINANCIAL,
					inc: path.resolve(__dirname, '../views/list_financial.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID        = req.agencyID;
                        let { type, typeFinancial, fromDay, toDay } = req.query;
                        // console.log({ type, typeFinancial, fromDay, toDay }); 
                        let status = 1;
                        let listFinancial = await FINANCIAL_MODEL.getListFilter({ 
                            agencyID, type, typeFinancial, fromDay, toDay, status
                        });

                        ChildRouter.renderToView(req, res, {
							listFinancial: listFinancial.data,
                            TYPE_TRANSACTION,
                            type,
                            TYPE_TICKET,
                            typeFinancial,
                            TYPE_FINANCIAL,
                            fromDay,
                            toDay
						});
                    }]
                },
            },
            /**
             * API lấy danh sách quỹ Mobile 
             */
            [CF_ROUTINGS_FINANCIAL.LIST_FINANCIAL_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID        = req.agencyID;
                        let { type, typeFinancial, fromDay, toDay } = req.query;

                        // Lấy danh sách quỹ tiền mặt
                        let status = 1;
                        let listFinancial = await FINANCIAL_MODEL.getListFilter({ 
                            agencyID, type, typeFinancial, fromDay, toDay, status
                        });

                        if(listFinancial.error == true){
                            listFinancial = []
                        }
                        let description = {
                            type:            "Loại thu/chi   (1. Thu/ 2. Chi )",
                            typeFinancial:   "Loại TD/PS     (1. Tự động/ 2. Phát sinh )",
                            status:          "Loại Quỹ lưu   (1. Tiền mặt/ 2. Ngân hàng )",
                            typeTransaction: "Loại giao dịch (0. Phát sinh/ 1. Cầm đồ/ 2. Gia hạn bình thường/ 3. Gia hạn vay thêm/ 4. Gia hạn trả bớt/ 5. Báo mất/ 6. Chuộc đồ )",
                            transaction:     "Loại giao dịch (Tạo giao dịch(Cầm đồ)/ Tạo giao dịch bổ sung(Dựa vào giao dịch tạo))"
                        }

                        res.json({
                            description,
							listFinancial,
                            TYPE_TRANSACTION,
                            type,
                            TYPE_TICKET,
                            typeFinancial,
                            TYPE_FINANCIAL,
                            fromDay,
                            toDay,
						});
                    }]
                },
            },

             [CF_ROUTINGS_FINANCIAL.LIST_FINANCIAL_BANK]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'view',
					title: `List Financial - K360`,
					code: CF_ROUTINGS_FINANCIAL.LIST_FINANCIAL_BANK,
					inc: path.resolve(__dirname, '../views/list_financial_bank.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID        = req.agencyID;
                        let { type, typeFinancial, fromDay, toDay } = req.query;
                        // console.log({ type, typeFinancial, fromDay, toDay }); 

                        // Lấy danh sách quỹ tiền ngân hàng
                        let status = 2;
                        let listFinancial = await FINANCIAL_MODEL.getListFilter({ 
                            agencyID, type, typeFinancial, fromDay, toDay, status
                        });

                        ChildRouter.renderToView(req, res, {
							listFinancial: listFinancial.data,
                            TYPE_TRANSACTION,
                            type,
                            TYPE_TICKET,
                            typeFinancial,
                            TYPE_FINANCIAL,
                            fromDay,
                            toDay
						});
                    }]
                },
            },
            /**
             * API lấy danh sách quỹ Mobile 
             */
             [CF_ROUTINGS_FINANCIAL.LIST_FINANCIAL_BANK_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let agencyID        = req.agencyID;
                        let { type, typeFinancial, fromDay, toDay } = req.query;
                        // console.log({ type, typeFinancial, fromDay, toDay }); 

                        // Lấy danh sách quỹ tiền ngân hàng
                        let status = 2;
                        let listFinancial = await FINANCIAL_MODEL.getListFilter({ 
                            agencyID, type, typeFinancial, fromDay, toDay, status
                        });
                        if(listFinancial.error == true){
                            listFinancial = []
                        }
                        let description = {
                            type:            "Loại thu/chi   (1. Thu/ 2. Chi )",
                            typeFinancial:   "Loại TD/PS     (1. Tự động/ 2. Phát sinh )",
                            status:          "Loại Quỹ lưu   (1. Tiền mặt/ 2. Ngân hàng )",
                            typeTransaction: "Loại giao dịch (0. Phát sinh/ 1. Cầm đồ/ 2. Gia hạn bình thường/ 3. Gia hạn vay thêm/ 4. Gia hạn trả bớt/ 5. Báo mất/ 6. Chuộc đồ )",
                            transaction:     "Loại giao dịch (Tạo giao dịch(Cầm đồ)/ Tạo giao dịch bổ sung(Dựa vào giao dịch tạo))"
                        }
                        res.json({
                            description,
							listFinancial,
                            TYPE_TRANSACTION,
                            type,
                            TYPE_TICKET,
                            typeFinancial,
                            TYPE_FINANCIAL,
                            fromDay,
                            toDay
						});
                    }]
                },
            },
        }
    }
};
