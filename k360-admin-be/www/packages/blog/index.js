const BLOG_MODEL 			= require('./models/blog').MODEL;
const BLOG_COLL  			= require('./databases/blog-coll')({});
const BLOG_ROUTES       	= require('./apis/blog');

const CATEGORY_MODEL        = require('./models/blog').MODEL;
const CATEGORY_COLL         = require('./databases/category-coll')({});

module.exports = {
    BLOG_ROUTES,
    BLOG_COLL,
    BLOG_MODEL,
    CATEGORY_MODEL,
    CATEGORY_COLL
}
