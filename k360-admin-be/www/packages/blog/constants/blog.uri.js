const BASE_ROUTE_BLOG = '/blog';

const CF_ROUTINGS_BLOG = {
    // BLOG
    LIST_POST: `${BASE_ROUTE_BLOG}/list-post`,
    ADD_POST: `${BASE_ROUTE_BLOG}/add-post`,
    UPDATE_POST: `${BASE_ROUTE_BLOG}/update-post`,
    DELETE_POST: `${BASE_ROUTE_BLOG}/delete-post`,
    INFO_POST: `${BASE_ROUTE_BLOG}/info-post`,

	LIST_COMMENT_BY_POST: `${BASE_ROUTE_BLOG}/list-comment-by-post`,
	UPDATE_STATUS_COMMENT: `${BASE_ROUTE_BLOG}/update-status-comment`,

    // CATEGORY
    LIST_CATEGORY: `${BASE_ROUTE_BLOG}/list-category`,
    ADD_CATEGORY: `${BASE_ROUTE_BLOG}/add-category`,
    UPDATE_CATEGORY: `${BASE_ROUTE_BLOG}/update-category`,
    DELETE_CATEGORY: `${BASE_ROUTE_BLOG}/delete-category`,
    INFO_CATEGORY: `${BASE_ROUTE_BLOG}/info-category`,

    ORIGIN_APP: BASE_ROUTE_BLOG,
}

exports.CF_ROUTINGS_BLOG = CF_ROUTINGS_BLOG;
