"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadSingle }                              = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_BLOG } 				            = require('../constants/blog.uri');

/**
 * MODELS
 */
const BLOG_MODEL                = require('../models/blog').MODEL;
const CATEGORY_MODEL            = require('../models/category').MODEL;
const { IMAGE_MODEL }           = require('../../image');

/**
 * COLLECTIONS
 */
const CATEGORY_COLL             = require('../databases/category-coll')({});
const COMMENT_MODEL				= require('../models/comment').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ BLOG  ===============================
             * =============================== ************* ===============================
             */

             /**
             * Function: Tạo post (API, VIEW)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.ADD_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Add Post - K360',
					code: CF_ROUTINGS_BLOG.ADD_POST,
					inc: path.resolve(__dirname, '../views/blogs/add_blog.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function(req, res) {
                        const listCategories = await CATEGORY_COLL.find({ status: 1 }).lean();

						ChildRouter.renderToView(req, res, {
                            listCategories,
						});
					}],
					post: [ uploadSingle, async function(req, res){
						let { _id: userCreate } = req.user;
						let { 
							title, category: categoryID, slug, description, content, seo, status, pathImage 
						} = JSON.parse(req.body.data);

                        // console.log({ req, __bodyData: JSON.parse(req.body.data), __file: req.file, fileName: req.fileName });

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size,
                                name: req.fileName,
                                path: pathImage, // `/files/${req.fileName}`
                            });
                            image = infoImageAfterInsert.data._id;
                        }

						let dataAfterAddPost = await BLOG_MODEL.insert({
							title, categoryID, slug, description, content, image, seo, status, userCreate
						});
						res.json(dataAfterAddPost);
					}]
                },
            },

            /**
             * Function: Cập nhật post (API, VIEW)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.UPDATE_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Update Post - K360',
					code: CF_ROUTINGS_BLOG.UPDATE_POST,
					inc: path.resolve(__dirname, '../views/blogs/update_blog.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function(req, res) {
                        let { postID } = req.query;
						let listCategories  = await CATEGORY_COLL.find({ status: 1 }).lean();
                        let infoPost        = await BLOG_MODEL.getInfo({ postID });

                        ChildRouter.renderToView(req, res, {
                            infoPost: infoPost.data,
                            listCategories
                        });
					}],
					post: [ uploadSingle, async function(req, res){
						let { _id: userUpdate } = req.user;
						let { 
							postID, title, category: categoryID, slug, description, content, seo, status, pathImage 
						} = JSON.parse(req.body.data);

                        let image = null;
                        if(req.file){
                            let { size } = req.file;
                            let infoImageAfterInsert = await IMAGE_MODEL.insert({ 
                                size, 
                                name: req.fileName, 
                                path: pathImage, // `/files/${req.fileName}`
                            });
                            image = infoImageAfterInsert.data._id;
                        }

						let dataAfterAddPost = await BLOG_MODEL.update({
							postID, title, categoryID, slug, description, content, image, seo, status, userUpdate
						});
						res.json(dataAfterAddPost);
					}]
                },
            },

            /**
             * Function: Xóa post (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.DELETE_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { postID } = req.query;

						let dataAfterRemovePost = await BLOG_MODEL.delete({ postID });
						res.json(dataAfterRemovePost);
					}]
                },
            },

            /**
             * Function: Chi tiết post (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.INFO_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { postID } = req.query;

						let infoPost = await BLOG_MODEL.getInfo({ postID });
						res.json(infoPost);
					}]
                },
            },

            /**
             * Function: LIST POST (API, VIEW)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.LIST_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List Posts - K360',
					code: CF_ROUTINGS_BLOG.LIST_POST,
					inc: path.resolve(__dirname, '../views/blogs/list_blog.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { keyword, slug, type } = req.query;
						let listPosts = await BLOG_MODEL.getList({ keyword, slug });

						if(type === 'API'){
							return res.json({
								listPosts,
							});
						}

						ChildRouter.renderToView(req, res, {
							listPosts: listPosts.data,
						});
                    }]
                },
            },


			/**
             * Function: LIST COMMENT BY POST (VIEW)
             * Date: 13/07/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.LIST_COMMENT_BY_POST]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List Comments - K360',
					code: CF_ROUTINGS_BLOG.LIST_COMMENT_BY_POST,
					inc: path.resolve(__dirname, '../views/comments/list_comment.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { postID } = req.query;
						const listComment = await COMMENT_MODEL.getListByBlog({ postID });

						ChildRouter.renderToView(req, res, {
							listComment: listComment.data,
						});
                    }]
                },
            },

			/**
             * Function: UPDATE STATUS COMMENT (API)
             * Date: 13/07/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_BLOG.UPDATE_STATUS_COMMENT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json'
                },
                methods: {
                    post: [ async function (req, res) {
						const { listCommentID, status } = req.body;
						console.log({ __body: req.body });

						const infoAfterUpdate = await COMMENT_MODEL.updateStatus({ listCommentID, status });
						res.json(infoAfterUpdate);
                    }]
                },
            },


             /**
             * =============================== **************** ===============================
             * =============================== QUẢN LÝ CATEGORY ================================
             * =============================== **************** ===============================
             */

             /**
             * Function: Tạo danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.ADD_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { _id: userCreate } = req.user;
						let { name, slug, description, status } = req.body;

						let dataAfterAddCategory = await CATEGORY_MODEL.insert({
							name, slug, description, status, userCreate
						});
						res.json(dataAfterAddCategory);
					}]
                },
            },

             /**
             * Function: Cập nhật danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.UPDATE_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { _id: userUpdate } = req.user;
						let { categoryID, name, slug, description, status } = req.body;

						let dataAfterUpdateCategory = await CATEGORY_MODEL.update({
							categoryID, name, slug, description, status, userUpdate
						});
						res.json(dataAfterUpdateCategory);
					}]
                },
            },

             /**
             * Function: Xóa danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.DELETE_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { categoryID } = req.query;

						let dataAfterRemoveCategory = await CATEGORY_MODEL.delete({ categoryID });
						res.json(dataAfterRemoveCategory);
					}]
                },
            },

             /**
             * Function: Chi tiết danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.INFO_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { categoryID } = req.query;

						let infoCategory = await CATEGORY_MODEL.getInfo({ categoryID });
						res.json(infoCategory);
					}]
                },
            },

             /**
             * Function: Danh sách danh mục (VIEW, API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.LIST_CATEGORY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'List Categories - K360',
					code: CF_ROUTINGS_BLOG.LIST_CATEGORY,
					inc: path.resolve(__dirname, '../views/categories/list_category.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						let { keyword, type } = req.query;
						let listCategories = await CATEGORY_MODEL.getList({ keyword });

						if(type === 'api'){
							return res.json({
								listCategories,
							});
						}

						ChildRouter.renderToView(req, res, { 
							listCategories: listCategories.data,
						});
					}]
                },
            },


        }
    }
};
