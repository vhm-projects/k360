"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { convertToSlug } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const BLOG_COLL = require('../databases/blog-coll')({});


class Model extends BaseModel {
    constructor() {
        super(BLOG_COLL);
    }

	insert({ title, slug, content, description, image, seo, status, categoryID = 0, userCreate }) {
        return new Promise(async resolve => {
            try {
				if((categoryID !== 0 && !ObjectID.isValid(categoryID)) || !ObjectID.isValid(userCreate))
                    return resolve({ error: true, message: 'param_not_valid' });

                if(!title || !slug || !content || !status)
                    return resolve({ error: true, message: 'title_slug_content_is_required' });

				slug = convertToSlug(slug);
                const checkExists = await BLOG_COLL.findOne({
					$or: [
						{ title },
						{ slug }
					]
				});
                if(checkExists)
                    return resolve({ error: true, message: 'title_or_slug_existed' });

                const dataInsert = {
                    title,
                    slug,
					content,
					image,
					description,
                    status,
					seo: seo.split(','),
					userCreate
                }

				categoryID !== 0 && (dataInsert.category = categoryID);

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_post_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoPost = await BLOG_COLL.findById(postID)
                                                .populate('category image');
                if(!infoPost)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ postID, title, categoryID, slug, description, content, image, seo, status, userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userUpdate) || 
					!ObjectID.isValid(postID) || 
					!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                if(!title || !slug || !content || !status)
                    return resolve({ error: true, message: 'title_slug_status_content_is_required' });

                const checkExists = await BLOG_COLL.findById(postID);
                if(!checkExists)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                const dataUpdatePost = {
                    title,
                    content,
                    slug: convertToSlug(slug),
                    seo: seo.split(','),
                    category: categoryID,
                    status,
                    userUpdate
                };
                description && (dataUpdatePost.description 	= description);
                image    	&& (dataUpdatePost.image 		= image);

                await this.updateWhereClause({ _id: postID }, {
                    ...dataUpdatePost
                });

                return resolve({ error: false, data: dataUpdatePost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await BLOG_COLL.findByIdAndRemove(postID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword, slug }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { title: new RegExp(key, 'i') },
                        { slug: new RegExp(key, 'i') },
                        { description: new RegExp(key, 'i') },
                        { content: new RegExp(key, 'i') },
                    ]
                }
                slug && (condition.slug = slug);

                const listPosts = await BLOG_COLL
					.find(condition)
					.populate('category image')
					.sort({ modifyAt: -1 })
					.lean();

                if(!listPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getRelatedPost({ postID, categoryID }){
        return new Promise(async resolve => {
            try {
                const listRelatedPosts = await BLOG_COLL.find({
                    $and: [
                        { _id: { $nin: [postID] } },
                        { category: categoryID }
                    ]
                }).populate('category image').limit(3).lean();

                if(!listRelatedPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listRelatedPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListMostViewed(){
		return new Promise(async resolve => {
			try {
				const listMostViewedPost = await BLOG_COLL.find({})
					.populate('image')
					.sort({ views: -1 })
					.limit(5)
                    .lean();

				if(!listMostViewedPost)
                    return resolve({ error: true, message: 'not_found_posts_list' });

				return resolve({ error: false, data: listMostViewedPost });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
