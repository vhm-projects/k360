"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID 				= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { checkObjectIDs } 	= require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel 			= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const COMMENT_COLL 			= require('../databases/comment-coll')({});


class Model extends BaseModel {
    constructor() {
        super(COMMENT_COLL);
    }

	updateStatus({ listCommentID, status }) {
        return new Promise(async resolve => {
            try {
				console.log({ listCommentID, status });
				if(!checkObjectIDs(listCommentID) || ![0,1].includes(+status))
                    return resolve({ error: true, message: 'params_invalid' });

                const infoAfterUpdate = await COMMENT_COLL.findOneAndUpdate({
					_id: { $in: listCommentID }
				}, {
					$set: { status }
				})

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_status_comment_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByBlog({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'params_invalid' });

                const listCommentByPost = await COMMENT_COLL.find({ post: postID }).lean();
                if(!listCommentByPost)
                    return resolve({ error: true, message: 'get_list_comment_failed' });

                return resolve({ error: false, data: listCommentByPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
