"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION BLOG CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'blog' }){
	return BASE_COLL(name, {
        // Tiêu đề bài viết
		title: {
			type: String,
			unique: true,
			require: true
		},

		// Danh mục
		category: {
			type:  Schema.Types.ObjectId,
			ref : 'category',
			default: null
		},

		slug: {
			type: String, 
			unique: true,
			require: true
		},

		// Mô tả ngắn
		description: {
			type: String,
			default: ''
		},

		// Nội dung
		content: {
			type: String,
		},

        // Liên Kết
        link: {
            type: String
        },

		/**
		 * 0: Không hoạt động
		 * 1: Hoạt động
		 */
		status: {
			type: Number,
			default: 1
		},

		// Hình ảnh bài viết
		image: {
			type:  Schema.Types.ObjectId,
			ref : 'image'
		},

		// Chứa những từ khóa seo
		seo: [
			{ type: String }
		],

		// Comment bài viết
		comments: [{
			type:  Schema.Types.ObjectId,
			ref : 'comment'
		}],

		/**
		 * 0: Bài viết thường
		 * 1: Sản phẩm dịch vụ
         * 2: Năng lực sản xuất
		 */
		type: {
			type: Number,
			default: 0
		},

		// Lượt xem
		views: {
			type: Number,
			default: 0
		},
		//_________Người tạo
		userCreate: {
			type:  Schema.Types.ObjectId,
			ref : 'user'
		},
		//_________Người cập nhật
		userUpdate: {
			type:  Schema.Types.ObjectId,
			ref : 'user'
		},
		...fields
    });
}
