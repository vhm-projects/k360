"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION GROUP CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'category' }){
	return BASE_COLL(name, {
       // Tên danh mục
       name: {
            type: String,
            unique: true,
            require: true
        }, 
        slug: {
            type: String,
            unique: true,
            require: true
        },
        description: {
            type: String,
            default: ''
        },
        /**
         * 0: Không hoạt động
         * 1: Hoạt động
         * 2: Tạm xóa
         */
        status: {
            type: Number,
            default: 1
        },
        //_________Người tạo
        userCreate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
        //_________Người cập nhật
        userUpdate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
		...fields
    });
}
