const BASE_ROUTE_ADMIN = '/transaction';
const BASE_ROUTE_API   = '/api/transaction';

const CF_ROUTINGS_TRANSACTION = {
    // ACCOUNT KHÁCH HÀNG
    VIEW_LIST_TRANSACTION: `${BASE_ROUTE_ADMIN}s`,
    VIEW_LIST_TRANSACTION_24H: `${BASE_ROUTE_ADMIN}/list-transaction-24h`,
    ADD_TRANSACTION: `${BASE_ROUTE_ADMIN}/add-transaction`,
    UPDATE_STATUS_TRANSACTION: `${BASE_ROUTE_ADMIN}/update-status-transaction/:transactionID`,

    ADD_INJECT_TRANSACTION: `${BASE_ROUTE_ADMIN}/add-inject-transaction/:transactionID`,
    UPDATE_STATUS_INJECT_TRANSACTION: `${BASE_ROUTE_ADMIN}/update-status-inject-transaction/:transactionID`,

    GET_INFO_TRANSACTION: `${BASE_ROUTE_ADMIN}/get-info/:transactionID`,
    GET_LIST_TRANSACTION_24H: `${BASE_ROUTE_ADMIN}/get-list-transaction-24h/:agencyID`,
    FILTER_TRANSACTION: `${BASE_ROUTE_ADMIN}/filter-transaction`,
    ORIGIN_APP: BASE_ROUTE_ADMIN,

    VIEW_LIST_TRANSACTION_MOBILE: `${BASE_ROUTE_API}s`,
    LIST_TRANSACTION_CUSTOMER: `${BASE_ROUTE_API}/list-transaction-customer`,
    INFO_TRANSACTION_API: `${BASE_ROUTE_API}/info-transaction/:transactionID`,
    // INFO_TRANSACTION_API: `${BASE_ROUTE_API}/list-tr/:transactionID`,
    EXPORT_EXCEL_TRANSACTION: `${BASE_ROUTE_ADMIN}/export-excel-transaction`,

}

exports.CF_ROUTINGS_TRANSACTION = CF_ROUTINGS_TRANSACTION;
