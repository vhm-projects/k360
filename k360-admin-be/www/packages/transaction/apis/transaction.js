"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path         = require('path');
 const fs           = require('fs');
 const moment       = require('moment');
 const XlsxPopulate = require('xlsx-populate');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadCusAndAuthPaper }                     = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_TRANSACTION }                   = require('../constant/transaction.uri');
const { KIND_OF_AUTH_PAPER, TYPE_GENDER, TYPE_TRANSACTION, STATUS_TRANSACTION }           = require('../../../config/cf_constants');
const { subStractDateGetDay, addNumberToSetDate, minusNumberToSetDate }                       = require('../../../config/cf_time');

/**
 * MODELS, COLLECTIONS
 */
 const { IMAGE_MODEL }                               = require('../../image');
 const TRANSACTION_MODEL                             = require('../models/transaction').MODEL;
 //const INJECT_TRANSACTION_MODEL                      = require('../models/inject_transaction').MODEL;
 const PRODUCT_MODEL                                 = require('../models/product').MODEL;
 const AGENCY_MODEL                                  = require('../../agency/models/agency').MODEL;
 const TRANSACTION_COLL                              = require('../database/transaction-coll')({});
 const PRODUCT_CATEGORY_MODEL                        = require('../../product_category/models/product_category').MODEL;
 const { INTEREST_RATE_MODEL }                       = require('../../interest_rate');

 const { provinces }                                 = require('../../common/constant/provinces');
 const { districts }                                 = require('../../common/constant/districts');
 const { isValidDate }                               = require('../../../utils/time_utils');
 const { checkObjectIDs }                            = require('../../../utils/utils');
module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    
    registerRouting() { 
        return {
            /**
             * ========================== ************************ ================================
             * ==========================       API TRANSACTION    ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Danh sách transaction có bộ lọc (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `List Transaction - K360`,
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION,
					inc: path.resolve(__dirname, '../views/manage_transaction.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID, type, start, end, name, city, district, ward, sort } = req.query;

                        //Danh sách danh mục sản phẩm
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ status: 1 });

                        //Danh sách agency
                        let listAgency = await TRANSACTION_MODEL.getListAgency();

                        //Danh sách transaction sau khi lọc
                        // let listTransactionWithTypeAndAgency = await TRANSACTION_MODEL.getListTransactionOfAgencyWithType({ agencyID, typeProductCategoryID: type, start, end, name, city, district, ward, sort });

                        /**
                         * DANH SÁCH GIAO DỊCH
                         */
                        let listTransaction = await TRANSACTION_MODEL.getListTransactionRevene({ agencyID, typeProductCategoryID: type, start, end, name, city, district, ward, status: sort });
                        //Danh sách tỉnh thành  
                        let listProvince = Object.entries(provinces);

                        //let top5ProductMostSale = await TRANSACTION_MODEL.top5ProductMostSale();

                        ChildRouter.renderToView(req, res, {
							listProductCategory: listProductCategory.data,
                            listAgency: listAgency.data,
                            listTransactionWithTypeAndAgency: listTransaction.data.listTransaction,
                            agencyID, type, start, end,
                            listProvince,
                            name, city, district, ward, sort, STATUS_TRANSACTION,
                            calculPriceBorrow: listTransaction.data.calculPriceBorrow,
                            calculPricePay:    listTransaction.data.calculPricePay,
                            calculPriceNormal: listTransaction.data.calculPriceNormal,
                            calculPriceRansom: listTransaction.data.calculPriceRansom,
                            calculPricePawn:   listTransaction.data.calculPricePawn,
                            totalInterestAllTransaction: listTransaction.data.totalInterestAllTransaction
						});
                    }]
                },
            },

            /**
             * Function: Danh sách transaction có bộ lọc (json)
             * API Mobile
             * Dev: VyPQ
             */
            [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION_MOBILE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID, type, start, end, name, city, district, ward, sort } = req.query;
                        
                        let listTransactionWithTypeAndAgency = await TRANSACTION_MODEL.getListTransactionOfAgencyWithType({ agencyID, typeProductCategoryID: type, start, end, name, city, district, ward, sort });
                       
                        let description = {
                            status       : "Trạng thái giao dịch (0: Đang xử lý - chờ duyệt; 1: Đang cầm - Đã duyệt; 2: Không duyệt)",
                            typeInventory: "Trạng thái kiểm kê (0: Chưa kiểm kê; 1: Đã kiểm kê)"
                        }

                        res.json({ description, ...listTransactionWithTypeAndAgency });
                    }]
                },
            },

            /**
             * Function: Danh sách giao dịch 24h (của admin) (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION_24H]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `List Transaction - K360`,
					code: CF_ROUTINGS_TRANSACTION.VIEW_LIST_TRANSACTION_24H,
					inc: path.resolve(__dirname, '../views/transaction_24h.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID, city, district, ward, sort } = req.query;
                        // let listTransaction24hOfAngency = await TRANSACTION_MODEL.getListTransactionNear24h({ agencyID });
                        let _fromDate   = moment(new Date()).startOf('day').format();
                        let _toDate     = moment(new Date()).endOf('day').format();

                        let listTransaction = await TRANSACTION_MODEL.getListTransactionRevene({ agencyID, start: _fromDate, end: _toDate, city, district, ward, status: sort });
                        
                        let listAgency = await TRANSACTION_MODEL.getListAgency();

                        ChildRouter.renderToView(req, res, {
                            listAgency: listAgency.data,
                            listTransaction24hOfAngency: listTransaction.data.listTransaction,
                            agencyID,
                            city, district, ward, sort,
                            calculPriceBorrow: listTransaction.data.calculPriceBorrow,
                            calculPricePay:    listTransaction.data.calculPricePay,
                            calculPriceNormal: listTransaction.data.calculPriceNormal,
                            calculPriceRansom: listTransaction.data.calculPriceRansom,
                            calculPricePawn:   listTransaction.data.calculPricePawn,
                            totalInterestAllTransaction: listTransaction.data.totalInterestAllTransaction
						});
                    }]
                },
            },

            /**
             * Function: Lấy danh sách giao dịch 24h của một đại lý
             * Dev: Dattv
             */
             [CF_ROUTINGS_TRANSACTION.GET_LIST_TRANSACTION_24H]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let listTransaction24hOfAngency = await TRANSACTION_MODEL.getListTransactionNear24h({ agencyID });
                        res.json(listTransaction24hOfAngency)
                    }]
                },
            },


            /**
             * Function: Thông tin giao dịch
             * Dev: Dattv
             */
            [CF_ROUTINGS_TRANSACTION.GET_INFO_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { transactionID } = req.params;
                        let data = await TRANSACTION_MODEL.getInfo({ transactionID });
                        res.json(data)
                    }]
                },
            },

             /**
             * Function: Bộ lọc giao dịch
             * Dev: Dattv
             */
              [CF_ROUTINGS_TRANSACTION.FILTER_TRANSACTION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        let { agencyID, typeProductCategoryID, start, end } = req.body;
                        let listTransactionWithTypeAndAgency = await TRANSACTION_MODEL.getListTransactionOfAgencyWithType({ agencyID, typeProductCategoryID, start, end });
                        res.json(listTransactionWithTypeAndAgency);
                    }]
                },
            },

             /**
             * Function: Thông tin giao dịch trong 24h của đại lý
             * Dev: Dattv
             */
            [CF_ROUTINGS_TRANSACTION.GET_LIST_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let data = await TRANSACTION_MODEL.getListTransactionNear24h({ agencyID });
                        res.json(data);
                    }]
                },
            },

            [CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_CUSTOMER]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: customerID } = req.user;
                        let { type, status, date, page, typeTransaction, typeStatusTransaction, fromDay, toDay } = req.query;

                        let listTransactionChange30Day;
                        let loanAmoutAfterInterest30Day = 0;

                        // Tính lãi 30 ngày trong tháng
                        let dayFirst = new Date();
                        let day      = dayFirst.getDate();
                        let dayLast  = new Date();
                        let DAY_OF_MONTH = 30;
                        if(date) {
                            day      = date.getDate();
                            minusNumberToSetDate({date1: dayFirst, numberToSet: day - 1});
                            addNumberToSetDate({date1: dayLast, numberToSet: DAY_OF_MONTH - day});
                        }else{
                            minusNumberToSetDate({date1: dayFirst, numberToSet: day - 1});
                            addNumberToSetDate({date1: dayLast, numberToSet: DAY_OF_MONTH - day});
                        }
                        let fromDateStartDay = moment(dayFirst).startOf('day');
                        let toDateStartDay   = moment(dayLast).endOf('day');
                        // listTransactionChange30Day = await TRANSACTION_MODEL.getListByCustomer({ customerID, dayFirst: fromDateStartDay, dayLast: toDateStartDay, status, typeTransaction, fromDay, toDay  });
                        listTransactionChange30Day = await TRANSACTION_MODEL.getListByCustomer({ customerID, status: 1, typeTransaction, fromDay, toDay  });
                        
                        // Lấy tổng tiền trong tháng
                        if(listTransactionChange30Day.error == false && listTransactionChange30Day.data.length) {
                            for (let transaction of listTransactionChange30Day.data) {
                                // let interestStartDate = transaction.interestStartDate;

                                // let dateNow   =  new Date();
                                // let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                                // limitDate     = Math.floor(limitDate);
                                // if(limitDate >= 0) {
                                //     limitDate     = limitDate + 1;
                                // }
                                // if(limitDate < 0) {
                                //     limitDate = 0;
                                // }
                                const DATE_OF_PERIOD = 30;
                                //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                                // if (limitDate > 0 ){
                                let TYPE_PRODUCT = transaction.products[0].type._id;
                                let agencyID     = transaction.agency;
                                let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID: agencyID, limitDate: DATE_OF_PERIOD, price: transaction.loanAmount, typeProduct: TYPE_PRODUCT});
                                checkDateToInterest.data.forEach( price => {
                                    loanAmoutAfterInterest30Day += price;
                                })
                                // }
                            }
                        }

                        let listTransactionCustomer = await TRANSACTION_MODEL.getListByCustomer({ customerID, status, typeTransaction, fromDay, toDay });
                        let totalPriceToPay        = 0;
                        let loanAmoutAfterInterest = 0;
                        let listTransaction;
                        // Lấy tất cả giao dịch, chỉ cộng tiền những giao dịch còn giao dịch
                        if(listTransactionCustomer.error == false && listTransactionCustomer.data.length) {
                            let dataAfterInsertInterest = [];
                            for (let transaction of listTransactionCustomer.data) {
                                if(transaction.status == 1) {
                                    totalPriceToPay       += transaction.loanAmount;
                                    let interestStartDate = transaction.interestStartDate;
    
                                    let dateNow   =  new Date();
                                    let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                                    limitDate     = Math.floor(limitDate);
                                    if(limitDate >= 0) {
                                        limitDate     = limitDate + 1;
                                    }
                                    if(limitDate < 0) {
                                        limitDate = 0;
                                    }
                                    let agencyID = transaction.agency._id;
                                    let nameAgency            = "";
                                    /**
                                     * Lấy thông tin agency 
                                     */
                                    let infoAgency = await AGENCY_MODEL.getInfoFieldNeed({ agencyID });
                                    if(infoAgency.error) {
                                        return res.json(infoAgency);
                                    }
                                    if(transaction.latestInjectTransaction) {
                                        TYPE_TRANSACTION.forEach( type => {
                                            let typeOfInjectTransaction = transaction.latestInjectTransaction.type;
                                            if(type.value == typeOfInjectTransaction) {
                                                nameAgency += type.text;
                                            }
                                        });
                                    }else{
                                        nameAgency += "Cầm đồ";
                                    }
                                    nameAgency += " từ " + infoAgency.data.name;

                                    let obj = {
                                        transactionDate:     transaction.transactionDate,
                                        expectedDate:        transaction.expectedDate,
                                        loanAmount:          transaction.loanAmount,
                                        approvalLimit:       transaction.approvalLimit,
                                        interestStartDate:   transaction.interestStartDate,
                                        status:              transaction.status,
                                        injectTransaction:   transaction.injectTransaction,
                                        products:            transaction.products,
                                        typeInventory:       transaction.typeInventory,
                                        _id:                 transaction._id,
                                        expireTime:          transaction.expireTime,
                                        customer:            transaction.customer,
                                        transactionEmployee: transaction.transactionEmployee,
                                        code:                transaction.code,
                                        createAt:            transaction.createAt,
                                        nameAgency
                                    }
                                    
                                    let interestOfTransaction = 0;
                                    // let infoInterest;
                                    // let percent = 0;
                                    //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                                    if (limitDate > 0 ){
                                        let TYPE_PRODUCT = transaction.products[0].type._id;
                                        let agencyID     = transaction.agency;
                                        let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID: agencyID, limitDate, price: transaction.loanAmount, typeProduct: TYPE_PRODUCT});
                                        checkDateToInterest.data.forEach( price => {
                                            loanAmoutAfterInterest += price;
                                            interestOfTransaction  += price;
                                        })
                                        obj.interestOfTransaction = interestOfTransaction;
                                        obj.percent               = checkDateToInterest.percent;
                                    }
                                    if(transaction.latestInjectTransaction) {
                                        obj = {
                                            ...obj,
                                            latestInjectTransaction: transaction.latestInjectTransaction,
                                            ransom: transaction.latestInjectTransaction.ransom, 
                                            type: transaction.latestInjectTransaction.type,
                                            nameAgency
                                        }
                                    }else{
                                        obj = {
                                            ...obj,
                                            latestInjectTransaction: {},
                                            ransom: 0, 
                                            type: 1
                                        }
                                    }
                                    dataAfterInsertInterest.push(
                                        obj
                                    );
                                }
                            }
                            listTransactionCustomer = { error: false, data: dataAfterInsertInterest };
                        }
                        // Set List 30 Ngày
                        if(type && type === "30day") {
                            let dataAfterInsertInterest = [];
                            for (let transaction of listTransactionChange30Day.data) {
                                // let interestStartDate = transaction.interestStartDate;

                                // let dateNow   =  new Date();
                                // let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                                // limitDate     = Math.floor(limitDate);
                                // if(limitDate >= 0) {
                                //     limitDate     = limitDate + 1;
                                // }
                                // if(limitDate < 0) {
                                //     limitDate = 0;
                                // }
                                // let infoInterest;
                                // let percent = 0;
                                let agencyID = transaction.agency._id;
                                let nameAgency            = "";
                                /**
                                 * Lấy thông tin agency 
                                 */
                                let infoAgency = await AGENCY_MODEL.getInfoFieldNeed({ agencyID });
                                if(infoAgency.error) {
                                    return res.json(infoAgency);
                                }

                                if(transaction.latestInjectTransaction) {
                                    TYPE_TRANSACTION.forEach( type => {
                                        let typeOfInjectTransaction = transaction.latestInjectTransaction.type;
                                        if(type.value == typeOfInjectTransaction) {
                                            nameAgency += type.text;
                                        }
                                    });
                                }else{
                                    nameAgency += "Cầm đồ";
                                }
                                nameAgency += " từ " + infoAgency.data.name;
                                //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                                let obj = {
                                    transactionDate:     transaction.transactionDate,
                                    expectedDate:        transaction.expectedDate,
                                    loanAmount:          transaction.loanAmount,
                                    approvalLimit:       transaction.approvalLimit,
                                    interestStartDate:   transaction.interestStartDate,
                                    status:              transaction.status,
                                    injectTransaction:   transaction.injectTransaction,
                                    products:            transaction.products,
                                    typeInventory:       transaction.typeInventory,
                                    _id:                 transaction._id,
                                    expireTime:          transaction.expireTime,
                                    customer:            transaction.customer,
                                    transactionEmployee: transaction.transactionEmployee,
                                    code:                transaction.code,
                                    createAt:            transaction.createAt,
                                    nameAgency
                                }
                                const DATE_OF_PERIOD = 30;
                                
                                let interestOfTransaction = 0;
                                // if (limitDate > 0 ){
                                let TYPE_PRODUCT = transaction.products[0].type._id;
                                let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID: agencyID, limitDate: DATE_OF_PERIOD, price: transaction.loanAmount, typeProduct: TYPE_PRODUCT});
                                checkDateToInterest.data.forEach( price => {
                                    interestOfTransaction += price;
                                })
                                obj.interestOfTransaction = interestOfTransaction;
                                obj.percent               = checkDateToInterest.percent;
                                // }
                                if(transaction.latestInjectTransaction) {
                                    obj = {
                                        ...obj,
                                        latestInjectTransaction: transaction.latestInjectTransaction,
                                        ransom: transaction.latestInjectTransaction.ransom, 
                                        type: transaction.latestInjectTransaction.type,
                                    }
                                }else{
                                    obj = {
                                        ...obj,
                                        latestInjectTransaction: {},
                                        ransom: 0, 
                                        type: 1
                                    }
                                }
                                dataAfterInsertInterest.push(
                                    obj
                                );
                            }
                            listTransactionCustomer = { error: false, data: dataAfterInsertInterest };
                        }
                        // Set List Giao dịch đang giao dịch
                        let listTransactionChange;
                        /**
                             * // STATUS: 1 
                             *    => 
                             */

                        let listTransactionChangeNow = await TRANSACTION_MODEL.getListByCustomer({ customerID, status: 1, typeTransaction, fromDay, toDay });
                        console.log({
                            listTransactionChangeNow
                        });
                        let dataAfterInsertInterest = [];
                        for (let transaction of listTransactionChangeNow.data) {
                            let interestStartDate = transaction.interestStartDate;

                            let dateNow   =  new Date();
                            let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                            limitDate     = Math.floor(limitDate);
                            if(limitDate >= 0) {
                                limitDate     = limitDate + 1;
                            }
                            if(limitDate < 0) {
                                limitDate = 0;
                            }
                            // let infoInterest;
                            // let percent = 0;
                            let agencyID = transaction.agency._id;
                            let nameAgency            = "";
                            /**
                             * Lấy thông tin agency 
                             */
                            let infoAgency = await AGENCY_MODEL.getInfoFieldNeed({ agencyID });
                            if(infoAgency.error) {
                                return res.json(infoAgency);
                            }

                            if(transaction.latestInjectTransaction) {
                                TYPE_TRANSACTION.forEach( type => {
                                    let typeOfInjectTransaction = transaction.latestInjectTransaction.type;
                                    if(type.value == typeOfInjectTransaction) {
                                        nameAgency += type.text;
                                    }
                                });
                            }else{
                                nameAgency += "Cầm đồ";
                            }
                            nameAgency += " từ " + infoAgency.data.name;
                            //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                            let obj = {
                                transactionDate:     transaction.transactionDate,
                                expectedDate:        transaction.expectedDate,
                                expireTime:          transaction.expireTime,
                                loanAmount:          transaction.loanAmount,
                                approvalLimit:       transaction.approvalLimit,
                                interestStartDate:   transaction.interestStartDate,
                                status:              transaction.status,
                                injectTransaction:   transaction.injectTransaction,
                                products:            transaction.products,
                                typeInventory:       transaction.typeInventory,
                                _id:                 transaction._id,
                                expireTime:          transaction.expireTime,
                                customer:            transaction.customer,
                                transactionEmployee: transaction.transactionEmployee,
                                code:                transaction.code,
                                createAt:            transaction.createAt,
                                nameAgency
                            }
                            let interestOfTransaction = 0;
                            if (limitDate > 0 ){
                                let TYPE_PRODUCT = transaction.products[0].type._id;
                                let agencyID     = transaction.agency;
                                let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID: agencyID, limitDate, price: transaction.loanAmount, typeProduct: TYPE_PRODUCT});
                                checkDateToInterest.data.forEach( price => {
                                    interestOfTransaction += price;
                                })
                                obj.interestOfTransaction = interestOfTransaction;
                                obj.percent               = checkDateToInterest.percent;
                            }
                            if(transaction.latestInjectTransaction) {
                                obj = {
                                    ...obj,
                                    latestInjectTransaction: transaction.latestInjectTransaction,
                                    ransom: transaction.latestInjectTransaction.ransom, 
                                    type: transaction.latestInjectTransaction.type
                                }
                            }else{
                                obj = {
                                    ...obj,
                                    latestInjectTransaction: {},
                                    ransom: 0, 
                                    type: 1
                                }
                            }
                            dataAfterInsertInterest.push(
                                obj
                            );
                        }
                        if(type && type === "now") {
                            listTransactionChange   = listTransactionChangeNow;
                            listTransactionCustomer = { error: false, data: dataAfterInsertInterest };
                        }
                        // Set List giao dịch đến hạn
                        if(type && type === "maturity") {
                            listTransactionChange = await TRANSACTION_MODEL.getListByCustomerIDCheckDate({ customerID, status: 1, typeTransaction, fromDay, toDay });
                            let dataAfterInsertInterestMaturity = [];
                            if(listTransactionChange.error == false && listTransactionChange.data.length) {
                                let DATE_MATURITY_MAX   = 5;
                                let DATE_MATURITY_LIMIT = 0;
                                listTransactionChange = listTransactionChange.data.filter( transaction => transaction.dateDifference < DATE_MATURITY_MAX && transaction.dateDifference >= DATE_MATURITY_LIMIT);
                                for (let transaction of listTransactionChange) {
                                    let interestStartDate = transaction.interestStartDate;

                                    let dateNow   =  new Date();
                                    let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                                    limitDate     = Math.floor(limitDate);
                                    if(limitDate >= 0) {
                                        limitDate     = limitDate + 1;
                                    }
                                    if(limitDate < 0) {
                                        limitDate = 0;
                                    }
                                    // let infoInterest;
                                    // let percent = 0;
                                    let agencyID = transaction.agency._id;
                                    let nameAgency            = "";
                                    /**
                                     * Lấy thông tin agency 
                                     */
                                    let infoAgency = await AGENCY_MODEL.getInfoFieldNeed({ agencyID });
                                    if(infoAgency.error) {
                                        return res.json(infoAgency);
                                    }

                                    if(transaction.latestInjectTransaction) {
                                        TYPE_TRANSACTION.forEach( type => {
                                            let typeOfInjectTransaction = transaction.latestInjectTransaction.type;
                                            if(type.value == typeOfInjectTransaction) {
                                                nameAgency += type.text;
                                            }
                                        });
                                    }else{
                                        nameAgency += "Cầm đồ";
                                    }
                                    nameAgency += " từ " + infoAgency.data.name;
                                    //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                                    let obj = {
                                        transactionDate:     transaction.transactionDate,
                                        expectedDate:        transaction.expectedDate,
                                        loanAmount:          transaction.loanAmount,
                                        approvalLimit:       transaction.approvalLimit,
                                        interestStartDate:   transaction.interestStartDate,
                                        status:              transaction.status,
                                        injectTransaction:   transaction.injectTransaction,
                                        // products:            transaction.products,
                                        typeInventory:       transaction.typeInventory,
                                        _id:                 transaction._id,
                                        expireTime:          transaction.expireTime,
                                        customer:            transaction.customer,
                                        transactionEmployee: transaction.transactionEmployee,
                                        code:                transaction.code,
                                        createAt:            transaction.createAt,
                                        nameAgency
                                    }
                                    let interestOfTransaction = 0;
                                    if (limitDate > 0 ){
                                        let TYPE_PRODUCT = transaction.products[0].type._id;
                                        let agencyID     = transaction.agency;
                                        let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID: agencyID, limitDate, price: transaction.loanAmount, typeProduct: TYPE_PRODUCT});
                                        checkDateToInterest.data.forEach( price => {
                                            interestOfTransaction += price;
                                        })
                                        obj.interestOfTransaction = interestOfTransaction;
                                        obj.percent               = checkDateToInterest.percent;
                                    }
                                    if(transaction.latestInjectTransaction && transaction.latestInjectTransaction.length) {
                                        obj = {
                                            ...obj,
                                            latestInjectTransaction: transaction.latestInjectTransaction,
                                            ransom: transaction.latestInjectTransaction.ransom, 
                                            type: transaction.latestInjectTransaction.type
                                        }
                                    }else{
                                        obj = {
                                            ...obj,
                                            latestInjectTransaction: [],
                                            ransom: 0, 
                                            type: 1
                                        }
                                    }
                                    dataAfterInsertInterestMaturity.push(
                                        obj
                                    );
                                }
                            }
                            listTransactionCustomer = { error: false, data: dataAfterInsertInterestMaturity};
                        }
                        let description = {
                            status: "Trạng thái (GD Cầm đồ chính) (0: Đang xử lý - Chờ duyệt/ 1: Đang cầm - Đã duyệt/ 2: Không duyệt/ 3: Hoàn thành giao dịch)",
                            ransom: "Trạng thái duyệt chuộc đồ (0: Không phải giao dịch chuộc đồ /1: Đã duyệt chuộc/ 2: Chưa duyệt chuộc/ 3: Không duyệt)",
                            type:   "Loại giao dịch bổ sung (0. Phát sinh/ 1. Cầm đồ/ 2. Gia hạn bình thường/ 3. Gia hạn vay thêm/ 4. Gia hạn trả bớt/ 5. Báo mất/ 6. Chuộc đồ )",
                        };
                        let data = {
                            listTransactionCustomer: listTransactionCustomer.data,
                            totalPriceToPay,
                            loanAmoutAfterInterest,
                            loanAmoutAfterInterest30Day
                        }

                        res.json({ description, error: false, data });
                    }]
                },
            },
            /**
             * API Xuất thông tin giao dịch cho Mobile
             */
            [CF_ROUTINGS_TRANSACTION.INFO_TRANSACTION_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID, level } = req.user;
                        let { transactionID }  = req.params;
                        // let agencyID = req.agencyID;
                        // log
                        let infoTransaction = await TRANSACTION_MODEL.getInfoTransaction({ transactionID });
                        // console.log({ infoTransaction });

                        if(infoTransaction.error) {
                            return res.json(infoTransaction);
                        }

                        let interestStartDate = infoTransaction.data.interestStartDate;

                        let dateNow   =  new Date();
                        let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                        limitDate     = Math.floor(limitDate);
                        /**
                         * 1: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại > 0
                         *    => limitDate + 1, ngày tính lãi bắt đầu 1
                         * 2: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại < 0
                         *    => Đã trả lãi trước
                         *    => limitDate - 1, ngày tính lãi - 1
                         */
                        if(limitDate >= 0) {
                            limitDate     = Math.floor(limitDate) + 1;
                        }
                        if(limitDate < 0) {
                            limitDate = 0;
                        }

                        let infoInterest;
                        if(infoTransaction.data.status == 3 || infoTransaction.data.status == 2) {
                            limitDate = 0;
                        }

                        let loanAmoutAfterInterest = 0;
                        let percent = 0;
                        //Tính lãi dựa trên ngày hiện tại với ngày bắt đầu tính lãi
                        // console.log({ limitDate });
                        let TYPE_PRODUCT = infoTransaction.data.products[0].type._id;
                        let arrObjPerPeriod = [];

                        if (limitDate > 0 ){
                            if(!infoTransaction.data.products || !infoTransaction.data.products.length) {
                                return res.json({ error: true, message: "product_invalid" }); 
                            }
                            let checkDateToInterest = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID: infoTransaction.data.agency, limitDate, price: infoTransaction.data.loanAmount, typeProduct: TYPE_PRODUCT});
                            // console.log({ checkDateToInterest });
                            checkDateToInterest.data.forEach( price => {
                                loanAmoutAfterInterest += price;
                            })
                            percent         = checkDateToInterest.percent;
                            infoInterest    = checkDateToInterest.infoInterest;
                            arrObjPerPeriod = checkDateToInterest.arrObjPerPeriod 
                        }
                        /**
                         * Lấy Lãi và Phần Trăm Lãi của 1 ngày
                         */
                         let loanAmoutAfterInterestOneDay = 0;
                         let percentOneDay = 0;
                         let DTAE_OF_PERIOD =  1;
 
                         let checkDateToInterestOneDay = await INTEREST_RATE_MODEL.checkDateToInterest({ agencyID: infoTransaction.data.agency, limitDate: DTAE_OF_PERIOD, price: infoTransaction.data.loanAmount, typeProduct: TYPE_PRODUCT});
                         checkDateToInterestOneDay.data.forEach( price => {
                              loanAmoutAfterInterestOneDay += price;
                         })
                         percentOneDay      = checkDateToInterestOneDay.percent;

                         let interestOneday = {
                             percentOneDay,
                             loanAmoutAfterInterestOneDay
                         }

                        let infoTransactionAfterChange = infoTransaction.data;
                        infoTransactionAfterChange = {
                            ...infoTransactionAfterChange._doc,
                            percent, loanAmoutAfterInterest, limitDate, arrObjPerPeriod, interestOneday
                        }
                        // console.log({ loanAmoutAfterInterest });
                        let listDistricts        = Object.entries(districts);
                        let districtArr = [];

                        let listProvince        = Object.entries(provinces);
                        let provinceArr = [];
                        for (let province of listProvince){
                            if ( province[1].code == infoTransactionAfterChange.customer.city ){
                                infoTransactionAfterChange.customer.city = province[1].name_with_type;
                                break;
                            }
                        }

                        let listWards = [];
                        let wardArr = [];
                        if ( infoTransactionAfterChange.customer.district ){
                            for (let district of listDistricts){
                                if ( district[1].code == infoTransactionAfterChange.customer.district ){
                                    districtArr = district;
                                    infoTransactionAfterChange.customer.district = district[1].name_with_type;
                                    break;
                                }
                            }
                            let  filePath = path.resolve(__dirname, `../../common/constant/wards/${districtArr[1].code}.json`);
                            await fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                                if (!err) {
                                    listWards = JSON.parse(data);
                                    listWards = Object.entries(listWards);
                                    for (let ward of listWards){
                                        if ( ward[1].code == infoTransactionAfterChange.customer.ward ){
                                            infoTransactionAfterChange.customer.ward = [1].name_with_type;
                                            break;
                                        }
                                    }
                                    res.json( 
                                        { 
                                            infoTransaction: infoTransactionAfterChange,
                                            level,
                                            infoInterest: infoInterest,
                                            limitDate,
                                            loanAmoutAfterInterest,
                                            percent
                                        }
                                    );
                                } else {
                                    res.json( 
                                        { 
                                            infoTransaction: infoTransactionAfterChange,
                                            level,
                                            infoInterest: infoInterest,
                                            limitDate,
                                            loanAmoutAfterInterest,
                                            percent
                                        }
                                    );
                                }
                            });
                        }else{
                            res.json( 
                                { 
                                    infoTransaction: infoTransactionAfterChange,
                                    level,
                                    infoInterest: infoInterest,
                                    limitDate,
                                    loanAmoutAfterInterest,
                                    percent
                                }
                            );
                        }
                    //     ChildRouter.renderToView(req, res, 
                    //         { 
                    //             infoTransaction: infoTransaction.data,
                    //             TYPE_GENDER
                    //         }
                    //     );
                    }]
                },
            },

            /**
             * Function: Thông tin giao dịch (duyệt các loại giao dịch khác)
             */
             [CF_ROUTINGS_TRANSACTION.EXPORT_EXCEL_TRANSACTION]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { _id: userID } = req.user;
                        let { type, start, end, name, city, district, ward, sort, agencyID } = req.query;
                        // let agencyID   = AGENCY_SESSION.getContextAgency(req.session).agencyID; ádasd

                        /**
                         * DANH SÁCH GIAO DỊCH
                         */
                        let listTransaction = await TRANSACTION_MODEL.getListTransactionReveneExportExcel({ agencyID, typeProductCategoryID: type, start, end, name, city, district, ward, status: sort });
                        
                        XlsxPopulate.fromFileAsync(path.resolve(__dirname, ('../../../../files/excel/Report_Transactions_Form.xlsx')))
                        .then(async workbook => {
                            let dateFilter = '';
                            if (start) {
                                dateFilter += `Từ ngày: ${moment(start).format('L')}`;
                            }
                            if (end) {
                                dateFilter += ` Đến ngày: ${moment(end).format('L')}`;
                            }
                            if (!start && !end) {
                                dateFilter = 'Tất cả';
                            }

                            workbook.sheet("Report").row(2).cell(1).value(dateFilter);

                            if (!listTransaction.error && listTransaction.data.listTransaction.length) {
                                let index = 0;
                               
                                for (let item of listTransaction.data.listTransaction) {
                                    let data = await TRANSACTION_MODEL.getDataToExportExcel({
                                        infoTransaction: item
                                    });
                                    
                                    if (item.injectTransaction && item.injectTransaction.length) {
                                        for (let i = -1; i < item.injectTransaction.length; i++) {
                                            if (i == -1) {
                                                workbook.sheet("Report").row(index + 5).cell(1).value(index + 1);
                                                workbook.sheet("Report").row(index + 5).cell(2).value(data.data.parentCode);
                                                workbook.sheet("Report").row(index + 5).cell(3).value(data.data.codeAgency);
                                                workbook.sheet("Report").row(index + 5).cell(4).value(data.data.nameAgency);
                                                workbook.sheet("Report").row(index + 5).cell(5).value(data.data.phoneAgency);
                                                workbook.sheet("Report").row(index + 5).cell(6).value(data.data.addressAgency);
                                                workbook.sheet("Report").row(index + 5).cell(7).value(data.data.listOwner);
                                                workbook.sheet("Report").row(index + 5).cell(8).value(data.data.emailAgency);
                                                workbook.sheet("Report").row(index + 5).cell(9).value(moment(data.data.createAt).format('L'));
                                                workbook.sheet("Report").row(index + 5).cell(10).value(data.data.codeCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(11).value(data.data.nameCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(12).value(data.data.phoneCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(13).value(data.data.addressCustomer);
                                                workbook.sheet("Report").row(index + 5).cell(15).value(data.data.codeTransaction);
                                                workbook.sheet("Report").row(index + 5).cell(16).value(data.data.typeProduct);
                                                workbook.sheet("Report").row(index + 5).cell(17).value(data.data.productName);
                                                workbook.sheet("Report").row(index + 5).cell(18).value(data.data.approvalLimit);

                                                let typeCheck = 'Cầm đồ';
                                                workbook.sheet("Report").row(index + 5).cell(14).value(typeCheck);
                                                workbook.sheet("Report").row(index + 5).cell(19).value(item.totalMoneyNumber);
                                                workbook.sheet("Report").row(index + 5).cell(20).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(21).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(22).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(23).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(24).value(0);
                                                workbook.sheet("Report").row(index + 5).cell(25).value(item.nameFinancial ? item.nameFinancial : 0);
                                                let status = '';
                                                switch (item.status) {
                                                    case 0:
                                                        status = 'Đang xử lý'   
                                                        break;
                                                    case 1:
                                                        status = 'Đang cầm'   
                                                        break;
                                                    case 2:
                                                        status = 'Không duyệt'   
                                                        break;
                                                    case 3:
                                                        status = 'Hoàn thành giao dịch'   
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                workbook.sheet("Report").row(index + 5).cell(26).value(status);
                                                workbook.sheet("Report").row(index + 5).cell(27).value(item.transactionEmployee ? item.transactionEmployee.username : '');
                                                workbook.sheet("Report").row(index + 5).cell(28).value(item.approvalNote ? item.approvalNote: '');
                                                workbook.sheet("Report").row(index + 5).cell(29).value(item.ownerConfirm ? item.ownerConfirm.username : '');
                                                workbook.sheet("Report").row(index + 5).cell(30).value(item.noteNotApproved);
                                                index ++;

                                           } else {
                                                // CHECK TYPE TRANSACTION
                                                let typeCheck = '';
                                                if (item.injectTransaction[i].type != 5) {
                                                    workbook.sheet("Report").row(index + 5).cell(1).value(index + 1);
                                                    workbook.sheet("Report").row(index + 5).cell(2).value(data.data.parentCode);
                                                    workbook.sheet("Report").row(index + 5).cell(3).value(data.data.codeAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(4).value(data.data.nameAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(5).value(data.data.phoneAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(6).value(data.data.addressAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(7).value(data.data.listOwner);
                                                    workbook.sheet("Report").row(index + 5).cell(8).value(data.data.emailAgency);
                                                    workbook.sheet("Report").row(index + 5).cell(9).value(moment(item.injectTransaction[i].createAt).format('L'));
                                                    workbook.sheet("Report").row(index + 5).cell(10).value(data.data.codeCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(11).value(data.data.nameCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(12).value(data.data.phoneCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(13).value(data.data.addressCustomer);
                                                    workbook.sheet("Report").row(index + 5).cell(15).value(data.data.codeTransaction);
                                                    workbook.sheet("Report").row(index + 5).cell(16).value(data.data.typeProduct);
                                                    workbook.sheet("Report").row(index + 5).cell(17).value(data.data.productName);
                                                    workbook.sheet("Report").row(index + 5).cell(18).value(data.data.approvalLimit);

                                                    let status = '';
                                                    switch (item.injectTransaction[i].status) {
                                                        case 0:
                                                            if (item.injectTransaction[i].ransom == 1) {
                                                                status = 'Đã duyệt';
                                                            } else if (item.injectTransaction[i].ransom == 2) {
                                                                status = 'Chưa duyệt';
                                                            } else if (item.injectTransaction[i].ransom == 3) {
                                                                status = 'Không duyệt';
                                                            } else {
                                                                status = 'Đã duyệt'   
                                                            }
                                                            break;
                                                        case 1:
                                                            status = 'Đã duyệt'   
                                                            break;
                                                        case 2:
                                                            status = 'Không duyệt'   
                                                            break;
                                                        case 3:
                                                            status = 'Đang xử lý'   
                                                            break;
                                                        default:
                                                            break;
                                                    }

                                                    let totalMoneyNumber = 0;
                                                    let totalPriceBorrow = 0;
                                                    let totalPricePay    = 0;
                                                    let totalPriceNormal = 0;
                                                    let totalPriceRansom = 0;
                                                    let loanAmountAfterChange = 0;
                                                    let nameStatusFinancial = '';
                                                    nameStatusFinancial = (item.injectTransaction[i].meta.statusFinancial && item.injectTransaction[i].meta.statusFinancial == 1) ? "Quỹ tiền mặt" : 'Quỹ ngân hàng';
                                                    switch (item.injectTransaction[i].type) {
                                                        case 2:
                                                            typeCheck = 'Gia hạn bình thường';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            loanAmountAfterChange = item.injectTransaction[i].meta.loanAmountAfterChange ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;
                                                            break;
                                                        case 3:
                                                            typeCheck = 'Gia hạn vay thêm';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            totalPriceBorrow = item.injectTransaction[i].meta.amountWantBorrowMore;
                                                            loanAmountAfterChange = item.injectTransaction[i].meta.loanAmountAfterChange ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;

                                                            break;
                                                        case 4:
                                                            typeCheck = 'Gia hạn trả bớt';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            totalPricePay    = item.injectTransaction[i].meta.amountWantPayback;
                                                            loanAmountAfterChange = item.injectTransaction[i].meta.loanAmountAfterChange ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;

                                                            break;
                                                        case 6:
                                                            typeCheck = 'Gia hạn chuộc đồ';
                                                            totalMoneyNumber = item.injectTransaction[i].meta.loanAmount;
                                                            totalPriceNormal = item.injectTransaction[i].meta.priceMuchPayInterest;
                                                            totalPriceRansom = item.injectTransaction[i].meta.priceRansomPay;
                                                            loanAmountAfterChange = (item.injectTransaction[i].meta.loanAmountAfterChange) ? item.injectTransaction[i].meta.loanAmountAfterChange: 0;
                                                            
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    
                                                    workbook.sheet("Report").row(index + 5).cell(14).value(typeCheck);
                                                    workbook.sheet("Report").row(index + 5).cell(19).value(totalMoneyNumber);
                                                    workbook.sheet("Report").row(index + 5).cell(20).value(totalPriceBorrow);
                                                    workbook.sheet("Report").row(index + 5).cell(21).value(totalPricePay);
                                                    workbook.sheet("Report").row(index + 5).cell(22).value(totalPriceRansom);
                                                    workbook.sheet("Report").row(index + 5).cell(23).value(loanAmountAfterChange);
                                                    workbook.sheet("Report").row(index + 5).cell(24).value(totalPriceNormal);
                                                    workbook.sheet("Report").row(index + 5).cell(25).value(nameStatusFinancial);
                                                    workbook.sheet("Report").row(index + 5).cell(26).value(status);
                                                    workbook.sheet("Report").row(index + 5).cell(27).value(item.injectTransaction[i].userCreateAt ? item.injectTransaction[i].userCreateAt.username: '');
                                                    workbook.sheet("Report").row(index + 5).cell(28).value(item.injectTransaction[i].note ? item.injectTransaction[i].note: '');
                                                    workbook.sheet("Report").row(index + 5).cell(29).value(item.injectTransaction[i].userConfirm ? item.injectTransaction[i].userConfirm.username : '');
                                                    workbook.sheet("Report").row(index + 5).cell(30).value(item.injectTransaction[i].noteNotApproved ? item.injectTransaction[i].noteNotApproved: '');
                                                    index ++;
                                                }
                                           }
                                        }
                                    } else {
                                        workbook.sheet("Report").row(index + 5).cell(1).value(index + 1);
                                        workbook.sheet("Report").row(index + 5).cell(2).value(data.data.parentCode);
                                        workbook.sheet("Report").row(index + 5).cell(3).value(data.data.codeAgency);
                                        workbook.sheet("Report").row(index + 5).cell(4).value(data.data.nameAgency);
                                        workbook.sheet("Report").row(index + 5).cell(5).value(data.data.phoneAgency);
                                        workbook.sheet("Report").row(index + 5).cell(6).value(data.data.addressAgency);
                                        workbook.sheet("Report").row(index + 5).cell(7).value(data.data.listOwner);
                                        workbook.sheet("Report").row(index + 5).cell(8).value(data.data.emailAgency);
                                        workbook.sheet("Report").row(index + 5).cell(9).value(moment(data.data.createAt).format('L'));
                                        workbook.sheet("Report").row(index + 5).cell(10).value(data.data.codeCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(11).value(data.data.nameCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(12).value(data.data.phoneCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(13).value(data.data.addressCustomer);
                                        workbook.sheet("Report").row(index + 5).cell(15).value(data.data.codeTransaction);
                                        workbook.sheet("Report").row(index + 5).cell(16).value(data.data.typeProduct);
                                        workbook.sheet("Report").row(index + 5).cell(17).value(data.data.productName);
                                        workbook.sheet("Report").row(index + 5).cell(18).value(data.data.approvalLimit);

                                        let typeCheck = 'Cầm đồ';
                                        workbook.sheet("Report").row(index + 5).cell(14).value(typeCheck);
                                        workbook.sheet("Report").row(index + 5).cell(19).value(item.totalMoneyNumber);
                                        workbook.sheet("Report").row(index + 5).cell(20).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(21).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(22).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(23).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(24).value(0);
                                        workbook.sheet("Report").row(index + 5).cell(25).value(item.nameFinancial ? item.nameFinancial : 0);
                                        let status = '';
                                        switch (item.status) {
                                            case 0:
                                                status = 'Đang xử lý'   
                                                break;
                                            case 1:
                                                status = 'Đang cầm'   
                                                break;
                                            case 2:
                                                status = 'Không duyệt'   
                                                break;
                                            case 3:
                                                status = 'Hoàn thành giao dịch'   
                                                break;
                                            default:
                                                break;
                                        }
                                        workbook.sheet("Report").row(index + 5).cell(26).value(status);
                                        workbook.sheet("Report").row(index + 5).cell(27).value(item.transactionEmployee ? item.transactionEmployee.username: '');
                                        workbook.sheet("Report").row(index + 5).cell(28).value(item.approvalNote ? item.approvalNote: '');
                                        workbook.sheet("Report").row(index + 5).cell(29).value(item.ownerConfirm ? item.ownerConfirm.username : '');
                                        workbook.sheet("Report").row(index + 5).cell(30).value(item.noteNotApproved);
                                        index ++;
                                    }
                                }
                            }

                            let fileNameRandom = require('../../../utils/string_utils').randomStringFixLengthOnlyAlphabet(10);
                            let pathWriteFile = path.resolve(__dirname, '../../../../files/upload_excel_temp/', `Report_Transaction_${fileNameRandom}.xlsx`);
                            
                            workbook.toFileAsync(pathWriteFile)
                                .then(_ => {
                                    // Download file from server
                                    res.download(pathWriteFile, function (err) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            // Remove file on server
                                            fs.unlinkSync(pathWriteFile);
                                        }    
                                    });
                                });                           
                        });
                    }]
                },
            },

            /**
             * API DANH SÁCH GIAO DỊCH CỦA KHÁCH HÀNG
             */
            //  [CF_ROUTINGS_TRANSACTION.LIST_TRANSACTION_CUSTOMER]: {
            //     config: {
            //         auth: [ roles.role.customer.bin ],
            //         type: 'json',
            //     },
            //     methods: {
            //         get: [ async function (req, res) {
            //             let { _id: customerID } = req.user;
            //             let { type, fromDate, toDate, sort }           = req.query;
            //             /**
            //              * Lưu ý: DATE
            //              *  fromDate, toDate: cần format mm/dd/yyyy
            //              */
            //             if(!checkObjectIDs(customerID))
            //                 return res.json({ error: true, message: 'customerID_invalid' })

            //             /**
            //              * 1 // TYPE: LOẠI GIAO DỊCH
            //              *      => TYPE: 0 (TẤT CẢ GIAO DỊCH) (TYPE_ALL_TRANSACTION)
            //              *      => TYPE: 1 (CẦM ĐỒ)           (TYPE_TRANSACTION_MAIN)
            //              *      => TYPE: 3 (CHUỘC ĐỒ)         (TYPE_TRANSACTION_RANSOM)
            //              */
            //             const TYPE_ALL_TRANSACTION    = -1;
            //             const TYPE_TRANSACTION_MAIN   = 1;
            //             const TYPE_TRANSACTION_RANSOM = 3;
                        
            //             /**
            //              * 2// SORT 
            //              *      1: GIÁ GIAO DỊCH TĂNG DẦN (SORT_PRICE_INCREASE)
            //              *      2: GIÁ GIAO DỊCH GIẢM DẦN (SORT_PRICE_DECREASE)
            //              *      3: NGÀY GẦY NHẤT          (SORT_DATE_INCREASE)
            //              */
            //             const SORT_PRICE_INCREASE     = 1;
            //             const SORT_PRICE_DECREASE     = 2;
            //             const SORT_DATE_INCREASE      = 3; 

            //             /**
            //              *      TỔNG SỐ TIỀN LÃI CỦA KHÁCH HÀNG (totalLoanAmountAfterInterest)
            //              *      TỔNG SỐ TIỀN VAY                (totalLoanAmount)
            //              *      LIST GIAO DỊCH SAU KHI TÍNH LÃI (listTransactionAfterInterest)
            //              */
            //             let totalLoanAmountAfterInterest = 0;
            //             let totalLoanAmount = 0;
            //             let listTransactionAfterInterest = [];

            //             const ARRAY_VALID_SORT        = [
            //                 SORT_PRICE_INCREASE, SORT_PRICE_DECREASE, SORT_DATE_INCREASE
            //             ];

            //             let conditionObj = {
            //                 customer: customerID
            //             },
            //                 sortObj = {};
                        
            //             if (type == TYPE_TRANSACTION_MAIN) {
            //                 conditionObj = { 
            //                     ...conditionObj,
            //                     status: TYPE_TRANSACTION_MAIN
            //                 }
            //             } else if (type == TYPE_TRANSACTION_RANSOM) {
            //                 conditionObj = { 
            //                     ...conditionObj,
            //                     status: TYPE_TRANSACTION_RANSOM
            //                 }
            //             } else {
            //                 conditionObj = { 
            //                     ...conditionObj,
            //                     $or: [
            //                         {
            //                             status: TYPE_TRANSACTION_MAIN
            //                         },
            //                         {
            //                             status: TYPE_TRANSACTION_RANSOM
            //                         }
            //                     ]
            //                 }
            //             }
            //             /**
            //              * FILTER WORKSPACE
            //              */
            //             // fromDate - toDate
            //             if (isValidDate({ d: new Date(fromDate)}) && isValidDate({ d: new Date(toDate)}) ) {
            //                 let fromDateStartDay = moment(fromDate).startOf('day');
            //                 let toDateStartDay   = moment(toDate).endOf('day');
            //                 conditionObj = { 
            //                     ...conditionObj,
            //                     interestStartDate: {
            //                         $gte: fromDateStartDay,
            //                         $lte: toDateStartDay,
            //                     }
            //                 }
            //             }   
                        
            //             //SORT
            //             if (ARRAY_VALID_SORT.includes(Number(sort))) {
            //                 if (sort == SORT_PRICE_INCREASE) {
            //                     sortObj.loanAmount = 1;
            //                 }
            //                 if (sort == SORT_PRICE_DECREASE) {
            //                     sortObj.loanAmount = -1;
            //                 }
            //                 if (sort == SORT_DATE_INCREASE) {
            //                     sortObj.interestStartDate = -1;
            //                 }
            //             }

            //             /**
            //              * 3 //  Lấy danh sách giao dịch
            //              */
            //             let listTransaction = await TRANSACTION_COLL.find({...conditionObj})
            //                 .sort({...sortObj, interestStartDate: -1})
            //                 .populate("latestInjectTransaction customer")
            //                 .populate({
            //                     path: "products",
            //                     select: "_id name type",
            //                     populate: "type"
            //                 });
                    
            //             let description = {
            //                 status: "1: Đang Cầm || 3: Đã chuộc (Hoàn Thành Giao Dịch)",
            //                 totalLoanAmountAfterInterest: "Tổng số tiền lãi của khách hàng",
            //                 totalLoanAmount: "Tổng số tiền vay",
            //                 priceInterestTransaction: "Tiền lãi của từng giao dịch",
            //                 interestStartDate: "Ngày bắt đầu tính lãi",
            //                 expireTime: "Ngày đến hạn",
            //             }
            //             if(!listTransaction || !listTransaction.length) {
            //                 return res.json({ description, error: false, data: [] })
            //             }
                        
            //             for (let transaction of listTransaction) {
            //                 /**
            //                  * 1// LẤY THÔNG TIN GIAO DỊCH ĐỂ TÍNH LÃI
            //                  *     1: NGÀY BẮT ĐẦU TÍNH LÃI (interestStartDate)
            //                  *     2: ID ĐẠI LÝ (agencyID)
            //                  *     3: ID SẢN PHẨM (TYPE_PRODUCT) (C)
            //                  *     4: 
            //                  */
            //                 let interestStartDate     = transaction.interestStartDate;
            //                 let agencyID              = transaction.agency;
            //                 const PRODUCT_TRANSACTION = transaction.products[0];
            //                 let TYPE_PRODUCT          = PRODUCT_TRANSACTION.type._id;
            //                 totalLoanAmount           += transaction.loanAmount;
            //                 let nameAgency            = "";
            //                 let priceInterestTransaction = 0;
            //                 /**
            //                  * Lấy thông tin agency 
            //                  */
            //                 let infoAgency = await AGENCY_MODEL.getInfoFieldNeed({ agencyID });
            //                 if(infoAgency.error) {
            //                     return res.json(infoAgency);
            //                 }

            //                 if(transaction.latestInjectTransaction) {
            //                     if(type == TYPE_TRANSACTION_RANSOM) {
            //                         let priceAfterPayRansom = transaction.latestInjectTransaction.meta.priceRansomPay;
            //                         totalLoanAmount         += priceAfterPayRansom;
            //                     }
            //                     TYPE_TRANSACTION.forEach( type => {
            //                         let typeOfInjectTransaction = transaction.latestInjectTransaction.type;
            //                         if(type.value == typeOfInjectTransaction) {
            //                             nameAgency += type.text;
            //                         }
            //                     });
            //                 }else{
            //                     nameAgency += "Cầm đồ";
            //                 }
            //                 nameAgency += " từ " + infoAgency.data.name;

            //                 /**
            //                  * 2// TÍNH LÃI GIAO DỊCH
            //                  */
            //                 let interestRateTransaction = await INTEREST_RATE_MODEL.checkDateToInterestTransaction({ agencyID, interestStartDate, price: transaction.loanAmount, typeProduct: TYPE_PRODUCT});

            //                 if(interestRateTransaction.error == false && interestRateTransaction.data) {
            //                     let loanAmoutAfterInterestTransaction = interestRateTransaction.data.loanAmoutAfterInterest;
            //                     totalLoanAmountAfterInterest          += loanAmoutAfterInterestTransaction;
            //                     priceInterestTransaction              += loanAmoutAfterInterestTransaction;
            //                 }
            //                 transaction = {
            //                     ...transaction._doc,
            //                     nameAgency,
            //                     priceInterestTransaction
            //                 }
            //                 listTransactionAfterInterest.push(transaction)
            //             }
            //             let data = {
            //                 listTransaction: listTransactionAfterInterest,
            //                 totalLoanAmountAfterInterest,
            //                 totalLoanAmount
            //             }
                        
            //             return res.json({ description, error: false, data: data })
            //         }]
            //     },
            // },
        }
    }
};
