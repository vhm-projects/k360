"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION GIAO DỊCH CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'inject_transaction' }){
   return BASE_COLl(name, {
        code: {
            type: Number,
            unique : true,
        },
        /**
         *  Loại giao dịch
         *  Tham khảo TYPE_TRANSACTION file cf_constants
         */

        type: {
            type: Number,
            require: true
        }, 
         /**
         * Tiêu đề giao dịch
         */
        title: String,
        /**
         * Đại lý
         */ 
        agency: {
            type: Schema.Types.ObjectId,
            ref: 'agency'
        },
        /**
         * Hình ảnh khách hàng
         */ 
        customerImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],

        /**
         * Hình Ảnh Giấy Tờ (CMND/CCCD)
         */ 
        KYCImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],

        /**
         * Hình Ảnh Biên Nhận
         */ 
        receiptImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],
         /**
         * Hình Ảnh Phiếu Kiểm Định
         */ 
        formVerificationImages: [{
            type: Schema.Types.ObjectId,
            ref: 'image'
        }],

        /**
         * GIAO DỊCH CHA
         */ 
        transaction: {
            type: Schema.Types.ObjectId,
            ref: 'transaction'
        }, 
        /**
        * NGÀY GIAO DỊCH
        * Chuyển thành ngày duyệt giao dịch 
        */ 
        transactionDate:{
            type: Date,
            default: new Date()
        },
        /**
        * NGÀY THỰC HIỆN GIAO DỊCH BỎ SUNG
        */ 
        transactionDateAdditional: {
            type: Date,
            default: Date.now()
        },

        /**
         * Ghi chú
         */
        note: String,
          /**
         * Ghi chú không duyệt chuộc đồ
         */
        noteNotApproved: String,
        /**
         * Trạng thái
         * 0: Đang xử lý - chờ duyệt
         * 1: Đã duyệt
         * 2: Không duyệt
         *   - chỉ có type chuộc đồ
        *  3: Đang xử lý - chờ duyệt
            VỚI GiAO DỊCH CẦN DUYỆT
        `       - GIAO DỊCH TRẢ LÃI TRÒN KỲ
        `       - GIAO DỊCH TRẢ LÃI KHÔNG TRÒN KỲ, CHỐT KHÔNG PHJẢI NGÀY GIAO DỊCH
        `       - GIAO DỊCH VAY THÊM
        `       - GIAO DỊCH TRẢ BỚT
         */ 
        status: {
            type: Number,
            default: 0
        },

        meta: {
            // Số tiền vay trước khi thay đổi
            loanAmount: Number,
            loanAmountAfterChange: Number,
            /**
             * Chu kỳ tính lãi(GD Bình Thường:)
             */
            cycleInterest: Number,
            /**
             * Số tiền phải trả ban đầu(GD Bình Thường:)
             */
            priceMuchPayInterestOriginal: Number,
            /**
             * Số tiền phải trả(GD Bình Thường:)
             */
            priceMuchPayInterest: Number,
            /**
             * Số tiền phải trả(GD Bình Thường:)
             */
            promotionInterest: Number,

            /**
             * Số ngày trả trễ(GD Bình Thường:)
             */ 
            numberDatePayLate: Number,

            /**
             * % trả (GD Bình Thường:)
             */ 
            percentDatePayLate: Number,

            expireTimeLate: Date,
             /**
             * ======================================================
             * ======================GD BÁO MĂT=====================
             * ======================================================
             /**

            /**
             * Phí cấp lại(GD Báo Mất:)
             */
            reissueFee: Number,
             /**
             * Yêu cầu in lần thứ(GD Báo Mất:)
             */
            requestPrintTh: Number,

             /**
             * ======================================================
             * ======================GD BÁO MĂT=====================
             * ======================================================
             /**

            /**
             * ======================================================
             * ======================GD VAY THÊM=====================
             * ======================================================
             /**
             * có thể vay thêm(GD GH VAY THÊM:)
             */
            canBorrowMore: Number,
            /**
             * Số tiền muốn vay thêm(GD GH VAY THÊM:)
             */
            amountWantBorrowMore: Number,

             /**
             * Tổng tiền phải trả vay thêm(GD GH VAY THÊM:) = số tiền muốn vay - số tiền lãi
             * 
             */
            totalAmountWantBorrowMore: Number,

            /**
             * ======================================================
             * ====================== GD VAY THÊM=====================
             * ======================================================
             /**
            /**
             * Số tiền đã trả trước (GD GH VAY THÊM/GD GH TRẢ BỚT)
             */
            pricePayInterest: Number,

            /**
             * Số ngày trả lãi (GD GH VAY THÊM/GD GH TRẢ BỚT)
             */
            numberDatePayInterest: Number,
            /**
             * ======================================================
             * ======================GD TRẢ BỚT=====================
             * ======================================================
            /**
             * Trả bớt tối thiều( GD GH TRẢ BỚT)
             */
            paybackMin: Number,
            /**
             * Số tiền muốn trả bớt( GD GH TRẢ BỚT)
             */
            amountWantPayback: Number,
            /**
             * Tổng tiền phải trả trả bớt(GD GH TRẢ BỚT:) = số tiền trả bớt + số tiền lãi
             * 
             */
            totalAmountWantPayBack: Number,
            /**
             * ======================================================
             * ======================GD TRẢ BỚT=====================
             * ======================================================
            /**
            /**
             * ======================================================
             * ======================GD CHUỘC ĐỒ=====================
             * ======================================================
            /**
            /**
             * Số tiền chuộc đồ ban đầu(GD chuộc đồ:)
             */
             priceRansomPayOriginal: Number,
            /**
             * Số tiền chuộc đồ(GD chuộc đồ:)
             */
             priceRansomPay: Number,

            //================LOẠI QUỸ===============
            statusFinancial: Number,

            note: String
        },
         /**
         * User duyệt giao dịch
         */
        userConfirm: {
            type: Schema.Types.ObjectId,
            ref: "user",
        },
         /**
         * User tạo giao dịch
         */
        userCreateAt: {
            type: Schema.Types.ObjectId,
            ref: "user",
        },
        /**
         * Trạng thái duyệt chuộc đồ
         * 0: Không phải giao dịch chuộc đồ
         * 1: Đã duyệt chuộc
         * 2: Chưa duyệt chuộc
         * 3: Không duyệt
         */
         ransom: {
            type: Number,
            default: 0
        },

        ...fields
    });
}
