"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION LỊCH SỬ GIAO DỊCH CỦA HỆ THỐNG
 * CHỈ ÁP DỤNG CHO 3 LOẠI GIAO DỊCH
 * VAY THÊM
 * TRẢ BỚT
 * GIA HẠN
 */
module.exports = function({ fields = {}, name = 'history_transaction' }){
   return BASE_COLl(name, {
     
        /**
         *  Loại giao dịch
         */
        type: {
            type: Number,
            default: 1
        },
        /**
         * Nhân viên giao dịch
         */
        employee: {
            type: Schema.Types.ObjectId,
            ref: "user"
        },
        /**
         * Lãi + phí
         * Số tiền trước khi reset cho Chu Kỳ TRƯỚC
         */
        interestRateTotal: Number,
        /**
         * Ngày Tính Lãi Trong Giao Dịch Chính
         */
        interestStartDate: {
            type: Date
        },
        /**
         * Ngày Reset Lãi (Ngày hiện tại thực hiện giao dịch bổ sung)
         */
        resetDate: {
            type: Date,
            default: Date.now()
        },
        /**
         * Số ngày tính lãi
         */
        interstAmountDate: Number,
        /**
         * % Lãi được tính theo số ngày dựa vào Module Lãi
         */
        interestPercent: Number,
        ...fields
    });
}
