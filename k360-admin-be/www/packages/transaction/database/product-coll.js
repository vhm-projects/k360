"use strict";
const BASE_COLl  = require('../../../databases/intalize/base-coll');
const Schema     = require('mongoose').Schema;

/**
 * COLLECTION SẢN PHẨM GIAO DỊCH CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'product' }){
   return BASE_COLl(name, {
        /**
         *  Tên sản phẩm
         */
        name: String, 
        /**
         * Ghi chú
         */
        note: String,
        /**
         *  Loại tài sản
         */
        type: {
            type: Schema.Types.ObjectId,
            ref: "product_category"
        },

        /**
         *  Danh sách danh mục đã chọn
         */
        lastChilds: [{
            type: Schema.Types.ObjectId,
            ref: "product_category"
        }],

        /**
         *  Đơn giá
         */
        price: {
            type: Number,
            default: 0
        },
        /**
         *  Đơn giá Chỉ áp cho GIÁ HÓA ĐƠN
         */
         priceHaveBill: {
            type: Number,
            default: 0
        },

        meta: {
            typeName: String, // Tên Đơn vị tính
            /**
             * 1: Chỉ => Trọng lượng
             * 2: Chỉ => Có hóa đơn
             * 3: Kích thước => Có hóa đơn
             * 4: Các loại danh mục còn lại =>  Cái
             * 
             */
            typeProductCategory: Number, // Loại đơn vị tính

            size: Number,
            weight: Number,
            have_bill: Number,
        },
        /**
         *  Giao dịch nào
         */
        transaction: {
            type: Schema.Types.ObjectId,
            ref: "transaction"
        },
        ...fields
    });
}
