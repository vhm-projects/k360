"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
 const moment                        = require("moment");
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */
//  const FINANCIAL_MODEL                  = require('../../financial/models/financial').MODEL;
 const INJECT_TRANSACTION_MODEL         = require('./inject_transaction').MODEL;
/**
 * COLLECTIONS
 */
const TRANSACTION_COLL                = require('../database/transaction-coll')({});
const CUSTOMER_COLL                   = require('../../customer/database/customer-coll')({});
const CUSTOMER_AGENCY_COLL            = require('../../customer/database/customer_agency-coll')({});
const PRODUCT_COLL                    = require('../database/product-coll')({});
const PRODUCT_CATEGORY_COLL           = require('../../product_category/databases/product-category-coll')({});
const AGENCY_COLL                     = require('../../agency/databases/agency-coll')({});
const PRODUCT_MODEL                   = require('../models/product').MODEL;
const INJECT_TRANSACTION_COLL         = require('../database/inject_transaction-coll')({});
 const FINANCIAL_COLL                 = require('../../financial/database/financial-coll')({});
 const COMMON_MODEL                                   = require('../../common/models/common').MODEL;

const { checkObjectIDs }            = require('../../../utils/utils');

class Model extends BaseModel {
    constructor() {
        super(TRANSACTION_COLL);
    }

    /**
     * Chi tiết giao dịch
     * Dattv
     */
    getInfo({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.findById(transactionID).populate({
                    path: "products injectTransaction"
                });
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoTransaction({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAfterUpdate = await TRANSACTION_COLL.findById(transactionID).populate({
                    path: "injectTransaction latestInjectTransaction customer customerImages KYCImages receiptImages formVerificationImages billImages"
                })
                // .populate({
                //     path : "injectTransaction",
                //     populate: {
                //         path: "customerImages KYCImages receiptImages formVerificationImages",
                //         select: "_id name path"
                //     }
                // })
                .populate({
                    path : "products",
                    // select: "_id name price note",
                    populate: {
                        path: "type",
                        select: "_id name"
                    }
                })
                .populate({
                    path : "qrCode",
                    select: "_id image",
                    populate: {
                        path: "image",
                        select: "_id path"
                    }
                });

                let listInjectTransactionOfTransactionID = await INJECT_TRANSACTION_COLL.find({ transaction: transactionID })
                    .populate({
                        path: "customerImages KYCImages receiptImages formVerificationImages "
                    })
                    .sort({ createAt: -1 });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoAfterUpdate, listInjectTransactionOfTransactionID });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách Agency
     * Dattv
     */
    getListAgency() {
        return new Promise(async resolve => {
            try {
				
                let listAgency = await AGENCY_COLL.find({ status: 1 });

                if(!listAgency) 
                    return resolve({ error: true, message: "cannot_get_list_agency" });

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * FUNCTION: DOANH SỐ BÁN HÀNG
     */
     getListTransactionRevene({ agencyID, typeProductCategoryID, start, end, name, city, district, ward, status }) {
        return new Promise(async resolve => {
            try {
                let conditionAgency = { };
                let condition = { };

                if(city && city != "null"){
                    conditionAgency.city = city;
                }
 
                if(district && district != "null"){
                    conditionAgency.district = district;
                }
 
                if(ward && ward != "null"){
                    conditionAgency.ward = ward;
                }

                if (city || district || ward) {
                    let listAgencyByAddress = await AGENCY_COLL.find({ ...conditionAgency, status: 1 });
                    condition = {
                        ...condition,
                        agency: {
                            $in: [...listAgencyByAddress.map(agency => agency._id)]
                        }
                    }
                }

                if (agencyID) {
                    if (!checkObjectIDs(agencyID)) {
                        return res.json({ error: true, message: "params_invalid" });
                    }
                    condition = {
                        ...condition,
                        agency: agencyID
                    }
                }
                 
                 
                if (typeProductCategoryID && checkObjectIDs(typeProductCategoryID)) {

                    let listProductWithType = await PRODUCT_MODEL.getListProductWithType({ typeProductCategoryID });
                    
                    let arrProduct = listProductWithType && listProductWithType.data && listProductWithType.data.length
                        && listProductWithType.data.map(item => {
                            return item._id
                        });
                    if(arrProduct.length){
                        condition.products = { $in: arrProduct }
                    }
                 }
                 
                /**
                 * Trạng thái
                 * 0: Đang xử lý - chờ duyệt
                 * 1: Đang cầm - Đã duyệt
                 * 2: Không duyệt
                 * 3: Hoàn thành giao dịch
                 */ 
                const SORT_VALUE_VALID = [0, 1, 2, 3];
                if (status && !Number.isNaN(Number(status)) && SORT_VALUE_VALID.includes(Number(status))) {
                    condition.status = status;
                }
 
                if(start){
                    let _fromDate   = moment(start).startOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $gte: new Date(_fromDate),
                    }
                 }

                 if(end){
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $lt: new Date(_toDate)
                    }
                }

                /**
                 * 1//  DANH SÁCH GIAO DỊCH
                 */
                let listTransaction = await TRANSACTION_COLL.find({ ...condition }, 
                    {
                        loanAmount: 1, 
                        status: 1, 
                        _id: 1, 
                        totalMoneyNumber: 1, 
                        createAt: 1, 
                        agency: 1,
                        note: 1

                    })
                    .sort({ createAt: -1 });
                /**
                 * 2// LẤY RA THÔNG TIN CỦA TỪNG GIAO DỊCH
                 *      *1: Forr mảng danh sách
                 *      *2: TÌm ID Transaction trong QUỸ
                 *      *3: 
                 *          => CỘNG SỐ TIỀN VAY THÊM
                 *          => CỘNG SỐ TIỀN TRẢ BỚT
                 *          => CỘNG SỐ TIỀN TRẢ LÃI
                 *          => CỘNG SỐ TIỀN CHUỘC ĐỒ
                 */

                /**
                 * CONSTANT INJECT TRANSACTION :
                 *      1// TYPE_TRANSACTION_NORMMAL:     2 (GIA HẠN BÌNH THƯỜNG)
                 *      2// TYPE_TRANSACTION_BORROW:      3 (GIA HẠN VAY THÊM)
                 *      3// TYPE_TRANSACTION_PAY:         4 (GIA HẠN TRẢ BỚT)
                 *      4// TYPE_TRANSACTION_RANSOM:      6 (GIA HẠN CHUỘC ĐỒ)
                 */
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                let listTransactionAfterChange = [];

                /**
                 *  1// TỔNG TIỀN VAY THÊM (calculPriceBorrow)
                 *  2// TỔNG TIỀN TRẢ BỚT  (calculPricePay)
                 *  3// TỔNG TIỀN TRÃ LÃI  (calculPriceNormal)
                 *  4// TỔNG TIỀN CHUỘC ĐỒ (calculPriceRansom)
                 *  5// TỔNG TIỀN CẦM ĐỒ   (calculPricePawn)
                 */
                let calculPriceBorrow  = 0;
                let calculPricePay     = 0;
                let calculPriceNormal  = 0;
                let calculPriceRansom  = 0;
                let calculPricePawn    = 0;
                let interestAndFeeAllInjectTransaction = 0;
                let totalInterestAllTransaction = 0;
                for (let transaction of listTransaction) {
                    calculPricePawn += transaction.totalMoneyNumber;
                    let { data: typeTransactionBorrow } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_BORROW,
                    });
                    calculPriceBorrow += typeTransactionBorrow.totalPrice;

                    let { data: typeTransactionPay } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_PAY,
                    });
                    calculPricePay += typeTransactionPay.totalPrice;

                    let { data: typeTransactionNormal } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_NORMMAL,
                    });
                    calculPriceNormal += typeTransactionNormal.totalPrice;

                    let { data: typeTransactionRansom } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_RANSOM,
                    });
                    calculPriceRansom += typeTransactionRansom.totalPrice;

                    interestAndFeeAllInjectTransaction = typeTransactionBorrow.totalInterestAndFee + typeTransactionPay.totalInterestAndFee + typeTransactionNormal.totalInterestAndFee + typeTransactionRansom.totalInterestAndFee;
                    totalInterestAllTransaction += interestAndFeeAllInjectTransaction;

                    let obj = {
                        ...transaction._doc,
                        totalPriceBorrow: typeTransactionBorrow.totalPrice,
                        totalPricePay: typeTransactionPay.totalPrice,
                        totalPriceNormal: typeTransactionNormal.totalPrice,
                        totalPriceRansom: typeTransactionRansom.totalPrice,
                        interestAndFeeAllInjectTransaction
                    }
                    /**
                     * LẤY AGENCY CỦA TRANSACTION
                     */
                    let agencyOfTransaction = await AGENCY_COLL.findById(transaction.agency, { name: 1 }).populate({
                        path: "parent",
                        select: "_id name"
                    });
                    if (agencyOfTransaction) {
                        obj = {
                            ...obj,
                            name: agencyOfTransaction.name
                        }
                        if (agencyOfTransaction.parent) {
                            obj = {
                                ...obj,
                                nameParent: agencyOfTransaction.parent.name
                            }
                        }
                    }

                    /**
                     * LẤY THÔNG TIN QUỸ
                     */
                    let infoFinancial = await FINANCIAL_COLL.find({ transaction: transaction._id, onModel: "transaction" }, { status: 1 });
                    
                    let nameFinancial = "";
                    if (infoFinancial.length) {
                        nameFinancial = infoFinancial[0].status == 1 ? "Quỹ tiền mặt" : "Quỹ Ngân Hàng";
                    }
                    listTransactionAfterChange.push({
                        ...obj, nameFinancial
                    });
                }
                // console.log({ listTransactionAfterChange });
                
                if(!listTransactionAfterChange) 
                    return resolve({ error: true, message: "cannot_get_list_transaction" });

                return resolve({ error: false, data: { listTransaction: listTransactionAfterChange, calculPriceBorrow, calculPricePay, calculPriceNormal, calculPriceRansom, calculPricePawn, totalInterestAllTransaction} });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListTransactionReveneExportExcel({ agencyID, typeProductCategoryID, start, end, name, city, district, ward, status, agencyFilter }) {
        return new Promise(async resolve => {
            try {
                let conditionAgency = { };
                let condition = { };
                
                if(city && city != "null"){
                    conditionAgency.city = city;
                }
 
                if(district && district != "null"){
                    conditionAgency.district = district;
                }
 
                if(ward && ward != "null"){
                    conditionAgency.ward = ward;
                }

                if (city || district || ward) {
                    let listAgencyByAddress = await AGENCY_COLL.find({ ...conditionAgency, status: 1 });
                    condition = {
                        ...condition,
                        agency: {
                            $in: [...listAgencyByAddress.map(agency => agency._id)]
                        }
                    }
                }

                if (agencyID) {
                    if (!checkObjectIDs(agencyID)) {
                        return res.json({ error: true, message: "params_invalid" });
                    }
                    condition = {
                        ...condition,
                        agency: agencyID
                    }
                }
                 
                 
                if (typeProductCategoryID && checkObjectIDs(typeProductCategoryID)) {
                    let listProductWithType = await PRODUCT_MODEL.getListProductWithType({ typeProductCategoryID });
                    
                    let arrProduct = listProductWithType && listProductWithType.data && listProductWithType.data.length
                        && listProductWithType.data.map(item => {
                            return item._id
                        });
                    if(arrProduct.length){
                        condition.products = { $in: arrProduct }
                    }

                    condition.category = typeProductCategoryID;
                 }
                 
                /**
                 * Trạng thái
                 * 0: Đang xử lý - chờ duyệt
                 * 1: Đang cầm - Đã duyệt
                 * 2: Không duyệt
                 * 3: Hoàn thành giao dịch
                 */ 
                const SORT_VALUE_VALID = [0, 1, 2, 3];
                if (status && !Number.isNaN(Number(status)) && SORT_VALUE_VALID.includes(Number(status))) {
                    condition.status = status;
                }
 
                if(start){
                    let _fromDate   = moment(start).startOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $gte: new Date(_fromDate),
                    }
                 }

                 if(end){
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        ...condition.createAt,
                        $lt: new Date(_toDate)
                    }
                }
                
                // if(agencyFilter)
                //     condition.agency = agencyFilter;
                /**
                 * 1//  DANH SÁCH GIAO DỊCH
                 */


                let listTransaction = await TRANSACTION_COLL.find({ ...condition }, {
                    loanAmount: 1, 
                    status: 1, 
                    _id: 1, 
                    totalMoneyNumber: 1, 
                    createAt: 1, 
                    agency: 1,
                    approvalNote: 1,
                    latestInjectTransaction: 1,
                    customer: 1,
                    products: 1,
                    code: 1,
                    approvalLimit: 1,
                    transactionEmployee: 1,
                    injectTransaction: 1,
                    noteNotApproved: 1,
                })
                .populate({
                    path: 'customer',
                    select: 'code name phone address city district ward'
                })
                .populate({
                    path: 'products',
                    select: 'type name ',
                    populate: {
                        path: 'type',
                        select: 'name '
                    }
                })
                .populate({
                    path: 'latestInjectTransaction',
                    select: 'type meta',
                })
                .populate({
                    path: 'injectTransaction',
                    select: 'type meta status ransom userConfirm userCreateAt note noteNotApproved createAt',
                    populate: {
                        path: 'userConfirm userCreateAt',
                        select: 'username'
                    }
                })
                .populate({
                    path: 'agency',
                    select: 'code name phone address email city district ward',
                    populate: {
                        path: 'owners parent',
                        select: 'username email code'
                    }
                })
                .populate({
                    path: 'transactionEmployee',
                    select: 'username',
                })
                .populate({
                    path: 'ownerConfirm',
                    select: 'username',
                })
                .sort({ modifyAt: -1 });
               
                /**
                 * 2// LẤY RA THÔNG TIN CỦA TỪNG GIAO DỊCH
                 *      *1: Forr mảng danh sách
                 *      *2: TÌm ID Transaction trong QUỸ
                 *      *3: 
                 *          => CỘNG SỐ TIỀN VAY THÊM
                 *          => CỘNG SỐ TIỀN TRẢ BỚT
                 *          => CỘNG SỐ TIỀN TRẢ LÃI
                 *          => CỘNG SỐ TIỀN CHUỘC ĐỒ
                 */

                /**
                 * CONSTANT INJECT TRANSACTION :
                 *      1// TYPE_TRANSACTION_NORMMAL:     2 (GIA HẠN BÌNH THƯỜNG)
                 *      2// TYPE_TRANSACTION_BORROW:      3 (GIA HẠN VAY THÊM)
                 *      3// TYPE_TRANSACTION_PAY:         4 (GIA HẠN TRẢ BỚT)
                 *      4// TYPE_TRANSACTION_RANSOM:      6 (GIA HẠN CHUỘC ĐỒ)
                 */
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                let listTransactionAfterChange = [];

                /**
                 *  1// TỔNG TIỀN VAY THÊM (calculPriceBorrow)
                 *  2// TỔNG TIỀN TRẢ BỚT  (calculPricePay)
                 *  3// TỔNG TIỀN TRÃ LÃI  (calculPriceNormal)
                 *  4// TỔNG TIỀN CHUỘC ĐỒ (calculPriceRansom)
                 *  5// TỔNG TIỀN CẦM ĐỒ   (calculPricePawn)
                 */
                let calculPriceBorrow  = 0;
                let calculPricePay     = 0;
                let calculPriceNormal  = 0;
                let calculPriceRansom  = 0;
                let calculPricePawn    = 0;
                let interestAndFeeAllInjectTransaction = 0;
                let totalInterestAllTransaction = 0;
                for (let transaction of listTransaction) {
                    calculPricePawn += transaction.totalMoneyNumber;
                    let { data: typeTransactionBorrow } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_BORROW,
                    });
                    calculPriceBorrow += typeTransactionBorrow.totalPrice;

                    let { data: typeTransactionPay } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_PAY,
                    });
                    calculPricePay += typeTransactionPay.totalPrice;

                    let { data: typeTransactionNormal } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_NORMMAL,
                    });
                    calculPriceNormal += typeTransactionNormal.totalPrice;

                    let { data: typeTransactionRansom } = await INJECT_TRANSACTION_MODEL.calculatePriceTypeTransaction({ 
                        transactionID: transaction._id, 
                        type: TYPE_TRANSACTION_RANSOM,
                    });
                    calculPriceRansom += typeTransactionRansom.totalPrice;

                    interestAndFeeAllInjectTransaction = typeTransactionBorrow.totalInterestAndFee + typeTransactionPay.totalInterestAndFee + typeTransactionNormal.totalInterestAndFee + typeTransactionRansom.totalInterestAndFee;
                    totalInterestAllTransaction += interestAndFeeAllInjectTransaction;

                    let obj = {
                        ...transaction._doc,
                        totalPriceBorrow: typeTransactionBorrow.totalPrice,
                        totalPricePay: typeTransactionPay.totalPrice,
                        totalPriceNormal: typeTransactionNormal.totalPrice,
                        totalPriceRansom: typeTransactionRansom.totalPrice,
                        interestAndFeeAllInjectTransaction
                    }
                    /**
                     * LẤY AGENCY CỦA TRANSACTION
                     */
                    let agencyOfTransaction = await AGENCY_COLL.findById(transaction.agency, { name: 1 }).populate({
                        path: "parent",
                        select: "_id name"
                    });
                    if (agencyOfTransaction) {
                        obj = {
                            ...obj,
                            name: agencyOfTransaction.name
                        }
                        if (agencyOfTransaction.parent) {
                            obj = {
                                ...obj,
                                nameParent: agencyOfTransaction.parent.name
                            }
                        }
                    }

                    /**
                     * LẤY THÔNG TIN QUỸ
                     */
                    let infoFinancial = await FINANCIAL_COLL.find({ transaction: transaction._id, onModel: "transaction" }, { status: 1 });
                    
                    let nameFinancial = "";
                    if (infoFinancial.length) {
                        nameFinancial = infoFinancial[0].status == 1 ? "Quỹ tiền mặt" : "Quỹ Ngân Hàng";
                    }
                    listTransactionAfterChange.push({
                        ...obj, nameFinancial
                    });
                }
                // console.log({ listTransactionAfterChange });

                if(!listTransactionAfterChange) 
                    return resolve({ error: true, message: "cannot_get_list_transaction" });

                return resolve({ error: false, data: { listTransaction: listTransactionAfterChange, calculPriceBorrow, calculPricePay, calculPriceNormal, calculPriceRansom, calculPricePawn, totalInterestAllTransaction} });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách giao dịch 24h của đại lý
     * Dattv
     */
     getListTransactionNear24h({ agencyID }) {
        return new Promise(async resolve => {
            try {
                let conditionObj = {};

                if (ObjectID.isValid(agencyID)){
                    conditionObj.agency = agencyID;
                }

                var d = new Date();
                let month = ``;
                let day = ``;

                if(Number(d.getMonth()+1) < 10){
                    month = `0`
                }

                if(Number(d.getDate()) < 10){
                    day = `0`
                }

                let datestring = d.getFullYear() + "-" + `${month}${(d.getMonth()+1)}` + "-" + `${day}${d.getDate()}`;
                let curentDate = moment(datestring).startOf('day').format();

                let listTransactionNear24h = await TRANSACTION_COLL.find({ 
                    ...conditionObj,
                    createAt: { $gt: new Date(curentDate) } 
                })
                .populate({ path: "agency", select: " " });

                if(!listTransactionNear24h)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransactionNear24h });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách giao dịch của đại lý theo type
     * Dattv
     */
    getListTransactionOfAgencyWithType({ agencyID, typeProductCategoryID, start, end, name, city, district, ward, sort }) {
        return new Promise(async resolve => {
            try {
                let condition = { };

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                } 

                let listAgencyByAddress = await AGENCY_COLL.find({ ...condition, status: 1 });
                let listAgencyIDByAddress = listAgencyByAddress.map(agency => agency._id);

                let listProductWithType = await PRODUCT_MODEL.getListProductWithType({ typeProductCategoryID });

                let arrTransactionID = listProductWithType && listProductWithType.data && listProductWithType.data.length
                    && listProductWithType.data.map(item => {
                        return item.transaction._id
                    });

                let conditionSearch = {};

                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    conditionSearch.name = new RegExp(keyword, 'i');
                }
                
                if(ObjectID.isValid(typeProductCategoryID)){
                    if(arrTransactionID.length){
                        conditionSearch._id = { $in: arrTransactionID }
                    }else{
                        conditionSearch._id = { $in: [] }
                    }
                }

                if(ObjectID.isValid(agencyID)){
                    conditionSearch.agency = agencyID
                }

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionSearch.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let objSort = {};
                switch(sort) {
                    case "1": {
                        objSort.totalMoneyNumber = -1;
                        break;
                    }
                    case "2": {
                        objSort.totalMoneyNumber = 1;
                        break;
                    }
                    default: {
                        objSort.totalMoneyNumber = -1;
                        break;
                    }
                }

                let listTransactionOfAgencyID;
                if(listAgencyIDByAddress && listAgencyIDByAddress.length && !agencyID){
                    listTransactionOfAgencyID = await TRANSACTION_COLL.find({ 
                        ...conditionSearch,
                        agency: { $in: listAgencyIDByAddress }
                    }).sort(objSort)
                    .populate({ path: "agency", select: "name" });
                }else{
                    listTransactionOfAgencyID = await TRANSACTION_COLL.find({ 
                        ...conditionSearch,
                    }).sort(objSort)
                    .populate({ path: "agency", select: "name" });
                }

                if(listAgencyIDByAddress.length == 0){
                    listTransactionOfAgencyID = listAgencyIDByAddress;
                }

                if(!listTransactionOfAgencyID)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransactionOfAgencyID });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Top 5 cửa hàng có doanh thu cao nhất
     * Dattv
     */
     topAgencyHaveBestSale({ limitNumber, start, end }) {
        return new Promise(async resolve => {
            try {

                let condition = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }
                let STATUS_ACCEPT_TRANSACTION = [1, 3];
                let listTopAgencyBestSale = await TRANSACTION_COLL.aggregate([
                    {
                        $match: { ...condition, status: { $in: STATUS_ACCEPT_TRANSACTION} }
                    },
                    {
                        $group : { 
                            _id : "$agency",
                            count: { $sum: 1 },
                            total: { $sum: "$totalMoneyNumber" },
                            agency : { $first: '$agency' }
                        },
                    },
                    {
                        $lookup: {
                            from: "agencies",
                            localField: "agency",
                            foreignField: "_id",
                            as: "agency"
                        }
                    },
                    {
                        $unwind: "$agency"
                    }
                ]).sort({ total: -1 }).limit(limitNumber);

                if(!listTopAgencyBestSale)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: listTopAgencyBestSale });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Top 5 cửa hàng có khách hàng cao nhất
     * Dattv
     */
     topAgencyHaveMostCustomer({ limitNumber, start, end }) {
        return new Promise(async resolve => {
            try {

                let condition = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let listTopAgencyHaveMostCustomer = await CUSTOMER_AGENCY_COLL.aggregate([
                    {
                        $match: condition
                    },
                    {
                        $group : { 
                            _id: {
                                agency: "$agency",
                                customer: "$customer"
                            }
                        },
                    },
                    {
                        $addFields: {
                          agency: "$_id.agency",
                          customer: "$_id.customer",
                       }
                    },
                    {
                        $group : { 
                            _id: {
                                agency: "$agency",
                                
                            },
                            count: { $sum: 1 }
                        },
                    },
                    {
                        $lookup: {
                            from: "agencies",
                            localField: "_id.agency",
                            foreignField: "_id",
                            as: "agency"
                        }
                    },
                    {
                        $unwind: "$agency"
                    }
                ]).sort({ count: -1 }).limit(limitNumber);

                if(!listTopAgencyHaveMostCustomer)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: listTopAgencyHaveMostCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Top 5 danh mục sản phẩm được giao dịch nhiều nhất trên hệ thống //Note: phải check agency đó có hoạt động hay k
     * Dattv
     */
     top5ProductCategoryMostSale({ limit, start, end }) {
        return new Promise(async resolve => {
            try {

                let STATUS_ACCEPT_TRANSACTION = [1, 3];
                let condition = {};
                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lte: new Date(_toDate)
                    }
                }
                // console.log({
                //     ["condition"]: condition
                // });
                let listProductCategory = await TRANSACTION_COLL.aggregate([
                    {
                        $match: {
                            ...condition,
                        }
                    },
                    { 
                        $group: { 
                            _id: "$_id",
                            agency: {$first: "$agency"},
                        }
                    },
                    {
                        $lookup: {
                            from: "agencies",
                            localField: "agency",
                            foreignField: "_id",
                            as: "agency"
                        }
                    },
                    {
                        $unwind: "$agency"
                    },
                    {
                        $lookup: {
                            from: "transactions",
                            localField: "_id",
                            foreignField: "_id",
                            as: "transaction"
                        }
                    },
                    {
                        $unwind: "$transaction"
                    },
                    {
                        $match: { "agency.status": 1, "transaction.status": { $in: STATUS_ACCEPT_TRANSACTION }}
                    },
                    { 
                        $group: { 
                            _id: "$transaction.category",
                            type: {$first: "$transaction.category"},
                            count: { $sum: 1 }
                        }
                    },
                    {
                        $lookup: {
                            from: "product_categories",
                            localField: "type",
                            foreignField: "_id",
                            as: "type"
                        }
                    },
                    {
                        $unwind: "$type"
                    },
                    // {
                    //     $match: { "type.parent": { 
                    //         $ne: null
                    //      }}
                    // },
                ])
                .sort({ count: -1 }).limit(limit);
                // let listProductCategoryID = listProductCategory.map( productCategory => productCategory._id );
                
                // console.log({
                //     ["listProductCategory"]: listProductCategory,

                // });
                if(!listProductCategory)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: listProductCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Top khách hàng có chi thu cao nhất
     * Dattv
     */
     topCustomer({ start, end }) {
        return new Promise(async resolve => {
            try {

                let condition = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }
                let STATUS_ACCEPT_TRANSACTION = [1, 3];
                let listTopCustomer = await TRANSACTION_COLL.aggregate([
                    {
                        $match: {...condition, status: { $in: STATUS_ACCEPT_TRANSACTION }}
                    },
                    {
                        $group : { 
                            _id : "$customer",
                            total: { $sum: "$totalMoneyNumber" },
                            customer : { $first: '$customer' }
                        },
                    },
                    {
                        $lookup: {
                            from: "customers",
                            localField: "_id",
                            foreignField: "_id",
                            as: "customer"
                        }
                    },
                    {
                        $unwind: "$customer"
                    }
                ]).sort({ total: -1 }).limit(5);

                //console.log({ listTopCustomer });

                if(!listTopCustomer)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: listTopCustomer });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chart thể hiển loại hàng kinh doanh biến động theo ngày
     * Dattv
     */
     chartProductCategoryByDay({ typeProductCategory, start, end }) {
        return new Promise(async resolve => {
            try {
                let STATUS_ACCEPT_TRANSACTION = [1, 3];
                let condition = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }
                
                let listProductCategory = await TRANSACTION_COLL.aggregate([
                    //{ $match: { date: { $gte: ISODate("2019-05-01") } } },
                    {
                        $match: { category: ObjectID(typeProductCategory), ...condition, status: { $in: STATUS_ACCEPT_TRANSACTION } }
                    },
                    {
                        $group : { 
                            _id: { 
                                dateCreate: {$dateToString: { format: "%Y-%m-%d", date: "$createAt"} } 
                            },
                            count: { $sum: 1 },
                            type : { $first: '$category' },
                        },
                    },
                    {
                        $lookup: {
                            from: "product_categories",
                            localField: "type",
                            foreignField: "_id",
                            as: "type"
                        }
                    },
                    {
                        $unwind: "$type"
                    }
                ]).sort({ _id: 1 })

                //console.log({ listProductCategory });

                if(!listProductCategory)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: listProductCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chart thể hiển loại hàng kinh doanh biến động theo ngày
     * Dattv
     */
     chartProductCategoryByDayV2({ month, start, end }) {
        return new Promise(async resolve => {
            try {

                let condition = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let result = [];
                let listProductCategory = await PRODUCT_CATEGORY_COLL.find({ parent: null, status: 1 }).lean().select('_id name');
                if(listProductCategory && listProductCategory.length){
                    for (let catetory of listProductCategory) {
                        
                        let amountCategoty = await PRODUCT_COLL.aggregate([
                            {
                                $match: condition
                            },
                            {
                                $project: {
                                    name: 1,
                                    type: 1, 
                                    createAt: 1, 
                                    month: {$month: '$createAt'}
                                }
                            },
                            {
                                $match: { month: month, type: ObjectID(catetory._id) }
                            },
                            {
                                $group : { 
                                    _id: { $dayOfMonth: "$createAt" },
                                    name : { $first: '$name' },
                                    count: { $sum: 1 },
                                    createAt : { $first: '$createAt' }
                                },
                            },
                        ]).sort({ _id: 1, createAt: 1 });
                        result.push({ name: catetory.name, data: amountCategoty });
                    }
                }
                //console.log({ resultXemayV2: result[3].data });
                if(!result)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: result });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chart thể hiển Đại Lý kinh doanh biến động theo ngày v3
     * Dattv
     */
     chartProductCategoryByDayV3({ month, start, end }) {
        return new Promise(async resolve => {
            try {
                let condition = {};
                let STATUS_ACCEPT_TRANSACTION = [1, 3];
                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let result = [];
                let listProductCategory = await PRODUCT_CATEGORY_COLL.find({ parent: null, status: 1 }).lean().select('_id name');
                if(listProductCategory && listProductCategory.length){
                    for (let catetory of listProductCategory) {
                        
                        let amountCategoty = await TRANSACTION_COLL.aggregate([
                            {
                                $match: condition
                            },
                            {
                                $project: {
                                    _id: 1, 
                                    category: 1, 
                                    createAt: 1, 
                                    status:1, 
                                    month: {$month: '$createAt'}
                                }
                            },
                            {
                                $match: { category: ObjectID(catetory._id), status: { $in: STATUS_ACCEPT_TRANSACTION }, month: month }
                            },
                            {
                                $lookup: {
                                    from: "product_categories",
                                    localField: "category",
                                    foreignField: "_id",
                                    as: "category"
                                }
                            },
                            {
                                $unwind: "$category"
                            },
                                
                            {
                                $group : { 
                                    _id: { $dayOfMonth: "$createAt" },
                                    name : { $first: '$category.name' },
                                    count: { $sum: 1 },
                                    createAt : { $first: '$createAt' }
                                },
                            },
                        ]).sort({ _id: 1, createAt: 1 });
                        result.push({ name: catetory.name, data: amountCategoty });
                    }
                }
                //console.log({ resultXemayV3: result[3].data });
                if(!result)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: result });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chart thể hiển Đại Lý kinh doanh biến động theo ngày
     * Dattv
     */
     chartAgencyByDay({ agencyID }) {
        return new Promise(async resolve => {
            try {
                let STATUS_ACCEPT_TRANSACTION = [1, 3]
                let listAgencyByDay = await TRANSACTION_COLL.aggregate([
                    //{ $match: { date: { $gte: ISODate("2019-05-01") } } },
                    {
                        $match: { agency: ObjectID(agencyID), status: { $in: STATUS_ACCEPT_TRANSACTION }}
                    },
                    {
                        $group : { 
                            _id: { 
                                dateCreate: { $dateToString: { format: "%Y-%m-%d", date: "$createAt"} } 
                            },
                            total: { $sum: "$totalMoneyNumber" },
                            agency : { $first: '$agency' },
                        },
                    },
                    {
                        $lookup: {
                            from: "agencies",
                            localField: "agency",
                            foreignField: "_id",
                            as: "agency"
                        }
                    },
                    {
                        $unwind: "$agency"
                    }
                ]).sort({ _id: 1 })

                // console.log({ listAgencyByDay });
                // console.log({ __: listAgencyByDay[0]._id });

                if(!listAgencyByDay)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: listAgencyByDay });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chart thể hiển Đại Lý kinh doanh biến động theo ngày
     * Dattv
     */
     chartFollowAgencySaleDaily({ month, start, end }) {
        return new Promise(async resolve => {
            try {

                let condition = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }
                
                let listTransactionEveryDateInMonth = await TRANSACTION_COLL.aggregate([
                    {
                        $project: {
                            _id: 1, 
                            totalMoneyNumber: 1, 
                            createAt: 1, 
                            month: {$month: '$createAt'}}},
                    {
                        $match: { month: month, ...condition }
                    },
                    {
                        $group : { 
                            _id: { $dayOfMonth: "$createAt" },
                            count: { $sum: 1 },
                            totalMoney: { $sum: "$totalMoneyNumber" },
                            createAt : { $first: '$createAt' }
                        },
                    },
                ]).sort({ createAt: 1 })

                if(!listTransactionEveryDateInMonth)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: listTransactionEveryDateInMonth });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách giao dịch của khách hàng
     */
     getListByCustomer({ customerID, dayFirst, dayLast, status, limit = 10, page = 1, typeTransaction, fromDay, toDay }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

                let data = {
                    customer: customerID
                }

                let arrTransactionID = [];
                let dataFindDateMainTrans = {};
                let dataFindDate = {};
                // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
                let start;
                let end;
                if ( fromDay && !toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    // dataFindDateMainTrans.interestStartDate = { $gte: start };
                    dataFindDate.createAt = { $gte: start }
                }
                if ( !fromDay && toDay ){
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = { $lte: end };
                    dataFindDate.createAt = { $lte: end }
                }
                if ( fromDay && toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = {     
                    //     $gte: start, 
                    //     $lte:  end 
                    // };
                    dataFindDate.createAt = {     
                        $gte: start, 
                        $lte:  end }
                }
                if ( typeTransaction ){
                    if ( typeTransaction == 1){
                        if( dataFindDate.createAt ){
                            dataFilter.interestStartDate = dataFindDate.createAt;
                        }
                        dataFilter.latestInjectTransaction = undefined;
                    }else{
                        dataFindDate.agency = agencyID;
                        dataFindDate.type   = typeTransaction;
                        if ( ransom ){
                            dataFindDate.ransom = ransom
                        }
                        let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate);
                        // console.log({ listInjectTransaction });
                        arrTransactionID = listInjectTransaction.map( item => item._id );
                        arrTransactionID = [...new Set(arrTransactionID)];
                        // console.log({ arrTransactionID });
                        dataFilter.latestInjectTransaction = {
                            $in: arrTransactionID
                        }
                    }
                }else{
                    if( dataFindDate.createAt ){
                        dataFilter.interestStartDate = dataFindDate.createAt;
                    }
                    // dataFilter.latestInjectTransaction = undefined;
                }

                if(status) {
                    data.status = status
                }

                if(dayFirst && dayLast) {
                    data.transactionDate = {
                        $gte: dayFirst, 
                        $lte: dayLast 
                    }
                }
                let infoData = await TRANSACTION_COLL.find(data)
                    // .limit(limit * 1)
                    // .skip((page - 1) * limit)
                    .populate({
                        path: "agency latestInjectTransaction customerImages KYCImages receiptImages formVerificationImages"
                    })
                    .populate({
                        path : "injectTransaction",
                        populate: {
                            path: "customerImages KYCImages receiptImages formVerificationImages",
                            select: "_id name path"
                        }
                    })
                    .populate({
                        path : "products",
                        select: "_id name price",
                        populate: {
                            path: "type",
                            select: "_id name"
                        }
                    })
                    // .populate({
                    //     path : "qrCode",
                    //     select: "_id image",
                    //     populate: {
                    //         path: "image",
                    //         select: "_id path"
                    //     }
                    // })
                    .sort({ interestStartDate: -1 })
                ;
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    // lấy danh sách hết hạn, đến hạn, thanh lý
    getListByCustomerIDCheckDate({ customerID, typeTransaction, typeStatusTransaction, fromDay, toDay, ransom, status }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(customerID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataFilter  = {
                    '$match': {
                      'customer': new ObjectID(customerID)
                    },
                    '$project': {}
                };
                
                let arrTransactionID = [];
                let dataFindDateMainTrans = {};
                let dataFindDate = {};
                // console.log({ agencyID, typeTransaction, typeStatusTransaction, fromDay, toDay  });
                let start;
                let end;

                // let nowDate = new Date();
                // subStractDate({ date1: nowDate,  })

                if ( fromDay && !toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    // dataFindDateMainTrans.interestStartDate = { $gte: start };
                    dataFindDate.createAt = { $gte: start }
                }
                if ( !fromDay && toDay ){
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = { $lte: end };
                    dataFindDate.createAt = { $lte: end }
                }
                if ( fromDay && toDay ){
                    start = new Date(fromDay);
                    start.setHours(0,0,0,0);
                    end = new Date(toDay);
                    end.setHours(23,59,59,999);
                    // dataFindDateMainTrans.interestStartDate = {     
                    //     $gte: start, 
                    //     $lte:  end 
                    // };
                    dataFindDate.createAt = {     
                        $gte: start, 
                        $lte:  end }
                }
                if ( typeTransaction ){
                    if ( typeTransaction == 1){
                        if( dataFindDate.createAt ){
                            // dataFilter.createAt = dataFindDate.createAt;
                            dataFilter['$match'].interestStartDate = dataFindDate.createAt;
                        }
                        dataFilter['$match'].latestInjectTransaction = undefined;
                    }else{
                        dataFindDate.agency = agencyID;
                        dataFindDate.type   = typeTransaction;
                        if ( ransom ){
                            dataFindDate.ransom = ransom
                        }
                        let listInjectTransaction = await INJECT_TRANSACTION_COLL.find(dataFindDate);
                        // console.log({ listInjectTransaction });
                        arrTransactionID = listInjectTransaction.map( item => ObjectID(item._id));
                        arrTransactionID = ["$latestInjectTransaction", [...new Set(arrTransactionID)]];
                        dataFilter['$project'].checkLastInjectTransaction = {
                            $in: arrTransactionID
                        }
                    }
                }else{
                    if( dataFindDate.createAt ){
                        dataFilter['$match'].interestStartDate = dataFindDate.createAt;
                    }
                }

                // if(typeStatusTransaction ){
                //     dataFilter['$match'].status = Number(typeStatusTransaction);
                // }
                if ( status ){
                    dataFilter['$match'].status = Number(status);
                }
                dataFilter['$project'] = {
                    ...dataFilter['$project'],
                    'customer': 1, 
                    'transactionDate': 1, 
                    'expectedDate': 1, 
                    'approvalLimit': 1, 
                    'approvalNote': 1, 
                    'code': 1, 
                    'loanAmount': 1, 
                    'code': 1, 
                    'status': 1, 
                    'expireTime': 1, 
                    'agency': 1, 
                    'latestInjectTransaction': 1,
                    'injectTransaction': 1,
                    'typeInventory': 1,
                    'createAt': 1,
                    'interestStartDate': 1, 
                    'dateDifference': {
                        '$divide': [
                        {
                            '$subtract': [
                                '$expireTime', new Date()
                            ]
                        }, 1000 * 60 * 60 * 24
                        ]
                    }
                }
                // console.log({ dataFilter });
                // console.log({latestInjectTransaction:  dataFilter['$project'].latestInjectTransaction });
                let listTransaction = await TRANSACTION_COLL.aggregate([
                    {
                        '$match':{
                            ...dataFilter['$match']
                        }
                    },
                    {
                        '$project':{
                            ...dataFilter['$project']
                        }
                    },
                    {
                      '$lookup': {
                        'from': 'customers', 
                        'localField': 'customer', 
                        'foreignField': '_id', 
                        'as': 'customer'
                      }
                    }
                    // , {
                    //   '$project': {
                    //     'customer': 1, 
                    //     'loanAmount': 1, 
                    //     'code': 1, 
                    //     'status': 1, 
                    //     'expireTime': 1, 
                    //     'interestStartDate': 1, 
                    //     'latestInjectTransaction': 1, 
                    //     'dateDifference': {
                    //       '$divide': [
                    //         {
                    //           '$subtract': [
                    //             '$expireTime', new Date()
                    //           ]
                    //         }, 1000 * 60 * 60 * 24
                    //       ]
                    //     }
                    //   }
                    // }
                    , {
                      '$lookup': {
                        'from': 'inject_transactions', 
                        'localField': 'latestInjectTransaction', 
                        'foreignField': '_id', 
                        'as': 'latestInjectTransaction'
                      }
                    }, {
                        '$lookup': {
                          'from': 'agencies', 
                          'localField': 'agency', 
                          'foreignField': '_id', 
                          'as': 'agency'
                        }
                      }
                ])

                if(!listTransaction)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listTransaction });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getDataToExportExcel({
        infoTransaction
    }) {
        return new Promise(async resolve => {
            try {
                let data = {}
                let addressAgency = '';
                
                if (infoTransaction.agency) {
                    let infoProvinceAgency   = COMMON_MODEL.getInfoProvince({ provinceCode: infoTransaction.agency.city }); 
                    
                    let infoDistrictAgency   = COMMON_MODEL.getInfoDistrict({ districtCode: infoTransaction.agency.district })
                    let infoWardAgency       = await COMMON_MODEL.getInfoWard({ district: infoTransaction.agency.district, wardCode: infoTransaction.agency.ward })

                    let addressName = '';
                    if (infoProvinceAgency.data.length) {
                        addressName   = infoTransaction.agency.address;
                    } 

                    let cityName = '';
                    if (infoProvinceAgency.data.length) {
                        cityName   = `, ${infoProvinceAgency.data[1].name_with_type}`;
                    } 

                    let districtName = '';
                    if (infoDistrictAgency.data.length) {
                        districtName   = `, ${infoDistrictAgency.data[1].name_with_type}`;
                    } 

                    if (infoWardAgency.data.length) {
                        addressAgency   = addressName + `, ${infoWardAgency.data[1].name_with_type}` + districtName + cityName; 
                    } else {
                        addressAgency   = addressName + districtName + cityName; 
                    }
                }
                
                let address = infoTransaction.customer.address ? infoTransaction.customer.address + ', ' : '';
                if (infoTransaction.customer.city) {
                    let cityNameCustomer = '';
                    let infoProvinceCustomer = COMMON_MODEL.getInfoProvince({ provinceCode: infoTransaction.customer.city });
                    if (infoProvinceCustomer.data.length) {
                        cityNameCustomer = infoProvinceCustomer.data[1].name_with_type;
                    }

                    if (infoTransaction.customer.district) {
                        let infoDistrictCustomer = COMMON_MODEL.getInfoDistrict({ districtCode: infoTransaction.customer.district });
                        if (infoTransaction.customer.ward) {
                            let infoWardCustomer       = await COMMON_MODEL.getInfoWard({ district: infoTransaction.customer.district, wardCode: infoTransaction.customer.ward });
                            if (infoWardCustomer.data.length) {
                                address = address + `${infoWardCustomer.data[1].name_with_type}, ` + `${infoDistrictCustomer.data[1].name_with_type}, ` + cityNameCustomer;
                            } else {
                                address = address + `${infoDistrictCustomer.data[1].name_with_type}, ` + cityNameCustomer;
                            }
                        } else {
                            address = address + `${infoDistrictCustomer.data[1].name_with_type}, ` + cityNameCustomer;
                        }
                    } else {
                        address = address + cityNameCustomer
                    }
                }
               
                //====================ĐẠI LÝ=======================
                let parentCode = '';
                if (infoTransaction.agency && infoTransaction.agency.parent) {
                    parentCode = infoTransaction.agency.parent.code;
                }
                let codeAgency        = infoTransaction.agency ? infoTransaction.agency.code  : '';
                let nameAgency        = infoTransaction.agency ? infoTransaction.agency.name  : '';
                let phoneAgency       = infoTransaction.agency ? infoTransaction.agency.phone : '';
                // let addressAgency     = infoTransaction.agency.address;
                let emailAgency       = infoTransaction.agency ? infoTransaction.agency.email : '';
                let createAt          = infoTransaction.agency ? infoTransaction.createAt     : '';
                data = {
                    ...data,
                    parentCode,
                    nameAgency,
                    codeAgency,
                    phoneAgency,
                    addressAgency,
                    emailAgency,
                    createAt,
                }
                //====================KHÁCH HÀNG====================
                let codeCustomer      = infoTransaction.customer.code;
                let nameCustomer      = infoTransaction.customer.name;
                let phoneCustomer     = infoTransaction.customer.phone;
                let addressCustomer   = address;
                let listOwner = '';

                infoTransaction.agency && infoTransaction.agency.owners.length && infoTransaction.agency.owners.forEach((owner, index) => {
                    if (index) {
                        listOwner += " - ";
                    }
                    listOwner += owner.username;
                });

                let typeProduct = infoTransaction.products[0].type ? infoTransaction.products[0].type.name : '';

                let productName = '';
                infoTransaction.products.forEach((product, index) => {
                    if (index) {
                        productName += " - ";
                    }
                    productName += product.name;
                })

                let approvalLimit = infoTransaction.approvalLimit;

                let codeTransaction = infoTransaction.code;
                data = {
                    ...data,
                    codeCustomer,
                    nameCustomer,
                    phoneCustomer,
                    addressCustomer,
                    listOwner,
                    codeTransaction,
                    productName,
                    typeProduct,
                    approvalLimit,
                }
                
                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
