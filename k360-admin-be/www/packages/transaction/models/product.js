"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const PRODUCT_COLL     = require('../database/product-coll')({});

class Model extends BaseModel {
    constructor() {
        super(PRODUCT_COLL);
    }

    /**
     * Lấy danh sách tất cả sản phẩm theo type
     * Dattv
     */
     getListProductWithType({ typeProductCategoryID }) {
        return new Promise(async resolve => {
            try {

                let listProductWithType = await PRODUCT_COLL.find({ type: typeProductCategoryID }).populate("transaction");

                if(!listProductWithType)
                    return resolve({ error: true, message: "cannot_get_list" });
                return resolve({ error: false, data: listProductWithType });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
