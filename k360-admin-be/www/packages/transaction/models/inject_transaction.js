"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const INJECT_TRANSACTION_COLL                = require('../database/inject_transaction-coll')({});
const TRANSACTION_COLL                       = require('../database/transaction-coll')({});
const { checkObjectIDs }            = require('../../../utils/utils');

class Model extends BaseModel {
    constructor() {
        super(INJECT_TRANSACTION_COLL);
    }

	insert({ agencyID, title, type, transaction, note, meta, ransom, dataUpdate }) {
        return new Promise(async resolve => {
            try {
                if (!title  || !type || !ObjectID.isValid(transaction) || !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataInsert = { title, type, transaction, agency: agencyID };
                if(note){
                    dataInsert.note = note;
                }
                 // Code sau này lấy theo 3 ký tự đầu của ký hiệu đại lý + random 5 số
                let code = Math.floor(100000 + Math.random() * 900000);
                let checkCode = await INJECT_TRANSACTION_COLL.find({ code: code });
                if(code) {
                    code = Math.floor(100000 + Math.random() * 900000);
                }
                dataInsert.code = code;
                if(meta){
                    if(meta.cycleInterest){
                        dataInsert["meta.cycleInterest"]        = Number(meta.cycleInterest)
                    }

                    if(meta.promotionInterest){
                        dataInsert["meta.promotionInterest"]       = Number(meta.promotionInterest)
                    }

                    if(meta.priceMuchPayInterest || meta.priceMuchPayInterest == 0){
                        dataInsert["meta.priceMuchPayInterest"] = Number(meta.priceMuchPayInterest)
                    }

                    if(meta.priceRansomPay){
                        dataInsert["meta.priceRansomPay"]       = Number(meta.priceRansomPay)
                    }

                    if(meta.cycleInterest){
                        dataInsert["meta.cycleInterest"] = Number(meta.cycleInterest)
                    }
                    
                    if(meta.reissueFee){
                        dataInsert["meta.reissueFee"] = Number(meta.reissueFee)
                    }

                    if(meta.requestPrintTh){
                        dataInsert["meta.requestPrintTh"] = Number(meta.requestPrintTh)
                    }

                    if(meta.canBorrowMore){
                        dataInsert["meta.canBorrowMore"] = meta.canBorrowMore
                    }

                    if(meta.amountWantBorrowMore){
                        dataInsert["meta.amountWantBorrowMore"] = meta.amountWantBorrowMore
                    }

                    if(meta.paybackMin){
                        dataInsert["meta.paybackMin"] = meta.paybackMin
                    }

                    if(meta.amountWantPayback){
                        dataInsert["meta.amountWantPayback"] = meta.amountWantPayback
                    }
                    
                }

                // if(type == 2) {
                //     return resolve({ error: true, message: 'cannot_insert_type_2_have_meta_invalid' });
                // }

                if(type == 3 && !meta.canBorrowMore && !meta.amountWantBorrowMore) {
                    return resolve({ error: true, message: 'cannot_insert_type_3_have_meta_invalid' });
                }
                
                if(type == 4 && !meta.paybackMin && !meta.amountWantPayback) {
                    return resolve({ error: true, message: 'cannot_insert_type_4_have_meta_invalid' });
                }
                if(type == 5 && !meta.reissueFee && !meta.requestPrintTh) {
                    return resolve({ error: true, message: 'cannot_insert_type_5_have_meta_invalid' });
                }
                // if(type == 6) {
                //     return resolve({ error: true, message: 'cannot_insert_type_6_have_meta' });
                // }
                if(type == 6 && ransom) {
                    dataInsert.ransom = ransom;
                }
                if (dataUpdate) {
                    dataInsert = {
                        ...dataInsert,
                        ...dataUpdate
                    }
                }
                // console.log({ dataInsert });
                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                // Cập nhật injectTransaction cho transaction.
                if ( type == 5 ){
                    await TRANSACTION_COLL.findByIdAndUpdate(transaction, {
                        $addToSet: {
                            injectTransaction: infoAfterInsert._id
                        }
                    }, { new: true });
                }else{
                    await TRANSACTION_COLL.findByIdAndUpdate(transaction, {
                        $addToSet: {
                            injectTransaction:       infoAfterInsert._id,
                        },
                        latestInjectTransaction: infoAfterInsert._id,
                    }, { new: true });
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateInjectTransaction({ transactionID, title, type, note, meta, customerImagesInjectIsDelete, KYCImagesInjectIsDelete, receiptImagesInjectIsDelete, formVerificationImagesInjectIsDelete }) {
        return new Promise(async resolve => {
            try {
                if (!title  || !type || !ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataUpdate = { title, type, transactionID , $pullAll: {} };
                if(note){
                    dataUpdate.note = note;
                }
                
                if(meta){
                    if(meta.reissueFee){
                        dataUpdate["meta.reissueFee"] = Number(meta.reissueFee)
                    }
                    if(meta.requestPrintTh){
                        dataUpdate["meta.requestPrintTh"] = Number(meta.requestPrintTh)
                    }
                    if(meta.canBorrowMore){
                        dataUpdate["meta.canBorrowMore"] = meta.canBorrowMore
                    }
                    if(meta.amountWantBorrowMore){
                        dataUpdate["meta.amountWantBorrowMore"] = meta.amountWantBorrowMore
                    }
                    if(meta.paybackMin){
                        dataUpdate["meta.paybackMin"] = meta.paybackMin
                    }
                    if(meta.amountWantPayback){
                        dataUpdate["meta.amountWantPayback"] = meta.amountWantPayback
                    }
                }

                if(customerImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].customerImages = customerImagesInjectIsDelete;
                }

                if(KYCImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].KYCImages = KYCImagesInjectIsDelete;
                }

                if(receiptImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].receiptImages = receiptImagesInjectIsDelete;
                }
                if(formVerificationImagesInjectIsDelete) {
                    dataUpdate['$pullAll'].formVerificationImages = formVerificationImagesInjectIsDelete;
                }
                // console.log({ dataUpdate });
                let infoAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(transactionID, 
                    dataUpdate,
                    {
                        new: true
                    }
                );

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateStatus({ transactionID, status }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID) || ![0, 1, 2].includes(Number(status)))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let infoAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(transactionID, { status }, { new: true });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ transactionID, ransom, noteNotApproved }) {
        return new Promise(async resolve => {
            try {
                // console.log({ transactionID, ransom });
                if (!ObjectID.isValid(transactionID) || ![1, 2, 3].includes(Number(ransom)))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let dataUpdate = { ransom }
                if(noteNotApproved) {
                    dataUpdate.noteNotApproved = noteNotApproved;
                }
                let infoAfterUpdate = await INJECT_TRANSACTION_COLL.findByIdAndUpdate(transactionID, 
                    dataUpdate, 
                    { new: true });
                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ transactionID }) {
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoData = await INJECT_TRANSACTION_COLL.findById(transactionID);
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });
                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculatePriceTransaction({ transactionID, type }) {
        return new Promise(async resolve => {
            try {
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                const SORT_VALUE_VALID            = [TYPE_TRANSACTION_NORMMAL, TYPE_TRANSACTION_BORROW, TYPE_TRANSACTION_PAY, TYPE_TRANSACTION_RANSOM]
                if (!checkObjectIDs(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let dataFind = { transaction: transactionID }
                if (type && !Number.isNaN(Number(type)) && SORT_VALUE_VALID.includes(Number(type))) {
                    dataFind = {
                        ...dataFind,
                        type
                    }
                    /**
                     * KIỂM TRA GIAO DỊCH CHUỘC ĐỒ ĐÃ ĐƯỢC DUYỆT CHƯA
                     */
                    if (type == TYPE_TRANSACTION_RANSOM) {
                        const STATUS_ACTIVE_RANSOM = 1;
                        dataFind = {
                            ...dataFind,
                            ransom: STATUS_ACTIVE_RANSOM
                        }
                    }
                }

                let infoData = await INJECT_TRANSACTION_COLL.find(dataFind, { meta: 1, _id: 1, transaction: 1 }).sort({ createAt: -1 });
                // console.log({ infoData });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });

                /**
                 * TÍNH TỔNG TIỀN CỦA TỪNG LOẠI GIAO DỊCH
                 */
                let totalPrice = 0
                if (infoData && infoData.length) {
                    switch (Number(type)) {
                        /**
                         * GIAO DỊCH BÌNH THƯỜNG (TRẢ LÃI)
                         */
                        case TYPE_TRANSACTION_NORMMAL:
                            infoData.forEach( inject => {
                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalPrice += inject.meta.priceMuchPayInterest;
                            });
                            break;
                        /**
                         * GIAO DỊCH VAY THÊM
                         */
                        case TYPE_TRANSACTION_BORROW:
                            infoData.forEach( inject => {
                                if (inject.meta.amountWantBorrowMore || inject.meta.amountWantBorrowMore == 0)
                                    totalPrice += inject.meta.amountWantBorrowMore;
                            });
                            break;
                        /**
                         * GIAO DỊCH TRẢ BỚT
                         */
                        case TYPE_TRANSACTION_PAY:
                            infoData.forEach( inject => {
                                if (inject.meta.amountWantPayback || inject.meta.amountWantPayback == 0)
                                    totalPrice += inject.meta.amountWantPayback;
                            });
                            break;

                        /**
                         * GIAO DỊCH CHUỘC ĐỒ
                         */
                        case TYPE_TRANSACTION_RANSOM:
                            infoData.forEach( inject => {
                                if (inject.meta.priceRansomPay || inject.meta.priceRansomPay == 0)
                                    totalPrice += inject.meta.priceRansomPay;
                            });
                            break;
                        default:
                            break;
                    }
                }

                return resolve({ error: false, data: totalPrice });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculatePriceTypeTransaction({ transactionID, type }) {
        return new Promise(async resolve => {
            try {
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                if (!checkObjectIDs(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                // điều kiện tìm kiếm với giao dịch bình thường chỉ lấy ra các giao dich có trạng thái đang xử lý - chờ duyệt
                const STATUS_PAY_ON_TIME = 0, STATUS_PAY_LATE = 1;
                
                // điều kiện tìm kiếm với giao dịch vay thêm và trả bớt
                const STATUS_VALID_BORROW_PAY = 1;

                // điều kiện tìm kiếm với các giao dịch là giao dịch chuộc đồ thì chỉ tính đối với các giao dịch chuộc đồ đã duyệt
                const STATUS_ACTIVE_RANSOM = 1;

                let dataFind = {};

                switch(+type) {
                    case TYPE_TRANSACTION_NORMMAL: {
                        dataFind = {
                            type: TYPE_TRANSACTION_NORMMAL, 
                            $or: [{ status: STATUS_PAY_ON_TIME }, { status: STATUS_PAY_LATE }] 
                        }
                        break;
                    }
                    case TYPE_TRANSACTION_BORROW: {
                        dataFind = {
                            type: TYPE_TRANSACTION_BORROW, 
                            status: STATUS_VALID_BORROW_PAY
                        }
                        break;
                    }
                    case TYPE_TRANSACTION_PAY: {
                        dataFind = {
                            type: TYPE_TRANSACTION_PAY, 
                            status: STATUS_VALID_BORROW_PAY
                        }
                        break;
                    }
                    case TYPE_TRANSACTION_RANSOM: {
                        dataFind = {
                            type: TYPE_TRANSACTION_RANSOM, ransom: STATUS_ACTIVE_RANSOM
                        }
                        break;
                    }
                }

                let infoData = await INJECT_TRANSACTION_COLL.find({
                    transaction: transactionID,
                    ...dataFind
                    
                }, { meta: 1, _id: 1, transaction: 1 }).sort({ createAt: -1 });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });

                /**
                 * TÍNH TỔNG TIỀN LÃI CỦA GIAO DỊCH
                 */
                let totalPrice = 0;
                /**
                 * Tổng số tiền lãi và phí của giao dịch
                 */
                let totalInterestAndFee = 0;
                if (infoData && infoData.length) {
                    switch (Number(type)) {
                        /**
                         * GIAO DỊCH BÌNH THƯỜNG (TRẢ LÃI)
                         */
                        case TYPE_TRANSACTION_NORMMAL:
                            infoData.forEach( inject => {
                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalPrice += inject.meta.priceMuchPayInterest;
                            });
                            totalInterestAndFee = totalPrice;
                            break;
                        /**
                         * GIAO DỊCH VAY THÊM
                         */
                        case TYPE_TRANSACTION_BORROW:
                            infoData.forEach( inject => {
                                if (inject.meta.amountWantBorrowMore || inject.meta.amountWantBorrowMore == 0)
                                    totalPrice += inject.meta.amountWantBorrowMore;

                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalInterestAndFee += inject.meta.priceMuchPayInterest;
                            });
                            break;
                        /**
                         * GIAO DỊCH TRẢ BỚT
                         */
                        case TYPE_TRANSACTION_PAY:
                            infoData.forEach( inject => {
                                if (inject.meta.amountWantPayback || inject.meta.amountWantPayback == 0)
                                    totalPrice += inject.meta.amountWantPayback;

                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalInterestAndFee += inject.meta.priceMuchPayInterest;
                            });
                            break;

                        /**
                         * GIAO DỊCH CHUỘC ĐỒ
                         */
                        case TYPE_TRANSACTION_RANSOM:
                            infoData.forEach( inject => {
                                if (inject.meta.priceRansomPayOriginal || inject.meta.priceRansomPayOriginal == 0)
                                    totalPrice += inject.meta.priceRansomPayOriginal;

                                if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                                    totalInterestAndFee += inject.meta.priceMuchPayInterest;
                            });
                            break;
                        default:
                            break;
                    }
                }

                return resolve({ error: false, data: { totalPrice, totalInterestAndFee } });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    calculateTotalInterest({ transactionID }) {
        return new Promise(async resolve => {
            try {
                const TYPE_TRANSACTION_NORMMAL    = 2;
                const TYPE_TRANSACTION_BORROW     = 3;
                const TYPE_TRANSACTION_PAY        = 4;
                const TYPE_TRANSACTION_RANSOM     = 6;
                const TYPE_BORROW_PAY             = [TYPE_TRANSACTION_BORROW, TYPE_TRANSACTION_PAY];
                if (!checkObjectIDs(transactionID))
                    return resolve({ error: true, message: 'params_invalid' });

                // điều kiện tìm kiếm với giao dịch bình thường chỉ lấy ra các giao dich có trạng thái đang xử lý - chờ duyệt
                const STATUS_PAY_ON_TIME = 0, STATUS_PAY_LATE = 1;
                let dataFindTransactionNormal = { 
                    type: TYPE_TRANSACTION_NORMMAL, 
                    $or: [{ status: STATUS_PAY_ON_TIME }, { status: STATUS_PAY_LATE }] 
                };

                // điều kiện tìm kiếm với giao dịch vay thêm và trả bớt
                const STATUS_VALID_BORROW_PAY = 1;
                let dataFindTransactionBorrowPay = { 
                    type: { $in: TYPE_BORROW_PAY }, 
                    status: STATUS_VALID_BORROW_PAY
                };

                // điều kiện tìm kiếm với các giao dịch là giao dịch chuộc đồ thì chỉ tính đối với các giao dịch chuộc đồ đã duyệt
                const STATUS_ACTIVE_RANSOM = 1;
                let dataFindTransactionRansom = {
                    type: TYPE_TRANSACTION_RANSOM, ransom: STATUS_ACTIVE_RANSOM
                }

                let infoData = await INJECT_TRANSACTION_COLL.find({
                    transaction: transactionID,
                    $or: [
                        dataFindTransactionNormal,
                        dataFindTransactionRansom,
                        dataFindTransactionBorrowPay
                    ]
                }, { meta: 1, _id: 1, transaction: 1 }).sort({ createAt: -1 });
                if(!infoData)
                    return resolve({ error: true, message: "cannot_get_info" });

                /**
                 * TÍNH TỔNG TIỀN LÃI CỦA GIAO DỊCH
                 */
                let totalPrice = 0
                if (infoData && infoData.length) {
                    infoData.forEach(inject => {
                        if (inject.meta.priceMuchPayInterest || inject.meta.priceMuchPayInterest == 0)
                            totalPrice += inject.meta.priceMuchPayInterest;
                    });
                }

                return resolve({ error: false, data: totalPrice });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
