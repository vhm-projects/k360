"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION GROUP CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'image' }){
	return BASE_COLL(name, {
        name: {
            type: String,
        },
        path: {
            type: String,
        },
        size: {
            type: String,
        },
        userCreate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
		...fields
    });
}
