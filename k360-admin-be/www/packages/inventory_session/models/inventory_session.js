"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const moment = require("moment");

/**
 * INTERNAL PAKCAGE
 */

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const INVENTORY_SESSION_COLL                = require('../databases/inventory_session-coll')({});
const TRANSACTION_COLL                      = require('../../transaction/database/transaction-coll')({});

class Model extends BaseModel {
    constructor() {
        super(INVENTORY_SESSION_COLL);
    }

    /**
     * Danh sách phiên giao dịch thuộc agency
     * Dev: Dattv
     */
     getListInventorySessionOfAgency({ agencyID, start, end }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let conditionObj = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let listInventorySessionOfAgency = await INVENTORY_SESSION_COLL.find({ ...conditionObj, agency: agencyID })
                .populate({
                    path: "transactions",
                    populate: {
                        path: "customer",
                        select: "name"
                    }
                });

                if(!listInventorySessionOfAgency)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listInventorySessionOfAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách giao dịch đã kiểm kê của mỗi đại lý 
     * Dev: Dattv
     */
     getListTransactionHaveTypeInventoryIsTrueOfEveryAgency({ start, end }){
        return new Promise(async resolve => {
            try {
                
                let conditionObj = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let listData = await TRANSACTION_COLL.aggregate([
                    { 
                        $match: { ...conditionObj, typeInventory: 1 }
                    },
                    {
                        $group : { 
                            _id : "$agency",
                            count: { $sum: 1 },
                        },
                    },
                    {
                        $lookup: {
                            from: "agencies",
                            localField: "_id",
                            foreignField: "_id",
                            as: "agency"
                        }
                    },
                    {
                        $unwind: "$agency"
                    }
                ])

                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách tất cả giao dịch của mỗi đại lý
     * Dev: Dattv
     */
     getAllTransactionOfEveryAgency({ start, end }){
        return new Promise(async resolve => {
            try {
                
                let conditionObj = {};

                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    conditionObj.createAt = {
                        $gte: new Date(_fromDate),
                        $lt: new Date(_toDate)
                    }
                }

                let listData = await TRANSACTION_COLL.aggregate([
                    { 
                        $match: { ...conditionObj }
                    },
                    {
                        $group : { 
                            _id : "$agency",
                            count: { $sum: 1 },
                        },
                    },
                    {
                        $lookup: {
                            from: "agencies",
                            localField: "_id",
                            foreignField: "_id",
                            as: "agency"
                        }
                    },
                    {
                        $unwind: "$agency"
                    }
                ])
                
                if(!listData)
                    return resolve({ error: true, message: 'cannot_get_list' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Chi tiết phiên giao dịch
     * Dev: Dattv
     */
     getInfo({ inventorySessionID }){
        return new Promise(async resolve => {
            try {
                if (!ObjectID.isValid(inventorySessionID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoInventorySession = await INVENTORY_SESSION_COLL.findById(inventorySessionID)
                .populate({
                    path: "transactions",
                    populate: {
                        path: "customer products",
                        select: "name price"
                    }
                })
                
                if(!infoInventorySession)
                return resolve({ error: true, message: 'cannot_get_info' });

                return resolve({ error: false, data: infoInventorySession });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
