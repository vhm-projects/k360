const INVENTORY_SESSION_MODEL 		= require('./models/inventory_session').MODEL;
const INVENTORY_SESSION_COLL  		= require('./databases/inventory_session-coll')({});
const INVENTORY_SESSION_ROUTES      = require('./apis/inventory_session');

module.exports = {
    INVENTORY_SESSION_ROUTES,
    INVENTORY_SESSION_COLL,
    INVENTORY_SESSION_MODEL,
}
