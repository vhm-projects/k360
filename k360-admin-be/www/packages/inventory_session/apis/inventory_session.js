"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs = require('fs');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadFields, uploadCusAndAuthPaper }       = require('../../../config/cf_helpers_multer');
const { CF_ROUTINGS_INVENTORY }                     = require('../constants/inventory_session.uri');

/**
 * MODELS, COLLECTIONS
 */
 const INVENTORY_SESSION_MODEL                       = require('../models/inventory_session').MODEL;
 const AGENCY_MODEL                                  = require('../../agency/models/agency').MODEL;

//const TRANSACTION_COLL                              = require('../database/transaction-coll')({});


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }
    
    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ==========================       API INVENTORY    ================================
             * ========================== ************************ ================================
             */
            
            /**
             * Danh sách các phiên kiểm kê của đại lý
             * Dev: Dattv
             * 08/05/2021
             */
             [CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSION,
					inc: path.resolve(__dirname, '../views/list_inventory_session.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { start, end } = req.query;
                        let listTransactionHaveTypeInventoryIsTrue = await INVENTORY_SESSION_MODEL.getListTransactionHaveTypeInventoryIsTrueOfEveryAgency({ });
                        let countTransactionOfAllAgency = await INVENTORY_SESSION_MODEL.getAllTransactionOfEveryAgency({ });

                        ChildRouter.renderToView(req, res, 
                            { 
                                listTransactionHaveTypeInventoryIsTrue: listTransactionHaveTypeInventoryIsTrue.data,
                                countTransactionOfAllAgency: countTransactionOfAllAgency.data,
                                start, end
                            }
                        );
                    }]
                },
            },

            /**
             * Danh sách các phiên kiểm kê của đại lý
             * Dev: Dattv
             * 08/05/2021
             */
             [CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSION_OF_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSISON_OF_AGENCY,
					inc: path.resolve(__dirname, '../views/list_inventory_session_of_agency.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { agencyID } = req.params;
                        let { start, end } = req.query;
                        let infoAgency = await AGENCY_MODEL.getInfo({ agencyID });
                        let listInventorySessionOfAgency = await INVENTORY_SESSION_MODEL.getListInventorySessionOfAgency({ agencyID, start, end });
                        ChildRouter.renderToView(req, res, 
                            {
                                listInventorySessionOfAgency: listInventorySessionOfAgency.data,
                                infoAgency: infoAgency.data,
                                start, end
                            }
                        );
                    }]
                },
            },

            /**
             * Chi tiết phiên kiểm kê
             * Dev: Dattv
             * 08/05/2021
             */
             [CF_ROUTINGS_INVENTORY.DETAIL_INVENTORY_SESSION]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_INVENTORY.DETAIL_INVENTORY_SESSISON,
					inc: path.resolve(__dirname, '../views/detail_inventory_session.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { inventorySessionID } = req.params;
                        let infoInventorySession = await INVENTORY_SESSION_MODEL.getInfo({ inventorySessionID });
                        ChildRouter.renderToView(req, res, 
                            {
                                infoInventorySession: infoInventorySession.data,
                            }
                        );
                    }]
                },
            },

            [CF_ROUTINGS_INVENTORY.VIEW_LIST_INVENTORY_SESSION_API]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let agencyID = req.agencyID;
                        let { start, end } = req.query;
                        let listInventorySession = await INVENTORY_SESSION_MODEL.getList({ agencyID, start, end });
                        res.json( 
                            { 
                                listInventorySession: listInventorySession.data,
                                start, end
                            }
                        );
                    }]
                },
            },

            
        }
    }
};
