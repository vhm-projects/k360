"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID          = require('mongoose').Types.ObjectId;
const moment            = require("moment");

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');


/**
 * COLLECTIONS
 */
const CONTACT_COLL = require('../databases/contact-coll')({});

class Model extends BaseModel {
    constructor() {
        super(CONTACT_COLL);
    }

    /**
     * Thêm liên hệ
     * Dattv
     */
	insert({ fullname, email, phone, content, type }) {
        return new Promise(async resolve => {
            try {
                if(!fullname || !email || !phone || !content)
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataInsert = {
                    fullname,
                    email,
                    phone,
                    content,
                    type
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'send_contact_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin contactID và cập nhật đã xem
     * Dattv
     */
    getInfo({ contactID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoContact = await CONTACT_COLL.findByIdAndUpdate(contactID, {
                    status: 1
                }, { new: true });
                if(!infoContact)
                    return resolve({ error: true, message: 'contact_is_not_exists' });

                return resolve({ error: false, data: infoContact });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Update liên hệ
     * Dattv
     */
    update({ contactID, fullname, email, phone, content, type }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const dataUpdate = {
                    fullname,
                    email, 
                    phone, 
                    content, 
                    type
                }

                await this.updateWhereClause({ _id: contactID }, {
                    ...dataUpdate
                });

                return resolve({ error: false, data: dataUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Xóa liên hệ
     * Dattv
     */
    remove({ contactID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await CONTACT_COLL.findByIdAndRemove(contactID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'contact_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Bộ lọc danh sách liên hệ
    getList({ status, email, start, end }){
        return new Promise(async resolve => {
            try {
				let condition = {};
				status      && (condition.status    = status);
				email       && (condition.email     = email);
                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = { 
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate) 
                    }
                }

                let listContact = await CONTACT_COLL.find(condition).sort({ createAt: -1 }).lean();
                if(!listContact) 
                    return resolve({ error: true, message: "cannot_get_list_contact" });

                return resolve({ error: false, data: listContact });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    //Bộ lọc danh sách liên hệ
    getListMobile({ status, email, start, end, page = 1 }){
        return new Promise(async resolve => {
            try {
                const limit = 10;
                const STATUS_INACTIVE = 0;

				let condition = {};
				status      && (condition.status    = status);
				email       && (condition.email     = email);
                if(start && end){
                    let _fromDate   = moment(start).startOf('day').format();
                    let _toDate     = moment(end).endOf('day').format();
                    condition.createAt = { 
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate) 
                    }
                }

                let countContact= await CONTACT_COLL.count({ status: STATUS_INACTIVE })
                let listContact = await CONTACT_COLL.find(condition)
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ createAt: -1 })
                    .lean();
                if(!listContact) 
                    return resolve({ error: true, message: "cannot_get_list_contact" });

                return resolve({ error: false, data: listContact, countContact });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }


    /**
     * Lọc danh sách từ ngày đến ngày
     * Dattv
     */
    getListContactBetweenDay({ start, end }){
        return new Promise(async resolve => {
            try {
                let _fromDate   = moment(start).startOf('day').format();
                let _toDate     = moment(end).endOf('day').format();

                const listContact = await CONTACT_COLL.find({ 
                    createAt : { 
                        $gte: new Date(_fromDate), 
                        $lt: new Date(_toDate) 
                    }
                }).lean();
                if(!listContact)
                    return resolve({ error: true, message: 'not_found_contacts_list' });
                return resolve({ error: false, data: listContact });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
