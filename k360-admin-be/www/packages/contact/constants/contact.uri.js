const BASE_ROUTE = '/contact';

const CF_ROUTINGS_CONTACT = {
    VIEW_LIST_CONTACT: `${BASE_ROUTE}`,
    VIEW_DETAIL_CONTACT: `${BASE_ROUTE}/detail/:contactID`,

    ADD_CONTACT: `${BASE_ROUTE}/add-contact`,
    INFO_CONTACT: `${BASE_ROUTE}/info-contact/:contactID`,
    REMOVE_CONTACT: `${BASE_ROUTE}/remove-contact/:contactID`,
    UPDATE_CONTACT: `${BASE_ROUTE}/update-contact/:contactID`,
    FILTER_CONTACT: `${BASE_ROUTE}/filter-contact`,

    ADD_CONTACT_API: `/api${BASE_ROUTE}/add-contact`,
    LIST_CONTACT_API: `/api${BASE_ROUTE}`,

	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CONTACT = CF_ROUTINGS_CONTACT;
