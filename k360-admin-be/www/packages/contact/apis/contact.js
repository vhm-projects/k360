"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_CONTACT }           = require('../constants/contact.uri');

const { TYPE_CONTACT }                  = require('../../../config/cf_constants');


/**
 * MODELS
 */
const CONTACT_MODEL                     = require("../models/contact").MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            
            /**
             * Function: Danh sách liên hệ (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.VIEW_LIST_CONTACT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_CONTACT.VIEW_LIST_CONTACT,
					inc: path.resolve(__dirname, '../views/list_contact.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { start, end, type } = req.query;
                        let listContact = await CONTACT_MODEL.getList({ start, end });

                        if (type && type === "api") {
                            return res.json(listContact);
                        }

                        ChildRouter.renderToView(req, res, {
							listContact: listContact.data,
                            start, end,
                            TYPE_CONTACT
						});
                    }]
                },
            },

            /**
             * Function: Chi tiết liên hệ (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.VIEW_DETAIL_CONTACT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `Detail Agency - K360`,
					code: CF_ROUTINGS_CONTACT.VIEW_DETAIL_CONTACT,
					inc: path.resolve(__dirname, '../views/detail_contact.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { contactID } = req.params;
                        let { type }      = req.query;
                        let infoContact = await CONTACT_MODEL.getInfo({ contactID });
                       
                        if (type && type === "api") {
                            return res.json(infoContact);
                        }
                        ChildRouter.renderToView(req, res, {
                            infoContact: infoContact.data,
                            TYPE_CONTACT
						});
                    }]
                },
            },

            //========================= JSON ============================

            /**
             * Function: Insert Contact
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.ADD_CONTACT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { fullname, email, phone, content, type } = req.body;
                        const infoAfterInsertContact = await CONTACT_MODEL.insert({ 
                            fullname, email, phone, content, type
                        });
                        
                        res.json(infoAfterInsertContact);
                    }]
                },
            },

            [CF_ROUTINGS_CONTACT.ADD_CONTACT_API]: {
                config: {
                    auth: [ roles.role.customer.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { fullname, email, phone, content, type } = req.body;

                        const infoAfterInsertContact = await CONTACT_MODEL.insert({ 
                            fullname, email, phone, content, type
                        });
                        let description = {
                            status: 'Trạng thái (0: Chưa xem/ 1: Đã xem)',
                            type:   'Pending'
                        }
                        res.json({ description, ...infoAfterInsertContact });
                    }]
                },
            },
            
            /**
             * Function: Info Contact
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.INFO_CONTACT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { contactID } = req.params;
                        const infoContact = await CONTACT_MODEL.getInfo({ contactID });
                        res.json(infoContact);
                    }]
                },
            },

            /**
             * Function: Xóa contact
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.REMOVE_CONTACT]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { contactID } = req.params;
                        const infoContactRemove = await CONTACT_MODEL.remove({ contactID });
                        res.json(infoContactRemove);
                    }]
                },
            },

            /**================================== API MOBILE=================================== */
            /**
             * Function: Danh sách liên hệ (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.LIST_CONTACT_API]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { start, end, type, page } = req.query;
                        let listContact = await CONTACT_MODEL.getListMobile({ start, end, page });

                        return res.json(listContact);
                    }]
                },
            },
        }
    }
};
