const BASE_ROUTE_ADMIN = '/agency';
const BASE_ROUTE_API   = '/api/interest-rate';

const CF_ROUTINGS_INTEREST_RATE = {
    // LÃI SUẤT
    ADD_INTEREST_RATE: `/add-interest-rate`,
    UPDATE_INTEREST_RATE: `/update-interest-rate`,
    INFO_INTEREST_RATE: `${BASE_ROUTE_ADMIN}/info-interest-rate/:interestRateID`,
    REMOVE_INTEREST_RATE: `${BASE_ROUTE_ADMIN}/remove-interest-rate/:interestRateID`,
    LIST_INTEREST_RATE: `/list-interest-rate`,

    //API MOBILE
    LIST_INTEREST_RATE_API: `${BASE_ROUTE_API}/list-interest-rate`,
}

exports.CF_ROUTINGS_INTEREST_RATE = CF_ROUTINGS_INTEREST_RATE;
