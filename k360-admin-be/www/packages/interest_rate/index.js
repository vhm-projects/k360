const INTEREST_RATE_MODEL 		= require('./models/interest_rate').MODEL;
const INTEREST_RATE_COLL  		= require('./database/interest_rate-coll')({});
const INTEREST_RATE_ROUTES        = require('./apis/interest_rate');

module.exports = {
    INTEREST_RATE_ROUTES,
    INTEREST_RATE_COLL,
    INTEREST_RATE_MODEL
}
