"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;
const request  = require('request');

/**
 * INTERNAL PAKCAGE
 */
 const jwt                           = require('jsonwebtoken');
 const { hash, hashSync, compare }   = require('bcryptjs');
 let { calculateInteres } = require('../../../utils/utils');
 const { subStractDateGetDay, addNumberToSetDate, minusNumberToSetDate }                       = require('../../../config/cf_time');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * MODELS
 */

/**
 * COLLECTIONS
 */
const  INTEREST_RATE_COLL           = require('../database/interest_rate-coll')({});
// const  PRODUCT_CATEGORY_COLL        = require('../../product_category/databases/product-category-coll')({});
const  PRODUCT_CATEGORY_COLL        = require('../../agency_product_category/databases/agency-product-category-coll')({});

class Model extends BaseModel {
    constructor() {
        super(INTEREST_RATE_COLL);
    }

	insert({ name, agencyID, productID, from, to , percent }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID) || !ObjectID.isValid(productID))
                    return resolve({ error: true, message: 'params_invalid' });
                
                let checkData = await INTEREST_RATE_COLL.find({  agency: agencyID, product: productID }).sort({ to: -1 });
               
                let maxTo = 0;
                if(checkData.length){
                    maxTo = checkData[0].to;
                }

                if(from != maxTo + 1 )
                    return resolve({ error: true, message: `from_invalid`, from: maxTo + 1 });

                if(Number(from) >= Number(to))
                    return resolve({ error: true, message: 'to_invalid' });

                let dataInsert = {
                    name, agency: agencyID, product: productID, from, to, percent
                };

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ interestRate, name , percent }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(interestRate))
                    return resolve({ error: true, message: 'params_invalid' });
                
                if(percent < 0 || percent > 100)
                    return resolve({ error: true, message: 'Phần trăm lãi không hợp lệ' });

                let infoAfterUpdate = await INTEREST_RATE_COLL.findByIdAndUpdate(interestRate, { name , percent }, { new: true });

                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'update_failed' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }


    getList({ agencyID, product, type, percent }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID)){
                    return resolve({ error: true, message:"param_invalid" });
                }
                let objSearch = { agency: agencyID };
                if(ObjectID.isValid(product)){
                    objSearch.product = product;
                }

                if(type){
                    objSearch.typeTransaction = type;
                }

                if(percent){
                    objSearch.percent = percent;
                }

                let listData = await INTEREST_RATE_COLL.find(objSearch).populate("agency product");
                if(!listData)
                    return resolve({ error: true, message: 'get_list_faild' });

                return resolve({ error: false, data: listData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ agencyID, limitDate, typeProduct }) {
        return new Promise(async resolve => {
            try {
                let objSearch = { agency: agencyID };


                if(limitDate){
                    limitDate = Math.floor(limitDate);
                    objSearch.to = {
                        $gte: limitDate, 
                    };
                    objSearch.from = {
                        $lte: limitDate, 
                    };
                }
                if(typeProduct) {
                    objSearch.product = typeProduct;
                }
                // console.log({ objSearch });
                let infoData = await INTEREST_RATE_COLL.find(objSearch).populate("agency product");
                if(!infoData)
                    return resolve({ error: true, message: 'get_list_faild' });

                return resolve({ error: false, data: infoData, message: "get_list_success" });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfoByID({ interestRateID }) {
        return new Promise(async resolve => {
            try {
                let infoData = await INTEREST_RATE_COLL.findById(interestRateID);
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_get' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    removeByID({ interestRateID }) {
        return new Promise(async resolve => {
            try {
                let infoData = await INTEREST_RATE_COLL.findByIdAndDelete(interestRateID);
                if(!infoData)
                    return resolve({ error: true, message: 'cannot_remove' });

                return resolve({ error: false, data: infoData });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkDate({ agencyID, limitDate, price, typeProduct, arrPrice = [], arrObjPerPeriod }){
        return new Promise(async resolve => {
            try {
                // if(limitDate <= 30) {
                let objSearch = {
                    agency: agencyID,
                    to: { $gte: limitDate }, 
                    from: { $lte: limitDate }
                };
                if(typeProduct) {
                    objSearch.product = typeProduct;
                }
                let infoInterest = await INTEREST_RATE_COLL.find(objSearch);
                let priceAfterChange
                let percent;
                if(!infoInterest || !infoInterest.length) {
                    let infoProduct  = await PRODUCT_CATEGORY_COLL.findOne({ product_category: typeProduct, agency: agencyID });
                    if(!infoProduct) {
                        return resolve({ error: true, message: "product_id_not_defined" })
                    }
                    priceAfterChange = price * infoProduct.interestDefault /100;
                    percent          = infoProduct.interestDefault;
                }else{
                    priceAfterChange = price * infoInterest[0].percent /100;
                    percent          = infoInterest[0].percent;
                }
                // let priceAfterChange = price * infoInterest[0].percent /100;
                arrPrice.push(priceAfterChange)
                arrObjPerPeriod.push({
                    percent: percent,
                    price: priceAfterChange,
                    day: limitDate
                });
                return resolve({ error: false, data: { arrPrice, infoInterest, percent, arrObjPerPeriod }});;
                
            } catch (error) {
                console.log({ error_checkdate: error });
                return resolve({ error: true, message: error.message });
            }
        })
    }
    
    checkDateToInterest({ agencyID, limitDate, price, typeProduct }){
        return new Promise(async resolve => {
            try {
                const calculateInterest = calculateInteres({daysForCalculate: limitDate});
                let arrPrice = [];
                let arrObjPerPeriod = [];
                let period = 0;
                let datePeriod = 0;
                // 1// CHECK XEM CÓ TỒN TẠI CHU KÌ HAY KH
                let infoInterest;
                if(calculateInterest.period) {
                    period = calculateInterest.period;
                    for (let i = 0; i < calculateInterest.period; i++) {
                        // 1//CHU KỲ CÓ 30 NGÀY
                        let LIMIT_DATE_OF_PERIOD = 30;
                        // 2//LẤY PHẦN TRĂM LÃI, SỐ TIỀN CỦA CHU KỲ
                        infoInterest = await this.checkDate({ agencyID, limitDate: LIMIT_DATE_OF_PERIOD, price, typeProduct, arrPrice, arrObjPerPeriod})  
                    }
                }
                // 2// CHECK NGÀY LẺ CÓ TỒN TẠI
                if(calculateInterest.days) {
                    datePeriod = calculateInterest.days;
                    let days = calculateInterest.days;
                    // 2//LẤY PHẦN TRĂM LÃI, SỐ TIỀN CỦA CHU KỲ
                    infoInterest = await this.checkDate({ agencyID, limitDate: days, price, typeProduct, arrPrice, arrObjPerPeriod})  
                }

                if(!infoInterest || infoInterest.error || !infoInterest.data) {
                    let data = {
                        infoInterest: {},
                        percent: 0,
                        arrObjPerPeriod: []
                    }
                    infoInterest = { data };
                }
                // console.log({ infoInterest: infoInterest.data.arrObjPerPeriod });
                return resolve({ error: false, data: arrPrice, period, datePeriod, infoInterest: infoInterest.data.infoInterest, percent: infoInterest.data.percent, arrObjPerPeriod: infoInterest.data.arrObjPerPeriod });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkDateToInterestTransaction({ agencyID, interestStartDate, price, typeProduct }){
        return new Promise(async resolve => {
            try {
                let arrPrice = [];
                let arrObjPerPeriod = [];
                let period = 0;
                let datePeriod = 0;
                /**
                 * 1 // Lấy ngày hiện tại
                 */
                let dateNow   =  new Date();
                 /**
                 * 2// Lấy ngày hiện tại - Ngày bắt đầu tính lãi
                 */
                let limitDate = subStractDateGetDay({ date1: dateNow, date2: interestStartDate });
                limitDate     = Math.floor(limitDate);

                /**
                 * 1: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại > 0
                 *    => limitDate + 1, ngày tính lãi bắt đầu 1
                 * 2: Kiểm tra nếu số ngày giữa ngày tính lãi và ngày hiện tại < 0
                 *    => Đã trả lãi trước
                 */
                if (limitDate >= 0) {
                    limitDate     = Math.floor(limitDate) + 1;
                }
                if (limitDate < 0) {
                    limitDate = 0;
                }

                const calculateInterest = calculateInteres({daysForCalculate: limitDate});
                // 1// CHECK XEM CÓ TỒN TẠI CHU KÌ HAY KH
                let infoInterest;
                if(calculateInterest.period) {
                    period = calculateInterest.period;
                    for (let i = 0; i < calculateInterest.period; i++) {
                        // 1//CHU KỲ CÓ 30 NGÀY
                        const LIMIT_DATE_OF_PERIOD = 30;
                        // 2//LẤY PHẦN TRĂM LÃI, SỐ TIỀN CỦA CHU KỲ
                        infoInterest = await this.checkDate({ agencyID, limitDate: LIMIT_DATE_OF_PERIOD, price, typeProduct, arrPrice, arrObjPerPeriod})  
                    }
                }
                // 2// CHECK NGÀY LẺ CÓ TỒN TẠI
                if(calculateInterest.days) {
                    datePeriod = calculateInterest.days;
                    let days = calculateInterest.days;
                    // 2//LẤY PHẦN TRĂM LÃI, SỐ TIỀN CỦA CHU KỲ
                    infoInterest = await this.checkDate({ agencyID, limitDate: days, price, typeProduct, arrPrice, arrObjPerPeriod})  
                }
                /** 
                 * KIỂM TRA XEM CÓ ĐƯỢC TÍNH LÃI
                 */
                // if(!infoInterest) {
                //     return resolve({ error: true, message: "cannot_found_interest"});
                // }
                let data = {}
                /**
                 * KIỂM TRA XEM CÓ BỊ LỖI TRONG QUÁ TRNHF TÍNH LÃI
                 */
                // if(infoInterest.error) {
                //     return resolve(infoInterest);
                // }

                if( !infoInterest || !infoInterest.data || infoInterest.error) {
                    data = {
                        arrPrice: [],
                        infoInterest: [],
                        percent: 0,
                        arrObjPerPeriod: [],
                        loanAmoutAfterInterest: 0
                    }
                }else{
                    let loanAmoutAfterInterest = 0;
                    data = {
                        infoInterest: infoInterest.data.infoInterest,
                        arrObjPerPeriod: infoInterest.data.arrObjPerPeriod
                    }
                    /**
                     * TÍNH RA TỔNG TIỀN LÃI
                     */
                    arrPrice.forEach( price => {
                        loanAmoutAfterInterest += price;
                    });
                    data.loanAmoutAfterInterest = loanAmoutAfterInterest;
                }
                // console.log({ infoInterest});
                return resolve({ error: false, data: data });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
