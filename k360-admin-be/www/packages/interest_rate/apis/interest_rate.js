"use strict";
/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { uploadSingle, uploadArray, uploadCusAndAuthPaper } = require('../../../config/cf_helpers_multer');
const INTEREST_RATE_MODEL                           = require('../models/interest_rate').MODEL;
const { CF_ROUTINGS_INTEREST_RATE }                 = require("../constant/interest_rate.uri");
const { TYPE_TRANSACTION }                          = require('../../../config/cf_constants');
const { PRODUCT_CATEGORY_MODEL }                    = require('../../product_category');
/**
 * MODELS, COLLECTIONS
 */
module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
          
            /**
             * Function: Thêm khách hàng (API)
             * Date: 17/04/2021
             * Dev: SonLP
             */
            [CF_ROUTINGS_INTEREST_RATE.ADD_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        const { _id: userID } = req.user;
                        const agencyID = req.agencyID;
                        // console.log({ agencyID });
                        const { name, productID, from, to, percent } = req.body;
                        let infoAfterInsert = await INTEREST_RATE_MODEL.insert({ name, agencyID, productID, from, to, userID, percent });
                        res.json(infoAfterInsert);
                    }]
                },
            },

            
            /**
             * Function: CẬP NHẬT QUẢN LÝ LÃI
             * Date: 17/04/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_INTEREST_RATE.UPDATE_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [async function (req, res) {
                        const { _id: userID } = req.user;
                        // const agencyID = req.agencyID;
                        const { interestRate, name, percent } = req.body;
                        let infoAfterUpdate = await INTEREST_RATE_MODEL.update({ interestRate, name, percent });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

             /**
             * Function: Lấy thông tin quản lý lãi
             * Date: 17/04/2021
             * Dev: DEPV
             */
            [CF_ROUTINGS_INTEREST_RATE.INFO_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        const { _id: userID } = req.user;
                        const { interestRateID } = req.params;
                        let infoAfterUpdate = await INTEREST_RATE_MODEL.getInfoByID({ interestRateID });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

             /**
             * Function: Xoá Quản lý lãi
             * Date: 17/04/2021
             * Dev: DEPV
             */
              [CF_ROUTINGS_INTEREST_RATE.REMOVE_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [async function (req, res) {
                        const { _id: userID } = req.user;
                        const { interestRateID } = req.params;
                        let infoAfterUpdate = await INTEREST_RATE_MODEL.removeByID({ interestRateID });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            // LIST INTEREST RATE
            [CF_ROUTINGS_INTEREST_RATE.LIST_INTEREST_RATE]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'view',
					title: `List Interest Rate - K360`,
					code: CF_ROUTINGS_INTEREST_RATE.LIST_INTEREST_RATE,
					inc: path.resolve(__dirname, '../views/list_interest_rate.ejs'),
                    view: 'index-agency.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { product, type, percent } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ parent: null, status: 1 });
                        const agencyID  = req.agencyID;
                        let listData = await INTEREST_RATE_MODEL.getList({ agencyID, product, type, percent });
                        ChildRouter.renderToView(req, res, {
                            TYPE_TRANSACTION,
                            listProductCategory: listProductCategory.data,
                            listData: listData.data,
                            _product: product , _type: type, percent
						});
                    }]
                },
            },

            //============API MOBILE ===================
            // LIST INTEREST RATE
            [CF_ROUTINGS_INTEREST_RATE.LIST_INTEREST_RATE_API]: {
                config: {
                    auth: [ roles.role.employee.bin ],
					type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let { product, type, percent } = req.query;
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ parent: null, status: 1 });
                        const agencyID  = req.agencyID;
                        let listData = await INTEREST_RATE_MODEL.getList({ agencyID, product, type, percent });
                        if(listData.error == true) {
                            listData = [];
                        }
                        res.json({
                            TYPE_TRANSACTION,
                            listProductCategory: listProductCategory.data,
                            listData,
                            _product: product , _type: type, percent
						});
                    }]
                },
            },
        }
    }
};
