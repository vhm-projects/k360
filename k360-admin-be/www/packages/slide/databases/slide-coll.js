"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION SLIDE CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'slide' }){
	return BASE_COLL(name, {
        /**
         * Hình ảnh slide
         */
        image: {
            type:  Schema.Types.ObjectId,
            ref : 'image'
        }, 
        title: {
            type: String,
        },

        description: {
            type: String,
        },
        link: {
            type: String,
        },
        status: {
            type: Number,
            default: 1
        },
        //_________Người tạo
        userCreate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
        //_________Người cập nhật
        userUpdate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
		...fields
    });
}
