const BASE_ROUTE = '/slide';

const CF_ROUTINGS_SLIDE = {
    // SLIDE
    ADD_SLIDE: `${BASE_ROUTE}/add-slide`,
    LIST_SLIDE: `${BASE_ROUTE}/list-slide`,
    INFO_SLIDE: `${BASE_ROUTE}/info-slide/:slideID`,
    UPDATE_STATUS_SLIDE: `${BASE_ROUTE}/update-status-slide`,
    UPDATE_SLIDE: `${BASE_ROUTE}/update-slide`,
    REMOVE_SLIDE: `${BASE_ROUTE}/remove-slide/:slideID`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_SLIDE = CF_ROUTINGS_SLIDE;
