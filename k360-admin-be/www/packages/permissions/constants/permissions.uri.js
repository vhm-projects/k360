const BASE_ROUTE = '/permission';

const CF_ROUTINGS_PERMISSION = {
    // USER PERMISSION
	ADD_ADMIN: `${BASE_ROUTE}/add-admin`,
    ADD_OWNER: `${BASE_ROUTE}/add-owner`,
	ADD_EMPLOYEE: `${BASE_ROUTE}/add-employee`,

	LIST_USER_ADMIN: `${BASE_ROUTE}/list-user-admin`,
	LIST_USER_ADMIN_API: `/account/list-account`,
	LIST_USER: `${BASE_ROUTE}/list-user`,

	INFO_USER: `${BASE_ROUTE}/info-user`,
	INFO_USER_BY_EMAIL: `${BASE_ROUTE}/info-user-by-email`,
	DELETE_USER: `${BASE_ROUTE}/delete-user`,
    UPDATE_USER: `${BASE_ROUTE}/update-user`,

	UPDATE_USER_OF_AGENCY: `${BASE_ROUTE}/update-user-of-agency`,
	UPDATE_TO_EMPLOYEE: `${BASE_ROUTE}/update-to-employee`,
	UPDATE_TO_OWNER: `${BASE_ROUTE}/update-to-owner`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PERMISSION = CF_ROUTINGS_PERMISSION;
