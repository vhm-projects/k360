"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../../routing/child_routing');
// delete require.cache[require.resolve('../../../../config/cf_role')]
const roles                                         = require('../../../../config/cf_role');
const { CF_ROUTINGS_PERMISSION } 					= require('../../constants/permissions.uri');

/**
 * MODELS
 */
const USER_MODEL  = require('../models/user').MODEL;

/**
 * vì cf_role file này khác so với các phần import file apis khác
 *  -> cần phải clear cache: https://stackoverflow.com/questions/15666144/how-to-remove-module-after-require-in-node-js
 */
// console.log({`
//     [`require.resolve('../../../../config/cf_role')`]: require.resolve('../../../../config/cf_role'),
//     [`require('../../../../config/cf_role')`]: require('../../../../config/cf_role')
// })
module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ USER PERMISSION  ================================
             * ========================== ************************ ================================
             */


			/**
             * Function: tạo user admin (permission: admin) (API)
             * Date: 18/04/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PERMISSION.ADD_ADMIN]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { username, email, password, status } = req.body;
                        
                        const infoAfterInsertAccount = await USER_MODEL.insert({ 
                            username, email, password, status, level: 0
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

			/**
             * Function: tạo user owner (permission: admin, owner) (API)
             * Date: 01/04/2021
             * Update:
             *  + 17/04/2021 Dattv: Thêm field agency để đăng ký tài khoản owner cho agency
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PERMISSION.ADD_OWNER]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { username, email, password, status, agency } = req.body;

                        const infoAfterInsertAccount = await USER_MODEL.insert({ 
                            username, email, password, status, level: 1, agency
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

			/**
             * Function: tạo user employee (permission: admin, owner) (API)
             * Date: 01/04/2021
             * Update:
             *  + 17/04/2021 Dattv: Thêm field agency để đăng ký tài khoản admin cho agency
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PERMISSION.ADD_EMPLOYEE]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { username, email, agency } = req.body;

                        const infoAfterInsertAccount = await USER_MODEL.insert({ 
                            username, email, level: 2, agency
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

			/**
             * Function: Danh sách user admin (permission: admin) (API)
             * Date: 18/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PERMISSION.LIST_USER_ADMIN]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { keyword, status } = req.query;

                        const listUser = await USER_MODEL.getListAdmin({ keyword, status });
                        res.json(listUser);
                    }]
                },
            },
            /**
             */
             [CF_ROUTINGS_PERMISSION.LIST_USER_ADMIN_API]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'view',
                    title: 'Admin Manager - K360',
					code: CF_ROUTINGS_PERMISSION.LIST_USER_ADMIN_API,
					inc: path.resolve(__dirname, '../views/list_user.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const listUser = await USER_MODEL.getListAdmin({  });
                        ChildRouter.renderToView(req, res, {
                            listUser: listUser.data,
                        });
                    }]
                },
            },

			/**
             * Function: Danh sách user (permission: admin, owner) (API)
             * Date: 18/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PERMISSION.LIST_USER]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { keyword, level, status } = req.query;

                        const listUser = await USER_MODEL.getListUser({ keyword, level, status });
                        res.json(listUser);
                    }]
                },
            },

			/**
			 * Function: Xóa user (permission: admin, owner) (API)
			 * Date: 18/04/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_PERMISSION.DELETE_USER]: {
                config: {
                    auth: [ roles.role.owner.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { level } = req.user;
                        const { userID, agency, levelUserRemove } = req.query;
                        
                        const infoAfterDelete = await USER_MODEL.delete({ userID, level, agency, levelUserRemove });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Cập nhật user (permission: admin) (API)
             * Date: 18/04/2021
             * Dev: MinhVH
             * Update: 27/04/2021: Bổ sung trường username (Dattv)
             */
			[CF_ROUTINGS_PERMISSION.UPDATE_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, email, username, password, oldPassword, status, level, agency } = req.body;
                        const infoAfterAdd = await USER_MODEL.update({
                            userID, email, username, password, oldPassword, status, level, agency
                        });
                        res.json(infoAfterAdd);
                    }]
                },
            },

			[CF_ROUTINGS_PERMISSION.UPDATE_USER_OF_AGENCY]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, email, username, status, level, agencyID } = req.body;

                        const infoAfterUpdate = await USER_MODEL.updateUserAgency({ 
                            userID, email, username, status, level, agencyID
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Thông tin user (permission: admin, owner, employee) (API)
             * Date: 18/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PERMISSION.INFO_USER]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { level } = req.user;
                        const { userID } = req.query;

                        const infoUser = await USER_MODEL.getInfo({ userID, level });
                        res.json(infoUser);
                    }]
                },
            },

            /**
             * Function: Thông tin user (permission: admin, owner, employee) (API)
             * Date: 18/04/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PERMISSION.INFO_USER_BY_EMAIL]: {
                config: {
                    auth: [ roles.role.employee.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { email, username } = req.body;
                        const infoUser = await USER_MODEL.checkEmailExisted({ email, username });
                        res.json(infoUser);
                    }]
                },
            },

            /**
             * Function: Set tài khoản owner -> employee (API)
             * Date: 20/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_PERMISSION.UPDATE_TO_EMPLOYEE]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, agency } = req.body;
                        const infoUserUpdate = await USER_MODEL.updateToEmployee({ userID, level: 2, agency });
                        res.json(infoUserUpdate);
                    }]
                },
            },

            /**
             * Function: Set tài khoản employee -> owner (API)
             * Date: 20/04/2021
             * Dev: Dattv
             */
			[CF_ROUTINGS_PERMISSION.UPDATE_TO_OWNER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, agency } = req.body;
                        const infoUserUpdate = await USER_MODEL.updateToOwner({ userID, level: 1, agency });
                        res.json(infoUserUpdate);
                    }]
                },
            },
        }
    }
};
