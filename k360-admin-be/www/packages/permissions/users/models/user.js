"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      					= require('mongoose').Types.ObjectId;
const jwt                           					= require('jsonwebtoken');
const { hash, hashSync, compare }   					= require('bcryptjs');
const { sendMailNewAccount, sendMailForgetPassword }    = require('../../../../mailer/module/mail_user');

/**
 * INTERNAL PACKAGE
 */
const cfJWS                         							 = require('../../../../config/cf_jws');
const { randomStringFixLengthCode, randomStringFixLengthNumber } = require('../../../../utils/string_utils');

/**
 * BASE
 */
const BaseModel = require('../../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const { AGENCY_COLL } 				= require('../../../agency');
const USER_COLL  					= require('../databases/user-coll')({});
const CODE_RESET_PASSWORD_COLL 	    = require('../databases/code_reset_password-coll')({});
const CODE_RESET_PASSWORD_MODEL 	= require('./code_reset_password').MODEL;
const { BLACKLIST_MODEL } 			= require('../../../blacklist');


class Model extends BaseModel {
    constructor() {
        super(USER_COLL);
    }

	insert({ username, email, password, status, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!username || !email)
                    return resolve({ error: true, message: 'params_not_valid' });

                if(agency && !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'agency_params_not_valid' });

                let emailValid = email.toLowerCase().trim();
                let usernameValid = username.trim();

                let checkExists = await USER_COLL.findOne({
                    $or: [
                        { username: usernameValid },
                        { email: emailValid },
                    ]
                });
                if(checkExists)
                    return resolve({ error: true, message: "name_or_email_existed" });

                let hashPassword;
                let dataInsert = {
                    username: usernameValid, 
                    email: emailValid,
                    status,
                    level
                }

                if(ObjectID.isValid(agency)){
					dataInsert.$addToSet = { agency };
                    password = randomStringFixLengthCode(6);
                    hashPassword = await hash(password, 8);
                    dataInsert.password = hashPassword;
                } else{
                    hashPassword = await hash(password, 8);
                    if (!hashPassword)
                        return resolve({ error: true, message: 'cannot_hash_password' });
                    dataInsert.password = hashPassword;
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_user_failed' });

                // Thêm vào danh sách owners của agency
                if(agency && ObjectID.isValid(agency) && +level === 1){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $addToSet: { owners: infoAfterInsert._id }
                    }, { new: true });

                    //Thêm vào agency của User
                    let { _id: userID } = infoAfterInsert;
                    await USER_COLL.findByIdAndUpdate(userID, {
                        $addToSet: { agency: agency }
                    });
                }

                // Thêm vào danh sách employees của agency
                if(agency && ObjectID.isValid(agency) && +level === 2){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $addToSet: { employees: infoAfterInsert._id }
                    }, { new: true });

                    //Thêm vào agency của User
                    let { _id: userID } = infoAfterInsert;
                    await USER_COLL.findByIdAndUpdate(userID, {
                        $addToSet: { agency: agency }
                    });
                }

                if(agency && ObjectID.isValid(agency)){
                    sendMailNewAccount(email, username, password);
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ userID, level }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoUser = await USER_COLL.findById(userID);
                if(!infoUser)
                    return resolve({ error: true, message: "user_is_not_exists" });

				if(level && [1,2].includes(+level) && +infoUser.level === 0){
					return resolve({ error: true, message: 'cannot_get_info_admin' });
				}

                return resolve({ error: false, data: infoUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkEmailExisted({ email, username }) {
        return new Promise(async resolve => {
            try {

                let infoUserByEmail = await USER_COLL.findOne({ email })
                    .populate("agency");
                let infoUserByUsername = await USER_COLL.findOne({ username });

                if(infoUserByEmail){
                    return resolve({ error: true, message: 'email_existed', data: infoUserByEmail });
                }else if(infoUserByUsername){
                    return resolve({ error: true, message: 'username_existed' });
                }else{
                    return resolve({ error: false, message: "can_use_account" });
                }
                
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ userID, email, username, password, oldPassword, status, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                if(agency && !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'param_not_valid' });

                const checkExists = await USER_COLL.findById(userID);
                if(!checkExists)
                    return resolve({ error: true, message: "user_is_not_exists" });

                const isExistEmail = await USER_COLL.findOne({ email, _id: { $nin: [userID] } });
                if(isExistEmail) {
                    return resolve({ error: true, message: 'Email đã tồn tại' });
                }

                const isExistUsername = await USER_COLL.findOne({ username, _id: { $nin: [userID] } });
                if(isExistUsername) {
                    return resolve({ error: true, message: 'Username đã tồn tại' });
                }

				if(oldPassword){
					const isMatchPass = await compare(oldPassword, checkExists.password);
					if (!isMatchPass) 
						return resolve({ error: true, message: 'old_password_wrong' });
				}

                const dataUpdateUser = {};
                email    && (dataUpdateUser.email       = email.toLowerCase().trim());
                username && (dataUpdateUser.username    = username.trim());
                password && (dataUpdateUser.password    = hashSync(password, 8));
                status   && (dataUpdateUser.status      = status);
                agency   && (dataUpdateUser.$addToSet   = { agency });

                if([1,2].includes(+level)){
                    dataUpdateUser.level = level;
                }

                // let dataAfterUpdate = await this.updateWhereClause({ _id: userID }, dataUpdateUser);
                let dataAfterUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, {new: true});
                // password && delete dataAfterUpdate.password;
                dataAfterUpdate.password = undefined;

                if(Number(level) == 1){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $addToSet: { owners: userID },
                        $pull: { employees: userID }
                    }, { new: true })
                }

                if(Number(level) == 2){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $pull: { owners: userID },
                        $addToSet: { employees: userID }
                    }, { new: true })
                }

                return resolve({ error: false, data: dataAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	updateUserAgency({ userID, email, username, status, level, agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: "param_invalid" });

                const checkExists = await USER_COLL.findById(userID);
                if(!checkExists)
                    return resolve({ error: true, message: "user_is_not_exists" });

                const dataUpdateUser = {};
                email    && (dataUpdateUser.email       = email.toLowerCase().trim());
                username && (dataUpdateUser.username    = username.toLowerCase().trim());

				if([1,2].includes(+level)){
                    dataUpdateUser.level = level;
                }

				if(status){
					status == 0 && await BLACKLIST_MODEL.insert({ agencyID, userID });
					status == 1 && await BLACKLIST_MODEL.delete({ agencyID, userID });
				}

                let infoUserUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, { new: true }).lean();

				if(!infoUserUpdate)
					return resolve({ error: true, message: 'cannot_update_user_agency' })

				switch (+level) {
					case 1:
						await AGENCY_COLL.findByIdAndUpdate(agencyID, {
							$addToSet: { owners: userID },
							$pull: { employees: userID }
						})
						break;
					case 2:
						await AGENCY_COLL.findByIdAndUpdate(agencyID, {
							$pull: { owners: userID },
							$addToSet: { employees: userID }
						})
						break;
					default:
						break;
				}
				delete infoUserUpdate.password;

				return resolve({ error: false, data: infoUserUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateForgetPassword({ email, code, password }) {
        return new Promise(async resolve => {
            try {
                const LEVEL_ADMIN = 0;
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim(), level: LEVEL_ADMIN });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                if (!password) {
                    return resolve({ error: true, message: 'password_invalid' });
                }
                let infoCode = await CODE_RESET_PASSWORD_COLL.findOne({ email, code })
                if(!infoCode) {
                    return resolve({ error: true, message: 'cannot_update_password_user' });
                }
                let passwordNew = hashSync(password, 8);

                let infoUserAfterUpdate = await USER_COLL.findOneAndUpdate({ email }, {
                    password: passwordNew
                });

                if (!infoUserAfterUpdate) {
                    return resolve({ error: true, message: 'cannot_update_password_user' });
                }

                return resolve({ error: false, data: email });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ userID, level, agency, levelUserRemove }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(agency))
                    return resolve({ error: true, message: "param_not_valid" });
                console.log({ userID, level, agency, levelUserRemove });
				let infoAfterDelete = null;

                //console.log({ levelUserRemove, agency });

				// switch (+level) {
				// 	case 0:
				// 		infoAfterDelete = await USER_COLL.findByIdAndRemove(userID);
                //         console.log("case 0");
				// 		break;
				// 	case 1:
				// 		infoAfterDelete = await USER_COLL.deleteOne({ 
				// 			_id: userID,
                //             level: 2
				// 		});
                //         console.log("case 1");
				// 		break;
				// 	default:
				// 		break;
				// }

                if(Number(levelUserRemove) == 1){
                    infoAfterDelete = await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $pull: { owners: userID }
                    }, { new: true })
                }

                if(Number(levelUserRemove) == 2){
                    infoAfterDelete = await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $pull: { employees: userID }
                    })
                }
                await USER_COLL.findByIdAndUpdate(userID, {
                    $pull: { agency: agency }
                }, {new: true});

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "cannot_delete_user" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListAdmin({ keyword, status }){
        return new Promise(async resolve => {
            try {
                const condition = {
					level: 0
				};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { username: new RegExp(key, 'i') },
                        { email: new RegExp(key, 'i') }
                    ]
                }

                status && (condition.status = status);

                const listUsersAdmin = await USER_COLL.find(condition);
                if(!listUsersAdmin)
                    return resolve({ error: true, message: "not_found_accounts_admin_list" });

                return resolve({ error: false, data: listUsersAdmin });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListUser({ keyword, level, status }){
        return new Promise(async resolve => {
            try {
                const condition = { };
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { username: new RegExp(key, 'i') },
                        { email: new RegExp(key, 'i') }
                    ]
                }

				if(level){
					if(![1,2].includes(+level)){
						return resolve({ error: false, message: "cannot_get_list_admin" });
					}

					condition.level = level;
				}

                status && (condition.status = status);

                const listUsers = await USER_COLL.find(condition);
                if(!listUsers)
                    return resolve({ error: true, message: "not_found_accounts_list" });

                return resolve({ error: false, data: listUsers });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    signIn({ email, password }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim() });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                const isMatchPass = await compare(password, checkExists.password);
                if (!isMatchPass) 
                    return resolve({ error: true, message: 'password_is_wrong' });

                if (checkExists.status == 0) 
                    return resolve({ error: true, message: 'user_blocked' });

				if(checkExists.level !== 0)
					return resolve({ error: true, message: 'user_not_admin' });

                const infoUser = {
                    _id: checkExists._id,
                    username: checkExists.username,
                    email: checkExists.email,
                    status: checkExists.status,
                    level: checkExists.level,
                }
                const token = jwt.sign(infoUser, cfJWS.secret);

                return resolve({
                    error: false,
                    data: { user: infoUser, token }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    forgetPassword({ email }) {
        return new Promise(async resolve => {
            try {
                const LEVEL_ADMIN = 0;
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim(), level: LEVEL_ADMIN });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                let codeReset = randomStringFixLengthNumber(6);

                let infoCodeAfterInsert =  await CODE_RESET_PASSWORD_MODEL.insert({ email, code: codeReset });

                if (infoCodeAfterInsert.error) {
                    return resolve(infoCodeAfterInsert);
                }

                sendMailForgetPassword(email, codeReset);

                return resolve(infoCodeAfterInsert);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkCodeForgetPassword({ email, code }) {
        return new Promise(async resolve => {
            try {
                const LEVEL_ADMIN = 0;
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim(), level: LEVEL_ADMIN });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                let infoCode =  await CODE_RESET_PASSWORD_MODEL.checkCode({ email, code });

                if (infoCode.error) {
                    return resolve(infoCode);
                }

                return resolve(infoCode);
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateToOwner({ userID, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(agency))
                    return resolve({ error: true, message: "param_not_valid" });

                let dataUpdateUser = {
                    level
                };

                let infoUserUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, { new: true });
                
                // Đẩy ra khỏi mảng Employees
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $pull: { employees: userID }
                }, { new: true })

                // Thêm vào mảng Owners
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $addToSet: { owners: userID }
                }, { new: true })
               
                return resolve({ error: false, data: infoUserUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    updateToEmployee({ userID, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID) || !ObjectID.isValid(agency))
                    return resolve({ error: true, message: "param_not_valid" });

                let dataUpdateUser = {
                    level
                };

                let infoUserUpdate = await USER_COLL.findByIdAndUpdate(userID, dataUpdateUser, { new: true });
                
                // Đẩy ra khỏi mảng Owners
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $pull: { owners: userID }
                }, { new: true })

                // Thêm vào mảng Employees
                await AGENCY_COLL.findByIdAndUpdate(agency, {
                    $addToSet: { employees: userID }
                }, { new: true })
               
                return resolve({ error: false, data: infoUserUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    checkAuth({ token }) {
        return new Promise(async resolve => {
            try {
                let decoded = await jwt.verify(token, cfJWS.secret);
                if (!decoded) return resolve({ error: true, message: 'token_not_valid' });

                const { _id: userID } = decoded;
                
                if (!userID) return resolve({ error: true, message: 'token_not_valid' });

                const STATUS_ACTIVE_OF_USER = 1;
                let infoUserDB = await USER_COLL.findOne({ status: STATUS_ACTIVE_OF_USER, _id: userID });

                if (!infoUserDB) 
                    return resolve({ error: true, message: 'token_not_valid' });

                return resolve({ error: false, data: infoUserDB });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
