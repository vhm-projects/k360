"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_FAQ }               = require('../constants/faq.uri');

/**
 * MODELS
 */
const FAQ_MODEL                     = require("../models/faq").MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            /**
             * Function: Thêm FAQ (API)
             * Date: 28/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_FAQ.ADD_FAQ]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userCreate } = req.user;
                        const { title, content } = req.body;

                        const infoAfterAddFaq = await FAQ_MODEL.insert({ 
                            title, content, userCreate
                        });
                        res.json(infoAfterAddFaq);
                    }],
                },
            },

            /**
             * Function: Thêm FAQ (API)
             * Date: 28/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_FAQ.UPDATE_FAQ]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { _id: userUpdate } = req.user;
                        const { faqID, title, content, status } = req.body;

                        const infoAfterUpdateFaq = await FAQ_MODEL.update({ 
                            faqID, title, content, status, userUpdate
                        });
                        res.json(infoAfterUpdateFaq);
                    }],
                },
            },

            /**
             * Function: Danh sách FAQ (API, VIEW)
             * Date: 28/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_FAQ.LIST_FAQ]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: `List FAQ - K360`,
					code: CF_ROUTINGS_FAQ.LIST_FAQ,
					inc: path.resolve(__dirname, '../views/list_faq.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { key, status, type } = req.query;
                        const listFAQ = await FAQ_MODEL.getList({ key, status });

                        if(type && type === 'API'){
                            return res.json(listFAQ);
                        }

                        ChildRouter.renderToView(req, res, { 
                            listFAQ: listFAQ.data || []
                        });
                    }]
                },
            },

            /**
             * Function: Thông tin FAQ (API)
             * Date: 28/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_FAQ.INFO_FAQ]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { faqID } = req.query;

                        const infoFAQ = await FAQ_MODEL.getInfo({ faqID });
                        res.json(infoFAQ);
                    }]
                },
            },

            /**
             * Function: Xóa FAQ (API)
             * Date: 28/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_FAQ.REMOVE_FAQ]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { faqID } = req.query;

                        const infoAfterRemove = await FAQ_MODEL.delete({ faqID });
                        res.json(infoAfterRemove);
                    }]
                },
            },

        }
    }
};
