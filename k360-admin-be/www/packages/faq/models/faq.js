"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                            = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const BaseModel                         = require('../../../models/intalize/base_model');
const FAQ_COLL                          = require('../databases/faq-coll')({});

class Model extends BaseModel {
    constructor() {
        super(FAQ_COLL);
    }

    insert({ title, content, userCreate }) {
        return new Promise(async resolve => {
            try {
                if(!title || !content)
                    return resolve({ error: true, message: "title_and_content_required" });

                let infoAfterInsert = await this.insertData({ title, content, userCreate });
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'cannot_insert' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ faqID, title, content, status, userUpdate }) {
        return new Promise(async resolve => {
            try {
                let dataUpdate = {};
                title   && (dataUpdate.title    = title);
                content && (dataUpdate.content  = content);
                status  && (dataUpdate.status   = status);

                if(!Object.keys(dataUpdate).length && dataUpdate.constructor === Object)
                    return resolve({ error: true, message: "cannot_update" });

                dataUpdate.userUpdate = userUpdate;

                let infoAfterUpdate = await FAQ_COLL.updateOne({ _id: faqID }, dataUpdate);
                if(!infoAfterUpdate)
                    return resolve({ error: true, message: 'cannot_update' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ faqID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(faqID))
                    return resolve({ error: true, message: "param_not_valid" });

                let infoAfterDelete = await FAQ_COLL.findByIdAndRemove(faqID);
                if(!infoAfterDelete)
                    return resolve({ error: true, message: "cannot_delete" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ faqID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(faqID))
                    return resolve({ error: true, message: "param_not_valid" });

                let infoFAQ = await FAQ_COLL.findById(faqID).lean();
                if(!infoFAQ)
                    return resolve({ error: true, message: "not_found_faq" });

                return resolve({ error: false, data: infoFAQ });
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ key, status = { $in: [0, 1] } }) {
        return new Promise(async resolve => {
            try {
                let condition = { status };
                if(key){
                    let keyword = key.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';

                    condition.$or = [
                        { title: new RegExp(keyword, 'i') },
                        { content: new RegExp(keyword, 'i') }
                    ]
                }

                let listFAQ = await FAQ_COLL
                    .find(condition)
                    .sort({ createAt: -1 })
                    .lean();

                if(!listFAQ)
                    return resolve({ error: true, message: "not_found_list_faq" });

                return resolve({ error: false, data: listFAQ });
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
