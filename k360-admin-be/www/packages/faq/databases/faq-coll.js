"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION FAQ CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'faq' }){
	return BASE_COLL(name, {
        title: {
            type: String,
        },
        content: {
            type: String,
        },
        status: {
            type: Number,
            default: 1
        },
        userCreate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        userUpdate: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
		...fields
    });
}
