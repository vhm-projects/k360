const BASE_ROUTE = '/faq';

const CF_ROUTINGS_FAQ = {
    ADD_FAQ: `${BASE_ROUTE}/add-faq`,
    INFO_FAQ: `${BASE_ROUTE}/info-faq`,
    LIST_FAQ: `${BASE_ROUTE}/list-faq`,
    REMOVE_FAQ: `${BASE_ROUTE}/remove-faq`,
    UPDATE_FAQ: `${BASE_ROUTE}/update-faq`,

	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_FAQ = CF_ROUTINGS_FAQ;
