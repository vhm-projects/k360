"use strict";
const jwt = require('jsonwebtoken');
const cfJwt = require('./cf_jws');
const USER_SESSION = require('../session/user-session');
const useragent         = require('express-useragent');
const ChildRouter       = require('../routing/child_routing');
// delete require.cache[require.resolve('../packages/permissions/users')]
const USER_MODEL = require('../packages/permissions/users/models/user').MODEL;
const USER_COLL = require('../packages/permissions/users/databases/user-coll')({});

const ADMIN_ACCESS = [0];
const OWNER_ACCESS = [0, 1];
const EMPLOYEE_ACCESS = [0, 1, 2];
const CUSTOMER_ACCESS = [0, 1, 2, 3];
const BROWSER_ACCESS = 1;
const MOBILE_ACCESS  = 2;

const TYPE_RESPONSE = {
    NOT_PROVIDE_TOKEN: 1,
    TOKEN_INVALID: 2,
    PERMISSION_DENIED: 3,
}

function isCallerMobile(req) {
    let source = req.headers['user-agent'];
    let ua = useragent.parse(source);
    // a Boolean that tells you if the request 
    // is from a mobile device
    let isAjax = req.xhr || (req.headers.accept && req.headers.accept.indexOf('json') > -1);
    const DEVICE_POSTMAN_DEFAULT = 'ldk_postman'; 
    let isPostman = req.headers['postman_test_sandbox'] == DEVICE_POSTMAN_DEFAULT || (req.headers['postman-token'] && req.headers['postman-token'].length > 0);
    // Lưu ý: sử dụng header postman_test_sandbox: ONLY TEST
    let isMobile  = ua.isMobile || isAjax;
    let isStateLess = isPostman || isMobile;
    return isStateLess;
}

function getParams(req) {
    let session         = USER_SESSION.getUser(req.session);
    let token           = null;
    let envAccess       = BROWSER_ACCESS; // default assign evnAccess BROWSER
    let agencyChoiced   = null;
    let isMobile = isCallerMobile(req);

    if (!session) {
        if (!isMobile) {
            console.log(`-----------💻 BROWSER request(redirect -> /login)--------`)
            envAccess       = BROWSER_ACCESS;
        } else {
            console.log(`-----------📱 MOBILE request--------`)
            token           = req.params.token || 
                              req.body.token || 
                              req.query.token || 
                              req.headers['x-access-token'] || 
                              req.headers.token;
            //Fix tên biến từ req.headers.agencyChoiced thành req.headers.agencychoiced
            // agencyChoiced   = req.params.agencychoiced || 
            //                   req.body.agencychoiced || 
            //                   req.query.agencychoiced || 
            //                   req.headers['x-access-agencychoiced'] || 
            //                   req.headers.agencychoiced;
    
            envAccess       = MOBILE_ACCESS;
        }
        
    } else if (session){
        console.log(`-----------💻 BROWSER request--------`)
        token           = session.token;
        // agencyChoiced   = AGENCY_SESSION.getContextAgency(req.session);
        // agencyChoiced   = agencyChoiced && agencyChoiced.agencyID;
        envAccess       = BROWSER_ACCESS;
    }

    return {
        token, agencyChoiced, envAccess
    }
}

/**
 ** kiểm tra môi trường BROWSER || MOBILE -> response data
 * @param envAccess môi trường truy cập
 * @param typeResponse
 *  ? 1 - not provide token
 *  ? 2 - token invalid 
 *  ? 3 - permission denied
 * @param res response object
 */

function checkAndResponseForEachEnv({ envAccess, typeResponse, res }) {
    if (envAccess === BROWSER_ACCESS) {
        // BROWSER WORKSPACE
        switch (typeResponse) {
            case 1:
                return ChildRouter.redirect(res, '/logout')
            case 2: // Phiên Đăng Nhập Hết Hạn
                return ChildRouter.redirect(res, '/logout')
            case 3:
                return ChildRouter.redirect(res, '/logout')
            default:
                return ChildRouter.redirect(res, '/logout')
        }
        // return res.redirect('/logout');
    } else {
        // MOBILE WORKSPACE
        switch (typeResponse) {
            case 1:
                return res.json({ error: true, message: 'no_token_provided' });
            case 2:
                return res.json({ error: true, message: 'failed_authenticate_token'});
            case 3:
                return res.json({ error: true, message: 'permission_denied'});
            default:
                return res.json({ error: true, message: 'error_something'});
        }
    }

    
}

module.exports = {
    role: {
        all: {
            bin: 1,
            auth: function (req, res, next) {
                next();
            }
        },
        employee: {
            bin: 2,
            auth: function (req, res, next) {
				let session = USER_SESSION.getUser(req.session);
				let token = session ? session.token : req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;

                if (token) {
                    jwt.verify(token, cfJwt.secret, function (err, decoded) {
                        if (err) {
                            return res.json({error: true, message: 'Failed to authenticate token.'});
                        } else {
                            if (EMPLOYEE_ACCESS.includes(+decoded.level)) {
                                req.user = decoded;
                                next();
                            } else {
                                return res.redirect('/logout');
                            }
                        }
                    });
                } else {
                    return res.redirect('/login');
                    // return res.status(403).send({
                    //     success: false,
                    //     message: 'No token provided.'
                    // });
                }
            }
        },
        admin: {
            bin: 3,
            auth: function (req, res, next) {
                // let session = USER_SESSION.getUser(req.session);
				// let token = session ? session.token : req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;
                let { token, envAccess } = getParams(req);
                console.log({ token, envAccess })
                if (token) {
                    jwt.verify(token, cfJwt.secret, async function (err, decoded) {
                        if (err) {
                            return res.json({error: true, message: 'Failed to authenticate token.'});
                        } else {
                            let { _id: adminID } = decoded;
                            let statusInActive = 0;
                            let infoAdmin = await USER_COLL.findById(adminID);

                            if(!infoAdmin) {
                                return checkAndResponseForEachEnv({ typeResponse: TYPE_RESPONSE.NOT_PROVIDE_TOKEN, envAccess, res })
                            }

                            if(infoAdmin && infoAdmin.status == statusInActive)
                                return checkAndResponseForEachEnv({ typeResponse: TYPE_RESPONSE.NOT_PROVIDE_TOKEN, envAccess, res })
                                // return res.redirect('/logout');

                            if (ADMIN_ACCESS.includes(+decoded.level)) {
                                req.user = decoded;
                                next();
                            } else {
                                return checkAndResponseForEachEnv({ typeResponse: TYPE_RESPONSE.PERMISSION_DENIED, envAccess, res })
                                // return res.redirect('/logout');
                            }
                        }
                    });
                } else {
                    // return res.redirect('/login');
                    return checkAndResponseForEachEnv({ typeResponse: TYPE_RESPONSE.TOKEN_INVALID, envAccess, res })
                    // return res.status(403).send({
                    //     success: false,
                    //     message: 'No token provided.'
                    // });
                }
            }
        },
        owner: {
            bin: 4,
            auth: function (req, res, next) {
				let session = USER_SESSION.getUser(req.session);
				let token = session ? session.token : req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;

                if (token) {
                    jwt.verify(token, cfJwt.secret, function (err, decoded) {
                        if (err) {
                            return res.json({error: true, message: 'Failed to authenticate token.'});
                        } else {
                            if (OWNER_ACCESS.includes(+decoded.level)) {
                                req.user = decoded;
                                next();
                            } else {
                                return res.redirect('/logout');
                            }
                        }
                    });
                } else {
                    return res.redirect('/login');
                    // return res.status(403).send({
                    //     success: false,
                    //     message: 'No token provided.'
                    // });
                }
            }
        },
        customer: { 
            bin: 5,
            auth: function (req, res, next) {
                let { token, agencyChoiced, envAccess } = getParams(req);
                if (token) {
                    jwt.verify(token, cfJwt.secret, function (err, decoded) {
                        if (err) {
                            return checkAndResponseForEachEnv({ typeResponse: TYPE_RESPONSE.TOKEN_INVALID, envAccess, res })
                        } else {
                            if (CUSTOMER_ACCESS.includes(+decoded.level)) {
                                // if(!agencyChoiced && decoded.agency && decoded.agency.length === 1){
                                //     agencyChoiced = decoded.agency[0]._id;
                                // }
                                // console.log({ __admin: agencyChoiced });
                                // console.log({ decoded });
                                // req.agencyID = agencyChoiced;
                                req.user        = decoded;
                                req.envAccess   = envAccess;
                                next();
                            } else {
                                return checkAndResponseForEachEnv({ typeResponse: TYPE_RESPONSE.PERMISSION_DENIED, envAccess, res })
                            }
                        }
                    });
                } else {
                    return checkAndResponseForEachEnv({ typeResponse: TYPE_RESPONSE.NOT_PROVIDE_TOKEN, envAccess, res })
                }
            }
        },
    },

    authorization: function (req, res, next) {
        var hasRole = false;
        var currentRole = null;

          
        // console.log({ url: req.originalUrl,
            // params: {
            //     ['req.params']: req.params,
            //     ['req.body']: req.body,
            //     ['req.query']: req.query,
            //     ['req.headers']: req.headers,
            //      mobile: isCallerMobile(req),
            // }
        // })
        for (var itemRole in this.role) {
            if (!hasRole) {
                if (res.bindingRole.config.auth.includes(this.role[ itemRole ].bin)) {
                    hasRole = true;
                    currentRole = this.role[ itemRole ];
                }
            }
        }
        currentRole.auth(req, res, next);
    }
};