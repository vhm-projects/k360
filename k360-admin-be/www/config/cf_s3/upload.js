let config                      = require('./config');
let AWS                         = require('aws-sdk');

AWS.config.update({
    accessKeyId : config.aws_access_key_id,
    secretAccessKey : config.aws_secret_access_key,
    region: config.aws_region,
})
let s3Object                    = new AWS.S3();

// link từ s3 upload file
let render_link_upload_file_s3 = ({ fileName, type }) =>{
    try {
        let bucket = `${config.bucket}/root`;
        
        var url = s3Object.getSignedUrl('putObject', {
            Bucket: bucket,
            Key: fileName,
            Expires: config.signedUrlExpireSeconds,
            ACL: "public-read",
            ContentType: type,
        });

        console.log({ url_sign: url })
        return { error : false, url };
    } catch (error) {
        return  { error : true, message : 'cant_create_url' }
    }
}

function render_link_upload_s3(fileName, type) {
    return new Promise(async resolve => {
        try {
            if(!fileName || !type)
                return resolve({ error: true, message: "params_invalid" });
            let result = await render_link_upload_file_s3({fileName, type });
            if(!result.error){
                return resolve({ error: false, data: result });
            }else{
                return resolve({ error: true, message: "Error" });
            }
        } catch (error) {
            return resolve({ error: true, message: error.message });
        }
    })
}

exports.GENERATE_LINK_S3                    = render_link_upload_s3; 