"use strict";

exports._default_tme_zone = 'Asia/Ho_Chi_Minh';


//sudo timedatectl set-timezone Asia/Ho_Chi_Minh
//Hàm trả về số năm giữa 2 thời gian
exports.subStractDate = function subStractDate({date1, date2}) {
    console.log({ date1, date2 });
    let timeDate1 = new Date(date1).getTime();
    let timeDate2 = new Date(date2).getTime();
    let resultSubtractTime = timeDate1 - timeDate2;
    
    const MINUTES = 60 * 1000;
    const HOURS   = MINUTES * 60;
    const DAYS    = HOURS * 24;
    let resultSubtractTimeAfterParseDay = resultSubtractTime / DAYS;
    // let bb = Math.round(Math.abs((timeDate1 - timeDate2) / DAYS));
    return resultSubtractTimeAfterParseDay/365;

    // return new Date((new Date(subAdded)).getTime() - minute * 60000);
};

//hàm trả về số ngày giữa 2 thời gian
exports.subStractDateGetDay = function subStractDate({date1, date2}) {
    let timeDate1 = new Date(date1).getTime();
    let timeDate2 = new Date(date2).getTime();
    let resultSubtractTime = timeDate1 - timeDate2;
    
    const MINUTES = 60 * 1000;
    const HOURS   = MINUTES * 60;
    const DAYS    = HOURS * 24;
    let resultSubtractTimeAfterParseDay = resultSubtractTime / (1000 * 3600 * 24);
    // return Math.round((second-first)/(1000*60*60*24));
    // let bb = Math.round(Math.abs((timeDate1 - timeDate2) / DAYS));
    return resultSubtractTimeAfterParseDay;

    // return new Date((new Date(subAdded)).getTime() - minute * 60000);
};

exports.addNumberToSetDate = function addNumberToSetDate({date1, numberToSet}) {
    // console.log({ date1, numberToSet });
    // let dateAfterChange = Date(date1);
    return date1.setDate(date1.getDate() + numberToSet); ;
};

exports.minusNumberToSetDate = function addNumberToSetDate({date1, numberToSet}) {
    // console.log({ date1, numberToSet });
    // let dateAfterChange = Date(date1);
    return date1.setDate(date1.getDate() - numberToSet); ;
};

/**
 * FUNCTION: LẤY SỐ TUẦN CỦA THÁNG VÀ NGÀY TRONG TUẦN
 * AUTHOR: SONLP
 */
exports.getWeek = function getWeekOfMonth(date) {
    const startWeekDayIndex = 1; // 1 MonthDay 0 Sundays
    const firstDate = new Date(date.getFullYear(), date.getMonth(), 1);
    const firstDay = firstDate.getDay();

    let weekNumber = Math.ceil((date.getDate() + firstDay) / 7);
    if (startWeekDayIndex === 1) {
        if (date.getDay() === 0 && date.getDate() > 1) {
            weekNumber -= 1;
        }

        if (firstDate.getDate() === 1 && firstDay === 0 && date.getDate() > 1) {
            weekNumber += 1;
        }

    }
    // var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var weekday = ["1", "2", "3", "4", "5", "6", "7"];
    // return weekday[dateTheDayBefore.getDay()];
    let dayOfWeek = weekday[date.getDay()];
    dayOfWeek     = Number(dayOfWeek) 
    let data = {
        weekNumber,
        dayOfWeek
    }
    return data;
}

exports.calculatedPercent = function calculatedPercent(now, old) {
    let totalTransactionNow = now, 
        totalTransactionYesterday = old;
    let subtractionRation = totalTransactionNow - totalTransactionYesterday;

    /**
     * ratio: 
     *     1: (dương)
     *     -1: âm
     */
    let ratio = subtractionRation != 0 ? (subtractionRation > 0 ? 1 : -1) : 0;
    //* giá trị chênh lệch giữa 2 mốc
    let valueRatio = (Math.abs(subtractionRation)/(subtractionRation >= 0 ?  totalTransactionNow : totalTransactionYesterday)) * 100;
    return {
        ratio, valueRatio 
    }
}

exports.calculatedPercentDiff = function calculatedPercentDiff(now, old) {
    let totalTransactionNow = now, 
        totalTransactionYesterday = old;
    let subtractionRation = (totalTransactionNow - totalTransactionYesterday);

    if (totalTransactionYesterday != 0 && totalTransactionNow != 0) {
        subtractionRation = subtractionRation/totalTransactionYesterday;
    } else if (totalTransactionYesterday == 0 && totalTransactionNow != 0) {
        subtractionRation = 1;
    } else if (totalTransactionYesterday == 0 && totalTransactionNow == 0) {
        subtractionRation = 0;
    } else if (totalTransactionYesterday != 0 && totalTransactionNow == 0) {
        subtractionRation = -1;
    }
    // console.log({ subtractionRation, totalTransactionNow, totalTransactionYesterday });
    let ratio = subtractionRation != 0 ? (subtractionRation > 0 ? 1 : -1) : 0;
    let valueRatio        = subtractionRation * 100;
    
    return {
        ratio, valueRatio: Math.abs(valueRatio)  
    }
}

