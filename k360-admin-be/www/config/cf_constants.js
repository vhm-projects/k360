// KIỂU TÀI KHOẢN
exports.ADMIN_LEVEL = [
    { value: 0, text: 'Editor' },
    { value: 1, text: 'Admin' },
]

// LOẠI TÀI KHOẢN
exports.TYPE_LEVEL = [
    { value: 0, text: 'Admin' },
    { value: 1, text: 'Quản lý' },
    { value: 2, text: 'Nhân viên' },
]

// TRẠNG THÁI
exports.ADMIN_STATUS = [
    { value: 0, text: 'Khóa' },
    { value: 1, text: 'Hoạt động' },
]

/**
 * MIME Types image
 */
 exports.MIME_TYPES_IMAGE = [ 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml' ];

 // LOẠI GIAO DỊCH
exports.TYPE_TRANSACTION = [
    { value: 1, text: 'Cầm đồ' },
    { value: 2, text: 'Gia hạn bình thường' },
    { value: 3, text: 'Gia hạn vay thêm' },
    { value: 4, text: 'Gia hạn trả bớt' },
    { value: 5, text: 'Báo mất' },
    { value: 6, text: 'Chuộc đồ' },
]

/**
 * Danh mục đặc biệt
 */
 exports.SPECIAL_CATEGORY = [
	{ value: 0, text: 'Bình thường' },
	{ value: 2, text: 'Vàng nguyên liệu' },
	{ value: 3, text: 'Trang sức' },
	{ value: 4, text: 'Non brand' },
	{ value: 5, text: 'Không có hóa đơn' },
	{ value: 6, text: 'Danh mục cuối' },
]

//LOẠI LIÊN HỆ
exports.TYPE_CONTACT = [
    { value: 0, text: 'Tư vấn cầm' },
    { value: 1, text: 'Định giá' },
    { value: 2, text: 'Khác' },
]

//TRẠNG THÁI GIAO DỊCH
exports.STATUS_TRANSACTION = [
    { value: 0, text: 'Đang xử lý', class: "badge-info" },
    { value: 1, text: 'Đang cầm', class: "badge-success" },
    { value: 2, text: 'Không duyệt', class: "badge-success" },
    { value: 3, text: 'Đã hoàn thành', class: "badge-danger" },
]

