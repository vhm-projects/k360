const ExcelJS = require('exceljs');

const PROVINCE_COLL     = require('./www/databases/province-coll');
const DISTRICT_COLL     = require('./www/databases/district-coll');
const WARD_COLL         = require('./www/databases/ward-coll');

(async () => {
    let listProvince = [];
    let listDistrict = [];
    let listWard = [];

    const workbookReader = new ExcelJS.stream.xlsx.WorkbookReader('./files/excel/provide_district_ward.xlsx');

    for await (const worksheetReader of workbookReader) {
        for await (const row of worksheetReader) {
            switch (row.worksheet.name) {
                case 'Tỉnh thành phố': {
                    const [,code, description, parent] = row.values;

                    listProvince[listProvince.length] = {
                        code, description, parent
                    }
                    break;
                }
                case 'Quận huyện': {
                    const [,code, description, parent] = row.values;

                    listDistrict[listDistrict.length] = {
                        code, description, parent
                    }
                    break;
                }
                case 'Phường xã': {
                    const [,stt, code, description, parent] = row.values;

                    listWard[listWard.length] = {
                        stt, code, description, parent
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

    listProvince.shift();
    listDistrict.shift();
    listWard.shift();

    await PROVINCE_COLL.deleteMany({});
    await DISTRICT_COLL.deleteMany({});
    await WARD_COLL.deleteMany({});

    await PROVINCE_COLL.insertMany(listProvince);
    await DISTRICT_COLL.insertMany(listDistrict);
    await WARD_COLL.insertMany(listWard);

    console.log({ 
        listProvince: listProvince.length, 
        listDistrict: listDistrict.length, 
        listWard: listWard.length
    });

})();