//Giới tính
exports.TYPE_GENDER = [
    { value: 0, text: 'Nữ' },
    { value: 1, text: 'Nam' },
]

//KHOẢNG CÁCH
exports.DISTANCES = [
    { value: 1000000, text: 'Không giới hạn' },
    { value: 2000, text: '2km' },
    { value: 5000, text: '5km' },
    { value: 10000, text: '10km' },
    { value: 20000, text: '20km' },
    { value: 50000, text: '50km' },
    { value: 100000, text: '100km' },
    { value: 200000, text: '200km' },
]

//LOẠI LIÊN HỆ
exports.TYPE_CONTACT = [
    { value: 0, text: 'Tư vấn cầm' },
    { value: 1, text: 'Định giá' },
    { value: 2, text: 'Khác' },
]

/**
 * MIME Types image
 */
 exports.MIME_TYPES_IMAGE = [ 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml' ];
