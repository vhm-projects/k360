const BASE_ROUTE = '/faq';

const CF_ROUTINGS_FAQ = {
    LIST_FAQ: '/list-faq',

	ORIGIN_ROUTE: BASE_ROUTE
}

exports.CF_ROUTINGS_FAQ = CF_ROUTINGS_FAQ;
