"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_FAQ }               = require('../constants/faq.uri');

/**
 * MODELS
 */
const FAQ_MODEL                     = require("../models/faq").MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

            /**
             * Function: Danh sách FAQ (VIEW)
             * Date: 28/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_FAQ.ORIGIN_ROUTE]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: `List FAQ - K360`,
					code: CF_ROUTINGS_FAQ.ORIGIN_ROUTE,
					inc: path.resolve(__dirname, '../views/list_faq.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const listFAQ = await FAQ_MODEL.getList();

                        ChildRouter.renderToView(req, res, { 
                            listFAQ: listFAQ.data || []
                        });
                    }]
                },
            },

        }
    }
};
