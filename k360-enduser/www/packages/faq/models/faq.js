"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                          = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const BaseModel                         = require('../../../models/intalize/base_model');
const FAQ_COLL                          = require('../databases/faq-coll')({});

class Model extends BaseModel {
    constructor() {
        super(FAQ_COLL);
    }

    getInfo({ faqID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(faqID))
                    return resolve({ error: true, message: "param_not_valid" });

                let infoFAQ = await FAQ_COLL.findById(faqID).lean();
                if(!infoFAQ)
                    return resolve({ error: true, message: "not_found_faq" });

                return resolve({ error: false, data: infoFAQ });
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList() {
        return new Promise(async resolve => {
            try {
                let listFAQ = await FAQ_COLL
                    .find({ status: 1 })
                    .sort({ modifyAt: -1 })
                    .lean();
                if(!listFAQ)
                    return resolve({ error: true, message: "not_found_list_faq" });

                return resolve({ error: false, data: listFAQ });
            } catch(error){
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
