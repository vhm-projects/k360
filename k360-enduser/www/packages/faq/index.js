const FAQ_MODEL 		    = require('./models/faq').MODEL;
const FAQ_COLL  		    = require('./databases/faq-coll')({});
const FAQ_ROUTES            = require('./apis/faq');

module.exports = {
    FAQ_COLL,
    FAQ_MODEL,
    FAQ_ROUTES
}
