"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION YÊU CẦU ĐỊNH GIÁ CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'pricing_request' }){
	return BASE_COLL(name, {
        agency: {
            type: Schema.Types.ObjectId,
            ref: 'agency',
            require: true
        },
		// Tên người gửi yêu cầu định giá
		name: {
			type: String,
			require: true
		},
        // Email người gửi yêu cầu định giá
        email: {
            type: String,
            require: true
        },
        // SĐT người gửi yêu cầu định giá
        phone: {
            type: String,
            require: true
        },
        // Những tiêu chí người dùng chọn
        categories: [{
            type: Schema.Types.ObjectId,
            ref: 'product_category'
        }],
        description: {
            type: String,
            default: ''
        },
        priced: {
            type: Number,
            default: 0
        },
        /**
		 * 0: Chưa xem
         * 1: Chưa liên hệ
         * 2: Đã liên hệ
         */
        status: {
            type: Number,
            default: 0
        },
        // Người cập nhật
        userUpdate: {
            type:  Schema.Types.ObjectId,
            ref : 'user'
        },
		...fields
    });
}
