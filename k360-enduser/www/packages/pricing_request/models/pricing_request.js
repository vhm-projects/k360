"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * EXTERNAL PAKCAGE
 */
// const { checkObjectIDs } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const PRICING_REQUEST_COLL = require('../databases/pricing-request-coll')({});


class Model extends BaseModel {
    constructor() {
        super(PRICING_REQUEST_COLL);
    }

	insert({ agency, name, email, phone, categories, note, priced, status = 0 }) {
        return new Promise(async resolve => {
            try {
                if(!agency || !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'params_not_valid' });

                if(!name || !email || !phone)
                    return resolve({ error: true, message: 'name_email_and_phone_required' });

                const infoAfterAdd = await this.insertData({
                    agency, name, email, phone, categories, description: note, priced, status
                });
                if(!infoAfterAdd)
                    return resolve({ error: true, message: 'add_pricing_request_failed' });

                return resolve({ error: false, data: infoAfterAdd });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
