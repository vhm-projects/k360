const PRICING_REQUEST_MODEL 		= require('./models/pricing_request').MODEL;
const PRICING_REQUEST_COLL  	    = require('./databases/pricing-request-coll')({});
const PRICING_REQUEST_ROUTES       	= require('./apis/pricing_request');

module.exports = {
    PRICING_REQUEST_ROUTES,
    PRICING_REQUEST_COLL,
    PRICING_REQUEST_MODEL,
}
