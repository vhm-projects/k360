"use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_PRICING_REQUEST } 				= require('../constants/pricing_request.uri');

/**
 * MODELS
 */
const PRICING_REQUEST_MODEL = require('../models/pricing_request').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ PRICING REQUEST  ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Thêm yêu cầu định giá (API)
             * Date: 22/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRICING_REQUEST.ADD_PRICING_REQUEST]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { agency, name, phone, email, categories, priced, status, note } = req.body;

                        const infoAfterInsert = await PRICING_REQUEST_MODEL.insert({
                            agency, name, phone, email, categories, priced, status, note
                        })

                        res.json(infoAfterInsert);
                    }]
                },
            },


        }
    }
};
