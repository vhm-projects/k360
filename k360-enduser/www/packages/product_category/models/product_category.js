"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const PRODUCT_CATEGORY_COLL             = require('../databases/product-category-coll')({});
const { AGENCY_PRODUCT_CATEGORY_COLL }  = require('../../agency_product_category');


class Model extends BaseModel {
    constructor() {
        super(PRODUCT_CATEGORY_COLL);
    }

    getInfo({ categoryID }){
        return new Promise(async resolve => {
            try {
                if(!categoryID || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'params_not_valid' });

                const infoProductCategory = await PRODUCT_CATEGORY_COLL
                    .findById(categoryID)
                    .populate('image')
                    .lean();

                if(!infoProductCategory)
                    return resolve({ error: true, message: 'get_info_product_category_failed' });

                return resolve({ error: false, data: infoProductCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ parent, status }){
        return new Promise(async resolve => {
            try {
                const conditionGetList = {};
                if(parent){
                    if(!ObjectID.isValid(parent)){
                        return resolve({ error: true, message: 'params_not_valid' });
                    }

                    conditionGetList.parent = parent;
                }else{
                    conditionGetList.parent = null;
                }

                conditionGetList.status = status || 1;

                const listChildsCategory = await PRODUCT_CATEGORY_COLL
                    .find(conditionGetList)
                    .populate('childs image')
					.sort({ createAt: -1 })
                    .lean();
                if(!listChildsCategory)
                    return resolve({ error: true, message: 'get_list_product_category_failed' });

                return resolve({ error: false, data: listChildsCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	// getListLimit({ status = 1, limit = 30 }){
	// 	return new Promise(async resolve => {
    //         try {
    //             const listCategories = await PRODUCT_CATEGORY_COLL
    //                 .find({ status, parent: null })
	// 				.populate('image')
	// 				.limit(limit)
	// 				.sort({ createAt: -1 })
    //                 .lean();
    //             if(!listCategories)
    //                 return resolve({ error: true, message: 'get_list_product_category_failed' });

    //             return resolve({ error: false, data: listCategories });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
	// }

    // getListChildByParent({ agency, categoryID = null }){
    //     return new Promise(async resolve => {
    //         try {
    //             if(categoryID && !ObjectID.isValid(categoryID))
    //                 return resolve({ error: true, message: 'params_not_valid' });

    //             const listChildsCategory = await AGENCY_PRODUCT_CATEGORY_COLL
    //                 .findOne({ agency, status: 1, product_category: categoryID })
    //                 .populate({
    //                     path: 'product_category',
    //                     match: { status: 1 },
    //                     populate: { path: 'image childs' }
    //                 })
    //                 .lean();

    //             let infoParent = await PRODUCT_CATEGORY_COLL
    //                 .findOne({ _id: categoryID, status: 1 })
    //                 .populate('image')
    //                 .lean();

    //             if(!listChildsCategory)
    //                 return resolve({ error: true, message: 'get_list_product_category_failed' });

    //             return resolve({ 
    //                 error: false, 
    //                 data: { 
    //                     listChildsCategory,
    //                     infoParent
    //                 }
    //             });
    //         } catch (error) {
    //             return resolve({ error: true, message: error.message });
    //         }
    //     })
    // }
}

exports.MODEL = new Model;
