"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path      = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_PRODUCT_CATEGORY } 				= require('../constants/product_category.uri');

/**
 * MODELS
 */
require('../../image');
const PRODUCT_CATEGORY_MODEL            = require('../../product_category/models/product_category').MODEL;
const { AGENCY_PRODUCT_CATEGORY_MODEL } = require('../../agency_product_category');

/**
 * COLLECTIONS
 */
const PRODUCT_CATEGORY_COLL         = require('../databases/product-category-coll')({});


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ PRODUCT CATEGORY ================================
             * ========================== ************************ ================================
             */

           /**
             * Function: LSIT PAWN CATEGORY (VIEW)
             * Date: 10/05/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_PRODUCT_CATEGORY.PAWN_CATEGORY]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
                    title: 'List Pawn Category - K360',
					code: CF_ROUTINGS_PRODUCT_CATEGORY.PAWN_CATEGORY,
                    inc: path.resolve(__dirname, '../views/pawn_category.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res){
                        let listProductCategory = await PRODUCT_CATEGORY_MODEL.getList({ });

                        ChildRouter.renderToView(req, res, {
                            listProductCategory: listProductCategory.data || []
                        })
                    }]
                }
            },

			/**
             * Function: DETAIL PAWN CATEGORY (VIEW)
             * Date: 10/05/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_PRODUCT_CATEGORY.DETAIL_PAWN_CATEGORY]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
                    title: 'Detail Pawn Category - K360',
					code: CF_ROUTINGS_PRODUCT_CATEGORY.DETAIL_PAWN_CATEGORY,
                    inc: path.resolve(__dirname, '../views/detail_pawn_category.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res){
						let { category: categoryID } = req.query;
                        let infoProductCategory = await PRODUCT_CATEGORY_MODEL.getInfo({ categoryID });

                        ChildRouter.renderToView(req, res, {
                            infoProductCategory: infoProductCategory.data || {}
                        })
                    }]
                }
            },

             /**
             * Function: STEP BY CHOOSE (VIEW)
             * Date: 10/05/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_PRODUCT_CATEGORY.STEP_BY_CHOOSE]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
                    title: 'Step by choose - K360',
					code: CF_ROUTINGS_PRODUCT_CATEGORY.STEP_BY_CHOOSE,
                    inc: path.resolve(__dirname, '../views/step_by_choose.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function(req, res){
                        let { category } = req.query;

						let { data: listProductCategory } = await PRODUCT_CATEGORY_MODEL.getList({ 
							parent: category, status: 1 
						});
						let infoParent = await PRODUCT_CATEGORY_COLL.findOne({ _id: category }).lean();

						ChildRouter.renderToView(req, res, {
							listProductCategory,
							infoParent,
						});
                    }]
                }
            },

            /**
             * Function: LIST AGENCY HAS ID CATEGORIES (API)
             * Date: 24/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.LIST_AGENCY_HAS_CATEGORIES]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res){
                        let { categories } = req.body;

						let listAgencyHasCategory = await AGENCY_PRODUCT_CATEGORY_MODEL.getListAgencyHaveCategories({ 
							categories 
						});
                        res.json(listAgencyHasCategory);
                    }]
                }
            },

            /**
             * Function: CALCULATE PRICE (API)
             * Date: 24/05/2021
             * Dev: MinhVH
             */
             [CF_ROUTINGS_PRODUCT_CATEGORY.CALCULATE_PRICED]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function(req, res){
                        let { categories, agencyID } = req.body;

						let calculatePrice = await AGENCY_PRODUCT_CATEGORY_MODEL.calculatePrice({ 
							agencyID, categories 
						});
                        res.json(calculatePrice);
                    }]
                }
            },

            // ===================================================================================== NOT USE
            /**
             * Function: Step by choose (VIEW)
             * Date: 10/05/2021
             * Dev: MinhVH
             */
            // '/step-by-choose-old': {
            //     config: {
            //         auth: [ roles.role.all.bin ],
            //         type: 'view',
            //         title: 'Step by choose - K360',
			// 		code: CF_ROUTINGS_PRODUCT_CATEGORY.STEP_BY_CHOOSE,
            //         inc: path.resolve(__dirname, '../views/step_by_choose_old.ejs'),
            //         view: 'index.ejs'
            //     },
            //     methods: {
            //         get: [ async function(req, res){
            //             let { category, agency } = req.query;

			// 			let { data: listProductCategory } = await PRODUCT_CATEGORY_MODEL.getList({ 
			// 				parent: category, status: 1 
			// 			});
			// 			let { data: listAgencyCategory } = await AGENCY_PRODUCT_CATEGORY_MODEL.getList({ 
			// 				agencyID: agency
			// 			});
			// 			let infoParent = await PRODUCT_CATEGORY_COLL.findOne({ _id: category }).lean();

            //             let listProductCategoryWithAgencyCategory = [];

			// 			listAgencyCategory.map(agencyCategory => {
			// 				let checkfalsyListCategory 	= listProductCategory 	 && listProductCategory.length;
            //                 let productCategory      	= checkfalsyListCategory && listProductCategory.find(productCategory => 
            //                     productCategory._id.toString() === agencyCategory.product_category.toString()
            //                 );

			// 				if(productCategory && agencyCategory.status){
			// 					listProductCategoryWithAgencyCategory = [
			// 						...listProductCategoryWithAgencyCategory, {
			// 						...productCategory,
			// 						priceAgency: agencyCategory.price,
			// 						statusAgency: agencyCategory.status
			// 					}];
			// 				}
			// 			})

            //             // console.log({
            //             //     listProductCategoryWithAgencyCategory,
            //             // });

			// 			return ChildRouter.renderToView(req, res, {
			// 				listProductCategory: listProductCategoryWithAgencyCategory,
			// 				infoParent,
			// 				agency,
            //                 CF_ROUTINGS_PRODUCT_CATEGORY
			// 			});
            //         }]
            //     }
            // },

        }
    }
};
