const BASE_ROUTE = '/product-category';

const CF_ROUTINGS_PRODUCT_CATEGORY = {
    // PRODUCT CATEGORY
    INFO_PRODUCT_CATEGORY: `/info-product-category`,

    STEP_BY_CHOOSE: `/step-by-choose`,
    LIST_AGENCY_HAS_CATEGORIES: `/list-agency-has-categories`,
    CALCULATE_PRICED: `/calculate-priced`,

    PAWN_CATEGORY: `/pawn-category`,
    DETAIL_PAWN_CATEGORY: `/detail-pawn-category`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_PRODUCT_CATEGORY = CF_ROUTINGS_PRODUCT_CATEGORY;
