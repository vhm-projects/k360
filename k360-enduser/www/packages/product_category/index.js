const PRODUCT_CATEGORY_MODEL 			= require('./models/product_category').MODEL;
const PRODUCT_CATEGORY_COLL  			= require('./databases/product-category-coll')({});
const PRODUCT_CATEGORY_ROUTES       	= require('./apis/product_category');

module.exports = {
    PRODUCT_CATEGORY_ROUTES,
    PRODUCT_CATEGORY_COLL,
    PRODUCT_CATEGORY_MODEL,
}
