const BASE_ROUTE = '/agency';

const CF_ROUTINGS_AGENCY = {
    //========== VIEW ==========//
    VIEW_LIST_AGENCY: `${BASE_ROUTE}/list-agency`,
    VIEW_DETAIL_AGENCY: `${BASE_ROUTE}/detail/:agencyID`,
    VIEW_ADD_AGENCY: `${BASE_ROUTE}/view-add-agency`,
    VIEW_LIST_EMPLOYEE: `${BASE_ROUTE}/view-list-employee`,
    //========== JSON ==========//
    INFO_AGENCY: `${BASE_ROUTE}/info-agency/:agencyID`,
    LIST_MEMBER_AGENCY: `${BASE_ROUTE}/list-member/:agencyID`,
    VIEW_LIST_AGENCY: `${BASE_ROUTE}/location`,
    
    //========== JSON ==========//
    INFO_AGENCY: `${BASE_ROUTE}/info-agency/:agencyID`,
    LIST_AGENCY_NEAR: `${BASE_ROUTE}/list-agency-near`,

	ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_AGENCY = CF_ROUTINGS_AGENCY;
