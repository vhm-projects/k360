"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');
 const fs = require('fs');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_AGENCY }            = require('../constants/agency.uri');
const { DISTANCES }                     = require('../../../config/cf_constants');

/**
 * MODELS
 */
const AGENCY_MODEL             = require("../models/agency").MODEL;
const { provinces }            = require('../../common/constant/provinces');
const { districts }            = require('../../common/constant/districts');

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            
            /**
             * Function: Trang tìm kiếm các đại lý lân cận
             * Date: 10/05/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_AGENCY.VIEW_LIST_AGENCY,
					inc: path.resolve(__dirname, '../views/list_agency_near.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let listProvince = Object.entries(provinces);
                        let listDistricts = Object.entries(districts);
                        let { city, district, ward, lng, lat, maxDistance, name } = req.query;
                        const listAgencyNear = await AGENCY_MODEL.getListAgencyNear({ city, district, ward, lng, lat, maxDistance, name });
                        ChildRouter.renderToView(req, res, {
							listProvince,
                            listDistricts,
                            listAgencyNear: listAgencyNear.data,
                            city, district, ward, maxDistance, name, lng, lat,
                            DISTANCES
						});
                    }]
                },
            },

            //========================= JSON ============================

            

            /**
             * Function: Danh sách agency gần đây
             * Date: 18/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_AGENCY.LIST_AGENCY_NEAR]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { city, district, ward, lng, lat, maxDistance } = req.body;
                        const listAgencyNear = await AGENCY_MODEL.getListAgencyNear({ city, district, ward, lng, lat, maxDistance });
                        res.json(listAgencyNear);
                    }]
                },
            },

        }

    }
};
