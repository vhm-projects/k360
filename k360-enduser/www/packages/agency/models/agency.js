"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID  = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const AGENCY_COLL       = require('../databases/agency-coll')({});

class Model extends BaseModel {
    constructor() {
        super(AGENCY_COLL);
    }

    getInfo({ agencyID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(agencyID))
                    return resolve({ error: true, message: 'params_invalid' });

                let infoAgency = await AGENCY_COLL.findById(agencyID)
                    .populate("owners employees childs")
                    .populate({ path: 'avatar', select: 'path name' })
                    .lean();

                if(!infoAgency)
                    return resolve({ error: true, message: 'cannot_get_info_agency' });

                return resolve({ error: false, data: infoAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
   
    /**
     * Danh sách agency
     * Dattv
     */
    getList({ city, district, ward }) {
        return new Promise(async resolve => {
            try {
				let condition = {};
				
                // if(name){
                //     condition.name = 
                //         { $text: { $search: name } },
                //         { score: { $meta: "textScore" } }
                // }

                if(city && city != "null"){
                    condition.city = city;
                }

                if(district && district != "null"){
                    condition.district = district;
                }

                if(ward && ward != "null"){
                    condition.ward = ward;
                }

                let listAgency = await AGENCY_COLL.find(condition)
                    .populate({ path: 'owners', select: 'username' })
                    .populate({ path: 'parent', select: '_id code' })
                    .lean();

                if(!listAgency) 
                    return resolve({ error: true, message: "cannot_get_list_agency" });

                return resolve({ error: false, data: listAgency });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Danh sách agency lân cận
     * Dattv
     */
    getListAgencyNear({ city, district, ward, lng, lat, maxDistance, minDistance, name }) {
        return new Promise(async resolve => {
            try {
                let listAgencyNear;

                let conditionObj = { };

                if(name && name.length > 0){
                    let keyword = name.split(" ");
                    keyword = '.*' + keyword.join(".*") + '.*';
                    conditionObj.name = new RegExp(keyword, 'i');
                }

                if(city && city != "null"){
                    conditionObj.city = city;
                }

                if(district && district != "null"){
                    conditionObj.district = district;
                }

                if(ward && ward != "null"){
                    conditionObj.ward = ward;
                }

                if (!lng || !lat){

                    listAgencyNear = await AGENCY_COLL.find({...conditionObj, status: 1});

                }else{

                    if(!maxDistance){
                        maxDistance = 700000
                    }
    
                    if(!minDistance){
                        minDistance = 0
                    }
    
                    listAgencyNear = await AGENCY_COLL.aggregate([
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(lng), parseFloat(lat)] },
                                maxDistance: Number(maxDistance),
                                minDistance: Number(minDistance),
                                distanceMultiplier: 0.001, //Returns km
                                distanceField: "dist.calculated",
                                includeLocs: "dist.location", // Returns distance
                                spherical: true
                            }
                        },
                        { 
                            $match: { ...conditionObj, status: 1 }
                        }
                    ]);
                }

                if(!listAgencyNear) 
                    return resolve({ error: true, message: "get_list_agency" });
                return resolve({ error: false, data: listAgencyNear });

            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
