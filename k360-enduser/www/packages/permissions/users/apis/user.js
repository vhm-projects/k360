"use strict";

/**
 * EXTERNAL PACKAGE
 */

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../../routing/child_routing');
const roles                                         = require('../../../../config/cf_role');
const { CF_ROUTINGS_AGENCY }                        = require('../constant/user.uri');

/**
 * MODELS
 */
 const USER_MODEL = require('../models/user').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ TÀI KHOẢN AGENCY ================================
             * ========================== ************************ ================================
             */

			[CF_ROUTINGS_AGENCY.INFO_ACCOUNT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { _id: userID } = req.user;

                        const infoAccount = await USER_MODEL.getInfo({ userID });
                        res.json(infoAccount);
                    }]
                },
            },

        }
    }
};
