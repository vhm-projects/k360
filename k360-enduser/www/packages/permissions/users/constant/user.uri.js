const BASE_ROUTE_AGENCY = '/agency';

const CF_ROUTINGS_AGENCY = {
    // ADD_ACCOUNT: TẠO TÀI KHOẢN - mặc định member (vì chỉ owner được tạo bởi admin cấp)
    /**
     * b1: Check user tạo có phải là owner của agency đó?
     * b2: Tạo user với Password Random
     */
    ADD_ACCOUNT: `${BASE_ROUTE_AGENCY}/add-account`, 
    INFO_ACCOUNT: `${BASE_ROUTE_AGENCY}/info-account`,

    ORIGIN_APP: BASE_ROUTE_AGENCY
}

exports.CF_ROUTINGS_AGENCY = CF_ROUTINGS_AGENCY;