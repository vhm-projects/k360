"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');

/**
 * INTERNAL PAKCAGE
 */
const cfJWS = require('../../../../config/cf_jws');

/**
 * BASE
 */
const BaseModel = require('../../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const USER_COLL         = require('../databases/user-coll')({});
const { AGENCY_COLL }   = require('../../../agency');


class Model extends BaseModel {
    constructor() {
        super(USER_COLL);
    }

	insert({ username, email, password, status, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!username || !email || !password)
                    return resolve({ error: true, message: 'params_not_valid' });

                if(agency && !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'agency_params_not_valid' });

                let emailValid = email.toLowerCase().trim();
                let usernameValid = username.toLowerCase().trim();

                let checkExists = await USER_COLL.findOne({
                    $or: [
                        { username: usernameValid },
                        { email: emailValid },
                    ]
                });
                if(checkExists)
                    return resolve({ error: true, message: "name_or_email_existed" });

                let hashPassword = await hash(password, 8);
                if (!hashPassword)
                    return resolve({ error: true, message: 'cannot_hash_password' });

                let dataInsert = {
                    username: usernameValid, 
                    email: emailValid,
                    status,
                }

                if([1,2].includes(+level)){
                    dataInsert.level = level;
                }

                if(ObjectID.isValid(agency)){
					dataInsert.$addToSet = { agency };

                    hashPassword = await hash(randomStringFixLengthCode(6), 8);
                    dataInsert.password = hashPassword;
                } else{
                    dataInsert.password = hashPassword;
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'create_user_failed' });

                // Thêm vào danh sách owners của agency
                if(agency && ObjectID.isValid(agency) && +level === 1){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $addToSet: { owners: infoAfterInsert._id }
                    }, { new: true });
                }

                // Thêm vào danh sách employees của agency
                if(agency && ObjectID.isValid(agency) && +level === 2){
                    await AGENCY_COLL.findByIdAndUpdate(agency, {
                        $addToSet: { employees: infoAfterInsert._id }
                    }, { new: true });
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                const infoUser = await USER_COLL.findById(userID);
                if(!infoUser)
                    return resolve({ error: true, message: "user_is_not_exists" });

                return resolve({ error: false, data: infoUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ userID, email, password, oldPassword, status, level, agency }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

                if(agency && !ObjectID.isValid(agency))
                    return resolve({ error: true, message: 'param_not_valid' });

                const checkExists = await USER_COLL.findById(userID);
                if(!checkExists)
                    return resolve({ error: true, message: "user_is_not_exists" });

				if(oldPassword){
					const isMatchPass = await compare(oldPassword, checkExists.password);
					if (!isMatchPass) 
						return resolve({ error: true, message: 'old_password_wrong' });
				}

                const dataUpdateUser = {};
                email    && (dataUpdateUser.email       = email.toLowerCase().trim());
                password && (dataUpdateUser.password    = hashSync(password, 8));
                status   && (dataUpdateUser.status      = status);
                agency   && (dataUpdateUser.$addToSet   = { agency });

                if([1,2].includes(+level)){
                    dataUpdateUser.level = level;
                }

                await this.updateWhereClause({ _id: userID }, dataUpdateUser);

                password && delete dataUpdateUser.password;
                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "param_not_valid" });

				let infoAfterDelete = await USER_COLL.deleteOne({ 
                    $and: [
                        { _id: userID },
                        { level: 2 }
                    ]
                });

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "cannot_delete_user" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListUser({ keyword, level, status }){
        return new Promise(async resolve => {
            try {
                const condition = { };
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { username: new RegExp(key, 'i') },
                        { email: new RegExp(key, 'i') }
                    ]
                }

				if(level){
					if(![1,2].includes(+level)){
						return resolve({ error: false, message: "cannot_get_list_admin" });
					}

					condition.level = level;
				}

                status && (condition.status = status);

                const listUsers = await USER_COLL.find(condition);
                if(!listUsers)
                    return resolve({ error: true, message: "not_found_accounts_list" });

                return resolve({ error: false, data: listUsers });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    signIn({ email, password }) {
        return new Promise(async resolve => {
            try {
                const checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim() });
                if (!checkExists) 
                    return resolve({ error: true, message: 'email_not_exist' });

                const isMatchPass = await compare(password, checkExists.password);
                if (!isMatchPass) 
                    return resolve({ error: true, message: 'password_is_wrong' });

                if (checkExists.status == 0) 
                    return resolve({ error: true, message: 'user_blocked' });


                let listAgency = [];
                if(checkExists.agency && checkExists.agency.length){
                    listAgency = await AGENCY_COLL.find({ _id: { $in: checkExists.agency } });
                }

                const infoUser = {
                    _id: checkExists._id,
                    username: checkExists.username,
                    email: checkExists.email,
                    status: checkExists.status,
                    level: checkExists.level,
                    agency: listAgency
                }
                const token = jwt.sign(infoUser, cfJWS.secret);

                return resolve({
                    error: false,
                    data: { user: infoUser, token }
                });
            } catch (error) {
                console.error(error);
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
