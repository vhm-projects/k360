"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */
const { convertToSlug } = require('../../../utils/utils');

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CATEGORY_COLL = require('../databases/category-coll')({});


class Model extends BaseModel {
    constructor() {
        super(CATEGORY_COLL);
    }

	insert({ name, slug, description, status, userCreate }) {
        return new Promise(async resolve => {
            try {
                if(!name || !slug)
                    return resolve({ error: true, message: 'params_not_valid' });

				name = name.toLowerCase().trim();
				slug = convertToSlug(slug);

                const checkExists = await CATEGORY_COLL.findOne({
					$or: [
						{ name },
						{ slug }
					]
				});
                if(checkExists)
                    return resolve({ error: true, message: 'name_or_slug_existed' });
    
                const dataInsert = {
                    name,
                    slug,
					description,
                    status,
					userCreate
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'add_category_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ categoryID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoCategory = await CATEGORY_COLL.findById(categoryID);
                if(!infoCategory)
                    return resolve({ error: true, message: 'category_is_not_exists' });

                return resolve({ error: false, data: infoCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    update({ categoryID, name, slug, description, status, userUpdate }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userUpdate) || !ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const checkExists = await CATEGORY_COLL.findById(categoryID);
                if(!checkExists)
                    return resolve({ error: true, message: 'category_is_not_exists' });

                const dataUpdateCategory = {};
                name    		&& (dataUpdateCategory.name 		= name.toLowerCase().trim());
                slug 			&& (dataUpdateCategory.slug 		= convertToSlug(slug));
                description   	&& (dataUpdateCategory.description 	= description);
                status    		&& (dataUpdateCategory.status 		= status);

                dataUpdateCategory.userUpdate = userUpdate;

                await this.updateWhereClause({ _id: categoryID }, {
                    ...dataUpdateCategory
                });

                return resolve({ error: false, data: dataUpdateCategory });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ categoryID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(categoryID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoAfterDelete = await CATEGORY_COLL.findByIdAndRemove(categoryID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: 'category_is_not_exists' });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword, status }){
        return new Promise(async resolve => {
            try {
                const condition = {};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { name: new RegExp(key, 'i') },
                        { slug: new RegExp(key, 'i') },
                        { description: new RegExp(key, 'i') },
                    ]
                }

                status && (condition.status = status);

                const listCategories = await CATEGORY_COLL.find(condition).lean();
                if(!listCategories)
                    return resolve({ error: true, message: 'not_found_categories_list' });

                return resolve({ error: false, data: listCategories });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
