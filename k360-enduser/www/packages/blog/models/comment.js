"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * INTERNAL PAKCAGE
 */

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const COMMENT_COLL 	= require('../databases/comment-coll')({});
const BLOG_COLL		= require('../databases/blog-coll')({});


class Model extends BaseModel {
    constructor() {
        super(COMMENT_COLL);
    }

	insert({ fullname, email, content, website, postID }) {
        return new Promise(async resolve => {
            try {
                if(!fullname || !email || !content)
                    return resolve({ error: true, message: 'params_invalid' });

				if(!postID || !ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_post_invalid' });

                const dataInsert = {
                    fullname,
                    email,
					content,
					website,
                    post: postID,
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'insert_failed' });

				await BLOG_COLL.updateOne({ _id: postID }, {
					$addToSet: { comments: infoAfterInsert._id }
				})

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getListByBlog({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const listCommentByPost = await COMMENT_COLL.find({ post: postID, status: 1 }).lean();
                if(!listCommentByPost)
                    return resolve({ error: true, message: 'get_data_faild' });

                return resolve({ error: false, data: listCommentByPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
