"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID = require('mongoose').Types.ObjectId;

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const BLOG_COLL = require('../databases/blog-coll')({});


class Model extends BaseModel {
    constructor() {
        super(BLOG_COLL);
    }

    getInfo({ postID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(postID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoPost = await BLOG_COLL.findById(postID)
                                                .populate('category image comments userCreate')
												.lean();

                if(!infoPost || infoPost.status == 0)
                    return resolve({ error: true, message: 'post_is_not_exists' });

                return resolve({ error: false, data: infoPost });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getList({ keyword }){
        return new Promise(async resolve => {
            try {
                const condition = {
					status: 1
				};
                if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    condition.$or = [
                        { title: new RegExp(key, 'i') },
                        { slug: new RegExp(key, 'i') },
                        { description: new RegExp(key, 'i') },
                        { content: new RegExp(key, 'i') },
                    ]
                }

                const listPosts = await BLOG_COLL
					.find(condition)
					.populate('category image userCreate')
					.sort({ createAt: -1 })
					.lean();
                if(!listPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getRelatedPost({ postID, categoryID }){
        return new Promise(async resolve => {
            try {
                const listRelatedPosts = await BLOG_COLL.find({
                    $and: [
                        { _id: { $nin: [postID] } },
                        { category: categoryID },
						{ status: 1 }
                    ]
                }).populate('category image')
				  .limit(3)
				  .lean();

                if(!listRelatedPosts)
                    return resolve({ error: true, message: 'not_found_posts_list' });

                return resolve({ error: false, data: listRelatedPosts });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getListMostViewed(){
		return new Promise(async resolve => {
			try {
				const listMostViewedPost = await BLOG_COLL.find({ status: 1 })
					.populate('category image userCreate')
					.sort({ views: -1 })
					.limit(5)
                    .lean();

				if(!listMostViewedPost)
                    return resolve({ error: true, message: 'not_found_posts_list' });

				return resolve({ error: false, data: listMostViewedPost });
			} catch (error) {
                return resolve({ error: true, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
