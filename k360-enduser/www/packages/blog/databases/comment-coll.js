"use strict";

const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION COMMENT CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'comment' }){
	return BASE_COLL(name, {
		fullname: {
			type: String,
			require: true
		}, 
		email: {
			type: String,
			require: true
		},
		content: {
			type: String,
			require: true
		},
		website: {
			type: String,
			default: ''
		},
		post: {
			type: Schema.Types.ObjectId,
			ref: 'blog'
		},
		/**
		 * 0: không cho phép hiển thị ngoài bài viết
		 * 1: Cho phép hiển thị
		 */
		status: {
			type: Number,
			default: 1
		},
		...fields
    });
}
