const BASE_ROUTE_BLOG = '/post';

const CF_ROUTINGS_BLOG = {
    // BLOG
    LIST_POST: `${BASE_ROUTE_BLOG}/list-post`,
    INFO_POST: `${BASE_ROUTE_BLOG}/detail-post/:postID`,

    // CATEGORY
    LIST_CATEGORY: `${BASE_ROUTE_BLOG}/list-category`,
    INFO_CATEGORY: `${BASE_ROUTE_BLOG}/info-category`,

	// COMMENT
	ADD_COMMENT: `${BASE_ROUTE_BLOG}/add-comment`,

    ORIGIN_APP: BASE_ROUTE_BLOG,
}

exports.CF_ROUTINGS_BLOG = CF_ROUTINGS_BLOG;
