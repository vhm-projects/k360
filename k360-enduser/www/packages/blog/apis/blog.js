"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                                   = require('../../../routing/child_routing');
const roles                                         = require('../../../config/cf_role');
const { CF_ROUTINGS_BLOG } 				            = require('../constants/blog.uri');

/**
 * MODELS
 */
const BLOG_MODEL                = require('../models/blog').MODEL;
const CATEGORY_MODEL            = require('../models/category').MODEL;
const COMMENT_MODEL				= require('../models/comment').MODEL;
const { IMAGE_MODEL }           = require('../../image');

/**
 * COLLECTIONS
 */
const BLOG_COLL                 = require('../databases/blog-coll')({});
const CATEGORY_COLL             = require('../databases/category-coll')({});


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
             /**
             * =============================== ************* ===============================
             * =============================== QUẢN LÝ BLOG  ===============================
             * =============================== ************* ===============================
             */

            /**
             * Function: LIST POST (API, VIEW)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.LIST_POST]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'List Posts - K360',
					code: CF_ROUTINGS_BLOG.LIST_POST,
					inc: path.resolve(__dirname, '../views/list_post.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let { keyword, type } = req.query;

						let listPosts 		= await BLOG_MODEL.getList({ keyword });
						let listMostViewed	= await BLOG_MODEL.getListMostViewed();

						if(type === 'api'){
							return res.json({
								listPosts,
								listMostViewed,
							});
						}

						ChildRouter.renderToView(req, res, {
							listPosts: listPosts.data,
							listMostViewed: listMostViewed.data,
						});
                    }]
                },
            },

			 /**
             * Function: Chi tiết post (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
			  [CF_ROUTINGS_BLOG.INFO_POST]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'Detail Post - K360',
					code: CF_ROUTINGS_BLOG.INFO_POST,
					inc: path.resolve(__dirname, '../views/detail_post.ejs'),
                    view: 'index.ejs'
                },
                methods: {
					get: [ async function(req, res){
						let { postID } = req.params;

						let infoPost 		= await BLOG_MODEL.getInfo({ postID });
						let categoryID 		= infoPost.data && 
											  infoPost.data.category && 
											  infoPost.data.category._id || '';

						if(infoPost.error){
							return res.render('pages/404.ejs');
						}

						let listRelatedPost = await BLOG_MODEL.getRelatedPost({ 
							postID,
							categoryID
						})

						// Increase views
						new Promise(async resolve => {
							try {
								await BLOG_COLL.updateOne({ _id: postID }, {
									$inc: { views: 1 }
								});
								resolve();
							} catch (error) {
								resolve();
							}
						})

						return ChildRouter.renderToView(req, res, {
							infoPost: infoPost.data,
							listRelatedPost: listRelatedPost.data,
							isSeo: true
						});
					}]
                },
            },

			/**
             * Function: Thêm bình luận (API)
             * Date: 15/05/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_BLOG.ADD_COMMENT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					post: [ async function(req, res){
						let { fullname, email, content, website, postID } = req.body;

						let dataAfterAdd = await COMMENT_MODEL.insert({
							fullname, email, content, website, postID
						})
						res.json(dataAfterAdd);
					}]
                },
            },



             /**
             * =============================== **************** ===============================
             * =============================== QUẢN LÝ CATEGORY ================================
             * =============================== **************** ===============================
             */

             /**
             * Function: Chi tiết danh mục (API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.INFO_CATEGORY]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
					get: [ async function(req, res){
						let { categoryID } = req.query;

						let infoCategory = await CATEGORY_MODEL.getInfo({ categoryID });
						res.json(infoCategory);
					}]
                },
            },

             /**
             * Function: Danh sách danh mục (VIEW, API)
             * Date: 13/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_BLOG.LIST_CATEGORY]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: 'List Categories - K360',
					code: CF_ROUTINGS_BLOG.LIST_CATEGORY,
					inc: path.resolve(__dirname, '../views/categories/list_category.ejs'),
                    view: 'index-admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						let { keyword, type } = req.query;

						let listCategories 		= await CATEGORY_MODEL.getList({ keyword });
						let countCategory 		= await CATEGORY_COLL.count();
						let countCategoryActive = await CATEGORY_COLL.count({ status: 1 });
						let countCategoryLock 	= await CATEGORY_COLL.count({ status: 0 });

						if(type === 'api'){
							return res.json({
								listCategories,
								countCategory,
								countCategoryActive,
								countCategoryLock
							});
						}

						ChildRouter.renderToView(req, res, { 
							listCategories: listCategories.data,
							countCategory,
							countCategoryActive,
							countCategoryLock
						});
					}]
                },
            },


        }
    }
};
