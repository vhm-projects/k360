const CONTACT_MODEL 		= require('./models/contact').MODEL;
const CONTACT_COLL  		= require('./databases/contact-coll')({});
const CONTACT_ROUTES        = require('./apis/contact');

module.exports = {
    CONTACT_ROUTES,
    CONTACT_COLL,
    CONTACT_MODEL,
}
