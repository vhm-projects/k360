"use strict";

/**
 * EXTERNAL PACKAGE
 */
 const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const roles                             = require('../../../config/cf_role');
const ChildRouter                       = require('../../../routing/child_routing');
const { CF_ROUTINGS_CONTACT }           = require('../constants/contact.uri');


let { TYPE_CONTACT }                    = require('../../../config/cf_constants')
/**
 * MODELS
 */
const CONTACT_MODEL                     = require("../models/contact").MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            
            /**
             * Function: Danh sách liên hệ (View)
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.VIEW_CONTACT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
					title: `List Agency - K360`,
					code: CF_ROUTINGS_CONTACT.VIEW_CONTACT,
					inc: path.resolve(__dirname, '../views/add_contact.ejs'),
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        ChildRouter.renderToView(req, res, {
                            TYPE_CONTACT
						});
                    }]
                },
            },

            //========================= JSON ============================

            /**
             * Function: Insert Contact
             * Date: 28/04/2021
             * Dev: Dattv
             */
             [CF_ROUTINGS_CONTACT.ADD_CONTACT]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { fullname, email, phone, content, type } = req.body;
                        const infoAfterInsertContact = await CONTACT_MODEL.insert({ 
                            fullname, email, phone, content, type
                        });
                        res.json(infoAfterInsertContact);
                    }]
                },
            },
        }
    }
};
