"use strict";
const Schema        = require('mongoose').Schema;
const BASE_COLL     = require('../../../databases/intalize/base-coll');

/**
 * COLLECTION GROUP CỦA HỆ THỐNG
 */
module.exports = function({ fields = {}, name = 'contact' }){
	return BASE_COLL(name, {

        // Tên danh mục
        fullname: {
            type: String,
            require: true
        }, 

        email: {
            type: String,
            require: true
        },

        phone: {
            type: String,
            require: true
        },

        content: {
            type: String,
            require: true
        },

        //cf_constant
        type: {
            type: Number,
            default: 0
        },

        /**
         * 0: Chưa xem
         * 1: Đã xem
         */

        status: {
            type: Number,
            default: 0
        },
     
		...fields
    });
}
