"use strict";

/**
 * EXTERNAL PAKCAGE
 */
const ObjectID          = require('mongoose').Types.ObjectId;
const moment            = require("moment");

/**
 * BASE
 */
const BaseModel = require('../../../models/intalize/base_model');


/**
 * COLLECTIONS
 */
const CONTACT_COLL = require('../databases/contact-coll')({});

class Model extends BaseModel {
    constructor() {
        super(CONTACT_COLL);
    }

    /**
     * Thêm liên hệ
     * Dattv
     */
	insert({ fullname, email, phone, content, type }) {
        return new Promise(async resolve => {
            try {
                if(!fullname || !email || !phone || !content || !type)
                    return resolve({ error: true, message: 'params_not_valid' });

                const dataInsert = {
                    fullname,
                    email,
                    phone,
                    content,
                    type
                }

                const infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'send_contact_failed' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    /**
     * Lấy thông tin contactID và cập nhật đã xem
     * Dattv
     */
    getInfo({ contactID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(contactID))
                    return resolve({ error: true, message: 'param_not_valid' });

                const infoContact = await CONTACT_COLL.findByIdAndUpdate(contactID, {
                    status: 1
                }, { new: true });
                if(!infoContact)
                    return resolve({ error: true, message: 'contact_is_not_exists' });

                return resolve({ error: false, data: infoContact });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }
}

exports.MODEL = new Model;
