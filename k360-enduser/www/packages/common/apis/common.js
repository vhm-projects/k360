"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path                  = require('path');
const fs                    = require('fs');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                               = require('../../../routing/child_routing');
const roles                                     = require('../../../config/cf_role');
const { districts }                             = require('../constant/districts');
const { CF_ROUTINGS_COMMON }                    = require('../constant/common.uri');
const USER_SESSION							    = require('../../../session/user-session');

/**
 * MODELS
 */
const { USER_MODEL }                = require('../../permissions/users');
const { PRODUCT_CATEGORY_MODEL }    = require('../../product_category');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************** ================================
             * ========================== QUẢN LÝ CHUNG  ================================
             * ========================== ************** ================================
             */

            /**
             * HOME PAGE
             */
            [CF_ROUTINGS_COMMON.ORIGIN_APP]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
                    inc : 'inc/home/index.ejs',
                    view: 'index.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { type } = req.query;

						if(type && type === 'API'){
							const { data: listProductCategory } = await PRODUCT_CATEGORY_MODEL.getList({});
							return res.json({
								listProductCategory
							})
						}

						const { data: listProductCategory } = await PRODUCT_CATEGORY_MODEL.getList({});

                        ChildRouter.renderToView(req, res, {
							listProductCategory
                        });
                    }]
                },
            },

            /**
             * Function: Trang chủ admin (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_COMMON.HOME]: {
                config: {
					auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Admin Manager - K360',
					code: CF_ROUTINGS_COMMON.HOME,
					inc: path.resolve(__dirname, '../../../views/inc/home.ejs'),
                    view: 'index-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						ChildRouter.renderToView(req, res);
					}]
				},
            },

            /**
             * Function: Đăng nhập account (VIEW)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_COMMON.LOGIN]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-admin.ejs',
                    view: 'pages/login-admin.ejs'
				},
				methods: {
					get: [ function (req, res) {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						let isExistLogin = USER_SESSION.getUser(req.session);
						if (isExistLogin && isExistLogin.user && isExistLogin.token)
							return res.redirect('/home');

						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { email, password } = req.body;
                    
                        const infoSignIn = await USER_MODEL.signIn({ email, password });
						if (!infoSignIn.error) {
                            USER_SESSION.saveUser(req.session, {
                                user: infoSignIn.data.user, 
                                token: infoSignIn.data.token,
                            });
                        }
                        res.json(infoSignIn);
                    }],
				},
            },

            /**
             * Function: Clear session and render site (API)
             * Date: 03/04/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_COMMON.LOGOUT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-admin.ejs',
                    view: 'pages/login-admin.ejs'
                },
                methods: {
                    get: [ (req, res) => {
                        USER_SESSION.destroySession(req.session);
                        res.clearCookie('x-token');
						res.redirect('/login');
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_DISTRICTS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { province } = req.params;
                        let listDistricts = [];

                        let filterObject = (obj, filter, filterValue) => 
                            Object.keys(obj).reduce((acc, val) =>
                            (obj[val][filter] === filterValue ? {
                                ...acc,
                                [val]: obj[val]  
                            } : acc
                        ), {});

                        if (province && !Number.isNaN(Number(province))) {
                            listDistricts = filterObject(districts, 'parent_code', province.toString())
                        }

                        res.json({ province, listDistricts });
                    }]
                },
            },

            [CF_ROUTINGS_COMMON.LIST_WARDS]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'JSON',
                },
                methods: {
                    get: [ (req, res) => {
                        let { district } = req.params;
                        let listWards = [];
                        let filePath  = path.resolve(__dirname, `../constant/wards/${district}.json`);
                        fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
                            if (!err) {
                                listWards = JSON.parse(data);
                                res.json({ district, listWards  });
                            } else {
                                res.json({ error: true, message: "district_not_exist" });
                            }
                        });
                    }]
                },
            },
        }
    }
};
