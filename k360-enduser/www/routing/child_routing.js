"use strict";
let express         = require('express');
let url             = require('url');
let formatCurrency  = require('number-format.js');
let moment			= require('moment');
let jwt             = require('jsonwebtoken');

let { encrypt }     = require('../utils/utils');

let { CF_ROUTINGS_PRODUCT_CATEGORY }        = require('../packages/product_category/constants/product_category.uri');
let { CF_ROUTINGS_PRICING_REQUEST }         = require('../packages/pricing_request/constants/pricing_request.uri');
let { CF_ROUTINGS_AGENCY }                  = require('../packages/agency/constants/agency.uri');
let { CF_ROUTINGS_CONTACT }                 = require('../packages/contact/constants/contact.uri');
let { CF_ROUTINGS_BLOG } 					= require('../packages/blog/constants/blog.uri');

let { ADMIN_DOMAIN, AGENCY_DOMAIN, ENDUSER_DOMAIN } = require('../config/cf_domain');        


class ChildRouter{
    constructor(basePath) {
        this.basePath = basePath;
        this.registerRouting;
    }

    registerRouting() {
    }
    exportModule() {
        let router = express.Router();
        
        for (let basePath in this.registerRouting()) {
            const item = this.registerRouting()[basePath];
    
            if (typeof item.methods.post !== 'undefined' && item.methods.post !== null) {
                if (item.methods.post.length === 1) {
                    router.post(basePath, item.methods.post[0]);
                } else if (item.methods.post.length === 2) {
                    router.post(basePath, item.methods.post[0], item.methods.post[1]);
                }
            }

            if (typeof item.methods.get !== 'undefined' && item.methods.get !== null) {
                if (item.methods.get.length === 1) {
                    router.get(basePath, item.methods.get[0]);
                } else if (item.methods.get.length === 2) {
                    router.get(basePath, item.methods.get[0], item.methods.get[1]);
                }
            }

            if (typeof item.methods.put !== 'undefined' && item.methods.put !== null) {
                if (item.methods.put.length === 1) {
                    router.put(basePath, item.methods.put[0]);
                } else if (item.methods.put.length === 2) {
                    router.put(basePath, item.methods.put[0], item.methods.put[1]);
                }
            }

            if (typeof item.methods.delete !== 'undefined' && item.methods.delete !== null) {
                if (item.methods.delete.length === 1) {
                    router.delete(basePath, item.methods.delete[0]);
                } else if (item.methods.delete.length === 2) {
                    router.delete(basePath, item.methods.delete[0], item.methods.delete[1]);
                }
            }

        }
        return router;
    }

    /**
     * 
     * @param {*} msg 
     * @param {*} res 
     * @param {*} data 
     * @param {*} code
     * @param {*} status  tham số nhận biết lỗi
     */
    static responseError(msg, res, data, code, status) {
        if (code) {
            res.status(code);
        }
        return res.json({error: true, message: msg, data: data, status: status});
    }

    static response(response, data) {
        return response.json(data);
    }


    static responseSuccess(msg, res, data, code) {
        if (code) {
            res.status(code);
        }

        return res.json({error: false, message: msg, data: data});
    }

    static pageNotFoundJson(res) {
        res.status(404);
        return res.json({"Error": "Page not found!"});
    }

     /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} data 
     * @param {*} title 
     */
    static renderToView(req, res, data, title) {
        data = typeof data === 'undefined' || data === null ? {} : data;

        if (title) {
            res.bindingRole.config.title = title;
        }

        data.render = res.bindingRole.config;
        data.url = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl
        });

        data.CF_ROUTINGS_AGENCY             = CF_ROUTINGS_AGENCY;
        data.CF_ROUTINGS_CONTACT            = CF_ROUTINGS_CONTACT;
        data.CF_ROUTINGS_PRODUCT_CATEGORY   = CF_ROUTINGS_PRODUCT_CATEGORY;
        data.CF_ROUTINGS_PRICING_REQUEST    = CF_ROUTINGS_PRICING_REQUEST;
		data.CF_ROUTINGS_BLOG				= CF_ROUTINGS_BLOG;

        // DOMAIM SOCKET
        data.ADMIN_DOMAIN                   = ADMIN_DOMAIN;
        data.AGENCY_DOMAIN                  = AGENCY_DOMAIN;
        data.ENDUSER_DOMAIN                 = ENDUSER_DOMAIN;
        
        // Thư viện format number
        data.formatCurrency = formatCurrency;

		// Thư viện format thời gian
		data.moment = moment;

        let token = req.cookies['x-token'];
        if(!token){
            token = jwt.sign({ hashed: encrypt(process.env.ENCRYPT_SECRET_KEY) }, process.env.JWT_SECRET_KEY);
            res.cookie('x-token', token, { httpOnly: true, secure: true });
        }

        data._token = token;
		data.isSeo	= data.isSeo || null;

        return res.render(res.bindingRole.config.view, data);
    }

    static renderToPath(req, res, path, data) {
        data = data == null ? {} : data;
        data.render = res.bindingRole.config;
        return res.render(path, data);
    }

    static renderToViewWithOption(req, res, pathRender, data) {
        return res.render(pathRender, {data});
    }

    static redirect(res, path) {
        return res.redirect(path);
    }
}

module.exports = ChildRouter;