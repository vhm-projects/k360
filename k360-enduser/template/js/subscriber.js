  $(document).on('click','#subcribe-now',function(){
   $('.email_error, .name_error').css('display','none');
   $('.modal-body-response').html('');
   $('.modal-body').css('display','block');
   
   $('.modal-body-response').css('display','none');
   $("#close-modal").click(function(){
   $('.modal-body').css('display','block');
     $('.modal-footer').css('display','block');
     $('.modal-body-response').css('display','none');
     $('.modal-body').css('display','block');
   });
   
   var subcriber_email = $('#field-email-innerpage').val();
   var subcriber_name = $('#field-name-innerpage').val();
   
   var is_valid_email = true;
   var is_valid_name = true;
   
   
   var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
   if (!filter.test(subcriber_email)) {
    $('.email_error').css('display','block');
    is_valid_email = false;
   }

   if(subcriber_name == '') {
    is_valid_name = false;
    $('.name_error').css('display','block');
   } 

   if(is_valid_name && is_valid_email) {
	$.ajax({
		type : 'POST',
		url : '/subcribe/save',
		data : { name: subcriber_name, email_address: subcriber_email},
		dataType : 'json',
		cache : false,
		async : true,
		timeout: 10000,
		success : function(response){
			$('.modal-body').css('display','none');
			$('.modal-footer').css('display','none');
			$('.modal-body-response').css('display','block');
			$('.modal-body').css('display','none');
			$('#field-email-innerpage').val('');
			$('#field-name-innerpage').val('');

			if(response == 1) {
				$('.modal-body-response').html('Thank you!');
			} else {
				$('.modal-body-response').html('Error on saving');
			}
		},
	});

    
   } else {
		return false;
   }
   
  });
  
  $(document).on('click','#pressrelease-submit-button',function(){
   
   var subcriber_email = $('#press-newsletter-email').val();
   var subcriber_name = $('#press-newsletter-name').val();
   
   var is_valid_email = true;
   var is_valid_name = true;
   
  
   var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
   if (!filter.test(subcriber_email)) {
    $('.email_error').css('display','block');
    is_valid_email = false;
   } else {
	$('.email_error').css('display','none');
   }

   if(subcriber_name == '') {
    is_valid_name = false;
    $('.name_error').css('display','block');
   } else {
	 $('.name_error').css('display','none');
   }

   if(is_valid_name && is_valid_email) {
	$.ajax({
		type : 'POST',
		url : '/subcribe/save',
		data : { name: subcriber_name, email_address: subcriber_email, source: 'press'},
		dataType : 'json',
		cache : false,
		async : true,
		timeout: 10000,
		success : function(response){
			$('#press-newsletter-email').val('');
			$('#press-newsletter-name').val('');
			if(response == 1) {
				alert('Thank you!');
				$('.subscriber_success').html('Thank you!');
			} else {
				$('.subscriber_success').html('Error on saving');
			}
		},
	});

    
   } else {
    return false;
   }
   
   
  });
  
  